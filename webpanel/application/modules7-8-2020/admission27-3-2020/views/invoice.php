<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Invoices</h1>            
          </div>
          <!-- <div>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
              <li><a href="<?php echo base_url();?>camera">Camera</a></li>
            </ul>
          </div> -->
    </div>
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="feesInfoform-validate" method="post" enctype="multipart/form-data">
						<input type="hidden" id="student_id" name="student_id" value="<?php if(!empty($student_id)){echo $student_id;}?>" />
						<?php if(isset($invoice)){?>
							<table class="table table-striped" style="border: 1px">
								<thead>
									<tr>
										<td>Fees Name</td>
					        			<td>Invoice</td>
					        			<td>Receipt</td>
									</tr>
								</thead>
								<tbody>
									<?php for($i=0;$i<sizeof($invoice);$i++){?>
										<tr>
											<td><?php echo $invoice[$i]['fees_name']?></td>
											<td><a href="<?php echo FRONT_API_URL."/images/payment_invoices/".$invoice[$i]['receipt_file_withoutgst']?>" target="_blank">View Invoice</a></td>
											<?php if(!empty($invoice[$i]['receipt_file'])){?>
											<td><a href="<?php echo FRONT_API_URL."/images/payment_invoices/".$invoice[$i]['receipt_file']?>" target="_blank">View Receipt</a></td>
										<tr>
									<?php }else{?>
									<td></td>
									<?php } }?>
								</tbody>
							</table>
						<?php }?>

					</form>
                </div>
            <div class="clearfix"></div>
            </div>
         </div>
    </div>        
	</div><!-- end: Content -->								
<script>

document.title = "Invoices";

 
</script>					
