<?php 
require_once 'includes/database.php';
$adminid = $_GET['adminid'];
$memberid = $_GET['id'];

$result = $db->query("SELECT * FROM user_registration WHERE user_id = " . $memberid);
$userreg = $result->fetch();
if($_GET['date'] == 'added'){
	echo "<script>alert('Date added successfully');</script>";
}
if($_GET['date'] == 'edited'){
	echo "<script>alert('Date edited successfully');</script>";
}
if($_GET['date'] == 'deleted'){
	echo "<script>alert('Date deleted successfully');</script>";
}
if($_GET['date'] == 'notadded'){
	echo "<script>alert('There was an error. Please try again.');</script>";
}
if ($_GET['action'] == 'add')
	{				
		$email_message_payment = urldecode($_GET['message']);
		$email_new_subject = urldecode($_GET['email_subject']);
		$q = $db->prepare("UPDATE user_registration SET approval_status = '1', userstatus = 'active' WHERE user_id = " . $memberid);
		$q->execute();
		echo "<script>alert('" . $userreg['fname'] . " " . $userreg['lname'] . " has been APPROVED and notified by email.');</script>";
		$active = 'active';
		$q2 = $db->prepare("INSERT INTO `user_status_history` (`user_id`, `status`) VALUES (:userid, :status);");
		$q2->bindParam('userid', $memberid,PDO::PARAM_STR);
		$q2->bindParam('status',$active,PDO::PARAM_STR);	
		$q2->execute();
		
		$today=date('Y-m-d');
		$end = date('Y-m-d', strtotime('+1 years'));
		$q81 = $db->prepare("UPDATE `user_registration` SET `approval_date` = :approval_date, `expiry_date` = :expiry_date WHERE `user_id` = '" . $memberid . "'");
		$q81->bindParam('approval_date', $today);
		$q81->bindParam('expiry_date', $end);
		$q81->execute();
		
		require '../../class.phpmailer.php';
		ob_start();
		include "../../emails/approval.php";
		$body = ob_get_contents();
		ob_end_clean();

		$mail = new PHPMailer;

		$mail->From = 'admin@sirfcoffee.com';
		$mail->FromName = 'Sirf Coffee';
		$mail->addAddress($userreg['email']);  // Add a recipient
		$mail->addReplyTo('query@sirfcoffee.com', 'Sirf Coffee');
		$mail->addBCC('query@sirfcoffee.com', 'Sirf Coffee');

		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = $email_new_subject;
		$mail->Body    = $body;

		if(!$mail->send()) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
		exit;
		}
	}
else if ($_GET['action'] == 'remove')
	{
		$email_message_payment = urldecode($_GET['message']);
		$email_new_subject = urldecode($_GET['email_subject']);
		$q = $db->prepare("UPDATE user_registration SET approval_status = '0', userstatus = 'declined' WHERE user_id = " . $memberid);
		$q->execute();
		$declined = 'declined';
		$q2 = $db->prepare("INSERT INTO `user_status_history` (`user_id`, `status`) VALUES (:userid, :status);");
		$q2->bindParam('userid', $memberid,PDO::PARAM_STR);
		$q2->bindParam('status',$declined,PDO::PARAM_STR);	
		$q2->execute();
		
		require '../../class.phpmailer.php';
		ob_start();
		include "../../emails/rejection.php";
		$body = ob_get_contents();
		ob_end_clean();

		$mail = new PHPMailer;

		$mail->From = 'admin@sirfcoffee.com';
		$mail->FromName = 'Sirf Coffee';
		$mail->addAddress($userreg['email']);  // Add a recipient
		$mail->addReplyTo('query@sirfcoffee.com', 'Sirf Coffee');
		$mail->addBCC('query@sirfcoffee.com', 'Sirf Coffee');
		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = $email_new_subject;
		$mail->Body    = $body;

		if(!$mail->send()) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
		exit;
		}
		echo "<script>alert('" . $userreg['fname'] . " " . $userreg['lname'] . " has been REJECTED and notified by email.');</script>";
	}
else if ($_GET['action'] == 'incompleteemail')
	{
		$encoded_email_message = base64_encode($_POST['approvaltext']);
		$decoded_email_message = base64_decode($encoded_email_message);
		$email_message_payment = $decoded_email_message;
		$email_new_subject = $_POST['email_subject'];
		
		require '../../class.phpmailer.php';
		ob_start();
		include "../../emails/rejection.php";
		$body = ob_get_contents();
		ob_end_clean();

		$mail = new PHPMailer;

		$mail->From = 'admin@sirfcoffee.com';
		$mail->FromName = 'Sirf Coffee';
		$mail->addAddress($userreg['email']);  // Add a recipient
		$mail->addReplyTo('query@sirfcoffee.com', 'Sirf Coffee');
		$mail->addBCC('query@sirfcoffee.com', 'Sirf Coffee');
		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = $email_new_subject;
		$mail->Body    = $body;

		if(!$mail->send()) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
		exit;
		}
		echo "<script>alert('" . $userreg['fname'] . " " . $userreg['lname'] . " has been notified by email.');</script>";
	}
else if ($_GET['action'] == 'expiredemail')
	{
		$email_message_payment = $_POST['approvaltext'];
		$email_new_subject = $_POST['email_subject'];
		
		require '../../class.phpmailer.php';
		ob_start();
		include "../../emails/rejection.php";
		$body = ob_get_contents();
		ob_end_clean();

		$mail = new PHPMailer;

		$mail->From = 'admin@sirfcoffee.com';
		$mail->FromName = 'Sirf Coffee';
		$mail->addAddress($userreg['email']);  // Add a recipient
		$mail->addReplyTo('query@sirfcoffee.com', 'Sirf Coffee');
		$mail->addBCC('query@sirfcoffee.com', 'Sirf Coffee');
		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = $email_new_subject;
		$mail->Body    = $body;

		if(!$mail->send()) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
		exit;
		}
		echo "<script>alert('" . $userreg['fname'] . " " . $userreg['lname'] . " has been notified by email.');</script>";
	}
else if ($_GET['action'] == 'unpaidemail')
	{
		$email_message_payment = $_POST['approvaltext'];
		$email_new_subject = $_POST['email_subject'];
		
		require '../../class.phpmailer.php';
		ob_start();
		include "../../emails/rejection.php";
		$body = ob_get_contents();
		ob_end_clean();

		$mail = new PHPMailer;

		$mail->From = 'admin@sirfcoffee.com';
		$mail->FromName = 'Sirf Coffee';
		$mail->addAddress($userreg['email']);  // Add a recipient
		$mail->addReplyTo('query@sirfcoffee.com', 'Sirf Coffee');
		$mail->addBCC('query@sirfcoffee.com', 'Sirf Coffee');
		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = $email_new_subject;
		$mail->Body    = $body;

		if(!$mail->send()) {
		echo 'Message could not be sent.';
		echo 'Mailer Error: ' . $mail->ErrorInfo;
		exit;
		}
		echo "<script>alert('" . $userreg['fname'] . " " . $userreg['lname'] . " has been notified by email.');</script>";
	}
else if ($_GET['status_changed'] == '1')
	{
	if($_POST['userstatus'] == 'active'){
		$today=date('Y-m-d');
		$end = date('Y-m-d', strtotime('+1 years'));
		$q3 = $db->prepare("UPDATE `user_registration` SET `approval_date` = :approval_date, `payment_date` = :payment_date, `expiry_date` = :expiry_date WHERE `user_id` = '" . $memberid . "'");
		$q3->bindParam('approval_date', $today);
		$q3->bindParam('payment_date', $today);
		$q3->bindParam('expiry_date', $end);
		$q3->execute();
	}
	$q = $db->prepare("UPDATE user_registration SET userstatus = :userstatus WHERE user_id = " . $memberid);
	$q->bindParam(':userstatus', $_POST['userstatus'], PDO::PARAM_STR);
	$q->execute();
	
	$q2 = $db->prepare("INSERT INTO `user_status_history` (`user_id`, `status`) VALUES (:userid, :status);");
	$q2->bindParam('userid', $memberid,PDO::PARAM_STR);
	$q2->bindParam('status',$_POST['userstatus'],PDO::PARAM_STR);	
	$q2->execute();
	}
	
if($_GET['action'] == 'renew'){
	$newexpirydate = $_POST['renew'];
	$active = 'active';
	$q3 = $db->prepare("UPDATE `user_registration` SET `expiry_date` = :expiry_date, `userstatus` = :userstatus WHERE `user_id` = '" . $memberid . "'");
	$q3->bindParam('expiry_date', $newexpirydate);
	$q3->bindParam('userstatus', $active);
	$q3->execute();
	
	$q2 = $db->prepare("INSERT INTO `user_status_history` (`user_id`, `status`) VALUES (:userid, :status);");
	$q2->bindParam('userid', $memberid,PDO::PARAM_STR);
	$q2->bindParam('status',$active,PDO::PARAM_STR);	
	$q2->execute();
}

if($_GET['do'] == 'nointereststatus'){

$active = 'nointerest';
	$q3 = $db->prepare("UPDATE `user_registration` SET `userstatus` = :userstatus WHERE `user_id` = '" . $memberid . "'");
	$q3->bindParam('userstatus', $active);
	$q3->execute();
	
	$q2 = $db->prepare("INSERT INTO `user_status_history` (`user_id`, `status`) VALUES (:userid, :status);");
	$q2->bindParam('userid', $memberid,PDO::PARAM_STR);
	$q2->bindParam('status',$active,PDO::PARAM_STR);	
	$q2->execute();
}	

$result = $db->query("SELECT * FROM user_registration WHERE user_id = " . $memberid);
$userreg = $result->fetch();
$profile_user = $userreg['fname'] . " " . $userreg['lname'];
$result = $db->query("SELECT * FROM user_basic_details WHERE user_id = " . $memberid);
$userbasic = $result->fetch();

$result = $db->query("SELECT * FROM user_edu_emp WHERE user_id = " . $memberid);
$usereduemp = $result->fetch();

$result = $db->query("SELECT * FROM user_about WHERE user_id = " . $memberid);
$userabout = $result->fetch();

$result = $db->query("SELECT * FROM user_soulmate WHERE user_id = " . $memberid);
$usersoul = $result->fetch();

$result = $db->query("SELECT * FROM user_photo WHERE user_id = " . $memberid);
$userphoto = $result->fetch();

$result = $db->query("SELECT * FROM date_history WHERE user_one = " . $memberid . " OR user_two = " . $memberid . " ORDER BY `when` DESC LIMIT 1");
$userdate = $result->fetch();

if (isset($userdate['dateid']))
{
$result = $db->query("SELECT fname, lname FROM user_registration WHERE user_id = " . $userdate['user_one']);
$user = $result->fetch();
$userone = $user['fname'] . " " . $user['lname'];

$result = $db->query("SELECT fname, lname FROM user_registration WHERE user_id = " . $userdate['user_two']);
$user = $result->fetch();
$usertwo = $user['fname'] . " " . $user['lname'];
}

$result = $db->query("SELECT * FROM user_communication WHERE userid = " . $memberid . " ORDER BY id DESC LIMIT 1" );
$usercomm = $result->fetch();

$result = $db->query("SELECT * FROM user_status_history WHERE user_id = " . $memberid . " AND status = '" . $userreg['userstatus'] . "' ORDER BY dateofchange DESC LIMIT 1" );
$userstat = $result->fetch();

$result->closeCursor();

$today=date('Y-m-d');
$today_plus_thirty = date ("Y-m-d", strtotime("+30 day", strtotime($today)));



?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>Sirf Coffee -  User Profile</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<?php include 'includes/headerdata.php'; ?>
		<link rel="stylesheet" href="css/jquery-ui.css" />
		<link rel="stylesheet" href="css/lightbox.css" />
		  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		  <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		  <script type="text/javascript" src="js/lightbox.js"></script>
		  <script type="text/javascript">
			function changeImage(){
				document.getElementById('toChange').src='../../customer-images/<?php echo $userphoto['photo1']; ?>';
				document.getElementById('toChangea').href='../../customer-images/<?php echo $userphoto['photo1']; ?>';
			}
		</script> 
		<script type="text/javascript">
			function statusconfirm(){
				var e = document.getElementById("userstatus");
				var strUser = e.options[e.selectedIndex].text;
			
				var sconfirm = confirm("Are you sure you want to change <?php echo $userreg['fname'] . " " . $userreg['lname']; ?>'s status to " + strUser + " ?")
				if (sconfirm==true)
				{
					document.getElementById('cstatusf').submit();
				}
				
			}
		</script>
		
		<script type="text/javascript">
			function renewconfirm(){
				var r = document.getElementById("renew");
				var rtext = r.value;
				
				var rconfirm = confirm("Are you sure you want to RENEW <?php echo $userreg['fname'] . " " . $userreg['lname']; ?> and change expiry date to " + rtext + " ?")
				if (rconfirm==true)
				{
					document.getElementById('renewform').submit();
				}
				
			}
		</script>
		<script type="text/javascript">
			function changeImage2(){
				document.getElementById('toChange').src='../../customer-images/<?php echo $userphoto['photo2']; ?>';
				document.getElementById('toChangea').href='../../customer-images/<?php echo $userphoto['photo2']; ?>';
			}
		</script>
		 <script>
		  $(function() {
			$( "#accordion" ).accordion({
			  heightStyle: "content"
			});
		  });
		  </script>
		  
		<link rel="stylesheet" type="text/css" href="css/digital.css">
		<script src="js/jquery.clock.js" type="text/javascript"></script>
		<?php if($userreg['timezone'] <> ''){ ?>
		<script type="text/javascript">
		 $(document).ready(function() {
		   $('#digital-clock').clock({offset: '<?php echo $userreg['timezone'];?>', type: 'digital'});
		 });
		</script>
		<?php }?>
		<style>
		.digital
		{
			position: relative;
			padding: 0px;
			margin: 80px 0px 0px 0px;
			list-style: none;
			font-size: 15px;
			text-align: center;
			width: 260px;
			font-family: 'myriad_proregular';
			color: #232323;
			font-weight: bold;
		}
		.digital li
		{
			position: relative;
			padding: 0px;
			margin: 0px;
			list-style: none;
			display: inline-block;
		}
		</style>
		
		<script>
			$(function() {
				$( "#renew" ).datepicker({ dateFormat: "yy-mm-dd" });
			});
		</script>
		<link rel="stylesheet" href="css/jquery-datepicker.css"/> 
	</head>

	<body>	
		<div class="wrapper">
		<?php include 'includes/logoandmenu.php'; ?>
		
			<div class="content">
				
				<br />
				<table width="100%">
					<tr>
						<td align="right" class="fakebutton" width="80%">			
							<form action="?id=<?php echo $memberid; ?>&adminid=<?php echo $adminid; ?>&status_changed=1" method="POST" style="float:right;" id="cstatusf">
							<div class="drop" style="float:left; width: 177px; position: relative; bottom: 5px; margin: 0px 5px;"><select name="userstatus" id="userstatus" style="width: 175px; height: 25px;">
								<?php if($userreg['userstatus'] == "active"){ ?>
								<option selected value="active">ACTIVE</option>
								<option value="incomplete">INCOMPLETE</option>
								<option value="pendingapproval">PENDING APPROVAL</option>
								<option value="completeunpaid">COMPLETE UNPAID</option>
								<option value="declined">DECLINED</option>
								<option value="expired">EXPIRED</option>
								<option value="matched">MATCHED</option>
								<option value="onhold">ON HOLD</option>
								<option value="refunded">REFUNDED</option>
								<option value="activeunpaid">ACTIVE UNPAID</option>
								<option value="nointerest">NO INTEREST</option>
								<?php } elseif($userreg['userstatus'] == "incomplete"){ ?>
								<option value="active">ACTIVE</option>
								<option selected value="incomplete">INCOMPLETE</option>
								<option value="pendingapproval">PENDING APPROVAL</option>
								<option value="completeunpaid">COMPLETE UNPAID</option>
								<option value="declined">DECLINED</option>
								<option value="expired">EXPIRED</option>
								<option value="matched">MATCHED</option>
								<option value="onhold">ON HOLD</option>
								<option value="refunded">REFUNDED</option>
								<option value="activeunpaid">ACTIVE UNPAID</option>
								<option value="nointerest">NO INTEREST</option>
								<?php } elseif($userreg['userstatus'] == "pendingapproval"){ ?>
								<option value="active">ACTIVE</option>
								<option value="incomplete">INCOMPLETE</option>
								<option selected value="pendingapproval">PENDING APPROVAL</option>
								<option value="completeunpaid">COMPLETE UNPAID</option>
								<option value="declined">DECLINED</option>
								<option value="expired">EXPIRED</option>
								<option value="matched">MATCHED</option>
								<option value="onhold">ON HOLD</option>
								<option value="refunded">REFUNDED</option>
								<option value="activeunpaid">ACTIVE UNPAID</option>
								<option value="nointerest">NO INTEREST</option>
								<?php } elseif($userreg['userstatus'] == "completeunpaid"){ ?>
								<option value="active">ACTIVE</option>
								<option value="incomplete">INCOMPLETE</option>
								<option value="pendingapproval">PENDING APPROVAL</option>
								<option selected value="completeunpaid">COMPLETE UNPAID</option>
								<option value="declined">DECLINED</option>
								<option value="expired">EXPIRED</option>
								<option value="matched">MATCHED</option>
								<option value="onhold">ON HOLD</option>
								<option value="refunded">REFUNDED</option>
								<option value="activeunpaid">ACTIVE UNPAID</option>
								<option value="nointerest">NO INTEREST</option>
								<?php } elseif($userreg['userstatus'] == "declined"){ ?>
								<option value="active">ACTIVE</option>
								<option value="incomplete">INCOMPLETE</option>
								<option value="pendingapproval">PENDING APPROVAL</option>
								<option value="completeunpaid">COMPLETE UNPAID</option>
								<option selected value="declined">DECLINED</option>
								<option value="expired">EXPIRED</option>
								<option value="matched">MATCHED</option>
								<option value="onhold">ON HOLD</option>
								<option value="refunded">REFUNDED</option>
								<option value="activeunpaid">ACTIVE UNPAID</option>
								<option value="nointerest">NO INTEREST</option>
								<?php } elseif($userreg['userstatus'] == "expired"){ ?>
								<option value="active">ACTIVE</option>
								<option value="incomplete">INCOMPLETE</option>
								<option value="pendingapproval">PENDING APPROVAL</option>
								<option value="completeunpaid">COMPLETE UNPAID</option>
								<option value="declined">DECLINED</option>
								<option selected value="expired">EXPIRED</option>
								<option value="matched">MATCHED</option>
								<option value="onhold">ON HOLD</option>
								<option value="refunded">REFUNDED</option>
								<option value="activeunpaid">ACTIVE UNPAID</option>
								<option value="nointerest">NO INTEREST</option>
								<?php } elseif($userreg['userstatus'] == "matched"){ ?>
								<option value="active">ACTIVE</option>
								<option value="incomplete">INCOMPLETE</option>
								<option value="pendingapproval">PENDING APPROVAL</option>
								<option value="completeunpaid">COMPLETE UNPAID</option>
								<option value="declined">DECLINED</option>
								<option value="expired">EXPIRED</option>
								<option selected value="matched">MATCHED</option>
								<option value="onhold">ON HOLD</option>
								<option value="refunded">REFUNDED</option>
								<option value="activeunpaid">ACTIVE UNPAID</option>
								<option value="nointerest">NO INTEREST</option>
								<?php } elseif($userreg['userstatus'] == "onhold"){ ?>
								<option value="active">ACTIVE</option>
								<option value="incomplete">INCOMPLETE</option>
								<option value="pendingapproval">PENDING APPROVAL</option>
								<option value="completeunpaid">COMPLETE UNPAID</option>
								<option value="declined">DECLINED</option>
								<option value="expired">EXPIRED</option>
								<option value="matched">MATCHED</option>
								<option selected value="onhold">ON HOLD</option>
								<option value="refunded">REFUNDED</option>
								<option value="activeunpaid">ACTIVE UNPAID</option>
								<option value="nointerest">NO INTEREST</option>
								<?php } elseif($userreg['userstatus'] == "refunded"){ ?>
								<option value="active">ACTIVE</option>
								<option value="incomplete">INCOMPLETE</option>
								<option value="pendingapproval">PENDING APPROVAL</option>
								<option value="completeunpaid">COMPLETE UNPAID</option>
								<option value="declined">DECLINED</option>
								<option value="expired">EXPIRED</option>
								<option value="matched">MATCHED</option>
								<option value="onhold">ON HOLD</option>
								<option selected value="refunded">REFUNDED</option>
								<option value="activeunpaid">ACTIVE UNPAID</option>
								<option value="nointerest">NO INTEREST</option>
								<?php } elseif($userreg['userstatus'] == "activeunpaid"){ ?>
								<option value="active">ACTIVE</option>
								<option value="incomplete">INCOMPLETE</option>
								<option value="pendingapproval">PENDING APPROVAL</option>
								<option value="completeunpaid">COMPLETE UNPAID</option>
								<option value="declined">DECLINED</option>
								<option value="expired">EXPIRED</option>
								<option value="matched">MATCHED</option>
								<option value="onhold">ON HOLD</option>
								<option value="refunded">REFUNDED</option>
								<option selected value="activeunpaid">ACTIVE UNPAID</option>
								<option value="nointerest">NO INTEREST</option>
								<?php } elseif($userreg['userstatus'] == "nointerest"){ ?>
								<option value="active">ACTIVE</option>
								<option value="incomplete">INCOMPLETE</option>
								<option value="pendingapproval">PENDING APPROVAL</option>
								<option value="completeunpaid">COMPLETE UNPAID</option>
								<option value="declined">DECLINED</option>
								<option value="expired">EXPIRED</option>
								<option value="matched">MATCHED</option>
								<option value="onhold">ON HOLD</option>
								<option value="refunded">REFUNDED</option>
								<option value="activeunpaid">ACTIVE UNPAID</option>
								<option selected value="nointerest">NO INTEREST</option>
								<?php }	else{ ?>
								<option value="active">ACTIVE</option>
								<option value="incomplete">INCOMPLETE</option>
								<option value="pendingapproval">PENDING APPROVAL</option>
								<option value="completeunpaid">COMPLETE UNPAID</option>
								<option value="declined">DECLINED</option>
								<option value="expired">EXPIRED</option>
								<option value="matched">MATCHED</option>
								<option value="onhold">ON HOLD</option>
								<option value="refunded">REFUNDED</option>
								<option value="activeunpaid">ACTIVE UNPAID</option>
								<option value="nointerest">NO INTEREST</option>
								<?php }?>
							</select></div>
							<a href="javascript:{}" onclick="statusconfirm()">CHANGE STATUS</a>
							</form>
							<?php if($userreg['imported'] == '1') echo "<a style='cursor: default; color: black; background: white;' href='javascript:void();'>IMPORTED</a>"; ?>
							<a href="edit-member.php?adminid=<?php echo $adminid; ?>&id=<?php echo $memberid; ?>">EDIT</a>
							<!--<a href="edit-member-picture-chooser.php?adminid=<?php echo $adminid; ?>&id=<?php echo $memberid; ?>">CHANGE PICTURE</a>-->
							<?php if($userreg['userstatus'] == 'completeunpaid' || $userreg['userstatus'] == 'incomplete') { echo "<a href='member-profile.php?adminid=" . $adminid . "&id=" . $memberid . "&do=nointereststatus'>NO INTEREST</a>"; } ?>
							<?php if($userreg['userstatus'] == 'pendingapproval') echo "<a href='approval.php?adminid=" . $adminid . "&id=" . $memberid . "'>APPROVE</a>"; ?>
							<?php if($userreg['userstatus'] == 'pendingapproval') echo "<a href='rejection.php?adminid=" . $adminid . "&id=" . $memberid . "'>REJECT</a>"; ?>
							<?php if($userreg['userstatus'] == 'incomplete') echo "<a href='request-email.php?adminid=" . $adminid . "&id=" . $memberid . "&s=incomplete'>REQUEST FOR INFO</a>"; ?>
							<?php if($userreg['userstatus'] == 'completeunpaid') echo "<a href='request-email.php?adminid=" . $adminid . "&id=" . $memberid . "&s=unpaid'>REQUEST FOR INFO</a>"; ?>
							<?php if($userreg['userstatus'] == 'expired') echo "<a href='request-email.php?adminid=" . $adminid . "&id=" . $memberid . "&s=expired'>SEND EMAIL</a>"; ?>
						</td>
						<?php if($userreg['userstatus'] == 'active' || $userreg['userstatus'] == 'activeunpaid' || $userreg['userstatus'] == 'expired'){?>
						<?php if($userreg['expiry_date'] <= $today_plus_thirty){ ?>
						<td width="20%" style="text-align:right;" valign="top">
							<div style="margin:-5px 0px 0px 0px;">
							<form action="?id=<?php echo $memberid; ?>&adminid=<?php echo $adminid; ?>&action=renew" method="POST" id="renewform" >
								<input type="text" style="width:100px;height:18px;margin-right:0px;" class="longtextbox spacer smallcaps" placeholder="YYYY-MM-DD" name="renew" id="renew" />
								<a href="javascript:{}" onclick="renewconfirm()">RENEW</a>
							</form>
							</div>
						</td>
						<?php } ?>
						<?php } ?>
					</tr>
				</table>
				<fieldset>
					<legend><h3><?php echo $userreg['fname'] . " " . $userreg['lname']; ?> <span class="<?php echo $userreg['userstatus']; ?>button"><?php getStatus($userreg['userstatus']); ?></span></h3></legend>
					<table cellspacing="15" width="100%">
						<tr>
							<td valign="top" width="15%">
								<a id="toChangea" href="../../customer-images/<?php echo $userphoto['photo1']; ?>" rel="lightbox"><img id="toChange" src="../../customer-images/<?php echo $userphoto['photo1']; ?>" width="150px"/></a>
								<div class="thumb">
									<img onclick="changeImage()" src="../../customer-images/<?php echo $userphoto['photo1']; ?>" width="70px"/>
								</div>
								<div class="thumb">		
									<img onclick="changeImage2()" src="../../customer-images/<?php echo $userphoto['photo2']; ?>" width="70px"/>
								</div>
							</td>
							<td valign="top" width="30%">
								<table>
									<tr>
										<td><i class="fa fa-user"></td>
										<td class="smallcaps2" style="text-transform: capitalize;"><?php echo $userreg['fname'] . " " . $userreg['lname']; ?></td>
									</tr>
									<tr>
										<td><i class="fa fa-user"></td>
										<td class="smallcaps2"><span class="rf"><?php echo $userreg['gender']; ?></span></td>
									</tr>
									<tr>
										<td><i class="fa fa-calendar"></td>
										<td class="smallcaps2">Born : <span class="rf"><?php $timestamp = strtotime($userreg['dob']); $age = floor(abs(time() - $timestamp)/(60*60*24*365)); echo date('d-M-Y', $timestamp) . "  (" . $age . ") "; ?></span></td>
									</tr>
									<tr>
										<td><i class="fa fa-map-marker"></td>
										<td class="smallcaps2" style="text-transform: capitalize;">From : <span class="rf"><?php echo $userbasic['originally_from']; ?></span></td>
									</tr>
									<tr>
										<td><i class="fa fa-home"></td>
										<td class="smallcaps2" style="text-transform: capitalize;">Currently In : <span class="rf"><?php echo $userbasic['city']; ?>, <?php echo $userbasic['country']; ?></span></td>
									</tr>
									<tr>
										<td><i class="fa fa-mobile"></td>
										<td class="smallcaps2">Cellphone : <span class="rf"><?php echo $userbasic['cellphone']; ?></span><br/></td>
									</tr>
									<tr>
										<td><i class="fa fa-envelope"></td>
										<td class="smallcaps2"><a href="mailto:<?php echo $userbasic['email']; ?>"><span class="rf"><?php echo $userreg['email']; ?></span></a><br/></td>
									</tr>
									<tr>
										<td><i class="fa fa-edit"></td>
										<td class="smallcaps2">Religion : <span class="rf"><?php echo $userbasic['religion']; ?></span></td>
									</tr>
									<tr>
										<td><i class="fa fa-edit"></td>
										<td class="smallcaps2" style="text-transform: capitalize;">Country of Citizenship : <span class="rf"><?php echo $userbasic['nationality']; ?></span></td>
									</tr>
									<tr>
										<td><i class="fa fa-edit"></td>
										<td class="smallcaps2">Sub-Ethnicity : <span class="rf"><?php echo $userbasic['sub_ethnicity']; ?></span><br/></td>
									</tr>
									<tr>
										<td valign="top"><i class="fa fa-briefcase"></i></td>
										<td class="smallcaps2">Currently Employed As : <br><span class="rf"><?php echo $usereduemp['position1']; ?></span> <br><span class="rf"><?php echo $usereduemp['current_employer']; ?> , <?php echo $usereduemp['job_city1']; ?> .</span></td>
									</tr>
									<tr>
										<td><i class="fa fa-edit"></td>
										<td class="smallcaps2">In current status since : <?php $stathis = strtotime($userstat['dateofchange']); echo date('d-M-Y', $stathis); ?><br/></td>
									</tr>
								</table>
							</td>
							<td valign="top" width="25%">
								<div>
									<fieldset>
										<legend><h3>LAST DATE</h3></legend>
										<table width="100%" class="lightgrey" cellpadding="5">
										
											<?php
												if($memberid <> $userdate['user_one'])
													{
											?>
											<tr>
												<td><img src="images/bname.png" /></td>
												<td class="smallcaps" valign="middle"><a href="member-profile.php?adminid=<?php echo $adminid; ?>&id=<?php echo $userdate['user_one']; ?>"><?php echo $userone; ?></a></td>
											</tr>
											<?php
													}
											?>
											<?php
												if($memberid <> $userdate['user_two'])
													{
											?>
											<tr>
												<td><img src="images/bname.png" /></td>
												<td class="smallcaps" valign="middle"><a href="member-profile.php?adminid=<?php echo $adminid; ?>&id=<?php echo $userdate['user_two']; ?>"><?php echo $usertwo; ?></a></td>
											</tr>
											<?php
													}
											?>
											<tr>
												<td><img src="images/calendar.png" /></td>
												<td class="smallcaps" valign="middle"><?php if($userdate['when'] <> ''){ $timestamp = strtotime($userdate['when']); echo date('d-M-Y', $timestamp); } ?></td>
											</tr>
											<tr>
												<td><img src="images/map-marker.png" /></td>
												<td class="smallcaps" valign="middle"><?php echo $userdate['where']; ?></td>
											</tr>
											<tr>
												<td><img src="images/map-marker.png" /></td>
												<td class="smallcaps" valign="middle"><?php echo $userdate['city']; ?></td>
											</tr>
										</table>
										<p style="text-align:right" class="fakebutton"><a href="add-date.php?adminid=<?php echo $adminid; ?>&id=<?php echo $memberid; ?>">ADD DATE</a></p>
									</fieldset>
								</div>
							</td>
							<td valign="top" width="30%">
								<fieldset>
										<legend><h3>LAST COMMUNICATION</h3></legend>
										<table width="100%" class="lightgrey" cellpadding="10">
											<tr>
												<td colspan="2"><?php echo $usercomm['comment']; ?></td>
											</tr>
											<tr>
												<td align="left"><?php echo $usercomm['admin']; ?></td>
												<td align="right"><?php echo $usercomm['updatedon']; ?></td>
											</tr>
										</table>
										<p style="text-align:right" class="fakebutton"><a href="add-communication.php?adminid=<?php echo $adminid; ?>&id=<?php echo $memberid; ?>">ADD COMMUNICATION</a></p> 
								</fieldset>
								<?php if($userreg['timezone'] <> ''){ ?>
								<ul id="digital-clock" class="digital">
									Current Local Time:<br/>
									<li class="hour"></li>
									<li class="min"></li>
									<li class="sec"></li>
									<li class="meridiem"></li>
								</ul>
								<?php } ?>
							</td>
						</tr>
					</table>
				</fieldset>
				
				<div id="accordion"> 
					<h3>STATUS HISTORY</h3>
					<div>
						<?php include 'includes/member-profile/status-history.php'; ?>
					</div>
					<h3>DATE HISTORY</h3>
					<div>
						<?php include 'includes/member-profile/datehistory.php'; ?>
					</div>
					<h3>COMMUNICATION HISTORY</h3>
					<div>
						<?php include 'includes/member-profile/usercommunication.php'; ?>
					</div>
					<h3>BASIC DETAILS</h3>
					<div>
						<?php include 'includes/member-profile/basicdetails.php'; ?>
					</div>
					<h3>EDUCATION AND EMPLOYMENT</h3>
					<div>
						<?php include 'includes/member-profile/education.php'; ?>
					</div>
					<h3>ABOUT YOU</h3>
					<div>
						<?php include 'includes/member-profile/about-you.php'; ?>
					</div>
					<h3>ABOUT YOUR SOULMATE</h3>
					<div>
						<?php include 'includes/member-profile/soulmate.php'; ?>
					</div>
				</div>
			</div>
		<?php include 'includes/footer.php' ?>
		</div>
	</body>
</html>
