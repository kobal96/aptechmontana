<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Remainingfees extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('remainingfeesmodel', '', TRUE);
		$this->load->model('admission/admissionmodel', '', TRUE);
	}

	function index(){
		if (!empty($_SESSION["webadmin"])) {
			if ($this->privilegeduser->hasPrivilege("RemainingFeesPayment")) {
				$result['academicyear'] = $this->remainingfeesmodel->getDropdown1("tbl_academic_year_master","academic_year_master_id,academic_year_master_name");
				$result['zones'] = $this->remainingfeesmodel->getDropdown1("tbl_zones","zone_id,zone_name");
				$result['login_name'] = $_SESSION["webadmin"][0]->first_name .' '. $_SESSION["webadmin"][0]->last_name;
				$result['groups'] = $this->remainingfeesmodel->getDropdown1("tbl_group_master", "group_master_id, group_master_name");
				$result['fees_type_details'] = $this->remainingfeesmodel->enum_select("tbl_assign_fees","fees_type");
				$this->load->view('template/header.php');
				$this->load->view('remainingfees/addEdit',$result);
				$this->load->view('template/footer.php');
			}else{
				redirect('home', 'refresh');
			}
		}else {
			redirect('login', 'refresh');
		}
	}
	
	public function getCenters(){
		$result = $this->remainingfeesmodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		$option = '';
		$center_id = '';
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->center_id == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getCenterStudent(){
	$condition ="  af.academic_year_id = '".$_REQUEST['academic_year_id']."' &&  p.promoted_academic_year_id='".$_REQUEST['academic_year_id']."' && s.zone_id='".$_REQUEST['zone_id']."' && s.center_id='".$_REQUEST['center_id']."' ";
		$result = $this->remainingfeesmodel->getStudentClassWiseList($condition);
		$option = '';
		$student_id = '';
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->student_id == $student_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->student_id.'" '.$sel.' >'.$result[$i]->enrollment_no.' ( '.$result[$i]->student_first_name.' '.$result[$i]->student_last_name.' )</option>';
			}
		}
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getGroupStudent(){
		$condition ="  d.academic_year_id='".$_REQUEST['academic_year_id']."' && d.group_id='".$_REQUEST['group_id']."' && d.zone_id='".$_REQUEST['zone_id']."' && d.center_id='".$_REQUEST['center_id']."' ";
		$result = $this->remainingfeesmodel->getStudentGroupWiseList($condition);
		$option = '';
		$student_id = '';
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->student_id == $student_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->student_id.'" '.$sel.' >'.$result[$i]->enrollment_no.' ( '.$result[$i]->student_first_name.' '.$result[$i]->student_last_name.' )</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getStudentDetails(){
		$condition = " student_id='".$_REQUEST['student_id']."' && academic_year_id='".$_REQUEST['academic_year_id']."' && zone_id='".$_REQUEST['zone_id']."' && center_id='".$_REQUEST['center_id']."' && center_id='".$_REQUEST['center_id']."' "; 
		$getDetails = $this->remainingfeesmodel->getdata("tbl_admission_fees", "admission_fees_id, academic_year_id, student_id, fees_id, fees_type, zone_id, center_id, category_id, course_id, group_id, is_instalment, no_of_installments, fees_total_amount, fees_amount_collected, fees_remaining_amount", $condition);
		$total_fees_amount = 0;
		$total_collected_amount = 0;
		$total_remaining_amount = 0;
		
		if (!empty($getDetails)) {
			$all_installments = array();
			foreach($getDetails as $key=>$val){
				$all_installments = $this->remainingfeesmodel->getdata("tbl_admission_fees_instalments", "admission_fees_instalment_id, admission_fees_id, instalment_due_date, instalment_amount, instalment_collected_amount, instalment_remaining_amount, instalment_status, created_on, updated_on", "academic_year_id='".$_REQUEST['academic_year_id']."' && student_id='".$_REQUEST['student_id']."' && admission_fees_id='".$val['admission_fees_id']."' ");
				// echo "<pre>";print_r($all_installments );echo "</pre>";
				if(!empty($all_installments)){
					for($i=0; $i < sizeof($all_installments); $i++){
						$all_installments[$i]['actual_due_date'] = strtotime($all_installments[$i]['instalment_due_date']);
						$all_installments[$i]['instalment_due_date'] = date("d M Y", strtotime($all_installments[$i]['instalment_due_date']));
						$getCategoryDetails = $this->remainingfeesmodel->getdata("tbl_categories", "categoy_name", "category_id='".$val['category_id']."' ");
						$getCourseDetails = $this->remainingfeesmodel->getdata("tbl_courses", "course_name", "course_id='".$val['course_id']."' ");
						$all_installments[$i]['categoy_name'] = (!empty($getCategoryDetails[0]['categoy_name'])) ? $getCategoryDetails[0]['categoy_name'] : '--';
						$all_installments[$i]['course_name'] = (!empty($getCourseDetails[0]['course_name'])) ? $getCourseDetails[0]['course_name'] : '';
						$getGroupDetails = $this->remainingfeesmodel->getdata("tbl_group_master", "group_master_name", "group_master_id='".$val['group_id']."' ");
						$all_installments[$i]['group_name'] = (!empty($getGroupDetails[0]['group_master_name'])) ? $getGroupDetails[0]['group_master_name'] : '';
						$total_fees_amount   = $total_fees_amount + $all_installments[$i]['instalment_amount'];
						$total_collected_amount  = $total_collected_amount + $all_installments[$i]['instalment_collected_amount'];
						$total_remaining_amount  = $total_remaining_amount + $all_installments[$i]['instalment_remaining_amount'];
					}
				}
				if(!empty($all_installments)){
					$getDetails[$key]['instalment_details'] = $all_installments;
				}else{
					$getDetails[$key]['instalment_details'] = NULL;
				}
			}
		}
		
		// exit;
		$totalDetails = array();
		$totalDetails['fees_total_amount'] = $total_fees_amount;
		$totalDetails['total_collected_amount'] = $total_collected_amount;
		$totalDetails['total_remaining_amount'] = $total_remaining_amount;

		//get student common details
		$student_common_details = $this->remainingfeesmodel->getdata("tbl_student_master","*","student_id = '".$_REQUEST['student_id']."'");

		echo json_encode(array("status"=>"success","option"=>$getDetails,'total_details'=>$totalDetails,'student_details'=>$student_common_details));
		exit;
	}

	
	function submitRemainingFees(){
		// echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
			$student_id = $_POST['student_id'];

			//get admission details
			$admissionDetails = array();
			if(!empty($_POST['admission_fees_id'][0])){
				$admissionDetails = $this->remainingfeesmodel->getAdmissionDetails($_POST['admission_fees_id'][0]);
			}

			//check receipt date
			if(!empty($admissionDetails)){
				if(strtotime($_POST['receipt_date']) < strtotime($admissionDetails[0]['admission_date'])){
					echo json_encode(array("success"=>"0",'msg'=>"Receipt date should not less than admission date. [Admission Date = ".date('d-M-Y',strtotime($admissionDetails[0]['admission_date']))."]"));
					exit;
				}
			}

			if($_POST['payment_mode'] == 'Cheque'){
				if(empty($_POST['bank_name'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Bank Name!'));
					exit;
				}
				if(empty($_POST['cheque_no'])){
					echo json_encode(array("success"=>"0",'msg'=>'EnterCheque No!'));
					exit;
				}
				if(empty($_POST['cheque_date'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Cheque Date!'));
					exit;
				}else{
					//cheque date equal or greater than admission date
					if(!empty($admissionDetails)){
						if(strtotime($_POST['cheque_date']) < strtotime($admissionDetails[0]['admission_date'])){
							echo json_encode(array("success"=>"0",'msg'=>'Cheque date should not less than admission date'));
							exit;
						}
					}

					//cheque date greater than or equal to receipt date
					if(!empty($_POST['receipt_date'])){
						if(strtotime($_POST['cheque_date']) < strtotime($_POST['receipt_date'])){
							echo json_encode(array("success"=>"0",'msg'=>'Cheque date should not less than 
							Receipt date'));
							exit;
						}
					}
				}
			}
			
			if($_POST['payment_mode'] == 'Netbanking')
			{
				if(empty($_POST['bank_name'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Bank Name!'));
					exit;
				}
				if(empty($_POST['transaction_id'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Transaction No!'));
					exit;
				}
				if(empty($_POST['transaction_date'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Transaction Date!'));
					exit;
				}else{
					//transaction date equal or greater than admission date
					if(!empty($admissionDetails)){
						if(strtotime($_POST['transaction_date']) < strtotime($admissionDetails[0]['admission_date'])){
							echo json_encode(array("success"=>"0",'msg'=>'Transaction date should not less than admission date'));
							exit;
						}
					}

					//transaction date greater than or equal to receipt date
					if(!empty($_POST['receipt_date'])){
						if(strtotime($_POST['transaction_date']) < strtotime($_POST['receipt_date'])){
							echo json_encode(array("success"=>"0",'msg'=>'Transaction date should not less than 
							Receipt date'));
							exit;
						}
					}
				} 
			}

			if(empty($_POST['collected_amount_total']) || $_POST['collected_amount_total'] == 0 || $_POST['collected_amount_total'] ==""){
				echo json_encode(array("success"=>"0",'msg'=>'Please enter some amount for payment.'));
				exit;
			}else if($_POST['collected_amount_total'] != $_POST['total_transaction_amount']){
				echo json_encode(array("success"=>"0",'msg'=>'Receipt Amount and transaction amount are different.'));
				exit;
			}

			$admission_fees_id_array = array();
			if(!empty($_POST['admission_fees_id'])){
				$admission_fees_id_array = explode(",",$_POST['admission_fees_id']);
			}
			
			$group_components_result = array();
			$class_components_result = array();
			$instalment_collected_amount = array();
			
			//group component fee insertion for installment
			if(!empty($_POST['admission_fees_typewise_id']['Group'])){
				foreach($_POST['admission_fees_typewise_id']['Group'] as $key => $val){
					$total_collected_group_amount = 0;
					foreach($_POST['installment_collected_amount']['Group'][$key] as $k => $v){
						$instalment_collected_amount[] = $v;
						$total_collected_group_amount += $v;
					}
					//get the components of the Group 
					$group_components_result = $this->remainingfeesmodel->getGroupFeesComponents($key,$val[0]);
					if(!empty($group_components_result)){
						$remaining_total_amount = 0;
						foreach($group_components_result as $ckey=>$cval){
							// existing history of payment
							$group_component_payment_history = $this->remainingfeesmodel->getdata("tbl_admission_fees_components_collected_history", "*", $condition = "is_last_entry = 'Yes' AND admission_fees_component_id = '".$cval['admission_fees_component_id']."'");
							if(!empty($group_component_payment_history)){
								//update exiting history entry 
								$existing_data = array();
								$existing_data['is_last_entry'] = "No";
								$this->remainingfeesmodel->updateRecord('tbl_admission_fees_components_collected_history', $existing_data, "component_collected_history_id",$group_component_payment_history[0]['component_collected_history_id']);

								//make new entry for payment history
								$collected_component_history_data = array();
								$collected_component_history_data['admission_fees_component_id'] = $cval['admission_fees_component_id'];
		
								//collected amount calculation
								$collected_amount = $total_collected_group_amount;
								$remaining_total_amount = $total_collected_group_amount - $group_component_payment_history[0]['remaining_amount'];
								if(($remaining_total_amount) > 0){
									$collected_amount = $group_component_payment_history[0]['remaining_amount'];
									$total_collected_group_amount = $remaining_total_amount;
								}else{
									$total_collected_group_amount = 0;
								}
								
								$collected_component_history_data['collected_amount'] = $collected_amount;
								$collected_component_history_data['remaining_amount'] = ($group_component_payment_history[0]['remaining_amount'] - $collected_amount);
								$collected_component_history_data['is_last_entry'] = "Yes";
								$collected_component_history_data['collected_date'] = date("Y-m-d");
								
								// insert new history
								$this->remainingfeesmodel->insertData('tbl_admission_fees_components_collected_history', $collected_component_history_data, 1);
							}else{
								$collected_component_history_data = array();
								$collected_component_history_data['admission_fees_component_id'] = $cval['admission_fees_component_id'];
		
								//collected amount calculation
								$collected_amount = $total_collected_group_amount;
								$remaining_total_amount = $total_collected_group_amount - $cval['sub_total'];
								if(($remaining_total_amount) > 0){
									$collected_amount = $cval['sub_total'];
									$total_collected_group_amount = $remaining_total_amount;
								}else{
									$total_collected_group_amount = 0;
								}
								$collected_component_history_data['collected_amount'] = $collected_amount;
								$collected_component_history_data['remaining_amount'] = ($cval['sub_total'] - $collected_amount);
								$collected_component_history_data['is_last_entry'] = "Yes";
								$collected_component_history_data['collected_date'] = date("Y-m-d");
								$this->remainingfeesmodel->insertData('tbl_admission_fees_components_collected_history', $collected_component_history_data, 1);
							}
						}
					}
				}
			}
		
			//Class component fee insertion  for installment
			if(!empty($_POST['admission_fees_typewise_id']['Class'])){
				foreach($_POST['admission_fees_typewise_id']['Class'] as $key => $val){
					$total_collected_class_amount = 0;
					foreach($_POST['installment_collected_amount']['Class'][$key] as $k => $v){
						$instalment_collected_amount[] = $v;
						$total_collected_class_amount += $v;
					}
					//get the components of the Class 
					$class_components_result = $this->remainingfeesmodel->getClassFeesComponents($key,$val[0]);
					if(!empty($class_components_result)){
						$remaining_total_amount = 0;
						foreach($class_components_result as $ckey=>$cval){
							// existing history of payment
							$class_component_payment_history = $this->remainingfeesmodel->getdata("tbl_admission_fees_components_collected_history", "*", $condition = "is_last_entry = 'Yes' AND admission_fees_component_id = '".$cval['admission_fees_component_id']."'");
							if(!empty($class_component_payment_history)){
								//update exiting history entry 
								$existing_data = array();
								$existing_data['is_last_entry'] = "No";
								$this->remainingfeesmodel->updateRecord('tbl_admission_fees_components_collected_history', $existing_data, "component_collected_history_id",$class_component_payment_history[0]['component_collected_history_id']);

								//make new entry for payment history
								$collected_component_history_data = array();
								$collected_component_history_data['admission_fees_component_id'] = $cval['admission_fees_component_id'];
		
								//collected amount calculation
								$collected_amount = $total_collected_class_amount;
								$remaining_total_amount = $total_collected_class_amount - $class_component_payment_history[0]['remaining_amount'];
								if(($remaining_total_amount) > 0){
									$collected_amount = $class_component_payment_history[0]['remaining_amount'];
									$total_collected_class_amount = $remaining_total_amount;
								}else{
									$total_collected_class_amount = 0;
								}
								
								$collected_component_history_data['collected_amount'] = $collected_amount;
								$collected_component_history_data['remaining_amount'] = ($class_component_payment_history[0]['remaining_amount'] - $collected_amount);
								$collected_component_history_data['is_last_entry'] = "Yes";
								$collected_component_history_data['collected_date'] = date("Y-m-d");
								
								//insert new history
								$this->remainingfeesmodel->insertData('tbl_admission_fees_components_collected_history', $collected_component_history_data, 1);
							}else{
								$collected_component_history_data = array();
								$collected_component_history_data['admission_fees_component_id'] = $cval['admission_fees_component_id'];
		
								//collected amount calculation
								$collected_amount = $total_collected_class_amount;
								$remaining_total_amount = $total_collected_class_amount - $cval['sub_total'];
								if(($remaining_total_amount) > 0){
									$collected_amount = $cval['sub_total'];
									$total_collected_class_amount = $remaining_total_amount;
								}else{
									$total_collected_class_amount = 0;
								}
								
								$collected_component_history_data['collected_amount'] = $collected_amount;
								$collected_component_history_data['remaining_amount'] = ($cval['sub_total'] - $collected_amount);
								$collected_component_history_data['is_last_entry'] = "Yes";
								$collected_component_history_data['collected_date'] = date("Y-m-d");
								$this->remainingfeesmodel->insertData('tbl_admission_fees_components_collected_history', $collected_component_history_data, 1);
							}
						}
					}
				}
			}
			
			
			/* ------------------------------ */
			$total_mandatory_amount = 0;
			//update data
			foreach($admission_fees_id_array as $key=>$val){
				//updation for class 
				if(!empty($_POST['installment_collected_amount']['Class'][$val])){
					$total_amount = 0;
					$already_collected_amount = 0;
					$total_remaining_amount = 0;
					for ($i=0; $i < sizeof($_POST['installment_collected_amount']['Class'][$val]); $i++) { 
						$total_amount = $total_amount + $_POST['instalment_collected_amount']['Class'][$val][$i] + $_POST['installment_collected_amount']['Class'][$val][$i];
						$already_collected_amount = $already_collected_amount + $_POST['installment_collected_amount']['Class'][$val][$i];
						$total_remaining_amount = $total_remaining_amount + $_POST['installment_remaining_amount']['Class'][$val][$i];
						$total_mandatory_amount = $total_mandatory_amount + $_POST['installment_collected_amount']['Class'][$val][$i];
					}
				//	print_r();exit;
					$total_collected_amount = $total_amount;
					$feesdata['fees_amount_collected'] = (!empty($total_collected_amount)) ? $total_collected_amount : 0;
					$feesdata['fees_remaining_amount'] = (!empty($total_remaining_amount)) ? $total_remaining_amount : 0;
					$feesdata['updated_on'] = date("Y-m-d H:m:i");
					$feesdata['updated_by'] = $_SESSION["webadmin"][0]->user_id;
					$this->remainingfeesmodel->updateRecord('tbl_admission_fees', $feesdata, "admission_fees_id",$val);
		
		
					for ($ins=0; $ins <sizeof($_POST['installment_collected_amount']['Class'][$val]) ; $ins++) {
						$collected_amount = $_POST['instalment_collected_amount']['Class'][$val][$ins] + $_POST['installment_collected_amount']['Class'][$val][$ins];
						$installmentdata['instalment_collected_amount'] = (!empty($collected_amount) ? $collected_amount : 0);
						$installmentdata['instalment_remaining_amount'] = (!empty($_POST['installment_remaining_amount']['Class'][$val][$ins]) ? $_POST['installment_remaining_amount']['Class'][$val][$ins] : $_POST['instalment_amount']['Class'][$val][$ins]);
						if($collected_amount == '0'){
							$installmentdata['instalment_status'] = 'Pending';
						}else if($_POST['installment_remaining_amount']['Class'][$val][$ins] == '0'){
							$installmentdata['instalment_status'] = 'Paid';
						}else if($_POST['instalment_amount']['Class'][$val][$ins] > $collected_amount){
							$installmentdata['instalment_status'] = 'Partial';
						}
						$installmentdata['updated_on'] = date("Y-m-d H:m:i");
						$installmentdata['updated_by'] = $_SESSION["webadmin"][0]->user_id;
						$result = $this->remainingfeesmodel->updateRecord('tbl_admission_fees_instalments', $installmentdata, "admission_fees_instalment_id",$_POST['admission_fees_instalment_id']['Class'][$val][$ins]);
					}
				}

				//updation for Group 
				if(!empty($_POST['installment_collected_amount']['Group'][$val])){
					$total_amount = 0;
					$already_collected_amount = 0;
					$total_remaining_amount = 0;
					for ($i=0; $i < sizeof($_POST['installment_collected_amount']['Group'][$val]); $i++) { 
						$total_amount = $total_amount + $_POST['instalment_collected_amount']['Group'][$val][$i] + $_POST['installment_collected_amount']['Group'][$val][$i];
						$already_collected_amount = $already_collected_amount + $_POST['installment_collected_amount']['Group'][$val][$i];
						$total_remaining_amount = $total_remaining_amount + $_POST['installment_remaining_amount']['Group'][$val][$i];
						$total_mandatory_amount = $total_mandatory_amount + $_POST['installment_collected_amount']['Group'][$val][$i];
					}
					$total_collected_amount = $total_amount;
					$feesdata['fees_amount_collected'] = (!empty($total_collected_amount)) ? $total_collected_amount : 0;
					$feesdata['fees_remaining_amount'] = (!empty($total_remaining_amount)) ? $total_remaining_amount : 0;
					$feesdata['updated_on'] = date("Y-m-d H:m:i");
					$feesdata['updated_by'] = $_SESSION["webadmin"][0]->user_id;
					$this->remainingfeesmodel->updateRecord('tbl_admission_fees', $feesdata, "admission_fees_id",$val);

					for ($ins=0; $ins <sizeof($_POST['installment_collected_amount']['Group'][$val]) ; $ins++) {
						$collected_amount = $_POST['instalment_collected_amount']['Group'][$val][$ins] + $_POST['installment_collected_amount']['Group'][$val][$ins];
						$installmentdata['instalment_collected_amount'] = (!empty($collected_amount) ? $collected_amount : 0);
						$installmentdata['instalment_remaining_amount'] = (!empty($_POST['installment_remaining_amount']['Group'][$val][$ins]) ? $_POST['installment_remaining_amount']['Group'][$val][$ins] : $_POST['instalment_amount']);
						if($collected_amount == '0'){
							$installmentdata['instalment_status'] = 'Pending';
						}else if($_POST['installment_remaining_amount']['Group'][$val][$ins] == '0'){
							$installmentdata['instalment_status'] = 'Paid';
						}else if($_POST['instalment_amount']['Group'][$val][$ins] > $collected_amount){
							$installmentdata['instalment_status'] = 'Partial';
						}
						$installmentdata['updated_on'] = date("Y-m-d H:m:i");
						$installmentdata['updated_by'] = $_SESSION["webadmin"][0]->user_id;
						$result = $this->remainingfeesmodel->updateRecord('tbl_admission_fees_instalments', $installmentdata, "admission_fees_instalment_id",$_POST['admission_fees_instalment_id']['Group'][$val][$ins]);
					}
				}
			}

			/* ------------------------------ */
			if(!empty($_POST['admission_fees_actual_id'])){
				foreach($_POST['admission_fees_actual_id'] as $key=>$val){
					$isPaymentDone = $this->remainingfeesmodel->getdata("tbl_fees_payment_details", "*", "admission_fees_id='".$val."' ");
					//changeexisting payment details
					if(!empty($isPaymentDone)){
						foreach($isPaymentDone as $pkey=>$pval){
							$existing_payment_data = array();
							$existing_payment_data['is_last_entry'] = 'No';
							$this->remainingfeesmodel->updateRecord('tbl_fees_payment_details', $existing_payment_data,"fees_payment_detail_id",$pval['fees_payment_detail_id']);
						}
					}
				}
				foreach($_POST['admission_fees_actual_id'] as $key=>$val){
					$payment_data = array();
					$payment_data['admission_fees_id'] = $val;
					$payment_data['admission_fees_instalment_id'] = $_POST['admission_fees_instalment_actual_id'][$key];
					$payment_data['instalment_collected_amount'] = $_POST['installment_collected_actual_amount'][$key];
					$payment_data['payment_mode'] = (!empty($_POST['payment_mode'])) ? $_POST['payment_mode'] : '' ;
					$payment_data['bank_name'] = (!empty($_POST['bank_name'])) ? $_POST['bank_name'] : '' ;
					if($_POST['payment_mode']=='Netbanking'){
						$payment_data['transaction_date'] = (!empty($_POST['transaction_date'])) ? date("Y-m-d", strtotime($_POST['transaction_date'])) : date('Y-m-d') ;
					}
					if($_POST['payment_mode']=='Cheque'){
						$payment_data['transaction_date'] = (!empty($_POST['cheque_date'])) ? date("Y-m-d", strtotime($_POST['cheque_date'])) : date('Y-m-d') ;
					}
					$payment_data['cheque_no'] = (!empty($_POST['cheque_no'])) ? $_POST['cheque_no'] : '' ;
					$payment_data['transaction_id'] = (!empty($_POST['transaction_id'])) ? $_POST['transaction_id'] : '' ;
					$payment_data['receipt_date'] = (!empty($_POST['receipt_date'])) ? date("Y-m-d",strtotime($_POST['receipt_date'])) : '' ;
					$payment_data['fees_remark'] = (!empty($_POST['fees_remark'])) ? $_POST['fees_remark'] : '' ;
					$payment_data['created_on'] = date("Y-m-d H:m:i");
					$payment_data['created_by'] = $_SESSION["webadmin"][0]->user_id;
					$payment_data['updated_on'] = date("Y-m-d H:m:i");
					$payment_data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
					$save_data = $this->remainingfeesmodel->insertData('tbl_fees_payment_details', $payment_data, 1);
				}
			}

			/* mandatory amount paid then student active for group and class start */
			// for course
			if(!empty($_POST['admission_fees_typewise_id']['Class'])){
				foreach($_POST['admission_fees_typewise_id']['Class'] as $key => $val){
					$total_class_remaining_details = $this->remainingfeesmodel->getFeesComponentPaidDetails($key);
					$class_components_result = $this->remainingfeesmodel->getClassFeesComponents($key,$val[0],"Yes");
					$status_data = array();
					if(!empty($class_components_result)){
						if(!empty($total_class_remaining_details)){
							$total_ramaining_amount =  array_sum(array_column($total_class_remaining_details,"remaining_amount"));
							if($total_ramaining_amount == 0){
								$status_data['status'] = "Active";
							}else{
								$status_data['status'] = "In-active";
							}
						}else{
							$status_data['status'] = "In-active";
						}
					}else{
						$status_data['status'] = "Active";
					}
					$this->remainingfeesmodel->updateRecord('tbl_student_master', $status_data, "student_id",$student_id);
					$this->remainingfeesmodel->updateRecord('tbl_student_details', $status_data, "student_id",$student_id);
				}
			}

			
			//for group
			if(!empty($_POST['admission_fees_typewise_id']['Group'])){
				foreach($_POST['admission_fees_typewise_id']['Group'] as $key => $val){
					$total_group_remaining_details = $this->remainingfeesmodel->getFeesComponentPaidDetails($key);
					//get admission fees data
					$group_admission_group_data = $this->remainingfeesmodel->getdata1("tbl_admission_fees","admission_fees_id='".$key."' ");
					$group_components_result = $this->remainingfeesmodel->getGroupFeesComponents($key,$val[0],"Yes");
					$status_data = array();
					if(!empty($group_components_result)){
						if(!empty($total_group_remaining_details) && !empty($group_admission_group_data)){
							$total_ramaining_amount =  array_sum(array_column($total_group_remaining_details,"remaining_amount"));
							if($total_ramaining_amount == 0){
								$status_data['status'] = "Active";
							}else{
								$status_data['status'] = "In-active";
							}
						}else{
							$status_data['status'] = "In-active";
						}
					}else{
						$status_data['status'] = "Active";
					}
					$group_status_condition = " zone_id='".$group_admission_group_data[0]['zone_id']."' AND center_id='".$group_admission_group_data[0]['center_id']."' AND academic_year_id='".$group_admission_group_data[0]['academic_year_id']."' AND  student_id='".$student_id."' AND group_id='".$val[0]."' ";
					$this->remainingfeesmodel->updateRecordbyConditionArray("tbl_student_group",$status_data,$group_status_condition);
				}
			}
			/* mandatory amount paid then student active for group and class end*/

			if (!empty($save_data)) {
				echo json_encode(array('success' => '1','msg' => 'Record Added/Updated Successfully.','student_id' => $student_id));
				exit;
			}else{
				echo json_encode(array('success' => '0','msg' => 'Problem in data update.'));
				exit;
			}
		}else{
			return false;
		}
	}

	//copied from admission payment process
	function getPayemtDetails(){
		// echo "<pre>";print_r($_POST);exit;
		if(!empty($_REQUEST['recipt_for'])){
			//update recipt print name
			$student_receipt_data = array();
			$student_receipt_data['guardian'] = $_REQUEST['recipt_for'];
			$this->admissionmodel->updateRecord('tbl_student_master', $student_receipt_data,'student_id',$_REQUEST['student_id']);
		}
		$reciept_date = (!empty($_POST['receipt_date']))?date("Y-m-d",strtotime($_POST['receipt_date'])):date("Y-m-d");

		$studentinfo = $this->admissionmodel->getdata1("tbl_admission_fees", "admission_fees_id,fees_type,category_id", "student_id='".$_REQUEST['student_id']."' && academic_year_id = '".$_REQUEST['academic_year_id']."' ");
		/* ---------------- single receiep strat ------------- */
		// single receipt
		$reciept_no = "";
		$receipt_amount = 0;
		$receipt_payment_mode = "";
		$return_receipt = "";

		// echo "<pre>";print_r($studentinfo);
		if(!empty($studentinfo)){
			$admition_id = array_column($studentinfo,"admission_fees_id");
			$admintion_fee_id = implode(',',$admition_id);
			$getPayemtDetails = $this->admissionmodel->getPayemtDetails("i.admission_fees_id IN(".$admintion_fee_id.")");
			$class_name = "";
			// echo "<pre>";print_r($getPayemtDetails);exit;

			if(!empty($getPayemtDetails)){
				$getPayemtDetails = array_reverse($getPayemtDetails);
				$class_name = array_values(array_filter(array_column($getPayemtDetails,"course_name")))[0];
			}
			$count = count($getPayemtDetails);
			$student_name = (!empty($getPayemtDetails[0]['student_first_name'])) ? $getPayemtDetails[0]['student_first_name']." ".$getPayemtDetails[0]['student_last_name'] : '';
			$academic_year = (!empty($getPayemtDetails[0]['academic_year_master_name'])) ? $getPayemtDetails[0]['academic_year_master_name'] : '';
			$category_name = (!empty($getPayemtDetails[0]['categoy_name'])) ? $getPayemtDetails[0]['categoy_name'] : '';
			$course_name = (!empty($getPayemtDetails[0]['course_name'])) ? $getPayemtDetails[0]['course_name'] : '';
			$batch_name = (!empty($getPayemtDetails[0]['batch_name'])) ? $getPayemtDetails[0]['batch_name'] : '';
			$group_name = (!empty($getPayemtDetails[0]['group_master_name'])) ? $getPayemtDetails[0]['group_master_name'] : '';
			$center_name = (!empty($getPayemtDetails[0]['center_name'])) ? $getPayemtDetails[0]['center_name'] : '';
			$receipt_print_name = (!empty($getPayemtDetails[0]['receipt_print_name'])) ? $getPayemtDetails[0]['receipt_print_name'] : '';
			$center_address = (!empty($getPayemtDetails[0]['center_address'])) ? $getPayemtDetails[0]['center_address'] : '';
			$center_contact_no = (!empty($getPayemtDetails[0]['center_contact_no'])) ? $getPayemtDetails[0]['center_contact_no'] : '';
			$center_email_id = (!empty($getPayemtDetails[0]['center_email_id'])) ? $getPayemtDetails[0]['center_email_id'] : '';
			$center_code = (!empty($getPayemtDetails[0]['center_code'])) ? $getPayemtDetails[0]['center_code'] : '';
			for($i=0;$i< $count;$i++){
				$fees_id[] = (!empty($getPayemtDetails[$i]['fees_id'])) ? $getPayemtDetails[$i]['fees_id'] : '';
			}
			$getRecieptNo = $this->admissionmodel->getdata_orderby_limit1("tbl_fees_payment_receipt", "fees_payment_receipt_id,receipt_no", "1=1", "order by fees_payment_receipt_id desc");	
			$reciept_id = 0;
			if(!empty($getRecieptNo)){
				$reciept_id =  substr($getRecieptNo[0]['receipt_no'], strrpos($getRecieptNo[0]['receipt_no'], '/') + 1) ;
			}
			$reciept_id = $reciept_id+1;
			if(!empty($center_code)){
				$reciept_no .= $center_code."/";
			}
			if(!empty($academic_year)){
				$reciept_no .= $academic_year."/";
			}
			
			$reciept_no .= $reciept_id;
			
			$uniquefee_id=array_unique($fees_id);
			$fee_ids =implode(',',$uniquefee_id);
			$date_v = date("d_m_Y_H_i_s",strtotime($reciept_date));
			$combine_receipt = '';
				$combine_receipt .= '<div style="width:750px;padding:0 30px;">
								<div style="margin:10px;text-align:center;margin:0 auto">
									<img src="https://app.montanapreschool.com/assets/images/aptech-montana-logo.jpg" alt="The Montana International Pre school Pvt. Ltd." title="The Montana International Pre school Pvt. Ltd." style="width:180px;"/>
								</div>
								<div>';
								//get center information
								$combine_receipt .= '<h4 style="text-align:center;color:#000;font-size:25px;margin:10px 0;padding:0;">'.$receipt_print_name.'</h4>
								<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><b>Centre Address:</b> '.$center_address.' Contact: '.$center_contact_no.', '.'<a href="mailto:'.$center_email_id.' " style="color:#000;text-decoration:none;">'.$center_email_id.'</a></p><p style="text-align:center;width:100%;margin-top:-10px;font-size:12px;">CIN No.: U80101MH2013PTC250962 | GST No.: 27AAECT8559Q1ZA</p>';
								
								$combine_receipt .= '</div>
								<div>
								<hr style="margin-top:-10px;">
								<h5 style="text-align: center;font-size:16px;margin:0px 0px 0px;padding:0;font-weight:300 !important;">Receipt ('.$academic_year.')</h5><br>
								<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
									<tr>
										<td>
											<table cellpadding="0" cellspacing="0" border="0" style="width:700px;">
												<tr>
													<th style="width:25%;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Date</th>
													<th style="width:25%;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">ENR Number</th>
													<th style="width:25%;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Name</th>
													<th style="width:25%;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000" colspan="2">Class</th>
												</tr>
												<tr>
													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.date("d-M-Y",strtotime($reciept_date)).'</strong></td>
													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getPayemtDetails[0]['enrollment_no'].'</strong></td>
													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$student_name.'</strong></td><td style="padding:10px;text-align:left;font-size:14px"><strong>'.$class_name.'</strong></td>
												</tr>
												<tr>
													<td colspan="3" style="padding:10px;text-align:left;font-size:14px">';
												if($getPayemtDetails[0]['guardian']=='father'){
													$combine_receipt.= '<p style="color:#000;font-size:14px;margin:3px 0;"><b>Father Name:</b> '.$getPayemtDetails[0]['father_name'].'</p>';
												}else if($getPayemtDetails[0]['guardian']=='mother'){
													$combine_receipt.= '<p style="color:#000;font-size:14px;margin:3px 0;"><b>Mother Name:</b> '.$getPayemtDetails[0]['mother_name'].'</p>';
												}else if($getPayemtDetails[0]['guardian']=='guadian'){
													$combine_receipt.= '<p style="color:#000;font-size:14px;margin:3px 0;"><b>Guadian Name: </b> '.$getPayemtDetails[0]['parent_name'].'</p>';
												
												}
												$combine_receipt.= '<p style="color:#000;font-size:14px;margin:3px 0;"><b>Address: </b>'.$getPayemtDetails[0]['present_address'].' ,'.$getPayemtDetails[0]['address1'].', '.$getPayemtDetails[0]['address2'].', '.$getPayemtDetails[0]['address3'].', '.$getPayemtDetails[0]['city_name'].' ,'.$getPayemtDetails[0]['state_name'].' ,'.$getPayemtDetails[0]['country_name'].', '.$getPayemtDetails[0]['pincode'].'</p>
													</td>						
															

													<td style="padding:10px;text-align:left;font-size:14px"> 
														<p style="color:#000;font-size:14px;margin:3px 0;"><b>Receipt No: </b>'.$reciept_no.'</p>
													</td>
												</tr>					
											</table>
										</td>
									</tr>
								</table>
								<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;" >
									<tr>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Component(s)</th>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Fee Type</th>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Class/Group</th>
										<th style="padding:10px;text-align:right;border-top:1px solid #000;border-bottom:1px solid #000">Amount</th>	
									</tr>';
									//update installment details collected amount and receipt no
									$getallFeesInstallmentUpdate_condition = " is_last_entry='Yes' && admission_fees_id IN(".$admintion_fee_id.") ";
									$getallFeesInstallmenForUpdate = $this->admissionmodel->getdata("tbl_fees_payment_details",$getallFeesInstallmentUpdate_condition);
									
									if(!empty($getallFeesInstallmenForUpdate)){
										foreach($getallFeesInstallmenForUpdate as $key=>$val){
											$installment_data = array();
											$installment_data['receipt_no'] = $reciept_no;
											$this->admissionmodel->updateRecord("tbl_fees_payment_details", $installment_data,"fees_payment_detail_id",$val['fees_payment_detail_id']);
										}
									}
									
									//get fees components
									$component_condition = "af.academic_year_id = '".$getPayemtDetails[0]['academic_year_id']."' && afc.fees_id IN(".$fee_ids.") && afcch.is_last_entry='Yes' && afcch.collected_amount > 0 && afc.admission_fees_id IN(".$admintion_fee_id.") ";
									$getallFeesComponents = $this->admissionmodel->getFeesComponentsforReceipt($component_condition);


									//update collected history by receipt number
									$getallFeesComponentsForUpdate_condition = "af.academic_year_id = '".$getPayemtDetails[0]['academic_year_id']."' &&  afc.fees_id IN(".$fee_ids.") && afcch.is_last_entry='Yes' && afc.admission_fees_id IN(".$admintion_fee_id.") ";
									$getallFeesComponentsForUpdate = $this->admissionmodel->getFeesComponentsforReceipt($getallFeesComponentsForUpdate_condition);
									// print_r($getallFeesComponentsForUpdate);exit;
									if(!empty($getallFeesComponentsForUpdate)){
										foreach($getallFeesComponentsForUpdate as $key=>$val){
											$receipt_collected_data = array();
											$receipt_collected_data['receipt_no'] = $reciept_no;
											$this->admissionmodel->updateRecord("tbl_admission_fees_components_collected_history", $receipt_collected_data,"component_collected_history_id",$val['component_collected_history_id']);
										}
									}

									if(!empty($getallFeesComponents)){
										/* -------------- combine fees for receipt start */
										$course_data = array();
										$group_data = array();
										//separate group and class fees
										foreach($getallFeesComponents as $key=>$val){
											if(!empty($val['course_name'])){
												$course_data[] = $val;
											}else{
												$group_data[]  = $val;
											}
										}
										//combine course fee component by receipt name
										$course_data_final_array = array();
										if(!empty($course_data)){
											$course_data = $this->array_formation($course_data,'receipt_name');
											$count = 0;
											foreach($course_data as $key=>$val){
												$totalVal = array_sum(array_column($val,"collected_amount"));
												$course_data_final_array[$count]['component_collected_history_id'] = $val[0]['component_collected_history_id'];
												$course_data_final_array[$count]['receipt_name'] = $val[0]['receipt_name'];
												$course_data_final_array[$count]['fees_type'] = $val[0]['fees_type'];
												$course_data_final_array[$count]['collected_amount'] = $totalVal;
												$course_data_final_array[$count]['course_name'] = $val[0]['course_name'];
												$course_data_final_array[$count]['group_master_name'] = $val[0]['group_master_name'];
												$count++;
											}
										}
									


										//combine group fee component by receipt name
										$group_data_final_array = array();
										if(!empty($group_data)){
											// multiple group formation
											$multiple_group_data =  $this->array_formation($group_data,'group_master_name');
											if(!empty($multiple_group_data)){
												$count = 0;
												foreach($multiple_group_data as $mgkey=>$mgval){
													$mgval = $this->array_formation($mgval,'receipt_name');
													foreach($mgval as $key=>$val){
														$totalVal = array_sum(array_column($val,"collected_amount"));
														$group_data_final_array[$count]['component_collected_history_id'] = $val[0]['component_collected_history_id'];
														$group_data_final_array[$count]['receipt_name'] = $val[0]['receipt_name'];
														$group_data_final_array[$count]['fees_type'] = $val[0]['fees_type'];
														$group_data_final_array[$count]['collected_amount'] = $totalVal;
														$group_data_final_array[$count]['course_name'] = $val[0]['course_name'];
														$group_data_final_array[$count]['group_master_name'] = $val[0]['group_master_name'];
														$count++;
													}
												}
											}
										}
										// echo "<pre>";print_r($group_data_final_array);exit;
										$getallFeesComponents = array_merge($course_data_final_array,$group_data_final_array);
										/* -------------- combine fees for receipt end */
										$sumofcomponent = 0;
										for($i=0; $i < sizeof($getallFeesComponents); $i++){
											$sumofcomponent += $getallFeesComponents[$i]['collected_amount'];
											$combine_receipt .= '<tr>						
												<td style="padding:10px;text-align:left;font-size:14px">'.$getallFeesComponents[$i]['receipt_name'].'</td>
												<td style="padding:10px;text-align:left;font-size:14px">'.$getallFeesComponents[$i]['fees_type'].' Fee</td>
												<td style="padding:10px;text-align:left;font-size:14px">'.(!empty($getallFeesComponents[$i]['group_master_name'])?$getallFeesComponents[$i]['group_master_name']:$getallFeesComponents[$i]['course_name']).'</td>
												<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getallFeesComponents[$i]['collected_amount'],2).'</td>
																	
											</tr>';
										}
									}
								
									$combine_receipt .= '<tr>
										<td style="padding:10px;text-align:left;font-size:14px" colspan="3"><strong>Total Amount</strong></td>
										
										<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofcomponent,2).'</strong></td>						
									</tr>';

									$componentrupees = (float)$sumofcomponent;
									$receipt_amount = number_format((float)$sumofcomponent, 2, '.', '');
									$receipt_payment_mode = $getPayemtDetails[0]['payment_mode'];
								
									$combine_receipt .='<tr>
									<td style="padding:10px;text-align:left;font-size:14px" colspan="1"><b>Total Amount (In Words): </b></td>
									<td style="padding:10px;text-align:left;font-size:14px" colspan="3">'.strtoupper($this->getIndianCurrency($componentrupees)).'</td>
									</tr>';
									

									$combine_receipt .= '</table>
								
								<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
													<tr>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Payment Mode:</strong> '.$getPayemtDetails[0]['payment_mode'].'</td>
														<td style="padding:10px;text-align:left;font-size:14px">';
														if($getPayemtDetails[0]['payment_mode'] != 'Cash' ){
															$combine_receipt .= '<strong>Bank Name: </strong>'.$getPayemtDetails[0]['bank_name'];
														}
												$combine_receipt .= '</td>
													</tr>';
												if($getPayemtDetails[0]['payment_mode'] == 'Cheque'){	
													$combine_receipt .= '<tr>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque No: </strong>'.$getPayemtDetails[0]['cheque_no'].'</td>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque Date: </strong>'.date("d-M-Y", strtotime($getPayemtDetails[0]['transaction_date'])).'</td>
													</tr>';
												}else if($getPayemtDetails[0]['payment_mode'] == 'Netbanking'){	
													$combine_receipt .= '<tr>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Transaction No: </strong>'.$getPayemtDetails[0]['transaction_id'].'</td>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Transaction Date: </strong>'.date("d-M-Y", strtotime($getPayemtDetails[0]['transaction_date'])).'</td>
													</tr>';
												}else{
													$combine_receipt .= '<tr>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>&nbsp;</strong></td>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>&nbsp;</strong></td>
													</tr>';
												}
												$combine_receipt .= '</table>';
									
								$combine_receipt .='<div><p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"> <b>Notes:</b> 1. Cheques Subject to REALISATION. 2. Fees once paid are Non-Refundable & Non-Transferable. 3. This Receipt must be produced when demanded. 4. This is electronically generated receipt, hence does not require signature. </p>
								</div><br><br>';
								if($getPayemtDetails[0]['payment_mode']=='Cheque' && (strtotime($getPayemtDetails[0]['transaction_date']) > strtotime(date("Y-m-d",strtotime($reciept_date))))){
									$combine_receipt .='<div><p style="text-align:center;color:#000;font-size:14px;"><b>“This receipt is issued against a Post Dated Cheque (PDC)” </b></p></div><br><br>';
								}	
			ini_set('memory_limit','100M');
			$date_v = date("d.m.y",strtotime($reciept_date));
			$receipt_file_name = "PaymentReceipt_".$reciept_id."_".$date_v.".pdf";
			$receipt_pdfFilePath = DOC_ROOT_FRONT_API."/images/payment_invoices/".$receipt_file_name;
			$file_path = FRONT_API_URL."/images/payment_invoices/".$receipt_file_name;	
			$return_receipt = $file_path;
			$this->load->library('M_pdf');
			$pdf = $this->m_pdf->load();
			$pdf = new mPDF('utf-8');
			$pdf->WriteHTML($combine_receipt); // write the HTML into the PDF
			$pdf->Output($receipt_pdfFilePath, 'F'); // save to file because we can	
		}
		/* ----------------- single recept end ---------------- */
		for ($s=0; $s <sizeof($studentinfo) ; $s++) { 
			$getbcNo = $this->admissionmodel->getdata_orderby_limit1("tbl_fees_payment_receipt", "fees_payment_receipt_id", "1=1", "order by fees_payment_receipt_id desc");
				
			$bc_id = (!empty($getbcNo)) ? ($getbcNo[0]['fees_payment_receipt_id'] + 1) : 1;
			
			$bc_no = "";
			if(!empty($center_code) || !empty($academic_year)){
				$bc_no .= "BC".$bc_id."/".$academic_year."/".$center_code;
			}

			$getreceiptinfo = $this->admissionmodel->getdata1("tbl_fees_payment_receipt", "fees_payment_receipt_id", "admission_fees_id='".$studentinfo[$s]['admission_fees_id']."'");

			$getPayemtDetails = $this->admissionmodel->getPayemtDetails("i.admission_fees_id='".$studentinfo[$s]['admission_fees_id']."' ");
			//print_r($getPayemtDetails);exit;
			$student_name = (!empty($getPayemtDetails[0]['student_first_name'])) ? $getPayemtDetails[0]['student_first_name']." ".$getPayemtDetails[0]['student_last_name'] : '';
			$academic_year = (!empty($getPayemtDetails[0]['academic_year_master_name'])) ? $getPayemtDetails[0]['academic_year_master_name'] : '';
			$category_name = (!empty($getPayemtDetails[0]['categoy_name'])) ? $getPayemtDetails[0]['categoy_name'] : '';
			$course_name = (!empty($getPayemtDetails[0]['course_name'])) ? $getPayemtDetails[0]['course_name'] : '';
			$batch_name = (!empty($getPayemtDetails[0]['batch_name'])) ? $getPayemtDetails[0]['batch_name'] : '';
			$group_name = (!empty($getPayemtDetails[0]['group_master_name'])) ? $getPayemtDetails[0]['group_master_name'] : '';
			$center_name = (!empty($getPayemtDetails[0]['center_name'])) ? $getPayemtDetails[0]['center_name'] : '';
			$receipt_print_name = (!empty($getPayemtDetails[0]['receipt_print_name'])) ? $getPayemtDetails[0]['receipt_print_name'] : '';
			$center_address = (!empty($getPayemtDetails[0]['center_address'])) ? $getPayemtDetails[0]['center_address'] : '';
			$center_contact_no = (!empty($getPayemtDetails[0]['center_contact_no'])) ? $getPayemtDetails[0]['center_contact_no'] : '';
			$center_email_id = (!empty($getPayemtDetails[0]['center_email_id'])) ? $getPayemtDetails[0]['center_email_id'] : '';
			$center_code = (!empty($getPayemtDetails[0]['center_code'])) ? $getPayemtDetails[0]['center_code'] : '';
			$intallment_yes_no = (!empty($getPayemtDetails[0]['is_instalment'])) ? $getPayemtDetails[0]['is_instalment'] : '';
			$installment_nos = (!empty($getPayemtDetails[0]['no_of_installments'])) ? $getPayemtDetails[0]['no_of_installments'] : '';
			$discount_amount = (!empty($getPayemtDetails[0]['discount_amount'])) ? $getPayemtDetails[0]['discount_amount'] : '';
			$gst_amount = (!empty($getPayemtDetails[0]['gst_amount'])) ? $getPayemtDetails[0]['gst_amount'] : '';
			$total_fees = (!empty($getPayemtDetails[0]['fees_total_amount'])) ? $getPayemtDetails[0]['fees_total_amount'] : '';
			$total_amount = (!empty($getPayemtDetails[0]['total_amount'])) ? $getPayemtDetails[0]['total_amount'] : '';
			$collected_fees = (!empty($getPayemtDetails[0]['fees_amount_collected'])) ? $getPayemtDetails[0]['fees_amount_collected'] : '';
			$remaining_fees = (!empty($getPayemtDetails[0]['fees_remaining_amount'])) ? $getPayemtDetails[0]['fees_remaining_amount'] : '';
			
			$_POST['isadmission'] = 'No';
			
			//create invoice
			ini_set('memory_limit','100M');
			$date_v = date("d.m.y",strtotime($reciept_date));
			$invoice_file_name = "BookingConfirmation_BC".$bc_id."_".$date_v.".pdf";
			$invoice_pdfFilePath = DOC_ROOT_FRONT_API."/images/payment_invoices/".$invoice_file_name;
			$file_path = FRONT_API_URL."/images/payment_invoices/".$invoice_file_name;
			// invoice format
			$invoice = '';
			$invoice  = '<html><body>';	
			$invoice .= '<div style="width:750px;padding:0 30px;">
								<div style="margin:10px;text-align:center;margin:0 auto">
								<img src="https://app.montanapreschool.com/assets/images/aptech-montana-logo.jpg" alt="The Montana International Pre school Pvt. Ltd." title="The Montana International Pre school Pvt. Ltd." style="width:180px;"/>
								</div>';
								$invoice .= '<div>
									<h4 style="text-align:center;color:#000;font-size:25px;margin:10px 0;padding:0;">'.$receipt_print_name.'</h4>
									<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><b>Centre Address:</b> '.$center_address.'</p>
									<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><a href="mailto:'.$center_email_id.' " style="color:#000;text-decoration:none;">'.$center_email_id.'</a></p><p style="text-align:center;width:100%;margin-top:-10px;font-size:12px;">CIN No.: U80101MH2013PTC250962 | GST No.: 27AAECT8559Q1ZA</p>
								</div>';
								
								$invoice .= '<hr style="1px dashed black">&nbsp;
								<h4 style="text-align: center;font-size:20px;margin:10px 0;padding:0">Booking Confirmation ('.$academic_year.')</h4>
								<table cellpadding="0" cellspacing="0" border="0" style="border:0px none;width:700px;">
									<tr>
										<td style="width:350px;padding:10px;text-align:left;">
										<p style="color:#000;font-size:14px;margin:7px 0;"><strong>BC Number: </strong>'.$bc_no.'</p>
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Child’s Name:</strong>'.$getPayemtDetails[0]['student_first_name'].' '.$getPayemtDetails[0]['student_last_name'].'</p>'; 

											if($getPayemtDetails[0]['guardian']=='father'){
												$invoice .=	'<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Father Name:</strong> '.$getPayemtDetails[0]['father_name'].'</p>';
											}else if($getPayemtDetails[0]['guardian']=='mother'){
												$invoice .=	'<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Mother Name: </strong>'.$getPayemtDetails[0]['mother_name'].'</p>';
											}else if($getPayemtDetails[0]['guardian']=='guadian'){
												$invoice .=	'<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Guadian Name:</strong> '.$getPayemtDetails[0]['parent_name'].'</p>';
											}
											$invoice .=	'<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Address: </strong>'.$getPayemtDetails[0]['present_address'].' ,'.$getPayemtDetails[0]['address1'].', '.$getPayemtDetails[0]['address2'].', '.$getPayemtDetails[0]['address3'].', '.$getPayemtDetails[0]['city_name'].' ,'.$getPayemtDetails[0]['state_name'].' ,'.$getPayemtDetails[0]['country_name'].', '.$getPayemtDetails[0]['pincode'].'</p>';
											if($getPayemtDetails[0]['course_name'] != ""){
												$invoice .= '<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Class/Group:</strong>'.$getPayemtDetails[0]['course_name'].'</p>';
											}else{
												$invoice .= '<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Class/Group:</strong>'.$getPayemtDetails[0]['group_master_name'].'</p>';
											}
									$invoice .= '</td>
										<td style="width:350px;padding:10px;text-align:left;" valign="top">
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>ENR Number:</strong>'.$getPayemtDetails[0]['enrollment_no'].'</p>
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Phone Number:</strong>'.$getPayemtDetails[0]['father_mobile_contact_no'].'</p> 
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Email ID:</strong>'.$getPayemtDetails[0]['father_email_id'].'</p> 
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Receipt Number: </strong>'.$reciept_no.'</p>
											
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Receipt Date: </strong>'.date("d-M-Y",strtotime($reciept_date)).'</p>
											
										</td>
										
									</tr>
								</table><br>';
								//fee component
								$invoice .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Details of Booking Confirmation.</h4>';
								$invoice .='<table width="100%" cellpadding="0" cellspacing="0" border="1" style="border:1px solid #000;width:700px;margin:0 auto;border-collapse: collapse;">
									<tr>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Fee Component </th>
										<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Gross </th>
										<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Discount</th>
										<th style="padding:10px;text-align:right;border-top:1px solid #000;border-bottom:1px solid #000">Total</th>	
									</tr>';
									$getFeesComponentsinvoice = $this->admissionmodel->getFeesComponentsforinvoice($getPayemtDetails[0]['admission_fees_id']);
									if(!empty($getFeesComponentsinvoice)){
										$sumofgross =0;
										$sumofdiscount =0;
										$sumofcomponent =0;
										for($i=0; $i < sizeof($getFeesComponentsinvoice); $i++){
											$sumofgross += $getFeesComponentsinvoice[$i]['fees_component_id_value'];
											$sumofdiscount += $getFeesComponentsinvoice[$i]['discount_amount'];
											$sumofcomponent += $getFeesComponentsinvoice[$i]['sub_total'];
											$invoice .= '<tr>						
												<td style="padding:10px;text-align:left;font-size:14px">'.$getFeesComponentsinvoice[$i]['fees_component_master_name'].'</td>
												<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getFeesComponentsinvoice[$i]['fees_component_id_value'],2).'</td>	
												<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getFeesComponentsinvoice[$i]['discount_amount'],2).'</td>
												<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getFeesComponentsinvoice[$i]['sub_total'],2).'</td>					
											</tr>';
										}
										$invoice .='<tr>
										<td style="padding:10px;text-align:left;font-size:14px"><strong>Total</strong></td>
										<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofgross,2).'</strong></td>
										<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofdiscount,2).'</strong></td>
										<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofcomponent,2).'</strong></td>
										</tr>';
									}
									
									$invoice .='</table><br>';
								//installment
								$invoice .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Details of Planned Installment Payment.</h4>';
								$invoice .='<table width="100%" cellpadding="0" cellspacing="0" border="1" style="border:1px solid #000;width:700px;margin:0 auto;border-collapse: collapse;">
									<tr>
										<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Installment Number</th>
										<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Planned Installment Date</th>
										
										<th style="padding:10px;text-align:right;border-top:1px solid #000;border-bottom:1px solid #000">Planned Total Installment Amount</th>	
									</tr>';
									$getinstallmentinvoice = $this->admissionmodel->getinstallmentforinvoice($getPayemtDetails[0]['admission_fees_id']);
									//print_r();exit;
									$sumofinstallment=0;
									$sumofinstallmentdue=0;
									if(!empty($getinstallmentinvoice)){
										$cnt = 0;
										for($i=0; $i < sizeof($getinstallmentinvoice); $i++){
											$sumofinstallment += $getinstallmentinvoice[$i]['instalment_amount'];
											$sumofinstallmentdue += $getinstallmentinvoice[$i]['instalment_remaining_amount'];
											$invoice .= '<tr>						
												<td style="padding:10px;text-align:right;font-size:14px">'.++$cnt.'</td>
												<td style="padding:10px;text-align:right;font-size:14px">'.date("d-M-Y", strtotime($getinstallmentinvoice[$i]['instalment_due_date'])).'</td>	
											
												<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getinstallmentinvoice[$i]['instalment_amount'],2).'</td>					
											</tr>';
										}
										$invoice .='<tr>
										<td colspan="2" style="padding:10px;text-align:left;font-size:14px"><strong>Total</strong></td>
										<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofinstallment,2).'</strong></td>
										
										</tr>';
									}
									$invoice .='</table><br>';
									$installmentrupees = (float)$sumofinstallment;
								
								$invoice .='<p style="font-size:13px;margin:4px 0;"><b>Total Amount (In Words): </b>'.strtoupper($this->getIndianCurrency($installmentrupees)).'
								</p><br>';
								//for Receipt Number
								$invoice .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Your Payment Details as on Date.</h4>';
								$invoice .='<table width="100%" cellpadding="0" cellspacing="0" border="1" style="border:1px solid #000;width:700px;margin:0 auto;border-collapse: collapse;">
									<tr>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Receipt Number</th>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Receipt Date</th>
										<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Receipt Amount</th>
											
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">'.$reciept_no.'</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">'.date("d-M-Y",strtotime($reciept_date)).'</td>
										<td style="padding:10px;text-align:right;font-size:14px;border:1px solid #000;">'.number_format($collected_fees,2).'</td>
																
									</tr>
									<tr>
									<td colspan="2" style="padding:10px;text-align:left;font-size:14px"><strong>Total</strong></td>
										<td style="padding:10px;text-align:right;font-size:14px;border:1px solid #000;"><strong>'.number_format($collected_fees,2).'</strong></td>
															
									</tr>
								</table><br>';
								$rupees = (float)$collected_fees;
								
								$invoice .='<p style="font-size:13px;margin:4px 0;"><b>Total Amount (In Words): </b>'.strtoupper($this->getIndianCurrency($rupees)).'
								</p><br>';

								$invoice .= '<p style="font-size:13px;margin:4px 0;">Notes:</p>
								<p style="font-size:13px;margin:4px 0;">1)  Cheques Subject to REALISATION.</p>
								<p style="font-size:13px;margin:4px 0;">2) Fees once paid are Non-Refundable & Non-Transferable. </p>
								<p style="font-size:13px;margin:4px 0;">3) This Receipt must be produced when demanded.</p>
								<p style="font-size:13px;margin:4px 0;">4) This Booking Confirmation provides provisional admission to the child.</p>
								<p style="font-size:13px;margin:4px 0;">5) Ensure timely payments as mentioned in the Booking confirmation.</p>
								<p style="font-size:13px;margin:4px 0;">6) Parents are required to inform the school if there is any change in their address and telephone numbers.</p>
								<p style="font-size:13px;margin:4px 0;">7) The parents are responsible for timely drop and pick up of child.</p>
								<p style="font-size:13px;margin:4px 0;">8) Please refer detailed code of conduct and policy Guidelines sent on your mail.</p>

								
									<p style="font-size:13px;margin:13px 0;">For any feedback or suggestions please write to us at <a href="mailto:feedback@montanapreschool.com" style="color:#000;text-decoration:none;"><u>feedback@montanapreschool.com</u></a></p><br>
									<p style="font-size:13px;margin:13px 0;">Registered Office : 402, Sagar Tech Plaza, B wing, Andheri-Kurla Road, Sakinaka,
									Andheri East, Mumbai - 400 072 Maharashtra, India, </p>
									<p style="font-size:13px;margin:13px 0;"><a href="info@montanapreschool.com/" target="_blank" style="color:#000;text-decoration:none;">info@montanapreschool.com/</a></p>
									<p style="font-size:13px;margin:13px 0;"><a href="https://www.facebook.com/montanapreschool/" target="_blank" style="color:#000;text-decoration:none;">https://www.facebook.com/montanapreschool/</a><br>

									

									<p style="font-size:13px;margin:4px 0;">Disclaimer: Taxes will be charged extra, as applicable, on the date of payment.</p>
									</p>
								</div><br>';

			
			$invoice .=  '</body></html>';

			//mailbody start
			$message_body ='';
			$message_body  = '<html><body>';	
			$message_body .= '<div style="width:750px;padding:0 30px;">';
			$message_body .= '<div style="width:100%;float:left;">
				
								<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Dear '.$getPayemtDetails[0]['father_name'].',</b></p><br>

								<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;">Welcome to Aptech Montana International Preschool! Your child’s journey of learning and fun begins with us and as your child embarks on this journey, here are few essential guidelines that will help you.</p><br>

								<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;">Let’s begin!</p>
							
								<br>';
								$message_body .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Admission & Payment Information</h4>';
								$message_body .= '<div style="width:100%;float:left;">
				
								<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>ENR Number: </b>'.$getPayemtDetails[0]['enrollment_no'].'</p>
								<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Child’s Name: </b>'.$getPayemtDetails[0]['student_first_name'].' '.$getPayemtDetails[0]['student_last_name'].'</p>
								<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Course Name: </b>'.$getPayemtDetails[0]['course_name'].'</p>';

								if($getPayemtDetails[0]['guardian']=='father'){
									$message_body .='<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><strong>Father Name: </strong> '.$getPayemtDetails[0]['father_name'].'</p>';
								}else if($getPayemtDetails[0]['guardian']=='mother'){
									$message_body .='<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><strong>Mother Name: </strong>'.$getPayemtDetails[0]['mother_name'].'</p>';
								}else if($getPayemtDetails[0]['guardian']=='guadian'){
									$message_body .='<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><strong>Guadian Name: </strong> '.$getPayemtDetails[0]['parent_name'].'</p>';
								}
								$message_body .='<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><strong>E-Mail ID: </strong> '.$getPayemtDetails[0]['father_email_id'].'</p>
								<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Mobile Number:</b>'.$getPayemtDetails[0]['father_mobile_contact_no'].'</p>
								<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Centre Name: </b>'.$center_name.'</p>
								<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Course Fee (Rs.): </b>'.$total_amount.'</p>
								<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b><p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Total Amount Received till now towards Course (Rs.): </b>'.$collected_fees.'</p>';

							
								$message_body .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Important Communication:</h4>';

								$message_body .= '
								<p style="font-size:13px;margin:4px 0;">1) A copy of the Booking Confirmation is attached with this mail. It contains the details of the program in which your child is enrolled, program fees, payment details and instalment payment schedule. You are requested to please check the details mentioned in the Booking Confirmation carefully.</p>
								<p style="font-size:13px;margin:4px 0;">2)We request parents to pay the school fees either by cheque, DD or by net banking. Payment to be done in favour of “The Montana International Preschool Pvt Ltd ”.</p>
								<p style="font-size:13px;margin:4px 0;">3)  Parents can also pay the fees using payment gateway attached to the mobile app.</p>
								<p style="font-size:13px;margin:4px 0;">4)Please insist on computerized receipt on payment of fees. Aptech Montana International Preschool is not responsible/ nor recognizes any manual receipt issued by the school.</p>
								<p style="font-size:13px;margin:4px 0;">5) On completion of the Academic Year , program completion certificate will be issued by Aptech Montana International Preschool. Any other certificate issued is invalid.</p>
								<p style="font-size:13px;margin:4px 0;"> 6)In case of any discrepancies, reach out to our centre or our customer care <a href="mailto: feedback@montanapreschool.com"><u>feedback@montanapreschool.com</u></a>with your centre name, enrollment number, name and contact details.</p>';

								$message_body .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">“Montana Parent Connect” a unique Mobile App</h4><br>
								<p style="font-size:13px;margin:4px 0;">You are requested to kindly download the ‘Montana Parent Connect App’ from the <a href="https://play.google.com/store/apps/details?id=com.eduquotient.montana.parentconnect&hl=en">Google Play Store </a>or <a href="https://apps.apple.com/in/app/montana-parent-connect/id1470218146">App Store </a></p><br>
								
								<p style="font-size:13px;margin:4px 0;">The Montana Parent Connect App comes with some amazing features like LIVE CCTV feed, child attendance history, updates on school activities through notifications, picture gallery to view photos and videos of events, newsletters etc.</p><br>
								
								<p style="font-size:13px;margin:4px 0;"><b>Username:</b>'.$getPayemtDetails[0]['enrollment_no'].'</p>
								<p style="font-size:13px;margin:4px 0;"><b>Default Password:</b>password</p><br>';

								$message_body .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Student Kit:</h4><br>

								<p style="font-size:13px;margin:4px 0;">On securing admission of your at Aptech Montana International Preschool, you will receive a child kit as per the program enrolled, which contains the following items.</p><br>';

								$message_body .='<table width="50%" cellpadding="0" cellspacing="0" border="2" style="border:1px solid #000;width:700px;margin:0 auto;">';
								$message_body .='<thead>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Sr. No.</th>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">List of items</th>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Quantity</th>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Applicable for Program (s)</th>
										
									</thead>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	T shirt (Common)</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">		
										Short/Skirt</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, K1 & K2</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	3</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Cap</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, Child Care, K1 & K2</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	4</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Apron</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, Child Care, K1 & K2</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	5</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Shoe</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1 pair</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	6</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Bag</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, K1 & K2</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	7</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Socks</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2 pair</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">		
										Pre-Nursery, Nursery, K1 & K2</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	8</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	School Diary</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	9</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Assessment Card</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	10</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Bag ID Card</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	11</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Family ID Card</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, Child Care, K1 & K2</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	12</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Student ID Card</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, Child Care, K1 & K2</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	13</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">		
										ID holder with Lanyard</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, Child Care, K1 & K2</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	14</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	PN/NU Workbooks</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">1 set</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery & Nursery</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	15</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	K1/K2 book kit</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">		
										1 set</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">K1 & K2</td>
									</tr>
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	16</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Child Care Diary</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
										<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Child Care</td>
									</tr>';
									$message_body .='</table>';

									$message_body .='<p style="font-size:13px;margin:4px 0;">Kindly acknowledge the receipt of the parent manual and provide your confirmation to adhere to it. Sign the Booking Confirmation document provided to you by your school. In case you haven’t received it, do request your centre to provide it to you at the earliest.</p><br>

								<p style="font-size:13px;margin:4px 0;">Attached herewith is the copy of the Parent Manual for your ready reference.</p><br>

								<p style="font-size:13px;margin:4px 0;"><b>Regards</b></p>
								<p style="font-size:13px;margin:4px 0;"><b>Aptech Montana International Preschool</b></p>';

								$message_body .=  '</div>';
								
								$message_body .=  '</body></html>';

			$this->load->library('M_pdf');
			$pdf = $this->m_pdf->load();
			$pdf = new mPDF('utf-8');
			$pdf->WriteHTML($invoice); // write the HTML into the PDF
			$pdf->Output($invoice_pdfFilePath, 'F'); // save to file because we can	
			
			//genrate invoice pdf
		
			$mail_body ='<div>
			<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;">Dear parent,</p><br>
			<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"> Find attached your BC & payment receipt.</p><br><br><br>
			<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"> Regards,</p><br>
			<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"> Team Aptech Montana
			</p><br>
			</div>';
			
			//insert invoice file
			$invoice_file = array();
			$invoice_file['admission_fees_id'] = $studentinfo[$s]['admission_fees_id'];
			$invoice_file['receipt_no'] = $reciept_no;
			$invoice_file['bc_no'] = $bc_no;
			$invoice_file['receipt_file'] = $receipt_file_name;
			$invoice_file['receipt_file_withoutgst'] = $invoice_file_name;
			$invoice_file['receipt_amount'] = $receipt_amount;
			$invoice_file['payment_mode'] = $receipt_payment_mode;
			
			$invoice_file['created_on'] = date("Y-m-d H:m:i",strtotime($reciept_date));
			$invoice_file['created_by'] = $_SESSION["webadmin"][0]->user_id;
			$invoice_file['updated_on'] = date("Y-m-d H:m:i",strtotime($reciept_date));
			$invoice_file['updated_by'] = $_SESSION["webadmin"][0]->user_id;

			$result_insert = $this->admissionmodel->insertData('tbl_fees_payment_receipt', $invoice_file, 1);
			if($result_insert){
				if(!empty($getPayemtDetails[0]['father_email_id'])){
					$get_receipt = $file_path = FRONT_API_URL."/images/payment_invoices/".$receipt_file_name;
					$get_invoice = $file_path = FRONT_API_URL."/images/payment_invoices/".$invoice_file_name;
					$get_manual = $file_path = FRONT_API_URL."/images/payment_invoices/ParentManual.pdf";
					$this->email->clear();
					$this->email->from("info@attoinfotech.in"); // change it to yours
					$subject = "";
					//uncommand
					$this->email->to($getPayemtDetails[0]['father_email_id']);
					//  $this->email->to("arul.selvam@attoinfotech.com");
					
					if($getPayemtDetails[$s]['fees_type']=="Class"){
						$subject = "Welcome to Aptech Montana International Preschool";
						$this->email->subject($subject);
						$this->email->message($message_body);
						$this->email->attach($get_receipt);
						$this->email->attach($get_invoice);
						$this->email->attach($get_manual);
					}else{
						$subject = " Your BC & Payment Receipt $reciept_no ";
						$this->email->subject($subject);
						$this->email->message($mail_body);
						$this->email->attach($get_receipt);
						$this->email->attach($get_invoice);
					};
					// $checkemail = $this->email->send();
					$this->email->clear(TRUE);
				}
			}
		}

		echo json_encode(array("status_code" => "200", "msg" => "Information saved successfully.", "body" => $return_receipt));
		exit;
	}  
	
	/* -----------------------------receipt funcitons -------------------------------- */
	function fetch(){
		$get_result = $this->remainingfeesmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				if(!empty($get_result['query_result'][$i])){
					array_push($temp, date("d-M-Y",strtotime($get_result['query_result'][$i]->created_on)));
					array_push($temp, $get_result['query_result'][$i]->center_name);
					array_push($temp, $get_result['query_result'][$i]->receipt_no);
					array_push($temp, $get_result['query_result'][$i]->enrollment_no);
					array_push($temp, $get_result['query_result'][$i]->student_name);
					array_push($temp, $get_result['query_result'][$i]->receipt_amount);
					// array_push($temp, $get_result['query_result'][$i]->payment_mode);
					$all_value= array($get_result['query_result'][$i]->bank_name,$get_result['query_result'][$i]->payment_mode,$get_result['query_result'][$i]->transaction_id);
					// $details_bank ='Bank Name:'.$all_value[0];
					// $details_payment ='Payment Type:'.$all_value[1];
					// $details_transaction ='Transaction Id:'.$all_value[2];
					
					// $details ='<table cellspacing="0" cellpadding="0">';
					// 				if(!empty($get_result['query_result'][$i]->bank_name)){
					// 					$details .='<tr><td ><strong>Bank Name: </strong></td><td>'.$all_value[0].'</td>
					// 					</tr>';
					// 				}
					// 				if(!empty($get_result['query_result'][$i]->payment_mode)){
					// 					$details .='<tr><td ><strong>Payment Type: </strong></td><td>'.$all_value[1].'</td>	</tr>';
					// 				}
					// 				if(!empty($get_result['query_result'][$i]->transaction_id)){	
					// 					$details .='<tr><td ><strong>Transaction Id: </strong></td><td>'.$all_value[2].'</td></tr>';
					// 				}
					// 				$details .= '</table>';
					$details = "";
					if(!empty($get_result['query_result'][$i]->bank_name)){
						$details .='<p  style="font-size:12px"><strong>Bank Name: </strong>'.$all_value[0].'</p>';
					}
					if(!empty($get_result['query_result'][$i]->payment_mode)){
						$details .='<p style="font-size:12px"><strong>Payment Type: </strong>'.$all_value[1].'</p>';
					}
					if(!empty($get_result['query_result'][$i]->transaction_id)){	
						$details .='<p style="font-size:12px"><strong>Transaction Id: </strong>'.$all_value[2].'</p>';
					}
								

					
					//$details = array($details_bank. '<br>'.$details_payment.'<br>'.$details_transaction);
					array_push($temp, $details);
					if($get_result['query_result'][$i]->status  == "Cancel"){
						$get_result['query_result'][$i]->status = "Cancelled";
					}
					if($get_result['query_result'][$i]->guardian  == "father"){
						$parent = $get_result['query_result'][$i]->father_name;
					}
					if($get_result['query_result'][$i]->guardian  == "mother"){
						$parent = $get_result['query_result'][$i]->mother_name;
					}
					if($get_result['query_result'][$i]->guardian  == "guardian"){
						$parent = $get_result['query_result'][$i]->parent_name;
					}

					array_push($temp, $get_result['query_result'][$i]->status);
					if($get_result['query_result'][$i]->cancelled_date != "0000-00-00 00:00:00"){
						array_push($temp, date("d-M-Y",strtotime($get_result['query_result'][$i]->cancelled_date)));
					}else if($get_result['query_result'][$i]->cleared_date != "0000-00-00 00:00:00"){
						array_push($temp, date("d-M-Y",strtotime($get_result['query_result'][$i]->cleared_date)));
					}else{
						array_push($temp, "--");
					}

					if($get_result['query_result'][$i]->cancelled_date != "0000-00-00 00:00:00"){
						$cancel_date = date("d-M-Y",strtotime($get_result['query_result'][$i]->cancelled_date));
					}else if($get_result['query_result'][$i]->cleared_date != "0000-00-00 00:00:00"){
						$cancel_date = date("d-M-Y",strtotime($get_result['query_result'][$i]->cleared_date));
					}else{
						$cancel_date = "--";
					}
					$change_html = "<button type='button' class='btn btn-success btn-sm view-receipt' receiptUrl = '".FRONT_API_URL."/images/payment_invoices/".$get_result['query_result'][$i]->receipt_file."' reciept_no ='".$get_result['query_result'][$i]->receipt_no."' recipientname='".$parent."' studentname ='".$get_result['query_result'][$i]->student_name."' receiptstatus='".$get_result['query_result'][$i]->status."' receiptdate='".date("d-M-Y",strtotime($get_result['query_result'][$i]->created_on))."' issuedate ='".$cancel_date."' ><i class='fa fa-eye ' ></i></button>&nbsp;";
					$change_html .= "<button type='button' class='btn btn-success btn-sm' onclick='changeStatusModel(".$get_result['query_result'][$i]->fees_payment_receipt_id.")' ><i class='fa fa-exchange  ' ></i></button>";
					array_push($temp, $change_html);
					array_push($items, $temp);
				}
			}
		}
		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}

	function array_formation($array,$label){
		$return_array = array();
		$tempkey = "";
		foreach($array as $key=>$val){
			$return_array[$val[$label]][] = $val;
		}
		return $return_array;
	}

	function getIndianCurrency($number){
		$decimal = round($number - ($no = floor($number)), 2) * 100;
		$hundred = null;
		$digits_length = strlen($no);
		$i = 0;
		$str = array();
		$words = array(0 => '', 1 => 'one', 2 => 'two',
			3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
			7 => 'seven', 8 => 'eight', 9 => 'nine',
			10 => 'ten', 11 => 'eleven', 12 => 'twelve',
			13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
			16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
			19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
			40 => 'forty', 50 => 'fifty', 60 => 'sixty',
			70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
		$digits = array('', 'hundred','thousand','lakh', 'crore');
		while( $i < $digits_length ) {
			$divider = ($i == 2) ? 10 : 100;
			$number = floor($no % $divider);
			$no = floor($no / $divider);
			$i += $divider == 10 ? 1 : 2;
			if ($number) {
				$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
				$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
				if($digits[$counter] == "thousand"){
					$plural = "";
				}
				$str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
			} else $str[] = null;
		}
		$Rupees = implode('', array_reverse($str));
		$paise = ($decimal > 0) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
		return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise; 
	}

	/* -----------------------------receipt funcitons -------------------------------- */
	function array_has_dupes($array) {
		return count($array) !== count(array_unique($array));
	}

	// assign fees
	function feeAssingFeesAddedit($id = NULL){
		if(!empty($_SESSION["webadmin"])) {
			$record_id  = $_POST['student_id'] ;
			$result = [];
			$result1 = [];
			$inquiryNo = [];
			$result['inquiries'] = $this->admissionmodel->getDropdown("tbl_inquiry_master","inquiry_master_id,lead_id,student_first_name,student_last_name");
			$result['academicyear'] = $this->admissionmodel->getDropdown1("tbl_academic_year_master","academic_year_master_id,academic_year_master_name");
			for($i=0;$i<sizeof($result['inquiries']);$i++){
				$result1['inquiry_nos'] = $this->admissionmodel->getFormdataInquiryNo($result['inquiries'][$i]->lead_id);
				$result['no'][] = $result1['inquiry_nos'];
			}
			$getenrollmentNo = $this->admissionmodel->getLastEnrollmentNo("tbl_student_master");
			if(!empty($getenrollmentNo)){
				$enrollmentNoPrefix = $getenrollmentNo[0]->enrollment_no +1;
				$result['enrollment_no'] = sprintf("%06d", $enrollmentNoPrefix);
			}
			else{
				$result['enrollment_no'] = sprintf("%06d",000001);
			}
			if(isset($_POST['student_id']))
			{
				$inquiry_id = $this->admissionmodel->getFormdata2('tbl_student_master','student_id',$_POST['student_id']);
				$result['studentImageDetails'] = $inquiry_id;
				$result['getStudentdetails'] = $this->admissionmodel->getFormdata2('tbl_inquiry_master','inquiry_master_id',$inquiry_id[0]->inquiry_master_id);
				$result['getStateName'] = $this->admissionmodel->getDetails("tbl_states",$result['getStudentdetails'][0]->state_id,"state_id");
				$result['getCityName'] = $this->admissionmodel->getDetails("tbl_cities",$result['getStudentdetails'][0]->city_id,"city_id");
			}
			$result['zones'] = $this->admissionmodel->getDropdown1("tbl_zones","zone_id,zone_name");
			$result['categories'] = $this->admissionmodel->getDropdown1("tbl_categories","category_id,categoy_name");
			$result['batch'] = $this->admissionmodel->getDropdown1("tbl_batch_master","batch_id,batch_name");
			$result['countries'] = $this->admissionmodel->getDropdown1("tbl_countries","country_id,country_name");
			$result['months'] = $this->admissionmodel->getMonths("tbl_childactivity_fees_month", "month_id, month_name,month,name", "status='Active' ");
			$result['details'] = $this->admissionmodel->getFormdata($record_id,$_POST['academic_year_id']);
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$result['getStateName'] = $this->admissionmodel->getDetails("tbl_states",$result['details'][0]->state_id,"state_id");
				$result['getCityName'] = $this->admissionmodel->getDetails("tbl_cities",$result['details'][0]->city_id,"city_id");
				$isAdmission = $this->admissionmodel->getdata1("tbl_admission_fees", "admission_fees_id", "student_id='".$record_id."' AND  academic_year_id='".$_POST['academic_year_id']."' ");
				if(!empty($isAdmission)){
					$result['flag'] = 1;
				}else{
					$result['flag'] = 0;
				}
			}
			$result['otherdetails'] = $this->remainingfeesmodel->getFormdata1($record_id,$_POST['academic_year_id']);
			$result['fees_level'] = $this->admissionmodel->getDropdown1("tbl_fees_level_master","fees_level_id,fees_level_name");
			$result['login_name'] = $_SESSION["webadmin"][0]->first_name .' '. $_SESSION["webadmin"][0]->last_name;
			$result['fees_type_details'] = $this->admissionmodel->enum_select("tbl_assign_fees","fees_type");
			// echo "<pre>";
			// print_r($result['otherdetails']);
			// exit;
			$this->load->view('remainingfees/assignFees', $result);
		}else{
			redirect('login', 'refresh');
		}
	}

	function getSelectedAcademicYear(){
		// print_r($_POST);
			$academic_info = $this->admissionmodel->getdata1("tbl_academic_year_master", "*", "academic_year_master_id='".$_POST['academic_year_id']."'");

			if($academic_info){
				echo json_encode(array("success"=>"1",'msg'=>'academic year found',"academic_name" =>$academic_info[0]['academic_year_master_name']));
				exit;
			}else{
				echo json_encode(array("success"=>"0",'msg'=>'academic year not found',"academic_name"=>null));
				exit;
			}

	}


	function submitfeesInfoForm(){

		// echo "<pre>";
		// print_r($_POST);
		// exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$addmissionfees_id = 0;
			
			if(empty($_POST['is_course_added']) && empty($_POST['is_group_added'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please add atleast one group fee or course fee.'));
				exit;
			}

			if(!isset($_SESSION['student_id']) && !$_POST['student_id']){
				echo json_encode(array("success"=>"0",'msg'=>'Enter Child Information First!'));
				exit;
			}

			//existing course fees checking
			// if(!empty($_POST['is_course_added']) ){
			// 	if(!empty($_POST['fees_id'])){
			// 		$exiting_course_feedetails = $this->admissionmodel->getdata1("tbl_admission_fees","*","student_id='".$_POST['student_id']."' AND fees_id='".$_POST['fees_id']."' AND academic_year_id ='".$_POST['academic_year_id']."' AND fees_type='Class'");
			// 		if(!empty($exiting_course_feedetails)){
			// 			echo json_encode(array("success"=>"0",'msg'=>'The course fee is already assigned.'));
			// 			exit;
			// 		}
			// 	}else{
			// 		echo json_encode(array("success"=>"0",'msg'=>'Please add atleast one course fee.'));
			// 		exit;
			// 	}
			// }

			//existing group fees checking
			if(!empty($_POST['is_group_added']) ){
				if(!empty($_POST['group_fees_id'])){
					foreach($_POST['group_fees_id'] as $key=>$val){
						$exiting_group_feedetails = $this->admissionmodel->getdata1("tbl_admission_fees","*","student_id='".$_POST['student_id']."' AND fees_id='".$val."' AND academic_year_id ='".$_POST['academic_year_id']."' AND fees_type='Group' AND group_start_date between '".date("Y-m-d",strtotime($_POST['group_start_date'][$key]))."' AND '".date("Y-m-d",strtotime($_POST['group_end_date'][$key]))."' ");
						if(!empty($exiting_group_feedetails)){
							echo json_encode(array("success"=>"0",'msg'=>'The Group '.($key+1).' fee is already assigned.'));
							exit;
						}
					}

					if(!empty($_POST['group_groupid'])){
						if($this->array_has_dupes($_POST['group_groupid'])){
							echo json_encode(array("success"=>"0",'msg'=>'You have selected same groups multiple time. '));
							exit;
						}
					}
				}else{
					echo json_encode(array("success"=>"0",'msg'=>'Please Add atleast one Group Fee.'));
					exit;
				}
				if($this->array_has_dupes($_POST['group_fees_id'])){
					echo json_encode(array("success"=>"0",'msg'=>'You have selected same fees for multiple groups'));
					exit;
				}
			}

			if(!empty($_POST['is_course_added'])){

				if($_POST['is_instalment'] == 'Yes'){
					if($_POST['no_of_installments'] > $_POST['max_installment'])
					{
						echo json_encode(array("success"=>"0",'msg'=>'Installment No should not greater than Max Installment!'));
						exit;
					}
					for ($i=0; $i < $_POST['no_of_installments'] ; $i++) { 
						if($_POST['instalment_due_date'][$i] == ''){
							echo json_encode(array("success"=>"0",'msg'=>'Please Enter Installment Due date!'));
							exit;
						}
						if(isset($_POST['instalment_due_date'][$i+1])){
							if(strtotime($_POST['instalment_due_date'][$i]) > strtotime($_POST['instalment_due_date'][$i+1])){
								echo json_encode(array(
									'success' => '0',
									'msg' => 'Please re-check the payment due dates.'
								));
								exit;
							}
						}
					}
					$_POST['total_amount'] = $_POST['total_amountl'];
					$_POST['discount_type'] = $_POST['discount_type_installment'];
				}else{
					$_POST['total_amount'] = $_POST['total_amount'];
					$_POST['discount_type'] = $_POST['discount_type'];
				}
				$_POST['discount_amount_rs'] = (!empty($_POST['discount_amount_rsl']))?$_POST['discount_amount_rsl']:$_POST['discount_amount_rs'];
				if (isset($_SESSION['student_id']) || $_POST['student_id']) {
					$data_array = array();	
					if(isset($_SESSION['student_id'])){		
						$student_id = $_SESSION['student_id'];
					}
					else{
						$student_id = $_POST['student_id'];
					}
	
					$getCenter = $this->admissionmodel->getDetails("tbl_student_master",$student_id,"student_id");

					// start shiv code on 7-7-2020 as new year fee is not getting 
					// $getzonecenter = $this->admissionmodel->getDetails("tbl_student_details",$student_id,"student_id");

					$getCategory = $this->admissionmodel->getdata1("tbl_promoted_student","promoted_academic_year_id as academic_year_id ,promoted_course_id as course_id ,promoted_batch_id as batch_id,category_id ","student_id = '".$_POST['student_id']."' && promoted_academic_year_id = '".$_POST['academic_year_id']."' ");
					// end shiv code on 7-7-2020 as new year fee is not getting 
					// echo $this->db->last_Query();
					// print_r($getCategory);
					// exit;
					
					if(!empty($getCenter) && !empty($getCategory)){
					
						$data_array['academic_year_id'] = $getCategory[0]['academic_year_id'];
						$data_array['student_id'] = $student_id;
						$data_array['fees_id'] = (!empty($_POST['fees_id'])) ? $_POST['fees_id'] : '';
						$data_array['fees_type'] = 'Class';
						$data_array['zone_id'] = $getCenter[0]->zone_id ;
						$data_array['center_id'] = $getCenter[0]->center_id;
						$data_array['category_id'] = $getCategory[0]['category_id'] ;
						$data_array['course_id'] = $getCategory[0]['course_id'] ;
						$data_array['batch_id'] = $getCategory[0]['batch_id'];
						$data_array['group_id'] = 0 ;
						$data_array['is_instalment'] = (!empty($_POST['is_instalment'])) ? $_POST['is_instalment'] : '';
						
						$total_discount_amount = 0;
						for($k=0;$k<sizeof($_POST['discount_amount']);$k++){
							if(!empty($_POST['discount_amount'][$k]) && $_POST['discount_amount'][$k] > 0){
								$total_discount_amount = floatval($total_discount_amount) + floatval($_POST['discount_amount'][$k]);
							}else{
								$total_discount_amount = $total_discount_amount;
							}
						}

						$total_collected_amount = 0;
						$total_remainig_amount = 0;
						if($_POST['is_instalment'] == 'Yes'){
							$data_array['no_of_installments'] = (!empty($_POST['no_of_installments'])) ? $_POST['no_of_installments'] : '';
							$data_array['fees_amount_collected'] = 0;
							$data_array['fees_remaining_amount'] = (!empty($_POST['total_amount'])) ? ($_POST['total_amount'] -$total_discount_amount) : 0;
						}
						else{
							$data_array['fees_amount_collected'] = 0;
							$data_array['fees_remaining_amount'] =  ($_POST['total_amount'] - $total_discount_amount);
						}

						$data_array['fees_total_amount'] = (!empty($_POST['fees_total_amount'])) ? $_POST['fees_total_amount'] : '';
						$data_array['discount_amount'] = $total_discount_amount;
						$total_gst_amount = 0;
						for ($g=0; $g <sizeof($_POST['gst_amount']) ; $g++) {
							$total_gst_amount = floatval($total_gst_amount) + floatval($_POST['gst_amount'][$g]);
						}
						$data_array['gst_amount'] = $total_gst_amount;
						$data_array['total_amount'] = (!empty($_POST['total_amount'])) ? $_POST['total_amount'] : '';
						$data_array['fees_remark'] = (!empty($_POST['fees_remark'])) ? $_POST['fees_remark'] : '';
						$data_array['fees_approved_accepted_by_name'] = (!empty($_POST['fees_approved_accepted_by_name'])) ? $_POST['fees_approved_accepted_by_name'] : '';
						$data_array['created_on'] = date("Y-m-d H:i:s");
						$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
						$data_array['updated_on'] = date("Y-m-d H:i:s");
						$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
						
						$payfeesdataresult = $this->admissionmodel->insertData('tbl_admission_fees', $data_array, '1');
						//saving in component
						if(!empty($payfeesdataresult)){
							//get fess components
							$component_condition = "f.status='Active' && i.fees_master_id='".$_POST['fees_id']."' ";
							$getFeesComponents = $this->admissionmodel->getFeesComponents($component_condition);
							if($_POST['discount_type'] == "percentage"){
								$_POST['discount_amount_percentage'] = (!empty($_POST['discount_amount_percentage']))?$_POST['discount_amount_percentage']:$_POST['discount_amount_percentage_installment'];
							}else{
								$_POST['discount_amount_percentage'] = (!empty($_POST['discount_amount_percentage']))?$_POST['discount_amount_percentage']:$_POST['discount_amount_percentage_installment'];
								$discount_amount_per_component = 0;
								foreach($getFeesComponents as $fkey=>$fval){
									if($fval['is_discountable'] == "Yes"){
										$discount_amount_per_component = ($discount_amount_per_component + $fval['component_fees']);
									}
								}
								$divide_value = ($discount_amount_per_component > 0)?$discount_amount_per_component:1;
								$_POST['discount_amount_percentage'] = (($_POST['discount_amount_percentage'] / $divide_value)*100);
							}
							if(!empty($getFeesComponents)){
								for($i=0; $i < sizeof($getFeesComponents); $i++){
									$getGstDetail = $this->admissionmodel->getdata1("tbl_gst_master", "gst_master_id, tax", "status='Active' &&  gst_master_id='".$getFeesComponents[$i]['gst_tax_id']."' ");
									$fees_component = array();
									$fees_component['admission_fees_id'] = $payfeesdataresult;
									$fees_component['fees_id'] = $_POST['fees_id'];
									$fees_component['fees_component_id'] = $getFeesComponents[$i]['fees_component_master_id'];
									$fees_component['fees_component_id_value'] = $getFeesComponents[$i]['component_fees'];
	
									$lumpsum = false;
									$installment = false;
									if($getFeesComponents[$i]['is_discountable'] == 'Yes'){
										if($_POST['discount_amount_percentage'] != '' ){
											$discount_amount = (floatval($getFeesComponents[$i]['component_fees']) * floatval($_POST['discount_amount_percentage']))/100;
											$fees_component['discount_amount'] = $discount_amount;
											$fees_component['discount_percentage'] = $_POST['discount_amount_percentage'];
											$lumpsum = true;
										}else{
											$discount_amount = 0;
											$fees_component['discount_amount'] = 0;
											$fees_component['discount_percentage'] = 0;
										}
	
										if($getFeesComponents[$i]['component_fees'] < $discount_amount){
											$fees_component['discount_amount'] = 0;
											$fees_component['discount_percentage'] = 0;
										}
									}else{
										$fees_component['discount_amount'] = 0;
										$fees_component['discount_percentage'] = 0;
									}
	
									$discountamount = $getFeesComponents[$i]['component_fees'] - $fees_component['discount_amount'];
									if($getFeesComponents[$i]['is_gst'] == 'Yes'){
										$fees_component['gst_amount'] = (floatval($discountamount) * floatval($getGstDetail[0]['tax']))/100;
										$tax = explode(' ', $getGstDetail[0]['tax']);
										$fees_component['gst_percentage'] = $tax[0];
									}
									else{
										$fees_component['gst_amount'] = 0;
										$fees_component['gst_percentage'] = 0;
									}
									$fees_component['sub_total'] = floatval($discountamount) - floatval($fees_component['gst_amount']);
									$this->admissionmodel->insertData('tbl_admission_fees_components', $fees_component, 1);
	
								}
							}
						}
						if($_POST['is_instalment'] == 'Yes'){
							$data_array2 = array();
							for($j=0;$j<$_POST['no_of_installments'];$j++){
								$data_array2['academic_year_id'] = $getCategory[0]['academic_year_id'];
								$data_array2['student_id'] = $student_id;
								$data_array2['admission_fees_id'] = (!empty($payfeesdataresult)) ? $payfeesdataresult : '';
								$data_array2['fees_id'] = (!empty($_POST['fees_id'])) ? $_POST['fees_id'] : '';
								$data_array2['fees_type'] = 'Class';
								$installment_due_date = explode('-',$_POST['instalment_due_date'][$j]);
								$data_array2['instalment_due_date'] = (!empty($_POST['instalment_due_date'][$j])) ?  $installment_due_date[2].'-'.$installment_due_date[1].'-'.$installment_due_date[0] : '';
								$data_array2['instalment_amount'] = (!empty($_POST['instalment_amount'][$j])) ? $_POST['instalment_amount'][$j] : '';
								$data_array2['instalment_collected_amount'] = 0;
								$data_array2['instalment_remaining_amount'] = (!empty($_POST['instalment_amount'][$j])) ? $_POST['instalment_amount'][$j] : '';
								$data_array2['instalment_status'] = 'Pending';
								$data_array2['created_on'] = date("Y-m-d H:i:s");
								$data_array2['created_by'] = $_SESSION["webadmin"][0]->user_id;
								$data_array2['updated_on'] = date("Y-m-d H:i:s");
								$data_array2['updated_by'] = $_SESSION["webadmin"][0]->user_id;
								$payfeesdataresult = $this->admissionmodel->insertData('tbl_admission_fees_instalments', $data_array2, '1');
							}
						}else{
							$data_array2 = array();
							$data_array2['academic_year_id'] = $getCategory[0]['academic_year_id'];
							$data_array2['student_id'] = $student_id;
							$data_array2['admission_fees_id'] = (!empty($payfeesdataresult)) ? $payfeesdataresult : '';
							$data_array2['fees_id'] = (!empty($_POST['fees_id'])) ? $_POST['fees_id'] : '';
							$data_array2['fees_type'] = 'Class';
							$data_array2['instalment_due_date'] = (!empty($_POST["course_lumpsum_date"]))?date('Y-m-d',strtotime($_POST["course_lumpsum_date"])):date("Y-m-d");
							$data_array2['instalment_amount'] = (!empty($_POST['total_amountl'])) ? $_POST['total_amountl'] : '';
							$data_array2['instalment_collected_amount'] = 0;
							$data_array2['instalment_remaining_amount'] = (!empty($_POST['total_amount'])) ? $_POST['total_amount'] : '';
							$data_array2['instalment_status'] = 'Pending';
							$data_array2['created_on'] = date("Y-m-d H:i:s");
							$data_array2['created_by'] = $_SESSION["webadmin"][0]->user_id;
							$data_array2['updated_on'] = date("Y-m-d H:i:s");
							$data_array2['updated_by'] = $_SESSION["webadmin"][0]->user_id;
							$payfeesdataresult = $this->admissionmodel->insertData('tbl_admission_fees_instalments', $data_array2, '1');
							
						}
						if(isset($_POST['paymentprocess'])){
							$flag =1 ;
						}
						else{
							$flag = 0;
						}
					}
					
				}
			}

		// save group fees -------------------------------start
			if(!empty($_POST['is_group_added'])){
				if(!empty($_POST['group_fees_id'])){
					foreach($_POST['group_total_amount'] as $key=>$val){
						$group_data = array();
						$student_id = $_POST['student_id'];
						// $studentinfo = $this->admissionmodel->getdata1("tbl_student_details", "academic_year_id, category_id,course_id", "student_id='".$student_id."'");

						$studentinfo = $this->admissionmodel->getdata1("tbl_promoted_student","promoted_academic_year_id as academic_year_id ,promoted_course_id as course_id ,promoted_batch_id as batch_id,category_id ","student_id = '".$_POST['student_id']."' && promoted_academic_year_id = '".$_POST['academic_year_id']."' ");
						$group_data['academic_year_id'] = $studentinfo[0]['academic_year_id'];
						$group_data['student_id'] = (!empty($_POST['student_id'])) ? $_POST['student_id'] : '';
						$group_data['fees_id'] = (!empty($_POST['group_fees_id'][$key])) ? $_POST['group_fees_id'][$key] : '';
						$group_data['fees_type'] = 'Group';
						$group_data['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
						$group_data['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
						$group_data['category_id'] = 0;
						$group_data['course_id'] = 0;
						$group_data['batch_id'] = (!empty($_POST['group_batchid'][$key])) ? $_POST['group_batchid'][$key] : '';
						$group_data['group_id'] = (!empty($_POST['group_groupid'][$key])) ? $_POST['group_groupid'][$key] : '';
						$group_data['group_start_date'] = (!empty($_POST['group_start_date'][$key])) ? date("Y-m-d",strtotime($_POST['group_start_date'][$key])) : NULL;
						$group_data['group_end_date'] = (!empty($_POST['group_end_date'][$key])) ? date("Y-m-d",strtotime($_POST['group_end_date'][$key])) : NULL;
						$frequency_count = 1;
						if(!empty($_POST['group_month_id'][$key])){
							$group_data['is_instalment'] = "Yes";
							$group_data['no_of_installments'] = 1;	
							//get frequency count
							$frequency_count_data = $this->admissionmodel->getdata1("tbl_childactivity_fees_month","*","month_id='".$_POST['group_month_id'][$key]."'");
							if(!empty($frequency_count_data)){
								$frequency_count = $frequency_count_data[0]['month'];
							}
						}else{
							$group_data['is_instalment'] = (!empty($_POST['is_group_installment'][$key]) && $_POST['is_group_installment'][$key] == "lumpsum")?"No":"Yes";
							$group_data['no_of_installments'] = (!empty($_POST['group_installment_count'][$key]))?$_POST['group_installment_count'][$key]:"0";
						}
						
						$group_data['fees_total_amount'] = (!empty($_POST['group_fees_total_amount'][$key])) ? $_POST['group_fees_total_amount'][$key] : '';
	
						// set discount percentage and amount
						$_POST['group_discount_amount'][$key] = (!empty($_POST['group_fees_total_amount'][$key])) ? $_POST['group_fees_total_amount'][$key] : '';

						$component_condition = "f.status='Active' && i.fees_master_id='".$_POST['group_fees_id'][$key]."' ";
						$getFeesComponents = $this->admissionmodel->getFeesComponents($component_condition);

						//convert discount into percentage
						$discount_amount_per_component = 0;
						foreach($getFeesComponents as $fkey=>$fval){
							$fval['component_fees'] = ($fval['component_fees'] * $frequency_count);
							if($fval['is_discountable'] == "Yes"){
								$discount_amount_per_component = ($discount_amount_per_component + $fval['component_fees']);
							}
						}
						$divide_value = ($discount_amount_per_component > 0)?$discount_amount_per_component:1;
						$_POST['group_discount_amount_percentage'][$key] = (($_POST['group_discount_amount_rs'][$key] / $divide_value)*100);

						$total_discount_amount = 0;
						if(!empty($getFeesComponents)){
							for($i=0; $i < sizeof($getFeesComponents); $i++){
								$fees_component = array();
								$getFeesComponents[$i]['component_fees'] = ($getFeesComponents[$i]['component_fees'] * $frequency_count);
								if($getFeesComponents[$i]['is_discountable'] == 'Yes'){
									$discount_amount = (floatval($getFeesComponents[$i]['component_fees']) * floatval($_POST['group_discount_amount_percentage'][$key]))/100;
									if($getFeesComponents[$i]['component_fees'] < $discount_amount){
										$fees_component['discount_amount'] = 0;
										$fees_component['discount_percentage'] = 0;
									}else{
										$fees_component['discount_amount'] = $discount_amount;
										$fees_component['discount_percentage'] = $_POST['group_discount_amount_percentage'][$key];
									}
								}else{
									$fees_component['discount_amount'] = 0;
									$fees_component['discount_percentage'] = 0;
								}
								$total_discount_amount = floatval($total_discount_amount) + floatval($fees_component['discount_amount']);
							}
						}
	
						$group_data['discount_amount'] = $total_discount_amount;
						$group_data['gst_amount'] = (!empty($_POST['group_total_gst_amount'][$key])) ? $_POST['group_total_gst_amount'][$key] : '';
						$group_data['total_amount'] = (!empty($_POST['group_total_amount'][$key])) ? $_POST['group_total_amount'][$key] : '';
						$group_data['fees_amount_collected'] = 0;
						$group_data['fees_remaining_amount'] =  (!empty($_POST['group_fees_total_amount'][$key])) ? $_POST['group_fees_total_amount'][$key] : '';
						$group_data['fees_remark'] = (!empty($_POST['group_fees_remark'][$key])) ? $_POST['group_fees_remark'][$key] : '';
						$group_data['fees_approved_accepted_by_name'] = (!empty($_POST['group_fees_approved_accepted_by_name'][$key])) ? $_POST['group_fees_approved_accepted_by_name'][$key] : '';
						
						
						$group_data['created_on'] = date("Y-m-d H:m:i");
						$group_data['created_by'] = $_SESSION["webadmin"][0]->user_id;
						$group_data['updated_on'] = date("Y-m-d H:m:i");
						$group_data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
						
						$result = $this->admissionmodel->insertData('tbl_admission_fees', $group_data, 1);
						// $getCategory = $this->admissionmodel->getDetails("tbl_student_details",$student_id,"student_id");
						// start shiv code on 7-7-2020 as new year fee is not getting 
						// $getzonecenter = $this->admissionmodel->getDetails("tbl_student_details",$student_id,"student_id");

						$getCategory = $this->admissionmodel->getdata1("tbl_promoted_student","promoted_academic_year_id as academic_year_id ,promoted_course_id as course_id ,promoted_batch_id as batch_id,category_id ","student_id = '".$_POST['student_id']."' && promoted_academic_year_id = '".$_POST['academic_year_id']."' ");
						// end shiv code on 7-7-2020 as new year fee is not getting 
						
						
						//save if installment fees present
						if(!empty($_POST['group_installment_count'][$key]) && !empty($_POST['group_instalment_amount'][$key])){
							foreach($_POST['group_instalment_amount'][$key] as $ikey=>$ival){
								$group_data_array2 = array();
								$group_data_array2['academic_year_id'] = $getCategory[0]['academic_year_id'];
								$group_data_array2['student_id'] = $student_id;
								$group_data_array2['admission_fees_id'] = (!empty($result)) ? $result : '';
								$group_data_array2['fees_id'] = (!empty($_POST['group_fees_id'][$key])) ? $_POST['group_fees_id'][$key] : '';
								$group_data_array2['fees_type'] = 'Group';
								$group_data_array2['instalment_due_date'] = (!empty($_POST['group_instalment_due_date'][$key][$ikey]))?date("Y-m-d",strtotime($_POST['group_instalment_due_date'][$key][$ikey])):date("Y-m-d");
								$group_data_array2['instalment_amount'] = (!empty($_POST['group_instalment_amount'][$key][$ikey])) ? $_POST['group_instalment_amount'][$key][$ikey] : '';
								$group_data_array2['instalment_collected_amount'] = 0;
								$group_data_array2['instalment_remaining_amount'] = (!empty($_POST['group_instalment_amount'][$key][$ikey])) ? $_POST['group_instalment_amount'][$key][$ikey] : '';
								$group_data_array2['instalment_status'] = 'Pending';
								$group_data_array2['created_on'] = date("Y-m-d H:i:s");
								$group_data_array2['created_by'] = $_SESSION["webadmin"][0]->user_id;
								$group_data_array2['updated_on'] = date("Y-m-d H:i:s");
								$group_data_array2['updated_by'] = $_SESSION["webadmin"][0]->user_id;
								$payfeesdataresult = $this->admissionmodel->insertData('tbl_admission_fees_instalments', $group_data_array2, '1');
							}
						}else{
							$group_data_array2 = array();
							$group_data_array2['academic_year_id'] = $getCategory[0]['academic_year_id'];
							$group_data_array2['student_id'] = $student_id;
							$group_data_array2['admission_fees_id'] = (!empty($result)) ? $result : '';
							$group_data_array2['fees_id'] = (!empty($_POST['group_fees_id'][$key])) ? $_POST['group_fees_id'][$key] : '';
							$group_data_array2['fees_type'] = 'Group';
							$group_data_array2['instalment_due_date'] = (!empty($_POST["group_lumpsum_date"][$key]))?date('Y-m-d',strtotime($_POST["group_lumpsum_date"][$key])):date("Y-m-d");
							$group_data_array2['instalment_amount'] = (!empty($_POST['group_total_amount'][$key])) ? $_POST['group_total_amount'][$key] : '';
							$group_data_array2['instalment_collected_amount'] = 0;
							$group_data_array2['instalment_remaining_amount'] = (!empty($_POST['group_total_amount'][$key])) ? $_POST['group_total_amount'][$key] : '';
							$group_data_array2['instalment_status'] = 'Pending';
							$group_data_array2['created_on'] = date("Y-m-d H:i:s");
							$group_data_array2['created_by'] = $_SESSION["webadmin"][0]->user_id;
							$group_data_array2['updated_on'] = date("Y-m-d H:i:s");
							$group_data_array2['updated_by'] = $_SESSION["webadmin"][0]->user_id;
							$payfeesdataresult = $this->admissionmodel->insertData('tbl_admission_fees_instalments', $group_data_array2, '1');
						}
						if(!empty($result)){
							//get fess components
							$component_condition = "f.status='Active' && i.fees_master_id='".$_POST['group_fees_id'][$key]."' ";
							$getFeesComponents = $this->admissionmodel->getFeesComponents($component_condition);
							if(!empty($getFeesComponents)){
								//convert discount into percentage
								$discount_amount_per_component = 0;
								foreach($getFeesComponents as $fkey=>$fval){
									if($fval['is_discountable'] == "Yes"){
										$discount_amount_per_component = ($discount_amount_per_component + ($fval['component_fees'] * $frequency_count));
									}
								}
								$divide_value = ($discount_amount_per_component > 0)?$discount_amount_per_component:1;
								$_POST['group_discount_amount_percentage'][$key] = (($_POST['group_discount_amount_rs'][$key] / $divide_value)*100);
								for($i=0; $i < sizeof($getFeesComponents); $i++){
									$getFeesComponents[$i]['component_fees'] = ($getFeesComponents[$i]['component_fees'] * $frequency_count);
									$getGstDetail = $this->admissionmodel->getdata1("tbl_gst_master", "gst_master_id, tax", "status='Active' &&  gst_master_id='".$getFeesComponents[$i]['gst_tax_id']."'");
									$fees_component = array();
									$fees_component['admission_fees_id'] = $result;
									$fees_component['fees_id'] = $_POST['group_fees_id'][$key];
									$fees_component['fees_component_id'] = $getFeesComponents[$i]['fees_component_master_id'];
									$fees_component['fees_component_id_value'] = $getFeesComponents[$i]['component_fees'];
									if($getFeesComponents[$i]['is_discountable'] == 'Yes'){
										$discount_amount = (floatval($getFeesComponents[$i]['component_fees']) * floatval($_POST['group_discount_amount_percentage'][$key]))/100;
										if($getFeesComponents[$i]['component_fees'] < $discount_amount){
											$fees_component['discount_amount'] = 0;
											$fees_component['discount_percentage'] = 0;
										}else{
											$fees_component['discount_amount'] = $discount_amount;
											$fees_component['discount_percentage'] = $_POST['group_discount_amount_percentage'][$key];
										}
									}else{
										$fees_component['discount_amount'] = 0;
										$fees_component['discount_percentage'] = 0;
									}
									$discountamount = $getFeesComponents[$i]['component_fees'] - $fees_component['discount_amount'];

									if($getFeesComponents[$i]['is_gst'] == 'Yes'){
										$fees_component['gst_amount'] = ($discountamount * $getGstDetail[0]['tax'])/100;
										$tax = explode(' ', $getGstDetail[0]['tax']);
										$fees_component['gst_percentage'] = $tax[0];
									}
									else{
										$fees_component['gst_amount'] = 0;
										$fees_component['gst_percentage'] = 0;
									}
									$fees_component['sub_total'] = ($discountamount) - $fees_component['gst_amount'];
									$this->admissionmodel->insertData('tbl_admission_fees_components', $fees_component, 1);
								}
							}
						}
	
						//get existing linked group
						$existing_group_condition = " zone_id ='".$_POST['zone_id']."' AND center_id='".$_POST['center_id']."' AND academic_year_id='".$studentinfo[0]['academic_year_id']."' AND student_id='".$_POST['student_id']."' AND group_id='".$_POST['group_groupid'][$key]."' AND batch_id='".$_POST['group_batchid'][$key]."' ";
						$existing_linked_group = $this->admissionmodel->getdata1("tbl_student_group","*",$existing_group_condition);
						$student_group_details_data = array();
						$student_group_details_data['group_start_date'] = (!empty($_POST['group_start_date'][$key])) ? date("Y-m-d",strtotime($_POST['group_start_date'][$key])) : NULL;
						$student_group_details_data['group_end_date'] = (!empty($_POST['group_end_date'][$key])) ? date("Y-m-d",strtotime($_POST['group_end_date'][$key])) : NULL;
						$student_group_details_data['status'] = "In-active";
						$student_group_details_data['updated_on'] = date("Y-m-d H:m:i");
						$student_group_details_data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
						if(empty($existing_linked_group)){
							//insert student group fees link data
							$student_group_details_data['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
							$student_group_details_data['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
							$student_group_details_data['academic_year_id'] = $studentinfo[0]['academic_year_id'];
							$student_group_details_data['student_id'] = (!empty($_POST['student_id'])) ? $_POST['student_id'] : '';
							$student_group_details_data['group_id'] = (!empty($_POST['group_groupid'][$key])) ? $_POST['group_groupid'][$key] : '';
							$student_group_details_data['batch_id'] = (!empty($_POST['group_batchid'][$key])) ? $_POST['group_batchid'][$key] : '';
							$student_group_details_data['created_on'] = date("Y-m-d H:m:i");
							$student_group_details_data['created_by'] = $_SESSION["webadmin"][0]->user_id;
							$this->admissionmodel->insertData('tbl_student_group', $student_group_details_data, 1);
						}else{
							$this->admissionmodel->updateRecord('tbl_student_group', $student_group_details_data, "student_group_id",$existing_linked_group[0]['student_group_id']);
						}
					}
				}
			}
		// save group fees -------------------------------end
			if (!empty($payfeesdataresult)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.',
					'payfeesdataresult' => $payfeesdataresult,
					'student_id' => $student_id
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}else {
			return false;
		}
	}

	function changeReceiptStatus(){
		if(!empty($_SESSION["webadmin"])) {
			$result = array();
			$reciept_id = $_POST['reciept_id'] ;
			$result['receipt_details'] = $this->remainingfeesmodel->getdata("tbl_fees_payment_receipt", "*", "fees_payment_receipt_id='".$reciept_id."'");
			$result['status']  = $this->remainingfeesmodel->enum_select("tbl_fees_payment_receipt","status");
			$result['reasons'] = $this->remainingfeesmodel->enum_select("tbl_fees_payment_receipt","reason");
			$this->load->view('remainingfees/changeReceiptStatus', $result);
		}else{
			redirect('login', 'refresh');
		}
	}

	function changeReceiptStatusSubmit(){
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!empty($_POST['fees_payment_receipt_id'])){
				//get receipt data 
				$receipt_details = $this->remainingfeesmodel->getdata("tbl_fees_payment_receipt","*","fees_payment_receipt_id='".$_POST['fees_payment_receipt_id']."' ");
				$receipt_no  = $receipt_details[0]['receipt_no'];
				$status_update_data = array();
				$status_update_data['status'] = $_POST['status'];
				if($_POST['status'] == "Cancel"){
					$status_update_data['reason'] =  $_POST['reason'];
					if($_POST['reason'] == "Others"){
						$status_update_data['other_reason'] = $_POST['other_reason'];
					}else{
						$status_update_data['other_reason'] = "";
					}
					$status_update_data['cancelled_date'] = date("Y-m-d",strtotime($_POST['cancelled_date']));
					$status_update_data['cleared_date'] = "0000-00-00 00:00:00";
					/* clear existing payment datas */
					$receipt_history_details = $this->remainingfeesmodel->getReceiptHistoryDetails($receipt_no);
					
					if(!empty($receipt_history_details)){
						// reverse  all the amount
						foreach($receipt_history_details as $key=>$val){
							if($val['instalment_collected_amount'] > 0){
								//update the installment collected amount
								$installment_details = $this->remainingfeesmodel->getdata("tbl_admission_fees_instalments", "*","admission_fees_instalment_id = '".$val['admission_fees_instalment_id']."' ");
								if($installment_details[0]['instalment_collected_amount'] > 0){
									$installment_amount_array = array();
									$installment_amount_array['instalment_collected_amount'] = ($installment_details[0]['instalment_collected_amount'] - $val['instalment_collected_amount']);
									$installment_amount_array['instalment_remaining_amount'] = ($installment_details[0]['instalment_remaining_amount'] + $val['instalment_collected_amount']);
									$this->remainingfeesmodel->updateRecord("tbl_admission_fees_instalments", $installment_amount_array, "admission_fees_instalment_id",$val['admission_fees_instalment_id']);
								}

								//update the admission fees collected amount
								$admission_fees_details = $this->remainingfeesmodel->getdata("tbl_admission_fees", "*",'admission_fees_id = "'.$val['admission_fees_id'].'"');
								if($admission_fees_details[0]['fees_amount_collected'] > 0){
									$admission_fees_amount_array = array();
									$admission_fees_amount_array['fees_amount_collected'] = ($admission_fees_details[0]['fees_amount_collected'] - $val['instalment_collected_amount']);
									$admission_fees_amount_array['fees_remaining_amount'] = ($admission_fees_details[0]['fees_remaining_amount'] + $val['instalment_collected_amount']);
									$this->remainingfeesmodel->updateRecord("tbl_admission_fees", $admission_fees_amount_array, "admission_fees_id",$val['admission_fees_id']);
								}
							}
						}

						//update the collection history values
						$collection_history_details = $this->remainingfeesmodel->getdata("tbl_admission_fees_components_collected_history", "*",'receipt_no = "'.$receipt_no.'" AND collected_amount != 0');
						if(!empty($collection_history_details)){
							//get the last collected history of this admission id
							foreach($collection_history_details as $key=>$val){
								$last_collected_history_details = $this->remainingfeesmodel->getdata("tbl_admission_fees_components_collected_history", "*",'admission_fees_component_id = "'.$val['admission_fees_component_id'].'" AND is_last_entry = "Yes"');
								$collected_history_update_data = array();
								$collected_history_update_data['remaining_amount'] = ($last_collected_history_details[0]['remaining_amount'] + $val['collected_amount']);
								$this->remainingfeesmodel->updateRecordbyConditionArray("tbl_admission_fees_components_collected_history", $collected_history_update_data, "admission_fees_component_id ='".$val['admission_fees_component_id']."' AND is_last_entry = 'Yes'");
							}
						}
					}

					//assign cheque bounce fee if the cheque bounced
					if($_POST['reason'] == "Cheque Bounce"){
						//get the cheque bounce fee details
						$getExistingAdmissionFeesDetails = $this->remainingfeesmodel->getdata("tbl_admission_fees","*","admission_fees_id='".$receipt_history_details[0]['admission_fees_id']."' ");
						if(!empty($getExistingAdmissionFeesDetails)){
							$condition = "f.fees_id ='21'"; 
							$chequeBounceFeesDetails = $this->remainingfeesmodel->getChequeBounceFeesDetails("21");
							//insert of admission fees data for cheque bounce
							$admission_fees_data = array();
							$admission_fees_data['fees_id'] =  $chequeBounceFeesDetails[0]['fees_id'];
							$admission_fees_data['fees_type'] = $chequeBounceFeesDetails[0]['fees_selection_type'];
							$admission_fees_data['fees_total_amount'] = $chequeBounceFeesDetails[0]['component_fees'];
							$admission_fees_data['total_amount'] = $chequeBounceFeesDetails[0]['component_fees'];
							$admission_fees_data['fees_remaining_amount'] = $chequeBounceFeesDetails[0]['component_fees'];
							$admission_fees_data['academic_year_id'] = $getExistingAdmissionFeesDetails[0]['academic_year_id'];
							$admission_fees_data['student_id'] = $getExistingAdmissionFeesDetails[0]['student_id'];
							$admission_fees_data['zone_id'] = $getExistingAdmissionFeesDetails[0]['zone_id'];
							$admission_fees_data['center_id'] = $getExistingAdmissionFeesDetails[0]['center_id'];
							$admission_fees_data['category_id'] = $getExistingAdmissionFeesDetails[0]['category_id'];
							$admission_fees_data['course_id'] = $getExistingAdmissionFeesDetails[0]['course_id'];
							$admission_fees_data['is_instalment']  = "No";
							$admission_fees_data['created_on'] = date("Y-m-d H:m:i");
							$admission_fees_data['created_by'] = $_SESSION["webadmin"][0]->user_id;
							$admission_fees_data['updated_on'] = date("Y-m-d H:m:i");
							$admission_fees_data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
							$admission_fees_insert_result  = $this->admissionmodel->insertData('tbl_admission_fees', $admission_fees_data, 1);
							if(!empty($admission_fees_insert_result)){
								//insert component data
								$fees_component_data_array = array();
								$fees_component_data_array['admission_fees_id'] = $admission_fees_insert_result;	
								$fees_component_data_array['fees_id'] = $chequeBounceFeesDetails[0]['fees_id'];	
								$fees_component_data_array['fees_component_id'] = $chequeBounceFeesDetails[0]['fees_component_id'];	
								$fees_component_data_array['fees_component_id_value'] = $chequeBounceFeesDetails[0]['component_fees'];	
								$fees_component_data_array['sub_total'] =$chequeBounceFeesDetails[0]['component_fees'];	
								$this->admissionmodel->insertData('tbl_admission_fees_components', $fees_component_data_array, 1);

								// insert installment details
								$installment_data_array = array();
								$installment_data_array['academic_year_id'] = $getExistingAdmissionFeesDetails[0]['academic_year_id'];
								$installment_data_array['student_id'] = $getExistingAdmissionFeesDetails[0]['student_id'];
								$installment_data_array['admission_fees_id'] = $admission_fees_insert_result;
								$installment_data_array['fees_id'] =  $chequeBounceFeesDetails[0]['fees_id'];
								$installment_data_array['fees_type'] =  $chequeBounceFeesDetails[0]['fees_selection_type'];
								$installment_data_array['instalment_due_date'] =  date("Y-m-d H:m:i");
								$installment_data_array['instalment_amount'] =  $chequeBounceFeesDetails[0]['component_fees'];
								$installment_data_array['instalment_remaining_amount'] =  $chequeBounceFeesDetails[0]['component_fees'];
								$installment_data_array['instalment_remaining_amount'] =  $chequeBounceFeesDetails[0]['component_fees'];
								$installment_data_array['instalment_status'] =  "Pending";
								$installment_data_array['created_on'] = date("Y-m-d H:m:i");
								$installment_data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
								$installment_data_array['updated_on'] = date("Y-m-d H:m:i");
								$installment_data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
								$this->admissionmodel->insertData('tbl_admission_fees_instalments', $installment_data_array, 1);
							}
						}
					}
				}else if($_POST['status'] == "Cleared"){
					$status_update_data['cleared_date'] = date("Y-m-d",strtotime($_POST['cleared_date']));
					$status_update_data['cancelled_date'] = "0000-00-00 00:00:00";
					$status_update_data['reason'] =  null;
					$status_update_data['other_reason'] = "";
				}else{
					$status_update_data['cancelled_date'] = "0000-00-00 00:00:00";
					$status_update_data['cleared_date'] = "0000-00-00 00:00:00";
					$status_update_data['reason'] =  null;
					$status_update_data['other_reason'] = "";
				}
				$result = $this->remainingfeesmodel->updateRecord("tbl_fees_payment_receipt",$status_update_data,"fees_payment_receipt_id",$_POST['fees_payment_receipt_id']);
				if($result){
					echo json_encode(array('success' => '1','msg' => 'Status changed successfully.'));
					exit;
				}else{
					echo json_encode(array('success' => '0','msg' => 'Problem in updating status.'));
					exit;
				}
			}else{
				echo json_encode(array('success' => '0','msg' => 'Receipt id not found.'));
				exit;
			}
		}
	}

	public function getFees(){
		$condition = "f.fees_selection_type='".$_REQUEST['feesselectiontype']."' && i.zone_id='".$_REQUEST['zone_id']."' && map.center_id='".$_REQUEST['center_id']."' &&  i.status='Active' && f.fees_type='".$_REQUEST['fees_type']."' "; 
					
		if($_REQUEST['feesselectiontype'] == 'Class'){
			$condition .=" && i.academic_year_id= '".$_REQUEST['academic_year_id']."' && i.category_id='".$_REQUEST['categoryid']."' && gc.course_id='".$_REQUEST['courseid']."' ";
		}else{
			$condition .=" && gc.group_id='".$_REQUEST['groupid']."'  ";
		}
		$current_date = date("Y-m-d");
		if(!empty($_REQUEST['batch_start_date']) && !empty($_REQUEST['batch_end_date'])){
			$condition .= " AND (f.fees_from_date >= '".date("Y-m-d",strtotime($_REQUEST['batch_start_date']))."'  AND i.open_from_date <= '".$current_date."') ";
		}
		
		$result = $this->remainingfeesmodel->getCenterFees($condition,$_REQUEST['feesselectiontype']);
		$option = '';
		$fees_id = '';
		
		if(isset($_REQUEST['fees_id']) && !empty($_REQUEST['fees_id'])){
			$fees_id = $_REQUEST['fees_id'];
		}
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]['fees_id'] == $fees_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]['fees_id'].'" '.$sel.' >'.$result[$i]['fees_name'].' </option>';
			}
		}
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
}

?>
