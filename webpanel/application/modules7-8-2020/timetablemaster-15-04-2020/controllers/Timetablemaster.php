<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Timetablemaster extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('timetablemastermodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('timetablemaster/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['categories'] = $this->timetablemastermodel->getDropdown("tbl_categories","category_id,categoy_name");
			$result['details'] = $this->timetablemastermodel->getFormdata($record_id);
			$result['videos'] = $this->timetablemastermodel->getdata("tbl_timetable_videos", "timetable_id='".$record_id."' ");
			$result['documents'] = $this->timetablemastermodel->getdata("tbl_timetable_documents", "timetable_id='".$record_id."' ");
			//echo "<pre>";print_r($result['videos']);exit;
			
			$this->load->view('template/header.php');
			$this->load->view('timetablemaster/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function getCourses(){
		$result = $this->timetablemastermodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['course_id']) && !empty($_REQUEST['course_id'])){
			$course_id = $_REQUEST['course_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getThemes(){
		$result = $this->timetablemastermodel->getOptions("tbl_themes",$_REQUEST['course_id'],"course_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$theme_id = '';
		
		if(isset($_REQUEST['theme_id']) && !empty($_REQUEST['theme_id'])){
			$theme_id = $_REQUEST['theme_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->theme_id == $theme_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->theme_id.'" '.$sel.' >'.$result[$i]->theme_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getWeeks(){
		$result = $this->timetablemastermodel->getOptions("tbl_weeks",$_REQUEST['theme_id'],"theme_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$week_id = '';
		
		if(isset($_REQUEST['week_id']) && !empty($_REQUEST['week_id'])){
			$week_id = $_REQUEST['week_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->week_id == $week_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->week_id.'" '.$sel.' >'.$result[$i]->week_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			
			
			$condition = "category_id='".$_POST['category_id']."' && course_id='".$_POST['course_id']."' &&  theme_id='".$_POST['theme_id']."' && timetable_title= '".$_POST['timetable_title']."' ";
			if(isset($_POST['timetable_id']) && $_POST['timetable_id'] > 0)
			{
				$condition .= " &&  timetable_id != ".$_POST['timetable_id'];
			}
			
			$check_name = $this->timetablemastermodel->getdata("tbl_timetable_master",$condition);
			//echo "<pre>";print_r($check_name);exit;
			//echo $_SESSION["webadmin"][0]->user_name;exit;
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}
			
			if(empty($_POST['video_title']) && empty($_POST['doc_title'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please upload video OR document!'));
				exit;
			}
			
			
			$thumnail_value = "";
			if(isset($_FILES) && isset($_FILES["cover_image"]["name"]))
			{
				$config['upload_path'] = DOC_ROOT_FRONT."/images/timetable_images/";
				$config['max_size']    = '1000000';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['file_name']     = md5(uniqid("100_ID", true));
				//$config['file_name']     = $_FILES["cover_image"]["name"].;
				
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("cover_image"))
				{
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
					exit;
				}
				else
				{
					$image_data = array('upload_data' => $this->upload->data());
					$thumnail_value = $image_data['upload_data']['file_name'];
				}
				
				/* Unlink previous category image */
				if(!empty($_POST['timetable_id']))
				{
					$image = $this->timetablemastermodel->getFormdata($_POST['timetable_id']);
					if(is_array($image) && !empty($image[0]->cover_image) && file_exists(DOC_ROOT_FRONT."/images/timetable_images/".$image[0]->cover_image))
					{
						@unlink(DOC_ROOT_FRONT."/images/timetable_images/".$image[0]->cover_image);
					}
				}
				
			}
			else
			{
				$thumnail_value = $_POST['input_cover_image'];
			}
			
			
			//exit;
			if (!empty($_POST['timetable_id'])) {
				$data_array = array();			
				$timetable_id = $_POST['timetable_id'];
		 		
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
				$data_array['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : '';
				$data_array['theme_id'] = (!empty($_POST['theme_id'])) ? $_POST['theme_id'] : '';
				//$data_array['week_id'] = (!empty($_POST['week_id'])) ? $_POST['week_id'] : '';
				$data_array['timetable_title'] = (!empty($_POST['timetable_title'])) ? $_POST['timetable_title'] : '';
				//$data_array['timetable_name'] = (!empty($_POST['timetable_name'])) ? $_POST['timetable_name'] : '';
				$data_array['cover_image'] = $thumnail_value;
				
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->timetablemastermodel->updateRecord('tbl_timetable_master', $data_array,'timetable_id',$timetable_id);
				
				if(!empty($_POST['video_title'])){
					$this->timetablemastermodel->delrecord1('tbl_timetable_videos','timetable_id',$timetable_id);
					for($i=0; $i < sizeof($_POST['video_title']); $i++){
						$data = array();
						$data['timetable_id'] = $timetable_id;
						$data['video_title'] = $_POST['video_title'][$i];
						$data['video_url'] = $_POST['video_url'][$i];
						$data['created_on'] = date("Y-m-d H:i:s");
						$data['created_by'] = $_SESSION["webadmin"][0]->user_id;
						$data['updated_on'] = date("Y-m-d H:i:s");
						$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
						$this->timetablemastermodel->insertData('tbl_timetable_videos', $data, '1');
						
					}
				}
				
				
				$this->load->library('upload');
				
				if(!empty($_POST['doc_title']) && isset($_POST['doc_title'])){
					$files = $_FILES;
					foreach($_POST['doc_title'] as $key=>$value){
						$doc_data = array();
						$doc_data['timetable_id'] = $timetable_id;
						$doc_data['doc_title'] = $_POST['doc_title'][$key];
						$doc_data['doc_type'] = $_POST['doc_type'][$key];
						$doc_data['doc_description'] = $_POST['doc_description'][$key];
						
						$doc_data['created_on'] = date("Y-m-d H:i:s");
						$doc_data['created_by'] = $_SESSION["webadmin"][0]->user_id;
						$doc_data['updated_on'] = date("Y-m-d H:i:s");
						$doc_data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
						
						if(!empty($files['doc_file']['name'][$key]) && isset($files['doc_file']['name'][$key])){
						
							if(!empty($_POST['doc_file_old'][$key])){
								$del_path = DOC_ROOT."/images/timetable_images/".$_POST['doc_file_old'][$key];
								if(file_exists($del_path)){
									@unlink($del_path);
								}
							}
							
							//$file_name	= md5(uniqid("100_ID", true));
							$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
							//$file_name = str_replace(' ', '-', $files['doc_file']['name'][$key]);
							
							$_FILES['doc_file']['name']= $file_name;
							$_FILES['doc_file']['type']= $files['doc_file']['type'][$key];
							$_FILES['doc_file']['tmp_name']= $files['doc_file']['tmp_name'][$key];
							$_FILES['doc_file']['error']= $files['doc_file']['error'][$key];
							$_FILES['doc_file']['size']= $files['doc_file']['size'][$key];    
							
							$this->upload->initialize($this->set_upload_options());
						
							$this->upload->do_upload('doc_file');
							
							$doc_data['doc_file'] = $file_name;
							
						}else{
							$doc_data['doc_file'] = $_POST['doc_file_old'][$key];
						}
						
						if(!empty($_POST['timetable_document_id'][$key])){
							$this->timetablemastermodel->updateRecord('tbl_timetable_documents', $doc_data,'timetable_document_id',$_POST['timetable_document_id'][$key]);
						}else{
							$this->timetablemastermodel->insertData('tbl_timetable_documents',$doc_data,'1');
						}
						
					}
				}
				
				
			}else {
				
				$data_array = array();
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
				$data_array['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : '';
				$data_array['theme_id'] = (!empty($_POST['theme_id'])) ? $_POST['theme_id'] : '';
				//$data_array['week_id'] = (!empty($_POST['week_id'])) ? $_POST['week_id'] : '';
				$data_array['timetable_title'] = (!empty($_POST['timetable_title'])) ? $_POST['timetable_title'] : '';
				//$data_array['timetable_name'] = (!empty($_POST['timetable_name'])) ? $_POST['timetable_name'] : '';
				$data_array['cover_image'] = $thumnail_value;
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				//echo "<pre>";print_r($data_array);exit;
				
				$result = $this->timetablemastermodel->insertData('tbl_timetable_master', $data_array, '1');
				
				if(!empty($_POST['video_title'])){
					for($i=0; $i < sizeof($_POST['video_title']); $i++){
						$data = array();
						$data['timetable_id'] = $result;
						$data['video_title'] = $_POST['video_title'][$i];
						$data['video_url'] = $_POST['video_url'][$i];
						$data['created_on'] = date("Y-m-d H:i:s");
						$data['created_by'] = $_SESSION["webadmin"][0]->user_id;
						$data['updated_on'] = date("Y-m-d H:i:s");
						$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
						$this->timetablemastermodel->insertData('tbl_timetable_videos', $data, '1');
						
					}
				}
				
				$this->load->library('upload');
				
				if(!empty($_POST['doc_title']) && isset($_POST['doc_title'])){
					$files = $_FILES;
					foreach($_POST['doc_title'] as $key=>$value){
						$doc_data = array();
						$doc_data['timetable_id'] = $result;
						$doc_data['doc_title'] = $_POST['doc_title'][$key];
						$doc_data['doc_type'] = $_POST['doc_type'][$key];
						$doc_data['doc_description'] = $_POST['doc_description'][$key];
						
						$doc_data['created_on'] = date("Y-m-d H:i:s");
						$doc_data['created_by'] = $_SESSION["webadmin"][0]->user_id;
						$doc_data['updated_on'] = date("Y-m-d H:i:s");
						$doc_data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
						
						if(!empty($files['doc_file']['name'][$key]) && isset($files['doc_file']['name'][$key])){
							
							//$file_name	= md5(uniqid("100_ID", true));
							$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
							//$file_name = str_replace(' ', '-', $files['doc_file']['name'][$key]);
							
							$_FILES['doc_file']['name']= $file_name;
							$_FILES['doc_file']['type']= $files['doc_file']['type'][$key];
							$_FILES['doc_file']['tmp_name']= $files['doc_file']['tmp_name'][$key];
							$_FILES['doc_file']['error']= $files['doc_file']['error'][$key];
							$_FILES['doc_file']['size']= $files['doc_file']['size'][$key];    
							
							$this->upload->initialize($this->set_upload_options());
						
							$this->upload->do_upload('doc_file');
							
							$doc_data['doc_file'] = $file_name;
							
						}else{
							$doc_data['doc_file'] = "";
						}
						
						$this->timetablemastermodel->insertData('tbl_timetable_documents',$doc_data,'1');
					}
				}
				
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		//echo $id;exit;
		$get_result = $this->timetablemastermodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->categoy_name);
				array_push($temp, $get_result['query_result'][$i]->course_name);
				array_push($temp, $get_result['query_result'][$i]->theme_name);
				//array_push($temp, $get_result['query_result'][$i]->week_name);
				array_push($temp, $get_result['query_result'][$i]->timetable_title);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("WeekAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->timetable_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	

				$actionCol22="";
				if($this->privilegeduser->hasPrivilege("WeekAddEdit")){
					$actionCol22 .= '<a href="javascript:void(0);" onclick="parent_show(\'' . $get_result['query_result'][$i]->timetable_id. '\',\'' . $get_result['query_result'][$i]->parent_show. '\');" title="">'.$get_result['query_result'][$i]->parent_show.'</a>';
				}
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("WeekAddEdit")){
					$actionCol1.= '<a href="timetablemaster/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->timetable_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol22);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function changeapproval(){
		$result = "";
		$condition = "1=1 && course_id='".$_GET['id']."' ";
		$result['details'] = $this->timetablemastermodel->getdata('tbl_courses',$condition);
		//echo "<pre>";print_r($result["details"]);exit;
		$view = $this->load->view('routinesactions/changeapproval',$result,true);
		echo $view;
	}
	
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->timetablemastermodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('coursesmaster/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	function delRecord()
	{
		
		$id=$_POST['id'];
		
		$appdResult = $this->timetablemastermodel->delrecord1("tbl_timetable_videos","timetable_video_id",$id);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delDocRecord()
	{
		
		$id=$_POST['id'];
		$get_name = $this->timetablemastermodel->getdata("tbl_timetable_documents","timetable_document_id='".$id."' ");
		
		if(!empty($get_name[0]['doc_file'])){
			$del_path = DOC_ROOT."/images/timetable_images/".$get_name[0]['doc_file'];
			if(file_exists($del_path)){
				@unlink($del_path);
			}
		}
		
		$appdResult = $this->timetablemastermodel->delrecord1("tbl_timetable_documents","timetable_document_id",$id);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delCoverImage()
	{
		
		$id=$_POST['id'];
		
		$get_name = $this->timetablemastermodel->getdata("tbl_timetable_master","timetable_id='".$id."' ");
		
		if(!empty($get_name[0]['cover_image'])){
			$del_path = DOC_ROOT."/images/timetable_images/".$get_name[0]['cover_image'];
			if(file_exists($del_path)){
				@unlink($del_path);
			}
		}
		
		$status="";
		
		$appdResult = $this->timetablemastermodel->delCoverImage("tbl_timetable_master","timetable_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}		
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->timetablemastermodel->delrecord12("tbl_timetable_master","timetable_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	private function set_upload_options()
	{   
		//upload an image options //products
		$config = array();
		$config['upload_path'] = "images/timetable_images/";
		//$config['allowed_types'] = '*';
		$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;

		return $config;
	}

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	function parent_show()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$value=$_POST['value'];
		
		$appdResult = $this->timetablemastermodel->parent_show("tbl_timetable_master","timetable_id",$id,$value);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
}

?>
