<?php 
error_reporting(0);
$arrayYesNo = array('Yes'=>'Yes','No'=>'No');
$arrayActive = array('Active'=>'Active','Inactive'=>'Inactive','SoldOut'=>'Sold Out');

/*
echo "<pre>";
print_r($students);
exit;
*/
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Send Newsletter</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>newsletters">Send Newsletter</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-4">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="newsletter_id" name="newsletter_id" value="<?php if(!empty($details[0]->newsletter_id)){echo $details[0]->newsletter_id;}?>" />
							  
								
								<div class="control-group form-group">
									<label class="control-label"><span>Students*</span></label>
									<div class="controls">
										<select id="students" name="students[]" class="form-control select2" multiple>
											<option value="">Select Students</option>
											<option value="danish.bijun@aptech.ac.in">Biju Nair</option>
											<option value="mayank@attoinfotech.com">Mayank</option>
											<option value="infodanish@gmail.com">Danish Akhtar</option>
											<option value="danish.akhtar@attoinfotech.com">Danish</option>
											<?php 
												if(isset($students) && !empty($students)){
												for($i=0; $i < sizeof($students);$i++){
												?>
											<option value="<?php echo $students[$i]['Email_Address'];?>"><?php echo $students[$i]['Student_Name']." (".$students[$i]['Email_Address']." )";?></option>
											<?php }}?>
										</select>
									</div>
								</div>
								
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>newsletters" class="btn btn-primary">Cancel</a>
								</div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>


$( document ).ready(function() {
	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						['Source', 'Templates'],['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
						['Find','Replace','-','Subscript','Superscript'],
						['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],['BidiLtr', 'BidiRtl' ],
						['Maximize', 'ShowBlocks'],['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
});

var vRules = {
	zone_text:{required:true},
	region_text:{required:true},
	area_text:{required:true},
	center_id:{required:true},
	newsletter_title:{required:true, alphanumericwithspace:true},
	newsletter_description:{required:true}
	
};
var vMessages = {
	zone_text:{required:"Please select zone."},
	region_text:{required:"Please select region."},
	area_text:{required:"Please select area."},
	center_id:{required:"Please select center."},
	newsletter_title:{required:"Please enter title."},
	newsletter_description:{required:"Please enter contents."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>newsletters/submitEmailForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						//window.location = "<?php echo base_url();?>newsletters";
						window.location.reload(true);
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

document.title = "Send Newsletter";

 
</script>					
