<?php 
//session_start();
//print_r($_SESSION["webadmin"]);
//echo "<pre>";
//print_r($roles);exit;
?>
<!-- start: Content -->
<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1>Attendance Report</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>attendance_report">Attendance Report</a></li>
        </ul>
      </div>
    </div> 
    <div class="card">    
    	<div class="page-title-border">
		<div class="col-sm-4 col-md-4 left-button-top" style="padding-top: 20px;">
				<!-- <p>
					<?php if ($this->privilegeduser->hasPrivilege("PromoteAddEdit")) { ?>
						<a href="<?php  echo base_url();?>promote/addEdit" class="btn btn-primary icon-btn"><i class="fa fa-plus"></i>Do Promote Student</a>	
					<?php } ?>
				</p>		 -->
            </div>
			
        <form name="filter_form admission_filter_form" method="post" id="filter_form">
			<div class="col-sm-4 col-xs-4">
				<div class="dataTables_filter searchFilterClass form-group">
					<label for="Zone" class="control-label">Select Academic year</label>
					<select name="sSearch_0" id="sSearch_0" class="searchInput form-control">
						<option value="">Select Academy year</option>
						<?php if(!empty($academicyear)){
								foreach($academicyear as $key=>$val){
						?>
							<option value="<?php echo $val['academic_year_master_id']; ?>"><?php echo $val['academic_year_master_name'];?></option>
						<?php
								}
							}
						?>
					</select>
				</div>
			</div>  
        </div>   
		
        <div class="col-sm-12" id="filter_data" style="clear: both;display: none" >
	         	<div class="box-content form-horizontal product-filter">    
				   

					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Zone" class="control-label">Zone</label>
							<select name="sSearch_1" id="sSearch_1" class="searchInput form-control">
								<option value="">Select Zone</option>
								<?php 
									if(!empty($zoneDetails)){
										foreach($zoneDetails as $key=>$val){
								?>
									<option value="<?php echo $val['zone_id']; ?>"><?php echo $val['zone_name'];?></option>
								<?php
										}
									}
								?>
							</select>
						</div>
					</div>
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Center" class="control-label">Center Name</label>
							<select name="sSearch_2" id="sSearch_2" class="searchInput form-control" placeholder="Center Name">
								<option value="">Select Center</option>
							</select>
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Class" class="control-label">Category Name</label>
							<select name="sSearch_3" id="sSearch_3" class="searchInput form-control" placeholder="Category Name">
							<option value="">Select Category</option>
							<?php 
							
									if(!empty($categories)){
										foreach($categories as $key=>$val){
								?>
									<option value="<?php echo $val['category_id']; ?>"><?php echo $val['categoy_name'];?></option>
								<?php
										}
									}
								?>
							</select>
						</div>
					</div>
				
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Center" class="control-label">Course Name</label>
							<select name="sSearch_4" id="sSearch_4" class="searchInput form-control" placeholder="Center Name">
								<option value="">Select Center</option>
							</select>
						</div>
					</div>

					<div class="col-sm-3 col-xs-12">
					 	<div class="dataTables_filter searchFilterClass form-group">
							<label for="firstname" class="control-label">Enrollment No./Student Name</label>
							<input id="sSearch_5" name="sSearch_5" type="text" class="searchInput form-control" placeholder="Enrollment No/Student Name"/>
						</div>
					</div>

					<div class="dataTables_filter searchFilterClass" style="margin-right:5px;margin-left:10px">
						<label for="firstname" class="control-label">Date</label>
						<input id="sSearch_6" name="sSearch_6" style="width:200px;" type="text" class="searchInput form-control" >
					</div>

					<div class="control-group clearFilter">
						<div class="controls">
							<a href="javascript:void(0)" onclick="clearSearchFilters();"><button class="btn btn-primary" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
							<a class="export_attendance_report"><button class="btn btn-primary" style="margin:32px 10px 10px 10px;">Export Data</button> </a>
						</div>
					</div>
	            </div>
         </div>
		 
		 </form>
         <div class="clearfix"></div>
         <div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                    <table class="dynamicTable display table table-bordered non-bootstrap" >
                        <thead>
                          <tr>
							<th>Date & Time</th>
							<th>Zone</th>
							<th>Centre Name</th>
							<th>Centre code</th>
							<th>ENR No.</th>
							<th>Student Name</th>
							<th>Manual Reference</th>
							<!-- <th>Category Name</th> -->
							<th>Course/group Name</th>
							<th>Batch Name</th>
							<th>Attendance date</th>
							<th>Attendance</th>
							
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
    </div>
</div><!-- end: Content -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script>
	$( document ).ready(function() {
		clearSearchFilters();
		var start = moment().subtract(29, 'days');
		var end = moment();

		function cb(start, end) {
			$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		}

		$('#sSearch_6').daterangepicker({
		startDate:new Date(),
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			locale:{
			format:'DD/MM/YYYY'
			},
		},cb);

		cb(start, end);

	});	

	$("#sSearch_0").on("change",function(){
		$("#filter_data").show();
	})
	
	$(".export_attendance_report").on("click", function(){
		var act = "<?php  echo base_url();?>attendance_report/export";
		$("#filter_form").attr("action",act);
		$("#filter_form").submit();
	});

	$("#filter_form").on("submit", function(){
		$('.export_attendance_report').removeAttr("disabled");
		$('.export_contactus_sizes').removeAttr("disabled");
		// setTimeout("location.reload(true);",1000);
	});
    <?php
	if(isset($_SESSION['admission_export_success']))
	{
		?>
		displayMsg("<?php echo $_SESSION['admission_export_success']; ?>", "<?php echo $_SESSION['admission_export_msg']; ?>");
		<?php
		unset($_SESSION['admission_export_success']);
		unset($_SESSION['admission_export_msg']);
	}
	?>
	<?php
	if(isset($_SESSION['student_id']))
	{
		unset($_SESSION['student_id']);
	}
	?>
	function deleteData1(id,status){
		var sta="";
		var sta1=" ";
		if(status=='Active'){
			sta="In-active";
		}else{
			sta="Active";
		}
    	var r=confirm("Are you sure to " +sta);
    	if (r==true){
    		//window.location.href="users/delete?id="+id;
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/delrecord12/",
				data:{"id":id,"status":sta},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Updated!");
						setTimeout("location.reload(true);",1000);
						
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});
    	}
    }


	$(document).on("change",".searchInput",function(){
		var element = $(this).attr("id");
		var functionName = "";
		var column_name = "";
		var id =  "";
		/* if(element == "sSearch_0" || element == "sSearch_1"){
			if(element == "sSearch_0"){
				column_name = "academic_year_id";
				id =  $("#sSearch_0").val();
				$("#sSearch_1").val("");
			}else */
			if(element == "sSearch_1"){
				column_name = "zone_id";
				id =  $("#sSearch_1").val();
				$("#sSearch_2").val("");
			} else if(element == "sSearch_3" ){
				column_name = "category_id";
				id =  $("#sSearch_3").val();
				$("#sSearch_2").val("");
			}else if(element == "sSearch_2"){
				column_name = "center_id";
				id =  $("#sSearch_2").val();
			}
			if(id != ""){
				$.ajax({
					url: "<?php echo base_url().$this->router->fetch_module();?>/getSearchLists",
					data:{"column_name":column_name,"value":id},
					async: false,
					type: "POST",
					dataType:"JSON",
					success: function(response){
						if(response.status){
							if(element == "sSearch_1"){
								$("#sSearch_2").html(response.option);
							}else if(element == "sSearch_3" ){
								$("#sSearch_4").html(response.option);
							}
						}
					}
				});
			}else{
				if(element == "sSearch_1"){
					$("#sSearch_2").html("<option value=''>Select Center</option>");
				}
			}
		// }
	})

	document.title = "Attendance Report";
</script>