<?php 
if (!empty($_GET['id']) && isset($_GET['id'])) {
	$varr = base64_decode(strtr($_GET['id'], '-_', '+/'));
	parse_str($varr, $url_prams);
	$record_id = $url_prams['id'];
}
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1>User - Batches</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>centerusers">Center User</a></li>
        </ul>
      </div>
    </div>
    <div class="card">       
     <div class="card-body">             
        <div class="box-content">
            <div class="col-sm-8 col-md-12">
				<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
					<input type="hidden" id="user_id" name="user_id" value="<?php echo $record_id;?>" />

					<div class="control-group form-group">
						<label class="control-label"><span>Batch Selection Type*</span></label>
						<div class="controls">
							<input type="radio" class="form-control batch_selection_type" name="batch_selection_type" value="Class" <?php echo (!empty($details[0]->batch_selection_type) &&  ($details[0]->batch_selection_type== 'Class')) ? "checked" : ""?> checked="checked">Class
							<input type="radio" class="form-control batch_selection_type" name="batch_selection_type" value="Group" <?php echo (!empty($details[0]->batch_selection_type) &&  ($details[0]->batch_selection_type== 'Group')) ? "checked" : ""?> >Group
						</div>
					</div>
					
					<div class="control-group form-group class_div">
						<label class="control-label" for="category_id">Category*</label>
						<div class="controls">
							<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);getBatches(this.value);">
								<option value="" >Select Category</option>
								<?php 
									if(isset($categories) && !empty($categories)){
										foreach($categories as $cdrow){
											$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>
					
					<div class="control-group form-group class_div">
						<label class="control-label"><span>Course*</span></label> 
						<div class="controls">
							<select id="course_id" name="course_id" class="form-control"  onchange="getBatches(this.value);">
								<option value="">Select Course</option>
							</select>
						</div>
					</div>

					<div class="control-group form-group group_div">
						<label class="control-label" for="category_id">Group*</label>
						<div class="controls">
							<select id="group_id" name="group_id" class="form-control" onchange="getGroupBatches(this.value);">
								<option value="">Select Group</option>
								<?php 
									if(isset($groups) && !empty($groups)){
										foreach($groups as $cdrow){
											$sel = ($cdrow->group_master_id == $details[0]->group_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->group_master_id;?>" <?php echo $sel; ?>><?php echo $cdrow->group_master_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>

					<div class="control-group form-group">
						<label class="control-label"><span>Batch*</span></label> 
						<input type="checkbox" id="checkbox" >Select All
						<div class="controls">
							<select id="batch_id" name="batch_id[]" class="form-control select2 batch_id" multiple >
								<option value="">Select Batch</option>
							</select>
						</div>
					</div>
					
					<div class="form-actions form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="javascript:history.go(-1)" class="btn btn-primary">Cancel</a>
					</div>
				</form>
            </div>
        <div class="clearfix"></div>
        </div>
     </div>
    </div>        
</div><!-- end: Content -->								
<script>
$( document ).ready(function() {
	$('.group_div').hide();
	$("#checkbox").click(function(){
		if($("#checkbox").is(':checked') ){
			$("#batch_id > option").prop("selected","selected");
			$("#batch_id").trigger("change");
		}else{
			$("#batch_id > option").removeAttr("selected");
			$("#batch_id").trigger("change");
		}
	});
	$('.batch_selection_type').change(function(){
		var batch_selection_type = $(this).val();
		if(batch_selection_type == 'Class'){
			$('.class_div').show();
			$('.group_div').hide();
		}
		else{
			$('.class_div').hide();
			$('.group_div').show();
		}
	})
});

function getCourses(category_id)
{
	if(category_id != "" )
	{
		var user_id = $('#user_id').val();
		$.ajax({
			url:"<?php echo base_url();?>userbatches/getCourses",
			data:{category_id:category_id , user_id : user_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#course_id").html("<option value=''>Select Courses</option>"+res['option']);
					}
					else
					{
						$("#course_id").html("<option value=''> Courses</option>");
					}
				}
				else
				{	
					$("#course_id").html("<option value=''> Courses</option>");
				}
			}
		});
	}
}

function getBatches(course_id){
	var category_id = $('#category_id').val();
	var course_id = $('#course_id').val();
	var user_id = $('#user_id').val();
	if(category_id !=''){
		$("#batch_id").html("<option value='' disabled> Select Batches</option>");
		$("#batch_id").select2();
	}
	if(category_id != '' && course_id != ''){
		$.ajax({
			url:"<?php echo base_url();?>userbatches/getBatches",
			data:{category_id:category_id , course_id : course_id ,user_id : user_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#batch_id").html("<option value='' disabled>Select Batches</option>"+res['option']);
						$("#batch_id").select2();
					}
					else
					{
						$("#batch_id").html("<option value='' disabled> Select Batches</option>");
						$("#batch_id").select2();
					}
				}
				else
				{	
					$("#batch_id").html("<option value='' disabled> Select Batches</option>");
					$("#batch_id").select2();
				}
			}
		});
	}
}

function getGroupBatches(group_id){
	var group_id = $('#group_id').val();
	var user_id = $('#user_id').val();
	if(group_id != ''){
		$.ajax({
			url:"<?php echo base_url();?>userbatches/getGroupBatches",
			data:{group_id:group_id ,user_id : user_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#batch_id").html("<option value='' disabled>Select Batches</option>"+res['option']);
						$("#batch_id").select2();
					}
					else
					{
						$("#batch_id").html("<option value='' disabled> Select Batches</option>");
						$("#batch_id").select2();
					}
				}
				else
				{	
					$("#batch_id").html("<option value='' disabled> Select Batches</option>");
					$("#batch_id").select2();
				}
			}
		});
	}
}

var vRules = {
	category_id:{required:true},
	course_id:{required:true},
	group_id:{required:true}
	
};
var vMessages = {
	category_id:{required:"Please select category."},
	course_id:{required:"Please select course."},
	group_id:{required:"Please select group."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>userbatches/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>userbatches/index?text=<?php echo $_GET['id'];?>";
					},2000);

				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Center User Batches";

 
</script>					
