<?php 
error_reporting(0);
$arrayYesNo = array('Yes'=>'Yes','No'=>'No');
$arrayActive = array('Active'=>'Active','Inactive'=>'Inactive','SoldOut'=>'Sold Out');

//echo "<pre>";
//print_r($product_colors);
//exit;

$cls = array();
$cls_val = "";
if(!empty($collections)){
	for($i=0; $i < sizeof($collections);$i++){
		$cls[] = $collections[$i]['collection_name'];
	}
	
	$cls_val = implode(",",$cls);
}

$col_arr = array();
$col_arr_val = "";
if(!empty($product_colors)){
	for($i=0; $i < sizeof($product_colors); $i++){
		$col_arr[] = $product_colors[$i]->color_name;
	}
	$col_arr_val = implode(",",$col_arr);
}

?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Products Details</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>products">Products</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-4">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="product_id" name="product_id" value="<?php if(!empty($details[0]->product_id)){echo $details[0]->product_id;}?>" />
							  
								
								<div class="control-group form-group">
									<label class="control-label"><span>Category*</span></label>
									<div class="controls">
										<?php if(!empty($details[0]->category_name)){echo $details[0]->category_name;}?>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Subcategory*</span></label>
									<div class="controls">
										<?php if(!empty($details[0]->subcategory_name)){echo $details[0]->subcategory_name;}?>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Collections*</span></label>
									<div class="controls">
										<?php if(!empty($cls_val)){echo $cls_val;}?>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Colors*</span></label>
									<div class="controls">
										<?php if(!empty($col_arr_val)){echo $col_arr_val;}?>
									</div>
								</div>
								
								
								<div class="control-group form-group">
									<label class="control-label"><span>Product Name*</span></label>
									<div class="controls">
										<?php if(!empty($details[0]->product_name)){echo $details[0]->product_name;}?>
									</div>
								</div>
								
								
								<div class="control-group form-group">
									<label class="control-label"><span>Product Code*</span></label>
									<div class="controls">
										<?php if(!empty($details[0]->product_code)){echo $details[0]->product_code;}?>
									</div>
								</div>
								
								
								<div class="control-group form-group">
									<label class="control-label"><span>Product Price *</span></label>
									<div class="controls">
										<?php if(!empty($details[0]->product_price)){echo $details[0]->product_price;}?>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Product Offer Price</span></label>
									<div class="controls">
										<?php if(!empty($details[0]->product_offer_price)){echo $details[0]->product_offer_price;}?>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Buy Now Link*</span></label>
									<div class="controls">
										<?php if(!empty($details[0]->buy_now_link)){echo $details[0]->buy_now_link;}?>
									</div>
								</div>
								
								
								<div class="control-group form-group">
									<label class="control-label"><span>Product Description</span></label>
									<div class="controls">
										<?php if(!empty($details[0]->short_description)){echo $details[0]->short_description;}?>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Meta Title</span></label>
									<div class="controls">
										<?php if(!empty($details[0]->meta_title)){echo $details[0]->meta_title;}?>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Meta Keyword</span></label>
									<div class="controls">
										<?php if(!empty($details[0]->meta_keywords)){echo $details[0]->meta_keywords;}?>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Meta Description</span></label>
									<div class="controls">
										<?php if(!empty($details[0]->meta_description)){echo $details[0]->meta_description;}?>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Status</span></label>
									<div class="controls">
										<?php if(!empty($details[0]->status)){echo $details[0]->status;}?>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label" for="selectError">Product Images</label>
									<div class="product_upload"   style="width: 900px;height: auto;overflow-x: scroll;overflow-y: hidden;white-space: nowrap;">
										<ul class="product-images">
											<?php if(!empty($productImages)):?>
											<?php foreach($productImages as $productImage):?>
											<li style="float:left;margin:0px 5px;">
												<img width="220" height="220" src = "<?php echo FRONT_URL;?>/images/products/<?php echo $productImage->product_thumbnail_image;?>" /><br />
												<div style="float:left; margin-top:10px;">
													<button class="setDefaultProductImage" type="button" disabled alt="<?php echo $productImage->image_id;?>">Set as Default</button>&nbsp;&nbsp;
													<button class="deleteProductImage" type="button" disabled alt="<?php echo $productImage->image_id;?>">Delete Image</button>
												</div>
												
											</li>
											<?php endforeach;?>
											<?php else:?>
												<li>No Product Image</li>
											<?php endif;?>
											
										</ul>
									</div>
								</div>
						
								<div class="form-actions form-group">
									<!--<button type="submit" class="btn btn-primary">Submit</button>-->
									<a href="<?php echo base_url();?>products" class="btn btn-primary">Cancel</a>
								</div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

 function showProductGift()
		 {
				if($("#product_type").val() == 'Product')
				{
					 $("#divTypeProduct").show();
					 $("#divTypeGiftCard").hide();
				}
				else if($("#product_type").val() == 'GiftCard')
				{
					 $("#divTypeProduct").hide();
					 $("#divTypeGiftCard").show();
				}
				else
				{				
					$("#divTypeProduct").hide();
					$("#divTypeGiftCard").hide();
				}
		 }

function adddiam(){
	$( "#appenddia" ).clone().appendTo( "#vis" );
}

function addgame(){
	$( "#appended" ).clone().appendTo( "#apenddiv" );
}

function addvendor(){
	$( "#app" ).clone().appendTo( "#ape" );
}



$( document ).ready(function() {
	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						['Source', 'Templates'],['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
						['Find','Replace','-','Subscript','Superscript'],
						['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],['BidiLtr', 'BidiRtl' ],
						['Maximize', 'ShowBlocks'],['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
});

var vRules = {
	
	product_type:{required:true},
	product_price:{required:true},
	gift_validity:{digits:true, required:true},
	metal:{required:true},
	category_id:{required:true},
	color_id:{required:true},
	width:{required:true},
	height:{required:true},
	diamond_name:{required:true},
	//clarity_id:{required:true},
	no_of_diamonds:{required:true},
	weight:{required:true},
	product_total_weight:{required:true},
	diamond_quality_price1:{required:true},
	diamond_quality_price2:{required:true},
	gemstone_name:{required:true},
	no_of_gemstone:{required:true},
	gem_weight:{required:true},
	gemstone_price:{required:true},
	making_charge:{required:true},
};
var vMessages = {
	
	product_type:{required:"Please select product type"},
	product_price:{required:"Please enter product price"},
	gift_validity:{required:"Plese enter month"},
	metal:{required:"Please select metal"},
	category_id:{required:"Please select category"},
	color_id:{required:"Please select color"},
	width:{required:"Please enter width"},
	height:{required:"Please enter height"},
	diamond_name:{required:"Please select diamond name"},
	//clarity_id:{required:"Please select stone clarity"},
	no_of_diamonds:{required:"Please enter no of diamonds"},
	weight:{required:"Please enter weight"},
	product_total_weight:{required:"Please enter total weight"},
	diamond_quality_price1:{required:"Please enter diamond quality"},
	diamond_quality_price2:{required:"Please enter diamond quality"},
	gemstone_name:{required:"Please enter gemstone name"},
	no_of_gemstone:{required:"Please enter no of gemstone"},
	gem_weight:{required:"Please enter weight"},
	gemstone_price:{required:"Please enter price"},
	making_charge:{required:"Please enter making charges"},
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>products/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>products";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

function myFunction() {
  var a = document.getElementById('metal_id').value;
  if(a == 2){
  	$("#metal_hide").show();
  }
  else
  {
  	$("#metal_hide").hide();
  }
}

$(document).ready(function(){
		
 
    $("#hide").click(function(){
        $("#vis").hide();
    });
	

    $("#show").click(function(){
        $("#vis").show();
    });
    $("#edit_color").hide();
     $("#diamond_quality").hide();
     $("#gem_quality").hide();
     $("#getDiamond").hide();
     $("#getGem").hide();
     //$("#getGem2").hide();
     //$("#getDiamond2").hide();
     $('stone_c').hide();
    
     $("#add").click(function(){
		 // $("#getDiamond2").show();
		  $( "#getDiamond_show" ).clone().prependTo( "#appendDimond" );
	 });
	 
	  $("#Gem_add").click(function(){
	  
	  $( "#getGem_show" ).clone().prependTo( "#appendGemstone" );
		//$("#getGem2").show();
	 });
	 
    $("#remove").click(function(){
		//alert($(".getDiamond_remove").length);
		  	if ($(".getDiamond_remove").length != 1) {
			
			$(".getDiamond_remove:last").remove();
				}
	 });
	  $("#Gem_remove").click(function(){
		  $("#getGem2").hide();
		   	if ($(".getGemstone_remove").length != 1) {
			
			$(".getGemstone_remove:last").remove();
				}
	 });
	 
	 var isGem=$("#IsGem").val();
	 if(isGem=="Yes")
	 {
		 $("#getGem").show();
		  $("#gem_quality").show();
	 }
	 else
	 {
		  $("#getGem").hide();
		   $("#gem_quality").hide();
	 }
	 
	 var isDia=$("#IsDiamond").val();
	 if(isDia=="Yes")
	 {
		 $("#getDiamond").show();
		 $("#diamond_quality").show();
	 }
	 else
	 {
		   $("#getDiamond").hide();
		  $("#diamond_quality").hide();
	 }
	 
	 var metal=$('#metal').val();
	 //alert(metal);
	 if(metal==1){
		 $('#edit_color').show();
	 }
	 
    $("#metal").change(function(){
		//alert($(this).val());
		var metal_id = $(this).val();
		if(metal_id !== ""){
			$.ajax({
				url: '<?php echo base_url();?>products/getcolor',
				data:{"metal_id":metal_id},
				type: 'post',
				cache: false,
				clearForm: false,
				success: function (response) {
					$("#edit_color").show();
					$("#edit_color").html(response);
				}
			});
		}
		else
		{
			$("#metal_color").html("");
		}
	});
	
	
	
	
	$(".addProductImage").click(function(){
		$("#divProductImage").append("<input type='file' name='product_image[]' /><br/>");
	});
	
	$(".setDefaultProductImage").click(function(){
		var image_id = $(this).attr("alt");
		if(image_id !== ""){
			$.ajax({
				url: '<?php echo base_url();?>products/setDefaultProductImage',
				data:{"image_id":image_id},
				type: 'post',
				cache: false,
				clearForm: false,
				success: function () {
					alert("Image is set as default");
					location.reload();
				}
			});
		}
	});
	
	$(".deleteProductImage").click(function(){
		if(confirm("Are you sure you want to delete this product image ?")){
			var image_id = $(this).attr("alt");
			if(image_id !== ""){
				$.ajax({
					url: '<?php echo base_url();?>products/deleteProductImage',
					data:{"image_id":image_id},
					type: 'post',
					cache: false,
					clearForm: false,
					success: function () {
						alert("Image is deleted");
						location.reload();
					}
				});
			}
		}
	});
});


document.title = "View Product";

 
</script>					
