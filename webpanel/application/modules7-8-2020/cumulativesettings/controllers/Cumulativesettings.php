<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Cumulativesettings extends CI_Controller

{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('cumulativesettingsmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
		
			$result = "";
			$this->load->view('template/header.php');
			$this->load->view('index',$result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->cumulativesettingsmodel->getFormdata($record_id);
			$result['zones'] = $this->cumulativesettingsmodel->getDropdown("tbl_zones","zone_id,zone_name");
			$result['categories'] = $this->cumulativesettingsmodel->getDropdown("tbl_categories","category_id,categoy_name");
			
			$this->load->view('template/header.php');
			$this->load->view('cumulativesettings/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	/*new code*/
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->cumulativesettingsmodel->getFormdata($record_id);
			
			if(!empty($result['details'])){
				//$result['productImages'] = $this->camerasettingsmodel->getProductImages($record_id);
			}
			
			$this->load->view('template/header.php');
			$this->load->view('cumulativesettings/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	
	/*new code end*/
	function fetch()
	{
		$get_result = $this->cumulativesettingsmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);
				array_push($temp, $get_result['query_result'][$i]->categoy_name);
				array_push($temp, $get_result['query_result'][$i]->course_name);
				array_push($temp, $get_result['query_result'][$i]->cumulative_max_viewing_time);
				array_push($temp, $get_result['query_result'][$i]->admin_cumulative_time);
				array_push($temp, $get_result['query_result'][$i]->center_cumulative_time);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("CCTVCumulativeSettingsAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->admin_cumulative_setting_id . '\',\'' . $get_result['query_result'][$i]->status. '\');" title="Delete">'.$get_result['query_result'][$i]->status .'</a>';
				}	
				
				$actionCol = "";
				if($this->privilegeduser->hasPrivilege("CCTVCumulativeSettingsAddEdit")){
					$actionCol.= '<a href="cumulativesettings/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->admin_cumulative_setting_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a>';
				}	
				
				array_push($temp, $actionCol21);
				//array_push($temp, $actionCol1);
				array_push($temp, $actionCol);
				array_push($items, $temp);
				
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			if (!empty($_POST['admin_cumulative_setting_id'])) {
			
				$condition = "1=1 && zone_id='".$_POST['zone_id']."' && center_id='".$_POST['center_id']."' && category_id='".$_POST['category_id']."' && course_id='".$_POST['course_id']."' ";
				if(!empty($_POST['admin_cumulative_setting_id'])){
					$condition .=" && admin_cumulative_setting_id != '".$_POST['admin_cumulative_setting_id']."' ";
				}
				$check_setting = $this->cumulativesettingsmodel->getdata('tbl_admin_cumulative_settings',$condition);
				if(!empty($check_setting)){
					echo json_encode(array("success"=>"0",'msg'=>'Setting already present.'));
					exit;
				}
						
				$data_array=array();			
				$admin_cumulative_setting_id = $_POST['admin_cumulative_setting_id'];
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '0';
				
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
				$data_array['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : '';
				
				$data_array['cumulative_max_viewing_time'] = (!empty($_POST['cumulative_max_viewing_time'])) ? $_POST['cumulative_max_viewing_time'] : '';
				$data_array['admin_cumulative_time'] = (!empty($_POST['admin_cumulative_time'])) ? $_POST['admin_cumulative_time'] : '';
				$data_array['center_cumulative_time'] = (!empty($_POST['center_cumulative_time'])) ? $_POST['center_cumulative_time'] : '';
				
				
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				//echo "<pre>";print_r($data_array);exit;
				
				$result = $this->cumulativesettingsmodel->updateRecord('tbl_admin_cumulative_settings', $data_array,'admin_cumulative_setting_id',$admin_cumulative_setting_id);
				
		
			}else {
				
				if(empty($_POST['category_id'])){
					echo json_encode(array("success"=>"0",'msg'=>'Please enter atleast one setting.'));
					exit;
				}
				
				if(!empty($_POST['category_id'])){
					
					$count =1;
					for($i=0; $i < sizeof($_POST['category_id']); $i++){
						
						$condition = "1=1 && zone_id='".$_POST['zone_id']."' && center_id='".$_POST['center_id']."' && category_id='".$_POST['category_id'][$i]."' && course_id='".$_POST['course_id'][$i]."' ";
						$check_setting = $this->cumulativesettingsmodel->getdata('tbl_admin_cumulative_settings',$condition);
						if(!empty($check_setting)){
							echo json_encode(array("success"=>"0",'msg'=>'Row no. '.$count.' setting already present.'));
							exit;
						}
						
						$count++;
					}
				}	
				//exit;
				if(!empty($_POST['category_id'])){
					for($i=0; $i < sizeof($_POST['category_id']); $i++){
					
						$data_array = array();
						
						$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
						$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '0';
						
						$data_array['category_id'] = (!empty($_POST['category_id'][$i])) ? $_POST['category_id'][$i] : '';
						$data_array['course_id'] = (!empty($_POST['course_id'][$i])) ? $_POST['course_id'][$i] : '';
						$data_array['cumulative_max_viewing_time'] = (!empty($_POST['cumulative_max_viewing_time'][$i])) ? $_POST['cumulative_max_viewing_time'][$i] : '';
						$data_array['admin_cumulative_time'] = (!empty($_POST['admin_cumulative_time'][$i])) ? $_POST['admin_cumulative_time'][$i] : '';
						
						$data_array['created_on'] = date("Y-m-d H:i:s");
						$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
						$data_array['updated_on'] = date("Y-m-d H:i:s");
						$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
						
						$result = $this->cumulativesettingsmodel->insertData('tbl_admin_cumulative_settings', $data_array, '1');
						$admin_cumulative_setting_id = $result;
						
					}
				}
				
				
				
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	function delRecord()
	{
		
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->cumulativesettingsmodel->delrecord("tbl_admin_cumulative_settings","admin_cumulative_setting_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->cumulativesettingsmodel->delrecord12("tbl_admin_cumulative_settings","admin_cumulative_setting_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}

	
	public function getCenters(){
		$result = $this->cumulativesettingsmodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		//echo "<pre>";
		//print_r($result);
		//exit;
		
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->center_id == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getCourses(){
		$result = $this->cumulativesettingsmodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['course_id']) && !empty($_REQUEST['course_id'])){
			$course_id = $_REQUEST['course_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
