<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Weeks Master</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>weeksmaster">Weeks Master</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="week_id" name="week_id" value="<?php if(!empty($details[0]->week_id)){echo $details[0]->week_id;}?>" />
								
								<div class="control-group form-group">
									<label class="control-label" for="category_id">Category*</label>
									<div class="controls">
										<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);">
											<option value="">Select Category</option>
											<?php 
												if(isset($categories) && !empty($categories)){
													foreach($categories as $cdrow){
														$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
											?>
												<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
											<?php }}?>
										</select>
									</div>
								</div>
							
								<div class="control-group form-group">
									<label class="control-label"><span>Course*</span></label> 
									<div class="controls">
										<select id="course_id" name="course_id" class="form-control" onchange="getThemes(this.value);">
											<option value="">Select Course</option>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Theme*</span></label> 
									<div class="controls">
										<select id="theme_id" name="theme_id" class="form-control" >
											<option value="">Select Theme</option>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Week Name*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Week Name" id="week_name" name="week_name" value="<?php if(!empty($details[0]->week_name)){echo $details[0]->week_name;}?>" >
									</div>
								</div>
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>weeksmaster" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	<?php 
		if(!empty($details[0]->theme_id)){
	?>
		getCourses('<?php echo $details[0]->category_id; ?>', '<?php echo $details[0]->course_id; ?>');
		getThemes('<?php echo $details[0]->course_id; ?>', '<?php echo $details[0]->theme_id; ?>');
	<?php }?>
});


function getCourses(category_id,course_id = null)
{
	//alert("Val: "+val);return false;
	if(category_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>weeksmaster/getCourses",
			data:{category_id:category_id, course_id:course_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#course_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getThemes(course_id,theme_id = null)
{
	//alert("Val: "+val);return false;
	if(course_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>weeksmaster/getThemes",
			data:{course_id:course_id, theme_id:theme_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#theme_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#theme_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#theme_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

var vRules = {
	category_id:{required:true},
	course_id:{required:true},
	theme_id:{required:true},
	week_name:{required:true, alphanumericwithspace:true}
	
};
var vMessages = {
	category_id:{required:"Please select category."},
	course_id:{required:"Please select course."},
	theme_id:{required:"Please select theme."},
	week_name:{required:"Please enter week name."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>weeksmaster/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>weeksmaster";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Weeks";

 
</script>					
