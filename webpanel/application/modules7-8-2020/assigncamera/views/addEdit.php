<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1>Assign Camera</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>assigncamera">Assign Camera</a></li>
        </ul>
      </div>
    </div>
    <div class="card">       
     <div class="card-body">             
        <div class="box-content">
            <div class="col-sm-8 col-md-12">
				<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
					<input type="hidden" id="assign_camera_id" name="assign_camera_id" value="<?php if(!empty($details[0]->assign_camera_id)){echo $details[0]->assign_camera_id;}?>" />
					

					<div class="control-group form-group">
						<label class="control-label" for="zone_id">Zone*</label>
						<div class="controls">
							<select id="zone_id" name="zone_id" class="form-control"  onchange="getCenters(this.value);" >
								<option value="">Select Zone</option>
								<?php 
									if(isset($zones) && !empty($zones)){
										foreach($zones as $cdrow){
											$sel = ($cdrow->zone_id == $details[0]->zone_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->zone_id;?>" <?php echo $sel; ?>><?php echo $cdrow->zone_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label"><span>Center*</span></label> 
						<div class="controls">
							<select id="center_id" name="center_id" class="form-control"  onchange="getBatch(this.value);getGroupBatch(this.value);" >
								<option value="">Select Center</option>
							</select>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label"><span>Fees Selection Type*</span></label>
						<div class="controls">
							<input type="radio" class="form-control fees_selection_type" name="fees_selection_type" value="Class" <?php echo (!empty($details[0]->fees_selection_type) &&  ($details[0]->fees_selection_type== 'Class')) ? "checked" : ""?> checked="checked">Class
							<input type="radio" class="form-control fees_selection_type" name="fees_selection_type" value="Group" <?php echo (!empty($details[0]->fees_selection_type) &&  ($details[0]->fees_selection_type== 'Group')) ? "checked" : ""?> >Group
						</div>
					</div>

					<div class="control-group form-group class_div">
						<label class="control-label" for="category_id">Category*</label>
						<div class="controls">
							<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);" >
								<option value="">Select Category</option>
								<?php 
									if(isset($categories) && !empty($categories)){
										foreach($categories as $cdrow){
											$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>
				
					<div class="control-group form-group class_div">
						<label class="control-label"><span>Course*</span></label> 
						<div class="controls">
							<select id="course_id" name="course_id" class="form-control" onchange="getBatch(this.value);">
								<option value="">Select Course</option>
							</select>
						</div>
					</div>

					<div class="control-group form-group group_div">
						<label class="control-label" for="category_id">Group*</label>
						<div class="controls">
							<select id="group_id" name="group_id" class="form-control"  onchange="getGroupBatch(this.value);">
								<option value="">Select Group</option>
								<?php 
									if(isset($groups) && !empty($groups)){
										foreach($groups as $cdrow){
											$sel = ($cdrow->group_master_id == $details[0]->group_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->group_master_id;?>" <?php echo $sel; ?>><?php echo $cdrow->group_master_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>
					
					<?php if(!empty($details[0]->assign_camera_id)){?>
						<div class="control-group form-group">
							<label class="control-label" for="batch_id">Batch*</label>
							<div class="controls">
								<select id="batch_id" name="batch_id" class="form-control">
									<option value="">Select Batch</option>
								</select>
							</div>
						</div>
					<?php }else{?>
						<div class="control-group form-group">
							<label class="control-label" for="batch_id">Batch*</label>
							<input type="checkbox" id="checkbox1" >Select All
							<div class="controls">
								<select id="batch_id" name="batch_id[]" class="form-control select2" multiple>
									<option value="" disabled="disabled">Select Batch</option>
								</select>
							</div>
						</div>
					<?php }?>
					

					<?php if(!empty($details[0]->assign_camera_id)){?>
						<div class="control-group form-group">
							<label class="control-label"><span>Camera*</span></label> 
							<div class="controls">
								<select id="camera_id" name="camera_id" class="form-control camera_id">
									<option value="" disabled="disabled">Select Camera</option>
									<?php 
									if(isset($camera) && !empty($camera)){
										foreach($camera as $cdrow){
											$sel = ($cdrow->camera_id == $details[0]->camera_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->camera_id;?>" <?php echo $sel; ?>><?php echo $cdrow->camera_name;?></option>
								<?php }}?>
								</select>
							</div>
						</div>
					<?php }else{?>
						<div class="control-group form-group">
							<label class="control-label"><span>Camera*</span></label> 
							<input type="checkbox" id="checkbox" >Select All
							<div class="controls">
								<select id="camera_id" name="camera_id[]" class="form-control select2 camera_id" multiple >
									<option value="" disabled="disabled">Select Camera</option>
									<?php 
									if(isset($camera) && !empty($camera)){
										foreach($camera as $cdrow){
											$sel = ($cdrow->camera_id == $details[0]->camera_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->camera_id;?>" <?php echo $sel; ?>><?php echo $cdrow->camera_name;?></option>
								<?php }}?>
								</select>
							</div>
						</div>
					<?php }?>
					
					<div class="form-actions form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="<?php echo base_url();?>assigncamera" class="btn btn-primary">Cancel</a>
					</div>
				</form>
            </div>
        <div class="clearfix"></div>
        </div>
     </div>
    </div>        
</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	$('.group_div').hide();
	$("#checkbox").click(function(){
		if($("#checkbox").is(':checked') ){
			$("#camera_id > option").prop("selected","selected");
			$("#camera_id").trigger("change");
		}else{
			$("#camera_id > option").removeAttr("selected");
			$("#camera_id").trigger("change");
		}
	});
	$("#checkbox1").click(function(){
		if($("#checkbox1").is(':checked') ){
			$("#batch_id > option").prop("selected","selected");
			$("#batch_id").trigger("change");
		}else{
			$("#batch_id > option").removeAttr("selected");
			$("#batch_id").trigger("change");
		}
	});
	<?php 
		if(!empty($details[0]->assign_camera_id)){
	?>
		getCourses('<?php echo $details[0]->category_id; ?>', '<?php echo $details[0]->course_id; ?>');
		getBatch('<?php echo $details[0]->course_id; ?>','<?php echo $details[0]->center_id; ?>','<?php echo $details[0]->batch_id; ?>');
		getGroupBatch('<?php echo $details[0]->group_id; ?>','<?php echo $details[0]->center_id; ?>','<?php echo $details[0]->batch_id; ?>');
		getCenters('<?php echo $details[0]->zone_id; ?>', '<?php echo $details[0]->center_id; ?>');
	<?php 
		if($details[0]->fees_selection_type == 'Class'){ ?>
			$('.class_div').show();
			$('.group_div').hide();
		<?php
		}
		else{ ?>
			$('.class_div').hide();
			$('.group_div').show();
		<?php
		} 
	}?>
	$('.fees_selection_type').change(function(){
		var fees_selection_type = $(this).val();
		if(fees_selection_type == 'Class'){
			$('.class_div').show();
			$('.group_div').hide();
		}
		else{
			$('.class_div').hide();
			$('.group_div').show();
		}
	})
});

function getCourses(category_id,course_id = null)
{
	//alert("Val: "+val);return false;
	if(category_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>assigncamera/getCourses",
			data:{category_id:category_id, course_id:course_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#course_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}
function getBatch(course_id,center_id,batch_id = null)
{
	console.log('hiii');
	<?php 
		if(!empty($details[0]->assign_camera_id)){
	?>
		if($('#course_id').val() == ''){
			var course_id = '<?php echo $details[0]->course_id; ?>';
		}
		else{
			var course_id = $('#course_id').val();
		}
		if($('#center_id').val() == ''){
			var center_id = '<?php echo $details[0]->center_id; ?>';
		}
		else{
			var center_id = $('#center_id').val();
		}
	<?php 
		}
		else{ 
	?>
		var course_id = $('#course_id').val();
		var center_id = $('#center_id').val();
	<?php
		} 
	?>
	if(course_id != "")
	{
		$.ajax({
			url:"<?php echo base_url();?>assigncamera/getBatch",
			data:{center_id:center_id, course_id:course_id,batch_id:batch_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#batch_id").html("<option value='' disabled='disabled'>Select</option>"+res['option']);
					}
					else
					{
						$("#batch_id").html("<option value='' disabled='disabled'>Select</option>");
					}
				}
				else
				{	
					$("#batch_id").html("<option value='' disabled='disabled'>Select</option>");
				}
			}
		});
	}
}

function getGroupBatch(group_id,center_id,batch_id = null)
{
	<?php 
		if(!empty($details[0]->assign_camera_id)){
	?>
		if($('#group_id').val() == ''){
			var course_id = '<?php echo $details[0]->group_id; ?>';
		}
		else{
			var group_id = $('#group_id').val();
		}
		if($('#center_id').val() == ''){
			var center_id = '<?php echo $details[0]->center_id; ?>';
		}
		else{
			var center_id = $('#center_id').val();
		}
	<?php 
		}
		else{ 
	?>
		var group_id = $('#group_id').val();
		var center_id = $('#center_id').val();
	<?php
		} 
	?>
	if(group_id != "")
	{
		$.ajax({
			url:"<?php echo base_url();?>assigncamera/getGroupBatch",
			data:{group_id:group_id,center_id:center_id,batch_id:batch_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#batch_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#batch_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#batch_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getCenters(zone_id,center_id = null)
{
	//alert("Val: "+val);return false;
	if(zone_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>assigncamera/getCenters",
			data:{zone_id:zone_id, center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#center_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#center_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#center_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

var vRules = {
	zone_id:{required:true},
	center_id:{required:true},
	category_id:{required:true},
	course_id:{required:true},
	group_id:{required:true},
	batch_id:{required:true}
	
};
var vMessages = {
	zone_id:{required:"Please select zone."},
	center_id:{required:"Please select center."},
	category_id:{required:"Please select category."},
	course_id:{required:"Please select course."},
	group_id:{required:"Please select group."},
	batch_id:{required:"Please select batch."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>assigncamera/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>assigncamera";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Assign Camera";

 
</script>					
