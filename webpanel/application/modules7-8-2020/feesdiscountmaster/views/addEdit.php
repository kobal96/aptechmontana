<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Create Group Payment Frequency</h1>            
          </div>
          <div>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
              <li><a href="<?php echo base_url();?>feesdiscountmaster">Create Group Payment Frequency</a></li>
            </ul>
          </div>
    </div>
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
						<input type="hidden" id="fees_discount_id" name="fees_discount_id" value="<?php if(!empty($details[0]->fees_discount_id)){echo $details[0]->fees_discount_id;}?>" />
						
						<div class="control-group form-group">
							<label class="control-label"><span>Child Care/Activities</span></label>
							<div class="controls" style="margin-top:20px;">
								<label for="is_childcare_activity1">
									<input type="radio" class="is_childcare_activity" name="is_childcare_activity" value="Child Care" id="is_childcare_activity1" <?php echo (!empty($details[0]->is_childcare_activity) &&  ($details[0]->is_childcare_activity == 'Child Care')) ? "checked" : ""?>>Child Care
								</label>&nbsp;&nbsp;
								
								<?php 
									if(isset($details[0]->is_childcare_activity) && !empty($details[0]->is_childcare_activity)){
								?>
								<label for="is_childcare_activity2">
									<input type="radio" class="is_childcare_activity" name="is_childcare_activity" value="Activities" id="is_childcare_activity2" <?php echo (!empty($details[0]->is_childcare_activity) &&  ($details[0]->is_childcare_activity == 'Activities')) ? "checked" : ""?>>Activities
								</label>&nbsp;&nbsp;
								<?php }else{
									?>
								<label for="is_childcare_activity3">
									<input type="radio" class="is_childcare_activity" name="is_childcare_activity" id="is_childcare_activity3" value="Activities" <?php echo (!empty($details[0]->is_childcare_activity) &&  ($details[0]->is_childcare_activity == 'Activities')) ? "checked" : ""?> checked="checked" >Activities
								</label>&nbsp;&nbsp;
								<?php
									}
								?>
							</div>
						</div>

						<div class="control-group form-group class_div">
							<label class="control-label" for="category_id">Month*</label>
							<div class="controls">
							<select id="month_id" name="month_id" class="form-control">
								<option value=""  disabled='disabled'>Select Zone</option>
								<?php 
									if(isset($months) && !empty($months)){
										foreach($months as $cdrow){
											$sel = ($cdrow->month_id == $details[0]->month_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->month_id;?>" <?php echo $sel; ?>><?php echo $cdrow->name;?></option>
								<?php }}?>
							</select>
						</div>
						</div>
						
						<div class="control-group form-group">
							<label class="control-label"><span>Discount in %*</span></label>
							<div class="controls">
								<input type="text" class="form-control required number_only" placeholder="Enter Discount in %" id="discount" name="discount" value="<?php if(!empty($details[0]->discount)){echo $details[0]->discount;}?>">
							</div>
						</div>
						<div style="clear:both; margin-bottom: 2%;"></div>
						
						<div class="form-actions form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
							<a href="<?php echo base_url();?>feesdiscountmaster" class="btn btn-primary">Cancel</a>
						</div>
					</form>
                </div>
            <div class="clearfix"></div>
            </div>
         </div>
    </div>        
	</div><!-- end: Content -->								
<script>
$('document').ready(function(){
	$(".number_only").keypress(function (e) {
    	if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
      	return false;
    	}
  	});
})
var vRules = {
	month_id:{required:true},
	discount:{required:true}
	
};
var vMessages = {
	month_id:{required:"Please enter select month name."},
	discount:{required:"Please enter discount."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>feesdiscountmaster/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>feesdiscountmaster";
					},2000);

				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			},
			error:function(){
				$("#msg_display").html("");
				$("#display_msg").html("");
				displayMsg("error",res['msg']);
			}
		});
	}
});


document.title = "AddEdit - Fees Discount Master";

 
</script>					
