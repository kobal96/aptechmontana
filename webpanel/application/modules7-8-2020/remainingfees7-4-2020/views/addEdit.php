<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Fee Payment Process</h1>            
          </div>
    </div>
    <div class="card"> 
	    <div id='loadingmessage' style='display:none'>
	        <img src='<?php echo base_url("images/loading.gif"); ?>'/>
	    </div>      
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="remainingFeesForm" method="post" enctype="multipart/form-data">
						<div class="col-md-6 control-group">
							<label class="control-label">Academic Year*</label>
							<div class="controls">
								<select id="academic_year_id" name="academic_year_id" class="form-control" onchange="getCenterStudent(this.value)">
									<option value="">Select Academic Year</option>
									<?php 
										if(isset($academicyear) && !empty($academicyear)){
											foreach($academicyear as $cdrow){
									?>
										<option value="<?php echo $cdrow->academic_year_master_id;?>"><?php echo $cdrow->academic_year_master_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						<div class="col-md-6 control-group" >
							<label class="control-label" for="zone_id">Zone*</label>
							<div class="controls">
								<select id="zone_id" name="zone_id" class="form-control"  onchange="getCenters(this.value);getCenterStudent(this.value)" >
									<option value="">Select Zone</option>
									<?php 
										if(isset($zones) && !empty($zones)){
											foreach($zones as $cdrow){
									?>
									<option value="<?php echo $cdrow->zone_id;?>"><?php echo $cdrow->zone_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						<div class="col-md-6 control-group">
							<label class="control-label"><span>Center*</span></label> 
							<div class="controls">
								<select id="center_id" name="center_id" class="select2" onchange="getCenterStudent(this.value)" style="width:98%;border:1px solid #cdcdcd;">
									<option value="">Select Center</option>
								</select>
							</div>
						</div>
						<div class="col-md-6 control-group">
							<label class="control-label">Select Student*</label>
							<div class="controls">
								<select id="student_id" name="student_id" class="select2"  onchange="getStudentDetails(this.value);" style="width:98%;border:1px solid #cdcdcd;">
									<option value="">Select Student</option>
								</select>
							</div>
						</div>
						<div class="clearfix"></div>
						<div id="tabwrapper" class="col-md-12">
							<ul  class="nav nav-pills" style="margin-top:25px;">
								<li class="active"><a  href="#1a" data-toggle="tab"><i class="fa fa-clock-o"></i> Pay Remaining Fees</a></li>
								<li><a onclick="assignFeesModel()" href="javascript:void(0);"><i class="fa fa-plus"></i>  Assign New Fee</a></li>
							</ul>
							<div class="tab-content clearfix">
								<div class="tab-pane active" id="1a" >
									<div class="ispaymentdone text-center">
										<div class="alert alert-success" role="alert">
											<h4 style="margin-bottom:0px;">All Assigned fees has been paid</h4>
										</div>
									</div>
									<div class="row form-group">
										<div class="col-md-6 control-group">
											<label class="control-label"><span>Total Of Fees</span></label>
											<div class="controls">
												<input type="text" class="form-control" name="fees_total_amount" value="" readonly="readonly" id="fees_total_amount">
											</div>
										</div>
										<!-- <div class="col-md-6 control-group">
											<label class="control-label"><span>Total Discount Amount</span></label>
											<div class="controls">
												<input type="text" class="form-control" name="fees_total_amount" value="" readonly="readonly" id="fees_total_amount">
											</div>
										</div> -->
										<div class="col-md-6 control-group">
											<label class="control-label"><span>Total Of Fees Paid</span></label>
											<div class="controls">
												<input type="text" class="form-control" name="fees_amount_collected" value="" readonly="readonly" id="fees_amount_collected">
											</div>
										</div>
										<div class="col-md-6 control-group">
											<label class="control-label"><span>Total Of Pending Fees</span></label>
											<div class="controls">
												<input type="text" class="form-control" name="fees_remaining_amount" value="" readonly="readonly" id="fees_remaining_amount">
											</div>
										</div>
									</div>
									<table class="table table-striped payment_detail_show" style="border: 1px">
										<tr>
											<th>Category Name</th>
											<th>Course Name / Group Name</th>
											<th>installment Due Date</th>
											<th>Installment Fees</th>
											<th>Collected Amount</th>
											<th>Fee Payment<span class="text-danger">*</span></th>
											<th>Remaining Installment Fees</th>
										</tr>
										<tbody class="c_table"></tbody>
										<tfoot>
											<tr>
												<td colspan="3">&nbsp;</td>
												<td class="text-right" id="component_installment_fees_total"></td>
												<td class="text-right"  id="component_collected_amount_total"></td>
												<td class="text-right" id="component_fees_payment_amount"></td>
												<td class="text-right" id="component_remaining_fees_amount"></td>
											</tr>
										</tfoot>
									</table>
									<input type="hidden" name="collected_amount_total" id="collected_amount_total" value="">
									<div class="clearfix"></div>
									<div class="ispaymentnotdone">
										<!-- payment mode -->
										<h3>Receipt Details</h3>
										<div class="row form-group">
											<div class="col-md-6 control-group">
												<label for="">Receipt Date</label>
												<input type="text" class="form-control datepicker required" id="receipt_date" name="receipt_date" value="" title="Please select receipt date.">
											</div>
											<div class="col-md-6 control-group">
												<label for="">Issue Receipt for</label>
												<label for="guardianfather">
													<input type="radio" class="required receipt_for" id="guardianfather" name="receipt_for" value="father" title="Please select reciept behalf.">Father
												</label>
												<label for="guardianmother">
													<input type="radio" class="required receipt_for" id="guardianmother" name="receipt_for" value="mother" title="Please select reciept behalf.">Mother
												</label>
												<label for="guardianguardian">
													<input type="radio" class="required receipt_for" id="guardianguardian" name="receipt_for" value="guardian" title="Please select reciept behalf.">Guardian
												</label>
												<div class="controls" >
													<input type="text" class="form-control required" name="receipt_for_name" id="receipt_for_name" value="" readonly title = "This Value is Required.">
												</div>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-md-6 control-group">
												<label class="control-label"><span>Payment Mode</span></label>
												<div class="controls">
													<label for="payment_cheque">
														<input type="radio" id="payment_cheque" class="payment_mode" name="payment_mode" value="Cheque"  checked="checked">Cheque
													</label>&nbsp;&nbsp;
													<label for="payment_cash">
														<input type="radio" id="payment_cash" class="payment_mode" name="payment_mode" value="Cash"  >Cash
													</label>&nbsp;&nbsp;
													<label for="payment_netbanking">
														<input type="radio" id="payment_netbanking" class="payment_mode" name="payment_mode" value="Netbanking" >Netbanking
													</label>
												</div>
											</div>
											<div class="col-md-6 control-group both">
												<label class="control-label"><span>Bank Name*</span></label>
												<div class="controls">
													<input type="text" class="form-control" name="bank_name" value="" id="bank_name">
												</div>
											</div>
										</div>
										<div class="row form-group cheque">
											<div class="col-md-6 control-group">
												<label class="control-label"><span>Cheque No*</span></label>
												<div class="controls">
													<input type="text" class="form-control onlyNumber" name="cheque_no" value="" id="cheque_no" min="0">
												</div>
											</div>
											<div class="col-md-6 control-group">
												<label class="control-label"><span>Cheque Date*</span></label>
												<div class="controls">
													<input type="text" class="form-control datepicker" placeholder="Select transaction_date date" id="transaction_date" name="cheque_date" value="">
												</div>
											</div>
										</div>
										<div class="row form-group transaction">
											<div class="col-md-6 control-group cheque transaction">
												<label class="control-label"><span>Transaction No*</span></label>
												<div class="controls">
													<input type="text" class="form-control" name="transaction_id" value="" id="transaction_id" >
												</div>
											</div>
											<div class="col-md-6 control-group">
												<label class="control-label"><span>Transaction Date*</span></label>
												<div class="controls">
													<input type="text" class="form-control datepicker" placeholder="Select transaction_date date" id="transaction_date" name="transaction_date" value="">
												</div>
											</div>
										</div>
										<div class="row form-group">
											<div class="col-md-6 control-group">
												<label class="control-label"><span>Receipt Amount*</span></label>
												<div class="controls">
													<input type="text" class="form-control required" placeholder="Total Collected Amount" id="total_transaction_amount" name="total_transaction_amount" value="" title="Please enter the Receipt amount.">
												</div>
											</div>
										</div>

										<!-- payment mode -->
										<div class="row form-group ">
											<div class="col-md-6 control-group">
												<label class="control-label"><span>Fees Remark</span></label>
												<div class="controls">
													<textarea class="form-control" name="fees_remark" value="" placeholder="Fees Remark" id="fees_remark"></textarea>
												</div>
											</div>
											<div class="col-md-6 control-group">
												<label class="control-label"><span>Fees Accepted By</span></label>
												<div class="controls">
													<input type="text" class="form-control" name="fees_approved_accepted_by_name" value="<?php echo $login_name;?>" readonly="readonly">
												</div>
											</div>
											<input type="hidden" name="admission_fees_id" id="admission_fees_id">
										</div>
										<div class="row form-group">
										</div>
										<div style="clear:both; margin-bottom: 2%;"></div>
										<div class="form-actions"  style="padding:20px;text-align:center;">
											<button type="submit" class="btn btn-primary">Save</button>
											<a href="<?php echo base_url();?>remainingfees" class="btn btn-primary">Cancel</a>
											<div class="clearfix"></div>
										</div>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						</div>
					</form>
                </div>
            	<div class="clearfix"></div>
            </div>
		</div>
	</div>      
	<!-- receipt listing start-->
	<h2>Receipts</h2>
	<div class="card"> 										
		<div class="col-sm-12">
         	<div class="box-content form-horizontal product-filter">            	
				<div class="col-sm-4 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Name/Receipt No.</label>
						<input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
					</div>
				</div>
				<div class="control-group clearFilter">
					<div class="controls">
						<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<div class="box-content">
			<div class="col-sm-12">
				<div class="table-responsive scroll-table">
					<table class="dynamicTable display table table-bordered non-bootstrap">
						<thead>
							<tr>
								<th>Receipt Date</th>
								<th>Center Name</th>
								<th>Receipt No.</th>
								<th>ENR No.</th>
								<th>Student Name</th>
								<th>Amount</th>
								<th>Transaction Details</th>
								<th>Status</th>
								<th data-bSortable ="false">Cancel/Clear Date</th>
								<th data-bSortable ="false">Action</th>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot></tfoot>
					</table>        
				</div>
			</div>
			<div class="clearfix"></div>
		</div>											
	</div>
	<!-- receipt listing end-->
</div><!-- end: Content -->			

<!-- pdf view modelbox -->
<div id="receiptView" class="modal fade" data-backdrop="static"  data-keyboard="false" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" style="max-height:750px;">
		<div class="col-sm-12">
			<table style="display: inline-block; float: left;" align="center"; width="50%">
				<tr>
					<th style="padding:10px;text-align:left;font-size:14px">Reciept Number:<th>
					<td id="receipt_no"></td>
				</tr>
				<tr>
					<th style="padding:10px;text-align:left;font-size:14px">Recipient Name:<th>
					<td id="recipient_name"></td>
				</tr>
				<tr>
					<th style="padding:10px;text-align:left;font-size:14px">Student Name:<th>
					<td id="student_name"></td>
				</tr>
			</table>
			<table style="display: inline-block; float: right;" ; width="50%">
				<tr>
					<th style="padding:10px;text-align:left;font-size:14px">Reciept Status:<th>
					<td id="receipt_status"></td>
				</tr>
				<tr>
					<th style="padding:10px;text-align:left;font-size:14px">Reciept Date:<th>
					<td id="receipts_date"></td>
				</tr>
				<tr>
					<th style="padding:10px;text-align:left;font-size:14px">Clear/Cancel Date:<th>
					<td id="issue_date"></td>
				</tr>								
			</table>
		</div>
        <iframe src="" frameborder="0" id="receiptUrl" width="100%" style="height:570px;float:left"></iframe>
		<div class="clearfix"></div>
      </div>
    </div>
	<div class="clearfix"></div>
  </div>
</div>

<div id="receiptViewfirst" class="modal fade" data-backdrop="static"  data-keyboard="false" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body" style="max-height:750px;">
        <iframe src="" frameborder="0" id="receiptUrlfirst" width="100%" style="height:570px;float:left"></iframe>
		<div class="clearfix"></div>
      </div>
    </div>
	<div class="clearfix"></div>
  </div>
</div>

<!-- assign fees modelbox -->
<div id="assignFeesModel" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" style="width: 80%;" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick = "closeFeesModel()" >&times;</button>
		<h4>Assign New Fees</h4>
      </div>
      <div class="modal-body" id="feesModelBody"style="max-height:600px;overflow-y:auto;"></div>
    </div>
  </div>
</div>

<!-- Change status -->
<div id="changeStatusModel" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog modal-lg" >
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" onclick = "closeChangeStatusModel()" >&times;</button>
		<h4>Change Status</h4>
      </div>
      <div class="modal-body" id="changeStatusBody"style="max-height:600px;overflow-y:auto;"></div>
    </div>
  </div>
</div>
<script>
var mother_name  = "";
var father_name  = "";
var parent_name  = "";
 $(document).on('keypress','.onlyNumber',function(event){
	if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
		event.preventDefault(); 
	}
});
// call assign fees
function assignFeesModel(){
	console.log('assignFeesModel');
	var student_id = $("#student_id").val();
	var academic_year_id = $("#academic_year_id").val();
	$.ajax({
		url:"<?php echo base_url();?>remainingfees/feeAssingFeesAddedit",
		data:{student_id:student_id,academic_year_id:academic_year_id},
		dataType:"html",
		method:"post",
		success:function(response){
			$("#feesModelBody").html(response);
			$("#assignFeesModel").modal("show");
		}
	})
}

// call change status receipt
function changeStatusModel(reciept_id){
	$.ajax({
		url:"<?php echo base_url();?>remainingfees/changeReceiptStatus",
		data:{reciept_id:reciept_id},
		dataType:"html",
		method:"post",
		success:function(response){
			$("#changeStatusBody").html(response);
			$("#changeStatusModel").modal("show");
		}
	})
}


$('#assignFeesModel').on('hidden.bs.modal', function () {
	var academic_year_id = $("#academic_year_id").val()
	var zone_id = $("#zone_id").val()
	var center_id = $("#center_id").val()
	var student_id = $("#student_id").val()
	getStudentDetails(academic_year_id,zone_id,center_id,student_id);
	$('#loadingmessage').hide();
})

// receipt view model function
$(document).on("click",".view-receipt",function(){
	var receiptUrl = $(this).attr("receiptUrl");
	var receipno = $(this).attr("reciept_no");
	var recipientname = $(this).attr("recipientname");
	var studentname = $(this).attr("studentname");
	var receiptstatus = $(this).attr("receiptstatus");
	var receiptdate = $(this).attr("receiptdate");
	var issuedate = $(this).attr("issuedate");
	$("#receipt_no").html(receipno);
	$("#recipient_name").html(recipientname);
	$("#student_name").html(studentname);
	$("#receipt_status").html(receiptstatus);
	$("#receipts_date").html(receiptdate);
	$("#issue_date").html(issuedate);

	$("#receiptUrl").attr("src",receiptUrl);
	$('#receiptView').modal('show');
})

$( document ).ready(function() {
	$('.cheque').hide();
	$('.transaction').hide();
	$('.both').hide();
	$('.payment_mode').trigger("change");
});

$('.payment_mode').change(function(){
	// var payment_mode = $(this).val();
	var payment_mode = $("input[name='payment_mode']:checked"). val();
	if(payment_mode == 'Cheque'){
		$('.cheque').show();
		$('.transaction').hide();
		$('.both').show();
	}
	else if(payment_mode == 'Netbanking'){
		$('.cheque').hide();
		$('.transaction').show();
		$('.both').show();
	}
	else{
		$('.cheque').hide();
		$('.transaction').hide();
		$('.both').hide();
	}
})

$('.receipt_for').change(function(){
	var receipt_for_value = $("input[name='receipt_for']:checked"). val();
	if(receipt_for_value == "mother"){
		$("#receipt_for_name").val(mother_name);
	}else if(receipt_for_value == "father"){
		$("#receipt_for_name").val(father_name);
	}else{
		$("#receipt_for_name").val(parent_name);
	}
});

$(".datepicker").datetimepicker({
	format: 'DD-MM-YYYY',
});

$(document).on('keypress','.number_decimal_only',function(e){
    // if (e.which != 8  && e.which != 46 && (e.which < 48 || e.which > 57)) {
    //     return false;
    // }
})

$( document ).ready(function() {
	$('.payment_detail_show').hide()
	$('.group_show').hide()
	$('.feestype').change(function(){
		$("#student_id").html("<option value=''>Select</option>");
		$("#select2-chosen-1").text("");
		$(".c_table").children().remove();
		if($('.feestypeGroup').is(':checked') == true){
			$('.group_show').show()
			$('.class_show').hide()
		}
		else{
			$('.group_show').hide()
			$('.class_show').show()
			$("#group_id").val("");
			let academic_year_id = $('#academic_year_id').val()
			let zone_id = $('#zone_id').val()
			let center_id = $('#center_id').val()
			getCenterStudent(academic_year_id,zone_id,center_id)
		}
	})
});

$(document).on('keypress','.number_decimal_only',function(e){
    if (e.which != 8  && e.which != 46 && (e.which < 48 || e.which > 57)) {
        return false;
    }
})

function getCenters(zone_id,center_id = null){
    var academic_year_id =  $("#academic_year_id").val();
	if(zone_id != ""  || zone_id != undefined){
		$.ajax({
			url:"<?php echo base_url();?>remainingfees/getCenters",
			data:{zone_id:zone_id, center_id:center_id,academic_year_id:academic_year_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#center_id").html("<option value=''>Select</option>"+res['option']);
					}else{
						$("#center_id").html("<option value=''>Select</option>");
					}
				}else{	
					$("#center_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getCenterStudent(academic_year_id,zone_id,center_id){
	academic_year_id = $('#academic_year_id').val()
	zone_id = $('#zone_id').val()
	center_id = $('#center_id').val()
	$("#student_id").html("<option value=''>Select</option>").val(null).trigger('change.select2');
	if($('.feestypeGroup').is(':checked') == true){
		group_id = $('#group_id').val()
		getGroupStudent(academic_year_id,zone_id,center_id,group_id)
	}else{
		if(zone_id != ""  || zone_id != undefined || center_id != ""  || center_id != undefined || academic_year_id != ""  || academic_year_id != undefined)
		{
			$.ajax({
				url:"<?php echo base_url();?>remainingfees/getCenterStudent",
				data:{zone_id:zone_id, center_id:center_id,academic_year_id:academic_year_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != "")
						{
							$("#student_id").html("<option value=''>Select</option>"+res['option']);
						}
						else
						{
							$("#student_id").html("<option value=''>Select</option>");
						}
					}
					else
					{	
						$("#student_id").html("<option value=''>Select</option>");
					}
				}
			});
		}
	}
}

function getGroupStudent(academic_year_id,zone_id,center_id,group_id){
	academic_year_id = $('#academic_year_id').val()
	zone_id = $('#zone_id').val()
	center_id = $('#center_id').val()
	group_id = $('#group_id').val()
	if(zone_id != ""  || zone_id != undefined || center_id != ""  || center_id != undefined || academic_year_id != ""  || academic_year_id != undefined || group_id != ""  || group_id != undefined){
		$.ajax({
			url:"<?php echo base_url();?>remainingfees/getGroupStudent",
			data:{zone_id:zone_id, center_id:center_id,academic_year_id:academic_year_id,group_id:group_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#student_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#student_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#student_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getStudentDetails(academic_year_id,zone_id,center_id,student_id){
	$(".c_table").html("");
	academic_year_id = $('#academic_year_id').val()
	zone_id = $('#zone_id').val()
	center_id = $('#center_id').val()
	student_id = $('#student_id').val()
	if(student_id != ""){
		$("#tabwrapper").slideDown();
		if($('.feestypeGroup').is(':checked') == true){
			$('#type').val('Group')
		}else{
			$('#type').val('Class')
		}
		let type = $('#type').val()
		if(zone_id != ""  || zone_id != undefined || center_id != ""  || center_id != undefined || academic_year_id != ""  || academeic_year_id != undefined || student_id != ""  || student_id != undefined){
			$.ajax({
				url:"<?php echo base_url();?>remainingfees/getStudentDetails",
				data:{zone_id:zone_id, center_id:center_id,academic_year_id:academic_year_id,student_id:student_id,type:type},
				dataType: 'json',
				method:'post',
				success: function(res){
					if(res['status']=="success"){
						$('.payment_detail_show').show()
						var admission_fees_ids = [];
						var count = 0;
						var selected_receipt_for = res['student_details'][0]['guardian'];
						$("input[value='"+selected_receipt_for+"']").attr("checked",true);
						mohter_name = res['student_details'][0]['mother_name'];
						father_name = res['student_details'][0]['father_name'];
						parent_name = res['student_details'][0]['parent_name'];
						if(selected_receipt_for == "mother"){
							$("#receipt_for_name").val(res['student_details'][0]['mother_name']);
						}else if(selected_receipt_for == "father"){
							$("#receipt_for_name").val(res['student_details'][0]['father_name']);
						}else{
							$("#receipt_for_name").val(res['student_details'][0]['parent_name']);
						}

						for(let i=0;i<res['option'].length;i++){
							$tr_html = "";
							admission_fees_ids.push(res['option'][i]['admission_fees_id']);

							for(let c=0;c<res['option'][i]['instalment_details'].length;c++){
								let placeholder = "";
								var readonlyFlag = "";
								placeholder = "0.00";
								if(res['option'][i]['instalment_details'][c]['instalment_remaining_amount'] == 0){
									placeholder = "PAID";
									readonlyFlag = "readonly";
								}
								var class_group_id = (res['option'][i]['course_id'] != 0 )?res['option'][i]['course_id']:res['option'][i]['group_id'];
								$tr_html ='<tr actual_due_date ="'+res['option'][i]['instalment_details'][c]['actual_due_date']+'" >';
										if(res['option'][i]['fees_id'] == 21){
								$tr_html +=	'<td colspan="3">Cheque Return Charges │ V1-48 │ X </td>';
										}else{
								$tr_html +=	'<td>'+res['option'][i]['instalment_details'][c]['categoy_name']+'</td>'+
											'<td>'+((res['option'][i]['instalment_details'][c]['course_name']!="")?res['option'][i]['instalment_details'][c]['course_name']:res['option'][i]['instalment_details'][c]['group_name'])+'</td>'+
											'<td>'+res['option'][i]['instalment_details'][c]['instalment_due_date']+'</td>';
										}
								$tr_html +=	'<td>'+
												'<div class="form-group">'+
													'<input type="text" class="form-control instalment_amount text-right"  id="installment_amnt_'+i+'_'+c+'"name="instalment_amount['+res['option'][i]['fees_type']+']['+res['option'][i]['instalment_details'][c]['admission_fees_id']+']['+c+']" readonly="readonly" value="'+res['option'][i]['instalment_details'][c]['instalment_amount']+'">'+
												'</div>'+
											'</td>'+
											'<td>'+
												'<div class="form-group">'+
													'<input type="text" class="form-control instalment_amount text-right"  id="collected_amount_'+i+'_'+c+'" name="instalment_collected_amount['+res['option'][i]['fees_type']+']['+res['option'][i]['instalment_details'][c]['admission_fees_id']+']['+c+']" readonly="readonly" value="'+res['option'][i]['instalment_details'][c]['instalment_collected_amount']+'">'+
												'</div>'+
											'</td>'+
											'<td>'+
												'<div class="form-group">'+
													'<input type="text" class="form-control number_decimal_only installment_collected_amount text-right"  id="id_'+i+'_'+c+'" name="installment_collected_amount['+res['option'][i]['fees_type']+']['+res['option'][i]['instalment_details'][c]['admission_fees_id']+']['+c+']" value="" placeholder="'+placeholder+'" '+readonlyFlag+'>'+
												'</div>'+
												'<span class="text-danger" id="amounterror_'+i+'_'+c+'"></span>'+
											'</td>'+
											'<td>'+
												'<div class="form-group">'+
													'<input type="text" class="form-control installment_remaining_amount text-right"  id="remaining_id_'+i+'_'+c+'" name="installment_remaining_amount['+res['option'][i]['fees_type']+']['+res['option'][i]['instalment_details'][c]['admission_fees_id']+']['+c+']" readonly="readonly" value="'+res['option'][i]['instalment_details'][c]['instalment_remaining_amount']+'">'+
												'</div>'+
											'</td>'+
											'<td>'+
												'<div class="form-group">'+
													'<input type="hidden" class="form-control"  id="admission_fees_instalment_id_'+i+'_'+c+'" name="admission_fees_instalment_id['+res['option'][i]['fees_type']+']['+res['option'][i]['instalment_details'][c]['admission_fees_id']+']['+c+']" value="'+res['option'][i]['instalment_details'][c]['admission_fees_instalment_id']+'">'+
													'<input type="hidden" id="admission_fees_instalment_actual_id_'+i+'_'+c+'" name="admission_fees_instalment_actual_id['+count+']" value="'+res['option'][i]['instalment_details'][c]['admission_fees_instalment_id']+'">'+
													'<input type="hidden" id="installment_collected_actual_amount_'+i+'_'+c+'" name="installment_collected_actual_amount['+count+']" value = "">'+
												'</div>'+
											'</td><input type="hidden" name="admission_fees_typewise_id['+res['option'][i]['fees_type']+']['+res['option'][i]['instalment_details'][c]['admission_fees_id']+']['+c+']" value="'+class_group_id+'">'+
											'<input type="hidden" name="admission_fees_actual_id['+count+']" value="'+res['option'][i]['admission_fees_id']+'">'+
										'</tr>';
										count++;
										$(".c_table").append($tr_html);
										$(document).on("keyup",'#id_'+i+'_'+c,function(){
											let collected_amount = Number($('#collected_amount_'+i+'_'+c).val());
											let paying_amount = Number($('#id_'+i+'_'+c).val() )
											let amount = Number($('#installment_amnt_'+i+'_'+c).val())
											$('#id_'+i+'_'+c).val(paying_amount)
											let remaining_amount = $('#remaining_id_'+i+'_'+c).val()
											let installment_amnt = $('#installment_amnt_'+i+'_'+c).val()
											let total_minus_amount = parseFloat(collected_amount) + parseFloat(paying_amount)
											if(Number(total_minus_amount) > Number(installment_amnt)){
												// $('#amounterror_'+c).text('Paying Amount grater than installment amount')
												let installment_remaining_amount = (amount - collected_amount)
												$('#remaining_id_'+i+'_'+c).val(installment_remaining_amount)
												$('#id_'+i+'_'+c).val('')

											}else{
												if(paying_amount == 0){
													$('#id_'+i+'_'+c).val('')
												}
												let installment_remaining_amount = (amount - total_minus_amount)
												$('#remaining_id_'+i+'_'+c).val(installment_remaining_amount.toFixed(2))
												$('#amounterror_'+i+'_'+c).text('')
											}
											var temp = 0;
											$(".installment_collected_amount").each(function(){
												if($(this).val() != ""){
													temp = (parseFloat(temp) + parseFloat($(this).val()));
												}
											})
											$("#collected_amount_total").val(temp);
											$("#component_fees_payment_amount").text(parseFloat(temp).toFixed(2));
											$component_remaining_fees_amount = ($('#fees_remaining_amount').val() - temp)
											$("#component_remaining_fees_amount").text(parseFloat($component_remaining_fees_amount).toFixed(2));
											$('#installment_collected_actual_amount_'+i+'_'+c).val($(this).val());
										})
								
							}
						}
						reorderTable()
						$('#admission_fees_id').val(admission_fees_ids.join());
						$('#fees_total_amount').val(res['total_details']['fees_total_amount'])
						$('#fees_amount_collected').val(res['total_details']['total_collected_amount'])
						$('#fees_remaining_amount').val(res['total_details']['total_remaining_amount']);
						$("#component_installment_fees_total").text(parseFloat(res['total_details']['fees_total_amount']).toFixed(2));
						$("#component_collected_amount_total").text(parseFloat(res['total_details']['total_collected_amount']).toFixed(2));
						$("#component_remaining_fees_amount").text(parseFloat(res['total_details']['total_remaining_amount']).toFixed(2));
						$("#component_fees_payment_amount").text(parseFloat(0).toFixed(2));
						if(res['total_details']['total_remaining_amount'] == 0){
							$(".ispaymentnotdone").hide();
							$(".ispaymentdone").show();
						}else{
							$(".ispaymentnotdone").show();
							$(".ispaymentdone").hide();
						}
						
					}else{
						$('.payment_detail_show').hide()
					}
				}
			});
		}
	}else{
		$("#tabwrapper").slideUp();
	}
}
//reorder table
function reorderTable() {
  var rows = $(".c_table").find('tr').toArray().sort(comparer($(this).index()));
  this.asc = !this.asc;
  if (!this.asc) {
    rows = rows.reverse();
  }
  $(".c_table").empty().html(rows);
}
function comparer(index) {
  return function(a, b) {
	var valA = $(a).attr("actual_due_date"),
      valB = $(b).attr("actual_due_date");
    return $.isNumeric(valA) && $.isNumeric(valB) ?
      valA - valB : valA.localeCompare(valB);
  };
}

var vRules = {
	academic_year_id : {required:true},
	zone_id :  {required:true},
	center_id :  {required:true},
	group_id :  {required:true},
	student_id:{required:true},
	// fees_remark:{required:true},
	payment_mode:{required:true},
	
};

var vMessages = {
	academic_year_id:{required:"Please select academic year."},
	zone_id:{required:"Please select zone."},
	center_id:{required:"Please select center."},
	group_id:{required:"Please select group."},
	student_id:{required:"Please select student."},
	// fees_remark:{required:"Please enter fees remark."},
	payment_mode:{required:"Please select payment mode"},
};

$("#remainingFeesForm").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		$('#loadingmessage').show();
		var act = "<?php echo base_url();?>remainingfees/submitRemainingFees";
		var receipt_date = $("#receipt_date").val();
		var recipt_for = $("input[name='receipt_for']:checked").val();
		$("#remainingFeesForm").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1"){
					let admission_fees_id = res['admission_fees_id'];
					let student_id = $("#student_id").val();
					let academic_year_id = $("#academic_year_id").val();
					$.ajax({
						url:"<?php echo base_url();?>remainingfees/getPayemtDetails",
						data:{admission_fees_id:admission_fees_id,student_id:student_id,receipt_date:receipt_date,recipt_for:recipt_for,academic_year_id:academic_year_id},
						dataType: 'json',
						method:'post',
						success: function(data){
							if(data['status_code'] == 200){
								$('#loadingmessage').hide();
								$("#receiptUrlfirst").attr("src",data['body']);
								$('#receiptViewfirst').modal('show');
								var academic_year_id = $("#academic_year_id").val()
								var zone_id = $("#zone_id").val()
								var center_id = $("#center_id").val()
								getStudentDetails(academic_year_id,zone_id,center_id,student_id);
								clearSearchFilters();
								setTimeout(function(){
									document.getElementById("remainingFeesForm").reset();
									$("#student_id").html("<option value=''>Select</option>").val(null).trigger('change.select2');
								}, 3000);
							}else{
								$('#loadingmessage').hide();
							}
						}
					});
				}else{
					$('#loadingmessage').hide();
					displayMsg("error",res['msg']);
				}
			}
		});
	}
});

document.title = "Fees Payment Process";
</script>

<style>
	.admission_tab{
		border:1px solid #ecf0f5;
		min-height:20px !important;
	}
	.admission_tab .custome-tab-nav{
		background:#cdcdcd;
	}
	.admission_tab .tab-content{
		padding: 30px 25px 10px 35px !important;
	}
	.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus{
		color: #fff;
		background-color: #3c8dbc !important;
		border-radius:0px !important;
	}
	.nav-pills>li>a{
		color:#fff;
		font-size:16px;
	}
	.nav-pills>li.active:after{
		content: '';
		position: absolute;
		top: 100%;
		left: 50%;
		margin-left: -10px;
		width: 0;
		height: 0;
		border-top: solid 10px #3c8dbc;
		border-left: solid 10px transparent;
		border-right: solid 10px transparent;
	}
	.nav-pills>li>a:hover, .nav-pills>li>a:focus {
		border-radius:0px !important;
	}
	.nav.nav-pills>li>a {
		position: relative;
		display: block;
		padding: 10px 20px;
		text-align:center;
	}
	.nav.nav-pills>li{
		padding-left: 0px;
		padding-right:0px;
		background-color: #3c8dbc !important;
	}
	.tab-content{
		padding: 20px !important;
    	border: 2px dashed #3c8dbc;	
	}
	#tabwrapper{
		display:none;
	}
	.group-row{
		padding:20px;
		/* text-align:center; */
		margin: 0px auto;
		padding-top:50px;
	}
	#groupTable>tbody>tr{
		border: 4px dashed #3c8dbc !important;
	}
	.removeGroup{
		position:absolute;
		top: 0;
		right: 0;
	}
	.group-count-cls{
		position: absolute !important;
		top: 0 !important;
		background: #cdcdcd !important;
		padding: 10px 20px;
		border-radius: 20px;
		left: 0 !important;
	}
	.group-count-cls span{
		padding: 3px 10px;
		background: #3c8dbc;
		color: #fff;
		border-radius: 20px;
	}
</style>

