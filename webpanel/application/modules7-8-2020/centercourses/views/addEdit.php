<?php 
//error_reporting(0);
$record_id = $_GET['id'];
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Center - Category Courses</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>centercourses">Center - Category Courses</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="center_id" name="center_id" value="<?php echo $record_id;?>" />
								
								<div class="control-group form-group">
									<label class="control-label" for="category_id">Category*</label>
									<div class="controls">
										<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);">
											<option value="">Select Category</option>
											<?php 
												if(isset($categories) && !empty($categories)){
													foreach($categories as $cdrow){
														$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
											?>
												<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
											<?php }}?>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label" for="course_id">Courses*</label>
									<input type="checkbox" id="checkbox" >Select All
									<div class="controls">
										<select id="course_id" name="course_id[]" class="form-control select2" multiple>
											<option value="" disabled>Select Courses</option>
										</select>
									</div>
								</div>
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="javascript:history.go(-1)" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>
$( document ).ready(function() {
	
	$("#checkbox").click(function(){
		if($("#checkbox").is(':checked') ){
			$("#course_id > option").prop("selected","selected");
			$("#course_id").trigger("change");
		}else{
			$("#course_id > option").removeAttr("selected");
			$("#course_id").trigger("change");
		}
	});
	
	<?php 
		if(!empty($details[0]->center_id)){
	?>
		getCourses('<?php echo $details[0]->category_id; ?>');
	<?php }?>
	
	
	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						//['Source', 'Templates'],
						['Cut','Copy','Paste','PasteText','-','Print', 'SpellChecker', 'Scayt'],
						['Subscript','Superscript'],
						['NumberedList','BulletedList'],
						['BidiLtr', 'BidiRtl' ],
						//['Maximize', 'ShowBlocks'],
						['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						//['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						//['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
	
	$.validator.addMethod("needsSelection", function(value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });
	
});

function getCourses(category_id)
{
	//alert("center_id: "+center_id);return false;
	if(category_id != "" )
	{
		//alert();
		$.ajax({
			url:"<?php echo base_url();?>centercourses/getCourses",
			data:{category_id:category_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#course_id").html("<option value='' disabled>Select Course</option>"+res['option']);
						$("#course_id").select2();
					}
					else
					{
						$("#course_id").html("<option value='' disabled>Select Course</option>");
						$("#course_id").select2();
					}
				}
				else
				{	
					$("#course_id").html("<option value='' disabled>Select Course</option>");
					$("#course_id").select2();
				}
			}
		});
	}
}

var vRules = {
	category_id:{required:true},
	course_id:{required:true}
	
};
var vMessages = {
	category_id:{required:"Please select zone."},
	course_id:{required:"Please enter center name."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>centercourses/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						//window.location = "<?php echo base_url();?>centermaster";
						//window.location.reload();
						history.go(-1);
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Center";

 
</script>					
