<?php 
/*echo "<pre>";
print_r($result);
exit;*/
?>
<!-- start: Content -->
<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1>Feedback Details</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>feedbacks"></a>Feedbacks</li>
        </ul>
      </div>
    </div>    
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
            	<div class="col-sm-8 col-md-4">
               		<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
                    	<input type="hidden" id="feedback_id" name="feedback_id" value="<?php if(!empty($details[0]->feedback_id)){echo $details[0]->feedback_id;}?>" />
					<div class="control-group form-group">
						<label class="control-label" for="zone_text">Zone</label>
						<div class="controls">
							<?php if(!empty($details[0]->zone_text)){ echo $details[0]->zone_text; }?>
						</div>
					</div>
						
					<div class="control-group form-group">
						<label class="control-label" for="region_text">Region</label>
						<div class="controls">
							<?php if(!empty($details[0]->region_text)){ echo $details[0]->region_text; }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="area_text">Area</label>
						<div class="controls">
							<?php if(!empty($details[0]->area_text)){ echo $details[0]->area_text; }?>
						</div>
					</div>
						
					<div class="control-group form-group">
						<label class="control-label" for="travel_dob">Center</label>
						<div class="controls">
							<?php if(!empty($CenterName)){ echo $CenterName; }?>
						</div>
					</div>
						
					<div class="control-group form-group">
						<label class="control-label" for="feedback_name">Name</label>
						<div class="controls">
							<?php if(!empty($details[0]->feedback_name)){ echo $details[0]->feedback_name; }?>
						</div>
					</div>
						
					<div class="control-group form-group">
						<label class="control-label" for="feedback_email">Email ID</label>
						<div class="controls">
							<?php if(!empty($details[0]->feedback_email)){ echo $details[0]->feedback_email; }?>
							<input type="hidden" id="feedback_email" name="feedback_email" value="<?php if(!empty($details[0]->feedback_email)){echo $details[0]->feedback_email;}?>" />
						</div>
					</div>
						
					<div class="control-group form-group">
						<label class="control-label" for="feedback_contact">Contact Number</label>
						<div class="controls">
							<?php if(!empty($details[0]->feedback_contact)){ echo $details[0]->feedback_contact; }?>
						</div>
					</div>
						
					<div class="control-group form-group">
						<label class="control-label" for="feedback_msg">Message</label>
						<div class="controls">
							<?php if(!empty($details[0]->feedback_msg)){ echo nl2br($details[0]->feedback_msg); }?>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="feedback_reply">Reply*</label>
						<div class="controls">
							<textarea class="form-control required" name="feedback_reply" id="feedback_reply" <?php if(!empty($details[0]->feedback_reply)){?> disabled <?php }?>><?php if(!empty($details[0]->feedback_msg)){ echo nl2br($details[0]->feedback_msg); }?></textarea>
						</div>
					</div>
						
					<div class="form-actions form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="<?php echo base_url();?>feedbacks" class="btn btn-primary">Back To Listing</a>
					</div>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
         </div>              
    </div>
</div><!-- end: Content -->			
<script>

$(function(){
	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						['Source', 'Templates'],['Cut','Copy','Paste','PasteText','PasteFromWord','-','Print', 'SpellChecker', 'Scayt'],
						['Find','Replace','-','Subscript','Superscript'],
						['NumberedList','BulletedList','-','Outdent','Indent','Blockquote','CreateDiv'],['BidiLtr', 'BidiRtl' ],
						['Maximize', 'ShowBlocks'],['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
});


var vRules = {
	feedback_reply:{required:true}
};
var vMessages = {
	feedback_reply:{required:"Please enter reply."}
};



$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>feedbacks/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false, 
			beforeSubmit : function(arr, $form, options){
				$(".btn-primary").hide();
				//return false;
			},
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					$(".btn-primary").show();
					setTimeout(function(){
						window.location = "<?php echo base_url();?>feedbacks";
					},2000);
				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					$(".btn-primary").show();
					return false;
				}
				$(".btn-primary").show();
			}
		});
	}
});

document.title = "Feedback Details";
</script>


