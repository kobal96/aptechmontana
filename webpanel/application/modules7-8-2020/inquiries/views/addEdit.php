

<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
	
	
	<div id="content" class="content-wrapper">
		<div class="page-title">
			<div>
			<h1>Enquiry Details </h1><br/>
			<!-- <span><label>Lead Number : L0<?= $details[0]->lead_id;?></label></span>            -->
			</div>
			<div>
			<ul class="breadcrumb">
				<li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
				<li><a href="<?php echo base_url();?>centermaster">Enquiry Details</a></li>
			
			</ul>
			</div>
		</div>

		<!-- <ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#enquiry_details")>Enquiry Details</a></li>
			<li><a data-toggle="tab" href="#for_office_use">For Office Use</a></li>
			
		</ul> -->
		<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data" novalidate>
			<input type="hidden" id="lead_id" name="lead_id" value="<?php if(!empty($details[0]->inquiry_master_id)){echo $details[0]->inquiry_master_id;}?>" />	
			<div class="tab-content">
				<!-- //starting of tab one -->
				<!-- <div id="enquiry_details" class="tab-pane fade in active"> -->
					<div class="card">       
					<div class="card-body">             
					   <div class="box-content">
                            <div class="row">
                                <div class="col-sm-6 control-group form-group">
                                    <label class="control-label" for="center_id">Select Zone*</label>
                                    <div class="controls">
                                    <select id="zone_id" name="zone_id"  readonly class="form-control selectpicker required show-tick "  onchange="getCenters(this.value)" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select zone...">
                                            <option value="">Select Zone</option>
                                            <?php 
                                                if(isset($zones) && !empty($zones)){
                                                    foreach($zones as $cdrow){
                                                        $sel='';
                                                        if(!empty($details[0]->zone_id)){
                                                            $sel = ($cdrow['zone_id'] == $details[0]->zone_id)? 'selected="selected"' : '';?>
                                                        <?php }
                                                        // else{
                                                        //     $sel = ($cdrow['zone_name']== 'India')?'selected="selected"':''?>
                                                            <?php //}?>
                                                <option value="<?php echo $cdrow['zone_id'];?>" <?php echo $sel; ?>><?php echo $cdrow['zone_name'];?></option>
                                            <?php }}?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-6 control-group form-group" style="padding-left: 23px;">
                                    <label class="control-label" for="lead_sub_status">Select Center*</label>
                                    <div class="controls">
                                        <select id="center_id" name="center_id" class="form-control show-tick required"  data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select center ..."  readonly>
                                            <option value="">Select Center</option>
                                            <?php 
                                                if(isset($centers) && !empty($centers)){
                                                    foreach($centers as $cdrow){
                                                        $sel='';
                                                        if(!empty($details[0]->center_id)){
                                                            $sel = ($cdrow['center_id'] == $details[0]->center_id)? 'selected="selected"' : '';?>
                                                        <?php }
                                                        // else{
                                                        //     $sel = ($cdrow['zone_name']== 'India')?'selected="selected"':''?>
                                                            <?php //}?>
                                                <option value="<?php echo $cdrow['center_id'];?>" <?php echo $sel; ?>><?php echo $cdrow['center_name'];?></option>
                                            <?php }}?>
                                        </select>
                                </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 control-group form-group">
                                    <label class="control-label"><span>Inquiry No</span></label>
                                    <div class="controls">
                                        <input type="text" class="form-control required "  readonly placeholder="" name="inquiry_no" value="<?php if(!empty($details[0]->inquiry_no)){echo $details[0]->inquiry_no;}?>"  maxlength="50">
                                    </div>
                                </div>

                                <div class="col-sm-6 control-group form-group">
                                    <label class="control-label"><span>Inquiry Date</span></label>
                                    <div class="controls">
                                        <input type="text" class="form-control required " readonly placeholder="Enter last name" name="inquiry_date" value="<?php if(!empty($details[0]->inquiry_date)){echo date('d-m-Y',strtotime($details[0]->inquiry_date));}?>"  maxlength="50">
                                    </div>
                                </div>
                            </div>
						<a href="javascript:void(0)" class="btn btn-primary">PERSIONAL INFORMATION</a><hr>
						
							<div class="col-sm-12 col-md-12">
									<div class="row">
										<div class="col-sm-6 control-group form-group">
											<label class="control-label"><span>First Name*</span></label>
											<div class="controls">
												<input type="text" class="form-control required " placeholder="Enter first name" name="student_first_name" value="<?php if(!empty($details[0]->student_first_name)){echo $details[0]->student_first_name;}?>"  maxlength="50">
											</div>
										</div>

										<div class="col-sm-6 control-group form-group">
											<label class="control-label"><span>Last Name*</span></label>
											<div class="controls">
												<input type="text" class="form-control required " placeholder="Enter last name" name="student_last_name" value="<?php if(!empty($details[0]->student_last_name)){echo $details[0]->student_last_name;}?>"  maxlength="50">
											</div>
										</div>
									</div>

									
									<div class="row">
										<div class="col-sm-6 control-group form-group" style="margin-top: 10px;" >
											<label class="control-label" style="margin-left: 5px;"><span>Gender*</span></label>
											<div class="controls">
											<input type="radio"    name="gender" class="required "  value="Male" <?= (!empty($details[0]->gender) && $details[0]->gender == 'Male'?'checked':'')?> ><label>Male</label>
											<input type="radio"   name="gender" class="required "  value="Female" <?= (!empty($details[0]->gender) && $details[0]->gender == 'Female'?'checked':'')?>
											><label>Female</label>
											</div>
										</div>

										<div class="col-sm-6 control-group form-group">
											<label class="control-label"><span>Date of birth*</span></label>
											<div class="controls">
												<input type="text" readonly class="form-control required " placeholder="choose date of birth"  id="student_dob" name="student_dob" value="<?php if(!empty($details[0]->student_dob)){echo date('d-m-Y', strtotime($details[0]->student_dob));}?>"  readonly>
											</div>
										</div>
									</div>
                                    
                                    <div class="row">
										<div class="col-sm-6 control-group form-group">
											<label class="control-label"><span>Years</span></label>
											<div class="controls">
												<input type="text" class="form-control required number_only"  readonly placeholder="" id="age_year" name="age_year" value="<?php if(!empty($details[0]->age_year)){echo $details[0]->age_year;}?>"  maxlength="3" minlength="1">
											</div>
										</div>

										<div class="col-sm-6 control-group form-group">
											<label class="control-label"><span>Months</span></label>
											<div class="controls">
											<input type="text" class="form-control  required number_only" placeholder="" readonly name="age_month" id="age_month" value="<?php if(!empty($details[0]->age_month)){echo$details[0]->age_month;}?>"  maxlength="2" minlength="1">

											</div>
										</div>
									</div>

									
									<div class="row">
										<div class="col-sm-6 control-group form-group">
											<label class="control-label" for="career_field">Category*</label>
											<div class="controls">
                                                <select id="category_id" name="category_id" class="form-control selectpicker required show-tick"   data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Select Category">
                                                <option value="">Select Category</option>
                                                <option value="1">PreSchool</option>
                                                </select>
											</div>
										</div>

										<div class="col-sm-6 control-group form-group">
										
											<label class="control-label" for="career_field">Course*</label>
											<div class="controls">
											<select id="course_id" name="course_id" class="form-control selectpicker required show-tick"   data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Select SCourse">
											<option value="">course*</option>
												<?php 
													// $stored_course = explode(",",$details[0]->course);
													foreach ($course as $key => $value) {
                                                        // print_r($value);
														$sel='';
														// $sel= (in_array($value,$stored_course)?'selected':'');
													?>
													<option value="<?=$value['course_id']?>" <?= $sel?> ><?=$value['course_name']?></option>
												<?php } ?>		
											</select>
											</div>
										</div>
									</div>

                                    <div class="row">
										<div class="col-sm-6 control-group form-group">
											<label class="control-label" style="margin-left: 5px;"><span>Has Child attended preschool before?</span></label>
											<div class="controls">
											<input type="radio"    name="has_attended_preschool_before" class="required has_attended_preschool_before "  value="Yes" <?= (!empty($details[0]->has_attended_preschool_before) && $details[0]->has_attended_preschool_before == 'Yes'?'checked':'')?> ><label>Yes</label>
											<input type="radio" checked  name="has_attended_preschool_before" class="required has_attended_preschool_before "  value="No" <?= (!empty($details[0]->has_attended_preschool_before) && $details[0]->has_attended_preschool_before == 'No'?'checked':'')?>
											><label>NO</label>
											</div>
										</div>

										<div class="col-sm-6 control-group form-group" id="has_attended_preschool_before" style="display:none"> 
											<label class="control-label"><span>Pre School Name*</span></label>
											<div class="controls">
												<input type="text" class="form-control required " placeholder="Enter Pre School Name"  id="preschool_name" name="preschool_name" value="<?php if(!empty($details[0]->preschool_name)){echo ($details[0]->preschool_name);}?>"  >
											</div>
										</div>
									</div>

                                    <a href="javascript:void(0)" class="btn btn-primary">FAMILY INFORMATION :</a><hr>

									<div class="row">
                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>Father Name*</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required " placeholder="Enter Father  name" name="father_name" value="<?php if(!empty($details[0]->father_name)){echo $details[0]->father_name;}?>"  maxlength="50">
                                            </div>
                                        </div>


                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>Mother Name*</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required " placeholder="Enter mother  name" name="mother_name" value="<?php if(!empty($details[0]->mother_name)){echo $details[0]->mother_name;}?>"  maxlength="50">
                                            </div>
                                        </div>
									</div>

                                    <div class="row">
                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>Father Contact Number*</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required " placeholder="Enter Father Contact Number" name="father_contact_no" value="<?php if(!empty($details[0]->father_contact_no)){echo $details[0]->father_contact_no;}?>"  maxlength="10"  minlength="10">
                                            </div>
                                        </div>


                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>Mother Contact Number*</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required " placeholder="Enter mother Contact Number" name="mother_contact_no" value="<?php if(!empty($details[0]->mother_contact_no)){echo $details[0]->mother_contact_no;}?>"  maxlength="10"  minlength="10">
                                            </div>
                                        </div>
									</div>

                                    <div class="row">
                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>Father's Email Id*</span></label>
                                            <div class="controls">
                                                <input type="email" class="form-control required " placeholder="Enter Father's Email Id" name="father_emailid" value="<?php if(!empty($details[0]->father_emailid)){echo $details[0]->father_emailid;}?>"  maxlength="50">
                                            </div>
                                        </div>


                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>Mother Email Id*</span></label>
                                            <div class="controls">
                                                <input type="email" class="form-control required " placeholder="Enter mother  name" name="mother_emailid" value="<?php if(!empty($details[0]->mother_emailid)){echo $details[0]->mother_emailid;}?>"  maxlength="50">
                                            </div>
                                        </div>
									</div>
                                    

                                    <div class="row">
                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>Father's Education*</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required " placeholder="Enter Father's Education" name="father_education" value="<?php if(!empty($details[0]->father_education)){echo $details[0]->father_education;}?>"  maxlength="50">
                                            </div>
                                        </div>


                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>Mother Education*</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required " placeholder="Enter mother  Education" name="mother_education" value="<?php if(!empty($details[0]->mother_education)){echo $details[0]->mother_education;}?>"  maxlength="50">
                                            </div>
                                        </div>
									</div>

                                     <div class="row">
                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>Father's Profession*</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required " placeholder="Enter Father's Email Id" name="father_profession" value="<?php if(!empty($details[0]->father_profession)){echo $details[0]->father_profession;}?>"  maxlength="50">
                                            </div>
                                        </div>


                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>Mother Profession*</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required " placeholder="Enter mother  Education" name="mother_profession" value="<?php if(!empty($details[0]->mother_profession)){echo $details[0]->mother_profession;}?>"  maxlength="50">
                                            </div>
                                        </div>
									</div>
                                    
                                    <div class="row">
                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>House/Bldg/Apt*</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required " placeholder="enter House/Bldg/Apt " name="present_address" value="<?php if(!empty($details[0]->present_address)){echo $details[0]->present_address;}?>"  maxlength="50">
                                            </div>
                                        </div>


                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>Street/Road/Lane*</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required " placeholder="Enter Street/Road/Lane" name="address1" value="<?php if(!empty($details[0]->address1)){echo $details[0]->address1;}?>"  maxlength="50">
                                            </div>
                                        </div>
									</div>

                                    <div class="row">
                                         <div class="col-sm-6 control-group form-group">
											<label class="control-label"><span>Landmark*</span></label>
											<div class="controls">
												<input type="text" class="form-control required " placeholder="Enter Landmark" name="address2"  id="address2" value="<?php if(!empty($details[0]->address2)){echo $details[0]->address2;}?>" >
											</div>
										</div>


                                        <div class="col-sm-6 control-group form-group">
											<label class="control-label"><span>Area/Locality/Sector*</span></label>
											<div class="controls">
												<input type="text" class="form-control required " placeholder="Enter Area/Locality/Sector" id="address3" name="address3" value="<?php if(!empty($details[0]->address3)){echo $details[0]->address3;}?>" >
											</div>
										</div>
									</div>

									<div class="row">
									
										<div class="col-sm-6 control-group form-group">
											<label class="control-label" for="center_id">Country*</label>
											<div class="controls">
											<select id="country_id" name="country_id" class="form-control selectpicker required show-tick "  onchange="getstate(this.value)" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select Country...">
													<option value="">Select Country</option>
													<?php 
														if(isset($countries) && !empty($countries)){
															foreach($countries as $cdrow){
																$sel='';
																$sel = ($cdrow['country_id'] == $details[0]->country_id)? 'selected="selected"' : '';?>
														<option value="<?php echo $cdrow['country_id'];?>" <?php echo $sel; ?>><?php echo $cdrow['country_name'];?></option>
													<?php }}?>
												</select>
											</div>
										</div>

                                        <div class="col-sm-6 control-group form-group">
											<label class="control-label" for="center_id">state*</label>
											<div class="controls">
											<select id="state_id" name="state_id" class="form-control show-tick required" onchange="getcity(this.value)" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select State..." >
													<option value="">Select state</option>
													
												</select>
											</div>
										</div>

									</div>

									<div class="row">
										<div class="col-sm-6 control-group form-group">
											<label class="control-label" for="center_id">City*</label>
											<div class="controls">
											<select id="city_id" name="city_id" class="form-control show-tick required" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select City..."  >
													<option value="">Select City</option>
												</select>
											</div>
										</div>

                                        <div class="col-sm-6 control-group form-group">
											<label class="control-label"><span>Pincode*</span></label>
											<div class="controls">
												<input type="text" class="form-control required " placeholder="Enter pincode" id="pincode" name="pincode" value="<?php if(!empty($details[0]->pincode)){echo $details[0]->pincode;}?>"  maxlength="6" minlength="6">
											</div>
										</div>
									</div>							
									
								
									<div class="row">
                                        <div class="col-sm-12 control-group form-group" style="margin-top: 10px;" >
                                            <label class="control-label" style="margin-left: 5px;"><span>How did you come to know about the school?</span></label>
                                            <div class="controls">
                                            <input type="radio"    name="how_know_about_school" class="required "  value="Website" <?= (!empty($details[0]->how_know_about_school) && $details[0]->how_know_about_school == 'Website'?'checked':'')?> ><label>Website</label>
                                            <input type="radio"   name="how_know_about_school" class="required "  value="Newspaper" <?= (!empty($details[0]->how_know_about_school) && $details[0]->how_know_about_school == 'Newspaper'?'checked':'')?>
                                            ><label>Newspaper</label>
                                            <input type="radio"   name="how_know_about_school" class="required "  value="FacebookPage" <?= (!empty($details[0]->how_know_about_school) && $details[0]->how_know_about_school == 'FacebookPage'?'checked':'')?>
                                            ><label>Facebook Page</label>
                                            <input type="radio"   name="how_know_about_school" class="required "  value="JustDial" <?= (!empty($details[0]->how_know_about_school) && $details[0]->how_know_about_school == 'JustDial'?'checked':'')?>
                                            ><label>Just Dial</label>
                                            <input type="radio"   name="how_know_about_school" class="required "  value="SchoolSignBoard" <?= (!empty($details[0]->how_know_about_school) && $details[0]->how_know_about_school == 'SchoolSignBoard'?'checked':'')?>
                                            ><label>School Sign Board</label>
                                            <input type="radio"   name="how_know_about_school" class="required "  value="Reference" <?= (!empty($details[0]->how_know_about_school) && $details[0]->how_know_about_school == 'Reference'?'checked':'')?>
                                            ><label>Reference</label>
                                            <input type="radio"   name="how_know_about_school" class="required "  value="PosterBanner" <?= (!empty($details[0]->how_know_about_school) && $details[0]->how_know_about_school == 'PosterBanner'?'checked':'')?>
                                            ><label>PosterBanner</label>
                                            <input type="radio"   name="how_know_about_school" class="required "  value="Leaflet" <?= (!empty($details[0]->how_know_about_school) && $details[0]->how_know_about_school == 'Leaflet'?'checked':'')?>
                                            ><label>Leaflet</label>
                                            <input type="radio"   name="how_know_about_school" class="required "  value="Other" <?= (!empty($details[0]->how_know_about_school) && $details[0]->how_know_about_school == 'Other'?'checked':'')?>
                                            ><label>Other</label>
                                            </div>
										</div>
									</div>

                                    <div class="row">
                                
                                        <div class="col-sm-6 control-group form-group">
                                            <?php $inquirystatus = array('Hot','Warm','Cold');?>
                                            <label class="control-label" for="leadstatus">inquiry Status*</label>
                                            <div class="controls">
                                            <select id="inquirystatus" name="inquirystatus" class="form-control show-tick required"  data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Choose  ..." >
                                                    <option value="">select inquiry Status</option>
                                                        <?php 
                                                            $stored_inquirystatus = explode(",",$details[0]->know_about_us);
                                                            foreach ($inquirystatus as $key => $value) {
                                                                $sel='';
                                                                $sel= (in_array($value,$stored_inquirystatus)?'selected':'');
                                                            ?>
                                                            <option value="<?=$value?>" <?= $sel?> ><?=$value?></option>
                                                        <?php } ?>		
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 control-group form-group">
										<?php $inquiryprogress = array('YTC - Yet to Call','To Call Back & follow up','Walked-in for Enquiry','Interested/will Walk-in','Interested/Expected to Enroll','Will enroll later -Child is below age Criteria','Not Interested Now/To Follow up Later','Lost to Competition','Enrolled in a High School','Wrong Contact details');?>
										<label class="control-label" for="inquiryprogress">inquiry Progress*</label>
										<div class="controls">
										<select id="inquiryprogress" name="inquiryprogress" class="form-control show-tick required"  data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Choose inquiry Progress ..." >
														<option value="">select inquiry Progress</option>
													<?php 
														$stored_inquiryprogress = explode(",",$details[0]->know_about_us);
														foreach ($inquiryprogress as $key => $value) {
															$sel='';
															$sel= (in_array($value,$stored_inquiryprogress)?'selected':'');
														?>
														<option value="<?=$value?>" <?= $sel?> ><?=$value?></option>
													<?php } ?>		
											</select>
										</div>
									</div>

                                    </div>	

                                <div class="row">
									<div class="col-sm-12 col-md-12  control-group form-group">
										<label class="control-label" for="inquiry_enquiry_status">Remark (For Office Use)*</label>
										<div class="controls">
										<textarea name="inquiry_remark" id="inquiry_remark" cols="130" rows="2"> <?php if(!empty($details[0]->inquiry_remark)){echo $details[0]->inquiry_remark;}?></textarea>
										</div>
									</div>
								</div>

                                <div class="row">
									<div class="col-sm-12 col-md-12 form-actions form-group">
										<button type="submit" class="btn btn-primary">Submit</button>
										<a href="<?php echo base_url();?>lead" class="btn btn-primary">Cancel</a>
									</div>
								</div>

							</div>
						<div class="clearfix"></div>
						
						</div>
					</div>
					</div>      
				<!-- </div> -->
				<!-- end of tab one -->
			</div>
			
		</form>

	</div><!-- end: Content -->								
<script>

$( function() {
    $( "#student_dob" ).datepicker();
  } );
$(".addreferences").click(function()
	{
		var index = 1;
		$("#reference_table tbody tr.reference_row_tr").each(function(){
			index = index + 1;
		});
		console.log(index);
		if(index == 6){
			alert("only 5 References is allowed");
			return false;
		}
		$html = '<tr class="reference_row_tr">'+
					'<td>'+
						'<label class="order-cls">'+index+'</label>'+
					'</td>'+
					'<td>'+
						'<input type="text" name="reference_name[]" id="reference_name'+index+'" class="form-control " />'+
					'</td>'+
					'<td>'+
						'<input type="text" name="reference_contact[]" id="reference_contact'+index+'" maxlength="10" minlength="10" class="form-control number_only"/>'+
					'</td>'+
					'<td>'+
						'<textarea name="reference_address[]" id="reference_address'+index+'" cols="40" ></textarea>'+
					'</td>'+
					'<td>'+
						'<input type="text" name="reference_qualification[]" id="reference_qualification'+index+'" class="form-control" />'+
					'</td>'+
					'<td>'+
						'<button type="button" class="btn-primary remove"><i class="fa fa-remove"></i></button>'
					'</td>'+
				'</tr>';
		$('#reference_table').find("tbody").append($html);
		reOrder();
	});

	function reOrder(){
		var order_by = 1;
		$('.order-cls').each(function(){
			$(this).html(order_by++);
			
		});
	}

	$('#reference_table').on('click', '.remove', function () {
		var table_row = $('#reference_table tbody  tr.reference_row_tr').length;
		console.log(table_row);
		if(table_row == '1'){
			alert("Atleast one 1 reference is must ");
		}else{
		$(this).closest('tr').remove();
		reOrder();
		}
	});

$('document').ready(function(){
	// $(".selectpicker").selectpicker("refresh");
	$(".number_only").keypress(function (e) {
    	if (e.which != 8 && e.which != 0 && e.which != 46 &&(e.which < 48|| e.which > 57)) {
      	return false;
    	}
  	});

      <?php if(!empty($details) && isset($details)){?>
        getstate(<?= $details[0]->country_id?>,<?= $details[0]->state_id?>);
        getcity(<?= $details[0]->state_id?>,<?= $details[0]->city_id?>);

		// getLeadEnquiyStage(<?= $details[0]->lead_status_id?>,<?= $details[0]->lead_sub_status_id?>,<?= $details[0]->lead_enquiry_status_id?>);
		// <?php if(!empty($details[0]->family_country_id)  && isset($details[0]->family_country_id)){?>
		// getfamilystate(<?= $details[0]->family_country_id?>,<?= $details[0]->family_state_id?>);
        // getfamilycity(<?= $details[0]->family_state_id?>,<?= $details[0]->family_city_id?>);
		<?php }else{?>
			/* this is for 1st time when we want country should be india */
			getfamilystate($('#family_country_id').val());
			
		<?php } }?> 
})
$( document ).ready(function() {
	
	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						//['Source', 'Templates'],
						['Cut','Copy','Paste','PasteText','-','Print', 'SpellChecker', 'Scayt'],
						['Subscript','Superscript'],
						['NumberedList','BulletedList'],
						['BidiLtr', 'BidiRtl' ],
						//['Maximize', 'ShowBlocks'],
						['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						//['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						//['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
	
	$.validator.addMethod("needsSelection", function(value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });
	
});

$('.has_attended_preschool_before').on('change', function() {
   var careerin = $("input[name='has_attended_preschool_before']:checked").val(); 
   if(careerin == 'Yes'){
	   $('#has_attended_preschool_before').fadeIn();
   }else{
	$('#has_attended_preschool_before').fadeOut()
    $('#preschool_name').val('');
   }
});



function getstate(country_id,state_id=null){
	if(country_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>lead/getStates",
			data:{country_id:country_id,state_id:state_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#state_id").html("<option value=''>Select State</option>"+res['option']);
						$("#state_id").selectpicker("refresh");
					}else{
						$("#state_id").html("<option value=''>No state found</option>");
						$("#state_id").selectpicker("refresh");
					}
				}
			}
		});
	}
}

// function getfamilystate(country_id,state_id=null){
// 	// alert(country_id);
// 	if(country_id != "" ){
// 		$.ajax({
// 			url:"<?php echo base_url();?>lead/getStates",
// 			data:{country_id:country_id,state_id:state_id},
// 			dataType: 'json',
// 			method:'post',
// 			success: function(res){
// 				if(res['status']=="success"){
// 					if(res['option'] != ""){
// 						$("#family_state_id").html("<option value=''>Select State</option>"+res['option']);
// 						$("#family_state_id").selectpicker("refresh");
// 					}else{
// 						$("#family_state_id").html("<option value=''>No state found</option>");
// 						$("#family_state_id").selectpicker("refresh");
// 					}
// 				}
// 			}
// 		});
// 	}
// }

// function getLeadEnquiyStage(status_id=null,sub_status_id=null,lead_enquiry_status_id=null){
// 	// alert(sub_status_id);
// 	<?php if(empty($details)){?>
// 		var status_id = $('#lead_status').val();
// 		var sub_status_id =$('#lead_sub_status').val() ;
// 		// alert("sub_status_id");
// 	<?php }?>
// 	if(status_id && sub_status_id ){
// 		$.ajax({
// 			url:"<?php echo base_url();?>lead/getLeadEnquiyStage",
// 			data:{status_id:status_id,sub_status_id:sub_status_id,lead_enquiry_status_id:lead_enquiry_status_id},
// 			dataType: 'json',
// 			method:'post',
// 			success: function(res){
// 				if(res['status']=="success"){
// 					if(res['option'] != ""){
// 						$("#lead_enquiry_status").html("<option value=''>Select lead enquiry status</option>"+res['option']);
// 						$("#lead_enquiry_status").selectpicker("refresh");
// 					}else{
// 						$("#lead_enquiry_status").html("<option value=''>lead enquiry status</option>");
// 						// $("#lead_enquiry_status").selectpicker("refresh");
// 					}
// 				}
// 				/* else{	
// 					$("#lead_enquiry_status").html("<option value=''>Select lead enquiry status</option>");
// 					$("#lead_enquiry_status").selectpicker("refresh");
// 				} */
// 			}
// 		});
// 	}
// }

function getcity(state_id,city_id=null){
	if(state_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>lead/getCities",
			data:{state_id:state_id,city_id:city_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#city_id").html("<option value=''>Select City</option>"+res['option']);
						$("#city_id").selectpicker("refresh");
					}else{
						$("#city_id").html("<option value=''>No state found</option>");
						$("#city_id").selectpicker("refresh");
					}
				}else{	
					$("#city_id").html("<option value=''>Select</option>");
					$("#city_id").selectpicker("refresh");
				}
			}
		});
	}
}

// function getfamilycity(state_id,city_id=null){
// 	if(state_id != "" ){
// 		$.ajax({
// 			url:"<?php echo base_url();?>lead/getCities",
// 			data:{state_id:state_id,city_id:city_id},
// 			dataType: 'json',
// 			method:'post',
// 			success: function(res){
// 				if(res['status']=="success"){
// 					if(res['option'] != ""){
// 						$("#family_city_id").html("<option value=''>Select City</option>"+res['option']);
// 						$("#family_city_id").selectpicker("refresh");
// 					}else{
// 						$("#family_city_id").html("<option value=''>No state found</option>");
// 						$("#family_city_id").selectpicker("refresh");
// 					}
// 				}else{	
// 					$("#city_id").html("<option value=''>Select</option>");
// 					$("#city_id").selectpicker("refresh");
// 				}
// 			}
// 		});
// 	}
// }

// function getSubStatus(status_id,sub_status_id=null){
// 	if(status_id != "" ){
// 		$.ajax({
// 			url:"<?php echo base_url();?>lead/getSubStatus",
// 			data:{status_id:status_id,sub_status_id:sub_status_id},
// 			dataType: 'json',
// 			method:'post',
// 			success: function(res){
// 				if(res['status']=="success"){
// 					if(res['option'] != ""){
						
// 						$("#lead_enquiry_status").html("<option value=''>Select lead enquiry status</option>");
// 						// $("#lead_enquiry_status").selectpicker("refresh");
// 						$("#lead_sub_status").html("<option value=''>Select Sub status</option>"+res['option']);
// 						$("#lead_sub_status").selectpicker("refresh");
// 					}else{
// 						$("#lead_sub_status").html("<option value=''>No Sub status found</option>");
// 						$("#lead_sub_status").selectpicker("refresh");
// 					}
// 				}
// 				/* else{	
// 					$("#lead_sub_status").html("<option value=''>Select Sub status</option>");
// 					$("#lead_sub_status").selectpicker("refresh");
// 				} */
// 			}
// 		});
// 	}
// }



var vRules = {
	student_first_name:{required:true},
	student_last_name:{required:true},
	email_id:{required:true},		  
	lead_status :{required:true},
	lead_sub_status :{required:true},
	lead_enquiry_status:{required :true},
	planned_status:{required:true},
    country_id:{required:true},
    state_id:{required:true},
    city_id:{required:true},
    know_about_us:{required:true},
    career_in:{required:true}
};
var vMessages = {
	student_first_name:{required:"please enter first name"},
	student_last_name:{required:"please enter last name"},
    contact_number1:{required:"please enter contact number 1"},
    age_month:{required:"please enter contact number 2"},
	email_id:{required:"Please Enter the Email Id",email:"Please enter correct email id",remote:"Email ID already exist.."},
	father_email_id:{required:"Please Enter the Email Id",email:"Please enter correct email id",remote:"Email ID already exist.."},
	mother_email_id:{required:"Please Enter the Email Id",email:"Please enter correct email id",remote:"Email ID already exist.."},
	country_id:{required:"select country "},
	lead_status :{required:"select  lead status "},
	lead_sub_status :{required:"select lead sub status"},
	planned_status:{required:"Select Planend Lead status"},
	lead_enquiry_status :{requred:"Select lead enquiy status"},
    state_id:{required:"select state "},
    city_id:{required:"select city "},
    know_about_us:{required:"Select how you know about us"},
    career_in:{required:"select career field"}
	
};

$("#form-validate").validate({
	// rules: vRules,
	// messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>inquiries/submitFormInquiry";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>inquiries";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Center";

 
</script>					
