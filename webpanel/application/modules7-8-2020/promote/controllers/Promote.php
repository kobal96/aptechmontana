<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Promote extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);
		$this->load->model('promotemodel', '', TRUE);
	}

	function index(){	
		if (!empty($_SESSION["webadmin"])) {
			if ($this->privilegeduser->hasPrivilege("PromoteList")) {
				//get search filters 
				$filterData = array();
				$filterData['zoneDetails'] = $this->promotemodel->getdata1("tbl_zones","zone_id,zone_name","status='Active'");
				$filterData['academicyear'] = $this->promotemodel->getdata1("tbl_academic_year_master","*");
				$filterData['categories'] = $this->promotemodel->getdata1("tbl_categories","category_id,categoy_name");
				$this->load->view('template/header.php');
				$this->load->view('promote/index',$filterData);
				$this->load->view('template/footer.php');
			}else{
				redirect('home', 'refresh');
			}
		}else {
			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	/*new code end*/
	function fetch($id=null){

		$get_result = $this->promotemodel->getRecords($_GET);
		// echo "<pre>";
		// print_r($get_result);
		// exit;
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				// array_push($temp, date("d-m-Y",strtotime($get_result['query_result'][$i]->inquiry_date)));
				array_push($temp, $get_result['query_result'][$i]->academic_year_master_name);
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);	
				array_push($temp, $get_result['query_result'][$i]->categoy_name);
				array_push($temp, $get_result['query_result'][$i]->course_name);
				array_push($temp, $get_result['query_result'][$i]->enrollment_no);
				array_push($temp, $get_result['query_result'][$i]->student_first_name." ".$get_result['query_result'][$i]->student_last_name);
				
				// $viewPaymentDetails="";
				// $viewPaymentDetails.= '<a href="paymentdetails/index?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->student_id) , '+/', '-_') , '=') . '" title="Payment Details">View Payment Details</a>';

				// array_push($temp, $viewPaymentDetails);
				$actionCol1 = "";
				if ($this->privilegeduser->hasPrivilege("PromoteAddEdit")) {

					if(empty($get_result['query_result'][$i]->fees_id)){
						$actionCol1.= '<a  class="btn btn-primary" href="promote/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->promoted_id) , '+/', '-_') , '=') . '" title="Edit Promotion"><i class="fa fa-edit"></i></a>&nbsp
						&nbsp &nbsp <a  class="btn btn-primary" href="admission/addEdit?text=' . rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->student_id) , '+/', '-_') , '=') .'&year='.$get_result['query_result'][$i]->promoted_academic_year_id.'"  title="Assign Fees"><i class="fa fa-money" aria-hidden="true"></i></a>';
												
					}else{
						$actionCol1.= '<a  class="btn btn-primary" href="promote/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->promoted_id) , '+/', '-_') , '=') . '" title="Edit Promotion"><i class="fa fa-edit"></i></a>';
						
					}
					array_push($temp, $actionCol1);
				}
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}


	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			if($this->privilegeduser->hasPrivilege("PromoteAddEdit")) {
				
				$result['academicyear'] = $this->promotemodel->getDropdown1("tbl_academic_year_master","academic_year_master_id,academic_year_master_name");
				$result['upcomingacademicyear'] = $this->promotemodel->getDataCondition("tbl_academic_year_master","academic_year_master_id,academic_year_master_name");
				$result['zones'] = $this->promotemodel->getDropdown1("tbl_zones","zone_id,zone_name");
				$result['categories'] = $this->promotemodel->getDropdown1("tbl_categories","category_id,categoy_name");

				 $record_id = "";
				if (!empty($_GET['text']) && isset($_GET['text'])) {
					$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
					parse_str($varr, $url_prams);
					$record_id = $url_prams['id'];
					$result['details'] = $this->promotemodel->getFormdata($record_id);
				}


				/*$result = [];
				$result1 = [];
				$inquiryNo = [];
				$result['inquiries'] = $this->promotemodel->getDropdown("tbl_inquiry_master","inquiry_master_id,lead_id,student_first_name,student_last_name");
				for($i=0;$i<sizeof($result['inquiries']);$i++){
					$result1['inquiry_nos'] = $this->promotemodel->getFormdataInquiryNo($result['inquiries'][$i]->lead_id);
					$result['no'][] = $result1['inquiry_nos'];
				}
				$getenrollmentNo = $this->promotemodel->getLastEnrollmentNo("tbl_student_master");
				if(!empty($getenrollmentNo)){
					$enrollmentNoPrefix = $getenrollmentNo[0]->enrollment_no +1;
					$result['enrollment_no'] = sprintf("%06d", $enrollmentNoPrefix);
				}
				else{
					$result['enrollment_no'] = sprintf("%06d",000001);
				}
				if(isset($_SESSION['student_id']))
				{
					$inquiry_id = $this->promotemodel->getFormdata2('tbl_student_master','student_id',$_SESSION['student_id']);
					$result['studentImageDetails'] = $inquiry_id;
					$result['getStudentdetails'] = $this->promotemodel->getFormdata2('tbl_inquiry_master','inquiry_master_id,inquiry_date',$inquiry_id[0]->inquiry_master_id);
					$result['getStateName'] = $this->promotemodel->getDetails("tbl_states",$result['getStudentdetails'][0]->state_id,"state_id");
					$result['getCityName'] = $this->promotemodel->getDetails("tbl_cities",$result['getStudentdetails'][0]->city_id,"city_id");
					print_r($result['getStudentdetails']);exit;
				}
				$result['batch'] = $this->promotemodel->getDropdown1("tbl_batch_master","batch_id,batch_name");
				$result['countries'] = $this->promotemodel->getDropdown1("tbl_countries","country_id,country_name");
				// $result['months'] = $this->promotemodel->getMonths("tbl_childactivity_fees_month", "month_id, month_name,month,name", "status='Active' ");
				// print_r($record_id);
				// exit();
				$result['details'] = $this->promotemodel->getFormdata($record_id);
				// print_r($result['details']);
				// exit();
				if (!empty($_GET['text']) && isset($_GET['text'])) {
					$result['getStateName'] = $this->promotemodel->getDetails("tbl_states",$result['details'][0]->state_id,"state_id");
					$result['getCityName'] = $this->promotemodel->getDetails("tbl_cities",$result['details'][0]->city_id,"city_id");
					$ispromote = $this->promotemodel->getdata1("tbl_promote_fees", "promote_fees_id", "student_id='".$record_id."'");
					if(!empty($ispromote)){
						$result['flag'] = 1;
					}else{
						$result['flag'] = 0;
					}
				}
				// print_r($result);exit();
				$result['otherdetails'] = $this->promotemodel->getFormdata1($record_id);
				$result['fees_level'] = $this->promotemodel->getDropdown1("tbl_fees_level_master","fees_level_id,fees_level_name");
				$result['login_name'] = $_SESSION["webadmin"][0]->first_name .' '. $_SESSION["webadmin"][0]->last_name;
				$result['fees_type_details'] = $this->promotemodel->enum_select("tbl_assign_fees","fees_type");
 */
				
				/* echo "<pre>";
				print_r($result);
				exit; */
				$this->load->view('template/header.php');
				$this->load->view('promote/addEdit', $result);
				$this->load->view('template/footer.php');
			}else{
				redirect('home', 'refresh');
			}
		}else {
			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function convertTopromote($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = [];
			if(isset($_SESSION['student_id']))
			{
				$inquiry_id = $this->promotemodel->getFormdata2('tbl_student_master','student_id',$_SESSION['student_id']);
				$result['studentImageDetails'] = $inquiry_id;
			}
			$result['inquiries'] = $this->promotemodel->getDropdown("tbl_inquiry_master","inquiry_master_id,lead_id,student_first_name,student_last_name,enrollment_no");
			$result['academicyear'] = $this->promotemodel->getDropdown1("tbl_academic_year_master","academic_year_master_id,academic_year_master_name");
			for($i=0;$i<sizeof($result['inquiries']);$i++){
				$result1['inquiry_nos'] = $this->promotemodel->getFormdataInquiryNo($result['inquiries'][$i]->lead_id);
				$result['no'][] = $result1['inquiry_nos'];
			}
			$getenrollmentNo = $this->promotemodel->getLastEnrollmentNo("tbl_student_master");
			if(!empty($getenrollmentNo)){
				$enrollmentNoPrefix = $getenrollmentNo[0]->enrollment_no +1;
				$result['enrollment_no'] = sprintf("%06d", $enrollmentNoPrefix);
			}
			else{
				$result['enrollment_no'] = sprintf("%06d",000001);
			}
			$result['zones'] = $this->promotemodel->getDropdown1("tbl_zones","zone_id,zone_name");
			$result['categories'] = $this->promotemodel->getDropdown1("tbl_categories","category_id,categoy_name");
			$result['batch'] = $this->promotemodel->getDropdown1("tbl_batch_master","batch_id,batch_name");
			// print_r($record_id);
			// exit();
			$result['details'] = $this->promotemodel->getDetails("tbl_inquiry_master",$record_id,"inquiry_master_id");
			// print_r($result);
			// exit();
			if(!empty($result['details'][0]->lead_id)){
				$result['getInquiryNo'] = $this->promotemodel->getDetails("tbl_inquiry_master",$result['details'][0]->lead_id,"inquiry_master_id");
			}
			$result['countries'] = $this->promotemodel->getDropdown1("tbl_countries","country_id,country_name");
			$result['getStateName'] = $this->promotemodel->getDetails("tbl_states",$result['details'][0]->state_id,"state_id");
			$result['getCityName'] = $this->promotemodel->getDetails("tbl_cities",$result['details'][0]->city_id,"city_id");
			$result['fees_level'] = $this->promotemodel->getDropdown1("tbl_fees_level_master","fees_level_id,fees_level_name");
			$result['login_name'] = $_SESSION["webadmin"][0]->first_name .' '. $_SESSION["webadmin"][0]->last_name;
			// $result['otherdetails'] = $this->promotemodel->getFormdata1($record_id);
			// print_r($result);
			// exit();
			$this->load->view('template/header.php');
			$this->load->view('promote/converttopromote', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	public function getCenters(){
		$result = $this->promotemodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		//echo "<pre>";
		//print_r($result);
		//exit;
		
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->center_id == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getCourses(){
		$result = $this->promotemodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['course_id']) && !empty($_REQUEST['course_id'])){
			$course_id = $_REQUEST['course_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getPromotedCourses(){
		$result = $this->promotemodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['promoted_course_id']) && !empty($_REQUEST['promoted_course_id'])){
			$course_id = $_REQUEST['promoted_course_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getBatch(){
		// print_r($_REQUEST);
		// exit();
		$result = $this->promotemodel->getOptions1("tbl_batch_master",$_REQUEST['center_id'],"center_id",$_REQUEST['course_id'],"course_id");
		
		$option = '';
		$batch_id = '';
		
		if(isset($_REQUEST['batch_id']) && !empty($_REQUEST['batch_id'])){
			$batch_id = $_REQUEST['batch_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->batch_id == $batch_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->batch_id.'" '.$sel.' >'.$result[$i]->batch_name.'</option>';
			}
		}
		// print_r($option);exit();
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getPromotedBatch(){
		// print_r($_REQUEST);
		// exit();
		$result = $this->promotemodel->getOptions1("tbl_batch_master",$_REQUEST['center_id'],"center_id",$_REQUEST['course_id'],"course_id");
		
		// echo $this->db->last_query();
		$option = '';
		$batch_id = '';
		
		if(isset($_REQUEST['promoted_batch_id']) && !empty($_REQUEST['promoted_batch_id'])){
			$batch_id = $_REQUEST['promoted_batch_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->batch_id == $batch_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->batch_id.'" '.$sel.' >'.$result[$i]->batch_name.'</option>';
			}
		}
		// print_r($option);exit();
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}


	function getBatchDetails(){
		$result = $this->promotemodel->getOptions("tbl_batch_master",$_REQUEST['batch_id'],"batch_id");

		echo json_encode(array("status"=>"success","result"=>$result));
		exit;
	}

	function getState(){
		$result = $this->promotemodel->getOptions("tbl_states",$_REQUEST['country_id'],"country_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$state_id = '';
		
		if(isset($_REQUEST['state_id']) && !empty($_REQUEST['state_id'])){
			$state_id = $_REQUEST['state_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->state_id == $state_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->state_id.'" '.$sel.' >'.$result[$i]->state_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	public function getCity(){
		// print_r($_REQUEST);
		// exit();
		$result = $this->promotemodel->getOptions2("tbl_cities",$_REQUEST['state_id'],"state_id");
		$option = '';
		$city_id = '';
		if(isset($_REQUEST['city_id']) && !empty($_REQUEST['city_id'])){
			$city_id = $_REQUEST['city_id'];
		}
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->city_id == $city_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->city_id.'" '.$sel.' >'.$result[$i]->city_name.'</option>';
			}
		}
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getFees(){
		// print_r($_REQUEST);exit();
		// $condition = "f.fees_selection_type='".$_REQUEST['feesselectiontype']."'  && i.fees_level_id='".$_REQUEST['feeslevelid']."' && i.zone_id='".$_REQUEST['zone_id']."' && i.center_id='".$_REQUEST['center_id']."' && i.status='Active' && f.fees_type='".$_REQUEST['fees_type']."' "; 
		$condition = "f.fees_selection_type='".$_REQUEST['feesselectiontype']."' && i.zone_id='".$_REQUEST['zone_id']."' && map.center_id='".$_REQUEST['center_id']."' &&  i.status='Active' && f.fees_type='".$_REQUEST['fees_type']."' "; 
					
		if($_REQUEST['feesselectiontype'] == 'Class'){
			$condition .=" && i.category_id='".$_REQUEST['categoryid']."' && gc.course_id='".$_REQUEST['courseid']."' ";
		}else{
			$condition .=" && gc.group_id='".$_REQUEST['groupid']."'  ";
		}
		$current_date = date("Y-m-d");
		if(!empty($_REQUEST['batch_start_date']) && !empty($_REQUEST['batch_end_date'])){
			// $condition .= " AND (f.fees_from_date >= '".date("Y-m-d",strtotime($_REQUEST['batch_start_date']))."' AND f.fees_to_date <= '".date("Y-m-d",strtotime($_REQUEST['batch_end_date']))."' AND i.open_from_date <= '".$current_date."') ";
			$condition .= " AND (f.fees_from_date >= '".date("Y-m-d",strtotime($_REQUEST['batch_start_date']))."'  AND i.open_from_date <= '".$current_date."') ";
		}
		
		$result = $this->promotemodel->getCenterFees($condition,$_REQUEST['feesselectiontype']);
		// echo $this->db->last_query();
		$option = '';
		$fees_id = '';
		
		if(isset($_REQUEST['fees_id']) && !empty($_REQUEST['fees_id'])){
			$fees_id = $_REQUEST['fees_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]['fees_id'] == $fees_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]['fees_id'].'" '.$sel.' >'.$result[$i]['fees_name'].' </option>';
			}
		}
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getFeesDetails(){
		$condition = "fees_master_id='".$_REQUEST['fees_id']."' && f.status='Active' "; 
		$result = $this->promotemodel->getFeesDetails($condition);
		foreach ($result as $key => $value) {
			$getGstDetail = $this->promotemodel->getdata1("tbl_gst_master", "gst_master_id, tax", "status='Active' &&  gst_master_id='".$result[$key]['gst_tax_id']."'");
			$result[$key]['tax'] = $getGstDetail[0]['tax'];
		}
		// print_r($result);exit();
		echo json_encode(array("status"=>"success","result"=>$result));
		exit;
	}

	function getInquiryDetails(){
		$result = $this->promotemodel->getDetails("tbl_inquiry_master",$_REQUEST['inquiry_master_id'],"inquiry_master_id");

		$getStateName = $this->promotemodel->getDetails("tbl_states",$result[0]->state_id,"state_id");
		$getCityName = $this->promotemodel->getDetails("tbl_cities",$result[0]->city_id,"city_id");
		$getLeadData = $this->promotemodel->getDetails("tbl_inquiry_master",$result[0]->lead_id,"lead_id");
		if(!empty($getLeadData)){
			$getLeadData[0]->created_on = date("Y-m-d",strtotime($getLeadData[0]->created_on));
		}
		echo json_encode(array("status"=>"success","result"=>$result,'getStateName'=>$getStateName,'getCityName'=>$getCityName,"getLeadData"=>$getLeadData));
		exit;
	}

	function getchildInfo(){
		$result = $this->promotemodel->getDetails("tbl_student_master",$_REQUEST['student_id'],"student_id");
		// print_r($result);
		// exit();
		$otherresult = $this->promotemodel->getDetails("tbl_student_details",$_REQUEST['student_id'],"student_id");
		$getStateName = $this->promotemodel->getDetails("tbl_states",$result[0]->state_id,"state_id");
		$getCityName = $this->promotemodel->getDetails("tbl_cities",$result[0]->city_id,"city_id");
		echo json_encode(array("status"=>"success","result"=>$result,"otherresult"=>$otherresult,'getStateName'=>$getStateName,'getCityName'=>$getCityName));
		exit;
	}

	function getSiblingName(){
		$result = $this->promotemodel->getDetails("tbl_student_master",$_REQUEST['enrollment_no'],"enrollment_no");
		// print_r($result);
		// exit();
		echo json_encode(array("status"=>"success","result"=>$result));
		exit;
	}

	function getStudents(){
		// print_r($_POST['student_id']);
	
		$condition =' ';
		$condition = " ps.promoted_academic_year_id = '".$_POST['academic_year_id']."' AND ps.zone_id = '".$_POST['zone_id']."' AND ps.category_id = '".$_POST['category_id']."' AND ps.course_id = '".$_POST['course_id']."' AND ps.batch_id='".$_POST['batch_id']."'";
		$result = $this->promotemodel->getStudents($condition,$_POST['promoted_academic_year_id']);

		
		$option='';
		if(!empty($result)){
			$sel ='';
			for($i=0;$i<sizeof($result);$i++){
				if(!empty($_POST['student_id'])){
					$sel =($result[$i]['student_id'] == $_POST['student_id']) ? 'selected="selected" ' : '';
				}
				$option .= '<option value="'.$result[$i]['student_id'].'" '.$sel.' >'.$result[$i]['student_first_name'].' '.$result[$i]['student_last_name'].'</option>';
			}
		}
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}	

	function getStudent(){
		$condition =' ';
		$condition = " ps.promoted_academic_year_id = '".$_POST['academic_year_id']."' AND ps.zone_id = '".$_POST['zone_id']."' AND ps.category_id = '".$_POST['category_id']."' AND ps.course_id = '".$_POST['course_id']."' AND ps.batch_id='".$_POST['batch_id']."'";
		$result = $this->promotemodel->getStudent($condition,$_POST['promoted_academic_year_id']);

		
		$option='';
		if(!empty($result)){
			$sel ='';
			for($i=0;$i<sizeof($result);$i++){
				if(!empty($_POST['student_id'])){
					$sel =($result[$i]['student_id'] == $_POST['student_id']) ? 'selected="selected" ' : '';
				}
				$option .= '<option value="'.$result[$i]['student_id'].'" '.$sel.' >'.$result[$i]['student_first_name'].' '.$result[$i]['student_last_name'].'</option>';
			}
		}
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}


	function submitForm()
	{ 
		// print_r($_POST);
		// exit();
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
		/* 	$condition = "zone_id='".$_POST['zone_id']."' &&  center_id='".$_POST['center_id']."'  &&  inquiry_master_id='".$_POST['inquiry_master_id']."' ";
			if(isset($_POST['student_id']) && $_POST['student_id'] > 0)
			{
				$condition .= " &&  student_id != ".$_POST['student_id'];
			}
			
			$check_name = $this->promotemodel->getdata("tbl_student_master",$condition);
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			} */

			/* if($_POST['batch_start_date']!= '' && $_POST['batch_end_date'] != ''){
				if(strtotime($_POST['programme_start_date']) >= strtotime($_POST['batch_start_date']) && strtotime($_POST['programme_start_date']) <= strtotime($_POST['batch_end_date'])){
				}
				else{
					echo json_encode(array("success"=>"0",'msg'=>'Program start date not correct!'));
					exit;
				}
				if(strtotime($_POST['programme_end_date']) >= strtotime($_POST['batch_start_date']) && strtotime($_POST['programme_end_date']) <= strtotime($_POST['batch_end_date'])){
				}
				else{
					echo json_encode(array("success"=>"0",'msg'=>'Program end date not correct!'));
					exit;
				}
			} */
			if(strtotime($_POST['programme_start_date']) > strtotime($_POST['programme_end_date'])){
				echo json_encode(array("success"=>"0",'msg'=>'Program start date not greater than program end date!'));
				exit;
			}

			if(strtotime($_POST['current_academic_year_id']) == strtotime($_POST['promoted_academic_year_id'])){
				echo json_encode(array("success"=>"0",'msg'=>'promoted academic year and from Academic year should not be same !'));
				exit;
			}

		/* 	if($_POST['has_attended_preschool_before'] == 'Yes'){
				if($_POST['preschool_name'] == '' || $_POST['preschool_name'] == null){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Preschool Name!'));
					exit;
				}
			} */
			
			if (!empty($_POST['promoted_id'])) {

				foreach ($_POST['student_id'] as $key => $value) {
					$data_array1 = array();
					$data_array1['student_id'] = $value;
					$data_array1['current_academic_year_id'] = (!empty($_POST['current_academic_year_id'])) ? $_POST['current_academic_year_id'] : '';
					$data_array1['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
					$data_array1['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : '';
					$data_array1['batch_id'] = (!empty($_POST['batch_id'])) ? $_POST['batch_id'] : '';
					$data_array1['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
					$data_array1['promoted_status'] = 'promoted';
					$data_array1['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
					$data_array1['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
					$data_array1['promoted_academic_year_id'] = (!empty($_POST['promoted_academic_year_id'])) ? $_POST['promoted_academic_year_id'] : '';
					$data_array1['promoted_course_id'] = (!empty($_POST['promoted_course_id'])) ? $_POST['promoted_course_id'] : '';
					$data_array1['promoted_batch_id'] = (!empty($_POST['promoted_batch_id'])) ? $_POST['promoted_batch_id'] : '';

					$data_array1['programme_start_date'] = (!empty($_POST['programme_start_date'])) ? date("Y-m-d", strtotime($_POST['programme_start_date'])) : '';
					$data_array1['programme_end_date'] = (!empty($_POST['programme_end_date'])) ? date("Y-m-d", strtotime($_POST['programme_end_date'])) : '';
					$data_array1['promotion_date'] = (!empty($_POST['promotion_date'])) ? date("Y-m-d", strtotime($_POST['promotion_date'])) : '';
					$data_array1['created_on'] = date("Y-m-d H:i:s");
					$data_array1['created_by'] = $_SESSION["webadmin"][0]->user_id;
					$data_array1['updated_on'] = date("Y-m-d H:i:s");
					$data_array1['updated_by'] = $_SESSION["webadmin"][0]->user_id;
					$result1 = $this->promotemodel->updateRecord('tbl_promoted_student', $data_array1,'promoted_id',$_POST['promoted_id']);
				}
			}else {
				foreach ($_POST['student_id'] as $key => $value) {
					$data_array1 = array();
					$data_array1['student_id'] = $value;
					$data_array1['current_academic_year_id'] = (!empty($_POST['current_academic_year_id'])) ? $_POST['current_academic_year_id'] : '';
					$data_array1['promoted_status'] = 'promoted';
					$data_array1['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
					$data_array1['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : '';
					$data_array1['batch_id'] = (!empty($_POST['batch_id'])) ? $_POST['batch_id'] : '';
					$data_array1['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
					$data_array1['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
					$data_array1['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
					$data_array1['promoted_academic_year_id'] = (!empty($_POST['promoted_academic_year_id'])) ? $_POST['promoted_academic_year_id'] : '';
					$data_array1['promoted_course_id'] = (!empty($_POST['promoted_course_id'])) ? $_POST['promoted_course_id'] : '';
					$data_array1['promoted_batch_id'] = (!empty($_POST['promoted_batch_id'])) ? $_POST['promoted_batch_id'] : '';
					$data_array1['programme_start_date'] = (!empty($_POST['programme_start_date'])) ? date("Y-m-d", strtotime($_POST['programme_start_date'])) : '';
					$data_array1['promotion_date'] = (!empty($_POST['promotion_date'])) ? date("Y-m-d", strtotime($_POST['promotion_date'])) : '';


					
					$data_array1['programme_end_date'] = (!empty($_POST['programme_end_date'])) ? date("Y-m-d", strtotime($_POST['programme_end_date'])) : '';
					$data_array1['created_on'] = date("Y-m-d H:i:s");
					$data_array1['created_by'] = $_SESSION["webadmin"][0]->user_id;
					$data_array1['updated_on'] = date("Y-m-d H:i:s");
					$data_array1['updated_by'] = $_SESSION["webadmin"][0]->user_id;
					$result1 = $this->promotemodel->insertData('tbl_promoted_student', $data_array1, '1');
				}
			}
	
		
			if (!empty($result1)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.',
				));
			     /*if(empty($_POST['student_id'])) {
					$_SESSION['student_id'] = $result;
				 }*/
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}

	function submitFamilyInfoForm(){
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!isset($_SESSION['student_id']) && empty($_POST['student_id'])){
				echo json_encode(array("success"=>"0",'msg'=>'Enter Child Information First!'));
				exit;
			}
			
			if (isset($_SESSION['student_id']) || !empty($_POST['student_id'])) {
				$data_array = array();	
				if(isset($_SESSION['student_id'])){		
					$student_id = $_SESSION['student_id'];
				}else{
					$student_id = $_POST['student_id'];
				}
		 		
				$data_array['father_name'] = (!empty($_POST['father_name'])) ? $_POST['father_name'] : '';
				$data_array['father_prof'] = (!empty($_POST['father_prof'])) ? $_POST['father_prof'] : '';
				$data_array['father_languages'] = (!empty($_POST['father_languages'])) ? $_POST['father_languages'] : '';
				$data_array['father_nationality'] = (!empty($_POST['father_nationality'])) ? $_POST['father_nationality'] : '';
				$data_array['mother_name'] = (!empty($_POST['mother_name'])) ? $_POST['mother_name'] : '';
				$data_array['mother_prof'] = (!empty($_POST['mother_prof'])) ? $_POST['mother_prof'] : '';
				$data_array['mother_languages'] = (!empty($_POST['mother_languages'])) ? $_POST['mother_languages'] : '';
				$data_array['mother_nationality'] = (!empty($_POST['mother_nationality'])) ? $_POST['mother_nationality'] : '';

				$data_array['parent_name'] = (!empty($_POST['parent_name'])) ? $_POST['parent_name'] : '';
				$data_array['guardian'] = (!empty($_POST['guardian'])) ? $_POST['guardian'] : '';

				$data_array['sibling1_id'] = (!empty($_POST['sibling1_id'])) ? $_POST['sibling1_id'] : 0;
				$data_array['sibling2_id'] = (!empty($_POST['sibling2_id'])) ? $_POST['sibling2_id'] : 0;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$familydataresult = $this->promotemodel->updateRecord('tbl_student_master', $data_array,'student_id',$student_id);
				
			}
		
			if (!empty($familydataresult)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.',
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	}

	function submitContactInfoForm(){
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			if(!isset($_SESSION['student_id']) && empty($_POST['student_id']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Enter Child Information First!'));
				exit;
			}
			
			if (isset($_SESSION['student_id']) || !empty($_POST['student_id'])) {
				$data_array = array();	
				if(isset($_SESSION['student_id'])){		
					$student_id = $_SESSION['student_id'];
				}else{
					$student_id = $_POST['student_id'];
				}
		 		
				$data_array['present_address'] = (!empty($_POST['present_address'])) ? $_POST['present_address'] : '';
				$data_array['address1'] = (!empty($_POST['address1'])) ? $_POST['address1'] : '';
				$data_array['address2'] = (!empty($_POST['address2'])) ? $_POST['address2'] : '';
				$data_array['address3'] = (!empty($_POST['address3'])) ? $_POST['address3'] : '';
				$data_array['country_id'] = (!empty($_POST['country_id'])) ? $_POST['country_id'] : '';
				$data_array['state_id'] = (!empty($_POST['state_id'])) ? $_POST['state_id'] : '';
				$data_array['city_id'] = (!empty($_POST['city_id'])) ? $_POST['city_id'] : '';
				$data_array['pincode'] = (!empty($_POST['pincode'])) ? $_POST['pincode'] : '';
				$data_array['mother_home_contact_no'] = (!empty($_POST['mother_home_contact_no'])) ? $_POST['mother_home_contact_no'] : '';
				$data_array['mother_office_contact_no'] = (!empty($_POST['mother_office_contact_no'])) ? $_POST['mother_office_contact_no'] : '';
				$data_array['mother_mobile_contact_no'] = (!empty($_POST['mother_mobile_contact_no'])) ? $_POST['mother_mobile_contact_no'] : '';
				$data_array['mother_email_id'] = (!empty($_POST['mother_email_id'])) ? $_POST['mother_email_id'] : '';
				$data_array['father_home_contact_no'] = (!empty($_POST['father_home_contact_no'])) ? $_POST['father_home_contact_no'] : '';
				$data_array['father_office_contact_no'] = (!empty($_POST['father_office_contact_no'])) ? $_POST['father_office_contact_no'] : '';
				$data_array['father_mobile_contact_no'] = (!empty($_POST['father_mobile_contact_no'])) ? $_POST['father_mobile_contact_no'] : '';
				$data_array['father_email_id'] = (!empty($_POST['father_email_id'])) ? $_POST['father_email_id'] : '';
				$data_array['emergency_contact_name'] = (!empty($_POST['emergency_contact_name'])) ? $_POST['emergency_contact_name'] : '';
				$data_array['emergency_contact_tel_no'] = (!empty($_POST['emergency_contact_tel_no'])) ? $_POST['emergency_contact_tel_no'] : '';				$data_array['emergency_contact_mobile_no'] = (!empty($_POST['emergency_contact_mobile_no'])) ? $_POST['emergency_contact_mobile_no'] : '';				$data_array['emergency_contact_relationship'] = (!empty($_POST['emergency_contact_relationship'])) ? $_POST['emergency_contact_relationship'] : '';
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$familydataresult = $this->promotemodel->updateRecord('tbl_student_master', $data_array,'student_id',$student_id);
				
			}
		
			if (!empty($familydataresult)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.',
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	}

	function submitAutorizedInfoForm(){
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			if(empty($_POST['student_id']) && empty($_FILES["img_file1"]["name"]) && empty($_POST['prev_value1']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Please upload profile pic!'));
				exit;
			}

			if(!isset($_SESSION['student_id']) && empty($_POST['student_id']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Enter Child Information First!'));
				exit;
			}

			if (isset($_SESSION['student_id']) || !empty($_POST['student_id'])) {
				$data_array = array();	
				if(isset($_SESSION['student_id'])){		
					$student_id = $_SESSION['student_id'];
				}else{
					$student_id = $_POST['student_id'];
				}
				$img_file_value1 = "";
				if(isset($_FILES) && isset($_FILES["img_file1"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/authorized_person_image/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["img_file1"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("img_file1"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$img_file_value1 = $image_data['upload_data']['file_name'];
					}
					$image1 = $this->promotemodel->getFormdata($student_id);
					if(is_array($image1) && !empty($image1[0]->authperson1_img) && file_exists(DOC_ROOT_FRONT."/images/authorized_person_image/".$image1[0]->authperson1_img))
					{
						@unlink(DOC_ROOT_FRONT."/images/authorized_person_image/".$image1[0]->authperson1_img);
					}				
				}else{
					$img_file_value1 = $_POST['prev_value1'];
				}

				$img_file_value2 = "";
				if(isset($_FILES) && isset($_FILES["img_file2"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/authorized_person_image/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["img_file2"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("img_file2"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$img_file_value2 = $image_data['upload_data']['file_name'];
					}
					$image2 = $this->promotemodel->getFormdata($student_id);
					if(is_array($image2) && !empty($image2[0]->authperson2_img) && file_exists(DOC_ROOT_FRONT."/images/authorized_person_image/".$image2[0]->authperson2_img))
					{
						@unlink(DOC_ROOT_FRONT."/images/authorized_person_image/".$image2[0]->authperson2_img);
					}				
				}else{
					$img_file_value2 = $_POST['prev_value2'];
				}

				$img_file_value3 = "";
				if(isset($_FILES) && isset($_FILES["img_file3"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/authorized_person_image/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["img_file3"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("img_file3"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$img_file_value3 = $image_data['upload_data']['file_name'];
					}
					$image3 = $this->promotemodel->getFormdata($student_id);
					if(is_array($image3) && !empty($image3[0]->authperson3_img) && file_exists(DOC_ROOT_FRONT."/images/authorized_person_image/".$image3[0]->authperson3_img))
					{
						@unlink(DOC_ROOT_FRONT."/images/authorized_person_image/".$image3[0]->authperson3_img);
					}				
				}else{
					$img_file_value3 = $_POST['prev_value3'];
				}
				$data_array['authperson1_img'] = $img_file_value1;
				$data_array['authperson2_img'] = $img_file_value2;
				$data_array['authperson3_img'] = $img_file_value3;

				$data_array['auth_person1_to_collect'] = (!empty($_POST['auth_person1_to_collect'])) ? $_POST['auth_person1_to_collect'] : '';
				$data_array['auth_person1_to_collect_relation'] = (!empty($_POST['auth_person1_to_collect_relation'])) ? $_POST['auth_person1_to_collect_relation'] : '';
				$data_array['auth_person1_to_collect_gender'] = (!empty($_POST['auth_person1_to_collect_gender'])) ? $_POST['auth_person1_to_collect_gender'] : '';

				$data_array['auth_person2_to_collect'] = (!empty($_POST['auth_person2_to_collect'])) ? $_POST['auth_person2_to_collect'] : '';
				$data_array['auth_person2_to_collect_relation'] = (!empty($_POST['auth_person2_to_collect_relation'])) ? $_POST['auth_person2_to_collect_relation'] : '';
				$data_array['auth_person2_to_collect_gender'] = (!empty($_POST['auth_person2_to_collect_gender'])) ? $_POST['auth_person2_to_collect_gender'] : '';

				$data_array['auth_person3_to_collect'] = (!empty($_POST['auth_person3_to_collect'])) ? $_POST['auth_person3_to_collect'] : '';
				$data_array['auth_person3_to_collect_relation'] = (!empty($_POST['auth_person3_to_collect_relation'])) ? $_POST['auth_person3_to_collect_relation'] : '';
				$data_array['auth_person3_to_collect_gender'] = (!empty($_POST['auth_person3_to_collect_gender'])) ? $_POST['auth_person3_to_collect_gender'] : '';
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$authorizeddataresult = $this->promotemodel->updateRecord('tbl_student_master', $data_array,'student_id',$student_id);
				
			}
			if (!empty($authorizeddataresult)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.',
					'img1'=>$img_file_value1,
					'img2'=>$img_file_value2,
					'img3'=>$img_file_value3,
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	}

	function submitdocumentInfoForm(){
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			if(empty($_POST['student_id']) && empty($_FILES["doc_file1"]["name"]) && empty($_POST['doc_value1']) && isset($_POST['Submit']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Please upload immunization certificate!'));
				exit;
			}

			if(empty($_POST['student_id']) && empty($_FILES["doc_file2"]["name"]) && empty($_POST['doc_value2']) && isset($_POST['Submit']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Please upload birth certificate!'));
				exit;
			}

			if(empty($_POST['student_id']) && empty($_FILES["doc_file3"]["name"]) && empty($_POST['doc_value3']) && isset($_POST['Submit']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Please upload child photo!'));
				exit;
			}

			if(empty($_POST['student_id']) && empty($_FILES["doc_file4"]["name"]) && empty($_POST['doc_value4']) && isset($_POST['Submit']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Please upload  family photo!'));
				exit;
			}

			if(empty($_POST['student_id']) && empty($_FILES["doc_file5"]["name"]) && empty($_POST['doc_value5']) && isset($_POST['Submit']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Please upload address proof!'));
				exit;
			}

			if(empty($_POST['student_id']) && empty($_FILES["doc_file6"]["name"]) && empty($_POST['doc_value6']) && isset($_POST['Submit']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Please upload profile form!'));
				exit;
			}

			if(!isset($_SESSION['student_id']) && empty($_POST['student_id']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Enter Child Information First!'));
				exit;
			}
			
			if (isset($_SESSION['student_id']) || !empty($_POST['student_id'])) {
				$data_array = array();	
				if(isset($_SESSION['student_id'])){		
					$student_id = $_SESSION['student_id'];
				}else{
					$student_id = $_POST['student_id'];
				}
				$doc_value1 = "";
				if(isset($_FILES) && isset($_FILES["doc_file1"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/promote_documents/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["doc_file1"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("doc_file1"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$doc_value1 = $image_data['upload_data']['file_name'];
					}
					$doc1 = $this->promotemodel->getFormdata($student_id);
					if(is_array($doc1) && !empty($doc1[0]->duly_filled_promote_form) && file_exists(DOC_ROOT_FRONT."/images/promote_documents/".$doc1[0]->duly_filled_promote_form))
					{
						@unlink(DOC_ROOT_FRONT."/images/promote_documents/".$doc1[0]->duly_filled_promote_form);
					}				
				}else{
					$doc_value1 = $_POST['doc_value1'];
				}
				$doc_value2 = "";
				if(isset($_FILES) && isset($_FILES["doc_file2"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/promote_documents/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["doc_file2"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("doc_file2"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$doc_value2 = $image_data['upload_data']['file_name'];
					}
					$doc2 = $this->promotemodel->getFormdata($student_id);
					if(is_array($doc2) && !empty($doc2[0]->birth_certificate) && file_exists(DOC_ROOT_FRONT."/images/promote_documents/".$doc2[0]->birth_certificate))
					{
						@unlink(DOC_ROOT_FRONT."/images/promote_documents/".$doc2[0]->birth_certificate);
					}				
				}else{
					$doc_value2 = $_POST['doc_value2'];
				}
				$doc_value3 = "";
				if(isset($_FILES) && isset($_FILES["doc_file3"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/promote_documents/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["doc_file3"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("doc_file3"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$doc_value3 = $image_data['upload_data']['file_name'];
					}
					$doc3 = $this->promotemodel->getFormdata($student_id);
					if(is_array($doc3) && !empty($doc3[0]->profile_pic) && file_exists(DOC_ROOT_FRONT."/images/promote_documents/".$doc3[0]->profile_pic))
					{
						@unlink(DOC_ROOT_FRONT."/images/promote_documents/".$doc3[0]->profile_pic);
					}				
				}else{
					$doc_value3 = $_POST['doc_value3'];
				}
				$doc_value4 = "";
				if(isset($_FILES) && isset($_FILES["doc_file4"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/promote_documents/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["doc_file4"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("doc_file4"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$doc_value4 = $image_data['upload_data']['file_name'];
					}
					$doc4 = $this->promotemodel->getFormdata($student_id);
					if(is_array($doc4) && !empty($doc4[0]->family_photo) && file_exists(DOC_ROOT_FRONT."/images/promote_documents/".$doc4[0]->family_photo))
					{
						@unlink(DOC_ROOT_FRONT."/images/promote_documents/".$doc4[0]->family_photo);
					}				
				}else{
					$doc_value4 = $_POST['doc_value4'];
				}
				$doc_value5 = "";
				if(isset($_FILES) && isset($_FILES["doc_file5"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/promote_documents/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["doc_file5"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("doc_file5"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$doc_value5 = $image_data['upload_data']['file_name'];
					}
					$doc5 = $this->promotemodel->getFormdata($student_id);
					if(is_array($doc5) && !empty($doc5[0]->address_proof) && file_exists(DOC_ROOT_FRONT."/images/promote_documents/".$doc5[0]->address_proof))
					{
						@unlink(DOC_ROOT_FRONT."/images/promote_documents/".$doc5[0]->address_proof);
					}				
				}else{
					$doc_value5 = $_POST['doc_value5'];
				}
				$doc_value6 = "";
				if(isset($_FILES) && isset($_FILES["doc_file6"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/promote_documents/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["doc_file6"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("doc_file6"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$doc_value6 = $image_data['upload_data']['file_name'];
					}
					$doc6 = $this->promotemodel->getFormdata($student_id);
					if(is_array($doc6) && !empty($doc6[0]->duly_filled_child_profile_form) && file_exists(DOC_ROOT_FRONT."/images/promote_documents/".$doc6[0]->duly_filled_child_profile_form))
					{
						@unlink(DOC_ROOT_FRONT."/images/promote_documents/".$doc6[0]->duly_filled_child_profile_form);
					}				
				}else{
					$doc_value6 = $_POST['doc_value6'];
				}
				$data_array['duly_filled_promote_form'] = $doc_value1;
				$data_array['birth_certificate'] = $doc_value2;
				$data_array['profile_pic'] = $doc_value3;
				$data_array['family_photo'] = $doc_value4;
				$data_array['address_proof'] = $doc_value5;
				$data_array['duly_filled_child_profile_form'] = $doc_value6;

				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$documentresult = $this->promotemodel->updateRecord('tbl_student_master', $data_array,'student_id',$student_id);
				
			}
			if (!empty($documentresult)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.',
					'doc1'=>$doc_value1,
					'doc2'=>$doc_value2,
					'doc3'=>$doc_value3,
					'doc4'=>$doc_value4,
					'doc5'=>$doc_value5,
					'doc6'=>$doc_value6,
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	}
	
	function array_has_dupes($array) {
		return count($array) !== count(array_unique($array));
	}

	function submitfeesInfoForm(){
		// echo"<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$addmissionfees_id = 0;
			if(!isset($_SESSION['student_id']) && !$_POST['student_id']){
				echo json_encode(array("success"=>"0",'msg'=>'Enter Child Information First!'));
				exit;
			}
			//existing course fees checking
			if(!empty($_POST['fees_id'])){
				$exiting_course_feedetails = $this->promotemodel->getdata1("tbl_promote_fees","*","student_id='".$_POST['student_id']."' AND fees_id='".$_POST['fees_id']."' AND academic_year_id ='".$_POST['academic_year_id']."' AND fees_type='Class'");
				if(!empty($exiting_course_feedetails)){
					echo json_encode(array("success"=>"0",'msg'=>'The course fee is already assigned.'));
					exit;
				}
			}

			//existing group fees checking
			if(!empty($_POST['group_fees_id'])){
				foreach($_POST['group_fees_id'] as $key=>$val){
					$exiting_group_feedetails = $this->promotemodel->getdata1("tbl_promote_fees","*","student_id='".$_POST['student_id']."' AND fees_id='".$val."' AND academic_year_id ='".$_POST['academic_year_id']."' AND fees_type='Group'");
					if(!empty($exiting_group_feedetails)){
						echo json_encode(array("success"=>"0",'msg'=>'The Group '.($key+1).' fee is already assigned.'));
						exit;
					}
				}
			}

			if(!empty($_POST['group_fees_id'])){
    			$exiting_group_feedetails = $this->promotemodel->getdata1("tbl_promote_fees","*","student_id='".$_POST['student_id']."' AND fees_id='".$val."' AND academic_year_id ='".$_POST['academic_year_id']."' AND fees_type='Group' AND group_start_date between '".date("Y-m-d",strtotime($_POST['group_start_date'][$key]))."' AND '".date("Y-m-d",strtotime($_POST['group_end_date'][$key]))."' ");
    			if(!empty($exiting_group_feedetails)){
    				echo json_encode(array("success"=>"0",'msg'=>'The Group '.($key+1).' fee is already assigned.'));
    				exit;
    			}
				if($this->array_has_dupes($_POST['group_fees_id'])){
					echo json_encode(array("success"=>"0",'msg'=>'You have selected same fees for multiple groups'));
					exit;
				}
			}

			if(!empty($_POST['group_groupid'])){
				if($this->array_has_dupes($_POST['group_groupid'])){
					echo json_encode(array("success"=>"0",'msg'=>'You have selected same groups multiple time. '));
					exit;
				}
			}

			if($_POST['is_instalment'] == 'Yes'){
				if($_POST['no_of_installments'] > $_POST['max_installment']){
					echo json_encode(array("success"=>"0",'msg'=>'Installment No should not greater than Max Installment!'));
					exit;
				}

				for ($i=0; $i < $_POST['no_of_installments'] ; $i++) { 
					if($_POST['instalment_due_date'][$i] == ''){
						echo json_encode(array("success"=>"0",'msg'=>'Please Enter Installment Due date!'));
						exit;
					}
					if(isset($_POST['instalment_due_date'][$i+1])){
						if(strtotime($_POST['instalment_due_date'][$i]) > strtotime($_POST['instalment_due_date'][$i+1])){
							echo json_encode(array(
								'success' => '0',
								'msg' => 'Please re-check the payment due dates.'
							));
							exit;
						}
					}
				}
				$_POST['total_amount'] = $_POST['total_amountl'];
				$_POST['discount_type'] = $_POST['discount_type_installment'];
			}else{
				$_POST['total_amount'] = $_POST['total_amount'];
				$_POST['discount_type'] = $_POST['discount_type'];
			}

			//date validations  for course
			if(!empty($_POST['assign_promote_date'])){
				if($_POST['is_instalment'] == 'Yes'){
					if(!empty($_POST['instalment_due_date'])){
						foreach($_POST['instalment_due_date'] as $key=>$val){
							if(strtotime($val) < strtotime($_POST['assign_promote_date'])){
								echo json_encode(array("success"=>"0",'msg'=>'Course Installment date should not less than promote date.'));
								exit;
							}
						}
					}
				}else{
					if(strtotime($_POST['course_lumpsum_date']) < strtotime($_POST['assign_promote_date'])){
						echo json_encode(array("success"=>"0",'msg'=>'Course Lumpsum date should not less than promote date.'));
						exit;
					}
				}
			}

			//date validation for group
			if(!empty($_POST['is_group_installment'])){
				$group_date_validation_error = array();
				foreach($_POST['is_group_installment'] as $key=>$val){
					if($val == "lumpsum"){
						if(strtotime($_POST['group_lumpsum_date'][$key]) < strtotime($_POST['assign_promote_date'])){
							$group_date_validation_error[] = 'Group '.($key+1).' Lumpsum date should not less than promote date.';
						}
					}else{
						if(!empty($_POST['group_instalment_due_date'][$key])){
							$installment_date_error_flag = false;
							foreach($_POST['group_instalment_due_date'][$key] as $inkey=>$inval){
								if(strtotime($inval) < strtotime($_POST['assign_promote_date']) && $installment_date_error_flag == false){
									$group_date_validation_error[] = 'Group '.($key+1).' Installment date should not less than promote date.';
									$installment_date_error_flag = true;
								}
							}
						}
					}
				}
				if(!empty($group_date_validation_error)){
					$group_date_error = implode("<br>",$group_date_validation_error);
					echo json_encode(array("success"=>"0",'msg'=>$group_date_error));
					exit;
				}
			}

			//check the student already assign the group or not based on start and end date
			
			/* if(!empty($_POST['group_start_date']) && !empty($_POST['group_start_date'])){
				$group_start_end_date_error = array();
				foreach($_POST['group_start_date'] as $key=>$val){
					if(strtotime($_POST['group_start_date'][$key]) < strtotime($_POST['assign_promote_date'])){
						$group_start_end_date_error[] = "Group ".($key+1)." group start should not less than promote date.";
					}
					if(strtotime($_POST['group_end_date'][$key]) < strtotime($_POST['assign_promote_date'])){
						$group_start_end_date_error[] = "Group ".($key+1)." group end should not less than promote date.";
					}

					if(strtotime($_POST['group_end_date'][$key]) < strtotime($_POST['group_start_date'][$key])){
						$group_start_end_date_error[] = "Group ".($key+1)." group end should not less than group start date.";
					}
				}
				if(!empty($group_start_end_date_error)){
					$group_start_end_date_error_string = implode("<br>",$group_start_end_date_error);
					echo json_encode(array("success"=>"0",'msg'=>$group_start_end_date_error_string));
					exit;
				}
			}else{
				echo json_encode(array("success"=>"0",'msg'=>"Please select group start date and end date."));
				exit;
			}  */


			if(!empty($_POST['group_total_amount']) && !empty($_POST['group_fees_id'])){
				if(empty($_POST['group_start_date']) || empty($_POST['group_start_date'])){
					echo json_encode(array("success"=>"0",'msg'=>"Please select group start date and end date."));
					exit;
				}
			}

			$_POST['discount_amount_rs'] = (!empty($_POST['discount_amount_rsl']))?$_POST['discount_amount_rsl']:$_POST['discount_amount_rs'];

			if (isset($_SESSION['student_id']) || $_POST['student_id']) {
				$data_array = array();	
				if(isset($_SESSION['student_id'])){		
					$student_id = $_SESSION['student_id'];
				}else{
					$student_id = $_POST['student_id'];
				}

				$getCenter = $this->promotemodel->getDetails("tbl_student_master",$student_id,"student_id");
				$getCategory = $this->promotemodel->getDetails("tbl_student_details",$student_id,"student_id");
				if(!empty($getCenter) && !empty($getCategory)){
					$data_array['academic_year_id'] = $getCategory[0]->academic_year_id;
					$data_array['student_id'] = $student_id;
					$data_array['fees_id'] = (!empty($_POST['fees_id'])) ? $_POST['fees_id'] : '';
					$data_array['fees_type'] = 'Class';
					$data_array['zone_id'] = $getCenter[0]->zone_id;
					$data_array['center_id'] = $getCenter[0]->center_id;
					$data_array['category_id'] = $getCategory[0]->category_id;
					$data_array['course_id'] = $getCategory[0]->course_id;
					$data_array['batch_id'] = $getCategory[0]->batch_id;
					$data_array['group_id'] = 0 ;
					$data_array['is_instalment'] = (!empty($_POST['is_instalment'])) ? $_POST['is_instalment'] : '';
					$total_collected_amount = 0;
					$total_remainig_amount = 0;
					if($_POST['is_instalment'] == 'Yes'){
						$data_array['no_of_installments'] = (!empty($_POST['no_of_installments'])) ? $_POST['no_of_installments'] : '';
						$data_array['fees_amount_collected'] = 0;
						$data_array['fees_remaining_amount'] = (!empty($_POST['total_amount'])) ? $_POST['total_amount'] : 0;
					}else{
						$data_array['fees_amount_collected'] = 0;
						$data_array['fees_remaining_amount'] =  $_POST['total_amount'];
					}
					$total_discount_amount = 0;
					for($k=0;$k<sizeof($_POST['discount_amount']);$k++){
						if(!empty($_POST['discount_amount'][$k])){
							$total_discount_amount = floatval($total_discount_amount) + floatval($_POST['discount_amount'][$k]);
						}else{
							$total_discount_amount = $total_discount_amount;
						}
					}
					// echo  $total_discount_amount;exit;
					$data_array['fees_total_amount'] = (!empty($_POST['fees_total_amount'])) ? $_POST['fees_total_amount'] : '';
					$data_array['discount_amount'] = $total_discount_amount;
					$total_gst_amount = 0;
					for ($g=0; $g <sizeof($_POST['gst_amount']) ; $g++) {
						$total_gst_amount = floatval($total_gst_amount) + floatval($_POST['gst_amount'][$g]);
					}
					$data_array['gst_amount'] = $total_gst_amount;
					$data_array['total_amount'] = (!empty($_POST['total_amount'])) ? $_POST['total_amount'] : '';
					$data_array['fees_remark'] = (!empty($_POST['fees_remark'])) ? $_POST['fees_remark'] : '';
					$data_array['fees_approved_accepted_by_name'] = (!empty($_POST['fees_approved_accepted_by_name'])) ? $_POST['fees_approved_accepted_by_name'] : '';
					$data_array['created_on'] = date("Y-m-d H:i:s");
					$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
					$data_array['updated_on'] = date("Y-m-d H:i:s");
					$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
					
					$payfeesdataresult = $this->promotemodel->insertData('tbl_promote_fees', $data_array, '1');
					//saving in component
					if(!empty($payfeesdataresult)){
						//get fess components
						$component_condition = "f.status='Active' && i.fees_master_id='".$_POST['fees_id']."' ";
						$getFeesComponents = $this->promotemodel->getFeesComponents($component_condition);
						if($_POST['discount_type'] == "percentage"){
							$_POST['discount_amount_percentage'] = (!empty($_POST['discount_amount_percentage']))?$_POST['discount_amount_percentage']:$_POST['discount_amount_percentage_installment'];
						}else{
							$_POST['discount_amount_percentage'] = (!empty($_POST['discount_amount_percentage']))?$_POST['discount_amount_percentage']:$_POST['discount_amount_percentage_installment'];
							$discount_amount_per_component = 0;
							foreach($getFeesComponents as $key=>$val){
								if($val['is_discountable'] == "Yes"){
									$discount_amount_per_component = ($discount_amount_per_component + $val['component_fees']);
								}
							}
							$divide_value = ($discount_amount_per_component > 0)?$discount_amount_per_component:1;
							$_POST['discount_amount_percentage'] = (($_POST['discount_amount_percentage'] / $divide_value)*100);
						}
						if(!empty($getFeesComponents)){
							for($i=0; $i < sizeof($getFeesComponents); $i++){
								$getGstDetail = $this->promotemodel->getdata1("tbl_gst_master", "gst_master_id, tax", "status='Active' &&  gst_master_id='".$getFeesComponents[$i]['gst_tax_id']."' ");
								$fees_component = array();
								$fees_component['promote_fees_id'] = $payfeesdataresult;
								$fees_component['fees_id'] = $_POST['fees_id'];
								$fees_component['fees_component_id'] = $getFeesComponents[$i]['fees_component_master_id'];
								$fees_component['fees_component_id_value'] = $getFeesComponents[$i]['component_fees'];

								$lumpsum = false;
								$installment = false;
								if($getFeesComponents[$i]['is_discountable'] == 'Yes'){
									if($_POST['discount_amount_percentage'] != '' ){
										$discount_amount = (floatval($getFeesComponents[$i]['component_fees']) * floatval($_POST['discount_amount_percentage']))/100;
										$fees_component['discount_amount'] = $discount_amount;
										$fees_component['discount_percentage'] = $_POST['discount_amount_percentage'];
										$lumpsum = true;
									}else{
										$discount_amount = 0;
										$fees_component['discount_amount'] = 0;
										$fees_component['discount_percentage'] = 0;
									}

									if($getFeesComponents[$i]['component_fees'] < $discount_amount){
										$fees_component['discount_amount'] = 0;
										$fees_component['discount_percentage'] = 0;
									}
								}else{
									$fees_component['discount_amount'] = 0;
									$fees_component['discount_percentage'] = 0;
								}

								$discountamount = $getFeesComponents[$i]['component_fees'] - $fees_component['discount_amount'];
								if($getFeesComponents[$i]['is_gst'] == 'Yes'){
									$fees_component['gst_amount'] = (floatval($discountamount) * floatval($getGstDetail[0]['tax']))/100;
									$tax = explode(' ', $getGstDetail[0]['tax']);
									$fees_component['gst_percentage'] = $tax[0];
								}
								else{
									$fees_component['gst_amount'] = 0;
									$fees_component['gst_percentage'] = 0;
								}
								$fees_component['sub_total'] = floatval($discountamount) - floatval($fees_component['gst_amount']);
								$this->promotemodel->insertData('tbl_promote_fees_components', $fees_component, 1);
							}
						}
					}
					if($_POST['is_instalment'] == 'Yes'){
						$data_array2 = array();
						for($j=0;$j<$_POST['no_of_installments'];$j++){
							$data_array2['academic_year_id'] = $getCategory[0]->academic_year_id;
							$data_array2['student_id'] = $student_id;
							$data_array2['promote_fees_id'] = (!empty($payfeesdataresult)) ? $payfeesdataresult : '';
							$data_array2['fees_id'] = (!empty($_POST['fees_id'])) ? $_POST['fees_id'] : '';
							$data_array2['fees_type'] = 'Class';
							$installment_due_date = explode('-',$_POST['instalment_due_date'][$j]);
							$data_array2['instalment_due_date'] = (!empty($_POST['instalment_due_date'][$j])) ?  $installment_due_date[2].'-'.$installment_due_date[1].'-'.$installment_due_date[0] : '';
							$data_array2['instalment_amount'] = (!empty($_POST['instalment_amount'][$j])) ? $_POST['instalment_amount'][$j] : '';
							$data_array2['instalment_collected_amount'] = 0;
							$data_array2['instalment_remaining_amount'] = (!empty($_POST['instalment_amount'][$j])) ? $_POST['instalment_amount'][$j] : '';
							$data_array2['instalment_status'] = 'Pending';
							$data_array2['created_on'] = date("Y-m-d H:i:s");
							$data_array2['created_by'] = $_SESSION["webadmin"][0]->user_id;
							$data_array2['updated_on'] = date("Y-m-d H:i:s");
							$data_array2['updated_by'] = $_SESSION["webadmin"][0]->user_id;
							$result2 = $this->promotemodel->insertData('tbl_promote_fees_instalments', $data_array2, '1');
						}
					}else{
						$data_array2 = array();
						$data_array2['academic_year_id'] = $getCategory[0]->academic_year_id;
						$data_array2['student_id'] = $student_id;
						$data_array2['promote_fees_id'] = (!empty($payfeesdataresult)) ? $payfeesdataresult : '';
						$data_array2['fees_id'] = (!empty($_POST['fees_id'])) ? $_POST['fees_id'] : '';
						$data_array2['fees_type'] = 'Class';
						$data_array2['instalment_due_date'] = (!empty($_POST["course_lumpsum_date"]))?date('Y-m-d',strtotime($_POST["course_lumpsum_date"])):date("Y-m-d");
						$data_array2['instalment_amount'] = (!empty($_POST['total_amountl'])) ? $_POST['total_amountl'] : '';
						$data_array2['instalment_collected_amount'] = 0;
						$data_array2['instalment_remaining_amount'] = (!empty($_POST['total_amount'])) ? $_POST['total_amount'] : '';
						$data_array2['instalment_status'] = 'Pending';
						$data_array2['created_on'] = date("Y-m-d H:i:s");
						$data_array2['created_by'] = $_SESSION["webadmin"][0]->user_id;
						$data_array2['updated_on'] = date("Y-m-d H:i:s");
						$data_array2['updated_by'] = $_SESSION["webadmin"][0]->user_id;
						$result2 = $this->promotemodel->insertData('tbl_promote_fees_instalments', $data_array2, '1');
						
					}
					if(isset($_POST['paymentprocess'])){
						$flag =1 ;
					}
					else{
						$flag = 0;
					}
				}
				
			}

		// save group fees -------------------------------start
			if(!empty($_POST['group_total_amount']) && !empty($_POST['group_fees_id'])){
				foreach($_POST['group_total_amount'] as $key=>$val){
					if(!empty($_POST['group_fees_id'][$key])){
						$group_data = array();
						$student_id = $_POST['student_id'];
						$studentinfo = $this->promotemodel->getdata1("tbl_student_details", "academic_year_id, category_id,course_id", "student_id='".$student_id."'");
						$group_data['academic_year_id'] = $studentinfo[0]['academic_year_id'];
						$group_data['student_id'] = (!empty($_POST['student_id'])) ? $_POST['student_id'] : '';
						$group_data['fees_id'] = (!empty($_POST['group_fees_id'][$key])) ? $_POST['group_fees_id'][$key] : '';
						$group_data['fees_type'] = 'Group';
						$group_data['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
						$group_data['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
						$group_data['category_id'] = 0;
						$group_data['course_id'] = 0;
						$group_data['batch_id'] = (!empty($_POST['group_batchid'][$key])) ? $_POST['group_batchid'][$key] : '';
						$group_data['group_id'] = (!empty($_POST['group_groupid'][$key])) ? $_POST['group_groupid'][$key] : '';
						$group_data['group_start_date'] = (!empty($_POST['group_start_date'][$key])) ? date("Y-m-d",strtotime($_POST['group_start_date'][$key])) : NULL;
						$group_data['group_end_date'] = (!empty($_POST['group_end_date'][$key])) ? date("Y-m-d",strtotime($_POST['group_end_date'][$key])) : NULL;
						$frequency_count = 1;
						if(!empty($_POST['group_month_id'][$key])){
							$group_data['is_instalment'] = "Yes";
							$group_data['no_of_installments'] = 1;	
							//get frequency count
							$frequency_count_data = $this->promotemodel->getdata1("tbl_childactivity_fees_month","*","month_id='".$_POST['group_month_id'][$key]."'");
							if(!empty($frequency_count_data)){
								$frequency_count = $frequency_count_data[0]['month'];
							}
						}else{
							$group_data['is_instalment'] = (!empty($_POST['is_group_installment'][$key]) && $_POST['is_group_installment'][$key] == "lumpsum")?"No":"Yes";
							$group_data['no_of_installments'] = (!empty($_POST['group_installment_count'][$key]))?$_POST['group_installment_count'][$key]:"0";
						}

						$group_data['fees_total_amount'] = (!empty($_POST['group_fees_total_amount'][$key])) ? $_POST['group_fees_total_amount'][$key] : '';

						// set discount percentage and amount
						$_POST['group_discount_amount'][$key] = (!empty($_POST['group_fees_total_amount'][$key])) ? $_POST['group_fees_total_amount'][$key] : '';

						$component_condition = "f.status='Active' && i.fees_master_id='".$_POST['group_fees_id'][$key]."' ";
						$getFeesComponents = $this->promotemodel->getFeesComponents($component_condition);
						
						//convert discount into percentage
						$discount_amount_per_component = 0;
						foreach($getFeesComponents as $fkey=>$fval){
							$fval['component_fees'] = ($fval['component_fees'] * $frequency_count);
							if($fval['is_discountable'] == "Yes"){
								$discount_amount_per_component = ($discount_amount_per_component + $fval['component_fees']);
							}
						}
						$divide_value = ($discount_amount_per_component > 0)?$discount_amount_per_component:1;
						$_POST['group_discount_amount_percentage'][$key] = (($_POST['group_discount_amount_rs'][$key] / $divide_value)*100);

						$total_discount_amount = 0;
						if(!empty($getFeesComponents)){
							for($i=0; $i < sizeof($getFeesComponents); $i++){
								$fees_component = array();
								$getFeesComponents[$i]['component_fees'] = ($getFeesComponents[$i]['component_fees'] * $frequency_count);
								if($getFeesComponents[$i]['is_discountable'] == 'Yes'){
									$discount_amount = (floatval($getFeesComponents[$i]['component_fees']) * floatval($_POST['group_discount_amount_percentage'][$key]))/100;
									if($getFeesComponents[$i]['component_fees'] < $discount_amount){
										$fees_component['discount_amount'] = 0;
										$fees_component['discount_percentage'] = 0;
									}else{
										$fees_component['discount_amount'] = $discount_amount;
										$fees_component['discount_percentage'] = $_POST['group_discount_amount_percentage'][$key];
									}
								}else{
									$fees_component['discount_amount'] = 0;
									$fees_component['discount_percentage'] = 0;
								}
								$total_discount_amount = floatval($total_discount_amount) + floatval($fees_component['discount_amount']);
							}
						}

						$group_data['discount_amount'] = $total_discount_amount;
						$group_data['gst_amount'] = (!empty($_POST['group_total_gst_amount'][$key])) ? $_POST['group_total_gst_amount'][$key] : '';
						$group_data['total_amount'] = (!empty($_POST['group_total_amount'][$key])) ? $_POST['group_total_amount'][$key] : '';
						$group_data['fees_amount_collected'] = 0;
						$group_data['fees_remaining_amount'] =  (!empty($_POST['group_fees_total_amount'][$key])) ? $_POST['group_fees_total_amount'][$key] : '';
						$group_data['fees_remark'] = (!empty($_POST['group_fees_remark'][$key])) ? $_POST['group_fees_remark'][$key] : '';
						$group_data['fees_approved_accepted_by_name'] = (!empty($_POST['group_fees_approved_accepted_by_name'][$key])) ? $_POST['group_fees_approved_accepted_by_name'][$key] : '';
						
						
						$group_data['created_on'] = date("Y-m-d H:m:i");
						$group_data['created_by'] = $_SESSION["webadmin"][0]->user_id;
						$group_data['updated_on'] = date("Y-m-d H:m:i");
						$group_data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
						
						$result = $this->promotemodel->insertData('tbl_promote_fees', $group_data, 1);
						$getCategory = $this->promotemodel->getDetails("tbl_student_details",$student_id,"student_id");
	
						//save if installment fees present
						if(!empty($_POST['group_installment_count'][$key]) && !empty($_POST['group_instalment_amount'][$key])){
							foreach($_POST['group_instalment_amount'][$key] as $ikey=>$ival){
								$group_data_array2 = array();
								$group_data_array2['academic_year_id'] = $getCategory[0]->academic_year_id;
								$group_data_array2['student_id'] = $student_id;
								$group_data_array2['promote_fees_id'] = (!empty($result)) ? $result : '';
								$group_data_array2['fees_id'] = (!empty($_POST['group_fees_id'][$key])) ? $_POST['group_fees_id'][$key] : '';
								$group_data_array2['fees_type'] = 'Group';
								$group_data_array2['instalment_due_date'] = (!empty($_POST['group_instalment_due_date'][$key][$ikey]))?date("Y-m-d",strtotime($_POST['group_instalment_due_date'][$key][$ikey])):date("Y-m-d");
								$group_data_array2['instalment_amount'] = (!empty($_POST['group_instalment_amount'][$key][$ikey])) ? $_POST['group_instalment_amount'][$key][$ikey] : '';
								$group_data_array2['instalment_collected_amount'] = 0;
								$group_data_array2['instalment_remaining_amount'] = (!empty($_POST['group_instalment_amount'][$key][$ikey])) ? $_POST['group_instalment_amount'][$key][$ikey] : '';
								$group_data_array2['instalment_status'] = 'Pending';
								$group_data_array2['created_on'] = date("Y-m-d H:i:s");
								$group_data_array2['created_by'] = $_SESSION["webadmin"][0]->user_id;
								$group_data_array2['updated_on'] = date("Y-m-d H:i:s");
								$group_data_array2['updated_by'] = $_SESSION["webadmin"][0]->user_id;
								$result2 = $this->promotemodel->insertData('tbl_promote_fees_instalments', $group_data_array2, '1');
							}
						}else{
							$group_data_array2 = array();
							$group_data_array2['academic_year_id'] = $getCategory[0]->academic_year_id;
							$group_data_array2['student_id'] = $student_id;
							$group_data_array2['promote_fees_id'] = (!empty($result)) ? $result : '';
							$group_data_array2['fees_id'] = (!empty($_POST['group_fees_id'][$key])) ? $_POST['group_fees_id'][$key] : '';
							$group_data_array2['fees_type'] = 'Group';
							$group_data_array2['instalment_due_date'] = (!empty($_POST["group_lumpsum_date"][$key]))?date('Y-m-d',strtotime($_POST["group_lumpsum_date"][$key])):date("Y-m-d");
							$group_data_array2['instalment_amount'] = (!empty($_POST['group_total_amount'][$key])) ? $_POST['group_total_amount'][$key] : '';
							$group_data_array2['instalment_collected_amount'] = 0;
							$group_data_array2['instalment_remaining_amount'] = (!empty($_POST['group_total_amount'][$key])) ? $_POST['group_total_amount'][$key] : '';
							$group_data_array2['instalment_status'] = 'Pending';
							$group_data_array2['created_on'] = date("Y-m-d H:i:s");
							$group_data_array2['created_by'] = $_SESSION["webadmin"][0]->user_id;
							$group_data_array2['updated_on'] = date("Y-m-d H:i:s");
							$group_data_array2['updated_by'] = $_SESSION["webadmin"][0]->user_id;
							$result2 = $this->promotemodel->insertData('tbl_promote_fees_instalments', $group_data_array2, '1');
						}
						if(!empty($result)){
							//get fess components
							$component_condition = "f.status='Active' && i.fees_master_id='".$_POST['group_fees_id'][$key]."' ";
							$getFeesComponents = $this->promotemodel->getFeesComponents($component_condition);
							if(!empty($getFeesComponents)){
								//convert discount into percentage
								$discount_amount_per_component = 0;
								foreach($getFeesComponents as $fkey=>$fval){
									if($fval['is_discountable'] == "Yes"){
										$discount_amount_per_component = ($discount_amount_per_component + ($fval['component_fees'] * $frequency_count));
									}
								}
								$divide_value = ($discount_amount_per_component > 0)?$discount_amount_per_component:1;
								$_POST['group_discount_amount_percentage'][$key] = (($_POST['group_discount_amount_rs'][$key] / $divide_value)*100);
								
								for($i=0; $i < sizeof($getFeesComponents); $i++){
									$getFeesComponents[$i]['component_fees'] = ($getFeesComponents[$i]['component_fees'] * $frequency_count);
									$getGstDetail = $this->promotemodel->getdata1("tbl_gst_master", "gst_master_id, tax", "status='Active' &&  gst_master_id='".$getFeesComponents[$i]['gst_tax_id']."'");
									$fees_component = array();
									$fees_component['promote_fees_id'] = $result;
									$fees_component['fees_id'] = $_POST['group_fees_id'][$key];
									$fees_component['fees_component_id'] = $getFeesComponents[$i]['fees_component_master_id'];
									$fees_component['fees_component_id_value'] = $getFeesComponents[$i]['component_fees'];
									if($getFeesComponents[$i]['is_discountable'] == 'Yes'){
										$discount_amount = (floatval($getFeesComponents[$i]['component_fees']) * floatval($_POST['group_discount_amount_percentage'][$key]))/100;
										if($getFeesComponents[$i]['component_fees'] < $discount_amount){
											$fees_component['discount_amount'] = 0;
											$fees_component['discount_percentage'] = 0;
										}else{
											$fees_component['discount_amount'] = $discount_amount;
											$fees_component['discount_percentage'] = $_POST['group_discount_amount_percentage'][$key];
										}
									}else{
										$fees_component['discount_amount'] = 0;
										$fees_component['discount_percentage'] = 0;
									}
									$discountamount = $getFeesComponents[$i]['component_fees'] - $fees_component['discount_amount'];
									if($getFeesComponents[$i]['is_gst'] == 'Yes'){
										$fees_component['gst_amount'] = ($discountamount * $getGstDetail[0]['tax'])/100;
										$tax = explode(' ', $getGstDetail[0]['tax']);
										$fees_component['gst_percentage'] = $tax[0];
									}else{
										$fees_component['gst_amount'] = 0;
										$fees_component['gst_percentage'] = 0;
									}
									$fees_component['sub_total'] = ($discountamount) - $fees_component['gst_amount'];
									$this->promotemodel->insertData('tbl_promote_fees_components', $fees_component, 1);
								}
							}
						}
	
						//get existing linked group
						$existing_group_condition = " zone_id ='".$_POST['zone_id']."' AND center_id='".$_POST['center_id']."' AND academic_year_id='".$studentinfo[0]['academic_year_id']."' AND student_id='".$_POST['student_id']."' AND group_id='".$_POST['group_groupid'][$key]."' AND batch_id='".$_POST['group_batchid'][$key]."' ";
						$existing_linked_group = $this->promotemodel->getdata1("tbl_student_group","*",$existing_group_condition);
						$student_group_details_data = array();
						$student_group_details_data['group_start_date'] = (!empty($_POST['group_start_date'][$key])) ? date("Y-m-d",strtotime($_POST['group_start_date'][$key])) : NULL;
						$student_group_details_data['group_end_date'] = (!empty($_POST['group_end_date'][$key])) ? date("Y-m-d",strtotime($_POST['group_end_date'][$key])) : NULL;
						$student_group_details_data['status'] = "In-active";
						$student_group_details_data['updated_on'] = date("Y-m-d H:m:i");
						$student_group_details_data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
						if(empty($existing_linked_group)){
							//insert student group fees link data
							$student_group_details_data['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
							$student_group_details_data['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
							$student_group_details_data['academic_year_id'] = $studentinfo[0]['academic_year_id'];
							$student_group_details_data['student_id'] = (!empty($_POST['student_id'])) ? $_POST['student_id'] : '';
							$student_group_details_data['group_id'] = (!empty($_POST['group_groupid'][$key])) ? $_POST['group_groupid'][$key] : '';
							$student_group_details_data['batch_id'] = (!empty($_POST['group_batchid'][$key])) ? $_POST['group_batchid'][$key] : '';
							$student_group_details_data['created_on'] = date("Y-m-d H:m:i");
							$student_group_details_data['created_by'] = $_SESSION["webadmin"][0]->user_id;
							$this->promotemodel->insertData('tbl_student_group', $student_group_details_data, 1);
						}else{
							$this->promotemodel->updateRecord('tbl_student_group', $student_group_details_data, "student_group_id",$existing_linked_group[0]['student_group_id']);
						}
					}
				}
			}
		// save group fees -------------------------------end
			if (!empty($payfeesdataresult)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.',
					'payfeesdataresult' => $payfeesdataresult,
					'flag' => $flag,
					'student_id' => $student_id
				));
				exit;
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}else {
			return false;
		}
	}

	function groupfees($id = NULL){
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $varr;
			}
			$result = [];
			$result['student_id'] = $record_id;
			$result['student_detail'] = $this->promotemodel->getdata1("tbl_student_master", "student_first_name, student_last_name,zone_id,center_id", "student_id='".$record_id."'");
			$user_id = $_SESSION["webadmin"][0]->user_id;
			$condition = "i.user_id='".$user_id."'  ";
			$result['groups'] = $this->promotemodel->getCenterGroups($condition);
			$result['zones'] = $this->promotemodel->getDropdown1("tbl_zones","zone_id,zone_name");
			$result['months'] = $this->promotemodel->getMonths("tbl_childactivity_fees_month", "month_id, month_name,month,name", "status='Active' ");
			$result['login_name'] = $_SESSION["webadmin"][0]->first_name .' '. $_SESSION["webadmin"][0]->last_name;
			$this->load->view('template/header.php');
			$this->load->view('promote/groupFees', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	function getGroup(){
		$groups = $this->promotemodel->getAllGroups();
		$option = "<option value=''>Select Group</option>";
		if(!empty($groups)){
			foreach($groups as $key=>$val){
				$option .= "<option value='".$val->group_id."' >".$val->group_master_name."</option>";
			}
		}
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function getStudentAssignedGroups(){
		$groups = $this->promotemodel->getStudentAssignedGroups($_POST['student_id'],$_POST['zone_id'],$_POST['center_id']);
		$option = "<option value=''>Select Group</option>";
		if(!empty($groups)){
			foreach($groups as $key=>$val){
				$option .= "<option value='".$val->group_id."' >".$val->group_master_name."</option>";
			}
		}
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function getCenterUserGroupBatches(){
		$condition = "bm.group_id='".$_REQUEST['group_id']."' && bm.zone_id='".$_REQUEST['zone_id']."' && bm.center_id='".$_REQUEST['center_id']."' ";
		$result = $this->promotemodel->getCenterUserGroupBatches($condition);
		$option = '';
		$batch_id = '';
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$option .= '<option value="'.$result[$i]['batch_id'].'" >'.$result[$i]['batch_name'].'</option>';
			}
		}
		echo json_encode(array("status"=>"success","option"=>$option,"query"=>$this->db->last_query()));
		exit;
	}

	function getCenterFees(){
		$condition = " f.fees_selection_type='Group' && i.zone_id='".$_REQUEST['zone_id']."' && map.center_id='".$_REQUEST['center_id']."' && i.status='Active' && gc.group_master_id='".$_REQUEST['group_id']."' "; 
		if(!empty($_REQUEST['batch_id'])){
			$condition .= " && fbm.batch_id='".$_REQUEST['batch_id']."' ";
		}
		$result = $this->promotemodel->getCenterFees($condition,'Group');
		$query =  $this->db->last_query();
		$option = '';
		$fees_id = '';
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$option .= '<option value="'.$result[$i]['fees_id'].'" >'.$result[$i]['fees_name'].'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option,"query"=>$query));
		exit;
	}

	function getCenterFeesLevel(){
		$condition = "fcm.center_id='".$_REQUEST['center_id']."' && i.zone_id='".$_REQUEST['zone_id']."' && f.status ='Active'  ";
		$result = $this->promotemodel->getCenterFeesLevel($condition);
		$option = '';
		$fees_level_id = '';
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$option .= '<option value="'.$result[$i]->fees_level_id.'" >'.$result[$i]->fees_level_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function getMonthDiscount(){
		$result = $this->promotemodel->getdata1("tbl_fees_discount_master", "discount", "month_id='".$_REQUEST['month_id']."'");
		$frequencyDetails = $this->promotemodel->getdata1("tbl_childactivity_fees_month", "days", "month_id='".$_REQUEST['month_id']."'");
		echo json_encode(array("status"=>"success","option"=>$result,"days"=>(!empty($frequencyDetails[0]['days']))?$frequencyDetails[0]['days']:0));
		exit;
	}

	function paymentprocess($id = NULL){
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			$getFeesDataDisplay = array();
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $varr;
			}
			$result = [];
			$result['student_id'] = $record_id;
			$condition = "i.student_id='".$record_id."' ";
			$getFeesData = $this->promotemodel->getFeesData($condition);
			// echo "<pre>";print_r($getFeesData);exit;
			$total_instalment_amount = 0;
			$getFeesDatagroup_id =array();
			for ($i=0; $i < sizeof($getFeesData); $i++) { 
				$total_instalment_amount = $total_instalment_amount + $getFeesData[$i]['total_amount'];
				
				$condition1 = " i.promote_fees_id='".$getFeesData[$i]['promote_fees_id']."' ";
				$isPaymentDoneDetails = $this->promotemodel->isPaymentDone($condition1);
				if(empty($isPaymentDoneDetails)){
					$getFeesDataDisplay['installment'][] = $getFeesData[$i];
					$installment_result = $this->promotemodel->getdata1("tbl_promote_fees_instalments", "*", "promote_fees_id='".$getFeesData[$i]['promote_fees_id']."' ");
					$group_id_result = $this->promotemodel->getgroup_id("tbl_promote_fees", "group_id,course_id", "promote_fees_id='".$getFeesData[$i]['promote_fees_id']."' ");
					if(!empty($group_id_result) && !empty($installment_result)){
						foreach($installment_result as $ikey=>$ival){
							$installment_result[$ikey]['group_id'] = $group_id_result[0]['group_id'];
							$installment_result[$ikey]['course_id'] = $group_id_result[0]['course_id'];
						}
					}
					$getFeesDataDisplay['installmentcomponent'][] = $installment_result;
				}
			}
			$single_payment_array = array();
			if(!empty($getFeesDataDisplay['installmentcomponent'])){
				foreach($getFeesDataDisplay['installmentcomponent'] as $key=>$val){
					if(!empty($val)){
						foreach($val as $ckey=>$cval){
							if(!empty($getFeesDataDisplay['installment'])){
								if(in_array($cval['promote_fees_id'],array_column($getFeesDataDisplay['installment'], 'promote_fees_id'))){
									$temp_key = array_search($cval['promote_fees_id'],array_column($getFeesDataDisplay['installment'], 'promote_fees_id'));
									$cval['course_group'] = (!empty($getFeesDataDisplay['installment'][$temp_key]['course_name']))?$getFeesDataDisplay['installment'][$temp_key]['course_name']:$getFeesDataDisplay['installment'][$temp_key]['group_master_name'];
									
								}
							}
							$single_payment_array[]=$cval;
						
						}
					}
				}
			}
			usort($single_payment_array,function($a, $b){
				$t1 = strtotime($a['instalment_due_date']);
				$t2 = strtotime($b['instalment_due_date']);
				return $t1 - $t2;
			});

			//get student common details
			$result['student_common_details'] = $this->promotemodel->getdata1("tbl_student_master","*","student_id = '".$record_id."'");
			
			$result['paymentinfo_single_array'] = $single_payment_array;
			$result['paymentinfo'] = $getFeesDataDisplay;
			$result['login_name'] = $_SESSION["webadmin"][0]->first_name .' '. $_SESSION["webadmin"][0]->last_name;
			$result['total_instalment_amount'] = $total_instalment_amount;
			$this->load->view('template/header.php');
			$this->load->view('promote/paymentprocess', $result);
			$this->load->view('template/footer.php');
		}
		else {
			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function promoteGroupFees(){
		// echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$student_id = $_POST['student_id'];

			//get promote details
			$promoteDetails = array();
			if(!empty($_POST['promote_fees_id'][0])){
				$promoteDetails = $this->promotemodel->getpromoteDetails($_POST['promote_fees_id'][0]);
			}
			//check receipt date
			if(!empty($promoteDetails)){
				if(strtotime($_POST['receipt_date']) < strtotime($promoteDetails[0]['promote_date'])){
					echo json_encode(array("success"=>"0",'msg'=>"Receipt date should not less than promote date. [promote Date = ".date('d-M-Y',strtotime($promoteDetails[0]['promote_date']))."]"));
					exit;
				}
			}

			if($_POST['payment_mode'] == 'Cheque'){
				if(empty($_POST['bank_name'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Bank Name!'));
					exit;
				}
				if(empty($_POST['cheque_no'])){
					echo json_encode(array("success"=>"0",'msg'=>'EnterCheque No!'));
					exit;
				}
				if(empty($_POST['cheque_date'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Cheque Date!'));
					exit;
				} else{
					//cheque date equal or greater than promote date
					if(!empty($promoteDetails)){
						if(strtotime($_POST['cheque_date']) < strtotime($promoteDetails[0]['promote_date'])){
							echo json_encode(array("success"=>"0",'msg'=>'Cheque date should not less than promote date'));
							exit;
						}
					}

					//cheque date greater than or equal to receipt date
					if(!empty($_POST['receipt_date'])){
						if(strtotime($_POST['cheque_date']) < strtotime($_POST['receipt_date'])){
							echo json_encode(array("success"=>"0",'msg'=>'Cheque date should not less than 
							Receipt date'));
							exit;
						}
					}
				}
			}

			if($_POST['payment_mode'] == 'Netbanking'){
				if(empty($_POST['bank_name'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Bank Name!'));
					exit;
				}
				if(empty($_POST['transaction_id'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Transaction No!'));
					exit;
				}
				if(empty($_POST['transaction_date'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Transaction Date!'));
					exit;
				}else{
					//transaction date equal or greater than promote date
					if(!empty($promoteDetails)){
						if(strtotime($_POST['transaction_date']) < strtotime($promoteDetails[0]['promote_date'])){
							echo json_encode(array("success"=>"0",'msg'=>'Transaction date should not less than promote date'));
							exit;
						}
					}

					//transaction date greater than or equal to receipt date
					if(!empty($_POST['receipt_date'])){
						if(strtotime($_POST['transaction_date']) < strtotime($_POST['receipt_date'])){
							echo json_encode(array("success"=>"0",'msg'=>'Transaction date should not less than 
							Receipt date'));
							exit;
						}
					}
				} 
			}

			if(!empty($_POST['total_transaction_amount']) && array_sum($_POST['installment_collected_actual_amount']) != $_POST['total_transaction_amount']){
				echo json_encode(array("success"=>"0",'msg'=>'Installment amount and Receipt Amount are different.'));
				exit;
			}

			$promote_fees_id_array = array_unique(!empty($_POST['promote_fees_id'])?$_POST['promote_fees_id']:$_POST['promote_fees_id']);
			$group_components_result = array();
			$class_components_result = array();
			$instalment_collected_amount = array();
			$lumsum_collected_amount = array();

			//Class component fee insertion  for installment
			if(!empty($_POST['instalment_collected_typewise_amount']['Class'])){
				foreach($_POST['instalment_collected_typewise_amount']['Class'] as $key => $val){
					$total_collected_course_amount = 0;
					foreach($val as $k => $v){
						$total_collected_course_amount += $v;
					}
					
					//check the mandatory fees amount and change student status active or in active
					$status_data = array();
					$class_components_mandatory_result = $this->promotemodel->getClassFeesComponents(array_values($_POST['promote_fees_typewise_id']['Class'][$key])[0],$key,"Yes");
					if($total_collected_course_amount > 0){
						if(!empty($class_components_mandatory_result)){
							$sumofinstallments = array_sum(array_column($class_components_mandatory_result,"sub_total"));
							if($total_collected_course_amount >= $sumofinstallments){
								$status_data['status'] = "Active";
							}else{
								$status_data['status'] = "In-active";
							}
						}else{
							$status_data['status'] = "Active";
						}
					}else{
						$status_data['status'] = "In-active";
					}
					$this->promotemodel->updateRecord('tbl_student_master', $status_data, "student_id",$student_id);
					$this->promotemodel->updateRecord('tbl_student_details', $status_data, "student_id",$student_id);
					
					//get the components of the class 
					$class_components_result = $this->promotemodel->getClassFeesComponents(array_values($_POST['promote_fees_typewise_id']['Class'][$key])[0],$key);
					if(!empty($class_components_result)){
						$remaining_total_amount = 0;
						foreach($class_components_result as $ckey=>$cval){
							$collected_component_history_data = array();
							$collected_component_history_data['promote_fees_component_id'] = $cval['promote_fees_component_id'];

							//collected amount calculation
							$collected_amount = $total_collected_course_amount;
							$remaining_total_amount = $total_collected_course_amount - $cval['sub_total'];
							if(($remaining_total_amount) > 0){
								$collected_amount = $cval['sub_total'];
								$total_collected_course_amount = $remaining_total_amount;
							}else{
								$total_collected_course_amount = 0;
							}
							
							$collected_component_history_data['collected_amount'] = $collected_amount;
							$collected_component_history_data['remaining_amount'] = ($cval['sub_total'] - $collected_amount);
							$collected_component_history_data['collected_date'] = date("Y-m-d");
							$collected_component_history_data['is_last_entry'] = "Yes";
							$this->promotemodel->insertData('tbl_promote_fees_components_collected_history', $collected_component_history_data, 1);
						}
					}	
				}
			}

			//group component fee insertion for installment;
			if(!empty($_POST['instalment_collected_typewise_amount']['Group'])){
				foreach($_POST['instalment_collected_typewise_amount']['Group'] as $key => $val){
					$total_collected_group_amount = 0;
					foreach($val as $k => $v){
						$total_collected_group_amount += $v;
					}

					//check the mandatory fees amount and change student status active or in active
					if($total_collected_group_amount > 0){
						$group_components_mandatory_result = $this->promotemodel->getGroupFeesComponents(array_values($_POST['promote_fees_typewise_id']['Group'][$key])[0],$key,"Yes");
						//get promote fees data
						$group_promote_group_data = $this->promotemodel->getdata("tbl_promote_fees","promote_fees_id='".array_values($_POST['promote_fees_typewise_id']['Group'][$key])[0]."' ");
						$status_data = array();
						if(!empty($group_components_mandatory_result)){
							if(!empty(!empty($group_promote_group_data))){
								$sumofinstallments = array_sum(array_column($group_components_mandatory_result,"sub_total"));
								if($total_collected_group_amount >= $sumofinstallments){
									$status_data['status'] = "Active";
								}else{
									$status_data['status'] = "In-active";
								}
							}else{
								$status_data['status'] = "In-active";
							}
						}else{
							$status_data['status'] = "Active";
						}
						$group_status_condition = " zone_id='".$group_promote_group_data[0]['zone_id']."' AND center_id='".$group_promote_group_data[0]['center_id']."' AND academic_year_id='".$group_promote_group_data[0]['academic_year_id']."' AND  student_id='".$student_id."' AND group_id='".$key."' ";
						$this->promotemodel->updateRecordbyConditionArray("tbl_student_group",$status_data,$group_status_condition);
					}
					
					//get the components of the Group 
					$group_components_result = $this->promotemodel->getGroupFeesComponents(array_values($_POST['promote_fees_typewise_id']['Group'][$key])[0],$key);
					if(!empty($group_components_result)){
						$remaining_total_amount = 0;
						foreach($group_components_result as $ckey=>$cval){
							$collected_component_history_data = array();
							$collected_component_history_data['promote_fees_component_id'] = $cval['promote_fees_component_id'];

							//collected amount calculation
							$collected_amount = $total_collected_group_amount;
							$remaining_total_amount = $total_collected_group_amount - $cval['sub_total'];
							if(($remaining_total_amount) > 0){
								$collected_amount = $cval['sub_total'];
								$total_collected_group_amount = $remaining_total_amount;
							}else{
								$total_collected_group_amount = 0;
							}
							
							$collected_component_history_data['collected_amount'] = $collected_amount;
							$collected_component_history_data['remaining_amount'] = ($cval['sub_total'] - $collected_amount);
							$collected_component_history_data['is_last_entry'] = "Yes";
							$collected_component_history_data['collected_date'] = date("Y-m-d");
							$this->promotemodel->insertData('tbl_promote_fees_components_collected_history', $collected_component_history_data, 1);
						}
					}
				}
			}
			
			if(isset($_POST['promote_fees_instalment_id'])){
				$unique_promote_fees_id = array();
				if(!empty($_POST['promote_fees_id'])){
					$unique_promote_fees_id = array_unique($_POST['promote_fees_id']);
				}
				if(!empty($_POST['instalment_amount']) && !empty($unique_promote_fees_id)){
					foreach($unique_promote_fees_id as $fkey => $fval){
						$total_collected_amount = 0;
						$total_remainig_amount = 0;
						$total_mandatory_amount = 0;
						$promote_fees_id = $fval;
						foreach($_POST['instalment_amount'] as $key=>$val){
							if($_POST['promote_fees_id'][$key] == $fval){
								$total_collected_amount = $total_collected_amount + $_POST['installment_collected_actual_amount'][$key];
								$total_remainig_amount = $total_remainig_amount + $_POST['instalment_remaining_amount'][$key];
								$total_mandatory_amount = $total_mandatory_amount + $_POST['instalment_amount'][$key];
							}
						}
						$feesdata['fees_amount_collected'] = (!empty($total_collected_amount)) ? $total_collected_amount : 0;
						$feesdata['fees_remaining_amount'] = (!empty($total_remainig_amount)) ? $total_remainig_amount : 0;
						$feesdata['updated_on'] = date("Y-m-d H:m:i");
						$feesdata['updated_by'] = $_SESSION["webadmin"][0]->user_id;
						$this->promotemodel->updateRecord('tbl_promote_fees', $feesdata, "promote_fees_id",$promote_fees_id);
						foreach($_POST['promote_fees_instalment_id'] as $key=>$val){
							if($_POST['promote_fees_id'][$key] == $fval){
								$installmentdata['instalment_collected_amount'] = (!empty($_POST['installment_collected_actual_amount'][$key]) ? $_POST['installment_collected_actual_amount'][$key] : 0);
								$installmentdata['instalment_remaining_amount'] = (!empty($_POST['instalment_remaining_amount'][$key]) ? $_POST['instalment_remaining_amount'][$key] : $_POST['instalment_amount'][$key]);
								if($_POST['installment_collected_actual_amount'][$key] == '0'){
									$installmentdata['instalment_status'] = 'Pending';
								}else if($_POST['instalment_remaining_amount'][$key] == '0'){
									$installmentdata['instalment_status'] = 'Paid';
								}else if($_POST['instalment_amount'] > $_POST['installment_collected_actual_amount'][$key]){
									$installmentdata['instalment_status'] = 'Partial';
								}
								$installmentdata['updated_on'] = date("Y-m-d H:m:i");
								$installmentdata['updated_by'] = $_SESSION["webadmin"][0]->user_id;
								$this->promotemodel->updateRecord('tbl_promote_fees_instalments', $installmentdata, "promote_fees_instalment_id",$_POST['promote_fees_instalment_id'][$key]);
							}
						}
					}
				}
			}

			$result1 = $this->promotemodel->getdata1("tbl_promote_fees", "promote_fees_id,fees_remark,fees_approved_accepted_by_name,is_instalment", "student_id='".$_POST['student_id']."' ");
			for($r=0;$r<sizeof($result1);$r++){
				$result = $result1[$r]['promote_fees_id'];
				$isPaymentDone = $this->promotemodel->getdata1("tbl_fees_payment_details", "*", "promote_fees_id='".$result."' ");
				$installmentid = [];
				$installmentid = $this->promotemodel->getdata1("tbl_promote_fees_instalments", "promote_fees_instalment_id", "promote_fees_id='".$result."' ");
				if(empty($isPaymentDone)){
					if(!empty($installmentid)){
						$size = sizeof($installmentid);
						for($s=0;$s<$size;$s++){
							$payment_data = array();
							$payment_data['promote_fees_id'] = $result;
							$payment_data['promote_fees_instalment_id'] = $installmentid[$s]['promote_fees_instalment_id'];
							$payment_data['payment_mode'] = (!empty($_POST['payment_mode'])) ? $_POST['payment_mode'] : '' ;
							$payment_data['bank_name'] = (!empty($_POST['bank_name'])) ? $_POST['bank_name'] : '' ;
							if($_POST['payment_mode']=='Netbanking'){
								$payment_data['transaction_date'] = (!empty($_POST['transaction_date'])) ? date("Y-m-d", strtotime($_POST['transaction_date'])) : date('Y-m-d') ;
							}
							if($_POST['payment_mode']=='Cheque'){
								$payment_data['transaction_date'] = (!empty($_POST['cheque_date'])) ? date("Y-m-d", strtotime($_POST['cheque_date'])) : date('Y-m-d') ;
							}
							
							$payment_data['cheque_no'] = (!empty($_POST['cheque_no'])) ? $_POST['cheque_no'] : '' ;
							$payment_data['transaction_id'] = (!empty($_POST['transaction_id'])) ? $_POST['transaction_id'] : '' ;
							$payment_data['fees_remark'] = (!empty($result1[$r]['fees_remark'])) ? $result1[$r]['fees_remark'] : '';
							$payment_data['fees_approved_accepted_by_name'] = (!empty($result1[$r]['fees_approved_accepted_by_name'])) ? $result1[$r]['fees_approved_accepted_by_name'] : '';
							$payment_data['is_last_entry'] = 'Yes';
							
							$payment_data['created_on'] = date("Y-m-d H:m:i");
							$payment_data['created_by'] = $_SESSION["webadmin"][0]->user_id;
							$payment_data['updated_on'] = date("Y-m-d H:m:i");
							$payment_data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
							$save_data = $this->promotemodel->insertData('tbl_fees_payment_details', $payment_data, 1);
						}
					}
				}
			}

			if (!empty($save_data)) {
				echo json_encode(array('success' => '1','msg' => 'Record Added/Updated Successfully.','student_id' => $student_id));
				exit;
			}else{
				echo json_encode(array('success' => '0','msg' => 'Problem in data update.'));
				exit;
			}
		}else{
			return false;
		}
	}

	function array_formation($array,$label){
		$return_array = array();
		$tempkey = "";
		foreach($array as $key=>$val){
			$return_array[$val[$label]][] = $val;
		}
		return $return_array;
	}

	function getPayemtDetails(){
		if(!empty($_REQUEST['recipt_for'])){
			//update recipt print name
			$student_receipt_data = array();
			$student_receipt_data['guardian'] = $_REQUEST['recipt_for'];
			$this->promotemodel->updateRecord('tbl_student_master', $student_receipt_data,'student_id',$_REQUEST['student_id']);
		}
		$reciept_date = (!empty($_POST['receipt_date']))?date("Y-m-d",strtotime($_POST['receipt_date'])):date("Y-m-d");

		$studentinfo = $this->promotemodel->getdata1("tbl_promote_fees", "promote_fees_id,fees_type,category_id", "student_id='".$_REQUEST['student_id']."'");
		/* ---------------- single receiep strat ------------- */
		// single receipt
		$reciept_no = "";
		$receipt_amount = 0;
		$receipt_payment_mode = "";
		if(!empty($studentinfo)){
			$admition_id = array_column($studentinfo,"promote_fees_id");
			$admintion_fee_id = implode(',',$admition_id);
			$getPayemtDetails = $this->promotemodel->getPayemtDetails("i.promote_fees_id IN(".$admintion_fee_id.")");
			$class_name = "";
			if(!empty($getPayemtDetails)){
				$class_name = array_values(array_filter(array_column($getPayemtDetails,"course_name")))[0];
			}
			$count = count($getPayemtDetails);
			$student_name = (!empty($getPayemtDetails[0]['student_first_name'])) ? $getPayemtDetails[0]['student_first_name']." ".$getPayemtDetails[0]['student_last_name'] : '';
			$academic_year = (!empty($getPayemtDetails[0]['academic_year_master_name'])) ? $getPayemtDetails[0]['academic_year_master_name'] : '';
			$category_name = (!empty($getPayemtDetails[0]['categoy_name'])) ? $getPayemtDetails[0]['categoy_name'] : '';
			$course_name = (!empty($getPayemtDetails[0]['course_name'])) ? $getPayemtDetails[0]['course_name'] : '';
			$batch_name = (!empty($getPayemtDetails[0]['batch_name'])) ? $getPayemtDetails[0]['batch_name'] : '';
			$group_name = (!empty($getPayemtDetails[0]['group_master_name'])) ? $getPayemtDetails[0]['group_master_name'] : '';
			$center_name = (!empty($getPayemtDetails[0]['center_name'])) ? $getPayemtDetails[0]['center_name'] : '';
			$receipt_print_name = (!empty($getPayemtDetails[0]['receipt_print_name'])) ? $getPayemtDetails[0]['receipt_print_name'] : '';
			$center_address = (!empty($getPayemtDetails[0]['center_address'])) ? $getPayemtDetails[0]['center_address'] : '';
			$center_contact_no = (!empty($getPayemtDetails[0]['center_contact_no'])) ? $getPayemtDetails[0]['center_contact_no'] : '';
			$center_email_id = (!empty($getPayemtDetails[0]['center_email_id'])) ? $getPayemtDetails[0]['center_email_id'] : '';
			$center_code = (!empty($getPayemtDetails[0]['center_code'])) ? $getPayemtDetails[0]['center_code'] : '';
			for($i=0;$i< $count;$i++){
				$fees_id[] = (!empty($getPayemtDetails[$i]['fees_id'])) ? $getPayemtDetails[$i]['fees_id'] : '';
			}
			$getRecieptNo = $this->promotemodel->getdata_orderby_limit1("tbl_fees_payment_receipt", "fees_payment_receipt_id,receipt_no", "1=1", "order by fees_payment_receipt_id desc");	
			$reciept_id = 0;
			if(!empty($getRecieptNo)){
				$reciept_id =  substr($getRecieptNo[0]['receipt_no'], strrpos($getRecieptNo[0]['receipt_no'], '/') + 1) ;
			}
			$reciept_id = $reciept_id+1;
			if(!empty($center_code)){
				$reciept_no .= $center_code."/";
			}
			if(!empty($academic_year)){
				$reciept_no .= $academic_year."/";
			}
			$reciept_no .= $reciept_id;
			$uniquefee_id=array_unique($fees_id);
			$fee_ids =implode(',',$uniquefee_id);
			$date_v = date("d_m_Y_H_i_s",strtotime($reciept_date));
			$combine_receipt = '';
			$combine_receipt .= '<div style="width:750px;padding:0 30px;">
								<div style="margin:10px;text-align:center;margin:0 auto">
									<img src="https://app.montanapreschool.com/assets/images/aptech-montana-logo.jpg" alt="The Montana International Pre school Pvt. Ltd." title="The Montana International Pre school Pvt. Ltd." style="width:180px;"/>
								</div>
								<div>';
								//get center information
								$combine_receipt .= '<h4 style="text-align:center;color:#000;font-size:25px;margin:10px 0;padding:0;">'.$receipt_print_name.'</h4>
								<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><b>Centre Address:</b> '.$center_address.' Contact: '.$center_contact_no.', '.'<a href="mailto:'.$center_email_id.' " style="color:#000;text-decoration:none;">'.$center_email_id.'</a></p><p style="text-align:center;width:100%;margin-top:-10px;font-size:12px;">CIN No.: U80101MH2013PTC250962 | GST No.: 27AAECT8559Q1ZA</p>';
								
								$combine_receipt .= '</div>
								<div>
								<hr style="margin-top:-10px;">
								<h5 style="text-align: center;font-size:16px;margin:0px 0px 0px;padding:0;font-weight:300 !important;">Receipt ('.$academic_year.')</h5><br>
								<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
									<tr>
										<td>
											<table cellpadding="0" cellspacing="0" border="0" style="width:700px;">
												<tr>
													<th style="width:25%;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Date</th>
													<th style="width:25%;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">ENR Number</th>
													<th style="width:25%;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Name</th>
													<th style="width:25%;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000" colspan="2">Class</th>
												</tr>
												<tr>
													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.date("d-M-Y",strtotime($reciept_date)).'</strong></td>
													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getPayemtDetails[0]['enrollment_no'].'</strong></td>
													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$student_name.'</strong></td>';
													$combine_receipt .= '<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$class_name.'</strong></td>
													';
													
												$combine_receipt .= '</tr>
												<tr>
													<td colspan="3" style="padding:10px;text-align:left;font-size:14px">';
												if($getPayemtDetails[0]['guardian']=='father'){
													$combine_receipt.= '<p style="color:#000;font-size:14px;margin:3px 0;"><b>Father Name:</b> '.$getPayemtDetails[0]['father_name'].'</p>';
												}else if($getPayemtDetails[0]['guardian']=='mother'){
													$combine_receipt.= '<p style="color:#000;font-size:14px;margin:3px 0;"><b>Mother Name:</b> '.$getPayemtDetails[0]['mother_name'].'</p>';
												}else if($getPayemtDetails[0]['guardian']=='guadian'){
													$combine_receipt.= '<p style="color:#000;font-size:14px;margin:3px 0;"><b>Guadian Name: </b> '.$getPayemtDetails[0]['parent_name'].'</p>';
												
												}
												$combine_receipt.= '<p style="color:#000;font-size:14px;margin:3px 0;"><b>Address: </b>'.$getPayemtDetails[0]['present_address'].' ,'.$getPayemtDetails[0]['address1'].', '.$getPayemtDetails[0]['address2'].', '.$getPayemtDetails[0]['address3'].', '.$getPayemtDetails[0]['city_name'].' ,'.$getPayemtDetails[0]['state_name'].' ,'.$getPayemtDetails[0]['country_name'].', '.$getPayemtDetails[0]['pincode'].'</p>
													</td>						
															

													<td style="padding:10px;text-align:left;font-size:14px"> 
														<p style="color:#000;font-size:14px;margin:3px 0;"><b>Receipt No: </b>'.$reciept_no.'</p>
													</td>
												</tr>					
											</table>
										</td>
									</tr>
								</table>
								<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;" >
									<tr>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Component(s)</th>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Fee Type</th>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Class/Group</th>
										<th style="padding:10px;text-align:right;border-top:1px solid #000;border-bottom:1px solid #000">Amount</th>	
									</tr>';
									
									//get fess components
									$component_condition = " afc.fees_id IN(".$fee_ids.") && afcch.is_last_entry='Yes' && afcch.collected_amount > 0 && afc.promote_fees_id IN(".$admintion_fee_id.") ";
									$getallFeesComponents = $this->promotemodel->getFeesComponentsforReceipt($component_condition);

									//update collected history by receipt number
									$getallFeesComponentsForUpdate_condition = " afc.fees_id IN(".$fee_ids.") && afcch.is_last_entry='Yes' && afc.promote_fees_id IN(".$admintion_fee_id.") ";
									$getallFeesComponentsForUpdate = $this->promotemodel->getFeesComponentsforReceipt($getallFeesComponentsForUpdate_condition);
									if(!empty($getallFeesComponentsForUpdate)){
										foreach($getallFeesComponentsForUpdate as $key=>$val){
											$receipt_collected_data = array();
											$receipt_collected_data['receipt_no'] = $reciept_no;
											$this->promotemodel->updateRecord("tbl_promote_fees_components_collected_history", $receipt_collected_data,"component_collected_history_id",$val['component_collected_history_id']);
										}
									}

									if(!empty($getallFeesComponents)){
										/* -------------- combine fees for receipt start */
										$course_data = array();
										$group_data = array();
										//separate group and class fees
										foreach($getallFeesComponents as $key=>$val){
											if(!empty($val['course_name'])){
												$course_data[] = $val;
											}else{
												$group_data[]  = $val;
											}
										}
										//combine course fee component by receipt name
										$course_data_final_array = array();
										if(!empty($course_data)){
											$course_data = $this->array_formation($course_data,'receipt_name');
											$count = 0;
											foreach($course_data as $key=>$val){
												$totalVal = array_sum(array_column($val,"collected_amount"));
												$course_data_final_array[$count]['receipt_name'] = $val[0]['receipt_name'];
												$course_data_final_array[$count]['fees_type'] = $val[0]['fees_type'];
												$course_data_final_array[$count]['collected_amount'] = $totalVal;
												$course_data_final_array[$count]['course_name'] = $val[0]['course_name'];
												$course_data_final_array[$count]['group_master_name'] = $val[0]['group_master_name'];
												$count++;
											}
										}
									


										//combine group fee component by receipt name
										$group_data_final_array = array();
										if(!empty($group_data)){
											// multiple group formation
											$multiple_group_data =  $this->array_formation($group_data,'group_master_name');
											if(!empty($multiple_group_data)){
												$count = 0;
												foreach($multiple_group_data as $mgkey=>$mgval){
													$mgval = $this->array_formation($mgval,'receipt_name');
													foreach($mgval as $key=>$val){
														$totalVal = array_sum(array_column($val,"collected_amount"));
														$group_data_final_array[$count]['receipt_name'] = $val[0]['receipt_name'];
														$group_data_final_array[$count]['fees_type'] = $val[0]['fees_type'];
														$group_data_final_array[$count]['collected_amount'] = $totalVal;
														$group_data_final_array[$count]['course_name'] = $val[0]['course_name'];
														$group_data_final_array[$count]['group_master_name'] = $val[0]['group_master_name'];
														$count++;
													}
												}
											}
										}
										// echo "<pre>";print_r($group_data_final_array);exit;
										$getallFeesComponents = array_merge($course_data_final_array,$group_data_final_array);
										/* -------------- combine fees for receipt end */
										$sumofcomponent = 0;
										for($i=0; $i < sizeof($getallFeesComponents); $i++){
											$sumofcomponent += $getallFeesComponents[$i]['collected_amount'];
											$combine_receipt .= '<tr>						
												<td style="padding:10px;text-align:left;font-size:14px">'.$getallFeesComponents[$i]['receipt_name'].'</td>
												<td style="padding:10px;text-align:left;font-size:14px">'.$getallFeesComponents[$i]['fees_type'].' Fee</td>
												<td style="padding:10px;text-align:left;font-size:14px">'.(!empty($getallFeesComponents[$i]['group_master_name'])?$getallFeesComponents[$i]['group_master_name']:$getallFeesComponents[$i]['course_name']).'</td>
												<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getallFeesComponents[$i]['collected_amount'],2).'</td>
																	
											</tr>';
										}
									}
								
									$combine_receipt .= '<tr>
										<td style="padding:10px;text-align:left;font-size:14px" colspan="3"><strong>Total Amount</strong></td>
										
										<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofcomponent,2).'</strong></td>						
									</tr>';

									$componentrupees = (float)$sumofcomponent;
									$receipt_amount = number_format((float)$sumofcomponent, 2, '.', '');
									$receipt_payment_mode = $getPayemtDetails[0]['payment_mode'];
								
									$combine_receipt .='<tr>
									<td style="padding:10px;text-align:left;font-size:14px" colspan="1"><b>Total Amount (In Words): </b></td>
									<td style="padding:10px;text-align:left;font-size:14px" colspan="3">'.strtoupper($this->getIndianCurrency($componentrupees)).'</td>
									</tr>';
									

									$combine_receipt .= '</table>
								
								<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
													<tr>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Payment Mode:</strong> '.$getPayemtDetails[0]['payment_mode'].'</td>
														<td style="padding:10px;text-align:left;font-size:14px">';
														if($getPayemtDetails[0]['payment_mode'] != 'Cash' ){
															$combine_receipt .= '<strong>Bank Name: </strong>'.$getPayemtDetails[0]['bank_name'];
														}
												$combine_receipt .= '</td>
													</tr>';
												if($getPayemtDetails[0]['payment_mode'] == 'Cheque'){	
													$combine_receipt .= '<tr>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque No: </strong>'.$getPayemtDetails[0]['cheque_no'].'</td>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque Date: </strong>'.date("d-M-Y", strtotime($getPayemtDetails[0]['transaction_date'])).'</td>
													</tr>';
												}else if($getPayemtDetails[0]['payment_mode'] == 'Netbanking'){	
													$combine_receipt .= '<tr>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Transaction No: </strong>'.$getPayemtDetails[0]['transaction_id'].'</td>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Transaction Date: </strong>'.date("d-M-Y", strtotime($getPayemtDetails[0]['transaction_date'])).'</td>
													</tr>';
												}else{
													$combine_receipt .= '<tr>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>&nbsp;</strong></td>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>&nbsp;</strong></td>
													</tr>';
												}
												$combine_receipt .= '</table>';
									
								$combine_receipt .='<div><p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"> <b>Notes:</b> 1. Cheques Subject to REALISATION. 2. Fees once paid are Non-Refundable & Non-Transferable. 3. This Receipt must be produced when demanded. 4. This is electronically generated receipt, hence does not require signature. </p>
								</div><br><br>';
								if($getPayemtDetails[0]['payment_mode']=='Cheque' && (strtotime($getPayemtDetails[0]['transaction_date']) > strtotime(date("Y-m-d",strtotime($reciept_date))))){
									$combine_receipt .='<div><p style="text-align:center;color:#000;font-size:14px;"><b>“This receipt is issued against a Post Dated Cheque (PDC)” </b></p></div><br><br>';
								}	
			ini_set('memory_limit','100M');
			$date_v = date("d.m.y",strtotime($reciept_date));
			$receipt_file_name = "PaymentReceipt_".$reciept_id."_".$date_v.".pdf";
			$receipt_pdfFilePath = DOC_ROOT_FRONT_API."/images/payment_invoices/".$receipt_file_name;
			$file_path = FRONT_API_URL."/images/payment_invoices/".$receipt_file_name;	
			$this->load->library('M_pdf');
			$pdf = $this->m_pdf->load();
			$pdf = new mPDF('utf-8');
			$pdf->WriteHTML($combine_receipt); // write the HTML into the PDF
			$pdf->Output($receipt_pdfFilePath, 'F'); // save to file because we can	
		}
		/* ----------------- single recept end ---------------- */
		for ($s=0; $s <sizeof($studentinfo) ; $s++) { 
			$getbcNo = $this->promotemodel->getdata_orderby_limit1("tbl_fees_payment_receipt", "fees_payment_receipt_id", "1=1", "order by fees_payment_receipt_id desc");
				
			$bc_id = (!empty($getbcNo)) ? ($getbcNo[0]['fees_payment_receipt_id'] + 1) : 1;
			
			$bc_no = "";
			if(!empty($center_code) || !empty($academic_year)){
				$bc_no .= "BC".$bc_id."/".$academic_year."/".$center_code;
			}

			$getreceiptinfo = $this->promotemodel->getdata1("tbl_fees_payment_receipt", "fees_payment_receipt_id", "promote_fees_id='".$studentinfo[$s]['promote_fees_id']."'");

			if(empty($getreceiptinfo)){
				$getPayemtDetails = $this->promotemodel->getPayemtDetails("i.promote_fees_id='".$studentinfo[$s]['promote_fees_id']."' ");
				//print_r($getPayemtDetails);exit;
				$student_name = (!empty($getPayemtDetails[0]['student_first_name'])) ? $getPayemtDetails[0]['student_first_name']." ".$getPayemtDetails[0]['student_last_name'] : '';
				$academic_year = (!empty($getPayemtDetails[0]['academic_year_master_name'])) ? $getPayemtDetails[0]['academic_year_master_name'] : '';
				$category_name = (!empty($getPayemtDetails[0]['categoy_name'])) ? $getPayemtDetails[0]['categoy_name'] : '';
				$course_name = (!empty($getPayemtDetails[0]['course_name'])) ? $getPayemtDetails[0]['course_name'] : '';
				$batch_name = (!empty($getPayemtDetails[0]['batch_name'])) ? $getPayemtDetails[0]['batch_name'] : '';
				$group_name = (!empty($getPayemtDetails[0]['group_master_name'])) ? $getPayemtDetails[0]['group_master_name'] : '';
				$center_name = (!empty($getPayemtDetails[0]['center_name'])) ? $getPayemtDetails[0]['center_name'] : '';
				$receipt_print_name = (!empty($getPayemtDetails[0]['receipt_print_name'])) ? $getPayemtDetails[0]['receipt_print_name'] : '';
				$center_address = (!empty($getPayemtDetails[0]['center_address'])) ? $getPayemtDetails[0]['center_address'] : '';
				$center_contact_no = (!empty($getPayemtDetails[0]['center_contact_no'])) ? $getPayemtDetails[0]['center_contact_no'] : '';
				$center_email_id = (!empty($getPayemtDetails[0]['center_email_id'])) ? $getPayemtDetails[0]['center_email_id'] : '';
				$center_code = (!empty($getPayemtDetails[0]['center_code'])) ? $getPayemtDetails[0]['center_code'] : '';
				$intallment_yes_no = (!empty($getPayemtDetails[0]['is_instalment'])) ? $getPayemtDetails[0]['is_instalment'] : '';
				$installment_nos = (!empty($getPayemtDetails[0]['no_of_installments'])) ? $getPayemtDetails[0]['no_of_installments'] : '';
				$discount_amount = (!empty($getPayemtDetails[0]['discount_amount'])) ? $getPayemtDetails[0]['discount_amount'] : '';
				$gst_amount = (!empty($getPayemtDetails[0]['gst_amount'])) ? $getPayemtDetails[0]['gst_amount'] : '';
				$total_fees = (!empty($getPayemtDetails[0]['fees_total_amount'])) ? $getPayemtDetails[0]['fees_total_amount'] : '';
				$total_amount = (!empty($getPayemtDetails[0]['total_amount'])) ? $getPayemtDetails[0]['total_amount'] : '';
				$collected_fees = (!empty($getPayemtDetails[0]['fees_amount_collected'])) ? $getPayemtDetails[0]['fees_amount_collected'] : '';
				$remaining_fees = (!empty($getPayemtDetails[0]['fees_remaining_amount'])) ? $getPayemtDetails[0]['fees_remaining_amount'] : '';
				
				$_POST['ispromote'] = 'No';
				
				//create invoice
				ini_set('memory_limit','100M');
				$date_v = date("d.m.y",strtotime($reciept_date));
				$invoice_file_name = "BookingConfirmation_BC".$bc_id."_".$date_v.".pdf";
				$invoice_pdfFilePath = DOC_ROOT_FRONT_API."/images/payment_invoices/".$invoice_file_name;
				$file_path = FRONT_API_URL."/images/payment_invoices/".$invoice_file_name;
				// invoice format
				$invoice = '';
				$invoice  = '<html><body>';	
				$invoice .= '<div style="width:750px;padding:0 30px;">
									<div style="margin:10px;text-align:center;margin:0 auto">
									<img src="https://app.montanapreschool.com/assets/images/aptech-montana-logo.jpg" alt="The Montana International Pre school Pvt. Ltd." title="The Montana International Pre school Pvt. Ltd." style="width:180px;"/>
									</div>';
									$invoice .= '<div>
										<h4 style="text-align:center;color:#000;font-size:25px;margin:10px 0;padding:0;">'.$receipt_print_name.'</h4>
										<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><b>Centre Address:</b> '.$center_address.'</p>
										<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><a href="mailto:'.$center_email_id.' " style="color:#000;text-decoration:none;">'.$center_email_id.'</a></p><p style="text-align:center;width:100%;margin-top:-10px;font-size:12px;">CIN No.: U80101MH2013PTC250962 | GST No.: 27AAECT8559Q1ZA</p>
									</div>';
									
									$invoice .= '<hr style="1px dashed black">&nbsp;
									<h4 style="text-align: center;font-size:20px;margin:10px 0;padding:0">Booking Confirmation ('.$academic_year.')</h4>
									<table cellpadding="0" cellspacing="0" border="0" style="border:0px none;width:700px;">
										<tr>
											<td style="width:350px;padding:10px;text-align:left;">
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>BC Number: </strong>'.$bc_no.'</p>
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Child’s Name:</strong>'.$getPayemtDetails[0]['student_first_name'].' '.$getPayemtDetails[0]['student_last_name'].'</p>'; 

												if($getPayemtDetails[0]['guardian']=='father'){
													$invoice .=	'<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Father Name:</strong> '.$getPayemtDetails[0]['father_name'].'</p>';
												}else if($getPayemtDetails[0]['guardian']=='mother'){
													$invoice .=	'<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Mother Name: </strong>'.$getPayemtDetails[0]['mother_name'].'</p>';
												}else if($getPayemtDetails[0]['guardian']=='guadian'){
													$invoice .=	'<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Guadian Name:</strong> '.$getPayemtDetails[0]['parent_name'].'</p>';
												}
												$invoice .=	'<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Address: </strong>'.$getPayemtDetails[0]['present_address'].' ,'.$getPayemtDetails[0]['address1'].', '.$getPayemtDetails[0]['address2'].', '.$getPayemtDetails[0]['address3'].', '.$getPayemtDetails[0]['city_name'].' ,'.$getPayemtDetails[0]['state_name'].' ,'.$getPayemtDetails[0]['country_name'].', '.$getPayemtDetails[0]['pincode'].'</p>';
												if($getPayemtDetails[0]['course_name'] != ""){
													$invoice .= '<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Class/Group:</strong>'.$getPayemtDetails[0]['course_name'].'</p>';
												}else{
													$invoice .= '<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Class/Group:</strong>'.$getPayemtDetails[0]['group_master_name'].'</p>';
												}
										$invoice .= '</td>
											<td style="width:350px;padding:10px;text-align:left;" valign="top">
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>ENR Number:</strong>'.$getPayemtDetails[0]['enrollment_no'].'</p>
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Phone Number:</strong>'.$getPayemtDetails[0]['father_mobile_contact_no'].'</p> 
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Email ID:</strong>'.$getPayemtDetails[0]['father_email_id'].'</p> 
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Receipt Number: </strong>'.$reciept_no.'</p>
												
												<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Receipt Date: </strong>'.date("d-M-Y",strtotime($reciept_date)).'</p>
												
											</td>
											
										</tr>
									</table><br>';
									//fee component
									$invoice .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Details of Booking Confirmation.</h4>';
									$invoice .='<table width="100%" cellpadding="0" cellspacing="0" border="1" style="border:1px solid #000;width:700px;margin:0 auto;border-collapse: collapse;">
										<tr>
											<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Fee Component </th>
											<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Gross </th>
											<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Discount</th>
											<th style="padding:10px;text-align:right;border-top:1px solid #000;border-bottom:1px solid #000">Total</th>	
										</tr>';
										$getFeesComponentsinvoice = $this->promotemodel->getFeesComponentsforinvoice($getPayemtDetails[0]['promote_fees_id']);
										if(!empty($getFeesComponentsinvoice)){
											$sumofgross =0;
											$sumofdiscount =0;
											$sumofcomponent =0;
											for($i=0; $i < sizeof($getFeesComponentsinvoice); $i++){
												$sumofgross += $getFeesComponentsinvoice[$i]['fees_component_id_value'];
												$sumofdiscount += $getFeesComponentsinvoice[$i]['discount_amount'];
												$sumofcomponent += $getFeesComponentsinvoice[$i]['sub_total'];
												$invoice .= '<tr>						
													<td style="padding:10px;text-align:left;font-size:14px">'.$getFeesComponentsinvoice[$i]['fees_component_master_name'].'</td>
													<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getFeesComponentsinvoice[$i]['fees_component_id_value'],2).'</td>	
													<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getFeesComponentsinvoice[$i]['discount_amount'],2).'</td>
													<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getFeesComponentsinvoice[$i]['sub_total'],2).'</td>					
												</tr>';
											}
											$invoice .='<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Total</strong></td>
											<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofgross,2).'</strong></td>
											<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofdiscount,2).'</strong></td>
											<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofcomponent,2).'</strong></td>
											</tr>';
										}
										
										$invoice .='</table><br>';
									//installment
									$invoice .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Details of Planned Installment Payment.</h4>';
									$invoice .='<table width="100%" cellpadding="0" cellspacing="0" border="1" style="border:1px solid #000;width:700px;margin:0 auto;border-collapse: collapse;">
										<tr>
											<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Installment Number</th>
											<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Planned Installment Date</th>
											
											<th style="padding:10px;text-align:right;border-top:1px solid #000;border-bottom:1px solid #000">Planned Total Installment Amount</th>	
										</tr>';
										$getinstallmentinvoice = $this->promotemodel->getinstallmentforinvoice($getPayemtDetails[0]['promote_fees_id']);
										//print_r();exit;
										if(!empty($getinstallmentinvoice)){
											$cnt = 0;
											$sumofinstallment=0;
											$sumofinstallmentdue=0;
											for($i=0; $i < sizeof($getinstallmentinvoice); $i++){
												$sumofinstallment += $getinstallmentinvoice[$i]['instalment_amount'];
												$sumofinstallmentdue += $getinstallmentinvoice[$i]['instalment_remaining_amount'];
												$invoice .= '<tr>						
													<td style="padding:10px;text-align:right;font-size:14px">'.++$cnt.'</td>
													<td style="padding:10px;text-align:right;font-size:14px">'.date("d-M-Y", strtotime($getinstallmentinvoice[$i]['instalment_due_date'])).'</td>	
												
													<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getinstallmentinvoice[$i]['instalment_amount'],2).'</td>					
												</tr>';
											}
											$invoice .='<tr>
											<td colspan="2" style="padding:10px;text-align:left;font-size:14px"><strong>Total</strong></td>
											<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofinstallment,2).'</strong></td>
											
											</tr>';
										}
										$invoice .='</table><br>';
										$installmentrupees = (float)$sumofinstallment;
									
									$invoice .='<p style="font-size:13px;margin:4px 0;"><b>Total Amount (In Words): </b>'.strtoupper($this->getIndianCurrency($installmentrupees)).'
									</p><br>';
									//for Receipt Number
									$invoice .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Your Payment Details as on Date.</h4>';
									$invoice .='<table width="100%" cellpadding="0" cellspacing="0" border="1" style="border:1px solid #000;width:700px;margin:0 auto;border-collapse: collapse;">
										<tr>
											<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Receipt Number</th>
											<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Receipt Date</th>
											<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Receipt Amount</th>
												
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">'.$reciept_no.'</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">'.date("d-M-Y",strtotime($reciept_date)).'</td>
											<td style="padding:10px;text-align:right;font-size:14px;border:1px solid #000;">'.number_format($collected_fees,2).'</td>
																	
										</tr>
										<tr>
										<td colspan="2" style="padding:10px;text-align:left;font-size:14px"><strong>Total</strong></td>
											<td style="padding:10px;text-align:right;font-size:14px;border:1px solid #000;"><strong>'.number_format($collected_fees,2).'</strong></td>
																
										</tr>
									</table><br>';
									$rupees = (float)$collected_fees;
									
									$invoice .='<p style="font-size:13px;margin:4px 0;"><b>Total Amount (In Words): </b>'.strtoupper($this->getIndianCurrency($rupees)).'
									</p><br>';

									$invoice .= '<p style="font-size:13px;margin:4px 0;">Notes:</p>
									<p style="font-size:13px;margin:4px 0;">1)  Cheques Subject to REALISATION.</p>
									<p style="font-size:13px;margin:4px 0;">2) Fees once paid are Non-Refundable & Non-Transferable. </p>
									<p style="font-size:13px;margin:4px 0;">3) This Receipt must be produced when demanded.</p>
									<p style="font-size:13px;margin:4px 0;">4) This Booking Confirmation provides provisional promote to the child.</p>
									<p style="font-size:13px;margin:4px 0;">5) Ensure timely payments as mentioned in the Booking confirmation.</p>
									<p style="font-size:13px;margin:4px 0;">6) Parents are required to inform the school if there is any change in their address and telephone numbers.</p>
									<p style="font-size:13px;margin:4px 0;">7) The parents are responsible for timely drop and pick up of child.</p>
									<p style="font-size:13px;margin:4px 0;">8) Please refer detailed code of conduct and policy Guidelines sent on your mail.</p>

									
										<p style="font-size:13px;margin:13px 0;">For any feedback or suggestions please write to us at <a href="mailto:feedback@montanapreschool.com" style="color:#000;text-decoration:none;"><u>feedback@montanapreschool.com</u></a></p><br>
										<p style="font-size:13px;margin:13px 0;">Registered Office : 402, Sagar Tech Plaza, B wing, Andheri-Kurla Road, Sakinaka,
										Andheri East, Mumbai - 400 072 Maharashtra, India, </p>
										<p style="font-size:13px;margin:13px 0;"><a href="info@montanapreschool.com/" target="_blank" style="color:#000;text-decoration:none;">info@montanapreschool.com/</a></p>
										<p style="font-size:13px;margin:13px 0;"><a href="https://www.facebook.com/montanapreschool/" target="_blank" style="color:#000;text-decoration:none;">https://www.facebook.com/montanapreschool/</a><br>

										

										<p style="font-size:13px;margin:4px 0;">Disclaimer: Taxes will be charged extra, as applicable, on the date of payment.</p>
										</p>
									</div><br>';

				
				$invoice .=  '</body></html>';

				//mailbody start
				$message_body ='';
				$message_body  = '<html><body>';	
				$message_body .= '<div style="width:750px;padding:0 30px;">';
					$message_body .= '<div style="width:100%;float:left;">
					
									<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Dear '.$getPayemtDetails[0]['father_name'].',</b></p><br>

									<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;">Welcome to Aptech Montana International Preschool! Your child’s journey of learning and fun begins with us and as your child embarks on this journey, here are few essential guidelines that will help you.</p><br>

									<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;">Let’s begin!</p>
								
									<br>';
									$message_body .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">promote & Payment Information</h4>';
									$message_body .= '<div style="width:100%;float:left;">
					
									<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>ENR Number: </b>'.$getPayemtDetails[0]['enrollment_no'].'</p>
									<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Child’s Name: </b>'.$getPayemtDetails[0]['student_first_name'].' '.$getPayemtDetails[0]['student_last_name'].'</p>
									<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Course Name: </b>'.$getPayemtDetails[0]['course_name'].'</p>';

									if($getPayemtDetails[0]['guardian']=='father'){
										$message_body .='<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><strong>Father Name: </strong> '.$getPayemtDetails[0]['father_name'].'</p>';
									}else if($getPayemtDetails[0]['guardian']=='mother'){
										$message_body .='<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><strong>Mother Name: </strong>'.$getPayemtDetails[0]['mother_name'].'</p>';
									}else if($getPayemtDetails[0]['guardian']=='guadian'){
										$message_body .='<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><strong>Guadian Name: </strong> '.$getPayemtDetails[0]['parent_name'].'</p>';
									}
									$message_body .='<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><strong>E-Mail ID: </strong> '.$getPayemtDetails[0]['father_email_id'].'</p>
									<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Mobile Number:</b>'.$getPayemtDetails[0]['father_mobile_contact_no'].'</p>
									<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Centre Name: </b>'.$center_name.'</p>
									<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Course Fee (Rs.): </b>'.$total_amount.'</p>
									<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b><p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"><b>Total Amount Received till now towards Course (Rs.): </b>'.$collected_fees.'</p>';

								
									$message_body .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Important Communication:</h4>';

									$message_body .= '
									<p style="font-size:13px;margin:4px 0;">1) A copy of the Booking Confirmation is attached with this mail. It contains the details of the program in which your child is enrolled, program fees, payment details and instalment payment schedule. You are requested to please check the details mentioned in the Booking Confirmation carefully.</p>
									<p style="font-size:13px;margin:4px 0;">2)We request parents to pay the school fees either by cheque, DD or by net banking. Payment to be done in favour of “The Montana International Preschool Pvt Ltd ”.</p>
									<p style="font-size:13px;margin:4px 0;">3)  Parents can also pay the fees using payment gateway attached to the mobile app.</p>
									<p style="font-size:13px;margin:4px 0;">4)Please insist on computerized receipt on payment of fees. Aptech Montana International Preschool is not responsible/ nor recognizes any manual receipt issued by the school.</p>
									<p style="font-size:13px;margin:4px 0;">5) On completion of the Academic Year , program completion certificate will be issued by Aptech Montana International Preschool. Any other certificate issued is invalid.</p>
									<p style="font-size:13px;margin:4px 0;"> 6)In case of any discrepancies, reach out to our centre or our customer care <a href="mailto: feedback@montanapreschool.com"><u>feedback@montanapreschool.com</u></a>with your centre name, enrollment number, name and contact details.</p>';

									$message_body .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">“Montana Parent Connect” a unique Mobile App</h4><br>
									<p style="font-size:13px;margin:4px 0;">You are requested to kindly download the ‘Montana Parent Connect App’ from the <a href="https://play.google.com/store/apps/details?id=com.eduquotient.montana.parentconnect&hl=en">Google Play Store </a>or <a href="https://apps.apple.com/in/app/montana-parent-connect/id1470218146">App Store </a></p><br>
									
									<p style="font-size:13px;margin:4px 0;">The Montana Parent Connect App comes with some amazing features like LIVE CCTV feed, child attendance history, updates on school activities through notifications, picture gallery to view photos and videos of events, newsletters etc.</p><br>
									
									<p style="font-size:13px;margin:4px 0;"><b>Username:</b>'.$getPayemtDetails[0]['enrollment_no'].'</p>
									<p style="font-size:13px;margin:4px 0;"><b>Default Password:</b>password</p><br>';

									$message_body .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Student Kit:</h4><br>

									<p style="font-size:13px;margin:4px 0;">On securing promote of your at Aptech Montana International Preschool, you will receive a child kit as per the program enrolled, which contains the following items.</p><br>';

									$message_body .='<table width="50%" cellpadding="0" cellspacing="0" border="2" style="border:1px solid #000;width:700px;margin:0 auto;">';
									$message_body .='<thead>
											<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Sr. No.</th>
											<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">List of items</th>
											<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Quantity</th>
											<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Applicable for Program (s)</th>
											
										</thead>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	T shirt (Common)</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">		
											Short/Skirt</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, K1 & K2</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	3</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Cap</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, Child Care, K1 & K2</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	4</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Apron</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, Child Care, K1 & K2</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	5</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Shoe</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1 pair</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	6</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Bag</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, K1 & K2</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	7</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Socks</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2 pair</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">		
											Pre-Nursery, Nursery, K1 & K2</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	8</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	School Diary</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	9</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Assessment Card</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	10</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Bag ID Card</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Pre-Nursery, Nursery, K1 & K2</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	11</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Family ID Card</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, Child Care, K1 & K2</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	12</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Student ID Card</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, Child Care, K1 & K2</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	13</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">		
											ID holder with Lanyard</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	2</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery, Nursery, Child Care, K1 & K2</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	14</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	PN/NU Workbooks</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">1 set</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">Pre-Nursery & Nursery</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	15</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	K1/K2 book kit</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">		
											1 set</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">K1 & K2</td>
										</tr>
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	16</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Child Care Diary</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	1</td>
											<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">	Child Care</td>
										</tr>';
										$message_body .='</table>';

										$message_body .='<p style="font-size:13px;margin:4px 0;">Kindly acknowledge the receipt of the parent manual and provide your confirmation to adhere to it. Sign the Booking Confirmation document provided to you by your school. In case you haven’t received it, do request your centre to provide it to you at the earliest.</p><br>

									<p style="font-size:13px;margin:4px 0;">Attached herewith is the copy of the Parent Manual for your ready reference.</p><br>

									<p style="font-size:13px;margin:4px 0;"><b>Regards</b></p>
									<p style="font-size:13px;margin:4px 0;"><b>Aptech Montana International Preschool</b></p>';

									$message_body .=  '</div>';
									
									$message_body .=  '</body></html>';

				$this->load->library('M_pdf');
				$pdf = $this->m_pdf->load();
				$pdf = new mPDF('utf-8');
				$pdf->WriteHTML($invoice); // write the HTML into the PDF
				$pdf->Output($invoice_pdfFilePath, 'F'); // save to file because we can	
				
				//genrate invoice pdf
			
				$mail_body ='<div>
				<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;">Dear parent,</p><br>
				<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"> Find attached your BC & payment receipt.</p><br><br><br>
				<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"> Regards,</p><br>
				<p style="text-align:left;color:#000;font-size:14px;margin:3px 0;"> Team Aptech Montana
				</p><br>
				</div>';
				
				//insert invoice file
				$invoice_file = array();
				$invoice_file['promote_fees_id'] = $studentinfo[$s]['promote_fees_id'];
				$invoice_file['receipt_no'] = $reciept_no;
				$invoice_file['bc_no'] = $bc_no;
				$invoice_file['receipt_file'] = $receipt_file_name;
				$invoice_file['receipt_file_withoutgst'] = $invoice_file_name;
				$invoice_file['receipt_amount'] = $receipt_amount;
				$invoice_file['payment_mode'] = $receipt_payment_mode;
				
				$invoice_file['created_on'] = date("Y-m-d H:m:i",strtotime($reciept_date));
				$invoice_file['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$invoice_file['updated_on'] = date("Y-m-d H:m:i",strtotime($reciept_date));
				$invoice_file['updated_by'] = $_SESSION["webadmin"][0]->user_id;

				$result_insert = $this->promotemodel->insertData('tbl_fees_payment_receipt', $invoice_file, 1);
				if($result_insert){
					if(!empty($getPayemtDetails[0]['father_email_id'])){
						$get_receipt = $file_path = FRONT_API_URL."/images/payment_invoices/".$receipt_file_name;
						$get_invoice = $file_path = FRONT_API_URL."/images/payment_invoices/".$invoice_file_name;
						$get_manual = $file_path = FRONT_API_URL."/images/payment_invoices/ParentManual.pdf";
						$this->email->clear();
						$this->email->from("info@attoinfotech.in"); // change it to yours
						$subject = "";
						//uncommand
						$this->email->to($getPayemtDetails[0]['father_email_id']);
						//  $this->email->to("arul.selvam@attoinfotech.com");
						
						if($getPayemtDetails[$s]['fees_type']=="Class"){
							$subject = "Welcome to Aptech Montana International Preschool";
							$this->email->subject($subject);
							$this->email->message($message_body);
							$this->email->attach($get_receipt);
							$this->email->attach($get_invoice);
							$this->email->attach($get_manual);
						}else{
							$subject = " Your BC & Payment Receipt $reciept_no ";
							$this->email->subject($subject);
							$this->email->message($mail_body);
							$this->email->attach($get_receipt);
							$this->email->attach($get_invoice);
						};
						// $checkemail = $this->email->send();
						$this->email->clear(TRUE);
					}
				}
			}
		}

		echo json_encode(array("status_code" => "200", "msg" => "Information saved successfully.", "body" => $_REQUEST['student_id']));
		exit;
	} 

	function viewAllInvoice($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $varr;
			}
			$result = [];
			$result['student_id'] = $record_id;
			$student_detail = $this->promotemodel->getdata1("tbl_promote_fees", "promote_fees_id", "student_id='".$record_id."'");
			$component_condition = "f.student_id='".$record_id."' ";
			$result['invoice'] = $this->promotemodel->getStudentFeesInvoice($component_condition);
			// print_r($getInvoice);exit();
			// for ($i=0; $i <sizeof($student_detail) ; $i++) { 
			// 	$result['invoice'][] = $this->promotemodel->getdata1("tbl_fees_payment_receipt", "receipt_file", "promote_fees_id='".$student_detail[$i]['promote_fees_id']."'");
			// }
			// print_r($result);
			// print_r($_SESSION["webadmin"]);
			// exit();
			$this->load->view('template/header.php');
			$this->load->view('promote/invoice', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function export(){
		$get_result = $this->promotemodel->getExportRecords($_POST);
		
		if(!empty($get_result['query_result']) && count($get_result['query_result']) > 0)
		{
			$this->load->library('Excel');
			
			//Create new PHPExcel object
			$objPHPExcel = new PHPExcel();
			//Set properties
			$objPHPExcel->getProperties()->setCreator("Attoinfotech")
			->setLastModifiedBy("Attoinfotech")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Export Excel");
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
			
			// Rename sheet
			$objPHPExcel->getActiveSheet()->setTitle('OutPut-File');
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("A1", 'First Name', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("B1", 'Last Name', PHPExcel_Cell_DataType::TYPE_STRING);
			
			
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("C1", 'Enrollment No', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("D1", 'Zone', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("E1", 'Center', PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objPHPExcel->getActiveSheet()->getStyle("A1:M1")->getFont()->setBold(true);
			
			$j = 2;
			
			for($i=0;$i<sizeof($get_result['query_result']);$i++)
			{
				
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("A$j", $get_result['query_result'][$i]->student_first_name);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("B$j", $get_result['query_result'][$i]->student_last_name);
				
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("C$j", $get_result['query_result'][$i]->enrollment_no);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("D$j", $get_result['query_result'][$i]->zone_name);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("E$j", $get_result['query_result'][$i]->center_name);
				
				$j++;
			}
			
			// Redirect output to a client's web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="promote('.date('d-m-Y').').xls"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			
			$_SESSION['promote_export_success'] = "success";
			$_SESSION['promote_export_msg'] = "Records Exported Successfully.";

			redirect('promote');
			exit;
		}
		else
		{
			$_SESSION['contactus_export_success'] = "error";
			$_SESSION['contactus_export_msg'] = "No records found for export";	
			redirect('promote');
			exit;
		}
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->promotemodel->delrecord12("tbl_student_master","student_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	

	function getStudentCourse(){
		if(!empty($_POST['student_id'])){
			$course_details = $this->promotemodel->getStudentCourseDetails($_POST['student_id']);
			$course_name = (!empty($course_details[0]['course_name']))?$course_details[0]['course_name']:"";
			$promote_date = (!empty($course_details[0]['promote_date']))?date("d-m-Y",strtotime($course_details[0]['promote_date'])):date("d-m-Y");
			$programme_start_date = (!empty($course_details[0]['programme_start_date']))?date("d-m-Y",strtotime($course_details[0]['programme_start_date'])):date("d-m-Y");
			$programme_end_date = (!empty($course_details[0]['programme_end_date']))?date("d-m-Y",strtotime($course_details[0]['programme_end_date'])):date("d-m-Y");
			echo json_encode(array("status"=>true,"msg"=>"course found successfully.","course_name"=>$course_name,"promote_date"=>$promote_date,"programme_start_date"=>$programme_start_date,"programme_end_date"=>$programme_end_date));
			exit;
		}else{
			echo json_encode(array("status"=>false,"msg"=>"Student id not provided"));
			exit;
		}
	}

	function checkAllowFrequency(){
		if(!empty($_POST['fees_id'])){
			$feesDetails = $this->promotemodel->getdata("tbl_fees_master","fees_id='".$_POST['fees_id']."' ");
			if(!empty($feesDetails) && $feesDetails[0]['allow_frequency'] == "Yes"){
				echo json_encode(array("status"=>true,"msg"=>"Frequncy allowed"));
				exit;
			}else{
				echo json_encode(array("status"=>false,"msg"=>"Frequncy not allowed"));
				exit;
			}
		}else{
			echo json_encode(array("status"=>false,"msg"=>"group not selected"));
			exit;
		}
	}

	function getFeesLevelDropdown(){
		$options = "";
		if(!empty($_POST['student_id'])){
			$feesLevelData = $this->promotemodel->getFeesLevelDetails($_POST['student_id']);
			if(!empty($feesLevelData)){
				foreach($feesLevelData as $key=>$val){
					$options .= "<option value=".$val['fees_level_id'].">".$val['fees_level_name']."</option>";
				}
				echo json_encode(array("status"=>true,"msg"=>"group not selected","option"=>$options));
			}else{
				echo json_encode(array("status"=>false,"msg"=>"group not selected","option"=>$options));
			}
		}else{
			echo json_encode(array("status"=>false,"msg"=>"group not selected","option"=>$options));
			exit;
		}
	}

	function getIndianCurrency($number){
		$decimal = round($number - ($no = floor($number)), 2) * 100;
		$hundred = null;
		$digits_length = strlen($no);
		$i = 0;
		$str = array();
		$words = array(0 => '', 1 => 'one', 2 => 'two',
			3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
			7 => 'seven', 8 => 'eight', 9 => 'nine',
			10 => 'ten', 11 => 'eleven', 12 => 'twelve',
			13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
			16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
			19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
			40 => 'forty', 50 => 'fifty', 60 => 'sixty',
			70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
		$digits = array('', 'hundred','thousand','lakh', 'crore');
		while( $i < $digits_length ) {
			$divider = ($i == 2) ? 10 : 100;
			$number = floor($no % $divider);
			$no = floor($no / $divider);
			$i += $divider == 10 ? 1 : 2;
			if ($number) {
				$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
				$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
				if($digits[$counter] == "thousand"){
					$plural = "";
				}
				$str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
			} else $str[] = null;
		}
		$Rupees = implode('', array_reverse($str));
		$paise = ($decimal > 0) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
		return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise; 
	}

	function getFrequencyData(){
		$options = "<option value=''>Select Payment Frequency</option>";
		if(!empty($_POST['fees_id'])){
			$frequencyDetails = $this->promotemodel->getFrequencyData($_POST['fees_id']);
			if(!empty($frequencyDetails)){
				foreach($frequencyDetails as $key=>$val){
					$options .= "<option value=".$val['month_id'] ." month_frequency=".$val['month']." >".$val['name']."</option>";
				}
			}
		}
		echo json_encode(array("status"=>true,"option"=>$options));
		exit;
	}

	function getSearchLists(){
		if(!empty($_POST['value']) && !empty($_POST['column_name'])){
			$options = "";
			if($_POST['column_name']  == "zone_id"){
				$centerDetails = $this->promotemodel->getdata1("tbl_centers","center_id,center_name","status='Active' AND zone_id='".$_POST['value']."' ");
				$options = "<option value=''>Select Center</option>";
				if(!empty($centerDetails)){
					foreach($centerDetails as $key=>$val){
						$options .= "<option value='".$val['center_id']."'>".$val['center_name']."</option>"; 
					}
				}
				echo json_encode(array("status"=>true,"option"=>$options));
				exit;
			}else if($_POST['column_name']  == "category_id"){
				$result = $this->promotemodel->getOptions("tbl_courses",$_POST['value'],"category_id");
				$option = '';
				if(!empty($result)){
					for($i=0;$i<sizeof($result);$i++){
						// $sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
						$option .= '<option value="'.$result[$i]->course_id.'"  >'.$result[$i]->course_name.'</option>';
					}
				}
				echo json_encode(array("status"=>"success","option"=>$option));
				exit;

			}else if($_POST['column_name']  == "center_id"){
				$courseDetails = $this->promotemodel->getCenterMappedCourses($_POST['value']);
				$options = " <option value=''>Select Class</option>";
				if(!empty($courseDetails)){
					foreach($courseDetails as $key=>$val){
						$options .= "<option value='".$val['course_id']."'>".$val['course_name']."</option>"; 
					}
				}
				echo json_encode(array("status"=>true,"option"=>$options));
				exit;
			}
		}
	}
}
?>
