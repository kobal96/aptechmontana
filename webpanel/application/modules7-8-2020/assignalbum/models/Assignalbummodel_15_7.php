<?PHP
class Assignalbummodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}
	
	function getRecords($get=null){
		$table = "tbl_assign_album";
		$table_id = 'i.assign_album_id';
		$default_sort_column = 'i.assign_album_id';
		$default_sort_order = 'desc';
		
		$condition = "";
		$condition .= "  1=1 ";
		
		$colArray = array('z.zone_name','ct.center_name','i.album_selection_type','a.album_name','i.status');
		$searchArray = array('z.zone_name','ct.center_name','i.album_selection_type','a.album_name','i.status');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<4;$i++)
		{
			
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $searchArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		$this -> db -> select('i.*, z.zone_name, ct.center_name,a.album_name');
		$this -> db -> from('tbl_assign_album as i');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as ct', 'i.center_id  = ct.center_id', 'left');	
		$this -> db -> join('tbl_album as a', 'i.album_id  = a.album_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		$this -> db -> select('i.*, z.zone_name, ct.center_name,a.album_name');
		$this -> db -> from('tbl_assign_album as i');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as ct', 'i.center_id  = ct.center_id', 'left');	
		$this -> db -> join('tbl_album as a', 'i.album_id  = a.album_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);		
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
	}
	
	
	function getDropdown($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}

	function getDropdownCategory($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status="Active";
		$category_id = 1;
		$this -> db -> where("status",$status);
		$this -> db -> where("category_id",$category_id);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}

	function getFormdata($ID){
		
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_assign_album as i');
		$this->db->where('i.assign_album_id', $ID);
	
		$query = $this -> db -> get();
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}
	
	function getOptions($tbl_name,$tbl_id,$comp_id){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptionsGroupBy($tbl_name,$tbl_id,$comp_id,$tbl_id1,$comp_id1){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$this -> db -> where($comp_id1,$tbl_id1);
		$status="Active";
		$this -> db -> where("status",$status);
		$this -> db -> group_by("group_id");
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptionsGroup1($tbl_id,$comp_id){
		$this -> db -> select('g.group_master_id, g.group_master_name');
		$this -> db -> from('tbl_group_master as g');
		$this -> db -> join('tbl_batch_master as b', 'b.group_id  = g.group_master_id', 'left');
		$this -> db -> where($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("b.status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getOptions1($tbl_name,$tbl_id,$comp_id,$tbl_id1,$comp_id1){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$this -> db -> where($comp_id1,$tbl_id1);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptionsClass($tbl_name,$tbl_id,$comp_id){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$status="Active";
		$category_id = 1;
		$batchSelectionType = 'Class';
		$this -> db -> where("status",$status);
		$this -> db -> where("category_id",$category_id);
		$this -> db -> where("batch_selection_type",$batchSelectionType);
	
		$query = $this -> db -> get();
		// print_r($query);
		// exit();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptionsGroup($tbl_name,$tbl_id,$comp_id){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$status="Active";
		$batchSelectionType = 'Group';
		$this -> db -> where("status",$status);
		$this -> db -> where("batch_selection_type",$batchSelectionType);
	
		$query = $this -> db -> get();
		// print_r($query);
		// exit();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}


	function getdata($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }
	
	
	function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	}  
	
	function delrecord12($tbl_name,$tbl_id,$record_id,$status)
	{
		$data = array('status' => $status);
		 
		$this->db->where($tbl_id, $record_id);
		$this->db->update($tbl_name,$data); 
		 
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}	
}
?>
