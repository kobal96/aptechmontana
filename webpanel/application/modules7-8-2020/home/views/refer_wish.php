<?php if(empty($customer[0]['profile_pic'])){
          if($customer[0]['gender']=='male'){
            $customer[0]['profile_pic'] = 'default_avatar_male.jpg';
          }else{
            $customer[0]['profile_pic'] = 'default_avatar_female.jpg';
          }
        } ?>

<style>
.table-borderless > tbody > tr > td,
.table-borderless > tbody > tr > th,
.table-borderless > tfoot > tr > td,
.table-borderless > tfoot > tr > th,
.table-borderless > thead > tr > td,
.table-borderless > thead > tr > th {
    border: none;
}
select.error, textarea.error, input.error {
    color:#FF0000;
}
.my-error-class {
    color:#FF0000;  /* red */
}
.my-valid-class {
    color:#00CC00; /* green */
}
    
    
    </style>
    <form id="form-validate">
<table border="1" width="100%" style="width: inherit ;" class="table table-borderless">
    <tbody>
        <tr>
            <td rowspan="3"><img width="100px" height="100px" style="border-radius: 200px; " src="<?php echo FRONT_URL; ?>/customer_images/<?php echo $customer[0]['profile_pic']; ?>"></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            
            <td><b style="margin-top: 45%;"><?php echo $customer[0]['customer_first_name'].' '.$customer[0]['customer_last_name']; ?></b></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <?php if(is_array($refer_wish)){ ?>
        
        <tr>
            <td colspan="4"><b>Wish & changes</b></td>
        </tr>
        <?php if(!empty($refer_wish[0]['wish1']) && $refer_wish[0]['wish_amount1'] != 0){ ?>
        <tr>
            <td><b>Wish 1:</b></td>
            <td><?php echo $refer_wish[0]['wish1']; ?></td>
            <td><b>Wish amount:</b></td>
            <td><?php echo $refer_wish[0]['wish_amount1']; ?></td>
        </tr>
        <?php } ?>
        <?php if(!empty($refer_wish[0]['wish2']) && $refer_wish[0]['wish_amount2'] != 0){ ?>
        <tr>
            <td><b>Wish 2:</b></td>
            <td><?php echo $refer_wish[0]['wish2']; ?></td>
            <td><b>Wish amount:</b></td>
            <td><?php echo $refer_wish[0]['wish_amount2']; ?></td>
        </tr>
        <?php } ?>
        <?php if(!empty($refer_wish[0]['wish']) && $refer_wish[0]['wish_amount'] != 0){ ?>
        <tr>
            <td><b>Current Wish:</b></td>
            <td><?php echo $refer_wish[0]['wish']; ?></td>
            <td><b>Wish amount:</b></td>
            <td><?php echo $refer_wish[0]['wish_amount']; ?></td>
        </tr>
        <?php } ?>
        <?php } ?>
        <?php 
        $show = true;
        if(!empty($refer_wish[0]['wish']) && $refer_wish[0]['wish_amount'] != 0){
          $show = false;?>
        <tr id="change_wish_id">
            <td colspan="4">
                <?php if(!empty($refer_wish[0]['wish2']) && $refer_wish[0]['wish_amount2'] != 0){ ?>
                Now Wish can't be changed anymore, as change limit has exceeded.
                <?php }else{ ?>
                <span class="btn btn-danger" onclick="$('#change_header').show();$('#change_wish').show();$('#change_wish_button').show();$('#change_wish_id').hide();" >Change Wish</span>
                <?php } ?>
            </td>
        </tr>
        <?php } ?>
        
        <tr id="change_header" style="display: none;">
            <td colspan="4"><b>Enter Changed Wish</b></td>
        </tr>
        <tr id="change_wish" <?php if(!$show){ ?>style="display: none;" <?php } ?>>
            <td>Wish*:</td>
            <td>
                <input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>">
                <textarea name="wish" id="wish" class="form-control"></textarea></td>
            <td>Wish Amount*:</td>
            <td><input type="text" name="wish_amount" id="wish_amount" class="form-control"></td>
        </tr>
        <tr id="change_wish_button" <?php if(!$show){ ?>style="display: none;" <?php } ?>>
            <td><button class="btn btn-primary">Submit</button></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
</table>
        <center><span id="msg_here"></span></center>
    </form>
    
    <script>
      var vRules = {
    wish:{required:true},
    wish_amount:{required:true,number:true,min:1000}
    };
var vMessages = {
	wish:{required:"Please enter <?php echo $customer[0]['customer_first_name']; ?>'s wish."},
	wish_amount:{required:"Please enter wish amount.",number:"Please enter valid amount",min:"Minimum wish amount must be INR 1000."}
};

$("#form-validate").validate({
  errorClass: "my-error-class",
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>home/add_edit_wish";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			beforeSubmit : function(arr, $form, options){
				$(".btn-primary").prop('disabled',true);
        $('#msg_here').html('<img src="<?php echo base_url(); ?>images/loading.gif"> <span style="color:blue;">Please wait...</span>');
				//return false;
			},
			success: function (response) {
        $(".btn-primary").prop('disabled',false);
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					
					$('#msg_here').html('<span style="color:green;">'+res['msg']+'</span>');
          setTimeout(function(){
						refer_and_wish(<?php echo $customer_id; ?>);
					},2000);
				}
				else
				{	
					//$("#error_msg").show();
					$('#msg_here').html('<span style="color:red;">'+res['msg']+'</span>');
          setTimeout(function(){
						refer_and_wish(<?php echo $customer_id; ?>);
					},2000);
				}
				
			}
		});
	}
});

$("form").validate({
   errorClass: "my-error-class",
   validClass: "my-valid-class"
});


      </script>

