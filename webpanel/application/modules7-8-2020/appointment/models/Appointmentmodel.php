<?PHP
class Appointmentmodel extends CI_Model
{	
	 
	function getData($table, $fields, $condition = '1=1'){
		$sql = $this->db->query("Select $fields from $table where $condition");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getRecords($get){
		$table = "tbl_appointments";
		$table_id = 'i.appointment_id';
		$default_sort_column = 'i.appointment_id';
		$default_sort_order = 'desc';
		$condition = "1=1";
		
		$colArray = array('tz.zone_id','ct.center_id','s.enrollment_no', 's.student_first_name','i.appointment_date','i.appointment_message');
		$searchArray =  array('tz.zone_id','ct.center_id','s.enrollment_no', 's.student_first_name','i.appointment_date','i.appointment_message');
		
		$page = $get['iDisplayStart'];	// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];	// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		if(isset($get['sSearch_0']) && $get['sSearch_0']!=''){
			$condition .= " AND tz.zone_id = '".$get['sSearch_0']."' " ;
		}
		if(isset($get['sSearch_1']) && $get['sSearch_1']!=''){
			$condition .= " AND tc.center_id ='".$get['sSearch_1']."' ";
		}
		if(isset($get['sSearch_2']) && $get['sSearch_2']!=''){
			$condition .= " &&  s.student_first_name like '%".$_GET['sSearch_2']."%' || s.student_last_name like '%".$_GET['sSearch_2']."%' || s.enrollment_no like '%".$_GET['sSearch_2']."%' ";
		}
		
		$this -> db -> select('i.appointment_id, i.appointment_date, i.appointment_message, s.student_first_name, s.student_last_name,tc.center_name,tz.zone_name,s.enrollment_no');
		$this -> db -> from('tbl_appointments as i');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_centers as tc', 'tc.center_id  = i.center_id', 'left');
		$this -> db -> join('tbl_zones as tz', 'tz.zone_id  = i.zone_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		$this -> db -> select('i.appointment_id, i.appointment_date, i.appointment_message, s.student_first_name, s.student_last_name,tc.center_name,tz.zone_name,s.enrollment_no');
		$this -> db -> from('tbl_appointments as i');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_centers as tc', 'tc.center_id  = i.center_id', 'left');
		$this -> db -> join('tbl_zones as tz', 'tz.zone_id  = i.zone_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);		
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1){
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}else{
			return array("totalRecords"=>0);
		}
	}

	function getCenterMappedCourses($center_id){
		$this -> db -> select('tc.course_id,tc.course_name');
		$this -> db -> from('tbl_center_courses as cc');
		$this -> db -> join('tbl_courses as tc', 'tc.course_id  = cc.course_id', 'left');
		$condition = array("cc.center_id"=>$center_id);
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
}
?>
