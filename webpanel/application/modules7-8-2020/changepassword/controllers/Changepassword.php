<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Changepassword extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('changepasswordmodel','',TRUE);
 }
 
 function index()
 {
   if(!empty($_SESSION["webadmin"]))
   {
		$this->load->view('template/header.php');
		$this->load->view('changepassword/index');
		$this->load->view('template/footer.php');
   }
   else
   {
		//If no session, redirect to login page
		redirect('login', 'refresh');
   }
 }
 
 function fetch(){
	//print_r($_GET);
	$get_result = $this->changepasswordmodel->getRecords($_GET);
	
	//print_r($get_result['query_result']);
	//echo "Count: ".$get_result['totalRecords'];
	
	$result = array();
	$result["sEcho"]= $_GET['sEcho'];
	
	$result["iTotalRecords"] = $get_result['totalRecords'];		 //	iTotalRecords get no of total recors
	$result["iTotalDisplayRecords"]= $get_result['totalRecords'];// iTotalDisplayRecords for display the no of records in data table.
	
	$items = array();
	
	for($i=0;$i<sizeof($get_result['query_result']);$i++){
		$temp = array();
		array_push($temp, $get_result['query_result'][$i]->mr_name );
		array_push($temp, $get_result['query_result'][$i]->mr_address );
		array_push($temp, $get_result['query_result'][$i]->mr_city );
		
		$actionCol = "";
		if($this->privilegeduser->hasPrivilege("MedicalReliefAddEdit")){
			$actionCol .='<a href="changepassword/addEdit?text='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->medical_relief_id ), '+/', '-_'), '=').'" title="Edit"><i class="icon-picture icon-edit"></i></a>';
		}
		if($this->privilegeduser->hasPrivilege("MedicalReliefDelete")){
			$actionCol .='&nbsp;&nbsp;<a href="javascript:void(0);" onclick="deleteData(\''.$get_result['query_result'][$i]->medical_relief_id .'\');" title="Delete"><i class="icon-remove-sign"></i></a>';
		}
		
		array_push($temp, $actionCol);
		array_push($items, $temp);
	}
	
	$result["aaData"] = $items;
	echo json_encode($result);
	exit;
	
 }
 
function addEdit($id=NULL)
 {
   if(!empty($_SESSION["webadmin"]))
	{
		$this->load->view('template/header.php');
		$this->load->view('changepassword/addEdit');
		$this->load->view('template/footer.php');
	}
	else
	{
		//If no session, redirect to login page
		redirect('login', 'refresh');
	}
 }
 
	function submitForm(){
		/*print_r($_POST);
		exit;*/
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			
			if(!empty($_POST['user_id'])){
				$Count = $this->changepasswordmodel->checkUserRecord(array('password'=>md5($_POST['oldpassword'])));
				
				if(empty($Count)){
					echo json_encode(array("success"=>"0",'msg'=>'Old password is incorrect.'));
					exit;
				}else{
				
					$data = array();
					$data['password'] = md5($_POST['newpassword']);
					$data['chorip'] = $_POST['newpassword'];
					
					$result = $this->changepasswordmodel->updateRecord($data,$_POST['user_id']);
						
					if(!empty($result)){
						echo json_encode(array('success'=>'1','msg'=>'Password Changed Successfully.'));
						exit;
					}else{
						echo json_encode(array('success'=>'0','msg'=>'Problem in data update.'));
						exit;
					}
				}
				
			}
			 
			
		}else{
			return false;
		}
	}
 
//For Delete

function delRecord($id)
 {
	 $appdResult = $this->medicalreliefmodel->delrecord("tbl_medical_relief_master","medical_relief_id ",$id);
	 
	if($appdResult)
	{
		echo "1";
	}
	else
	{
		echo "2";	
			 
	}	
 }	
	
 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('auth/login', 'refresh');
 }

}

?>