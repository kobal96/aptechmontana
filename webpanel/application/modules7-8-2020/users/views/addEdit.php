<?php 
//session_start();
//print_r($_SESSION["webadmin"]);

//print_r($users);
//echo "Name: ".$users[0]->first_name;

$selzones1 = array();
//$selprodsizes1 = array();

if(!empty($selzones)){
	for($i=0; $i < sizeof($selzones); $i++){
		$selzones1[] = $selzones[$i]->zone_id;
	}
}
?>
			<!-- start: Content -->
			<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Add User</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>users">Users</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-4">
                        		<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
                                	<input type="hidden" id="user_id" name="user_id" value="<?php if(!empty($users[0]->user_id)){echo $users[0]->user_id;}?>" />
									<div class="control-group form-group">
										<label class="control-label" for="selectError">Role</label>
										<div class="controls">
											<select id="role_id" name="role_id" class="input-xlarge form-control">
												<option value="">Select</option>
												<?php 
													if(isset($roles) && !empty($roles)){
													foreach($roles as $cdrow){
														//if($cdrow->role_id !=2){
														$sel = ($cdrow->role_id == $users[0]->role_id) ? 'selected="selected"' : '';
												?>
													<option value="<?php echo $cdrow->role_id;?>" <?php echo $sel; ?>><?php echo $cdrow->role_name;?></option>
													<?php //}
													}}?>
											</select>
										</div>
									</div>
									
									<div class="control-group form-group">
										<label class="control-label" for="selectError">Zones*</label>
										<input type="checkbox" id="checkbox" >Select All
										<div class="controls">
											<select id="zone_id" name="zone_id[]" class="form-control select2" multiple>
												<option value="" disabled>Select</option>
												<?php 
													if(isset($zones) && !empty($zones)){
													foreach($zones as $cdrow){
														$sel = (in_array($cdrow->zone_id, $selzones1)) ? 'selected="selected"' : '';
												?>
													<option value="<?php echo $cdrow->zone_id;?>" <?php echo $sel; ?>><?php echo $cdrow->zone_name;?></option>
													<?php 
													}}?>
											</select>
										</div>
									</div>
									
									<div class="control-group form-group">
										<label class="control-label" for="focusedInput">First Name*</label>
										<div class="controls">
											<input class="input-xlarge form-control" id="first_name" name="first_name" type="text" value="<?php if(!empty($users[0]->first_name)){echo $users[0]->first_name;}?>">
										</div>
									</div>
									<div class="control-group form-group">
										<label class="control-label" for="focusedInput">Last Name*</label>
										<div class="controls">
											<input class="input-xlarge form-control" id="last_name" name="last_name" type="text" value="<?php if(!empty($users[0]->last_name)){echo $users[0]->last_name;}?>">
										</div>
									</div>
									<div class="control-group form-group">
										<label class="control-label" for="focusedInput">User Name*</label>
										<div class="controls">
											<input class="input-xlarge form-control" id="user_name" name="user_name" type="text" value="<?php if(!empty($users[0]->user_name)){echo $users[0]->user_name;}?>">
										</div>
									</div>
									<?php 
									$change_pwd_display = "block";
									if(!empty($users[0]->user_id)){
										$change_pwd_display = "none";
										?>
									<div class="control-group form-group">
										<label class="control-label" for="focusedInput"><input class="" id="change_pwd" name="change_pwd" type="checkbox"> Change Password</label>
									</div>
									<?php } ?>
						
									<div class="cls_change_pwd" style="display:<?php echo $change_pwd_display; ?>" id="change_password_div">
										<div class="control-group form-group change_pwd">
											<label class="control-label" for="focusedInput">Password*</label>
											<div class="controls">
												<input class="input-xlarge form-control" id="password" name="password" type="password"  autocomplete="off">
											</div>
										</div>
										
										<div class="control-group form-group">
											<label class="control-label" for="focusedInput">Confirm Password*</label>
											<div class="controls">
												<input class="input-xlarge form-control" id="confirm_password" name="confirm_password" type="password">
											</div>
										</div>
									</div>
									<div class="control-group form-group">
										<label class="control-label" for="status">Status*</label>
										<div class="controls">
											<select name="status" id="status" class="form-control">
												<option value="Active" <?php if(!empty($users[0]->status) && $users[0]->status == "Active"){?> selected <?php }?>>Active</option>
												<option value="In-active" <?php if(!empty($users[0]->status) && $users[0]->status == "In-active"){?> selected <?php }?>>In-active</option>
											</select>
										</div>
									</div>
									<div class="form-actions form-group">
										<button type="submit" class="btn btn-primary">Submit</button>
										<a href="<?php echo base_url();?>users" class="btn btn-primary">Cancel</a>
									</div>
                                </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->
			
<script>

$( document ).ready(function() {

	$("#checkbox").click(function(){
		if($("#checkbox").is(':checked') ){
			$("#zone_id > option").prop("selected","selected");
			$("#zone_id").trigger("change");
		}else{
			$("#zone_id > option").removeAttr("selected");
			$("#zone_id").trigger("change");
		}
	});
	
	/*$.noty({
		text:"This is a success notification",
		layout:"topRight",
		type:"success"
	});*/
});

$("#change_pwd").on("change", function(){
	$("#password").val("");
	$("#confirm_password").val("");
	
	if(this.checked) {
		var returnVal = confirm("Are you sure?");
		$(this).prop("checked", returnVal);
		if(returnVal){
			$('.cls_change_pwd').show();  
		}
	}else{
		$(this).prop("checked", false);
		$('.cls_change_pwd').hide();  
	}   
});

var vRules = {
	first_name:{required:true},
	last_name:{required:true},
	user_name:{required:true, email:true},
	<?php if(empty($users[0]->user_id)){ ?>
	password:{required:true},
	confirm_password:{required:true,equalTo:password},
	<?php }else{ ?>
	password:{required:"#change_pwd:checked"},
	confirm_password:{required:"#change_pwd:checked",equalTo:password},
	<?php } ?>
	role_id:{required:true}
};
var vMessages = {
	first_name:{required:"Please enter first name"},
	last_name:{required:"Please enter last name"},
	user_name:{required:"Please enter user name"},
	password:{required:"Please enter password"},
	confirm_password:{required:"Please enter confirm password",equalTo:"Password does not match."},
	role_id:{required:"Please select role."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>users/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>users";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

document.title = "AddEdit";
</script>