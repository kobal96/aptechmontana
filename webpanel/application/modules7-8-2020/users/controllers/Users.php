<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Users extends CI_Controller {

function __construct(){
	parent::__construct();
	$this->load->model('usersmodel','',TRUE);
	if(!$this->privilegeduser->hasPrivilege("UserList")){
		redirect('home');
	}
}
 
function index(){
	if(!empty($_SESSION["webadmin"])){
		$result['roles'] = $this->usersmodel->getDropdown("roles","role_id,role_name");
		$result['zones'] = $this->usersmodel->getDropdown("tbl_zones","zone_id,zone_name");
		//print_r($result);exit;
		
		$this->load->view('template/header.php');
		$this->load->view('users/index',$result);
		$this->load->view('template/footer.php');
	}else{
		//If no session, redirect to login page
		redirect('login', 'refresh');
	}
}
 
 function fetch(){
	//print_r($_GET);
	$get_result = $this->usersmodel->getRecords($_GET);
	
	//print_r($get_result['query_result']);
	//echo "Count: ".$get_result['totalRecords'];
	
	$result = array();
	$result["sEcho"]= $_GET['sEcho'];
	
	$result["iTotalRecords"] = $get_result['totalRecords'];									//	iTotalRecords get no of total recors
	$result["iTotalDisplayRecords"]= $get_result['totalRecords'];							//  iTotalDisplayRecords for display the no of records in data table.
	
	$items = array();
	
	if(!empty($get_result['query_result'])){
		for($i=0;$i<sizeof($get_result['query_result']);$i++){
			$temp = array();
			array_push($temp, $get_result['query_result'][$i]->user_name);
			array_push($temp, $get_result['query_result'][$i]->first_name);
			array_push($temp, $get_result['query_result'][$i]->last_name);
			//array_push($temp, $get_result['query_result'][$i]->email_id);
			//array_push($temp, $get_result['query_result'][$i]->phone);
			array_push($temp, $get_result['query_result'][$i]->role_name);
			array_push($temp, $get_result['query_result'][$i]->status);
			
			$actionCol = "";
			if ($this->privilegeduser->hasPrivilege("UserAddEdit")) {
				$actionCol .='<a href="users/addEdit?text='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->user_id), '+/', '-_'), '=').'" title="Edit"><i class="fa fa-edit"></i></a>';
			}
			if ($this->privilegeduser->hasPrivilege("UserDelete")) {
	//			$actionCol .='&nbsp;&nbsp;<a href="javascript:void(0);" onclick="deleteData(\''.$get_result['query_result'][$i]->user_id.'\');" title="Delete"><i class="fa fa-remove"></i></a>';
			}
			
			array_push($temp, $actionCol);
			array_push($items, $temp);
		}
	}	
	
	$result["aaData"] = $items;
	echo json_encode($result);
	exit;
	
}
 
 function addEdit($id=NULL){
	if(!empty($_SESSION["webadmin"])){
     
		//print_r($_GET);
		$user_id = "";
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$user_id = $url_prams['id'];
		}
		
		//echo $user_id;
		$result['roles'] = $this->usersmodel->getDropdown("roles","role_id,role_name");
		$result['zones'] = $this->usersmodel->getDropdown("tbl_zones","zone_id,zone_name");
		$result['users'] = $this->usersmodel->getFormdata($user_id);
		
		if(!empty($result['users'])){
			$result['selzones'] = $this->usersmodel->getDropdownSelval('tbl_user_zones', 'user_id', 'zone_id',$user_id);
		}
		//echo "<pre>";print_r($result['selzones']);exit;
		
		
		$this->load->view('template/header.php');
		//$this->load->view('homebanner/addEdit',$resulte);
		$this->load->view('users/addEdit',$result);
		$this->load->view('template/footer.php');
	}else{
		//If no session, redirect to login page
		redirect('login', 'refresh');
	}
}
 
function submitForm(){
		/*print_r($_POST);
		exit;*/
		
	if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
	{
		
		$condition = "user_name= '".$_POST['user_name']."'";
		if(isset($_POST['user_id']) && $_POST['user_id'] > 0)
		{
			$condition .= " &&  user_id !='".$_POST['user_id']."'  ";
		}
		
		$check_name = $this->usersmodel->checkRecord($_POST,$condition);
		if(!empty($check_name[0]->user_id)){
			echo json_encode(array("success"=>"0",'msg'=>'User Already Present!'));
			exit;
		}
		//exit;
		
		if(!empty($_POST['user_id'])){
			//update
			/*echo "in update";
			print_r($_POST);
			exit;*/
			
			$data = array();
			$data['role_id'] = $_POST['role_id'];
			$data['first_name'] = $_POST['first_name'];
			$data['last_name'] = $_POST['last_name'];
			
			$data['user_name'] = $_POST['user_name'];
			if(!empty($_POST['password'])){
				$data['password'] = md5($_POST['password']);
			}
			
			$data['status'] = $_POST['status'];
			
			$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
			
			$result = $this->usersmodel->updateUserId($data,$_POST['user_id']);
				
			if(!empty($result)){
			
				$this->usersmodel->delrecord_condition("tbl_user_zones", "user_id='".$_POST['user_id']."'  ");
				for($i=0; $i < sizeof($_POST['zone_id']); $i++){
					$data_array = array();
					$data_array['user_id'] = (!empty($_POST['user_id'])) ? $_POST['user_id'] : '';
					$data_array['zone_id'] = $_POST['zone_id'][$i];
					
					$this->usersmodel->insertData('tbl_user_zones', $data_array, '1'); 
				}
				
				echo json_encode(array('success'=>'1','msg'=>'Record Updated Successfully'));
				exit;
			}else{
				echo json_encode(array('success'=>'0','msg'=>'Problem in data update.'));
				exit;
			}
			
			
		}else{
			//add
			/*echo "in add";
			print_r($_POST);
			exit;*/
			
			//print_r($_SESSION["webadmin"]);
			//echo $_SESSION["webadmin"][0]->user_id;
			//exit;
			
			$data = array();
			//$data['is_active'] = $_POST['is_active'];
			
			$data['role_id'] = $_POST['role_id'];
			$data['first_name'] = $_POST['first_name'];
			$data['last_name'] = $_POST['last_name'];
			$data['user_name'] = $_POST['user_name'];
			if(!empty($_POST['password'])){
				$data['password'] = md5($_POST['password']);
			}
			
			$data['status'] = $_POST['status'];
			
			$data['created_on'] = date("Y-m-d H:i:s");
			
			$data['created_by'] = $_SESSION["webadmin"][0]->user_id;
			$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
			
			$result = $this->usersmodel->insertData('tbl_admin_users',$data,'1');
			
			
			
			
			if(!empty($result)){
				for($i=0; $i < sizeof($_POST['zone_id']); $i++){
					$data_array = array();
					$data_array['user_id'] = $result;
					$data_array['zone_id'] = $_POST['zone_id'][$i];
					
					$this->usersmodel->insertData('tbl_user_zones', $data_array, '1');
				}
				
				$message = "";
					$message  = '<html><body>';		
					$message .= '<table border="0" cellpadding="0" cellspacing="0" style="background:#fff;width:100%;">
										<tbody>
											<tr>
												<td align="center" valign="top">
													<table border="0" cellpadding="0" cellspacing="0" style="background:#fff;max-width:700px;border:1px solid #ccc;" width="100%">
														<tbody>
															<tr>
																<td align="center" valign="top">
																	<table border="0" cellpadding="0" cellspacing="0" style="background:#fff;max-width:700px;" width="100%">
																		<tbody>
																			<tr>
																				<td style="height:100px;text-align:left;text-align:center;border-bottom:2px solid #0075bc">Montana - CRM</td>
																			</tr>
																			<tr>
																				<td style="padding:20px;">
																					<p style="margin-bottom:10px;">
																						Hello '.$_POST['first_name'].' '.$_POST['last_name'].',</p>
																					<p style="margin-bottom:10px;">
																						You have successfully registered as a User on Montana - CRM.</p>
																					<p style="margin-bottom:10px;">
																						Your Details are below</p>
																					<p style="margin-bottom:10px;">
																						Link: http://attoinfotech.in/demo/aptechmontanacrm</p>
																					<p style="margin-bottom:10px;">
																						Username: '.$_POST['user_name'].'</p>
																					<p style="margin-bottom:10px;">
																						Password: '.$_POST['password'].'<br />
																						<br />
																					</p>
																					<p style="margin-bottom:2px;">
																						Regards,</p>
																					<p style="margin:2px 0 2px 0">
																						Support Team</p>
																					<p style="margin:2px 0 2px 0">
																						Montana - CRM</p>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
';
					$message .=  '</body></html>';
					
					$this->email->clear();
					$this->email->from("info@attoinfotech.in"); // change it to yours
					$subject = "";
					$subject = "Montana CRM - User Credentials";
					$this->email->to($_POST['user_name']); // change it to yours
					$this->email->subject($subject);
					$this->email->message($message);
					$checkemail = $this->email->send();
					$this->email->clear(TRUE);
					
				echo json_encode(array("success"=>"1",'msg'=>'Record Added Successfully.'));
				exit;
			}else{
				echo json_encode(array("success"=>"0",'msg'=>'Problem in data insert.'));
				exit;
			}
			
		}
		 
		
	}else{
		return false;
	}
}
	
function importExcel()
	{
		//if (!empty($_SESSION["stars_cosmetics_webadmin"])) 
		//{
			$result = "";			
			$this->load->view('template/header.php');
			$this->load->view('users/importstoreexcel',$result);
			$this->load->view('template/footer.php');
		//}
	}
	
	function importStoreExcel()
	{
		// ini_set('memory_limit', '-1');
		ini_set('memory_limit', '128M');
		error_reporting(0);
		
		if(isset($_FILES['import_store_excel']['name']) && !empty($_FILES['import_store_excel']['name']))
  		{
			// echo "<pre>";print_r($_FILES);exit;
			$name = $_FILES['import_store_excel']['name'];
			$names = explode(".", $name);
			$size = $_FILES['import_store_excel']['size'];
			$max_file_size = 1024*1024*2;	// 2 MB
			if ((end($names)=="xls"||end($names)=="xlsx"||end($names)=="XLS"||end($names)=="XLSX"))
			{
				//Load the excel library
				$this->load->library('excel');
				
				//Read file from path
				$objPHPExcel = PHPExcel_IOFactory::load($_FILES['import_store_excel']['tmp_name']);
				
				//Get only the Cell Collection
				$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,false,true);
				//echo "<pre>";
				//print_r($sheetData);
				//print_r(array_keys($sheetData));
				//exit;
				
				if(array_key_exists(2,$sheetData))
				{
					$error = 0;
					for($i = 2; $i < count($sheetData)+1; $i++)
					{
						
						//echo $sheetData[$i]['A'];exit;
						/*
						// Check Store Title
						if(empty($sheetData[$i]['A'])){
							echo json_encode(array("success"=>false,'msg'=>'Store Title is blank at Line No. A'.$i.' '));
							exit;
						}
						
						//Check State name
						if(empty($sheetData[$i]['B'])){
							echo json_encode(array("success"=>false,'msg'=>'State name is blank at Line no. B'.$i.' '));
							exit;
						}
						
						//Check City name
						if(empty($sheetData[$i]['C'])){
							echo json_encode(array("success"=>false,'msg'=>'City name is blank at Line no. C'.$i.' '));
							exit;
						}
						
						//Check Address
						if(empty($sheetData[$i]['D'])){
							echo json_encode(array("success"=>false,'msg'=>'Address is blank at Line no. D'.$i.' '));
							exit;
						}
						
						//Check Pin Code
						if(empty($sheetData[$i]['E'])){
							echo json_encode(array("success"=>false,'msg'=>'Pin Code is blank at Line no. E'.$i.' '));
							exit;
						}
						
						if(!empty($sheetData[$i]['E']) && !is_numeric($sheetData[$i]['E'])){
							echo json_encode(array("success"=>false,'msg'=>'Invalid Pin Code at Line no. E'.$i.' '));
							exit;
						}
						
						//Check Contact Number
						if(empty($sheetData[$i]['G'])){
							echo json_encode(array("success"=>false,'msg'=>'Contact Number is blank at Line no. G'.$i.' '));
							exit;
						}
						*/
						
						// echo "city_id".$city_id;
						// exit;
												
						$data = array();
						
						$data['zone_text'] = (!empty($sheetData[$i]['A'])) ? trim((string)$sheetData[$i]['A']) : '';
						$data['region_text'] = (!empty($sheetData[$i]['B'])) ? trim((string)$sheetData[$i]['B']) : '';
						$data['area_text'] = (!empty($sheetData[$i]['C'])) ? $sheetData[$i]['C'] : '';
						$data['center_id'] = (!empty($sheetData[$i]['D'])) ? trim((string)$sheetData[$i]['D']) : '';
						$data['user_type'] = 2;
						$data['user_name'] = (!empty($sheetData[$i]['E'])) ? trim((string)$sheetData[$i]['E']) : '';
						$data['password'] = "5f4dcc3b5aa765d61d8327deb882cf99";
						$data['first_name'] = (!empty($sheetData[$i]['G'])) ? trim((string)$sheetData[$i]['G']) : '';
						$data['last_name'] = (!empty($sheetData[$i]['H'])) ? trim((string)$sheetData[$i]['H']) : '';
						$data['role_id'] = 2;
					
						$data['status'] = "Active";
						$data['created_on']  = date("Y-m-d H:i:s");
						$data['created_by']  = 22;
						//echo "<pre>";print_r($data);exit;
						
						$store_id = $this->usersmodel->insertData('tbl_admin_users',$data,'1');
						
						if(empty($store_id))
						{
							$error += 1;
						}
					}
					exit;
					if($error > 0)
					{
						echo json_encode(array('success' => false,'msg' => 'Problem While Importing Excel File.'));
						exit;
					}
					else
					{
						echo json_encode(array('success' => true,'msg' => 'Excel File Imported Successfully.'));
						exit;
					}
				}
				else
				{
					echo json_encode(array('success' => false, 'msg' => 'Empty file cannot be imported.'));
					exit;
				}	
			}
			else 
			{
				echo json_encode(array('success'=>false,'msg'=>'Unsupported file format.'));
				exit;
			}
		}
		else 
		{
			echo json_encode(array('success'=>false,'msg'=>'File not uploaded properly.'));
			exit;
		}
	}
	
	
 
//For Delete

function delRecord($id)
 {
	$data = array();
	$data['status'] = "In-active";
	$appdResult = $this->usersmodel->delrecord("tbl_admin_users","user_id",$id,$data);
	
	 
	if($appdResult)
	{
		echo "1";
	}
	else
	{
		echo "2";	
			 
	}	
 }	
	
 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('auth/login', 'refresh');
 }

}

?>
