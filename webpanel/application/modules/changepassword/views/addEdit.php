<?php 
//session_start();
//echo "<pre>";print_r($_SESSION["webadmin"][0]);
//echo $_SESSION["webadmin"][0]->user_id;
//print_r($users);
//echo "Name: ".$users[0]->first_name;
//exit;
?>
<!-- start: Content -->
<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1><!--<i class="fa fa-dashboard"></i>--> Change Password</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>changepassword">Change Password</a></li>
        </ul>
      </div>
    </div>    
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
            	<div class="col-sm-8 col-md-4">
               		<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
                    	<input type="hidden" id="user_id" name="user_id" value="<?php if(!empty($_SESSION["webadmin"][0]->user_id)){echo $_SESSION["webadmin"][0]->user_id;}?>" />									
                        <div class="control-group form-group">
                            <label class="control-label" for="oldpassword">Current Password*</label>
                            <div class="controls">
                                <input class="input-xlarge form-control" id="oldpassword" name="oldpassword" type="password">
                            </div>
                        </div>
                        <div class="control-group form-group">
                            <label class="control-label" for="newpassword">New Password*</label>
                            <div class="controls">
                                <input class="input-xlarge form-control" id="newpassword" name="newpassword" type="password">
                            </div>
                        </div>
                        <div class="control-group form-group">
                            <label class="control-label" for="confirmpassword">Re-Confirm Password*</label>
                            <div class="controls">
                                <input class="input-xlarge form-control" id="confirmpassword" name="confirmpassword" type="password">
                            </div>
                        </div>
                        
                        <div class="form-actions form-group">
                            <button type="submit" class="btn btn-primary">Submit</button>
                            <a href="<?php echo base_url();?>home"><button class="btn" type="button">Cancel</button></a>
                        </div>
                    
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
         </div>              
    </div>
</div><!-- end: Content -->	
			
<script>

$( document ).ready(function() {
	
});

var vRules = {
	oldpassword:{required:true}, 
	newpassword:{required:true}, 
	confirmpassword:{equalTo:"#newpassword" }
	
};
var vMessages = {
	oldpassword:{required:"Please enter your Current Password."},
	newpassword:{required:"Please enter New Password."},
	confirmpassword:{required:"Please confirm your password.", equalTo:"Please confirm your password." }
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>changepassword/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>home";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});
</script>