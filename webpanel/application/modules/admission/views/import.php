<div id="content" class="content-wrapper">
	<div class="page-title">
		<div>
			<h1>Student</h1>            
		</div>
		<div>
			<ul class="breadcrumb">
				<li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
				<li><a href="<?php echo base_url();?>admission">Student</a></li>
			</ul>
		</div>
    </div> 
	<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
			<fieldset >
				<div id="final_report" style="height:300px;overflow-y:scroll;padding:20px;background:#d1cbc9;display:none;">
				</div>
			</fieldset>
			<div class="card" style="padding:50px !important;">
				
				<div class="control-group form-group">
					<label for="firstname" class="control-label">Import Student</label>
					<div class="input-group col-sm-12 col-xs-12 col-md-6">
						<div class="input-group ">
							  <span class="input-group-addon"><i class="fa fa-file"></i></span>
							  <input type="text" class="form-control input-lg" disabled placeholder="Upload .xls">
							  <span class="input-group-btn">
								<button class="browse btn btn-primary input-lg" type="button"><i class="fa fa-search"></i>Browse</button>
							  </span>
						</div >
						<input type="file" id="students" name="students" class="file" style="display:none" >
					</div>
				</div> 	
				<div class="form-actions form-group">											
					<a class="btn btn-success btn-sm" href="<?php echo FRONT_URL."\images\sample_files\student_import_sample.xlsx"?>"><i class="fa fa-file"></i> Download Sample .xls</a> (<span style="color:red">The Importing file should be the given sample format</span>)
				</div>
				<div>&nbsp;</div>
				<div class="form-actions form-group">
					<button type="submit" class="btn btn-primary">Submit</button>
					<a href="<?php echo base_url();?>admission" class="btn btn-primary cancel">Cancel</a>
				</div>
			</div>
		</form>
</div>

<script>

$(document).on('click', '.browse', function(){
  var file = $(this).parent().parent().parent().find('.file');
  file.trigger('click');
});
$(document).on('change', '.file', function(){
  $(this).parent().find('.form-control').val($(this).val().replace(/C:\\fakepath\\/i, ''));
});

var vRules = {
	employee:{required:true}
};
var vMessages = {
	employee:{required:"Please select file to upload."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>admission/importRecordExcel";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false, 
			beforeSubmit : function(arr, $form, options){
				$(".btn-primary").hide();
				// if($("company_id").val()==""){
				// 	$(".company_error").show();
				// 	$(".btn-primary").show();
				// 	return false;
				// }else{
					$(".company_error").hide();
					//loading
					$('#LoadingModal .modal-body .process_name').html("Importing...");
					$('#LoadingModal').modal('show');
				// }
			},
			success: function (response) {
				var res = eval('('+response+')');
				$("#final_report").html(res['msg']);
				$("#final_report").show();
				$(".cancel").show();
				$(".browse").hide();
				
				//loading
				$('#LoadingModal .modal-body .process_name').html("");
				$('#LoadingModal').modal('hide');
				$('html, body').animate({
					scrollTop: $("#final_report").offset().top
				}, 1000)
				return false;
			}
		});
	}
});
</script>
