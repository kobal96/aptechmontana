<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1>Inquiries</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>inquiries">Inquiries</a></li>
        </ul>
      </div>
    </div> 
    <div class="card">
    <!-- 	<div class="page-title-border ">
		<?php 
			/* if ($this->privilegeduser->hasPrivilege("InquiryExport")) {
		?>
			<a style="margin-left:10px;" class="btn btn-primary icon-btn export_contactus pull-right"><i class="fa fa-download"></i>Export Inquiry Details</a>
			<?php } */?>	
		</div>     --> 
        <div class="col-sm-12" style="clear: both">
			<form name="filter_form" method="post" id="filter_form">
				<div class="box-content form-horizontal product-filter">            	
					<div class="col-sm-3 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Zone" class="control-label">Zone</label>
							<select name="sSearch_0" id="sSearch_0" class="searchInput form-control">
								<option value="">Select Zone</option>
								<?php 
									if(!empty($zoneDetails)){
										foreach($zoneDetails as $key=>$val){
								?>
									<option value="<?php echo $val['zone_id']; ?>"><?php echo $val['zone_name'];?></option>
								<?php
										}
									}
								?>
							</select>
						</div>
					</div>
					<div class="col-sm-3 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Center" class="control-label">Center</label>
							<select name="sSearch_1" id="sSearch_1" class="searchInput form-control" >
								<option value="">Select Center</option>
							</select>
						</div>
					</div>
					<div class="col-sm-3 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Class" class="control-label">Course</label>
							<select name="sSearch_2" id="sSearch_2" class="searchInput form-control">
								<option value="">Select Course</option>
							</select>
						</div>
					</div>
					<div class="col-sm-3 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Center" class="control-label">Source</label>
							<select name="sSearch_3" id="sSearch_3" class="searchInput form-control" >
								<option value="">Select Source</option>
								<option value="Website">Website</option>
								<option value="Newspaper">Newspaper</option>
								<option value="FacebookPage">FacebookPage</option>
								<option value="JustDial">JustDial</option>
								<option value="SchoolSignBoard">SchoolSignBoard</option>
								<option value="Reference">Reference</option>
								<option value="PosterBanner">PosterBanner</option>
								<option value="Leaflet">Leaflet</option>
								<option value="Other">Other</option>
							</select>
						</div>
					</div>
					<div class="col-sm-3 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="firstname" class="control-label">Status</label>
							<?php $status = array('YTC - Yet to Call','To Call Back & follow up','Enrolled with us','Interested/Expected to Enroll','Interested/Future Prospect','Will enroll later -Child is below age Criteria','Not Interested Now/To Follow up Later','Lost to Competition','Enrolled in a High School','Wrong Contact details'); ?>
							<select name="sSearch_4" id="sSearch_4" class="searchInput form-control">
								<option value="">Select Status</option>
								<?php 
									foreach($status as $key=>$val){
								?>
								<option value="<?php echo $val;?>" ><?php echo $val;?></option>
								<?php
									}
								?>
							</select>
							
						</div>
					</div>
					<div class="col-sm-3 col-xs-12">
					 	<div class="dataTables_filter searchFilterClass form-group">
							<label for="firstname" class="control-label"> ENQ No. / Student Name</label>
							<input id="sSearch_5" name="sSearch_5" type="text" class="searchInput form-control" placeholder="LD No. / Student Name "/>
						</div>
					</div>
					<div class="control-group clearFilter">
						<div class="controls">
							<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
							<?php if ($this->privilegeduser->hasPrivilege("InquiryExport")) {?>
								<button  type="button" class="btn btn-primary export_contactus" style="margin:32px 10px 10px 10px;">Export Inquiry Details</button>
							<?php } ?>	
							
						</div>
					</div>
				</div>
			</form>
        </div> 
		<div class="clearfix"></div>
		<div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                   <table class="dynamicTable display table table-bordered non-bootstrap">
                        <thead>
                          <tr>
							<th>Lead No</th>
							<th>ENQ Date</th>
							<th>ENQ No.</th>
							<th>Student Name</th>
							<th>Course Name</th>
							<th>Source</th>
							<th>Zone</th>
							<th>Center</th>
							<th>Type</th>
							<th>Status</th>
							<th>Action</th>
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
    </div>
</div><!-- end: Content -->
			
<script>
	
	$( document ).ready(function() {
		clearSearchFilters();
		$(".datepicker").datepicker({
			//format: 'YYYY-MM-DD'
		});
	});	

	
	// $("#sSearch_3").datepicker({format: 'yyyy-dd-mm '});
	function changestatus(id){
		$.ajax({
			url: "<?php echo base_url(); ?>feedbacks/changestatus/"+id,
			dataType:'json',
			beforeSend:function(){
			},
			success: function(res){
				if(res['success']){
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>feedbacks";
					},2000);

				}
				else{
					if(res['msg']=='redirect'){
						displayMsg("error",'Session has expired.');
						setTimeout(function(){
							window.location = "<?php echo base_url();?>login";
						},2000)
					}
					else{
						displayMsg("error",res['msg']);
						return false;
					}
				}
			},
			error: function(){
				displayMsg("error",'Error Occured,Please reload the page.');
				return false;
			}
		});
	}
	
	$(".export_contactus").on("click", function()
	{
		$('.export_contactus').attr("disabled","disabled");
		var act = "<?php  echo base_url();?>inquiries/export";
		$("#filter_form").attr("action",act);
		$("#filter_form").submit();
	});
	
	$("#filter_form").on("submit", function()
	{
		$('.export_contactus').removeAttr("disabled");
		
		$('.export_contactus_sizes').removeAttr("disabled");
		
		// setTimeout("location.reload(true);",1000);
	});
	
	<?php
	if(isset($_SESSION['contactus_export_success']))
	{
		?>
		displayMsg("<?php echo $_SESSION['contactus_export_success']; ?>", "<?php echo $_SESSION['contactus_export_msg']; ?>");
		<?php
		unset($_SESSION['contactus_export_success']);
		unset($_SESSION['contactus_export_msg']);
	}
	?>

	$(document).on("change",".searchInput",function(){
		var element = $(this).attr("id");
		var functionName = "";
		var column_name = "";
		var id =  "";
		if(element == "sSearch_0" || element == "sSearch_1"){
			if(element == "sSearch_0"){
				column_name = "zone_id";
				id =  $("#sSearch_0").val();
				$("#sSearch_1").val("");
			}else{
				column_name = "center_id";
				id =  $("#sSearch_1").val();
			}
			if(id != ""){
				$.ajax({
					url: "<?php echo base_url().$this->router->fetch_module();?>/getSearchLists",
					data:{"column_name":column_name,"value":id},
					async: false,
					type: "POST",
					dataType:"JSON",
					success: function(response){
						if(response.status){
							if(element == "sSearch_0"){
								$("#sSearch_1").html(response.option);
							}else{
								$("#sSearch_2").html(response.option);
							}
						}
					}
				});
			}else{
				if(element == "sSearch_0"){
					$("#sSearch_1").html("<option value=''>Select Center</option>");
				}
			}
		}
	})
	
	document.title = "Inquiries";
</script>