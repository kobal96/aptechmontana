<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Follow UP</h1>            
                  </div>
                </div>

                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-6 col-md-6">
                            <!-- <div>Lead/Inquiry Details</div> -->
							<div class="row">
									<div class="col-sm-12 control-group form-group">
                                         <div class="col-sm-4"><strong>Data Type:</strong></div>
                                         <div class="col-sm-8"><?php echo $details[0]->rec_type ?></div>
									</div>
									<div class="col-sm-12 control-group form-group">
                                         <div class="col-sm-4"><strong>LD Date:</strong></div>
                                         <div class="col-sm-8"><?php echo date("d-M-Y",strtotime($details[0]->created_on)) ?></div>
									</div>
                                    <div class="col-sm-12 control-group form-group">
                                         <div class="col-sm-4"><strong>Lead Name:</strong></div>
                                         <div class="col-sm-8"><?php echo $details[0]->father_name ?></div>
									</div>
									<?php if(!empty($details[0]->student_first_name)){?>
									<div class="col-sm-12 control-group form-group">
                                         <div class="col-sm-4"><strong>Child Name:</strong></div>
                                         <div class="col-sm-8"><?php echo $details[0]->student_first_name.' '.$details[0]->student_last_name ?></div>
									</div>
									<?php }?>
                                    <div class="col-sm-12 control-group form-group">
                                         <div class="col-sm-4"><strong>Call:</strong></div>
                                         <div class="col-sm-8"><?php echo (!empty($details[0]->father_contact_no) ? $details[0]->father_contact_no : '') ; ?></div>
									</div>
                                    <div class="col-sm-12 control-group form-group">
                                         <div class="col-sm-4"><strong>Email:</strong></div>
                                         <div class="col-sm-8"><a href="mailto:<?php echo $details[0]->father_emailid; ?>"><?php echo $details[0]->father_emailid; ?></a></div>
									</div>
									<div class="col-sm-12 control-group form-group">
                                         <div class="col-sm-4"><strong>Category:</strong></div>
                                         <div class="col-sm-8"><?php echo (!empty($details[0]->lead_category) ? $details[0]->lead_category : '') ; ?></div>
									</div>
                                    <div class="col-sm-12 control-group form-group">
                                         <div class="col-sm-4"><strong>Last Status:</strong></div>
                                         <div class="col-sm-8"><?php echo (!empty($follow_logs_last[0]->status_description) ? $follow_logs_last[0]->status_description :''); ?></div>
									</div>
                                    <div class="col-sm-12 control-group form-group">
                                         <div class="col-sm-4"><strong>Last Sub Status:</strong></div>
                                         <div class="col-sm-8"><?php echo (!empty($follow_logs_last[0]->status_sub_description) ?$follow_logs_last[0]->status_sub_description : ''); ?></div>
									</div>
                                    <div class="col-sm-12 control-group form-group">
                                         <div class="col-sm-4"><strong>Action Planned/Taken:</strong></div>
                                         <div class="col-sm-8"><?php echo (!empty($follow_logs_last[0]->default_status_description) ?$follow_logs_last[0]->default_status_description :''); ?></div>
									</div>
                                    <div class="col-sm-12 control-group form-group">
                                         <div class="col-sm-4"><strong>Lead/Enquiry Stage:</strong></div>
                                         <div class="col-sm-8"><?php echo (!empty($follow_logs_last[0]->lead_enquiry_status_description) ? $follow_logs_last[0]->lead_enquiry_status_description : ''); ?></div>
									</div>
                                    <div class="col-sm-12 control-group form-group">
                                         <div class="col-sm-4"><strong>Notes:</strong></div>
                                         <div class="col-sm-8"><?php echo (!empty($follow_logs_last[0]->notes) ? $follow_logs_last[0]->notes : ''); ?></div>
									</div>
									
								</div>
                        </div>

        <!-- second div -------------------------------------->
        <div class="col-sm-6 col-md-6">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="log_lead_id" name="log_lead_id" value="<?php if(!empty($follow_logs_id[0]['ID'])){echo $follow_logs_id[0]['ID'];}?>" />

								<input type="hidden" id="hidden_lead_id" name="hidden_lead_id" value="<?php if(!empty($details[0]->inquiry_master_id)){echo $details[0]->inquiry_master_id;}?>" />
								
								<div class="row">
									<div class="col-sm-6 control-group form-group">
										<label class="control-label" for="lead_status">Status*</label>
										<div class="controls">
										<select id="lead_status" name="lead_status" class="form-control selectpicker show-tick required" onchange="getSubStatus(this.value)" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select Status...">
										<option value="">Select Status </option>
											<?php foreach ($status as $key => $value) { ?>
												<option value="<?=$value['lead_status_id']?>"><?=$value['status_description']?></option>
											<?php } ?>		
											</select>
										</div>
									</div>

									<div class="col-sm-6 control-group form-group" style="padding-left: 23px;">
										<label class="control-label" for="lead_sub_status">Sub Status*</label>
										<div class="controls">
										<select id="lead_sub_status" name="lead_sub_status" class="form-control show-tick required" onchange="getLeadEnquiyStage('',this.value)"data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select Sub Status...">
												<option value="">Select Sub State</option>
												
											</select>
										</div>
									</div>
								</div>					
								

								<div class="row">
									<div class="col-sm-6 control-group form-group">
										<label class="control-label" for="know_about_us">Action Planned/Taken*</label>
										<div class="controls">
										<select id="planned_status" name="planned_status" class="form-control selectpicker show-tick required" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select Action Planned/Taken...">
													<option value="">Select Planned/Taken</option>
													<?php foreach ($planned_status as $key => $value) {
														?>
														<option value="<?=$value['default_status_id']?>" ><?=$value['status_description']?></option>
													<?php } ?>		
											</select>
										</div>
									</div>	

									<div class="col-sm-6 control-group form-group" style="padding-left: 23px;">
										<label class="control-label" for="lead_enquiry_status">Lead/Enquiry Stage*</label>
										<div class="controls">
										<select id="lead_enquiry_status" name="lead_enquiry_status" class="form-control required show-tick "  data-style="btn-primary"  data-live-search="true" data-live-search-placeholder="Select Lead/Enquiry Stage..">
													<option value="">Select Lead/Enquiry Stage</option>	
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 control-group form-group" >
									<label class="control-label"><span>Note</span></label>
										<div class="controls" style="padding-right: 25px;">
											<textarea class="form-control" id="note" name="note"  style="text-align: left">
											</textarea>
										</div>
									</div>
								</div>
								<div class="row"><strong>Next Follow up Schedule</strong></div>
								<div class="row">
									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>Select date from Calender</span></label>
										<div class="controls">
											<input type="text" class="form-control required datepicker" placeholder="follow up date" id="next_follow_up" name="next_follow_up" value="">
										</div>
									</div>
									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>Select Time</span></label>
										<div class="controls">
										<input placeholder="Selected time" type="text" id="input_starttime" name="follow_up_time" class="form-control timepicker">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12 col-md-12 form-actions form-group">
										<div class="col-sm-6 control-group form-group" >
											<button type="submit" class="btn btn-primary">Submit</button>
											<a href="<?php echo base_url();?>inquiries" class="btn btn-primary">Cancel</a>
										</div>
										<div class="col-sm-6 control-group form-group" style="padding-left: 35px;">
											<label class="control-label"><span>Updated BY</span></label>
											<div class="controls">
											<input type="text" class="form-control" value="<?php echo $_SESSION["webadmin"][0]->first_name." ".$_SESSION["webadmin"][0]->last_name; ?>" disabled>
											</div>
										</div>
									</div>
								</div>
							</form>
                        </div>
                        <?php if(!empty($status_logs)){ ?>
							<a href="javascript:void(0)" class="btn btn-primary">Follow Up Timelines</a>
									<div class="row">
									<br>
									<table border="1" >
										<tr>
											<th>Scheduled Date & Time</th>
										</tr>
										
										<tr>
											<td><?php echo (!empty($follow_logs[0]['follow_up_date']) ?date("d-M-Y", strtotime($follow_logs[0]['follow_up_date'])) :'').' '.$follow_logs[0]['follow_up_time']?></td>
										</tr>
										
									</table>
									<br>
									<table style="border-collapse:collapse;width: 93%;" border="1">
									<tr>
										<th>Sr No</th>
										<th>Scheduled Date</th>
										<th>Followed up on</th>
										<th>Type </th>
										<th>Status</th>
										<th>Sub Status</th>
										<th>Action Planned/Taken</th>
										<th>Lead/Enquiry Stage</th>
										<th>Notes</th>
										<th>Updated By</th>
									</tr>
										<?php 
										foreach ($status_logs as $key => $value) { ?>
										<tr>
											<td><?php echo (sizeof($status_logs)- $key)?></td>
											<td><?php echo (!empty($value->follow_up_date) ?date("d-M-Y", strtotime($value->follow_up_date)) :'').' '.$value->follow_up_time?></td>
											<td><?php echo (!empty($value->created_on) ?date("d-M-Y H:m:s a", strtotime($value->created_on)) :'')?></td>
											<td><?php echo ($value->type ==1? 'lead' :'Inquiries')?></td>
											<td><?php echo $value->status_description?></td>
											<td><?php echo $value->status_sub_description?></td>
											<td><?php echo $value->default_status_description?></td>
											<td><?php echo $value->lead_enquiry_status_description?></td>
											<td><?php echo $value->notes?></td>
											<td><?php echo $value->user_name?></td>
										</tr>
										<?php }?>
									</table>
								</div>
								<?php }	?>				
								<br>
                    <div class="clearfix"></div>
                    
                    </div>
                 </div>
                </div>    
                
                    
			</div><!-- end: Content -->								
<script>


$('document').ready(function(){
	// $(".selectpicker").selectpicker("refresh");
	$(".number_only").keypress(function (e) {
    	if (e.which != 8 && e.which != 0 && e.which != 46 &&(e.which < 48|| e.which > 57)) {
      	return false;
    	}
  	});
})
// $( document ).ready(function() {
	
// 	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
// 			toolbar_Full:
// 			[
// 						//['Source', 'Templates'],
// 						['Cut','Copy','Paste','PasteText','-','Print', 'SpellChecker', 'Scayt'],
// 						['Subscript','Superscript'],
// 						['NumberedList','BulletedList'],
// 						['BidiLtr', 'BidiRtl' ],
// 						//['Maximize', 'ShowBlocks'],
// 						['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
// 						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
// 						//['SelectAll','RemoveFormat'],'/',
// 						['Styles','Format','Font','FontSize'],
// 						['TextColor','BGColor'],								
// 						//['Image','Flash','Table','HorizontalRule','Smiley'],
// 					],
// 					 width: "620px"
// 			};
// 	$('.editor').ckeditor(config);
	
// 	$.validator.addMethod("needsSelection", function(value, element) {
//         return $(element).multiselect("getChecked").length > 0;
//     });
	
// });

// $('.career_in').on('change', function() {
//    var careerin = $("input[name='career_in']:checked").val(); 
//    if(careerin == '2'){
// 	   $('#career_main_div').fadeIn();
//    }else{
// 	$('#career_main_div').fadeOut()
//    }
// });





function getLeadEnquiyStage(status_id=null,sub_status_id=null,lead_enquiry_status_id=null){
	<?php if(empty($details)){?>
		var status_id = $('#lead_status').val();
		var sub_status_id =$('#lead_sub_status').val() ;
	<?php }else{?>
		var status_id = $('#lead_status').val()
	<?php }?>
	if(status_id && sub_status_id ){
		$.ajax({
			url:"<?php echo base_url();?>lead/getLeadEnquiyStage",
			data:{status_id:status_id,sub_status_id:sub_status_id,lead_enquiry_status_id:lead_enquiry_status_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#lead_enquiry_status").html("<option value=''>Select lead enquiry status</option>"+res['option']);
						$("#lead_enquiry_status").selectpicker("refresh");
					}else{
						$("#lead_enquiry_status").html("<option value=''>lead enquiry status</option>");
						// $("#lead_enquiry_status").selectpicker("refresh");
					}
				}
				/* else{	
					$("#lead_enquiry_status").html("<option value=''>Select lead enquiry status</option>");
					$("#lead_enquiry_status").selectpicker("refresh");
				} */
			}
		});
	}
}


function getSubStatus(status_id,sub_status_id=null){
	if(status_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>lead/getSubStatus",
			data:{status_id:status_id,sub_status_id:sub_status_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						
						$("#lead_enquiry_status").html("<option value=''>Select lead enquiry status</option>");
						$("#lead_enquiry_status").selectpicker("refresh");
						$("#lead_sub_status").html("<option value=''>Select Sub status</option>"+res['option']);
						$("#lead_sub_status").selectpicker("refresh");
					}else{
						$("#lead_sub_status").html("<option value=''>No Sub status found</option>");
						$("#lead_sub_status").selectpicker("refresh");
					}
				}
				/* else{	
					$("#lead_sub_status").html("<option value=''>Select Sub status</option>");
					$("#lead_sub_status").selectpicker("refresh");
				} */
			}
		});
	}
}



var vRules = {
	lead_status :{required:true},
	lead_sub_status :{required:true},
	lead_enquiry_status:{required :true},
	planned_status:{required:true},
    
};
var vMessages = {
	lead_status :{required:"select  lead status "},
	lead_sub_status :{required:"select lead sub status"},
	planned_status:{required:"Select Planend Lead status"},
	lead_enquiry_status :{requred:"Select lead enquiy status"},
    
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>inquiries/submitFollowUpForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>inquiries";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEditFollow - Center";

$(".datepicker").datepicker({
	format: 'dd-mm-yyyy',
	startDate: new Date()
});
$('#input_starttime').timepicker({
// 12 or 24 hour
twelvehour: true,
defaultTime: '08:00 AM'
});
</script>					
