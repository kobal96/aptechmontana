<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Inquiries extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('inquiriesmodel','',TRUE);
		checklogin();
		//echo "here...";
	}

	function index(){
		//get search filters 
		$filterData = array();
		$filterData['zoneDetails'] = $this->inquiriesmodel->getData("tbl_zones","zone_id,zone_name",array("status"=>'Active'));
		$this->load->view('template/header.php');
		$this->load->view('inquiries/index',$filterData);
		$this->load->view('template/footer.php');
	}
	
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->inquiriesmodel->getFormdata($record_id);
			//echo "<pre>";
			//print_r($result['details']);
			//echo $result['details'][0]->center_id;
			//exit;
			
			
			$this->load->view('template/header.php');
			$this->load->view('inquiries/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}


	function addEdit()
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			$result = "";
			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
				$result['follow_logs'] = $this->inquiriesmodel->getdata("tbl_lead_enquiry_log", "*","lead_id = $record_id and follow_up_date !='' order by created_on desc limit 1 ");
				// echo "<pre>";
				// print_r($result['follow_logs']);
				// exit;
			}
			// echo $record_id;
			

			$result['zones'] = $this->inquiriesmodel->getdata("tbl_zones","*","status = 'Active' ");
			$result['countries'] = $this->inquiriesmodel->getdata("tbl_countries", "*","status = 'Active' ");
			$result['centers'] = $this->inquiriesmodel->getdata("tbl_centers", "*","status = 'Active' ");
			$result['course'] = $this->inquiriesmodel->getData("tbl_courses","*",array("status"=>'Active',"category_id"=>'1'));
			$result['details'] = $this->inquiriesmodel->getFormdata($record_id);
			
			$bday = new DateTime($result['details'][0]->student_dob); // Your date of birth
			$today = new Datetime(date('y.m.d'));
			$diff = $today->diff($bday);
			// echo "<pre>";print_r($diff);exit;
			// $diff->y, $diff->m, $diff->d);
			$result['details'][0]->age_year = $diff->y ;
			$result['details'][0]->age_month = $diff->m ;

			// echo "<pre>";
			// print_r($result['details']);
			// exit;
			$result['status'] = $this->inquiriesmodel->getdata("tbl_lead_status", "*","status = 'Active' ");
			$result['planned_status'] = $this->inquiriesmodel->getdata("tbl_default_status", "*","status = 'Active' ");
			$this->load->view('template/header.php');
			$this->load->view('inquiries/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function fetch(){
		//print_r($_GET);
		$get_result = $this->inquiriesmodel->getRecords($_GET);

		$result = array();
		$result["sEcho"]= $_GET['sEcho'];
	
		$result["iTotalRecords"] = $get_result['totalRecords'];									//	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"]= $get_result['totalRecords'];							//  iTotalDisplayRecords for display the no of records in data table.

		$items = array();
		if(!empty($get_result['query_result'])){
			for($i=0;$i<sizeof($get_result['query_result']);$i++){
				$temp = array();
				array_push($temp, "LD00".$get_result['query_result'][$i]->lead_id);
				array_push($temp,  (!empty($get_result['query_result'][$i]->inquiry_date)?date("d-M-Y",strtotime($get_result['query_result'][$i]->inquiry_date)):""));
				array_push($temp, "ENQ".$get_result['query_result'][$i]->lead_no);
				array_push($temp, $get_result['query_result'][$i]->student_first_name." ".$get_result['query_result'][$i]->student_last_name );
				array_push($temp, $get_result['query_result'][$i]->course_name);
				array_push($temp, $get_result['query_result'][$i]->how_know_about_school);
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);
				array_push($temp, $get_result['query_result'][$i]->rec_type);
				
				array_push($temp, $get_result['query_result'][$i]->inquiry_progress);
				$actionCol1 = "";
				$actionCol1.= '<a class="btn btn-primary" href="inquiries/view?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->inquiry_master_id) , '+/', '-_') , '=') . '" title="View Details"><i class="fa fa-eye"></i></a>';
				
				$getOnlyinquiry = $this->inquiriesmodel->getdata("tbl_student_master", "inquiry_master_id","inquiry_master_id = '".$get_result['query_result'][$i]->inquiry_master_id."' ");
				if(empty($getOnlyinquiry)){
					$actionCol1.= '
					&nbsp &nbsp <a class="btn btn-primary" href="admission/addEdit?text=' . rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->inquiry_master_id) , '+/', '-_') , '=') . '" title="Convert to Enrollment"><i class="fa fa-user-plus"></i></a>';
				}
				$actionCol1.= '<a class="btn btn-primary" href="inquiries/addEditFollow?text=' . rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->inquiry_master_id) , '+/', '-_') , '=') . '" title="Follow Up"><i class="fa fa-arrow-up"></i></a>
				
				<a class="btn btn-primary" href="inquiries/addEdit?text=' . rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->inquiry_master_id) , '+/', '-_') , '=') . '" title="Edit Lead"><i class="fa fa-edit"></i></a>';

				array_push($temp, $actionCol1);
				array_push($items, $temp);
			}
		}
		
		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function test_email(){
	    $to_email = "danish.akhtar@attoinfotech.com";			
        $replyto = "info@attoinfotech.website";		
        $subject = "Test Email";
        $headers = "From: info@attoinfotech.website\r\n";
        $headers .= "Reply-To: info@attoinfotech.website\r\n";		
        $headers .= "BCC: infodanish@gmail.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";							
        $message  = '<html><body>';		
        $message .= '<p> Testing email...</p>';
        $message .=  '</body></html>';
        
        $mailop = mail($to_email, $subject, $message, $headers);
        
        if($mailop){
        	echo "Email Send";exit;
        }else{
        	echo "Email Not Send";exit;
        }
	}
	
	function submitFormInquiry(){
			// echo "<pre>";
			// print_r($_POST);
			// exit;	
		if(!empty($_POST['hidden_inquiryid']) && $_POST['rec_type'] =='Inquiry'){
			// echo "inside update";exit;
			$data['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '' ;
			$data['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '' ;
			$data['inquiry_date'] = (!empty($_POST['inquiry_date'])) ? date("Y-m-d", strtotime($_POST['inquiry_date'])) : '' ;
			$data['student_first_name'] = (!empty($_POST['student_first_name'])) ? $_POST['student_first_name'] : '' ;
			$data['student_last_name'] = (!empty($_POST['student_last_name'])) ? $_POST['student_last_name'] : '' ;
			$data['student_dob'] = (!empty($_POST['student_dob'])) ? date("Y-m-d", strtotime($_POST['student_dob'])) : '' ;
			$data['gender'] = (!empty($_POST['gender'])) ? $_POST['gender'] : '' ;
			$data['age_year'] = (!empty($_POST['age_year'])) ? $_POST['age_year'] : '' ;
			$data['age_month'] = (!empty($_POST['age_month'])) ? $_POST['age_month'] : '' ;
			$data['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '' ;
			$data['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : '' ;
			$data['has_attended_preschool_before'] = (!empty($_POST['has_attended_preschool_before'])) ? $_POST['has_attended_preschool_before'] : '' ;
			$data['preschool_name'] = (!empty($_POST['preschool_name'])) ? $_POST['preschool_name'] : '' ;
			$data['present_address'] = (!empty($_POST['present_address'])) ? $_POST['present_address'] : '' ;
			$data['address1'] = (!empty($_POST['address1'])) ? $_POST['address1'] : '' ;
			$data['address2'] = (!empty($_POST['address2'])) ? $_POST['address2'] : '' ;
			$data['address3'] = (!empty($_POST['address3'])) ? $_POST['address3'] : '' ;
			$data['country_id'] = (!empty($_POST['country_id'])) ? $_POST['country_id'] : '' ;
			$data['state_id'] = (!empty($_POST['state_id'])) ? $_POST['state_id'] : '' ;
			$data['city_id'] = (!empty($_POST['city_id'])) ? $_POST['city_id'] : '' ;
			$data['pincode'] = (!empty($_POST['pincode'])) ? $_POST['pincode'] : '' ;
			$data['father_name'] = (!empty($_POST['father_name'])) ? $_POST['father_name'] : '' ;
			$data['father_contact_no'] = (!empty($_POST['father_contact_no'])) ? $_POST['father_contact_no'] : '' ;
			$data['father_contact_no2'] = (!empty($_POST['father_contact_no2'])) ? $_POST['father_contact_no2'] : '' ;
			$data['father_emailid'] = (!empty($_POST['father_emailid'])) ? $_POST['father_emailid'] : '' ;
			$data['father_education'] = (!empty($_POST['father_education'])) ? $_POST['father_education'] : '' ;
			$data['father_profession'] = (!empty($_POST['father_profession'])) ? $_POST['father_profession'] : '' ;
			$data['mother_name'] = (!empty($_POST['mother_name'])) ? $_POST['mother_name'] : '' ;
			$data['mother_contact_no'] = (!empty($_POST['mother_contact_no'])) ? $_POST['mother_contact_no'] : '' ;
			$data['mother_contact_no2'] = (!empty($_POST['mother_contact_no2'])) ? $_POST['mother_contact_no2'] : '' ;
			
			$data['mother_emailid'] = (!empty($_POST['mother_emailid'])) ? $_POST['mother_emailid'] : '' ;
			$data['mother_education'] = (!empty($_POST['mother_education'])) ? $_POST['mother_education'] : '' ;
			$data['mother_profession'] = (!empty($_POST['mother_profession'])) ? $_POST['mother_profession'] : '' ;
			// $data['how_know_about_school'] = (!empty($_POST['how_know_about_school'])) ? $_POST['how_know_about_school'] : '' ;
			// $data['how_know_about_other'] = (!empty($_POST['how_know_about_other'])) ? $_POST['how_know_about_other'] : '' ;
			$data['inquiry_remark'] = (!empty($_POST['inquiry_remark'])) ? $_POST['inquiry_remark'] : '' ;
			// $data['inquiry_status'] = (!empty($_POST['inquirystatus'])) ? $_POST['inquirystatus'] : '' ;
			// $data['inquiry_progress'] = (!empty($_POST['inquiryprogress'])) ? $_POST['inquiryprogress'] : '' ;
			
			// $data['inquiry_handled_by'] = (!empty($_POST['inquiry_handled_by'])) ? $_POST['inquiry_handled_by'] : '' ;
			$data['lead_category'] = (!empty($_POST['lead_category'])) ? $_POST['lead_category'] : '' ;
			if(!empty($_POST['know_about_us']) && isset($_POST['know_about_us'])){
				$data['know_about_us'] = implode(",",$_POST['know_about_us']);
			}
			$data['type'] = 2;
			$data['lead_status_id'] = $_POST['lead_status'];
			$data['lead_sub_status_id'] = $_POST['lead_sub_status'];
			$data['lead_planned_status_id'] = $_POST['planned_status'];
			$data['lead_enquiry_status_id'] = $_POST['lead_enquiry_status'];
			$result = $this->inquiriesmodel->updateRecord('tbl_inquiry_master', $data, "inquiry_master_id='".$_POST['hidden_inquiryid']."' ");
			
		}else
		{
			// echo "inside insert";exit;
			$data['lead_id'] = (!empty($_POST['hidden_inquiryid'])) ? $_POST['hidden_inquiryid'] : '' ;
			$data['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '' ;
			$data['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '' ;
			// $data['inquiry_no'] = (!empty($_POST['inquiry_no'])) ? $_POST['inquiry_no'] : '' ;
			$data['inquiry_date'] = (!empty($_POST['inquiry_date'])) ? date("Y-m-d", strtotime($_POST['inquiry_date'])) : '' ;
			// $data['academic_year_master_id'] = (!empty($response_headers['academicyear'])) ? $response_headers['academicyear'] : '' ;
			
			$data['student_first_name'] = (!empty($_POST['student_first_name'])) ? $_POST['student_first_name'] : '' ;
			$data['student_last_name'] = (!empty($_POST['student_last_name'])) ? $_POST['student_last_name'] : '' ;
			$data['student_dob'] = (!empty($_POST['student_dob'])) ? date("Y-m-d", strtotime($_POST['student_dob'])) : '' ;
			$data['gender'] = (!empty($_POST['gender'])) ? $_POST['gender'] : '' ;
			$data['age_year'] = (!empty($_POST['age_year'])) ? $_POST['age_year'] : '' ;
			$data['age_month'] = (!empty($_POST['age_month'])) ? $_POST['age_month'] : '' ;
			$data['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '' ;
			$data['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : '' ;
			$data['has_attended_preschool_before'] = (!empty($_POST['has_attended_preschool_before'])) ? $_POST['has_attended_preschool_before'] : '' ;
			$data['preschool_name'] = (!empty($_POST['preschool_name'])) ? $_POST['preschool_name'] : '' ;
			$data['present_address'] = (!empty($_POST['present_address'])) ? $_POST['present_address'] : '' ;
			$data['address1'] = (!empty($_POST['address1'])) ? $_POST['address1'] : '' ;
			$data['address2'] = (!empty($_POST['address2'])) ? $_POST['address2'] : '' ;
			$data['address3'] = (!empty($_POST['address3'])) ? $_POST['address3'] : '' ;
			$data['country_id'] = (!empty($_POST['country_id'])) ? $_POST['country_id'] : '' ;
			$data['state_id'] = (!empty($_POST['state_id'])) ? $_POST['state_id'] : '' ;
			$data['city_id'] = (!empty($_POST['city_id'])) ? $_POST['city_id'] : '' ;
			$data['pincode'] = (!empty($_POST['pincode'])) ? $_POST['pincode'] : '' ;
			$data['father_name'] = (!empty($_POST['father_name'])) ? $_POST['father_name'] : '' ;
			$data['father_contact_no'] = (!empty($_POST['father_contact_no'])) ? $_POST['father_contact_no'] : '' ;

			$data['father_contact_no2'] = (!empty($_POST['father_contact_no2'])) ? $_POST['father_contact_no2'] : '' ;

			$data['father_emailid'] = (!empty($_POST['father_emailid'])) ? $_POST['father_emailid'] : '' ;
			$data['father_education'] = (!empty($_POST['father_education'])) ? $_POST['father_education'] : '' ;
			$data['father_profession'] = (!empty($_POST['father_profession'])) ? $_POST['father_profession'] : '' ;
			$data['mother_name'] = (!empty($_POST['mother_name'])) ? $_POST['mother_name'] : '' ;
			$data['mother_contact_no'] = (!empty($_POST['mother_contact_no'])) ? $_POST['mother_contact_no'] : '' ;

			$data['mother_contact_no2'] = (!empty($_POST['mother_contact_no2'])) ? $_POST['mother_contact_no2'] : '' ;

			$data['mother_emailid'] = (!empty($_POST['mother_emailid'])) ? $_POST['mother_emailid'] : '' ;
			$data['mother_education'] = (!empty($_POST['mother_education'])) ? $_POST['mother_education'] : '' ;
			$data['mother_profession'] = (!empty($_POST['mother_profession'])) ? $_POST['mother_profession'] : '' ;
			// $data['how_know_about_school'] = (!empty($_POST['how_know_about_school'])) ? $_POST['how_know_about_school'] : '' ;
			$data['how_know_about_other'] = (!empty($_POST['how_know_about_other'])) ? $_POST['how_know_about_other'] : '' ;
			$data['inquiry_remark'] = (!empty($_POST['inquiry_remark'])) ? $_POST['inquiry_remark'] : '' ;
			// $data['inquiry_status'] = (!empty($_POST['inquirystatus'])) ? $_POST['inquirystatus'] : '' ;
			// $data['inquiry_progress'] = (!empty($_POST['inquiryprogress'])) ? $_POST['inquiryprogress'] : '' ;
			// $data['inquiry_handled_by'] = (!empty($_POST['inquiry_handled_by'])) ? $_POST['inquiry_handled_by'] : '' ;
			$data['lead_category'] = (!empty($_POST['lead_category'])) ? $_POST['lead_category'] : '' ;
			if(!empty($_POST['know_about_us']) && isset($_POST['know_about_us'])){
				$data['know_about_us'] = implode(",",$_POST['know_about_us']);
			}
			$data['type'] = 2;
			$data['lead_status_id'] = $_POST['lead_status'];
			$data['lead_sub_status_id'] = $_POST['lead_sub_status'];
			$data['lead_planned_status_id'] = $_POST['planned_status'];
			$data['lead_enquiry_status_id'] = $_POST['lead_enquiry_status'];

			$data['created_on'] = date("Y-m-d H:i:s");
			$data['created_by'] = $_SESSION["webadmin"][0]->user_id;
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
			$result = $this->inquiriesmodel->insertData('tbl_inquiry_master', $data, 1);
		}
		if(!empty($result)){
			echo json_encode(array("success"=>"1",'msg'=>'Inquery created/updated successfully!'));
			exit;
		}else{
			echo json_encode(array("success"=>"0",'msg'=>'Inquery not created.!'));
			exit;
		}	
	}
	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			//exit;
			$data_array=array();			
			$inquiry_master_id = $_POST['inquiry_master_id'];
			
			$data_array['inquiry_status'] = (!empty($_POST['inquiry_status'])) ? nl2br($_POST['inquiry_status']) : '';
			$data_array['inquiry_progress'] = (!empty($_POST['inquiry_progress'])) ? nl2br($_POST['inquiry_progress']) : '';
			
			$data_array['updated_on'] = date("Y-m-d H:i:s");
			
			$result = $this->inquiriesmodel->updateRecord('tbl_inquiry_master', $data_array,'inquiry_master_id',$inquiry_master_id);
			
			
		/*	
			if(!empty($_POST['feedback_reply'])){
				$to_email = "danish.akhtar@attoinfotech.com";			
				$replyto = "info@attoinfotech.website";		
				$subject = "Montana Feedback Reply";
				$headers = "From: info@attoinfotech.website\r\n";
				//$headers .= "Reply-To: info@attoinfotech.website\r\n";		
				//$headers .= "BCC: infodanish@gmail.com\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";							
				$message  = '<html><body>';		
				$message .= '<p> '.$message_txt.'</p>';
				$message .=  '</body></html>';
		   
				$mailop = mail($to_email, $subject, $message, $headers);
			}	
        */
			
			//exit;
			
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	
	function export(){
		$get_result = $this->inquiriesmodel->getExportRecords($_POST);

		// echo "<pre>";
		// print_r($get_result);
		// exit;
		if(!empty($get_result['query_result']) && count($get_result['query_result']) > 0)
		{
			$this->load->library('Excel');
			
			//Create new PHPExcel object
			$objPHPExcel = new PHPExcel();
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			//Set properties
			$objPHPExcel->getProperties()->setCreator("Attoinfotech")
			->setLastModifiedBy("Attoinfotech")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Export Excel");
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
			
			// Rename sheet
			$objPHPExcel->getActiveSheet()->setTitle('OutPut-File');
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("A1", 'LD Date', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("B1", 'LD No', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("C1", 'ENQ Date', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("D1", 'ENQ NO', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("E1", 'Type', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("F1", 'Student Name', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("G1", 'Father Name', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("H1", 'Father Email', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("I1", 'Father Number', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("J1", 'Mother Name', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("K1", 'Mother Email', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("L1", 'Mother Number', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("M1", 'Course Name', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("N1", 'Source', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("O1", 'Zone', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("P1", 'Center', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("Q1", 'Status', PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->getFont()->setBold(true);
			
			$j = 2;
			
			for($i=0;$i<sizeof($get_result['query_result']);$i++)
			{
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("A$j", (!empty($get_result['query_result'][$i]->lead_date)?date("m/d/Y",strtotime($get_result['query_result'][$i]->lead_date)):""));
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("B$j", "LD".$get_result['query_result'][$i]->lead_id);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("C$j", (!empty($get_result['query_result'][$i]->inquiry_date)?date("m/d/Y",strtotime($get_result['query_result'][$i]->inquiry_date)):""));
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("D$j", "ENQ".$get_result['query_result'][$i]->lead_no);
				
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("E$j", $get_result['query_result'][$i]->rec_type);
				
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("F$j", $get_result['query_result'][$i]->student_first_name." ".$get_result['query_result'][$i]->student_last_name );
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("G$j", $get_result['query_result'][$i]->father_name);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("H$j", $get_result['query_result'][$i]->father_emailid);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("I$j", $get_result['query_result'][$i]->father_contact_no);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("J$j", $get_result['query_result'][$i]->mother_name);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("K$j", $get_result['query_result'][$i]->mother_emailid);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("L$j", $get_result['query_result'][$i]->mother_contact_no);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("M$j", $get_result['query_result'][$i]->course_name);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("N$j", $get_result['query_result'][$i]->how_know_about_school);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("O$j", $get_result['query_result'][$i]->zone_name);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("P$j",  $get_result['query_result'][$i]->center_name);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("Q$j", $get_result['query_result'][$i]->inquiry_progress);
				
				$j++;

			}
			
			// Redirect output to a client's web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Inquiries('.date('d-m-Y').').xls"');
			header('Cache-Control: max-age=0');
			
			$objWriter->save('php://output');
			$_SESSION['contactus_export_success'] = "success";
			$_SESSION['contactus_export_msg'] = "Records Exported Successfully.";
			die();
			redirect('inquiries');
			exit;
		}
		else
		{
			$_SESSION['contactus_export_success'] = "error";
			$_SESSION['contactus_export_msg'] = "No records found for export";	
			redirect('inquiries');
			exit;
		}
	}
	
	
	public function getRegion(){
		//$result = $this->newslettersmodel->getOptions("tbl_subcategories",$_REQUEST['category_id'],"category_id");
		$input_data = array('action'=>'FetchRegion', 'BrandID'=>'112', 'Zone'=>''.$_REQUEST['zone_text'].'');
		$result = curlFunction(SERVICE_URL, $input_data);
		$result = json_decode($result, true);
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$region_text = '';
		
		if(isset($_REQUEST['region_text']) && !empty($_REQUEST['region_text'])){
			$region_text = $_REQUEST['region_text'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]['Region'] == $region_text) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]['Region'].'" '.$sel.' >'.$result[$i]['Region'].'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	
	public function getArea(){
		$input_data = array('action'=>'FetchArea', 'BrandID'=>'112', 'Region'=>''.$_REQUEST['region_text'].'');
		$result = curlFunction(SERVICE_URL, $input_data);
		$result = json_decode($result, true);
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$area_text = '';
		
		if(isset($_REQUEST['area_text']) && !empty($_REQUEST['area_text'])){
			$area_text = $_REQUEST['area_text'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]['Area'] == $area_text) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]['Area'].'" '.$sel.' >'.$result[$i]['Area'].'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getCenter(){
		$input_data = array('action'=>'FetchCenter', 'BrandID'=>'112', 'Area'=>''.$_REQUEST['area_text'].'');
		$result = curlFunction(SERVICE_URL, $input_data);
		$result = json_decode($result, true);
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]['CenterID'] == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]['CenterID'].'" '.$sel.' >'.$result[$i]['CenterName'].'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	function getSearchLists(){
		if(!empty($_POST['value']) && !empty($_POST['column_name'])){
			$options = "";
			if($_POST['column_name']  == "zone_id"){
				$centerDetails = $this->inquiriesmodel->getData("tbl_centers","center_id,center_name",array("status"=>'Active',"zone_id"=>$_POST['value']));
				$options = "<option value=''>Select Center</option>";
				if(!empty($centerDetails)){
					foreach($centerDetails as $key=>$val){
						$options .= "<option value='".$val['center_id']."'>".$val['center_name']."</option>"; 
					}
				}
				echo json_encode(array("status"=>true,"option"=>$options));
				exit;
			}else{
				$courseDetails = $this->inquiriesmodel->getCenterMappedCourses($_POST['value']);
				$options = " <option value=''>Select Class</option>";
				if(!empty($courseDetails)){
					foreach($courseDetails as $key=>$val){
						$options .= "<option value='".$val['course_id']."'>".$val['course_name']."</option>"; 
					}
				}
				echo json_encode(array("status"=>true,"option"=>$options));
				exit;
			}
		}
	}
	public function addEditFollow()
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			
			$result = "";
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
				// echo $record_id;
				$result['details'] = $this->inquiriesmodel->getFormdata($record_id);
				$result['status_logs'] = $this->inquiriesmodel->getdata_log($record_id);
				$result['follow_logs_id'] = $this->inquiriesmodel->getDataDescLimit("tbl_lead_enquiry_log", "*","lead_id = $record_id and lead_status_id IS NULL and lead_sub_status_id IS NULL and lead_planned_status_id IS NULL and lead_enquiry_status_id IS NULL");
				// echo $this->db->last_query();exit;
				$condition ="i.lead_id = '".$record_id."' and i.lead_status_id !='' and i.lead_sub_status_id !='' and i.lead_planned_status_id !='' and i.lead_enquiry_status_id !='' ";
				$result['follow_logs_last'] = $this->inquiriesmodel->getlastdata($condition);
				// echo $this->db->last_query();exit;
			}
			$result['status'] = $this->inquiriesmodel->getdata("tbl_lead_status", "*","status = 'Active' ");
			$result['planned_status'] = $this->inquiriesmodel->getdata("tbl_default_status", "*","status = 'Active' ");
			
			// echo "<pre>";
			// print_r($result['follow_logs']);
			// exit;
			$this->load->view('template/header.php');
			$this->load->view('inquiries/addEditfollow', $result);
			$this->load->view('template/footer.php');
		}
		else {
			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	public function submitFollowUpForm()
	{
		// echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				$data = array();
				$data_follow = array();
				$data['lead_status_id'] = $_POST['lead_status'];
				$data['lead_sub_status_id'] = $_POST['lead_sub_status'];
				$data['lead_planned_status_id'] = $_POST['planned_status'];
				$data['lead_enquiry_status_id'] = $_POST['lead_enquiry_status'];
				$data['notes'] = $_POST['note'];
				$data['type'] = 2;

			
				$data['updated_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				$result = $this->inquiriesmodel->updateRecord('tbl_lead_enquiry_log', $data,'ID',$_POST['log_lead_id']);

				if(!empty($_POST['next_follow_up']) && isset($_POST['next_follow_up'])){
					$data_follow['follow_up_date'] =  date('Y-m-d', strtotime($_POST['next_follow_up']));
					$data_follow['follow_up_time'] =  $_POST['follow_up_time'];
				}
				if(!empty($_POST['next_follow_up']) && isset($_POST['next_follow_up'])){
					$data_follow['created_on'] = date("Y-m-d H:i:s");
					$data_follow['created_by'] = $_SESSION["webadmin"][0]->user_id;
					$data_follow['lead_id'] = $_POST['hidden_lead_id'];
					$data_follow['type'] = 2;
					$result_log = $this->inquiriesmodel->insertData('tbl_lead_enquiry_log', $data_follow, '1');
				}
			if (!empty($result)) {
				echo json_encode(array('success' => true,'msg' => 'Record Added/Updated Successfully.'));
				exit;
			}else{
				echo json_encode(array('success' => false,'msg' => 'Problem in data update.'));
				exit;
			}
			
		}else {
			echo json_encode(array('success' => false,'msg' => 'this is not ajax submit'));
			exit;
		}
	}
	function getSubStatus(){
		$result = $this->inquiriesmodel->getdata("tbl_lead_sub_status","*"," lead_status_id = ".$_POST['status_id']."  ");
		$option = '';
			if(!empty($result)){
				foreach ($result as $key => $value) {
					$sel='';
					$sel = ($value['lead_sub_status'] == $_POST['sub_status_id'])? 'selected="selected"' : '';
					$option .= '<option value='.$value['lead_sub_status'].' '.$sel.' >'.$value['status_sub_description'].'</option>';
				}
				echo json_encode(array("status"=>"success","option"=>$option));
				exit;
			}
	}
	function getLeadEnquiyStage(){
		// print_r($_POST);
		$result = $this->inquiriesmodel->getdata("tbl_lead_enquiry_status","*"," status_id = ".$_POST['status_id']." AND sub_status_id = ".$_POST['sub_status_id']."  ");
		// print_r($result);exit;
		$option = '';
			if(!empty($result)){
				foreach ($result as $key => $value) {
					$sel='';
					if(!empty($_POST['lead_enquiry_status_id'])){
						$sel = ($value['lead_enquiry_status_id'] == $_POST['lead_enquiry_status_id'])? 'selected' : '';
					}else{
						$sel='selected';
					}
					$option .= '<option value='.$value['lead_enquiry_status_id'].' '.$sel.' >'.$value['lead_enquiry_status_description'].'</option>';
				}
				echo json_encode(array("status"=>"success","option"=>$option));
				exit;
			}
	}
	
}

?>
