<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Feesmaster extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('feesmastermodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('feesmaster/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$result = array();
			$record_id = "";
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
				$result['details'] = $this->feesmastermodel->getFormdata($record_id);
				if(!empty($result['details']) && $result['details'][0]->fees_selection_type == "Class"){
					$selected_courses = $this->feesmastermodel->getdata("tbl_fees_master_course_mapping","fees_id='".$record_id."' ");
					if(!empty($selected_courses)){
						$result['selected_courses'] = array_column($selected_courses,"course_id");
					}
				}else{
					//selected group mappiing
					$selected_groups = $this->feesmastermodel->getdata("tbl_fees_master_group_mapping","fees_id='".$record_id."' ");
					if(!empty($selected_groups)){
						$result['selected_groups'] = array_column($selected_groups,"group_master_id");
					}

					//selected frequency mapping
					$selected_frequency = $this->feesmastermodel->getdata("tbl_fees_frequency_mapping","fees_id='".$record_id."' ");
					if(!empty($selected_frequency)){
						$result['selected_frequency'] = array_column($selected_frequency,"month_id");
					}
				}
			}
			
			$result['academic_year'] = $this->feesmastermodel->getDropdown("tbl_academic_year_master","academic_year_master_id,academic_year_master_name");
			$result['categories'] = $this->feesmastermodel->getDropdown("tbl_categories","category_id,categoy_name");

			//course is only for preschool category_id=1
			$result['courses'] = $this->feesmastermodel->getdata("tbl_courses","category_id=1");

			$result['groups'] = $this->feesmastermodel->getDropdown("tbl_group_master","group_master_id,group_master_name");
			$result['fees_components'] = $this->feesmastermodel->getDropdown("tbl_fees_component_master","fees_component_master_id,fees_component_master_name");
			$result['fees_level'] = $this->feesmastermodel->getDropdown("tbl_fees_level_master","fees_level_id,fees_level_name");
			$result['gsttax'] = $this->feesmastermodel->getDropdown("tbl_gst_master","gst_master_id,tax");
			$result['fees_type_details'] = $this->feesmastermodel->enum_select("tbl_fees_master","fees_type");
			
			if(!empty($result['details'][0]->fees_id)){
				$result['componentdetails'] = $this->feesmastermodel->getFormdata1($result['details'][0]->fees_id);
			}

			//get group frequency 
			$result['frequency_details'] = $this->feesmastermodel->getAllFrequency();
			// echo "<pre>";print_r($result);exit;
			$this->load->view('template/header.php');
			$this->load->view('feesmaster/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function getCourses(){
		$result = $this->feesmastermodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['course_id']) && !empty($_REQUEST['course_id'])){
			$course_id = $_REQUEST['course_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}	

	function getCalcamount(){
		$result = $this->feesmastermodel->getOptions1("tbl_fees_component_master",$_REQUEST['components_id'],"fees_component_master_id");
		if(!empty($result)){
			$sum = 0;
			for($i=0;$i<sizeof($result);$i++){
				$sum = $sum + $result[$i]->amount;
			}
		}
		echo json_encode(array("status"=>"success","sum"=>$sum));
		exit();
	}

	function getComponentReceiptName(){
		$result = $this->feesmastermodel->getOptions("tbl_fees_component_master",$_REQUEST['component_id'],"fees_component_master_id");
		
		echo json_encode(array("status"=>"success","option"=>$result));
		exit;
	}

	function getGstRate(){
		$result = $this->feesmastermodel->getDropdown("tbl_gst_master","gst_master_id,tax");
		$option = '';
		if($_REQUEST['gst_id'] == 'Yes'){
			if(!empty($result)){
				for($i=0;$i<sizeof($result);$i++){
					$option .= '<option value="'.$result[$i]->gst_master_id.'" >'.$result[$i]->tax.'</option>';
				}
			}
		}
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function submitForm()
	{ 
		// echo "<pre>";
		// print_r($_POST);
		// exit();
		// $components_id_string = implode(',',$_REQUEST['fees_component_id']);
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if(!isset($_POST['fees_component_id'])){
				echo json_encode(array("success"=>"0",'msg'=>'Please select component!'));
				exit;
			}
			foreach($_POST['fees_component_id'] as $key=>$value){
				if($_POST['is_gst'][$key] == 'Yes'){
					if($_POST['gst_tax_id'][$key] == ''){
						echo json_encode(array("success"=>"0",'msg'=>' Select GST Rate!'));
							exit;
					}
				}
			}
			if(!empty($_POST['fees_component_id'])){
				if(!empty($_POST['fees_id'])){
					if($_POST['fees_selection_type'] == 'Class'){
						$condition = "academic_year_id='".$_POST['academic_year_id']."' && category_id='".$_POST['category_id']."' && fees_name= '".$_POST['fees_name']."' ";
						
					}
					if($_POST['fees_selection_type'] == 'Group'){
						$condition = "academic_year_id='".$_POST['academic_year_id']."' && fees_name= '".$_POST['fees_name']."' ";
					}
					if(isset($_POST['fees_id']) && $_POST['fees_id'] > 0){
						$condition .= " &&  fees_id != ".$_POST['fees_id'];
					}

					$check_name = $this->feesmastermodel->getdata("tbl_fees_master",$condition);
					if(!empty($check_name)){
						$getFeesName = $this->feesmastermodel->getdata("tbl_fees_master", "fees_id='".$_POST['fees_id']."' ");
						echo json_encode(array("success"=>"0",'msg'=>' '.$getFeesName[0]['fees_name'].' Already assign to selected fees!'));
						exit;
					}
				}else{
					if($_POST['fees_selection_type'] == 'Class'){
						foreach($_POST['course_id'] as $key=>$val){
							// $condition = "academic_year_id='".$_POST['academic_year_id']."' && category_id='".$_POST['category_id']."' && course_id='".$val."' && fees_name= '".$_POST['fees_name']."' ";
							$condition = "academic_year_id='".$_POST['academic_year_id']."' && category_id='".$_POST['category_id']."' && fees_name= '".$_POST['fees_name']."' ";

							$check_name = $this->feesmastermodel->getdata("tbl_fees_master",$condition);
							if(!empty($check_name)){
								$getFeesName = $this->feesmastermodel->getdata("tbl_fees_master", "fees_id='".$_POST['fees_id']."' ");
								echo json_encode(array("success"=>"0",'msg'=>' '.$getFeesName[0]['fees_name'].' Already assign to selected fees!'));
								exit;
							}
						}
					}
					if($_POST['fees_selection_type'] == 'Group'){
						foreach($_POST['group_id'] as $key=>$val){
							$condition = "academic_year_id='".$_POST['academic_year_id']."' && fees_name= '".$_POST['fees_name']."' ";
							$check_name = $this->feesmastermodel->getdata("tbl_fees_master",$condition);
							// echo "<pre>";
							// echo "group";
							// print_r($check_name);
							// echo $this->db->last_query();
							if(!empty($check_name)){
								$getFeesName = $this->feesmastermodel->getdata("tbl_fees_master", "fees_id='".$_POST['fees_id']."' ");
								echo json_encode(array("success"=>"0",'msg'=>' '.$getFeesName[0]['fees_name'].' Already assign to selected fees!'));
								exit;
							}
						}
					}
				}
			}
			// exit;
			$fees_id = "";
			if (!empty($_POST['fees_id'])) {
				$data_array = array();			
				$fees_id = $_POST['fees_id'];
				$data_array['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
				$data_array['fees_selection_type'] = (!empty($_POST['fees_selection_type'])) ? $_POST['fees_selection_type'] : '';
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0;
				$data_array['fees_level_id'] = (!empty($_POST['fees_level_id'])) ? $_POST['fees_level_id'] : '';
				$data_array['fees_type'] = (!empty($_POST['fees_type'])) ? $_POST['fees_type'] : '';
				$data_array['fees_name'] = (!empty($_POST['fees_name'])) ? $_POST['fees_name'] : '';
				if($data_array['fees_selection_type'] == "Class"){
					$data_array['installment_no'] = (!empty($_POST['installment_no'])) ? $_POST['installment_no'] : 0;
					$data_array['allow_frequency'] = "No";
				}else{
					if(!empty($_POST['allow_frequency']) && $_POST['allow_frequency'] == "Yes"){
						$data_array['installment_no'] = 0;
						$data_array['allow_frequency'] = $_POST['allow_frequency'];
					}else{
						$data_array['installment_no'] = (!empty($_POST['installment_no'])) ? $_POST['installment_no'] : 0;
						$data_array['allow_frequency'] = "No";
					}
				}
				
				//fees from date and to date
				$data_array['fees_from_date'] = (!empty($_POST['fees_from_date'])) ? date("Y-m-d", strtotime($_POST['fees_from_date'])) : '';
				$data_array['fees_to_date'] = (!empty($_POST['fees_to_date'])) ? date("Y-m-d", strtotime($_POST['fees_to_date'])) : '';
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				$result = $this->feesmastermodel->updateRecord('tbl_fees_master', $data_array,'fees_id',$fees_id);
				$getComponentId = $this->feesmastermodel->deletedata("tbl_fees_component_data", 'fees_master_id',$_POST['fees_id']);

				
				$data_array1 = array();
				$data_array1['fees_master_id'] = $_POST['fees_id'];
				$fees_component_data_id = $_POST['fees_component_data_id'];
				foreach($_POST['fees_component_id'] as $key=>$value){
					$fees_component_id = (!empty($_POST['fees_component_id'][$key])) ? $_POST['fees_component_id'][$key] : '';
						// print_r($_POST['fees_component_id']);exit();
					$data_array1['fees_component_id'] = $fees_component_id;
					$data_array1['component_type'] = (!empty($_POST['component_type'][$key])) ? $_POST['component_type'][$key] : '';
					$data_array1['component_fees'] = (!empty($_POST['component_fees'][$key])) ? $_POST['component_fees'][$key] : '';
					$data_array1['receipt_name'] = (!empty($_POST['receipt_name'][$key])) ? $_POST['receipt_name'][$key] : '';
					$data_array1['is_discountable'] = (!empty($_POST['is_discountable'][$key])) ? $_POST['is_discountable'][$key] : '';
					$data_array1['is_gst'] = (!empty($_POST['is_gst'][$key])) ? $_POST['is_gst'][$key] : '';
					if($_POST['is_gst'][$key] == 'Yes'){
						$data_array1['gst_tax_id'] = (!empty($_POST['gst_tax_id'][$key])) ? $_POST['gst_tax_id'][$key] : '';
					}
					else{
						$data_array1['gst_tax_id'] = 0;
					}

					$data_array1['created_on'] = date("Y-m-d H:i:s");
					$data_array1['updated_on'] = date("Y-m-d H:i:s");
					$result = $this->feesmastermodel->insertData('tbl_fees_component_data', $data_array1, '1');
				}
			}else {
				$data_array = array();
				$data_array['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
				$data_array['fees_selection_type'] = (!empty($_POST['fees_selection_type'])) ? $_POST['fees_selection_type'] : '';
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0;
				$data_array['fees_level_id'] = (!empty($_POST['fees_level_id'])) ? $_POST['fees_level_id'] : '';
				$data_array['fees_type'] = (!empty($_POST['fees_type'])) ? $_POST['fees_type'] : '';
				$data_array['fees_name'] = (!empty($_POST['fees_name'])) ? $_POST['fees_name'] : '';
			if($data_array['fees_selection_type'] == "Class"){
					$data_array['installment_no'] = (!empty($_POST['installment_no'])) ? $_POST['installment_no'] : 0;
					$data_array['allow_frequency'] = "No";
				}else{
					if(!empty($_POST['allow_frequency']) && $_POST['allow_frequency'] == "Yes"){
						$data_array['installment_no'] = 0;
						$data_array['allow_frequency'] = $_POST['allow_frequency'];
					}else{
						$data_array['installment_no'] = (!empty($_POST['installment_no'])) ? $_POST['installment_no'] : 0;
						$data_array['allow_frequency'] = "No";
					}
				}
				
				//fees from date and to date
				$data_array['fees_from_date'] = (!empty($_POST['fees_from_date'])) ? date("Y-m-d", strtotime($_POST['fees_from_date'])) : '';
				$data_array['fees_to_date'] = (!empty($_POST['fees_to_date'])) ? date("Y-m-d", strtotime($_POST['fees_to_date'])) : '';
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->feesmastermodel->insertData('tbl_fees_master', $data_array, '1');
				$fees_id = $result;
				$data_array1 = array();
				$data_array1['fees_master_id'] = $result;
				// print_r($_POST);exit();
				foreach($_POST['fees_component_id'] as $key=>$value){
					$fees_component_id = (!empty($_POST['fees_component_id'])) ? $_POST['fees_component_id'][$key] : '';
					$data_array1['fees_component_id'] = $fees_component_id;
					$data_array1['component_type'] = (!empty($_POST['component_type'][$key])) ? $_POST['component_type'][$key] : '';
					$data_array1['component_fees'] = (!empty($_POST['component_fees'][$key])) ? $_POST['component_fees'][$key] : '';
					$data_array1['receipt_name'] = (!empty($_POST['receipt_name'][$key])) ? $_POST['receipt_name'][$key] : '';
					$data_array1['is_discountable'] = (!empty($_POST['is_discountable'][$key])) ? $_POST['is_discountable'][$key] : '';
					$data_array1['is_gst'] = (!empty($_POST['is_gst'][$key])) ? $_POST['is_gst'][$key] : '';
					if($_POST['is_gst'][$key] == 'Yes'){
						$data_array1['gst_tax_id'] = (!empty($_POST['gst_tax_id'][$key])) ? $_POST['gst_tax_id'][$key] : '';
					}
					else{
						$data_array1['gst_tax_id'] = 0;
					}

					$data_array1['created_on'] = date("Y-m-d H:i:s");
					$data_array1['updated_on'] = date("Y-m-d H:i:s");
					$this->feesmastermodel->insertData('tbl_fees_component_data', $data_array1, '1');
				}
			}

			//assign multiple course or group and frequency
			if($fees_id != ""){
				//delete group and course mapped data
				$this->feesmastermodel->deletedata("tbl_fees_master_course_mapping","fees_id",$fees_id);
				$this->feesmastermodel->deletedata("tbl_fees_master_group_mapping","fees_id",$fees_id);
				$this->feesmastermodel->deletedata("tbl_fees_frequency_mapping","fees_id",$fees_id);
				if($_POST['fees_selection_type'] == "Class"){
					if(!empty($_POST['course_id'])){
						foreach($_POST['course_id'] as $key=>$val){
							$couse_mapping_data = array();
							$couse_mapping_data['fees_id'] = $fees_id;
							$couse_mapping_data['course_id'] = $val;
							$this->feesmastermodel->insertData('tbl_fees_master_course_mapping', $couse_mapping_data, '1');
						}
					}
				}else{
					if(!empty($_POST['group_id'])){
						foreach($_POST['group_id'] as $key=>$val){
							$group_mapping_data = array();
							$group_mapping_data['fees_id'] = $fees_id;
							$group_mapping_data['group_master_id'] = $val;
							$this->feesmastermodel->insertData('tbl_fees_master_group_mapping', $group_mapping_data, '1');
						}
					}

					if(!empty($_POST['frequency']) && $_POST['allow_frequency'] == "Yes"){
						foreach($_POST['frequency'] as $key=>$val){
							$frequency_mapping_data = array();
							$frequency_mapping_data['fees_id'] = $fees_id;
							$frequency_mapping_data['month_id'] = $val;
							$this->feesmastermodel->insertData('tbl_fees_frequency_mapping', $frequency_mapping_data, '1');
						}
					}
				}
			}

			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
		}
		else {
			return false;
		}
	
	}
	
	function saveMultipleData($post,$id,$type){
		$_POST = $post;
		$data_array = array();
		$data_array['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
		$data_array['fees_selection_type'] = (!empty($_POST['fees_selection_type'])) ? $_POST['fees_selection_type'] : '';
		$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0;
		if($type == "Class"){
			$data_array['course_id'] = (!empty($id)) ? $id : 0;
			$data_array['group_id'] = 0;
		}else{
			$data_array['group_id'] = (!empty($id)) ? $id : 0;
			$data_array['course_id'] = 0;
		}
		$data_array['fees_level_id'] = (!empty($_POST['fees_level_id'])) ? $_POST['fees_level_id'] : '';
		$data_array['fees_type'] = (!empty($_POST['fees_type'])) ? $_POST['fees_type'] : '';
		$data_array['fees_name'] = (!empty($_POST['fees_name'])) ? $_POST['fees_name'] : '';
		$data_array['installment_no'] = (!empty($_POST['installment_no'])) ? $_POST['installment_no'] : 0;

		//fees from date and to date
		$data_array['fees_from_date'] = (!empty($_POST['fees_from_date'])) ? date("Y-m-d", strtotime($_POST['fees_from_date'])) : '';
		$data_array['fees_to_date'] = (!empty($_POST['fees_to_date'])) ? date("Y-m-d", strtotime($_POST['fees_to_date'])) : '';
		
		$data_array['created_on'] = date("Y-m-d H:i:s");
		$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
		$data_array['updated_on'] = date("Y-m-d H:i:s");
		$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
		
		$result = $this->feesmastermodel->insertData('tbl_fees_master', $data_array, '1');
		$data_array1 = array();
		$data_array1['fees_master_id'] = $result;
		// print_r($_POST);exit();
		foreach($_POST['fees_component_id'] as $key=>$value){
			$fees_component_id = (!empty($_POST['fees_component_id'])) ? $_POST['fees_component_id'][$key] : '';
			$data_array1['fees_component_id'] = $fees_component_id;
			$data_array1['component_type'] = (!empty($_POST['component_type'][$key])) ? $_POST['component_type'][$key] : '';
			$data_array1['component_fees'] = (!empty($_POST['component_fees'][$key])) ? $_POST['component_fees'][$key] : '';
			$data_array1['receipt_name'] = (!empty($_POST['receipt_name'][$key])) ? $_POST['receipt_name'][$key] : '';
			$data_array1['is_discountable'] = (!empty($_POST['is_discountable'][$key])) ? $_POST['is_discountable'][$key] : '';
			$data_array1['is_gst'] = (!empty($_POST['is_gst'][$key])) ? $_POST['is_gst'][$key] : '';
			if($_POST['is_gst'][$key] == 'Yes'){
				$data_array1['gst_tax_id'] = (!empty($_POST['gst_tax_id'][$key])) ? $_POST['gst_tax_id'][$key] : '';
			}
			else{
				$data_array1['gst_tax_id'] = 0;
			}

			$data_array1['created_on'] = date("Y-m-d H:i:s");
			$data_array1['updated_on'] = date("Y-m-d H:i:s");
			$this->feesmastermodel->insertData('tbl_fees_component_data', $data_array1, '1');
		}
		return $result;
	}


	/*new code end*/
	function fetch($id=null)
	{
		$get_result = $this->feesmastermodel->getRecords($_GET);
		// print_r($get_result);
		// exit();
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->academic_year_master_name);
				array_push($temp, $get_result['query_result'][$i]->fees_selection_type);
				array_push($temp, $get_result['query_result'][$i]->fees_level_name);
				array_push($temp, $get_result['query_result'][$i]->fees_name);
				// array_push($temp, $get_result['query_result'][$i]->fees_type);
				if($get_result['query_result'][$i]->fees_selection_type == "Class"){
					array_push($temp, $get_result['query_result'][$i]->categoy_name);
				}else{
					array_push($temp, "--");
				}
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("FeesMasterAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->fees_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	
				
				$actionCol1 = "";
				// if($this->privilegeduser->hasPrivilege("FeesMasterAddEdit")){
					$actionCol1.= '<a href="feesmaster/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->fees_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				// }	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}

	
	function delrecord12()
	{
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->feesmastermodel->delrecord12("tbl_fees_master","fees_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
}

?>
