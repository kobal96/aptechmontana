<?PHP
class Remainingfeesmodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
	 	//print_r($this->db->last_query());
	    //exit;
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}

	function getDropdown1($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status = "Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}

	function getOptions($tbl_name,$tbl_id,$comp_id){
		//echo "Cat ID: ".$category_id;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getStudentClassWiseList($condition){
		$this -> db -> select('s.student_id,s.inquiry_master_id,s.enrollment_no,s.zone_id,s.center_id,s.student_first_name,s.student_last_name,s.dob, d.admission_date, d.academic_year_id, d.category_id, d.course_id, d.batch_id ');
		$this -> db -> from('tbl_student_details as d');
		$this -> db -> join('tbl_student_master as s', 'd.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_admission_fees as af', 'af.student_id  = d.student_id', 'right');
		$this->db->where("($condition)");
		$this->db->order_by('s.student_first_name', 'asc');
		$this->db->group_by('d.student_id');
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
		
	}

	function getdata($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getdata1($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }

	function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 

	function updateRecordbyConditionArray($table,$data,$condition){
		$this->db->where("($condition)");
		$this->db->update($table,$data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}else {
			return true;
		}
	} 

	function getPayemtDetails($condition){
		$this -> db -> select('i.*, s.student_id, s.student_first_name, s.student_last_name,s.enrollment_no, s.father_name, s.present_address,s.address1, s.address2, s.address3,s.pincode, s.father_mobile_contact_no, s.father_email_id,s.parent_name,s.guardian,s.mother_name,s.password, ct.categoy_name, cr.course_name, b.batch_name, g.group_master_name, a.academic_year_master_name, cn.center_name, cn.center_address, cn.center_contact_no, cn.center_email_id, cn.center_code,cn.receipt_print_name,fp.*,country.country_name,state.state_name,city.city_name');
		$this -> db -> from('tbl_admission_fees as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cr', 'i.course_id  = cr.course_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id = g.group_master_id', 'left');
		$this -> db -> join('tbl_centers as cn', 'i.center_id = cn.center_id', 'left');
		$this -> db -> join('tbl_fees_payment_details as fp', 'i.admission_fees_id = fp.admission_fees_id', 'right');
		$this -> db -> join('tbl_countries as country', 's.country_id = country.country_id', 'inner');
		$this -> db -> join('tbl_states as state', 's.state_id = state.state_id', 'inner');
		$this -> db -> join('tbl_cities as city', 's.city_id = city.city_id', 'inner');

		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		//echo $this->db->last_query();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

	function getdata_orderby_limit1($table, $fields, $condition = '1=1', $order_by){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition $order_by limit 1");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getFeesComponents($condition){
		
		$this -> db -> select('i.*,f.fees_component_master_id, f.fees_component_master_name');
		$this -> db -> from('tbl_fees_component_data as i');
		$this -> db -> join('tbl_fees_component_master as f', 'i.fees_component_id  = f.fees_component_master_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

	function getStudentGroupWiseList($condition){
		
		$this -> db -> select('s.student_id,s.inquiry_master_id,s.enrollment_no,s.zone_id,s.center_id,s.student_first_name,s.student_last_name,s.dob, d.academic_year_id');
		$this -> db -> from('tbl_student_group as d');
		$this -> db -> join('tbl_student_master as s', 'd.student_id  = s.student_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('s.student_first_name', 'asc');
		$this->db->group_by('s.student_id', 'asc');
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}

	/* ----------------------------------receipt functions ------------------------------------ */
	function getRecords($get){
		$table = "tbl_fees_payment_receipt as i";
		$table_id = 'i.fees_payment_receipt_id';
		$default_sort_column = 'i.fees_payment_receipt_id';
		$default_sort_order = 'desc';
		
		$condition = "1=1 ";
		if($_SESSION["webadmin"][0]->role_id != 1){
			$condition .= " && af.center_id='".$_SESSION["webadmin"][0]->center_id."' ";
		}
		$colArray = array('i.created_on','c.center_name','i.receipt_no','sm.student_first_name','i.receipt_amount','i.payment_mode','i.status');
		$searchArray = array('i.created_on','c.center_name','i.receipt_no','sm.student_first_name','i.receipt_amount','i.payment_mode','i.status');
		
		$page = $get['iDisplayStart'];// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];// iDisplayLength no of records from the offset
		if(isset($get['Searchkey_0']) && $get['Searchkey_0']!=''){
			$condition .= " && i.receipt_no like '%".$_GET['Searchkey_0']."%' || sm.student_first_name like '%".$_GET['Searchkey_0']."%' || sm.student_last_name like '%".$_GET['Searchkey_0']."%' || c.center_name like '%".$_GET['Searchkey_0']."%' || sm.enrollment_no like '%".$_GET['Searchkey_0']."%' ";
		}
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;
		
		$this -> db -> select('i.*,c.center_name,CONCAT(sm.student_first_name," ",sm.student_last_name) as student_name,sm.enrollment_no,sm.father_name,sm.mother_name,sm.parent_name,sm.guardian,fpd.payment_mode,fpd.fees_remark,fpd.transaction_id,fpd.bank_name');
		$this -> db -> from($table);
		$this -> db -> join("tbl_admission_fees as af","af.admission_fees_id = i.admission_fees_id");
		$this -> db -> join("tbl_student_master as sm","sm.student_id = af.student_id");
		$this -> db -> join("tbl_centers as c","sm.center_id = c.center_id");

		$this -> db -> join("tbl_fees_payment_details as fpd","i.receipt_no = fpd.receipt_no");

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$this->db->group_by('i.receipt_no');		
		$query = $this -> db -> get();
		
		//echo $this->db->last_query();exit;
		$this -> db -> select('i.*,c.center_name,CONCAT(sm.student_first_name," ",sm.student_last_name) as student_name,sm.enrollment_no,sm.father_name,sm.mother_name,sm.parent_name,sm.guardian,fpd.payment_mode,fpd.fees_remark,fpd.transaction_id,fpd.bank_name');
		$this -> db -> from($table);
		$this -> db -> join("tbl_admission_fees as af","af.admission_fees_id = i.admission_fees_id");
		$this -> db -> join("tbl_student_master as sm","sm.student_id = af.student_id");
		$this -> db -> join("tbl_centers as c","sm.center_id = c.center_id");
		$this -> db -> join("tbl_fees_payment_details as fpd","i.receipt_no = fpd.receipt_no");
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);	
		$this->db->group_by('i.receipt_no');	
		$query1 = $this -> db -> get();
		//echo $this->db->last_query();exit;
		if($query -> num_rows() >= 1){
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}else{
			return array("totalRecords"=>0);
		}
		//exit;
	}

	
	function getGroupFeesComponents($admission_fees_id,$group_id,$is_mandatory = null){
		if($admission_fees_id != "" && $group_id != ""){
			$condition = array("af.admission_fees_id"=>$admission_fees_id,"af.group_id"=>$group_id);
			if($is_mandatory != null){
				$condition['fcm.is_mandatory'] = $is_mandatory;
			}
			$this -> db -> select('afc.*,fcm.is_mandatory');
			$this -> db -> from('tbl_admission_fees_components as afc');
			$this -> db -> join('tbl_admission_fees as af', 'af.admission_fees_id  = afc.admission_fees_id', 'left');
			$this -> db -> join('tbl_fees_component_master as fcm', 'fcm.fees_component_master_id  = afc.fees_component_id', 'left');
			$this->db->where($condition);
			$this->db->order_by("afc.fees_component_id","ASC");
			$query = $this -> db -> get();
			if($query -> num_rows() >= 1){
				return $query->result_array();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function getClassFeesComponents($admission_fees_id,$course_id,$is_mandatory = null){
		if($admission_fees_id != "" && $course_id != ""){
			$condition = array("af.admission_fees_id"=>$admission_fees_id,"af.course_id"=>$course_id);
			if($is_mandatory != null){
				$condition['fcm.is_mandatory'] = $is_mandatory;
			}
			$this -> db -> select('afc.*,fcm.is_mandatory');
			$this -> db -> from('tbl_admission_fees_components as afc');
			$this -> db -> join('tbl_admission_fees as af', 'af.admission_fees_id  = afc.admission_fees_id', 'left');
			$this -> db -> join('tbl_fees_component_master as fcm', 'fcm.fees_component_master_id  = afc.fees_component_id', 'left');
			$this->db->where($condition);
			$this->db->order_by("afc.fees_component_id","ASC");
			$query = $this -> db -> get();
			if($query -> num_rows() >= 1){
				return $query->result_array();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function getFeesComponentsforReceipt($condition){
		$this -> db -> select('afcch.component_collected_history_id,fcm.fees_component_master_name,fcm.receipt_name,af.fees_type, afcch.collected_amount,c.course_name,g.group_master_name');
		$this -> db -> from('tbl_admission_fees_components as afc');
		$this -> db -> join('tbl_admission_fees_components_collected_history as afcch', 'afcch.admission_fees_component_id  = afc.admission_fees_component_id', 'left');
		$this -> db -> join('tbl_fees_component_master as fcm', 'fcm.fees_component_master_id  = afc.fees_component_id ', 'left');
		$this -> db -> join('tbl_admission_fees as af', 'af.admission_fees_id  = afc.admission_fees_id ', 'right');
		$this -> db -> join('tbl_courses as c', 'c.course_id  = af.course_id ', 'left');
		$this -> db -> join('tbl_group_master as g', 'g.group_master_id  = af.group_id ', 'left');
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

	function getFeesComponentPaidDetails($admission_fees_id){
		$this -> db -> select('afcch.remaining_amount');
		$this -> db -> from('tbl_admission_fees_components as afc');
		$this -> db -> join('tbl_admission_fees_components_collected_history as afcch', 'afcch.admission_fees_component_id  = afc.admission_fees_component_id', 'left');
		$this -> db -> join('tbl_fees_component_master as fcm', 'fcm.fees_component_master_id  = afc.fees_component_id ', 'left');
		$this -> db -> join('tbl_admission_fees as af', 'af.admission_fees_id  = afc.admission_fees_id ', 'right');
		$condition  =array("afcch.is_last_entry"=>"Yes","af.admission_fees_id"=>$admission_fees_id,"fcm.is_mandatory"=>"Yes");
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

	function getFeesComponentsforinvoice($admission_fees_id){
		$this -> db -> select('ac.*,f.fees_component_master_id, f.fees_type, f.fees_component_master_name');
		$this -> db -> from('tbl_admission_fees_components as ac');
		$this -> db -> join('tbl_fees_component_master as f', 'ac.fees_component_id  = f.fees_component_master_id', 'inner');
		
		$this->db->where('ac.admission_fees_id',$admission_fees_id);
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	function getinstallmentforinvoice($admission_fees_id)
	{
		$this -> db -> select('*');
		$this -> db -> from('tbl_admission_fees_instalments');
		$this->db->where('admission_fees_id',$admission_fees_id);
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	function enum_select($table, $field){
		$query = " SHOW COLUMNS FROM `$table` LIKE '$field' ";
		$row = $this->db->query(" SHOW COLUMNS FROM `$table` LIKE '$field' ")->row()->Type;
		$regex = "/'(.*?)'/";
		preg_match_all( $regex , $row, $enum_array );
		$enum_fields = $enum_array[1];
		return( $enum_fields );
	}

	function getAdmissionDetails($admission_fees_id){
		$this -> db -> select('sd.*,af.*');
		$this -> db -> from('tbl_student_details as sd');
		$this -> db -> join('tbl_admission_fees as af', 'af.student_id  = sd.student_id AND af.academic_year_id = sd.academic_year_id', 'left');
		$condition = array("af.admission_fees_id"=>$admission_fees_id);
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

	function getReceiptHistoryDetails($receipt_no){
		$this -> db -> select('fpd.*');
		$this -> db -> from('tbl_fees_payment_details as fpd');
		$condition = array("fpd.receipt_no"=>$receipt_no);
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

	function getRceievedInstallmentDetials($admission_fees_component_id){
		$this -> db -> select('afi.*');
		$this -> db -> from('tbl_admission_fees_instalments as afi');
		$this -> db -> join('tbl_admission_fees_components as afc', 'afc.admission_fees_id  = afi.admission_fees_id', 'LEFT');
		$this -> db -> join('tbl_admission_fees_components_collected_history as afcc', 'afcc.admission_fees_component_id  = afc.admission_fees_component_id', 'LEFT');
		$condition = array("afc.admission_fees_component_id"=>$admission_fees_component_id);
		$this->db->where($condition);
		$this->db->group_by("afi.admission_fees_instalment_id");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

	function getCenterFees($condition,$fees_type =  null){
		$this -> db -> select('f.fees_id,f.fees_name, f.installment_no as max_installment_allowed');
		$this -> db -> from('tbl_assign_fees as i');
		$this -> db -> join('tbl_fees_master as f', 'i.fees_id  = f.fees_id', 'left');
		$this -> db -> join('tbl_assign_fees_center_mapping as map', 'i.assign_fees_id  = map.assign_fees_id', 'left');
		if($fees_type != null){
			if($fees_type == "Class"){
				$this -> db -> join('tbl_assign_fees_course_mapping as gc', 'i.assign_fees_id  = gc.assign_fees_id', 'left');
			}else{
				$this -> db -> join('tbl_assign_fees_group_mapping as gc', 'i.assign_fees_id  = gc.assign_fees_id', 'left');
				$this -> db -> join('tbl_fees_batch_mapping as fbm', 'fbm.group_id  = gc.group_master_id AND fbm.fees_id = i.fees_id', 'LEFT');
			}
		}
		$this->db->where("($condition)");
		$this->db->order_by('f.fees_name', 'asc');
		$this->db->group_by('f.fees_id');
		$query = $this -> db -> get();
		// echo "<pre>";
		// print_r($query->result_array());exit();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

	function getChequeBounceFeesDetails($fees_id){
		$this -> db -> select('f.*,fc.*');
		$this -> db -> from('tbl_fees_component_data as fc');
		$this -> db -> join('tbl_fees_master as f', 'f.fees_id  = fc.fees_master_id', 'left');
		$this->db->where("fc.fees_master_id",$fees_id);
		$this->db->order_by('f.fees_name', 'asc');
		$this->db->group_by('f.fees_id');
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
}
?>
