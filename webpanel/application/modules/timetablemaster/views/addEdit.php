<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Timetable Master</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>timetablemaster">Timetable Master</a></li>
                    </ul>
                  </div>
                </div>
                <div id='loadingmessage' style='display:none'>
                    <img src='<?php echo FRONT_URL; ?>/images/loading.gif?>'/>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="timetable_id" name="timetable_id" value="<?php if(!empty($details[0]->timetable_id)){echo $details[0]->timetable_id;}?>" />
								
								<div class="control-group form-group">
									<label class="control-label" for="category_id">Category*</label>
									<div class="controls">
										<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);">
											<option value="">Select Category</option>
											<?php 
												if(isset($categories) && !empty($categories)){
													foreach($categories as $cdrow){
														$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
											?>
												<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
											<?php }}?>
										</select>
									</div>
								</div>
							
								<div class="control-group form-group">
									<label class="control-label"><span>Course*</span></label> 
									<div class="controls">
										<select id="course_id" name="course_id" class="form-control" onchange="getThemes(this.value);">
											<option value="">Select Course</option>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Theme*</span></label> 
									<div class="controls">
										<!--<select id="theme_id" name="theme_id" class="form-control" onchange="getWeeks(this.value);" >-->
										<select id="theme_id" name="theme_id" class="form-control" >
											<option value="">Select Theme</option>
										</select>
									</div>
								</div>
								
								<!--<div class="control-group form-group">
									<label class="control-label"><span>Week*</span></label> 
									<div class="controls">
										<select id="week_id" name="week_id" class="form-control" >
											<option value="">Select Week</option>
										</select>
									</div>
								</div>-->
								
								<div class="control-group form-group">
									<label class="control-label"><span>Timetable Title*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Title" id="timetable_title" name="timetable_title" value="<?php if(!empty($details[0]->timetable_title)){echo $details[0]->timetable_title;}?>" >
									</div>
								</div>
								
								<!--<div class="control-group form-group">
									<label class="control-label"><span>Name*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Name" id="timetable_name" name="timetable_name" value="<?php if(!empty($details[0]->timetable_name)){echo $details[0]->timetable_name;}?>" >
									</div>
								</div>-->
								
								<div class="control-group form-group">
									<label class="control-label" for="cover_image">Cover Image</label>
									<div class="controls">
										
										<input class="input-xlarge" id="input_cover_image" value="<?php if(!empty($details[0]->cover_image)){echo $details[0]->cover_image;}?>" name="input_cover_image" type="hidden" >
										
										<input class="input-xlarge" id="cover_image" name="cover_image" type="file">
										Only (gif, jpg, png, jpeg) and Max Size 2 MB allowed.<br/>
										
										<?php if(!empty($details[0]->cover_image) && file_exists(DOC_ROOT_FRONT."/images/timetable_images/".$details[0]->cover_image))
										{
										?>
											<img style="width: 150px;height:150px;padding-top:5px;" src="<?php echo FRONT_URL; ?>/images/timetable_images/<?php echo $details[0]->cover_image; ?>">
											</img>
											<a href="javascript:void(0);" onclick="deleteData('<?php echo $details[0]->timetable_id; ?>');" title="">Delete Image</a>
											
										<?php 
										}
										?>
									</div>
								</div>
								
								<a class="minia-icon-file-add btn btn-primary tip" style="float: none; margin-right: 0%; margin-top: 3%; margin-bottom: 2%;" onclick="addFiles();" title="" aria-describedby="ui-tooltip-1"> Add Video </a>
								<div class="table-responsive scroll-table" style="overflow-x:auto;width:100%;">
									<table class="display table table-bordered non-bootstrap">
										<thead>
											<tr>
												<th>Video Title</th>
												<th>Video URL</th>
												<th>Is Downloadable?</th>
												<th width="10%">Parent Show</th>
												<th>Action</th>
											</tr>
										</thead>
										
										<tbody id="tableBody">
											<?php
												if(!empty($videos)){
													$count = 1;
													for($i=0;$i<sizeof($videos);$i++){
												?>	
													<tr id="divTD_<?php echo $count;?>">
														<td>
															<input type="text" name="video_title[]" id="video_title_<?php echo $count;?>" value="<?php if(!empty($videos[$i]['video_title'])){ echo $videos[$i]['video_title']; }?>" class="required form-control" title="Please enter title." />
														</td>
														<td>
															<textarea name="video_url[]" id="video_url_<?php echo $count;?>" class="required form-control" title="Please enter video url." rows="7"  ><?php if(!empty($videos[$i]['video_url'])){ echo $videos[$i]['video_url']; }?></textarea>
															
															<iframe src="<?php echo $videos[$i]['video_url']; ?>" style="border:0;height:200;width:350px;max-width: 100%" allowFullScreen="true" allow="encrypted-media"></iframe>
															
														</td>
														
													<!-----------------new------------>
													<td>
															<select name="video_is_downloadable[]" id="video_is_downloadable_<?php echo $count;?>" class="required form-control" title="Please select option." >
															<option value="No" <?= ($videos[$i]['is_downloadable'] == 'No'?"selected":"")?>>No</option>
															<option value="Yes" <?= ($videos[$i]['is_downloadable'] == 'Yes'?"selected":"")?>>Yes</option>
															</select>
														</td>
														
														<td>
															<select name="video_parent_show[]" id="video_parent_show_<?php echo $count;?>" class="required form-control" title="Please select option.">
															
															<option value="No" <?= ($videos[$i]['parent_show'] == 'No'?"selected":"")?>>No</option>
															<option value="Yes" <?= ($videos[$i]['parent_show'] == 'Yes'?"selected":"")?>>Yes</option>
															
															</select>
														</td>
													<!-----------------new-end----------->
														<td><a class="minia-icon-file-remove btn tip" title="Remove" onclick="removeFiles('divTD_<?php echo $count;?>','<?php if(!empty($videos[$i]['timetable_video_id'])){ echo $videos[$i]['timetable_video_id']; }?>');">Remove</a>
														</td>
													</tr>
												<?php $count++; }}?>
										</tbody>
									</table>					
								</div>
								<div style="clear:both; margin-bottom: 2%;"></div>
								
								<a class="minia-icon-file-add btn btn-primary tip" style="float: none; margin-right: 0%; margin-top: 3%; margin-bottom: 2%;" onclick="addFiles1();" title="" aria-describedby="ui-tooltip-1"> Add Document </a>
								<div class="table-responsive scroll-table" style="overflow-x:auto;width:100%;">
									<table class="display table table-bordered non-bootstrap">
										<thead>
											<tr>
												<th>Document Title</th>
												<th>Type</th>
												<th>Description</th>
												<th>Document</th>
												<th>Is Downloadable?</th>
												<th width="10%">Parent Show</th>
												<th>Action</th>
											</tr>
										</thead>
										
										<tbody id="tableBody1">
											<?php
												if(!empty($documents)){
													$count1 = 1;
													for($i=0;$i<sizeof($documents);$i++){
												?>	
													<tr id="div1TD_<?php echo $count1;?>">
														<td>
															<input type="text" name="doc_title[<?php echo $count1;?>]" id="doc_title_<?php echo $count1;?>" value="<?php if(!empty($documents[$i]['doc_title'])){ echo $documents[$i]['doc_title']; }?>" class="required form-control" title="Please enter title." />
															<input type="hidden" name="timetable_document_id[<?php echo $count1;?>]" id="timetable_document_id_<?php echo $count1;?>" value="<?php if(!empty($documents[$i]['timetable_document_id'])){ echo $documents[$i]['timetable_document_id']; }?>" />
														</td>
														<td>
															<select name="doc_type[<?php echo $count1;?>]" id="doc_type_<?php echo $count1;?>" class="required form-control">
																<option value=""> Select Type</option>
																<option value="doc" <?php if(!empty($documents[$i]['doc_type']) && $documents[$i]['doc_type'] == 'doc'){ ?> selected <?php }?> > Document</option>
																<option value="image" <?php if(!empty($documents[$i]['doc_type']) && $documents[$i]['doc_type'] == 'image'){ ?> selected <?php }?>> Image</option>
															</select>
														</td>
														<td>
															<textarea name="doc_description[<?php echo $count1;?>]" id="doc_description_<?php echo $count1;?>" class="required form-control" title="Please enter description." rows="7"  ><?php if(!empty($documents[$i]['doc_description'])){ echo $documents[$i]['doc_description']; }?></textarea>
														</td>
														<td>
															<input type="file" name="doc_file[<?php echo $count1;?>]" id="doc_file_<?php echo $count1;?>" />
															<input type="hidden" name="doc_file_old[<?php echo $count1;?>]" id="doc_file_old_<?php echo $count1;?>" value="<?php echo $documents[$i]['doc_file']; ?>" />
															<?php if(!empty($documents[$i]['doc_type']) && $documents[$i]['doc_type'] == 'doc'){?>
																<a href="<?php echo FRONT_URL; ?>/images/timetable_images/<?php echo $documents[$i]['doc_file']; ?>" target="_blank" title="timetable_image" alt="timetable_image">View Document</a>
															<?php }else{?>
																<img src="<?php echo base_url();?>images/timetable_images/<?php echo $documents[$i]['doc_file']; ?>" title="timetable_image" alt="timetable_image" width="50" height="50" />
															<?php }?>
															
														</td>
													<!-----------------new------------>
													<td>
															<select name="is_downloadable[<?php echo $count1;?>]" id="is_downloadable_<?php echo $count1;?>" class="required form-control" title="Please select option." >
															<option value="No" <?= ($documents[$i]['is_downloadable'] == 'No'?"selected":"")?>>No</option>
															<option value="Yes" <?= ($documents[$i]['is_downloadable'] == 'Yes'?"selected":"")?>>Yes</option>
															</select>
														</td>
														
														<td>
															<select name="parent_show[<?php echo $count1;?>]" id="parent_show_<?php echo $count1;?>" class="required form-control" title="Please select option.">
															
															<option value="No" <?= ($documents[$i]['parent_show'] == 'No'?"selected":"")?>>No</option>
															<option value="Yes" <?= ($documents[$i]['parent_show'] == 'Yes'?"selected":"")?>>Yes</option>
															
															</select>
														</td>
													<!-----------------new-end----------->
														<td><a class="minia-icon-file-remove btn tip" title="Remove" onclick="removeFiles1('div1TD_<?php echo $count1;?>','<?php if(!empty($documents[$i]['timetable_document_id'])){ echo $documents[$i]['timetable_document_id']; }?>');">Remove</a>
														</td>
													</tr>
												<?php $count1++; }}?>
										</tbody>
									</table>					
								</div>
								<div style="clear:both; margin-bottom: 2%;"></div>
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>timetablemaster" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	<?php 
		if(!empty($details[0]->timetable_id)){
	?>
		getCourses('<?php echo $details[0]->category_id; ?>', '<?php echo $details[0]->course_id; ?>');
		getThemes('<?php echo $details[0]->course_id; ?>', '<?php echo $details[0]->theme_id; ?>');
		//getWeeks('<?php echo $details[0]->theme_id; ?>', '<?php echo $details[0]->week_id; ?>');
		<?php if(!empty($count)){?>
			count = '<?php echo $count;?>';
		<?php }else{?>
			count = '1';
		<?php }?>
		
		<?php if(!empty($count1)){?>
			count1 = '<?php echo $count1;?>';
		<?php }else{?>
			count1 = '1';
		<?php }?>
		
	<?php }else{?>
		//addFiles();
		//addFiles1();
	<?php }?>
});

function deleteData(id)
{
	
	var r=confirm("Are you sure to delete image?");
	if (r==true)
	{
		$.ajax({
			url: "<?php echo base_url().$this->router->fetch_module();?>/delCoverImage/",
			data:{"id":id},
			async: false,
			type: "POST",
			success: function(data2){
				data2 = $.trim(data2);
				if(data2 == "1")
				{
					displayMsg("success","Record has been deleted!");
					setTimeout("location.reload(true);",1000);
					
				}
				else
				{
					displayMsg("error","Oops something went wrong!");
					setTimeout("location.reload(true);",1000);
				}
			}
		});
	}
}

var count = 1;
function addFiles()
{
	var fileCount = count - 1;
	var text = '<tr id="divTD_'+count+'">'+
					'<td>'+
						'<input type="text" name="video_title[]" id="video_title_'+count+'" class="required form-control" title="Please enter title." />'+
					'</td>'+
					'<td>'+
						'<textarea name="video_url[]" id="video_url_'+count+'" class="required form-control" title="Please enter video url." ></textarea>'+
					'</td>'+
					'<td>'+
						'<select name="video_is_downloadable[]" id="video_is_downloadable_'+count+'" class="required form-control" title="Please select option.">'+
							'<option value="No"> No</option>'+
							'<option value="Yes"> Yes</option>'+
						'</select>'+
					'</td>'+
					'<td>'+
						'<select name="video_parent_show[]" id="video_parent_show_'+count+'" class="required form-control" title="Please select option.">'+
							'<option value="No"> No</option>'+
							'<option value="Yes"> Yes</option>'+
						'</select>'+
					'</td>'+
					'<td><a class="minia-icon-file-remove btn tip" title="Remove" onclick="removeFiles(\'divTD_'+count+'\',0);">Remove</a>'+
					'</td>'+
				'</tr>';	
	$("#tableBody").append(text);
	count++;
}

function removeFiles(divId,record_id)
{
    //alert(record_id);
	//return false;
	var r=confirm("Are you sure you want to delete this record?");
	if (r==true)
   	{
		if(record_id == '0'){
			//alert("in");
			$("#"+divId).remove();
		}else{
			//alert("else");
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/timetablemaster/delRecord/",
				data:{"id":record_id},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Deleted!");
						//setTimeout("location.reload(true);",1000);
						$("#"+divId).remove();
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						//setTimeout("location.reload(true);",1000);
					}
				}
			});
		}
	}
}


var count1 = 1;
function addFiles1()
{
	var fileCount = count1 - 1;
	var text = '<tr id="div1TD_'+count+'">'+
					'<td>'+
						'<input type="text" name="doc_title['+count1+']" id="doc_title_'+count1+'" value="" class="required form-control" title="Please enter title." />'+
						'<input type="hidden" name="timetable_document_id['+count1+']" id="timetable_document_id_'+count1+'" value="" />'+
					'</td>'+
					'<td>'+
						'<select name="doc_type['+count1+']" id="doc_type_'+count1+'" class="required form-control">'+
							'<option value=""> Select Type</option>'+
							'<option value="doc"> Document</option>'+
							'<option value="image"> Image</option>'+
						'</select>'+
					'</td>'+
					'<td>'+
						'<textarea name="doc_description['+count1+']" id="doc_description_'+count1+'" class="required form-control" title="Please enter description." ></textarea>'+
					'</td>'+
					'<td>'+
						'<input type="file" name="doc_file['+count1+']" id="doc_file_'+count1+'" />'+
					'</td>'+
					'<td>'+
						'<select name="is_downloadable['+count1+']" id="is_downloadable_'+count1+'" class="required form-control" title="Please select option.">'+
							'<option value="No"> No</option>'+
							'<option value="Yes"> Yes</option>'+
						'</select>'+
					'</td>'+
					'<td>'+
						'<select name="parent_show['+count1+']" id="parent_show_'+count1+'" class="required form-control" title="Please select option.">'+
							'<option value="No"> No</option>'+
							'<option value="Yes"> Yes</option>'+
						'</select>'+
					'</td>'+
					'<td><a class="minia-icon-file-remove btn tip" title="Remove" onclick="removeFiles1(\'div1TD_'+count+'\',0);">Remove</a>'+
					'</td>'+
				'</tr>';	
	$("#tableBody1").append(text);
	count1++;
}

function removeFiles1(divId,record_id)
{
    //alert(record_id);
	//return false;
	var r=confirm("Are you sure you want to delete this record?");
	if (r==true)
   	{
		if(record_id == '0'){
			//alert("in");
			$("#"+divId).remove();
		}else{
			//alert("else");
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/timetablemaster/delDocRecord/",
				data:{"id":record_id},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Deleted!");
						//setTimeout("location.reload(true);",1000);
						$("#"+divId).remove();
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						//setTimeout("location.reload(true);",1000);
					}
				}
			});
		}
	}
}


function getCourses(category_id,course_id = null)
{
	//alert("Val: "+val);return false;
	if(category_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>timetablemaster/getCourses",
			data:{category_id:category_id, course_id:course_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#course_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getThemes(course_id,theme_id = null)
{
	//alert("Val: "+val);return false;
	if(course_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>timetablemaster/getThemes",
			data:{course_id:course_id, theme_id:theme_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#theme_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#theme_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#theme_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}


function getWeeks(theme_id,week_id = null)
{
	//alert("Val: "+val);return false;
	if(theme_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>timetablemaster/getWeeks",
			data:{theme_id:theme_id, week_id:week_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#week_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#week_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#week_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

var vRules = {
	category_id:{required:true},
	course_id:{required:true},
	theme_id:{required:true},
	//week_id:{required:true},
	timetable_title:{required:true, alphanumericwithspace:true},
	timetable_name:{required:true, alphanumericwithspace:true}
	
};
var vMessages = {
	category_id:{required:"Please select category."},
	course_id:{required:"Please select course."},
	theme_id:{required:"Please select theme."},
	//week_id:{required:"Please select week."},
	timetable_title:{required:"Please enter timetable title."},
	timetable_name:{required:"Please enter timetable name."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		$('#loadingmessage').show();
		var act = "<?php echo base_url();?>timetablemaster/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					$('#loadingmessage').hide();
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>timetablemaster";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			},
			error:function(){
				$("#msg_display").html("");
				$("#display_msg").html("");
				//$("#submit_button").prop('disabled', false);
				displayMsg("error",res['msg']);
			}
		});
	}
});


document.title = "AddEdit - Timetable";

 
</script>					
