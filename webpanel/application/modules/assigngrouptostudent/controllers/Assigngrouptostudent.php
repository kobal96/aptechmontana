<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Assigngrouptostudent extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('assigngrouptostudentmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
            $result = array();
            $user_id = $_SESSION["webadmin"][0]->user_id;
			$getUserZones = $this->assigngrouptostudentmodel->getUserZones("user_id='".$user_id."' ");
// 			echo "<pre>";print_r($getUserZones);exit;
        	$result['zoneDetails'] = $this->assigngrouptostudentmodel->getDropdown("tbl_zones","zone_id,zone_name","status = 'Active' ");
			$result['zones'] = (!empty($getUserZones)) ? $getUserZones : '';
			$this->load->view('template/header.php');
			$this->load->view('assigngrouptostudent/index',$result);
			$this->load->view('template/footer.php');
		}
		else {
			redirect('login', 'refresh');
		}
    }

    function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		//echo $id;exit;
		$get_result = $this->assigngrouptostudentmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				
				array_push($temp, (!empty($get_result['query_result'][$i]->timestamp))?date("d-M-Y h:i:s A",strtotime($get_result['query_result'][$i]->timestamp)):" ");
				array_push($temp, date("d-m-Y",strtotime($get_result['query_result'][$i]->inquiry_date)));
				array_push($temp, $get_result['query_result'][$i]->academic_year_master_name);
				array_push($temp, $get_result['query_result'][$i]->enrollment_no);
				array_push($temp, $get_result['query_result'][$i]->student_name);
				array_push($temp, $get_result['query_result'][$i]->course_name);
				$group_names = implode("<br/>",explode(",",$get_result['query_result'][$i]->group_master_name));
				array_push($temp, $group_names);
				$batch_names = implode("<br/>",explode(",",$get_result['query_result'][$i]->batch_name));
				array_push($temp, $batch_names);
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);
				
				$actionCol1 = "";
				$actionCol1.= '<a href="assigngrouptostudent/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->student_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				array_push($temp, $actionCol1);
				array_push($items, $temp);
			}
		}	
		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
    }
    
    function getSearchLists(){
		if(!empty($_POST['value']) && !empty($_POST['column_name'])){
			$options = "";
			if($_POST['column_name']  == "zone_id"){
				$centerDetails = $this->assigngrouptostudentmodel->getDropdown("tbl_centers","center_id,center_name","status='Active' AND zone_id='".$_POST['value']."' ");
				$options = "<option value=''>Select Center</option>";
				if(!empty($centerDetails)){
					foreach($centerDetails as $key=>$val){
						$options .= "<option value='".$val->center_id."'>".$val->center_name."</option>"; 
					}
				}
				echo json_encode(array("status"=>true,"option"=>$options));
				exit;
			}else{
				$courseDetails = $this->assigngrouptostudentmodel->getCenterMappedCourses($_POST['value']);
				$options = " <option value=''>Select Class</option>";
				if(!empty($courseDetails)){
					foreach($courseDetails as $key=>$val){
						$options .= "<option value='".$val['course_id']."'>".$val['course_name']."</option>"; 
					}
				}
				echo json_encode(array("status"=>true,"option"=>$options));
				exit;
			}
		}
	}
	
	
    function addEdit($id = NULL){
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			$result = "";
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
				$student_condition = "1=1 AND tsg.student_id = $record_id AND batch_selection_type = 'Group' ";
    			$main_table = 	array("tbl_student_group as tsg", array("tsg.student_group_id,tsg.group_id,tsg.academic_year_id,tsg.zone_id,tsg.center_id"));
    			$join_tables = array(
    				array("left","tbl_student_master as tsm","tsm.student_id = tsg.student_id", array("concat(tsm.student_first_name, ' ',tsm.student_last_name) as student_name,tsm.student_id")),
    				array("left", "tbl_group_master as tgm", "tgm.group_master_id = tsg.group_id", array("tgm.group_master_name")),
    				array("left", "tbl_batch_master as tbm", "tbm.batch_id = tsg.batch_id", array("tbm.batch_name_master_id,tbm.batch_id")),
    				array("left", "tbl_batch_name_master as tbnm", "tbnm.batch_name_master_id = tbm.batch_name_master_id", array("tbnm.batch_name"))
				);
    			$rs = $this->assigngrouptostudentmodel->JoinFetch($main_table, $join_tables, $student_condition, array("tsm.student_id" => "ASC"));  // fetch query
    			$result['student'] = $this->assigngrouptostudentmodel->MySqlFetchRow($rs, "array"); // fetch result
		        $result['edit'] = '1';
				//  echo "<pre>";
    			// print_r($result['student']);
    // 			echo $this->db->last_query();
    			// exit;
			}else{
				$student_condition = "1=1 AND tsg.group_id IS null";
				$main_table = array("tbl_student_master as tsm", array("concat(tsm.student_first_name, ' ',tsm.student_last_name) as student_name,tsm.student_id"));
				$join_tables = array(
					array("left", "tbl_student_group as tsg", "tsg.student_id = tsm.student_id", array("tsg.student_group_id,tsg.group_id")));
				$rs = $this->assigngrouptostudentmodel->JoinFetch($main_table, $join_tables, $student_condition, array("tsm.student_id" => "ASC"));  // fetch query
				$result['student'] = $this->assigngrouptostudentmodel->MySqlFetchRow($rs, "array"); // fetch result
			
				$result['batch_details'] = $this->assigngrouptostudentmodel->getDropdown("tbl_batch_name_master","batch_name,batch_name_master_id");
				$result['group_details'] = $this->assigngrouptostudentmodel->getDropdown("tbl_group_master","group_master_name,group_master_id");
				$result['edit'] = '0';		    
			}
    
        	$result['academic_details'] = $this->assigngrouptostudentmodel->getDropdown("tbl_academic_year_master","academic_year_master_id,academic_year_master_name","status = 'Active'");
    		$result['zone_details'] = $this->assigngrouptostudentmodel->getDropdown("tbl_zones","zone_id,zone_name","status = 'Active' ");
			$this->load->view('template/header.php');
			$this->load->view('assigngrouptostudent/addEdit', $result);
			$this->load->view('template/footer.php');
		}else {
			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
    }
    
    
    function getStudentlist(){
        
            $student_condition = "1=1 AND tsg.group_id IS null AND tsm.center_id = ".$_POST['center_id']." ";
			$main_table = array("tbl_student_master as tsm", array("concat(tsm.student_first_name, ' ',tsm.student_last_name) as student_name,tsm.student_id"));
			$join_tables = array(
				array("left", "tbl_student_group as tsg", "tsg.student_id = tsm.student_id", array("tsg.student_group_id,tsg.group_id")));
			$rs = $this->assigngrouptostudentmodel->JoinFetch($main_table, $join_tables, $student_condition, array("tsm.student_id" => "ASC"));  // fetch query
			$student_list = $this->assigngrouptostudentmodel->MySqlFetchRow($rs, "array"); // fetch result
			
			if(!empty($student_list)){
				$student_options = "";
				foreach ($student_list as $key => $value) {
					$student_options .= "<option value='".$value['student_id']."'>".$value['student_name']."</option>";
				}
				echo json_encode(array("success"=>true,"option"=>$student_options));
				exit;
			}else{
				echo json_encode(array("success"=>false));
				exit;
			}
    }
    function getbacthlist(){
		$student_condition = "1=1 AND  tbm.group_id = ".$_POST['group_id']."  ";
		$main_table = array("tbl_batch_master as tbm", array("tbm.batch_id,tbm.group_id"));
		$join_tables = array(
			array("left", "tbl_batch_name_master as tbnm", "tbnm.batch_name_master_id = tbm.batch_name_master_id", array("tbnm.batch_name,tbnm.batch_name_master_id")));
		$rs = $this->assigngrouptostudentmodel->JoinFetch($main_table, $join_tables, $student_condition, array("tbnm.batch_name_master_id" => "ASC"),"tbnm.batch_name_master_id");  // fetch query
		$batch_list = $this->assigngrouptostudentmodel->MySqlFetchRow($rs, "array"); // fetch result
		
		if(!empty($batch_list)){
			$batch_options = "";
			foreach ($batch_list as $key => $value) {
				$batch_options .= "<option value='".$value['batch_id']."'>".$value['batch_name']."</option>";
			}
			echo json_encode(array("success"=>true,"option"=>$batch_options));
			exit;
		}else{
			echo json_encode(array("success"=>false));
			exit;
		}
    }
    
    function getstudentgroupnotin(){
        $groups_id = implode(",",$_POST['group_ids']);
        $group_list_Not_IN = $this->assigngrouptostudentmodel->getDropdown("tbl_group_master","group_master_name,group_master_id"," group_master_id NOT IN (".$groups_id.") AND status = 'Active' ");
        if(!empty($group_list_Not_IN)){
			$group_options = "";
			foreach ($group_list_Not_IN as $key => $value) {
				$group_options .= "<option value=".$value->group_master_id." >".$value->group_master_name."</option>";
			}
			echo json_encode(array("success"=>true,"option"=>$group_options));
			exit;
		}else{
		    $group_options = "";
			echo json_encode(array("success"=>false,"option"=>$group_options));
			exit;
		}
	}
	
	function getCenterGroup(){
		if(!empty($_POST['center_id'])){
			$group_list = $this->assigngrouptostudentmodel->getCenterGroup($_POST['center_id']);
			if(!empty($group_list)){
				$group_options = "";
				foreach ($group_list as $key => $val) {
					$group_options .= "<option value=".$val['group_id']." >".$val['group_name']."</option>";
				}
				echo json_encode(array("success"=>true,"option"=>$group_options));
				exit;
			}else{
				$group_options = "";
				echo json_encode(array("success"=>false,"option"=>$group_options));
				exit;
			}
		}
	}
    
    
    /*function getstudentbatchnotin(){
        $groups_id = implode(",",$_POST['group_ids']);
        $group_list_Not_IN = $this->assigngrouptostudentmodel->getDropdown("tbl_group_master","group_master_name,group_master_id"," group_master_id NOT IN (".$groups_id.") AND status = 'Active' ");
        if(!empty($group_list_Not_IN)){
			$group_options = "";
			foreach ($group_list_Not_IN as $key => $value) {
				$group_options .= "<option value=".$value->group_master_id." >".$value->group_master_name."</option>";
			}
			echo json_encode(array("success"=>true,"option"=>$group_options));
			exit;
		}else{
		    $group_options = "<option value=''>No Group Found</option>";
			echo json_encode(array("success"=>false,"option"=>$group_options));
			exit;
		}
        
    }*/
    
    
    function getzonelist(){
        // print_r($_POST);
        $center_list = $this->assigngrouptostudentmodel->getDropdown("tbl_centers","center_id,center_name","status = 'Active' AND zone_id = ".$_POST['zone_id']." ");
        if(!empty($center_list)){
			$center_options = "";
			$sel='';
			foreach ($center_list as $key => $value) {  
			    if(!empty($_POST['center_id']) && isset($_POST['center_id'])){
			        $sel = ($value->center_id == $_POST['center_id']) ? 'selected="selected"' : '';
			    }
				$center_options .= "<option value=".$value->center_id." ".$sel.">".$value->center_name."</option>";
			}
			echo json_encode(array("success"=>true,"option"=>$center_options));
			exit;
		}else{
			echo json_encode(array("success"=>false));
			exit;
		}
	}
	
    function getStudentDetails(){
       // $condition = " student_id='".$_REQUEST['student_id']."'  "; 
        $getDetails = $this->assigngrouptostudentmodel->getdata($_REQUEST['student_id']);
        print_r($getDetails);
    }
    
    function submitForm(){ 
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$data = array();
			$data['student_id'] = $_POST['student_id'];
			$data['group_id'] =   $_POST['group_id'];
			$data['batch_id'] =  $_POST['batch_id'];
			$data['academic_year_id'] = $_POST['academic_year_id'] ;
			$data['zone_id'] =   $_POST['zone_id'];
			$data['center_id'] =  !empty($_POST['center_id'])?$_POST['center_id']:$_POST['hidden_center_id'];
			$data['created_on'] = date("Y-m-d H:i:s");
			$data['created_by'] = $_SESSION["webadmin"][0]->user_id;
			$data['updated_on'] = date("Y-m-d H:i:s");
			$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
			$getInsertedData = $this->assigngrouptostudentmodel->insertData("tbl_student_group",$data,"1");
			if (!empty($getInsertedData)) {
				echo json_encode(array(	'success' => true,'msg' => 'Record Added Successfully.'));
				exit;
			}else{
			    	echo json_encode(array(	'success' => false,'msg' => 'error'));
				exit;
			}
	
		}
	
	}
    
    function deleteStudent(){
		$getDetails = $this->assigngrouptostudentmodel->getStdDatas("tbl_admission_fees","*","student_id =".$_POST['student_id']." AND group_id = ".$_POST['group_id']."  AND fees_type ='Group' ");
		$getGroupDetails = $this->assigngrouptostudentmodel->getStdDatas("tbl_student_group","*","student_id =".$_POST['student_id']." AND group_id = '".$_POST['group_id']."' ");
		if(!empty($getDetails) && count($getGroupDetails) < 2){
			echo json_encode(array("success" => "false","msg" => "You can't remove this record beacause the Admission fees assigned for this group."));
			exit;  
		}else{
			$getDeletedRecords = $this->assigngrouptostudentmodel->delete_records($_POST['student_id'],$_POST['group_id'],$_POST['batch_id']);
		}
         
        if($getDeletedRecords){
            echo json_encode(array("success" => "true","msg" => "Record Deleted Successfully"));
            exit;
        }
    }
    
}