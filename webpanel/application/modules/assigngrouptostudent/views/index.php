<!-- start: Content -->
<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1>Assign To Groups</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>assigngrouptostudent">Assign To Groups</a></li>
        </ul>
      </div>
    </div> 
    <div class="card">
    	<div class="page-title-border">
        	<div class="col-sm-12 col-md-12  text-right">
            	<?php 
					
				?>
					<p>
					<?php if ($this->privilegeduser->hasPrivilege("DocumentFolderAddEdit")) {?>	
						<a href="<?php  echo base_url();?>assigngrouptostudent/addEdit" class="btn btn-primary icon-btn"><i class="fa fa-plus"></i> Assign Groups</a>
					<?php }?>	
            <div class="clearfix"></div>
            </div>
        </div>     
		
        <div class="col-sm-12" style="clear: both">
         	<div class="box-content form-horizontal product-filter">            	
            		<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Zone" class="control-label">Zone</label>
							<select name="sSearch_0" id="sSearch_0" class="searchInput form-control">
								<option value="">Select Zone</option>
								<?php 
									if(!empty($zoneDetails)){
										foreach($zoneDetails as $key=>$val){
								?>
									<option value="<?php echo $val->zone_id; ?>"><?php echo $val->zone_name ?></option>
								<?php
										}
									}
								?>
							</select>
						</div>
					</div>
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Center" class="control-label">Center</label>
							<select name="sSearch_1" id="sSearch_1" class="searchInput form-control" >
								<option value="">Select Center</option>
							</select>
						</div>
					</div>
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="Class" class="control-label">Class</label>
							<select name="sSearch_2" id="sSearch_2" class="searchInput form-control">
								<option value="">Select Class</option>
							</select>
						</div>
					</div>
					<div class="col-sm-3 col-xs-12">
					 	<div class="dataTables_filter searchFilterClass form-group">
							<label for="firstname" class="control-label">ENR No./ Student Name </label>
							<input id="sSearch_3" name="sSearch_3" type="text" class="searchInput form-control" placeholder="ENR No./ Student Name "/>
						</div>
					</div>
			<!--	<div class="col-sm-4 col-xs-12" style="padding-left:0px;">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Student Name</label>
						<input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
					</div>
				</div>-->
				
				<div class="control-group clearFilter">
					<div class="controls">
						<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
					</div>
				</div>
				
            </div>
         </div>
		 
         <div class="clearfix"></div>
         <div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                    <table class="dynamicTable display table table-bordered non-bootstrap" >
                        <thead>
                          <tr>
                            <th>Date & Time</th>
                            <th>ENR Date</th>
							<th>AY</th>
							<th>ENR No.</th>
							<th>Student Name</th>
							<th>Class</th>
							<th data-bSortable="false">Group Name</th>
							<th data-bSortable="false">Batch Name</th>
							<th>Zone</th>
							<th>Center</th>
							<th data-bSortable="false">Action</th>
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
    </div>
</div><!-- end: Content -->
			
<script>
	$( document ).ready(function() {

		clearSearchFilters();
	});	
	
	function deleteData1(id,status)
	{
		var sta="";
		var sta1=" ";
		
		if(status=='Active')
		{
		
			sta="In-active";
			
		}
		else{
			
			sta="Active";
		}
		
		
		
    	var r=confirm("Are you sure to " +sta);
    	if (r==true)
   		{
    		//window.location.href="users/delete?id="+id;
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/delrecord12/",
				data:{"id":id,"status":sta},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Updated!");
						setTimeout("location.reload(true);",1000);
						
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});
    	}
    }
    
    $(document).on("change",".searchInput",function(){
		var element = $(this).attr("id");
		var functionName = "";
		var column_name = "";
		var id =  "";
		if(element == "sSearch_0" || element == "sSearch_1"){
			if(element == "sSearch_0"){
				column_name = "zone_id";
				id =  $("#sSearch_0").val();
				$("#sSearch_1").val("");
				// $("#sSearch_2").html("<option value=''>Select Class</option>");
			}else{
				column_name = "center_id";
				id =  $("#sSearch_1").val();
			}
			if(id != ""){
				$.ajax({
					url: "<?php echo base_url().$this->router->fetch_module();?>/getSearchLists",
					data:{"column_name":column_name,"value":id},
					async: false,
					type: "POST",
					dataType:"JSON",
					success: function(response){
						if(response.status){
							if(element == "sSearch_0"){
								$("#sSearch_1").html(response.option);
							}else{
								$("#sSearch_2").html(response.option);
							}
						}
					}
				});
			}else{
				if(element == "sSearch_0"){
					$("#sSearch_1").html("<option value=''>Select Center</option>");
					// $("#sSearch_2").html("<option value=''>Select Class</option>");
				}else{
					// $("#sSearch_2").html("<option value=''>Select Class</option>");
				}
			}
		}
	})
	
	document.title = "Document Folders";
</script>