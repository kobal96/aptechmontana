<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Foldercourses extends CI_Controller

{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('foldercoursesmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {

				if (!empty($_GET['text']) && isset($_GET['text'])) {
					$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
					parse_str($varr, $url_prams);
					$record_id = $url_prams['id'];
				}
				$result = "";
				// $result['categories'] = $this->foldercoursesmodel->getDropdown("tbl_categories","category_id,categoy_name");
				$result['foldername'] = $this->foldercoursesmodel->getdata("tbl_document_folders","document_folder_id= ".$record_id." ");
				
				//$result['details'] = $this->foldercoursesmodel->getFormdata($record_id);
	
		
			$this->load->view('template/header.php');
			$this->load->view('foldercourses/index',$result);
			$this->load->view('template/footer.php');
		}
		else {
			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			$record_id = $_GET['id'];

			// print_r($_GET);

			// if (!empty($_GET['text']) && isset($_GET['text'])) {
			// 	$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
			// 	parse_str($varr, $url_prams);
			// 	$record_id = $url_prams['id'];
			// }
			$result = "";

			$result['foldername'] = $this->foldercoursesmodel->getdata("tbl_document_folders","document_folder_id= ".$record_id." ");
			$result['categories'] = $this->foldercoursesmodel->getDropdown("tbl_categories","category_id,categoy_name");
			//$result['details'] = $this->foldercoursesmodel->getFormdata($record_id);
				// echo "<pre>";
			// print_r($result); exit;
			$this->load->view('template/header.php');
			$this->load->view('foldercourses/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		$_GET['document_folder_id'] = $id;
		//echo $id;exit;
		$get_result = $this->foldercoursesmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				// array_push($temp, $get_result['query_result'][$i]->folder_name);
				array_push($temp, $get_result['query_result'][$i]->categoy_name);
				array_push($temp, $get_result['query_result'][$i]->course_name);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("DocumentFolderCategoryCoursesDelete")){
					// $actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData(\'' . $get_result['query_result'][$i]->folder_course_id. '\');" title="edit"><i class="fa fa-edit btn btn-primary"></i></a>';

					$actionCol21 .= '<a href="javascript:void(0);" onclick="change_status(\'' . $get_result['query_result'][$i]->folder_course_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}
				
				array_push($temp, $actionCol21);
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	public function getCourses(){
		$result = $this->foldercoursesmodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		$option = '';
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$option .= '<option value="'.$result[$i]->course_id.'" >'.$result[$i]->course_name.'</option>';
			}
		}
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	
	function submitForm()
	{ 
		// echo "<pre>";print_r($_SESSION);exit;
		
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			// $user_name = $this->foldercoursesmodel->getdata("tbl_admin_users","user_id= ".$record_id." ");
			// echo "<pre>";print_r($_POST);exit;
			if(!empty($_POST['course_id'])){
				// $this->foldercoursesmodel->delrecord_condition("tbl_folder_courses", "category_id='".$_POST['category_id']."' && document_folder_id='".$_POST['document_folder_id']."' ");
				for($i=0; $i < sizeof($_POST['course_id']); $i++){
					$check_exist = $this->foldercoursesmodel->getdata("tbl_folder_courses", "category_id='".$_POST['category_id']."' && document_folder_id='".$_POST['document_folder_id']."' && course_id='".$_POST['course_id'][$i]."' ");
					
					

					if(empty($check_exist)){
						$data_array = array();
						$data_array['document_folder_id'] = (!empty($_POST['document_folder_id'])) ? $_POST['document_folder_id'] : '';
						$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
						$data_array['course_id'] = $_POST['course_id'][$i];
						$data_array['status'] = 'Active';
						$data_array['created_on'] = date('Y-m-d H:i:s');
						$data_array['created_by'] =$_SESSION["webadmin"][0]->user_id;
						$data_array['updated_on'] =date('Y-m-d H:i:s');
						$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
						$result = $this->foldercoursesmodel->insertData('tbl_folder_courses', $data_array, '1');
						$course_id_added[] = $_POST['course_id'][$i];
					}
				}
				// print_r($course_id_added);exit;
				if($result){
					// $course_id = implode(',' ,$course_id_added);
					foreach ($course_id_added as $key => $value_course_id) {
						$category_id = $_POST['category_id'];
						$document_id = $_POST['document_folder_id'];
						$getteachers = $this->foldercoursesmodel->getFcm($category_id,$value_course_id[$key],$document_id);

						$folder_count = $this->db->where(['document_folder_id'=>$_POST['document_folder_id']])->from("tbl_folder_document")->count_all_results();

						$folder_name =$this->db->select('folder_name')->where(['document_folder_id'=>$_POST['document_folder_id']])->from("tbl_document_folders")->get()->row()->folder_name;

						$course_name =$this->db->select('course_name')->where(['course_id'=>$value_course_id[$key]])->from("tbl_courses")->get()->row()->course_name;

						if(!empty($getteachers)){
							for($s=0; $s < sizeof($getteachers); $s++){
								
								$title = 'Assign Document';
								$notification = '';
								$email_content=  '';
								$fcm_token = '';
								$subject='';
								$from_email='';
								$to_email='';
								$cc_email = '';
								$notification_type = array();
							
								$notification = $folder_count ." Documents added for <b>".$course_name."</b> in <u>".$folder_name."<u>";
								// $notification = $user_name[0]['first_name']." added <u>".."</u> for {Course Name};
	
								// {UserName} added {Title with underline} for {Course Name}
								$notification_type = array('type'=>'service_request');
								
								$notification_data = array();
								$notification_data['teacher_id'] = $getteachers[$s]['user_id'];
								$notification_data['notification_type'] = 'Document';
								$notification_data['notification_title'] = $title;
								$notification_data['notification_contents'] = $notification;
								
								$notification_data['created_on'] = date("Y-m-d H:i:s");
								$notification_data['created_by'] = (!empty($_SESSION["webadmin"][0]->user_id)) ? $_SESSION["webadmin"][0]->user_id : '' ;
								$notification_data['updated_on'] = date("Y-m-d H:i:s");
								$notification_data['updated_by'] = (!empty($_SESSION["webadmin"][0]->user_id)) ? $_SESSION["webadmin"][0]->user_id : '' ;
								$this->foldercoursesmodel->insertData('tbl_teacher_notifications', $notification_data, 1);
								
								if(!empty($getteachers[$s]['fcm_token'])){
									//echo "h1";
									$fcm_token = $getteachers[$s]['fcm_token'];
									if(!empty($fcm_token)){
										sendNotification($title,$notification,$email_content,$fcm_token,$subject,$from_email,$to_email,$cc_email,$notification_type);
									}	
								}
							}
						}
					}
					
				}
			}else{
				echo json_encode(array("success"=>"0",'msg'=>'Kindly select atleast one record!'));
				exit;
			}
			
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	

	function changestatus()
	{
		// echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->foldercoursesmodel->delrecord12("tbl_folder_courses","folder_course_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}

	private function set_upload_options()
	{   
		//upload an image options //products
		$config = array();
		//$config['file_name']     = md5(uniqid("100_ID", true));
		$config['upload_path'] = DOC_ROOT_FRONT."/images/teacher_album_images";
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;

		return $config;
	}
	
	function delRecord()
	{
		$id=$_POST['id'];
		$appdResult = $this->foldercoursesmodel->delrecord1("tbl_folder_courses","folder_course_id",$id);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
