<?php 
//error_reporting(0);
$record_id = $_GET['id'];
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
		<div>
		<h1 style="font-size:20px"><?= $foldername[0]['folder_name']?> >> course Mapping </h1>            
		</div>
		<div>
		<ul class="breadcrumb">
			<li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
			<li><a href="<?php echo base_url();?>documentfoldermaster"><?= $foldername[0]['folder_name']?>	</a></li>
		</ul>
		</div>
	</div>
	<div class="card">       
		<div class="card-body">             
		<div class="box-content">
			<div class="col-sm-8 col-md-12">
				<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
					<input type="hidden" id="document_folder_id" name="document_folder_id" value="<?php echo $record_id;?>" />
					<div id='loadingmessage' style='display:none'>
						<img src='<?php echo base_url("images/loading.gif"); ?>'/>
					</div>
					<div class="control-group form-group">
						<label class="control-label" for="category_id">Category*</label>
						<div class="controls">
							<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);">
								<option value="">Select Category</option>
								<?php 
									if(isset($categories) && !empty($categories)){
										foreach($categories as $cdrow){
								?>
									<option value="<?php echo $cdrow->category_id;?>"><?php echo $cdrow->categoy_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label" for="course_id">Courses*</label>
						<input type="checkbox" id="checkbox" >Select All
						<div class="controls">
							<select id="course_id" name="course_id[]" class="form-control select2" multiple>
								<option value="">Select Courses</option>
							</select>
						</div>
					</div>
					
					<div class="form-actions form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="javascript:history.go(-1)" class="btn btn-primary">Cancel</a>
					</div>
				</form>
			</div>
		<div class="clearfix"></div>
		</div>
		</div>
	</div>  

	<div class="card">
    	<!-- <div class="page-title-border">
        	<div class="col-sm-12 col-md-12 left-button-top">
					<p>
					<?php if ($this->privilegeduser->hasPrivilege("DocumentFolderCategoryCoursesAdd")) {?>	
						<a href="<?php  echo base_url();?>foldercourses/addEdit?id=<?php echo $record_id; ?>" class="btn btn-primary icon-btn"><i class="fa fa-plus"></i> ADD COURSES</a>
					<?php }?></p>	
            <div class="clearfix"></div>
            </div>
        </div>      -->
		
        <div class="col-sm-12" style="clear: both">
         	<div class="box-content form-horizontal product-filter">            	
            	
				<!-- <div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Folder Name</label>
						<input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
					</div>
				</div> -->
				
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Category</label>
						<input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Course</label>
						<input id="sSearch_1" name="sSearch_1" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<div class="control-group clearFilter">
					<div class="controls">
						<a href="#" onclick="clearSearchFilters();"><button class="btn btn-primary" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
					</div>
				</div>
				
				
            </div>
         </div>
		 
		 
         <div class="clearfix"></div>
         <div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                    <table class="dynamicTable display table table-bordered non-bootstrap" callfunction="<?php echo base_url();?>foldercourses/fetch/<?php echo $record_id;?>">
                        <thead>
                          <tr>
							<!-- <th>Folder Name</th> -->
							<th>Category</th>
							<th>Course</th>
							<th data-bSortable="false">Action</th>
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
	    
		<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
			   <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
			   <div class="modal-content">
				  <div class="modal-body">
					 <img src="" alt="" />
				  </div>
			   </div>
		    </div>
		</div>
	    
	    
    </div>      
</div><!-- end: Content -->		



<script>
$( document ).ready(function() {
	
	$("#checkbox").click(function(){
		if($("#checkbox").is(':checked') ){
			$("#course_id > option").prop("selected","selected");
			$("#course_id").trigger("change");
		}else{
			$("#course_id > option").removeAttr("selected");
			$("#course_id").trigger("change");
		}
	});
	
	<?php 
		if(!empty($details[0]->center_id)){
	?>
		//getCourses('<?php echo $details[0]->category_id; ?>');
	<?php }?>
	
	
	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						//['Source', 'Templates'],
						['Cut','Copy','Paste','PasteText','-','Print', 'SpellChecker', 'Scayt'],
						['Subscript','Superscript'],
						['NumberedList','BulletedList'],
						['BidiLtr', 'BidiRtl' ],
						//['Maximize', 'ShowBlocks'],
						['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						//['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						//['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
	
	$.validator.addMethod("needsSelection", function(value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });
	
});

function getCourses(category_id)
{
	//alert("center_id: "+center_id);return false;
	if(category_id != "" )
	{
		//alert();
		$.ajax({
			url:"<?php echo base_url();?>foldercourses/getCourses",
			data:{category_id:category_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#course_id").html(res['option']);
						$("#course_id").select2();
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
						$("#course_id").select2();
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
					$("#course_id").select2();
				}
			}
		});
	}
}

var vRules = {
	category_id:{required:true},
	course_id:{required:true}
	
};
var vMessages = {
	category_id:{required:"Please select zone."},
	course_id:{required:"Please enter center name."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>foldercourses/submitForm";
		$('#loadingmessage').show();
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					$('#loadingmessage').hide();
					displayMsg("success",res['msg']);
					setTimeout(function(){
						//window.location = "<?php echo base_url();?>centermaster";
						window.location.reload();
						// history.go(-1);
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					$('#loadingmessage').hide();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Folder Courses";

function change_status(id,status)
	{
		var sta="";
		var sta1=" ";
		
		if(status=='Active')
		{
		
			sta="In-active";
			
		}
		else{
			
			sta="Active";
		}
		
		
		
    	var r=confirm("Are you sure to " +sta);
    	if (r==true)
   		{
    		//window.location.href="users/delete?id="+id;
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/changestatus",
				data:{"id":id,"status":sta},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					// console.log(data2);
					// exit;
					if(data2 == "1")
					{
						displayMsg("success","Record has been Updated!");
						setTimeout("location.reload(true);",1000);
						
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});

			
    	}
    }
</script>					
