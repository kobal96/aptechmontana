<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Assignfeestobatch extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('assignfeestobatchmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('assignfeestobatch/index');
			$this->load->view('template/footer.php');
		}else {
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = [];
			$result['zones'] = $this->assignfeestobatchmodel->getDropdown("tbl_zones","zone_id,zone_name");
			$result['details'] = $this->assignfeestobatchmodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('assignfeestobatch/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {
			redirect('login', 'refresh');
		}
	}
	
	public function getCourses(){
		$result = $this->assignfeestobatchmodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['course_id']) && !empty($_REQUEST['course_id'])){
			$course_id = $_REQUEST['course_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getCenters(){
		$result = $this->assignfeestobatchmodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		$option = '';
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = (!empty($_POST['selected_center']) && $_POST['selected_center'] == $result[$i]->center_id ) ? "selected" : "";
				$option .= "<option value='".$result[$i]->center_id."' ".$sel." >".$result[$i]->center_name."</option>";
			}
		}
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}	


	function submitForm(){ 
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {		
			if(!empty($_POST['fees_id'])){
				//check existing
				$existing_error = array();
				if(is_array($_POST['fees_id'])){
					foreach($_POST['fees_id'] as $key=>$val){
						$existing_condition = " zone_id = '".$_POST['zone_id']."' && center_id='".$_POST['center_id']."' && fees_id='".$val."' && group_id='".$_POST['group_id']."' && batch_id ='".$_POST['batch_id']."' ";
						$existingData  =  $this->assignfeestobatchmodel->getdata("tbl_fees_batch_mapping",$existing_condition);
						if(!empty($existingData)){
							$fees_name_details = $this->assignfeestobatchmodel->getdata("tbl_fees_master","fees_id='".$val."' ");
							if(!empty($fees_name_details)){
								$existing_error[] = "Fees ".$fees_name_details[0]['fees_name']." Already exist";
							}
						}
					}
				}else{
					$existing_condition = " zone_id = '".$_POST['zone_id']."' && center_id='".$_POST['center_id']."' && fees_id='".$_POST['fees_id']."' && group_id='".$_POST['group_id']."' && batch_id ='".$_POST['batch_id']."' ";
					if(!empty($_POST['fees_batch_mapping_id'])){
						$existing_condition .= " && fees_batch_mapping_id !='".$_POST['fees_batch_mapping_id']."' ";
					}
					$existingData  =  $this->assignfeestobatchmodel->getdata("tbl_fees_batch_mapping",$existing_condition);
					if(!empty($existingData)){
						$fees_name_details = $this->assignfeestobatchmodel->getdata("tbl_fees_master","fees_id='".$_POST['fees_id']."' ");
						if(!empty($fees_name_details)){
							$existing_error[] = "Fees ".$fees_name_details[0]['fees_name']."Already exist";
						}
					}
				}
				
				if(empty($existing_error)){
					$data_array = array();
					$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : 0;
					$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : 0;
					$data_array['group_id'] = (!empty($_POST['group_id'])) ? $_POST['group_id'] : 0;
					$data_array['updated_on'] = date("Y-m-d H:i:s");
					$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
					$data_array['batch_id'] = (!empty($_POST['batch_id'])) ? $_POST['batch_id'] : 0;
					if (!empty($_POST['fees_batch_mapping_id'])) {
						$fees_batch_mapping_id = $_POST['fees_batch_mapping_id'];
						$data_array['fees_id'] = (!empty($_POST['fees_id'])) ? $_POST['fees_id'] : 0;
						$result = $this->assignfeestobatchmodel->updateRecord('tbl_fees_batch_mapping', $data_array,'fees_batch_mapping_id',$fees_batch_mapping_id);
					}else {
						$data_array['created_on'] = date("Y-m-d H:i:s");
						$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
						foreach($_POST['fees_id'] as $key=>$val){
							$data_array['fees_id'] = $val;
							$result = $this->assignfeestobatchmodel->insertData('tbl_fees_batch_mapping', $data_array, '1');	
						}
					}
					if (!empty($result)) {
						echo json_encode(array(
							'success' => '1',
							'msg' => 'Record Added/Updated Successfully.'
						));
						exit;
					}else{
						echo json_encode(array(
							'success' => '0',
							'msg' => 'Problem in data update.'
						));
						exit;
					}
				}else{
					$errors = implode("<br>",$existing_error);
					echo json_encode(array(
						'success' => '0',
						'msg' => $errors
					));
					exit;
				}
			}else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Batch not selected.'
				));
				exit;
			}
		}else {
			return false;
		}
	}
	
	/*new code end*/
	function fetch($id=null){
		$get_result = $this->assignfeestobatchmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);
				array_push($temp, $get_result['query_result'][$i]->group_master_name);
				array_push($temp, $get_result['query_result'][$i]->batch_name);
				array_push($temp, $get_result['query_result'][$i]->fees_name);
				$actionCol1 = "";
				$actionCol1.= '<a href="assignfeestobatch/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->fees_batch_mapping_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;';
				$actionCol1.= '<a href="javascript:void(0);" title="Delete" onclick="deleteMapping('.$get_result['query_result'][$i]->fees_batch_mapping_id.')"><i class="fa fa-trash"></i></a> ';
				array_push($temp, $actionCol1);
				array_push($items, $temp);
			}
		}	
		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function delrecord(){
		$id=$_POST['id'];
		$consition = array("fees_batch_mapping_id"=>$id);
		$appdResult = $this->assignfeestobatchmodel->delrecord("tbl_fees_batch_mapping",$consition);
		if($appdResult){
			echo "1";
		}else{
			echo "2";
		}	
	}

	function getCenterGroup(){
		if(!empty($_POST['center_id']) && !empty($_POST['zone_id'])){
			$option = "";
			$condition = array("afcm.center_id"=>$_POST['center_id'], "gm.status"=>'Active');
			$groupDetails  = $this->assignfeestobatchmodel->getCenterGroup($condition);
			if(!empty($groupDetails)){
				foreach($groupDetails as $key=>$val){
					$selected = "";
					if(!empty($_POST['selected_value']) && $_POST['selected_value'] == $val['group_master_id']){
						$selected = "selected";
					}
					$option .= "<option value=".$val['group_master_id']." ".$selected.">".$val['group_master_name']."</option>"; 
				}
			}
			echo json_encode(array("status"=>"success","option"=>$option));
			exit;
		}
	}

	function getFeesandBatch(){
		if(!empty($_POST['center_id']) && !empty($_POST['group_id'])){
			$fees_option = "";
			$fees_condition = array("afcm.center_id"=>$_POST['center_id'],"afgm.group_master_id"=>$_POST['group_id']);
			$fees_details = $this->assignfeestobatchmodel->getGroupFees($fees_condition);
			if(!empty($fees_details)){
				foreach($fees_details as $key=>$val){
					$selected = "";
					if(!empty($_POST['fees_selected_value']) && $_POST['fees_selected_value'] == $val['fees_id']){
						$selected = "selected";
					}
					$fees_option .= "<option value= ".$val['fees_id']." ".$selected.">".$val['fees_name']."</option>"; 
				}
			}

			$batch_option = "";
			$batch_condition = array("bm.center_id"=>$_POST['center_id'],"bm.group_id"=>$_POST['group_id']);
			$batch_details = $this->assignfeestobatchmodel->getGroupBatch($batch_condition);
			if(!empty($batch_details)){
				foreach($batch_details as $key=>$val){
					$selected = "";
					if(!empty($_POST['batch_selected_value']) && $_POST['batch_selected_value'] == $val['batch_id']){
						$selected = "selected";
					}
					$batch_option .= "<option value=".$val['batch_id']." ".$selected." >".$val['batch_name']."</option>"; 
				}
			}
			echo json_encode(array("status"=>"success","fees_option"=>$fees_option,"batch_option"=>$batch_option));
			exit;
		}
	}
	
}

?>
