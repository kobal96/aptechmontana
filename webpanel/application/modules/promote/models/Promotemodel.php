<?PHP
class Promotemodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
	 	//print_r($this->db->last_query());
	    //exit;
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}

	function getRecords($get=null){

		if(!empty($get['sSearch_0']) && isset($get['sSearch_0'])){

			$table = "tbl_promoted_student";
			$table_id = 'i.current_academic_year_id';
			$default_sort_column = 'i.current_academic_year_id';
			$default_sort_order = 'desc';
			
			$condition = "";
			$condition .= "  1=1 AND  promoted_status = 'promoted' ";
			
			$colArray = array('i.promoted_academic_year_id','z.zone_id','ct.center_id','cat.category_id','cr.course_name','sm.student_first_name');
			$searchArray = array('i.promoted_academic_year_id','z.zone_id','ct.center_id','cat.category_id','cr.course_name','sm.student_first_name');
			
			$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
			$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
			
			// sort order by column
			$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
			$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

			if(isset($get['sSearch_0']) && $get['sSearch_0']!=''){
				$condition .= " AND i.promoted_academic_year_id = '".$get['sSearch_0']."' " ;
			}
			if(isset($get['sSearch_1']) && $get['sSearch_1']!=''){
				$condition .= " AND z.zone_id = '".$get['sSearch_1']."' " ;
			}
			if(isset($get['sSearch_2']) && $get['sSearch_2']!=''){
				$condition .= " AND ct.center_id ='".$get['sSearch_2']."' ";
			}
			if(isset($get['sSearch_3']) && $get['sSearch_3']!=''){
				$condition .= " AND cat.category_id ='".$get['sSearch_3']."' ";
			}
			if(isset($get['sSearch_4']) && $get['sSearch_4']!=''){
				$condition .= " AND cr.course_id ='".$get['sSearch_4']."' ";
			}
			if(isset($get['sSearch_5']) && $get['sSearch_5']!=''){
				$condition .= " AND  (sm.student_first_name like '%".$_GET['Searchkey_5']."%' || sm.student_last_name like '%".$_GET['Searchkey_5']."%' || sm.enrollment_no like '%".$_GET['Searchkey_5']."%') ";
			}
			
			// echo "condition ".$condition;exit;
			
			$this -> db -> select('i.*,sm.*,z.zone_name,ct.center_name,cr.course_name,cat.categoy_name,ay.academic_year_master_name,af.fees_id');
			$this -> db -> from('tbl_promoted_student as i');
			$this -> db -> join('tbl_student_master as sm', 'sm.student_id  = i.student_id', 'left');
			$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
			$this -> db -> join('tbl_centers as ct', 'i.center_id  = ct.center_id', 'left');
			$this -> db -> join('tbl_categories as cat', 'cat.category_id  = i.category_id', 'left');
			$this -> db -> join('tbl_courses as cr', 'i.promoted_course_id  = cr.course_id', 'left');
			// $this -> db -> join('tbl_academic_year_master as ay', 'i.promoted_academic_year_id  = ay.academic_year_master_id', 'left');
			$this -> db -> join('tbl_academic_year_master as ay', 'i.current_academic_year_id  = ay.academic_year_master_id', 'left');

			$this -> db -> join('tbl_admission_fees as af', 'af.academic_year_id  = i.promoted_academic_year_id AND i.student_id = af.student_id', 'left');
			
			
			// $this->db->where('e.inquiry_no != ""');

			$this->db->where("($condition)");
			$this->db->order_by($sort, $order);
			$this->db->group_by("i.student_id,i.promoted_academic_year_id");
			$this->db->limit($rows,$page);		
			$query = $this -> db -> get();
		
			$this -> db -> select('i.*,sm.*,z.zone_name,ct.center_name,cr.course_name,cat.categoy_name,ay.academic_year_master_name,af.fees_id');
			$this -> db -> from('tbl_promoted_student as i');
			$this -> db -> join('tbl_student_master as sm', 'sm.student_id  = i.student_id', 'left');
			$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
			$this -> db -> join('tbl_centers as ct', 'i.center_id  = ct.center_id', 'left');
			$this -> db -> join('tbl_categories as cat', 'cat.category_id  = i.category_id', 'left');
			// $this -> db -> join('tbl_courses as cr', 'i.course_id  = cr.course_id', 'left');
			$this -> db -> join('tbl_courses as cr', 'i.promoted_course_id  = cr.course_id', 'left');
			// $this -> db -> join('tbl_academic_year_master as ay', 'i.promoted_academic_year_id  = ay.academic_year_master_id', 'left');
			$this -> db -> join('tbl_academic_year_master as ay', 'i.current_academic_year_id   = ay.academic_year_master_id', 'left');

			$this -> db -> join('tbl_admission_fees as af', 'af.academic_year_id  = i.promoted_academic_year_id AND i.student_id = af.student_id', 'left');

			$this->db->where("($condition)");
			$this->db->order_by($sort, $order);	
			$this->db->group_by("i.student_id,i.promoted_academic_year_id");

			$query1 = $this -> db -> get();
			
			// echo $this->db->last_query();
			if($query -> num_rows() >= 1)
			{
				$totcount = $query1 -> num_rows();
				return array("query_result" => $query->result(),"totalRecords"=>$totcount);
			}
			else
			{
				//return false;
				return array("totalRecords"=>0);
			}
		}else{
			return array("query_result" =>'',"totalRecords"=>'0');
		}
		
		
		
		//exit;
	} 

	function getCenterFeesLevel($condition){
		$this -> db -> select('f.fees_level_name,f.fees_level_id');
		$this -> db -> from('tbl_assign_fees as i');
		$this -> db -> join('tbl_assign_fees_center_mapping as fcm','i.assign_fees_id  = fcm.assign_fees_id', 'left');
		$this -> db -> join('tbl_fees_level_master as f', 'i.fees_level_id  = f.fees_level_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->group_by("i.fees_level_id");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		
	}

	function getMonths($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition order by cast(month_name as unsigned)");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getFrequencyData($fees_id){
		$this -> db -> select('m.month_id, m.month_name,m.month,name');
		$this -> db -> from('tbl_childactivity_fees_month as m');
		$this -> db -> join('tbl_fees_frequency_mapping as ffm','m.month_id = ffm.month_id', 'left');
		$this->db->where("ffm.fees_id",$fees_id);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}
	}

	function getCenterFees($condition,$fees_type =  null){
		$this -> db -> select('f.fees_id,f.fees_name, f.installment_no as max_installment_allowed');
		$this -> db -> from('tbl_assign_fees as i');
		$this -> db -> join('tbl_fees_master as f', 'i.fees_id  = f.fees_id', 'left');
		$this -> db -> join('tbl_assign_fees_center_mapping as map', 'i.assign_fees_id  = map.assign_fees_id', 'left');
		if($fees_type != null){
			if($fees_type == "Class"){
				$this -> db -> join('tbl_assign_fees_course_mapping as gc', 'i.assign_fees_id  = gc.assign_fees_id', 'left');
			}else{
				$this -> db -> join('tbl_assign_fees_group_mapping as gc', 'i.assign_fees_id  = gc.assign_fees_id', 'left');
				$this -> db -> join('tbl_fees_batch_mapping as fbm', 'fbm.group_id  = gc.group_master_id AND fbm.fees_id = i.fees_id', 'LEFT');
			}
		}
		$this->db->where("($condition)");
		$this->db->order_by('f.fees_name', 'asc');
		$this->db->group_by('f.fees_id');
		$query = $this -> db -> get();
		// echo "<pre>";
		// print_r($query->result_array());exit();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

	function isPaymentDone($condition){
		$this -> db -> select('i.is_instalment,i.admission_fees_id');
		$this -> db -> from('tbl_fees_payment_details as f');
		$this -> db -> join('tbl_admission_fees as i', 'i.admission_fees_id  = f.admission_fees_id', 'left');
		$this->db->where("($condition)");
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		
	}

	function getFeesDetails($condition){
		$this -> db -> select('fc.*,f.fees_component_master_name,f.is_mandatory,fm.installment_no,fm.allow_frequency');
		$this -> db -> from('tbl_fees_component_data as fc');
		$this -> db -> join('tbl_fees_component_master as f', 'fc.fees_component_id  = f.fees_component_master_id', 'left');
		$this->db->where("($condition)");
		$this -> db -> join('tbl_fees_master as fm', 'fc.fees_master_id  = fm.fees_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('fc.fees_component_data_id','asc');
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

	function getCenterGroups($condition){
		$this -> db -> select('i.*,g.group_master_name');
		$this -> db -> from('tbl_center_user_groups as i');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		

		$this->db->where("($condition)");
		// $this->db->group_by("i.group_id");

		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		
	}

	function getAllGroups(){
		$this -> db -> select('g.group_master_id as group_id,g.group_master_name');
		$this -> db -> from('tbl_group_master as g');
		$this->db->where('g.status',"Active");

		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
	}

	//tobe uncommand after assign
	/* function getStudentAssignedGroups($student_id,$zone_id,$center_id){
		$condition = "g.status='Active' AND  sg.zone_id = '".$zone_id."' AND sg.center_id='".$center_id."' AND sg.student_id='".$student_id."' ";
		$this -> db -> select('g.group_master_id as group_id,g.group_master_name');
		$this -> db -> from('tbl_group_master as g');
		$this -> db -> join('tbl_student_group as sg', 'sg.group_id  = g.group_master_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->group_by("sg.group_id");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result();
		}else{
			$this -> db -> select('g.group_master_id as group_id,g.group_master_name');
			$this -> db -> from('tbl_group_master as g');
			$this->db->where('g.status',"Active");
			$query = $this -> db -> get();
			if($query -> num_rows() >= 1){
				return $query->result();
			}
		}
	} */

	function getStudentAssignedGroups($student_id,$zone_id,$center_id){
		$condition = "g.status='Active' AND  af.zone_id = '".$zone_id."' AND afc.center_id='".$center_id."' AND af.fees_selection_type = 'Group' ";
		$this -> db -> select('g.group_master_id as group_id,g.group_master_name');
		$this -> db -> from('tbl_assign_fees as af');
		$this -> db -> from('tbl_assign_fees_group_mapping as afg','afg.assign_fees_id  = af.assign_fees_id', 'left');
		$this -> db -> from('tbl_assign_fees_center_mapping as afc','afc.assign_fees_id  = af.assign_fees_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'g.group_master_id  = afg.group_master_id', 'left');
		$this->db->where("($condition)");
		$this->db->group_by("g.group_master_id");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	} 

	function getExportRecords($get){
		$table = "tbl_admission_fees";
		$table_id = 'i.admission_fees_id';
		$default_sort_column = 'i.admission_fees_id';
		$default_sort_order = 'desc';
		$condition = "";
		$condition .= "  1=1 ";
		
		//$colArray = array('feedback_name','feedback_email','feedback_contact','created_on');
		$colArray = array('s.student_first_name','s.student_last_name','s.enrollment_no','e.inquiry_no','z.zone_name','ct.center_name');
		$searchArray = array('s.student_first_name','s.student_last_name','s.enrollment_no','e.inquiry_no','z.zone_name','ct.center_name');
		
		// sort order by column
		$sort = $default_sort_column;  
		$order = $default_sort_order;
		
		for($i=0;$i<6;$i++){
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!=''){
				$condition .= " AND $colArray[$i] like '%".$get['sSearch_'.$i]."%'";
			}
		}
		
		$this -> db -> select('i.*, s.student_first_name,s.student_last_name,z.zone_name, s.enrollment_no,ct.center_name,e.inquiry_no');
		$this -> db -> from('tbl_admission_fees as i');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as ct', 'i.center_id  = ct.center_id', 'left');	
		$this -> db -> join('tbl_inquiry_master as e', 's.inquiry_master_id  = e.inquiry_master_id', 'left');
		
		$this->db->where("($condition)");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			$totcount = $query -> num_rows();
			return array("query_result" => $query->result(), "totalRecords" => $totcount);
		}else{
			return array("totalRecords" => 0);
		}
	}
	
	function delrecord12($tbl_name,$tbl_id,$record_id,$status)
	{
		$data = array('status' => $status);
		 
		$this->db->where($tbl_id, $record_id);
		$this->db->update($tbl_name,$data); 
		 
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// function getFormdata($ID){
		
	// 	//$condition = "1=1 && i.product_id= ".$ID." ";
	// 	$this -> db -> select('i.*');
	// 	$this -> db -> from('tbl_inquiry_master as i');
	// 	$this->db->where('i.inquiry_master_id', $ID);
	// 	//$this->db->where("($condition)");
	
	// 	$query = $this -> db -> get();
	   
	// 	//print_r($this->db->last_query());
	// 	//exit;
	   
	// 	if($query -> num_rows() >= 1)
	// 	{
	// 		return $query->result();
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
		
	// }

	function getFormdata($ID){
		
		//$condition = "1=1 && i.product_id= ".$ID." ";
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_promoted_student as i');
		$this->db->where('i.promoted_id', $ID);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}	

	function getFormdata1($ID){
		
		//$condition = "1=1 && i.product_id= ".$ID." ";
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_student_details as i');
		$this->db->where('i.student_id', $ID);
		//$this->db->where("($condition)");
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}	


	function getFormdata2($table,$col,$ID){
		
		//$condition = "1=1 && i.product_id= ".$ID." ";
		$this -> db -> select('*');
		$this -> db -> from($table);
		$this->db->where($col, $ID);
		//$this->db->where("($condition)");
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}	

	function getDropdown($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$rec_type = "Inquiry";
		$this -> db -> where("rec_type",$rec_type);
	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}

	function getDropdown1($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status = "Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}

	function getLastEnrollmentNo($tbl_name){
		$this -> db -> select('enrollment_no');
		$this -> db -> from($tbl_name);
		$this->db->order_by("enrollment_no", "desc");
		$this->db->limit(1);  
	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getFormdataInquiryNo($ID){
		//$condition = "1=1 && i.product_id= ".$ID." ";
		$this -> db -> select('i.inquiry_no');
		$this -> db -> from('tbl_inquiry_master as i');
		$this->db->where('i.inquiry_master_id', $ID);
		//$this->db->where("($condition)");
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}	

	function getOptions($tbl_name,$tbl_id,$comp_id){
		//echo "Cat ID: ".$category_id;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptions1($tbl_name,$tbl_id,$comp_id,$tbl_id1,$comp_id1){
		//echo "Cat ID: ".$category_id;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$this -> db -> where($comp_id1,$tbl_id1);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptions2($tbl_name,$tbl_id,$comp_id){
		//echo "Cat ID: ".$category_id;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptions3($tbl_name,$tbl_id,$comp_id){
	   
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where('fees_selection_type','Class');
		$this -> db -> where($comp_id,$tbl_id);
	
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}

	function getDetails($tbl_name,$tbl_id,$comp_id){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getStudentFeesInvoice($condition){
		
		$this -> db -> select('i.receipt_file,i.receipt_file_withoutgst,fm.fees_name');
		$this -> db -> from('tbl_fees_payment_receipt as i');
		$this -> db -> join('tbl_admission_fees as f', 'i.admission_fees_id  = f.admission_fees_id', 'left');
		$this -> db -> join('tbl_fees_master as fm', 'f.fees_id  = fm.fees_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

	function getdata($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
	}
	
	function getDataCondition($table,$condition=NULL){

		if(!empty($condition) && isset($condition)){
			$condition ='1=1';
		}else{
			$condition =$condition;
		}
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result();
        }else{
            return false;
        }
    }

    function getdata1($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
	function getgroup_id($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getdata2($condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		// $sql = $this->db->query("Select $fields from $table groupby group_id where $condition");
		$this -> db -> select('batch_id, batch_name, start_date, end_date');
		$this -> db -> from('tbl_batch_master');
		
		$this->db->where("($condition)");
		$this->db->group_by("batch_name_master_id");
		$sql = $this -> db -> get();
		
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getFeesData($condition){
		
		$this -> db -> select('i.*,c.course_name,g.group_master_name');
		$this -> db -> from('tbl_admission_fees as i');
		$this -> db -> join('tbl_courses as c', 'i.course_id  = c.course_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

	function getStudent($condition){
		
		$this -> db -> select('ps.*,sm.* ');
		$this -> db -> from('tbl_student_master as sm');
		$this -> db -> join('tbl_promoted_student as ps', 'ps.student_id  = sm.student_id', 'left');
		$this->db->where("($condition)");
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}


	function getStudents($condition,$promoted_year){
			
		$this -> db -> select('ps.*,sm.* ');
		$this -> db -> from('tbl_student_master as sm');
		$this -> db -> join('tbl_promoted_student as ps', 'ps.student_id  = sm.student_id', 'left');
		$this->db->where($condition);
		$this->db->where('ps.student_id NOT IN (select student_id from tbl_promoted_student where promoted_academic_year_id ='.$promoted_year.')');
		$query = $this -> db -> get();
// 		echo $this->db->last_query();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

    function getPayemtDetails($condition){
		$this -> db -> select('i.*, s.student_id, s.student_first_name, s.student_last_name,s.enrollment_no, s.father_name, s.present_address,s.address1, s.address2, s.address3,s.pincode, s.father_mobile_contact_no, s.father_email_id,s.parent_name,s.guardian,s.mother_name,s.password, ct.categoy_name, cr.course_name, b.batch_name, g.group_master_name, a.academic_year_master_name, cn.center_name, cn.center_address, cn.center_contact_no, cn.center_email_id, cn.center_code,cn.receipt_print_name,fp.*,country.country_name,state.state_name,city.city_name');
		$this -> db -> from('tbl_admission_fees as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_student_details as sd', 'i.student_id  = sd.student_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cr', 'i.course_id  = cr.course_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id = g.group_master_id', 'left');
		$this -> db -> join('tbl_centers as cn', 'i.center_id = cn.center_id', 'left');
		$this -> db -> join('tbl_fees_payment_details as fp', 'i.admission_fees_id = fp.admission_fees_id', 'left');
		$this -> db -> join('tbl_countries as country', 's.country_id = country.country_id', 'left');
		$this -> db -> join('tbl_states as state', 's.state_id = state.state_id', 'left');
		$this -> db -> join('tbl_cities as city', 's.city_id = city.city_id', 'left');

		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		// echo $this->db->last_query();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	function getPayemtDetails1($condition){
		
		$this -> db -> select('i.*, s.student_id, s.student_first_name, s.student_last_name,s.enrollment_no, s.father_name, s.present_address, s.father_mobile_contact_no, s.father_email_id,s.parent_name,s.guardian,s.mother_name,s.password, ct.categoy_name, cr.course_name, b.batch_name, g.group_master_name, a.academic_year_master_name, cn.center_name, cn.center_address, cn.center_contact_no, cn.center_email_id, cn.center_code,cn.receipt_print_name,fp.*');
		$this -> db -> from('tbl_admission_fees as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cr', 'i.course_id  = cr.course_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id = g.group_master_id', 'left');
		$this -> db -> join('tbl_centers as cn', 'i.center_id = cn.center_id', 'left');
		$this -> db -> join('tbl_fees_payment_details as fp', 'i.admission_fees_id = fp.admission_fees_id', 'inner');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		//echo $this->db->last_query();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	function getdata_orderby_limit1($table, $fields, $condition = '1=1', $order_by){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition $order_by limit 1");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getFeesComponents($condition){
		
		$this -> db -> select('i.*,f.fees_component_master_id, f.fees_type, f.fees_component_master_name');
		$this -> db -> from('tbl_fees_component_data as i');
		$this -> db -> join('tbl_fees_component_master as f', 'i.fees_component_id  = f.fees_component_master_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

	function getFeesComponentsforinvoice($admission_fees_id)
	{
		$this -> db -> select('ac.*,f.fees_component_master_id, f.fees_type, f.fees_component_master_name');
		$this -> db -> from('tbl_admission_fees_components as ac');
		$this -> db -> join('tbl_fees_component_master as f', 'ac.fees_component_id  = f.fees_component_master_id', 'inner');
		
		$this->db->where('ac.admission_fees_id',$admission_fees_id);
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	function getFeesComponentsforReceipt($condition){
		$this -> db -> select('afcch.component_collected_history_id,fcm.fees_component_master_name,fcm.receipt_name,af.fees_type, afcch.collected_amount,c.course_name,g.group_master_name');
		$this -> db -> from('tbl_admission_fees_components as afc');
		$this -> db -> join('tbl_admission_fees_components_collected_history as afcch', 'afcch.admission_fees_component_id  = afc.admission_fees_component_id', 'left');
		$this -> db -> join('tbl_fees_component_master as fcm', 'fcm.fees_component_master_id  = afc.fees_component_id ', 'left');
		$this -> db -> join('tbl_admission_fees as af', 'af.admission_fees_id  = afc.admission_fees_id ', 'right');
		$this -> db -> join('tbl_courses as c', 'c.course_id  = af.course_id ', 'left');
		$this -> db -> join('tbl_group_master as g', 'g.group_master_id  = af.group_id ', 'left');
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	function getFeescollectionfor_receipt($condition){
		$this -> db -> select('*');
		$this -> db -> from('tbl_admission_fees');
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	

	function getinstallmentforinvoice($admission_fees_id)
	{
		$this -> db -> select('*');
		$this -> db -> from('tbl_admission_fees_instalments');
		$this->db->where('admission_fees_id',$admission_fees_id);
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

    function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 

	function updateRecordbyConditionArray($table,$data,$condition){
		$this->db->where("($condition)");
		$this->db->update($table,$data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}else {
			return true;
		}
	} 

	function enum_select($table, $field){
		$query = " SHOW COLUMNS FROM `$table` LIKE '$field' ";
		$row = $this->db->query(" SHOW COLUMNS FROM `$table` LIKE '$field' ")->row()->Type;
		$regex = "/'(.*?)'/";
		preg_match_all( $regex , $row, $enum_array );
		$enum_fields = $enum_array[1];
		return( $enum_fields );
	}

	function getStudentCourseDetails($student_id){
		$this -> db -> select('c.course_name,s.admission_date,s.programme_start_date,s.programme_end_date');
		$this -> db -> from('tbl_student_details as s');
		$this -> db -> join('tbl_courses as c', 's.course_id  = c.course_id', 'left');
		
		$this->db->where("s.student_id",$student_id);
		$this->db->order_by("s.student_detail_id","DESC");
		$this->db->limit("1");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

	function getFeesLevelDetails($student_id){
		$this -> db -> select('fl.*');
		$this -> db -> from('tbl_fees_level_master as fl');
		$this -> db -> join('tbl_fees_level_center_mapping as flc', 'flc.fees_level_id  = fl.fees_level_id', 'left');
		$this -> db -> join('tbl_student_master as sm', 'sm.center_id  = flc.center_id', 'left');
		
		$this->db->where("sm.student_id",$student_id);
		$this->db->order_by("fl.fees_level_name","ASC");
		$query = $this -> db -> get();
		// echo $this->db->last_query();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

	function getGroupFeesComponents($admission_fees_id,$group_id,$is_mandatory = null){
		if($admission_fees_id != "" && $group_id != ""){
			$condition = array("af.admission_fees_id"=>$admission_fees_id,"af.group_id"=>$group_id);
			if($is_mandatory != null){
				$condition['fcm.is_mandatory'] = $is_mandatory;
			}
			$this -> db -> select('afc.*,fcm.is_mandatory');
			$this -> db -> from('tbl_admission_fees_components as afc');
			$this -> db -> join('tbl_admission_fees as af', 'af.admission_fees_id  = afc.admission_fees_id', 'left');
			$this -> db -> join('tbl_fees_component_master as fcm', 'fcm.fees_component_master_id  = afc.fees_component_id', 'left');
			$this->db->where($condition);
			$this->db->order_by("afc.fees_component_id","ASC");
			$query = $this -> db -> get();
			if($query -> num_rows() >= 1){
				return $query->result_array();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function getClassFeesComponents($admission_fees_id,$course_id,$is_mandatory = null){
		if($admission_fees_id != "" && $course_id != ""){
			$condition = array("af.admission_fees_id"=>$admission_fees_id,"af.course_id"=>$course_id);
			if($is_mandatory != null){
				$condition['fcm.is_mandatory'] = $is_mandatory;
			}
			$this -> db -> select('afc.*,fcm.is_mandatory');
			$this -> db -> from('tbl_admission_fees_components as afc');
			$this -> db -> join('tbl_admission_fees as af', 'af.admission_fees_id  = afc.admission_fees_id', 'left');
			$this -> db -> join('tbl_fees_component_master as fcm', 'fcm.fees_component_master_id  = afc.fees_component_id', 'left');
			$this->db->where($condition);
			$this->db->order_by("afc.fees_component_id","ASC");
			$query = $this -> db -> get();
			if($query -> num_rows() >= 1){
				return $query->result_array();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	function getAdmissionDetails($admission_fees_id){
		$this -> db -> select('sd.*,af.*');
		$this -> db -> from('tbl_student_details as sd');
		$this -> db -> join('tbl_admission_fees as af', 'af.student_id  = sd.student_id AND af.academic_year_id = sd.academic_year_id', 'left');
		$condition = array("af.admission_fees_id"=>$admission_fees_id);
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

	function getCenterMappedCourses($center_id){
		$this -> db -> select('tc.course_id,tc.course_name');
		$this -> db -> from('tbl_center_courses as cc');
		$this -> db -> join('tbl_courses as tc', 'tc.course_id  = cc.course_id', 'left');
		$condition = array("cc.center_id"=>$center_id);
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

	function getCenterUserGroupBatches($condition){
		$this -> db -> select('bm.batch_id,bm.batch_name,bm.start_date,bm.end_date');
		$this -> db -> from('tbl_batch_master bm');
		$this -> db -> join('tbl_fees_batch_mapping as fbm','fbm.batch_id = bm.batch_id ',"RIGHT");
		$this->db->where("($condition)");
		$this->db->group_by("bm.batch_name");
		$this->db->order_by("batch_name","ASC");
		$sql = $this -> db -> get();
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
}
?>
