<?PHP
class Gstmastermodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}
	 
	function getRecords($get=null){
		$table = "tbl_gst_master";
		$table_id = 'g.gst_master_id';
		$default_sort_column = 'g.gst_master_id';
		$default_sort_order = 'desc';
		
		$condition = "";
		$condition = "";
		$condition .= "  1=1 ";
		
		$colArray = array('g.tax','g.status');
		$searchArray = array('g.tax','g.status');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<1;$i++)
		{
			
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $searchArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		
		$this -> db -> select('g.*');
		$this -> db -> from('tbl_gst_master as g where '.$condition.'ORDER BY CAST(tax as SIGNED INTEGER)'.$order);

		// $this->db->where("($condition)");
		// $this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		$this -> db -> select('g.*');
		$this -> db -> from('tbl_gst_master as g where '.$condition.'ORDER BY CAST(tax as SIGNED INTEGER)'.$order);

		// $this->db->where("($condition)");
		// $this->db->order_by($sort, $order);		
		$query1 = $this -> db -> get();

		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			return array("totalRecords"=>0);
		}
		
		exit;
	}
	
	
	function getFormdata($ID){
		
		$this -> db -> select('g.*');
		$this -> db -> from('tbl_gst_master as g');
		$this->db->where('g.gst_master_id', $ID);
	
		$query = $this -> db -> get();
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}
	
	function getdata($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }
	
	function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 
	
	function delrecord12($tbl_name,$tbl_id,$record_id,$status)
	{
		$data = array('status' => $status);
		 
		$this->db->where($tbl_id, $record_id);
		$this->db->update($tbl_name,$data); 
		 
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
}
?>
