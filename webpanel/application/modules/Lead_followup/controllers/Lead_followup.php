<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Lead_followup extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('leadfollowupmodel','',TRUE);
		checklogin();
		//echo "here...";
	}

	function index(){
		//get search filters 
		$filterData = array();
		$filterData['zoneDetails'] = $this->leadfollowupmodel->getData("tbl_zones","zone_id,zone_name",array("status"=>'Active'));

		$filterData['status'] = $this->leadfollowupmodel->getdata("tbl_lead_status", "*","status = 'Active' ");
		// echo "<pre>";print_r($result['status']);exit;
		$this->load->view('template/header.php');
		$this->load->view('index',$filterData);
		$this->load->view('template/footer.php');
	}
	
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->leadfollowupmodel->getFormdata($record_id);
			//echo "<pre>";
			//print_r($result['details']);
			//echo $result['details'][0]->center_id;
			//exit;
			
			
			$this->load->view('template/header.php');
			$this->load->view('view',$result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}


	public function getCenters(){
		$result = $this->leadfollowupmodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
	
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->center_id == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			$result = "";
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
				$result['follow_logs'] = $this->leadfollowupmodel->getdata("tbl_lead_enquiry_log", "*","lead_id = $record_id and follow_up_date !='' order by created_on desc limit 1 ");
			}
			

			$result['zones'] = $this->leadfollowupmodel->getdata("tbl_zones","*","status = 'Active' ");
			$result['countries'] = $this->leadfollowupmodel->getdata("tbl_countries", "*","status = 'Active' ");
			$result['details'] = $this->leadfollowupmodel->getFormdata($record_id);

			

			$result['status'] = $this->leadfollowupmodel->getdata("tbl_lead_status", "*","status = 'Active' ");
			$result['planned_status'] = $this->leadfollowupmodel->getdata("tbl_default_status", "*","status = 'Active' ");
			// echo "<pre>";
			// print_r($result);
			// print_r($result['details']);
			// echo $result['details'][0]->center_id;
			// exit;
			// $result['details'] = $this->leadfollowupmodel->getFormdata($record_id);
			//echo "<pre>";
			//print_r($result['details']);
			//echo $result['details'][0]->center_id;
			//exit;
			
			
			$this->load->view('template/header.php');
			$this->load->view('lead/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function getStates(){
		$result = $this->leadfollowupmodel->getdata("tbl_states","*"," country_id = ".$_POST['country_id']." && status ='Active' ");
		$option = '';
			if(!empty($result)){
				foreach ($result as $key => $value) {
					$sel='';
					if(!empty($_POST['state_id'])){
						$sel = ($value['state_id'] == $_POST['state_id'])? 'selected="selected"' : '';
					}
					/* else{
						$sel = ($value['state_name'] == 'Maharashtra')? 'selected="selected"' : '';
					} */
					$option .= '<option value='.$value['state_id'].' '.$sel.' >'.$value['state_name'].'</option>';
				}
				echo json_encode(array("status"=>"success","option"=>$option));
				exit;
			}
	}

	function getCities(){
		$result = $this->leadfollowupmodel->getdata("tbl_cities","*"," state_id = ".$_POST['state_id']."  ");
		$option = '';
			if(!empty($result)){
				foreach ($result as $key => $value) {
					$sel='';
					$sel = ($value['city_id'] == $_POST['city_id'])? 'selected="selected"' : '';
					$option .= '<option value='.$value['city_id'].' '.$sel.' >'.$value['city_name'].'</option>';
				}
				echo json_encode(array("status"=>"success","option"=>$option));
				exit;
			}
	}
		
	function fetch(){
		//print_r($_GET);
		$get_result = $this->leadfollowupmodel->getRecords($_GET);
		// echo "<pre>";
		// print_r($get_result);
		// exit;


		$result = array();
		$result["sEcho"]= $_GET['sEcho'];
	
		$result["iTotalRecords"] = $get_result['totalRecords'];									//	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"]= $get_result['totalRecords'];							//  iTotalDisplayRecords for display the no of records in data table.

		$items = array();
		if(!empty($get_result['query_result'])){
			for($i=0;$i<sizeof($get_result['query_result']);$i++){

				$report = array();
				$temp = array();
				$lead = false ;
				$inquiry = false;
				$admission = false;
				$report['followUpCount'] = 0;
				$report['inqury_date'] = '';
			    $report['enrollment_date'] = '';
			    $report['enrollment_no']='';
			    $report['course_name'] = '';
			    $report['notes'] = '';
			    $report['nextFollowUpDate'] = '';
			    $report['nextFollowUpTime'] = '';
			    $report['nextFollowUpDate'] = '';
			    $report['nextFollowUpTime'] = '';
				$report['last_update_on']= date("d-m-Y",strtotime($get_result['query_result'][$i]->created_on));
				$report['last_updated_by']= ($get_result['query_result'][$i]->last_updated_by);

				$report['type'] =  $get_result['query_result'][$i]->rec_type;
				$report['lead_no'] =  $get_result['query_result'][$i]->inquiry_master_id;
				$report['lead_date'] = date("d-m-Y h:m:s",strtotime($get_result['query_result'][$i]->created_on));

				$report['student_name']= $get_result['query_result'][$i]->student_first_name." ".$get_result['query_result'][$i]->student_last_name;
				$report['father_name']= $get_result['query_result'][$i]->father_name;
				$report['mother_name']= $get_result['query_result'][$i]->mother_name;

				$report['zone_name']=  $get_result['query_result'][$i]->zone_name;
				$report['center_name']= $get_result['query_result'][$i]->center_name;
				$report['know_about_us']= $get_result['query_result'][$i]->know_about_us;

				if($get_result['query_result'][$i]->lead_status_id == '0'){
					$report['status_description']= 'YTC/No Response';
					$report['status_sub_description']= 'YTC';
					$report['default_status_description']= 'YTC';
					$report['lead_enquiry_status_description']= 'YTC';
				}else{
					$report['status_description']= $get_result['query_result'][$i]->status_description;
					$report['status_sub_description']= $get_result['query_result'][$i]->status_sub_description;
					$report['default_status_description']= $get_result['query_result'][$i]->default_status_description;
					$report['lead_enquiry_status_description']= $get_result['query_result'][$i]->lead_enquiry_status_description;
				}
				

					$report['lead_category']= $get_result['query_result'][$i]->lead_category;
			
				if($get_result['query_result'][$i]->no_further_follow_up =='1'){
					$report['nextFollowUpDate'] = 'No Further Follow up';
					$report['nextFollowUpTime'] = 'No Further Follow up';
				}

								
				// check whether the this lead id having followup or not code by shiv on 4-6-2021
				$condition = " lead_id = '".$get_result['query_result'][$i]->inquiry_master_id."' AND type = '1'  AND  follow_up_date is not NULL AND lead_status_id is not NULL ";
				$totalLeadCount = $this->leadfollowupmodel->getdata("tbl_lead_enquiry_log", "count(ID) as totalLeadCount",$condition);

				if($totalLeadCount[0]['totalLeadCount'] > 0){
					$report['followUpCount'] = $totalLeadCount[0]['totalLeadCount'] ;
					$lead = true;
					// get lead latest status n notes with respective to lead id'
					$condition = "lead_id = '".$get_result['query_result'][$i]->inquiry_master_id."' AND type = '1'  AND  follow_up_date is not NULL AND i.lead_status_id is not NULL ";
					$report['type'] =  'Lead';
					$getFollowUpData = $this->leadfollowupmodel->getFollowUpData($condition);
					// echo "<pre>";
					// print_r($getFollowUpData);
					// exit;
					if(!empty($getFollowUpData[0]) && isset($getFollowUpData) ){
						$report['nextFollowUpDate'] = date("d-m-Y",strtotime($getFollowUpData[0]['follow_up_date']));
						$report['nextFollowUpTime'] = $getFollowUpData[0]['follow_up_time'];
					    $report['status_description']= $getFollowUpData[0]['status_description'];
					    $report['status_sub_description']= $getFollowUpData[0]['status_sub_description'];
					    $report['default_status_description']= $getFollowUpData[0]['default_status_description'];
					    $report['lead_enquiry_status_description']= $getFollowUpData[0]['lead_enquiry_status_description'];
					    $report['notes']= $getFollowUpData[0]['notes'];
						$report['last_update_on']= date("Y-m-d",strtotime($getFollowUpData[0]['created_on']));
						$report['last_updated_by']= ($getFollowUpData[0]['last_updated_by']);
					}
					$inquiry = true;
				} // lead section end here 

				// check all condition for  inquiry number of count n last updated data  
				if($inquiry){
					$condition = "i.lead_id = '".$get_result['query_result'][$i]->inquiry_master_id."' AND rec_type = 'Inquiry' ";
					$getInquiryData = $this->leadfollowupmodel->getInquiryData($condition);

					$report['inqury_no']= $getInquiryData[0]['inquiry_no'];
					$report['inqury_date']= $getInquiryData[0]['inquiry_date'];
					// $report['last_update_on']= date("Y-m-d",strtotime($getInquiryData[0]['created_on']));
					// $report['last_updated_by']= ($getInquiryData[0]['last_updated_by']);
				
				// 	echo "<pre>";
				// 	print_r($getInquiryData);
				// 	echo $this->db->last_query();
				// exit;
					if($getInquiryData[0]){
							// check whether the this inquiry id having followup or not code by shiv on 7-6-2021
						$condition = " lead_id = '".$getInquiryData[0]['inquiry_master_id']."' AND type = '2'  AND  follow_up_date is not NULL AND lead_status_id is not NULL ";

						$totalInquiryCount = $this->leadfollowupmodel->getdata("tbl_lead_enquiry_log", "count(ID) as totalInquiryCount",$condition);	
							// echo "<pre>";
					// print_r($totalInquiryCount);
					// 	exit;

						if($totalInquiryCount[0]['totalInquiryCount'] > 0){
							
							$report['followUpCount'] = $totalInquiryCount[0]['totalInquiryCount']+ $report['followUpCount'] ;
							$report['type'] =  'Inquiry';
							// get lead latest status n notes with respective to lead id'
							$condition = "l.lead_id = '".$getInquiryData[0]['inquiry_master_id']."' AND l.type = '2'  And i.rec_type= 'Inquiry'  AND l. follow_up_date is not NULL AND l.lead_status_id is not NULL ";

							$getFollowUpData = $this->leadfollowupmodel->getInquryFollowUpData($condition);
							// echo "<pre>";
							// echo $this->db->last_Query();
							// print_r($getFollowUpData);
							// exit;
							if(!empty($getFollowUpData[0]) && isset($getFollowUpData) ){
								$report['nextFollowUpDate'] = date("d-m-Y",strtotime($getFollowUpData[0]['follow_up_date']));
								$report['nextFollowUpTime'] = $getFollowUpData[0]['follow_up_time'];
								$report['status_description']= $getFollowUpData[0]['status_description'];
								$report['status_sub_description']= $getFollowUpData[0]['status_sub_description'];
								$report['default_status_description']= $getFollowUpData[0]['default_status_description'];
								$report['lead_enquiry_status_description']= $getFollowUpData[0]['lead_enquiry_status_description'];
								$report['notes']= $getFollowUpData[0]['notes'];
								$report['course_name']= $getFollowUpData[0]['course_name'];
								$report['last_update_on']= date("Y-m-d",strtotime($getFollowUpData[0]['created_on']));
								$report['last_updated_by']= ($getFollowUpData[0]['last_updated_by']);
							}
							$admission = true;
						}

					}
				} // inquiry section end here 


				// Admission section start here  code by shiv on 6-7-2021
					if($admission){
						// check whether the this inquiry id having admission data or not code by shiv on 7-6-2021
						$condition = " inquiry_master_id = '".$getInquiryData[0]['inquiry_master_id']."' ";
						$getAdmissionData = $this->leadfollowupmodel->getAdmissionData($condition);

						if(!empty($getAdmissionData) && isset($getAdmissionData)){
							$admission = true;
							$report['enrollment_no']= 'ENR'.$getAdmissionData[0]['enrollment_no'];
							$report['enrollment_date']= date("d-m-Y  H:m:s",strtotime($getAdmissionData[0]['created_on']));
							$report['type']= 'Enrolled';
							$report['student_name']= $getAdmissionData[0]['student_first_name']." ".$getAdmissionData[0]['student_last_name'];
							$report['father_name']= $getAdmissionData[0]['father_name'];
							$report['mother_name']= $getAdmissionData[0]['mother_name'];
							$report['zone_name']= $getAdmissionData[0]['zone_name'];
							$report['center_name']= $getAdmissionData[0]['center_name'];
							$report['course_name']= $getAdmissionData[0]['course_name'];
							$report['last_update_on']= date("Y-m-d",strtotime($getAdmissionData[0]['created_on']));
							$report['last_updated_by']= ($getAdmissionData[0]['last_updated_by']);
							$report['followUpCount'] =  $report['followUpCount']+1;

						}	

						// exit;
					}
					// Admission section end here  code by shiv on 6-7-2021
					// echo "<pre>";
					// print_r($report);


				array_push($temp,$report['lead_date']);
				array_push($temp, "LD00".$report['lead_no']);
				array_push($temp, $report['inqury_date']);
				array_push($temp, (!empty($report['inqury_no'])?"ENQ".$report['inqury_no']:"" ));
				array_push($temp, $report['enrollment_date']);
				array_push($temp, $report['enrollment_no']);
				array_push($temp, $report['type']);
				array_push($temp, $report['student_name']);
				array_push($temp, $report['father_name']);
				array_push($temp, $report['mother_name']);
				array_push($temp, $report['course_name']);
				array_push($temp, $report['lead_category']);
				array_push($temp, $report['know_about_us']);
				array_push($temp, $report['zone_name']);
				array_push($temp, $report['center_name']);
				array_push($temp, $report['last_updated_by']);
				array_push($temp, $report['last_update_on']);
				
				array_push($temp, $report['status_description']);
				array_push($temp, $report['status_sub_description']);
				array_push($temp, $report['default_status_description']);
				array_push($temp, $report['lead_enquiry_status_description']);
				array_push($temp, $report['notes']);
				
				array_push($temp, $report['nextFollowUpDate']);
				array_push($temp, $report['nextFollowUpTime']);
			
				array_push($temp, $report['followUpCount']);
			
				array_push($items, $temp);
			}// main loop end here 
		}
		
		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function test_email(){
	    $to_email = "danish.akhtar@attoinfotech.com";			
        $replyto = "info@attoinfotech.website";		
        $subject = "Test Email";
        $headers = "From: info@attoinfotech.website\r\n";
        $headers .= "Reply-To: info@attoinfotech.website\r\n";		
        $headers .= "BCC: infodanish@gmail.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";							
        $message  = '<html><body>';		
        $message .= '<p> Testing email...</p>';
        $message .=  '</body></html>';
        
        $mailop = mail($to_email, $subject, $message, $headers);
        
        if($mailop){
        	echo "Email Send";exit;
        }else{
        	echo "Email Not Send";exit;
        }
	}
	
	function submitForm()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			//exit;
			$data_array=array();			
			$inquiry_master_id = $_POST['inquiry_master_id'];
			
			$data_array['inquiry_status'] = (!empty($_POST['inquiry_status'])) ? nl2br($_POST['inquiry_status']) : '';
			$data_array['inquiry_progress'] = (!empty($_POST['inquiry_progress'])) ? nl2br($_POST['inquiry_progress']) : '';
			
			$data_array['updated_on'] = date("Y-m-d H:i:s");
			
			$result = $this->leadfollowupmodel->updateRecord('tbl_inquiry_master', $data_array,'inquiry_master_id',$inquiry_master_id);
			
			
		/*	
			if(!empty($_POST['feedback_reply'])){
				$to_email = "danish.akhtar@attoinfotech.com";			
				$replyto = "info@attoinfotech.website";		
				$subject = "Montana Feedback Reply";
				$headers = "From: info@attoinfotech.website\r\n";
				//$headers .= "Reply-To: info@attoinfotech.website\r\n";		
				//$headers .= "BCC: infodanish@gmail.com\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";							
				$message  = '<html><body>';		
				$message .= '<p> '.$message_txt.'</p>';
				$message .=  '</body></html>';
		   
				$mailop = mail($to_email, $subject, $message, $headers);
			}	
        */
			
			//exit;
			
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	

	function submitLeaddata(){
		// echo "<pre>";
		// print_r($_POST);
		// exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if($_POST['rectype'] == 'Lead'){
						
				// if (empty($response_headers['inquiryno']) ) {
				// 	echo json_encode(array("status_code" => "400", "Metadata" => array("Message" => "Please provide /leadinquiry no." ), "Data" => NULL ));
				// 	exit;
				// }
				
				//check duplicate
				$condition = "father_emailid='".$_POST['father_emailid']."' &&  father_contact_no='".$_POST['father_contact_no']."' && student_first_name='".$_POST['student_first_name']."' ";
				
				if(!empty($_POST['hidden_lead_id'])){
					$condition .= " && inquiry_master_id <> '".$_POST['hidden_lead_id']."' ";
				}
				
				$getRecord = $this->leadfollowupmodel->getdata("tbl_inquiry_master", "inquiry_master_id", $condition);
				
				if(!empty($getRecord)){
					echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
					exit;
				}
				$data_follow = array();
				if(!empty($_POST['follow_up']) && isset($_POST['follow_up'])){
					$data_follow['follow_up_date'] =  date('Y-m-d', strtotime($_POST['follow_up']));
					$data_follow['follow_up_time'] =  $_POST['follow_up_time'];
				}
				$data_status = array();
				$data_status['lead_status_id'] = (!empty($_POST['lead_status'])) ? $_POST['lead_status'] : '';
				$data_status['lead_sub_status_id'] = (!empty($_POST['lead_sub_status'])) ? $_POST['lead_sub_status'] : '';
				$data_status['lead_planned_status_id'] = (!empty($_POST['planned_status'])) ? $_POST['planned_status'] : '' ;
				$data_status['lead_enquiry_status_id'] = (!empty($_POST['lead_enquiry_status'])) ? $_POST['lead_enquiry_status'] : '';
				$data_status['type'] = 1;
				
				$data = array();
				
				
				if(!empty($_POST['hidden_lead_id'])){
					$data['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '' ;
					$data['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '' ;
					$data['student_first_name'] = (!empty($_POST['student_first_name'])) ? $_POST['student_first_name'] : '' ;
					$data['student_last_name'] = (!empty($_POST['student_last_name'])) ? $_POST['student_last_name'] : '' ;
					$data['student_dob'] = (!empty($_POST['student_dob'])) ? date("Y-m-d", strtotime($_POST['student_dob'])) : '' ;
					$data['gender'] = (!empty($_POST['gender'])) ? $_POST['gender'] : '' ;
					$data['country_id'] = (!empty($_POST['country_id'])) ? $_POST['country_id'] : '' ;
					$data['state_id'] = (!empty($_POST['state_id'])) ? $_POST['state_id'] : '' ;
					$data['city_id'] = (!empty($_POST['city_id'])) ? $_POST['city_id'] : '' ;
					$data['pincode'] = (!empty($_POST['student_pincode'])) ? $_POST['student_pincode'] : '' ;
					$data['father_name'] = (!empty($_POST['father_name'])) ? $_POST['father_name'] : '' ;
					$data['father_contact_no'] = (!empty($_POST['father_contact_no'])) ? $_POST['father_contact_no'] : '' ;
					$data['father_contact_no2'] = (!empty($_POST['father_contact_no2'])) ? $_POST['father_contact_no2'] : '' ;
					$data['area_locality_sector'] = (!empty($_POST['area_locality_sector'])) ? $_POST['area_locality_sector'] : '' ;
					$data['lead_category'] = (!empty($_POST['lead_category'])) ? $_POST['lead_category'] : '' ;
					if(!empty($_POST['know_about_us']) && isset($_POST['know_about_us'])){
						$data['know_about_us'] = implode(",",$_POST['know_about_us']);
					}
					if(!empty($_POST['course_interested_in']) && isset($_POST['course_interested_in'])){
						$data['course_interested_in'] = implode(",",$_POST['course_interested_in']);
					}
					$data['father_emailid'] = (!empty($_POST['father_emailid'])) ? $_POST['father_emailid'] : '' ;
					
					$data['inquiry_remark'] = (!empty($_POST['inquiry_remark'])) ? $_POST['inquiry_remark'] : '' ;
					$data['know_us_others'] = (!empty($_POST['know_us_others'])) ? $_POST['know_us_others'] : '' ;
				
					$data['updated_on'] = date("Y-m-d H:i:s");
					$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
					$result = $this->leadfollowupmodel->updateRecord('tbl_inquiry_master', $data,'inquiry_master_id',$_POST['hidden_lead_id']);
					
				}else{
					
					$get_inquiry_no = $this->leadfollowupmodel->getdata_orderby_limit1("tbl_inquiry_master", "inquiry_no", "rec_type='Lead' ", " order by inquiry_master_id desc");
					if(!empty($get_inquiry_no)){
						$data['inquiry_no'] = str_pad( ($get_inquiry_no[0]['inquiry_no'] + 1) , 6, '0', STR_PAD_LEFT);
					}else{
						$data['inquiry_no'] = '000001';
					}
					
					$data['rec_type'] = (!empty($_POST['rectype'])) ? $_POST['rectype'] : '' ;
					$data['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '' ;
					$data['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '' ;
					$data['inquiry_date'] = date("Y-m-d");
					//$data['inquiry_no'] = (!empty($response_headers['inquiryno'])) ? $response_headers['inquiryno'] : '' ;
					$data['academic_year_master_id'] = (!empty($_POST['academicyear'])) ? $_POST['academicyear'] : '' ;
					$data['student_dob'] = (!empty($_POST['student_dob'])) ? date("Y-m-d", strtotime($_POST['student_dob'])) : '' ;
					$data['gender'] = (!empty($_POST['gender'])) ? $_POST['gender'] : '' ;
					$data['student_first_name'] = (!empty($_POST['student_first_name'])) ? $_POST['student_first_name'] : '' ;
					$data['student_last_name'] = (!empty($_POST['student_last_name'])) ? $_POST['student_last_name'] : '' ;
					
					$data['country_id'] = (!empty($_POST['country_id'])) ? $_POST['country_id'] : '' ;
					$data['state_id'] = (!empty($_POST['state_id'])) ? $_POST['state_id'] : '' ;
					$data['city_id'] = (!empty($_POST['city_id'])) ? $_POST['city_id'] : '' ;
					$data['pincode'] = (!empty($_POST['student_pincode'])) ? $_POST['student_pincode'] : '' ;
					$data['father_name'] = (!empty($_POST['father_name'])) ? $_POST['father_name'] : '' ;
					$data['father_contact_no'] = (!empty($_POST['father_contact_no'])) ? $_POST['father_contact_no'] : '' ;
					$data['father_contact_no2'] = (!empty($_POST['father_contact_no2'])) ? $_POST['father_contact_no2'] : '' ;
					$data['area_locality_sector'] = (!empty($_POST['area_locality_sector'])) ? $_POST['area_locality_sector'] : '' ;
					$data['lead_category'] = (!empty($_POST['lead_category'])) ? $_POST['lead_category'] : '' ;
					if(!empty($_POST['know_about_us']) && isset($_POST['know_about_us'])){
						$data['know_about_us'] = implode(",",$_POST['know_about_us']);
					}
					if(!empty($_POST['course_interested_in']) && isset($_POST['course_interested_in'])){
						$data['course_interested_in'] = implode(",",$_POST['course_interested_in']);
					}
					$data['father_emailid'] = (!empty($_POST['father_emailid'])) ? $_POST['father_emailid'] : '' ;
					
					$data['inquiry_remark'] = (!empty($_POST['inquiry_remark'])) ? $_POST['inquiry_remark'] : '' ;
					$data['know_us_others'] = (!empty($_POST['know_us_others'])) ? $_POST['know_us_others'] : '' ;
					// $data['lead_status'] = (!empty($response_headers['leadstatus'])) ? $response_headers['leadstatus'] : '' ;
					// $data['lead_progress'] = (!empty($response_headers['leadprogress'])) ? $response_headers['leadprogress'] : '' ;
					$data['type'] = 1;
					$data['lead_status_id'] = $_POST['lead_status'];
					$data['lead_sub_status_id'] = $_POST['lead_sub_status'];
					$data['lead_planned_status_id'] = $_POST['planned_status'];
					$data['lead_enquiry_status_id'] = $_POST['lead_enquiry_status'];
					// $data['manual_ref_no'] = (!empty($response_headers['manualrefno'])) ? $response_headers['manualrefno'] : '' ;
					// $data['inquiry_handled_by'] = (!empty($_POST['inquiry_handled_by'])) ? $_POST['inquiry_handled_by'] : '' ;
					
					$data['created_on'] = date("Y-m-d H:i:s");
					$data['created_by'] = $_SESSION["webadmin"][0]->user_id;	
					$result = $this->leadfollowupmodel->insertData('tbl_inquiry_master', $data, 1);
					$data_status['lead_id'] = $result;
					$data_status['created_on'] = date("Y-m-d H:i:s");
					$data_status['created_by'] = $_SESSION["webadmin"][0]->user_id;
					$result_log = $this->leadfollowupmodel->insertData('tbl_lead_enquiry_log', $data_status, '1');
					if(!empty($_POST['follow_up']) && isset($_POST['follow_up'])){
						$data_follow['created_on'] = date("Y-m-d H:i:s");
						$data_follow['created_by'] = $_SESSION["webadmin"][0]->user_id;
						$data_follow['lead_id'] = $result;
						$data_follow['type'] = 1;
						$result_log = $this->leadfollowupmodel->insertData('tbl_lead_enquiry_log', $data_follow, '1');
					}
					
				}
				if(!empty($result)){
					echo json_encode(array("success"=>"1",'msg'=>'lead created/updated successfully!'));
					exit;
				}else{
					echo json_encode(array("success"=>"0",'msg'=>'lead not created.!'));
					exit;
				}	
			}
		}
	}
	
	function export(){
		$get_result = $this->leadfollowupmodel->getExportRecords($_POST);

		// echo "<pre>";
		// print_r($get_result);
		// exit;
		if(!empty($get_result['query_result']) && count($get_result['query_result']) > 0)
		{
			$this->load->library('Excel');
			
			//Create new PHPExcel object
			$objPHPExcel = new PHPExcel();
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			//Set properties
			$objPHPExcel->getProperties()->setCreator("Attoinfotech")
			->setLastModifiedBy("Attoinfotech")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Export Excel");
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
			
			
			// Rename sheet
			$objPHPExcel->getActiveSheet()->setTitle('OutPut-File');
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("A1", 'LD Date', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("B1", 'LD No', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("C1", 'ENQ Date', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("D1", 'ENQ No', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("E1", 'Type', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("F1", 'Student Name', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("G1", 'Father Name', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("H1", 'Father Email', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("I1", 'Father Number', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("J1", 'Mother Name', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("K1", 'Mother Email', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("L1", 'Mother Number', PHPExcel_Cell_DataType::TYPE_STRING);

			$objPHPExcel->getActiveSheet()->setCellValueExplicit("M1", 'Course Name', PHPExcel_Cell_DataType::TYPE_STRING);

			$objPHPExcel->getActiveSheet()->setCellValueExplicit("N1", 'Source', PHPExcel_Cell_DataType::TYPE_STRING);

			$objPHPExcel->getActiveSheet()->setCellValueExplicit("O1", 'Zone', PHPExcel_Cell_DataType::TYPE_STRING);

			$objPHPExcel->getActiveSheet()->setCellValueExplicit("P1", 'Center', PHPExcel_Cell_DataType::TYPE_STRING);

			$objPHPExcel->getActiveSheet()->setCellValueExplicit("Q1", 'Status', PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objPHPExcel->getActiveSheet()->getStyle("A1:Q1")->getFont()->setBold(true);
			
			$j = 2;
			
			for($i=0;$i<sizeof($get_result['query_result']);$i++)
			{
				$enq_date ='';
				$enq_no ='';
				$course = "";
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("A$j", date("m/d/Y",strtotime($get_result['query_result'][$i]->lead_date)));

				$objPHPExcel->getActiveSheet()->setCellValueExplicit("B$j", "LD".$get_result['query_result'][$i]->lead_no);

				$objPHPExcel->getActiveSheet()->setCellValueExplicit("C$j", $enq_date);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("D$j", $enq_no);


				$objPHPExcel->getActiveSheet()->setCellValueExplicit("E$j", $get_result['query_result'][$i]->rec_type);

				$objPHPExcel->getActiveSheet()->setCellValueExplicit("F$j", $get_result['query_result'][$i]->student_first_name." ".$get_result['query_result'][$i]->student_last_name );

				$objPHPExcel->getActiveSheet()->setCellValueExplicit("G$j", $get_result['query_result'][$i]->father_name);

				$objPHPExcel->getActiveSheet()->setCellValueExplicit("H$j", $get_result['query_result'][$i]->father_emailid);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("I$j", $get_result['query_result'][$i]->father_contact_no);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("J$j", $get_result['query_result'][$i]->mother_name);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("K$j", $get_result['query_result'][$i]->mother_emailid);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("L$j", $get_result['query_result'][$i]->mother_contact_no);

				$objPHPExcel->getActiveSheet()->setCellValueExplicit("M$j", $course);

				$objPHPExcel->getActiveSheet()->setCellValueExplicit("N$j", $get_result['query_result'][$i]->how_know_about_school);
				
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("O$j", $get_result['query_result'][$i]->zone_name);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("P$j",  $get_result['query_result'][$i]->center_name);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("Q$j", $get_result['query_result'][$i]->inquiry_progress);
				
				$j++;

			}
			
			// Redirect output to a client's web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="lead('.date('d-m-Y').').xls"');
			header('Cache-Control: max-age=0');
			
			$objWriter->save('php://output');
			$_SESSION['contactus_export_success'] = "success";
			$_SESSION['contactus_export_msg'] = "Records Exported Successfully.";
			die();
			redirect('lead');
			exit;
		}
		else
		{
			$_SESSION['contactus_export_success'] = "error";
			$_SESSION['contactus_export_msg'] = "No records found for export";	
			redirect('lead');
			exit;
		}
	}
	
	
	public function getRegion(){
		//$result = $this->newslettersmodel->getOptions("tbl_subcategories",$_REQUEST['category_id'],"category_id");
		$input_data = array('action'=>'FetchRegion', 'BrandID'=>'112', 'Zone'=>''.$_REQUEST['zone_text'].'');
		$result = curlFunction(SERVICE_URL, $input_data);
		$result = json_decode($result, true);
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$region_text = '';
		
		if(isset($_REQUEST['region_text']) && !empty($_REQUEST['region_text'])){
			$region_text = $_REQUEST['region_text'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]['Region'] == $region_text) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]['Region'].'" '.$sel.' >'.$result[$i]['Region'].'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	
	public function getArea(){
		$input_data = array('action'=>'FetchArea', 'BrandID'=>'112', 'Region'=>''.$_REQUEST['region_text'].'');
		$result = curlFunction(SERVICE_URL, $input_data);
		$result = json_decode($result, true);
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$area_text = '';
		
		if(isset($_REQUEST['area_text']) && !empty($_REQUEST['area_text'])){
			$area_text = $_REQUEST['area_text'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]['Area'] == $area_text) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]['Area'].'" '.$sel.' >'.$result[$i]['Area'].'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getCenter(){
		$input_data = array('action'=>'FetchCenter', 'BrandID'=>'112', 'Area'=>''.$_REQUEST['area_text'].'');
		$result = curlFunction(SERVICE_URL, $input_data);
		$result = json_decode($result, true);
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]['CenterID'] == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]['CenterID'].'" '.$sel.' >'.$result[$i]['CenterName'].'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	function getSearchLists(){
		if(!empty($_POST['value']) && !empty($_POST['column_name'])){
			$options = "";
			if($_POST['column_name']  == "zone_id"){
				$centerDetails = $this->leadfollowupmodel->getData("tbl_centers","center_id,center_name",array("status"=>'Active',"zone_id"=>$_POST['value']));
				$options = "<option value=''>Select Center</option>";
				if(!empty($centerDetails)){
					foreach($centerDetails as $key=>$val){
						$options .= "<option value='".$val['center_id']."'>".$val['center_name']."</option>"; 
					}
				}
				echo json_encode(array("status"=>true,"option"=>$options));
				exit;
			}else{
				$courseDetails = $this->leadfollowupmodel->getCenterMappedCourses($_POST['value']);
				$options = " <option value=''>Select Class</option>";
				if(!empty($courseDetails)){
					foreach($courseDetails as $key=>$val){
						$options .= "<option value='".$val['course_id']."'>".$val['course_name']."</option>"; 
					}
				}
				echo json_encode(array("status"=>true,"option"=>$options));
				exit;
			}
		}
	}
	function getSubStatus(){
		$result = $this->leadfollowupmodel->getdata("tbl_lead_sub_status","*"," lead_status_id = ".$_POST['status_id']."  ");
		// echo sizeof($result);exit;
		$option = '';
			if(!empty($result)){
				foreach ($result as $key => $value) {
					$sel='';
					// if(sizeof($result) =='1'){
					// 	$sel='selected';
					// }else{
					// 	$sel = ($value['lead_sub_status'] == $_POST['sub_status_id'])? 'selected="selected"' : '';
					// }
					$sel = ($value['lead_sub_status'] == $_POST['sub_status_id'])? 'selected="selected"' : '';
					$option .= '<option value='.$value['lead_sub_status'].' '.$sel.' >'.$value['status_sub_description'].'</option>';
				}
				
				echo json_encode(array("status"=>"success","option"=>$option));
				exit;
			}
	}
	function getLeadEnquiyStage(){
		// print_r($_POST);
		$result = $this->leadfollowupmodel->getdata("tbl_lead_enquiry_status","*"," status_id = ".$_POST['status_id']." AND sub_status_id = ".$_POST['sub_status_id']."  ");
		// print_r($result);
		$option = '';
			if(!empty($result)){
				foreach ($result as $key => $value) {
					$sel='';
					if(!empty($_POST['lead_enquiry_status_id'])){
						$sel = ($value['lead_enquiry_status_id'] == $_POST['lead_enquiry_status_id'])? 'selected' : '';
					}else{
						$sel='selected';
					}
					$option .= '<option value='.$value['lead_enquiry_status_id'].' '.$sel.' >'.$value['lead_enquiry_status_description'].'</option>';
				}
				echo json_encode(array("status"=>"success","option"=>$option));
				exit;
			}
	}
	public function addEditFollow()
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			
			$result = "";
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
				// echo $record_id;exit;
				$result['details'] = $this->leadfollowupmodel->getFormdata($record_id);
				// echo "<pre>";
				// print_r($result['details']);
				// exit;
				$result['status_logs'] = $this->leadfollowupmodel->getdata_log($record_id);
				// echo "<pre>";
				// print_r($result['status_logs']);
				// exit;
				$result['follow_logs_id'] = $this->leadfollowupmodel->getDataDescLimit("tbl_lead_enquiry_log", "*","lead_id = $record_id and lead_status_id IS NULL and lead_sub_status_id IS NULL and lead_planned_status_id IS NULL and lead_enquiry_status_id IS NULL");
				
				// echo $this->db->last_query();exit;
				$condition ="i.lead_id = '".$record_id."' and i.lead_status_id !='' and i.lead_sub_status_id !='' and i.lead_planned_status_id !='' and i.lead_enquiry_status_id !='' ";
				$result['follow_logs_last'] = $this->leadfollowupmodel->getlastdata($condition);
				// echo $this->db->last_query();exit;
				
				$result['follow_logs'] = $this->leadfollowupmodel->getdata("tbl_lead_enquiry_log", "*","lead_id = $record_id and follow_up_date !='' order by created_on desc limit 1 ");
				// echo "<pre>";print_r($result['follow_logs']);exit;
			}
			$result['status'] = $this->leadfollowupmodel->getdata("tbl_lead_status", "*","status = 'Active' ");
			$result['planned_status'] = $this->leadfollowupmodel->getdata("tbl_default_status", "*","status = 'Active' ");
			
			// echo "<pre>";
			// print_r($result['follow_logs']);
			// exit;
			$this->load->view('template/header.php');
			$this->load->view('lead/addEditfollow', $result);
			$this->load->view('template/footer.php');
		}
		else {
			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	public function submitFollowUpForm()
	{
		// echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				$data = array();
				$data_follow = array();
				$no_further_follow_up = array();
				$data['lead_status_id'] = $_POST['lead_status'];
				$data['lead_sub_status_id'] = $_POST['lead_sub_status'];
				$data['lead_planned_status_id'] = $_POST['planned_status'];
				$data['lead_enquiry_status_id'] = $_POST['lead_enquiry_status'];
				$data['notes'] = $_POST['note'];
				$data['type'] = 1;

			
				$data['updated_on'] = date("Y-m-d H:i:s");
				$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				$result = $this->leadfollowupmodel->updateRecord('tbl_lead_enquiry_log', $data,'ID',$_POST['log_lead_id']);
				if(!empty($_POST['no_further_follow_up']) && $_POST['no_further_follow_up'] =='1'){
					$no_further_follow_up['no_further_follow_up'] = $_POST['no_further_follow_up'];
					
					$update_inquiry = $this->leadfollowupmodel->updateRecord('tbl_inquiry_master', $no_further_follow_up,'inquiry_master_id',$_POST['hidden_lead_id']);
				}
				

				if(!empty($_POST['next_follow_up']) && isset($_POST['next_follow_up'])){
					$data_follow['follow_up_date'] =  date('Y-m-d', strtotime($_POST['next_follow_up']));
					$data_follow['follow_up_time'] =  $_POST['follow_up_time'];
				}
				if(!empty($_POST['next_follow_up']) && isset($_POST['next_follow_up'])){
					$data_follow['created_on'] = date("Y-m-d H:i:s");
					$data_follow['created_by'] = $_SESSION["webadmin"][0]->user_id;
					$data_follow['lead_id'] = $_POST['hidden_lead_id'];
					$data_follow['type'] = 1;
					$result_log = $this->leadfollowupmodel->insertData('tbl_lead_enquiry_log', $data_follow, '1');
				}
			if (!empty($result)) {
				echo json_encode(array('success' => true,'msg' => 'Record Added/Updated Successfully.'));
				exit;
			}else{
				echo json_encode(array('success' => false,'msg' => 'Problem in data update.'));
				exit;
			}
			
		}else {
			echo json_encode(array('success' => false,'msg' => 'this is not ajax submit'));
			exit;
		}
	}
	
		public function incrementInqury_no(){
		$filterData['zoneDetails'] =
		$condition = "rec_type = 'Lead' AND inquiry_master_id between 1295 and 1950 ";
		$total_inquiry =  $this->leadfollowupmodel->getData("tbl_inquiry_master","inquiry_master_id,inquiry_no",$condition);

		echo "<pre>";
		// print_r($total_inquiry);
		foreach ($total_inquiry as $key => $value) {
			$get_inquiry_no = $this->leadfollowupmodel->getdata_orderby_limit1("tbl_inquiry_master", "inquiry_no", "rec_type='Lead' AND inquiry_master_id >= 1294 ", " order by inquiry_no desc");
			// print_r($get_inquiry_no);
			$data =array();
			$data['inquiry_no'] = str_pad( ($get_inquiry_no[0]['inquiry_no'] + 1) , 6, '0', STR_PAD_LEFT);
			// print_r($data);
			$this->leadfollowupmodel->updateRecord('tbl_inquiry_master', $data,'inquiry_master_id',$value['inquiry_master_id']);
			// exit;
		}

		exit;
	}
}

?>
