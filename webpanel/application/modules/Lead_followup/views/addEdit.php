

<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
		<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Lead Details</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>centermaster">Lead Details</a></li>
                    </ul>
                  </div>
                </div>

			<div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-12 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								 <input type="hidden" id="hidden_lead_id" name="hidden_lead_id" value="<?php if(!empty($details[0]->inquiry_master_id)){echo $details[0]->inquiry_master_id;}?>" />
								 <input type="hidden" id="rectype" name="rectype" value="Lead" />

                                <hr>
								<div class="row">
										<div class="col-sm-6 control-group form-group">
                                            <label class="control-label" for="center_id">Select Zone*</label>
                                            <div class="controls">
                                            <select id="zone_id" name="zone_id" class="form-control selectpicker required show-tick "  onchange="getCenters(this.value)" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select zone...">
                                                    <option value="">Select Zone</option>
                                                    <?php 
                                                        if(isset($zones) && !empty($zones)){
                                                            foreach($zones as $cdrow){
                                                                $sel='';
                                                                if(!empty($details[0]->zone_id)){
                                                                    $sel = ($cdrow['zone_id'] == $details[0]->zone_id)? 'selected="selected"' : '';?>
																<?php }
																// else{
                                                                //     $sel = ($cdrow['zone_name']== 'India')?'selected="selected"':''?>
                                                                 <?php //}?>
                                                        <option value="<?php echo $cdrow['zone_id'];?>" <?php echo $sel; ?>><?php echo $cdrow['zone_name'];?></option>
                                                    <?php }}?>
                                                </select>
                                            </div>
                                        </div>

										<div class="col-sm-6 control-group form-group" style="padding-left: 23px;">
											<label class="control-label" for="lead_sub_status">Select Center*</label>
											<div class="controls">
												<select id="center_id" name="center_id" class="form-control show-tick required"  data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select center ..." >
													<option value="">Select Center</option>
												</select>
										</div>
									</div>
								</div>
								<!-- <h3><center>CHILD INFORMATION :</center></h3> -->
								<a href="javascript:void(0)" class="btn btn-primary">CHILD INFORMATION </a>
								<hr>
								<div class="row">
									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>First Name</span></label>
										<div class="controls">
											<input type="text" class="form-control " placeholder="Enter first name" name="student_first_name" value="<?php if(!empty($details[0]->student_first_name)){echo $details[0]->student_first_name;}?>"  maxlength="50">
										</div>
									</div>

									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>Last Name</span></label>
										<div class="controls">
											<input type="text" class="form-control" placeholder="Enter last name" name="student_last_name" value="<?php if(!empty($details[0]->student_last_name)){echo $details[0]->student_last_name;}?>"  maxlength="50">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-6 control-group form-group" style="margin-top: 10px;" >
										<label class="control-label" style="margin-left: 5px;"><span>Gender</span></label>
										<div class="controls">
										<input type="radio"    name="gender" class=""  value="Male" <?= (!empty($details[0]->gender) && $details[0]->gender == 'Male'?'checked':'')?> ><label>Male</label>
										<input type="radio"   name="gender" class="required "  value="Female" <?= (!empty($details[0]->gender) && $details[0]->gender == 'Female'?'checked':'')?>
										><label>Female</label>
										</div>
									</div>

									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>Date of birth</span></label>
										<div class="controls">
											<input type="text" class="form-control " placeholder="choose date of birth"  id="student_dob" name="student_dob" value="<?php if(!empty($details[0]->student_dob)){echo date('d-m-Y', strtotime($details[0]->student_dob));}?>"  readonly>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6 control-group form-group" >
											<?php $course_interested_in = array('Prenursery', 'Nursery', 'Kindergarten 1','Kindergarten 2','Day Care','ECCP Program','Summer Camp','Winter Camp');?>
											<label class="control-label" for="course_interested_in">Course interested In</label>
											<div class="controls">
											<select id="course_interested_in" name="course_interested_in[]" class="form-control selectpicker show-tick " data-style="btn-primary" multiple data-actions-box="true" data-live-search="true" data-live-search-placeholder="Course interested In..."   >
												<option value="">Select Course interested In</option>
												<?php 
													$stored_course_interested_in = explode(",",$details[0]->course_interested_in);
													foreach ($course_interested_in as $key => $value) {
														$sel='';
														$sel= (in_array($value,$stored_course_interested_in)?'selected':'');
													?>
													<option value="<?=$value?>" <?= $sel?> ><?=$value?></option>
												<?php } ?>		
											</select>
											</div>
										</div>
								</div>

								<!-- <h3><center>FAMILY INFORMATION:</center></h3> -->
								<a href="javascript:void(0)" class="btn btn-primary">FAMILY INFORMATION </a><hr>
								<div class="row">
									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>Parent Name*</span></label>
										<div class="controls">
											<input type="text" class="form-control required " placeholder="Enter Parent  name" name="father_name" value="<?php if(!empty($details[0]->father_name)){echo $details[0]->father_name;}?>"  maxlength="50">
										</div>
									</div>

									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>Parent Contant Number*</span></label>
										<div class="controls">
											<input type="text" class="form-control required " placeholder="Enter Parent contact number" name="father_contact_no" value="<?php if(!empty($details[0]->father_contact_no)){echo $details[0]->father_contact_no;}?>" minlength="10" >
										</div>
									</div>

									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>Parent Contant Number 2</span></label>
										<div class="controls">
											<input type="text" class="form-control" placeholder="Enter Parent contact number 2" name="father_contact_no2" value="<?php if(!empty($details[0]->father_contact_no2)){echo $details[0]->father_contact_no2;}?>"  maxlength="10" minlength="10">
										</div>
									</div>

									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>Parent's Email Id*</span></label>
										<div class="controls">
											<input type="email" class="form-control required " placeholder="Enter Parent  name" name="father_emailid" value="<?php if(!empty($details[0]->father_emailid)){echo $details[0]->father_emailid;}?>"  maxlength="50">
										</div>
									</div>
									
									<div class="col-sm-6 control-group form-group">
										<label class="control-label" for="center_id">Country*</label>
										<div class="controls">
										<select id="country_id" name="country_id" class="form-control  required show-tick "  onchange="getstate(this.value)" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select Country...">
												<option value="">Select Country</option>
												<?php 
													if(isset($countries) && !empty($countries)){
														foreach($countries as $cdrow){
															$sel='';
															if(!empty($details[0]->country_id)){
																$sel = ($cdrow['country_id'] == $details[0]->country_id)? 'selected="selected"' : '';?>
															<?php }
															else{
																$sel = ($cdrow['country_name']== 'India')?'selected="selected"':''?>
																<?php }?>
													<option value="<?php echo $cdrow['country_id'];?>" <?php echo $sel; ?>><?php echo $cdrow['country_name'];?></option>
												<?php }}?>
											</select>
										</div>
									</div>

									<div class="col-sm-6 control-group form-group">
										<label class="control-label" for="center_id">State*</label>
										<div class="controls">
											<select id="state_id" name="state_id" class="form-control show-tick " onchange="getcity(this.value)" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select State...">
												<option value="">Select state</option>
											</select>
										</div>
									</div>

									<div class="col-sm-6 control-group form-group">
										<label class="control-label" for="center_id">City*</label>
										<div class="controls">
										<select id="city_id" name="city_id" class="form-control show-tick " data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select City..."  >
												<option value="">Select City</option>
											</select>
										</div>
									</div>

									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>Area Locality Sector</span></label>
										<div class="controls">
											<input type="text" class="form-control" placeholder="Enter Area Locality Sector" name="area_locality_sector" value="<?php if(!empty($details[0]->area_locality_sector)){echo $details[0]->area_locality_sector;}?>" >
										</div>
									</div>

									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>Pincode</span></label>
										<div class="controls">
											<input type="text" class="form-control " placeholder="Enter pincode" id="student_pincode" name="student_pincode" value="<?php if(!empty($details[0]->pincode)){echo $details[0]->pincode;}?>"  maxlength="6" minlength="6">
										</div>
									</div>

									<div class="col-sm-6 control-group form-group">
										<?php $lead_type = array('Direct Walk-ins', 'Incoming Phone Calls', 'Database Tele-calling','Incoming Email/Message','Social Media Campaign','Google Ad wards');?>
										<label class="control-label" for="career_field">Lead Category*</label>
										<div class="controls">
										<select id="lead_category" name="lead_category" class="form-control show-tick required" data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Lead Category ..."   >
											<option value="">Select Lead Category</option>
											<?php 
												$stored_lead_type = explode(",",$details[0]->lead_category);
												foreach ($lead_type as $key => $value) {
													$sel='';
													$sel= (in_array($value,$stored_lead_type)?'selected':'');
												?>
												<option value="<?=$value?>" <?= $sel?> ><?=$value?></option>
											<?php } ?>		
										</select>
										</div>
									</div>
								</div>
								<div class="row">
									
								</div>
								<div class="row">
									<div class="col-sm-6 control-group form-group" style="padding-right: 25px;">
										<?php $knowaboutus = array('Montana Website','Facebook','Instagram','Google Ad-wards', 'Google Local Business', 'Just Dial','Newspaper Ads','Leaflet Inserts','Leaflet Distribution','Posters','Banners','Hoarding','Platform Boards','Bus Shelter Displays','Database Tele-calling','School Sign Boards','Existing Parent Reference','Pass out’s Parent Reference','Friends/Relatives Reference','Staff Reference','Others');?>
										<label class="control-label" for="know_about_us">How did you come to Know about us*</label>
										<div class="controls">
										<select id="know_about_us" name="know_about_us[]" class="form-control selectpicker required" multiple data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="How did you come to Know about us..." onchange="others_field()">
														<option value="">Select How did you come to Know about us</option>
													<?php 
														$stored_knowaboutus = explode(",",$details[0]->know_about_us);
														foreach ($knowaboutus as $key => $value) {
															$sel='';
															$sel= (in_array($value,$stored_knowaboutus)?'selected':'');
														?>
														<option value="<?=$value?>" <?= $sel?> ><?=$value?></option>
													<?php } ?>		
											</select>
										</div>
									</div>
									<?php
									$display = 'none'; 
									if(!empty($details[0]->know_us_others)){
										$display = 'block';
									}?>	
									<div class="col-sm-6 control-group form-group" id="know_us_others_div" style="display:<?= $display ?>">
										<label class="control-label">Others</label>
										<div class="controls">
											<textarea name="know_us_others" id="know_us_others" cols="76" rows="2"><?php if(!empty($details[0]->know_us_others)){echo $details[0]->know_us_others;}?></textarea>
										</div>
									</div>
								</div>
							
								<div class="row">
									

                                    <!-- <div class="col-sm-6 control-group form-group">
										<?php $leadstatus = array('Hot','Worm','Cold');?>
										<label class="control-label" for="leadstatus">Lead Status*</label>
										<div class="controls">
										<select id="leadstatus" name="leadstatus" class="form-control show-tick required"  data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Choose  ..." >
												<option value="">select Lead Status</option>
													<?php 
														$stored_leadstatus = explode(",",$details[0]->know_about_us);
														foreach ($leadstatus as $key => $value) {
															$sel='';
															$sel= (in_array($value,$stored_leadstatus)?'selected':'');
														?>
														<option value="<?=$value?>" <?= $sel?> ><?=$value?></option>
													<?php } ?>		
											</select>
										</div>
									</div> -->

								</div>	
								<a href="javascript:void(0)" class="btn btn-primary">Follow-Ups </a><hr>
								<div class="row">
									<div class="col-sm-6 control-group form-group">
										<label class="control-label" for="lead_status">Status*</label>
										<div class="controls">
										<select id="lead_status" name="lead_status" class="form-control selectpicker show-tick required" onchange="getSubStatus(this.value)" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select Status..."  <?php echo (!empty($details)) ? "disabled" : '' ?>>
										<option value="">Select Status </option>
											<?php foreach ($status as $key => $value) {
													$sel='';
													$sel = ($value['lead_status_id'] == $details[0]->lead_status_id)? 'selected="selected"' : '';?>
												<option value="<?=$value['lead_status_id']?>" <?= $sel?> ><?=$value['status_description']?></option>
											<?php } ?>		
											</select>
										</div>
									</div>

									<div class="col-sm-6 control-group form-group" style="padding-left: 23px;">
										<label class="control-label" for="lead_sub_status">Sub Status*</label>
										<div class="controls">
										<select id="lead_sub_status" name="lead_sub_status" class="form-control show-tick required" onchange="getLeadEnquiyStage('',this.value)"data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select Sub Status..." <?php echo (!empty($details)) ? "disabled" : '' ?>>
												<option value="">Select Sub State</option>
												
											</select>
										</div>
									</div>
								</div>					

								<div class="row">
									<div class="col-sm-6 control-group form-group">
										<label class="control-label" for="know_about_us">Action Planned/Taken*</label>
										<div class="controls">
										<select id="planned_status" name="planned_status" class="form-control selectpicker show-tick required" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select Action Planned/Taken..."  <?php echo (!empty($details)) ? "disabled" : '' ?>>
													<option value="">Select Planned/Taken</option>
													<?php foreach ($planned_status as $key => $value) {
														$sel='';
														$sel = ($value['default_status_id'] == $details[0]->lead_planned_status_id)? 'selected="selected"' : '';?>?>
														<option value="<?=$value['default_status_id']?>" <?= $sel ?>><?=$value['status_description']?></option>
													<?php } ?>		
											</select>
										</div>
									</div>	

									<div class="col-sm-6 control-group form-group" style="padding-left: 23px;">
										<label class="control-label" for="lead_enquiry_status">Lead/Enquiry Stage*</label>
										<div class="controls">
										<select id="lead_enquiry_status" name="lead_enquiry_status" class="form-control required show-tick "  data-style="btn-primary"  data-live-search="true" data-live-search-placeholder="Select Lead/Enquiry Stage.."  <?php echo (!empty($details)) ? "disabled" : '' ?>>
													<option value="">Select Lead/Enquiry Stage</option>	
											</select>
										</div>
									</div>
								</div>	
					
								<div class="row">
									<div class="col-sm-6 control-group form-group">
										<label class="control-label">Note</label>
										<div class="controls">
											<textarea name="note" id="note" cols="64" rows="2" <?php echo (!empty($follow_logs[0]['notes'])) ? 'disabled' : ''?>><?php echo (!empty($follow_logs[0]['notes'])) ?$follow_logs[0]['notes'] : ''?></textarea>
										</div>
									</div>
									
									<div class="col-sm-4 control-group form-group" style="padding-left: 23px;">
										<label class="control-label"><span>Follow-Up date*</span></label>
										<div class="controls">
											<input type="text" class="form-control required datepicker" placeholder="follow up date" id="follow_up" name="follow_up" <?php echo (!empty($follow_logs[0]['follow_up_date'])) ? 'disabled' : ''?> value="<?php echo (!empty($follow_logs[0]['follow_up_date'])) ? date('d-m-Y',strtotime($follow_logs[0]['follow_up_date'])) : '' ?>">
										</div>
									</div>
									<div class="col-sm-2 control-group form-group">
										<label class="control-label"><span>Select Time</span></label>
										<div class="controls">
										<input placeholder="Selected time" type="text" id="input_starttime" name="follow_up_time" <?php echo (!empty($follow_logs[0]['follow_up_time'])) ? 'disabled' : ''?> class="form-control timepicker">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-6 control-group form-group">
										<label class="control-label" for="lead_enquiry_status">Remark (For Office Use)</label>
										<div class="controls">
											<textarea name="inquiry_remark" id="inquiry_remark" cols="64" rows="2"><?php if(!empty($details[0]->inquiry_remark)){echo $details[0]->inquiry_remark;}?></textarea>
										</div>
									</div>
									<div class="col-sm-6 control-group form-group" style="padding-left: 23px;">
										<label class="control-label"><span>Updated BY</span></label>
										<div class="controls">
										<input type="text" class="form-control" value="<?php echo $_SESSION["webadmin"][0]->first_name." ".$_SESSION["webadmin"][0]->last_name; ?>" disabled>
										</div>
									</div>
								</div>
						</div>					
								<div class="row">
									<div class="col-sm-12 col-md-12 form-actions form-group">
										<button type="submit" class="btn btn-primary">Submit</button>
										<a href="<?php echo base_url();?>lead" class="btn btn-primary">Cancel</a>
									</div>
								</div>
							</form>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>        
		<!-- end: Content -->								
<script>
function others_field(){
	var checkvalues = $('#know_about_us').val(); 
	if(checkvalues ==null){
		$('#know_us_others_div').fadeOut()
		$('#know_us_others').val('');
	}
	var getval = checkvalues.includes("Others");
	if(getval){
		$('#know_us_others_div').fadeIn();
	}else{
		$('#know_us_others_div').fadeOut()
		$('#know_us_others').val('');
	}
}
$( function() {
    $( "#student_dob" ).datepicker();
  } );

$('document').ready(function(){
	// $(".selectpicker").selectpicker("refresh");
	$(".number_only").keypress(function (e) {
    	if (e.which != 8 && e.which != 0 && e.which != 46 &&(e.which < 48|| e.which > 57)) {
      	return false;
    	}
  	});

      <?php if(!empty($details) && isset($details)){?>
        getstate(<?= $details[0]->country_id?>,<?= $details[0]->state_id?>);
        getcity(<?= $details[0]->state_id?>,<?= $details[0]->city_id?>);
		getSubStatus(<?= $details[0]->lead_status_id?>,<?= $details[0]->lead_sub_status_id?>);
		getLeadEnquiyStage(<?= $details[0]->lead_status_id?>,<?= $details[0]->lead_sub_status_id?>,<?= $details[0]->lead_enquiry_status_id?>);
		getCenters(<?= $details[0]->zone_id?>,<?= $details[0]->center_id?>);
		<?php }else{?>
			/* this is for 1st time when we want country should be india */
			getstate($('#country_id').val());
		<?php } ?>
})
$( document ).ready(function() {
	
	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						//['Source', 'Templates'],
						['Cut','Copy','Paste','PasteText','-','Print', 'SpellChecker', 'Scayt'],
						['Subscript','Superscript'],
						['NumberedList','BulletedList'],
						['BidiLtr', 'BidiRtl' ],
						//['Maximize', 'ShowBlocks'],
						['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						//['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						//['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
	
	$.validator.addMethod("needsSelection", function(value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });
	
});

$('.career_in').on('change', function() {
   var careerin = $("input[name='career_in']:checked").val(); 
   if(careerin == '2'){
	   $('#career_main_div').fadeIn();
   }else{
	$('#career_main_div').fadeOut()
   }
});



function getstate(country_id,state_id=null){
	if(country_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>lead/getStates",
			data:{country_id:country_id,state_id:state_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#state_id").html("<option value=''>Select State</option>"+res['option']);
						$("#state_id").selectpicker("refresh");
					}else{
						$("#state_id").html("<option value=''>No state found</option>");
						$("#state_id").selectpicker("refresh");
					}
				}
				/* else{	
					$("#state_id").html("<option value=''>Select</option>");
					$("#state_id").selectpicker("refresh");
				} */
			}
		});
	}
}

function getLeadEnquiyStage(status_id=null,sub_status_id=null,lead_enquiry_status_id=null){
	// alert(sub_status_id);
	<?php if(empty($details)){?>
		var status_id = $('#lead_status').val();
		var sub_status_id =$('#lead_sub_status').val() ;
		// alert("sub_status_id");
	<?php }?>
	if(status_id && sub_status_id ){
		$.ajax({
			url:"<?php echo base_url();?>lead/getLeadEnquiyStage",
			data:{status_id:status_id,sub_status_id:sub_status_id,lead_enquiry_status_id:lead_enquiry_status_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#lead_enquiry_status").html("<option value=''>Select lead enquiry status</option>"+res['option']);
						$("#lead_enquiry_status").selectpicker("refresh");
					}else{
						$("#lead_enquiry_status").html("<option value=''>lead enquiry status</option>");
						// $("#lead_enquiry_status").selectpicker("refresh");
					}
				}
				/* else{	
					$("#lead_enquiry_status").html("<option value=''>Select lead enquiry status</option>");
					$("#lead_enquiry_status").selectpicker("refresh");
				} */
			}
		});
	}
}

function getcity(state_id,city_id=null){
	if(state_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>lead/getCities",
			data:{state_id:state_id,city_id:city_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#city_id").html("<option value=''>Select City</option>"+res['option']);
						$("#city_id").selectpicker("refresh");
					}else{
						$("#city_id").html("<option value=''>No state found</option>");
						$("#city_id").selectpicker("refresh");
					}
				}else{	
					$("#city_id").html("<option value=''>Select</option>");
					$("#city_id").selectpicker("refresh");
				}
			}
		});
	}
}
function getSubStatus(status_id,sub_status_id=null){
	if(status_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>lead/getSubStatus",
			data:{status_id:status_id,sub_status_id:sub_status_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						
						$("#lead_enquiry_status").html("<option value=''>Select lead enquiry status</option>");
						$("#lead_enquiry_status").selectpicker("refresh");
						$("#lead_sub_status").html("<option value=''>Select Sub status</option>"+res['option']);
						$("#lead_sub_status").selectpicker("refresh");
					}else{
						$("#lead_sub_status").html("<option value=''>No Sub status found</option>");
						$("#lead_sub_status").selectpicker("refresh");
					}
				}
				/* else{	
					$("#lead_sub_status").html("<option value=''>Select Sub status</option>");
					$("#lead_sub_status").selectpicker("refresh");
				} */
			}
		});
	}
}

function getCenters(zone_id,center_id = null)
	{
		//alert("Val: "+val);return false;
		if(zone_id != "" )
		{
			$.ajax({
				url:"<?php echo base_url();?>lead/getCenters",
				data:{zone_id:zone_id, center_id:center_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != "")
						{
							$("#center_id").html("<option value=''>Select</option>"+res['option']);
						}
						else
						{
							$("#center_id").html("<option value=''>Select</option>");
						}
					}
					else
					{	
						$("#center_id").html("<option value=''>Select</option>");
					}
				}
			});
		}
	}



var vRules = {
	//student_first_name:{required:true},
	//student_last_name:{required:true},
	//gender:{required:true},
	lead_status :{required:true},
	lead_program :{required:true},
	planned_status:{required:true},
    country_id:{required:true},
    //state_id:{required:true},
    //city_id:{required:true},
	lead_status :{required:true},
	lead_sub_status :{required:true},
	lead_enquiry_status:{required :true},
	planned_status:{required:true},
	follow_up:{required:true}
};
var vMessages = {
	student_first_name:{required:"please enter first name"},
	student_last_name:{required:"please enter last name"},
	gender:{required:"Please select the gender "},
	country_id:{required:"select country "},
	lead_status :{required:"select  lead status "},
	lead_program :{required:"select lead sub status"},
    state_id:{required:"select state "},
    city_id:{required:"select city "},
	lead_status :{required:"select  lead status "},
	lead_sub_status :{required:"select lead sub status"},
	planned_status:{required:"Select Planend Lead status"},
	lead_enquiry_status :{requred:"Select lead enquiy status"},
	follow_up:{required:"select follow up date"}
	
};



$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>lead/submitLeaddata";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>lead";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

document.title = "AddEdit - Center";
$(".datepicker").datepicker({
	format: 'dd-mm-yyyy',
	startDate: '+1d',
	// startDate: new Date()
});
$('#input_starttime').timepicker({
// 12 or 24 hour
twelvehour: true,
defaultTime: '08:00 AM'
});
</script>					
