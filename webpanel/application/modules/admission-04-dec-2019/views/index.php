<?php 
//session_start();
//print_r($_SESSION["webadmin"]);
//echo "<pre>";
//print_r($roles);exit;
?>
<!-- start: Content -->
<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1>Admission</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>admission">Admission</a></li>
        </ul>
      </div>
    </div> 
    <div class="card">    
    	<div class="page-title-border">
        	<div class="col-sm-12 col-md-12 left-button-top">
				<p>
					<a href="<?php  echo base_url();?>admission/addEdit" class="btn btn-primary icon-btn"><i class="fa fa-plus"></i>Add Admission</a>	
				</p>		
            <div class="clearfix"></div>
            </div>
        </div>   
		
        <div class="col-sm-12" style="clear: both">
        	<form name="filter_form" method="post" id="filter_form">
	         	<div class="box-content form-horizontal product-filter">            	
	            	
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="firstname" class="control-label">First Name</label>
							<input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="lastname" class="control-label">Last Name</label>
							<input id="sSearch_1" name="sSearch_1" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="enrollment" class="control-label">Enrollment No</label>
							<input id="sSearch_2" name="sSearch_2" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="zonename" class="control-label">Zone</label>
							<input id="sSearch_3" name="sSearch_3" type="text" class="searchInput form-control"/>
						</div>
					</div>

					<div class="col-sm-2 col-xs-12">
						<div class="dataTables_filter searchFilterClass form-group">
							<label for="centername" class="control-label">Center</label>
							<input id="sSearch_4" name="sSearch_4" type="text" class="searchInput form-control"/>
						</div>
					</div>
					
					
					<div class="control-group clearFilter">
						<div class="controls">
							<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
							<a class="export_admission"><button class="btn" style="margin:32px 10px 10px 10px;">Export Data</button> </a>
						</div>
					</div>
					
					
	            </div>
	        </form>
         </div>
		 
		 
         <div class="clearfix"></div>
         <div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                    <table class="dynamicTable display table table-bordered non-bootstrap" >
                        <thead>
                          <tr>
							<th>First Name</th>
							<th>Last Name</th>
							<th>Enrollment No</th>
							<th>Zone</th>
							<th>Center</th>
							<th data-bSortable="false">View Payment Details</th>
							<th data-bSortable="false">Action</th>
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
    </div>
</div><!-- end: Content -->
			
<script>
	$( document ).ready(function() {

		clearSearchFilters();
	});	
	
	$(".export_admission").on("click", function()
	{
		console.log('hi')
		var act = "<?php  echo base_url();?>admission/export";
		$("#filter_form").attr("action",act);
		$("#filter_form").submit();
	});

	$("#filter_form").on("submit", function()
	{
		$('.export_admission').removeAttr("disabled");
		
		$('.export_contactus_sizes').removeAttr("disabled");
		
		setTimeout("location.reload(true);",1000);
	});

    <?php
	if(isset($_SESSION['admission_export_success']))
	{
		?>
		displayMsg("<?php echo $_SESSION['admission_export_success']; ?>", "<?php echo $_SESSION['admission_export_msg']; ?>");
		<?php
		unset($_SESSION['admission_export_success']);
		unset($_SESSION['admission_export_msg']);
	}
	?>
	<?php
	if(isset($_SESSION['student_id']))
	{
		unset($_SESSION['student_id']);
	}
	?>
	function deleteData1(id,status)
	{
		var sta="";
		var sta1=" ";
		
		if(status=='Active')
		{
		
			sta="In-active";
			
		}
		else{
			
			sta="Active";
		}
		
		
		
    	var r=confirm("Are you sure to " +sta);
    	if (r==true)
   		{
    		//window.location.href="users/delete?id="+id;
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/delrecord12/",
				data:{"id":id,"status":sta},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Updated!");
						setTimeout("location.reload(true);",1000);
						
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});
    	}
    }


	document.title = "Admission";
</script>