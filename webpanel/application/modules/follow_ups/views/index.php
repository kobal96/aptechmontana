<?php 
//session_start();
//print_r($_SESSION["webadmin"]);
//echo "<pre>";
//print_r($roles);exit;
?>
<!-- start: Content -->
<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1>Follow-ups</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>home">Follow-Up Details</a></li>
        </ul>
      </div>
    </div> 
    <div class="card">
    	<div class="page-title-border">
        	<div class="col-sm-12 col-md-12 left-button-top">
				<h3>Follow-Up Details</h3>
            <div class="clearfix"></div>
            </div>
        </div>     
		
        <div class="col-sm-12" style="clear: both">
            <div class="box-content form-horizontal product-filter">    
                <div class="col-sm-2 col-xs-12">
                    <div class="dataTables_filter searchFilterClass form-group">
                        <label for="Zone" class="control-label">Zone</label>
                        <select name="sSearch_0" id="sSearch_0" class="searchInput form-control">
                            <option value="">Select Zone</option>
                            <?php 
                                if(!empty($zoneDetails)){
                                    foreach($zoneDetails as $key=>$val){
                            ?>
                                <option value="<?php echo $val['zone_id']; ?>"><?php echo $val['zone_name'];?></option>
                            <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-12">
                    <div class="dataTables_filter searchFilterClass form-group">
                        <label for="Center" class="control-label">Center</label>
                        <select name="sSearch_1" id="sSearch_1" class="searchInput form-control" >
                            <option value="">Select Center</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3 col-xs-12">
                    <div class="dataTables_filter searchFilterClass form-group">
                        <label for="date_range" class="control-label">Follow-ups Schedules</label>
                        <input id="sSearch_2" name="sSearch_2" type="text" class="searchInput form-control"/>
                    </div>
                </div>

                <div class="col-sm-2 col-xs-12">
                    <div class="dataTables_filter searchFilterClass form-group">
                        <label for="brandname" class="control-label">Data Type</label>
                        <select name="sSearch_3" id="sSearch_3" class="searchInput form-control">
                        <option value="">All</option>
                        <option value="1">lead</option>
                        <option value="2">enquiry</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-3 col-xs-12">
                    <div class="dataTables_filter searchFilterClass form-group">
                    <?php $lead_type = array('Direct Walk-ins', 'Incoming Phone Calls', 'Database Tele-calling','Incoming Email/Message','Social Media Campaign','Google Ad wards');?>
                        <label for="brandname" class="control-label">Lead Category</label>
                        <select name="sSearch_4" id="sSearch_4" class="searchInput form-control">
                        <option value="">Select Lead Category</option>
                        <?php 
                            foreach ($lead_type as $key => $value) {?>
                            <option value="<?=$value?>"><?=$value?></option>
                        <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-3 col-xs-12">
                    <?php $knowaboutus = array('Montana Website','Facebook','Instagram','Google Ad-wards', 'Google Local Business', 'Just Dial','Newspaper Ads','Leaflet Inserts','Leaflet Distribution','Posters','Banners','Hoarding','Platform Boards','Bus Shelter Displays','Database Tele-calling','School Sign Boards','Existing Parent Reference','Pass out’s Parent Reference','Friends/Relatives Reference','Staff Reference','Others');?>
                    <label class="control-label" for="know_about_us">Lead Source</label>
                    <div class="dataTables_filter searchFilterClass form-group">
                        <select id="sSearch_5" name="sSearch_5" class="searchInput form-control">
                                    <option value="">Select Lead Source</option>
                                <?php 
                                    foreach ($knowaboutus as $key => $value) {?>
                                    <option value="<?=$value?>"><?=$value?></option>
                                <?php } ?>		
                        </select>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="dataTables_filter searchFilterClass form-group">
                        <label class="control-label" for="lead_status">Status</label>
                        <select id="sSearch_6" name="sSearch_6" class="searchInput form-control">
                            <option value="">Select Status </option>
                            <?php 
                                foreach ($status as $key => $value) { ?>
                                    <option value="<?=$value['lead_status_id']?>"><?=$value['status_description']?></option>
                            <?php } ?>		
                        </select>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-12">
                    <div class="dataTables_filter searchFilterClass form-group">
                        <label for="firstname" class="control-label"> LD No. / Student/Parent Name</label>
                        <input id="sSearch_7" name="sSearch_7" type="text" class="searchInput form-control" placeholder="LD No. / Student/Parent Name "/>
                    </div>
                </div>
                <div class="control-group clearFilter">
                    <div class="controls">
                        <a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
                    </div>
                </div>
                <input type="hidden" name="over_duedate" id="over_duedate" value="<?=date('d/m/Y', strtotime($overduedate[0]['follow_up_date']))?>">
            </div>
        </div>

    
         <div class="clearfix"></div>
         <div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                    <table class="dynamicTable display table table-bordered non-bootstrap" >
                        <thead>
                          <tr>
                            <th data-bSortable="false">Sr No</th>
                            <th>Follow-up date</th>
							<th data-bSortable="false">Over Due</th>
                            <th>Data Type </th>
                            <th>Zone</th>
							<th>Center</th>
                            <th>Parent Name & Number</th>
                            <th>Student Name</th>
                            <th>Lead Category </th>
							<th>Source</th>
							<th>Status</th>
                            <th>Sub-Status</th>
							<th>Date & Time (Stamp)</th>
                            <th data-bSortable="false">Action</th>
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
    </div>
</div><!-- end: Content -->
			
<script>
$( document ).ready(function() {

clearSearchFilters();
});	

$( document ).ready(function() {
clearSearchFilters();
var start = moment().subtract(29, 'days');
var end = moment();
var overduedate = $('#over_duedate').val();
// alert(overduedate);
function cb(start, end) {
$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
}
$('#sSearch_2').daterangepicker({
startDate:new Date(),
ranges: {
'Overdue': [overduedate,moment().subtract(1, 'days')],
'Today`s': [moment(), moment()],
'Tomorrows': [moment().add(1, 'days'), moment().add(1, 'days')],
// 'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
'Last 7 Days': [moment().subtract(6, 'days'), moment()],
// 'Last 30 Days': [moment().subtract(29, 'days'), moment()],
// 'This Month': [moment().startOf('month'), moment().endOf('month')],
// 'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]

},
locale:{
format:'DD/MM/YYYY'
},
},cb);

cb(start, end);
});

$(document).on("change",".searchInput",function(){
		var element = $(this).attr("id");
		var functionName = "";
		var column_name = "";
		var id =  "";
		if(element == "sSearch_0" || element == "sSearch_1"){
			if(element == "sSearch_0"){
				column_name = "zone_id";
				id =  $("#sSearch_0").val();
				$("#sSearch_1").val("");
			}else{
				column_name = "center_id";
				id =  $("#sSearch_1").val();
			}
			if(id != ""){
				$.ajax({
					url: "<?php echo base_url().$this->router->fetch_module();?>/getSearchLists",
					data:{"column_name":column_name,"value":id},
					async: false,
					type: "POST",
					dataType:"JSON",
					success: function(response){
						if(response.status){
							if(element == "sSearch_0"){
								$("#sSearch_1").html(response.option);
							}else{
								$("#sSearch_2").html(response.option);
							}
						}
					}
				});
			}else{
				if(element == "sSearch_0"){
					$("#sSearch_1").html("<option value=''>Select Center</option>");
				}
			}
		}
	})
	document.title = "Follow-ups";
</script>