<?PHP
class Follow_upsmodel extends CI_Model
{
	function getRecords($get=null){
		// echo "<pre>";print_r($get);exit;
		$table = "tbl_lead_enquiry_log";
		$table_id = 'i.ID';
		$default_sort_column = 'i.ID';
		$default_sort_order = 'desc';
		$condition = "";
		$condition = "";
		$condition .= "  1=1 ";
		
		$colArray = array('','i.follow_up_date','','i.type','le.lead_type','z.zone_name','c.center_name','le.father_name','le.student_first_name,le.student_last_name','le.lead_category','le.know_about_us','le.lead_status_id','le.lead_sub_status_id','i.created_on');
		$searchArray = array('i.type','le.lead_type','i.follow_up_date','le.status');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<7;$i++)
		{
			
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				if(isset($get['Searchkey_2']) && $get['Searchkey_2']!='' && $i==2){
					$date_here = explode(' - ', $get['Searchkey_2']);
					$changefor1 = explode('/', $date_here[0]);
					$changefor2 = explode('/', $date_here[1]);
					$cangefor1for = $changefor1[2] . "-" . $changefor1[1] . "-" . $changefor1[0];
					$cangefor2for = $changefor2[2] . "-" . $changefor2[1] . "-" . $changefor2[0];
					$condition .= " AND  i.follow_up_date >= '" .date("Y-m-d", strtotime($cangefor1for)). "' AND i.follow_up_date <='" . date('Y-m-d', strtotime($cangefor2for)) ."'";
				}
				if(isset($get['sSearch_0']) && $get['sSearch_0']!=''){
					$condition .= " AND z.zone_id ='".$_GET['sSearch_0']."'";
				}
				if(isset($get['sSearch_1']) && $get['sSearch_1']!=''){
					$condition .= " && c.center_id ='".$_GET['sSearch_1']."'";
				}
				if(isset($get['sSearch_3']) && $get['sSearch_3']!=''){
					$condition .= " && le.type = '".$_GET['sSearch_3']."' ";
				}
				if(isset($get['sSearch_4']) && $get['sSearch_4']!=''){
					$condition .= " && le.lead_category LIKE '%".$_GET['sSearch_4']."%' ";
				}
				if(isset($get['sSearch_5']) && $get['sSearch_5']!=''){
					$condition .= " && le.know_about_us LIKE '%".$_GET['sSearch_5']."%' ";
				}
				if(isset($get['sSearch_6']) && $get['sSearch_6']!=''){
					$condition .= " && le.lead_status_id = '".$_GET['sSearch_6']."'";
				}
				if(isset($get['sSearch_7']) && $get['sSearch_7']!=''){
					$condition .= " && (le.inquiry_master_id = '".$_GET['sSearch_7']."' || le.student_first_name LIKE '%".$_GET['sSearch_7']."%' || le.student_last_name LIKE '%".$_GET['sSearch_7']."%' || le.father_name LIKE '%".$_GET['sSearch_7']."%')";
				}
				// if($i==0 || $i==1 ||$i==3){
				// 	$condition .= " && $searchArray[$i] like '%".$get['sSearch_'.$i]."%'";
				// }
				// echo $condition;
			}
			
		}
		// echo $condition;exit;
		
		$this -> db -> select('i.*,CONCAT(le.student_first_name, " ",le.student_last_name) as name ,le.father_emailid,le.rec_type,le.father_contact_no,le.father_contact_no2,le.know_about_us,ls.status_description,lss.status_sub_description,z.zone_name, c.center_name,le.father_name,le.lead_category');
		$this -> db -> from('tbl_lead_enquiry_log as i');
		$this -> db -> join('tbl_inquiry_master as le', 'le.inquiry_master_id  = i.lead_id', 'inner');
		$this -> db -> join('tbl_lead_status as ls', 'i.lead_status_id  = ls.lead_status_id', 'left');
		$this -> db -> join('tbl_lead_sub_status as lss', 'i.lead_sub_status_id  = lss.lead_sub_status', 'left');
		$this -> db -> join('tbl_zones as z', 'le.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'le.center_id  = c.center_id', 'left');
		$this->db->where("($condition)");
		$this->db->group_by('i.lead_id');
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		// echo $this->db->last_query();exit;
		$this -> db -> select('i.*,CONCAT(le.student_first_name, " ",le.student_last_name) as name ,le.father_emailid,le.rec_type,le.father_contact_no,le.father_contact_no2,le.know_about_us,ls.status_description,lss.status_sub_description,z.zone_name, c.center_name,le.father_name,le.lead_category');
		$this -> db -> from('tbl_lead_enquiry_log as i');
		$this -> db -> join('tbl_inquiry_master as le', 'le.inquiry_master_id  = i.lead_id', 'inner');
		$this -> db -> join('tbl_lead_status as ls', 'i.lead_status_id  = ls.lead_status_id', 'left');
		$this -> db -> join('tbl_lead_sub_status as lss', 'i.lead_sub_status_id  = lss.lead_sub_status', 'left');
		$this -> db -> join('tbl_zones as z', 'le.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'le.center_id  = c.center_id', 'left');
		$this->db->where("($condition)");
		$this->db->group_by('i.lead_id');
		$this->db->order_by($sort, $order);		
		$query1 = $this -> db -> get();

		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			return array("totalRecords"=>0);
		}
		
		exit;
	}
	function getDataCustom($table,$fields,$condition){
		$this -> db -> select($fields);
		$this -> db -> from($table);
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	function getCenterMappedCourses($center_id){
		$this -> db -> select('tc.course_id,tc.course_name');
		$this -> db -> from('tbl_center_courses as cc');
		$this -> db -> join('tbl_courses as tc', 'tc.course_id  = cc.course_id', 'left');
		$condition = array("cc.center_id"=>$center_id);
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	function overduedate(){
		$condition ="le.no_further_follow_up ='0' AND follow_up_date !='' ";
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_lead_enquiry_log as i');
		$this -> db -> join('tbl_inquiry_master as le', 'le.inquiry_master_id  = i.lead_id', 'inner');
		$this->db->where("($condition)");
		$this->db->group_by('i.lead_id');
		$this->db->order_by('i.ID', 'ASC');
		$this->db->limit('1');		
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
 

}
?>
