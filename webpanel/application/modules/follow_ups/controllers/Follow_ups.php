<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Follow_ups extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('follow_upsmodel','',TRUE);
 }
 
 function index()
 {
//   echo '<pre>';
//   print_r($_SESSION["webadmin"]);
//   exit;
   if(!empty($_SESSION["webadmin"]))
	{   
		$this->load->view('template/header.php');
		$filterData = array();
		$filterData['zoneDetails'] = $this->follow_upsmodel->getDataCustom("tbl_zones","zone_id,zone_name",array("status"=>'Active'));
		$filterData['status'] = $this->follow_upsmodel->getDataCustom("tbl_lead_status", "*","status = 'Active' ");
		$condition ="";
		$filterData['overduedate'] = $this->follow_upsmodel->overduedate();
		// echo "<pre>";print_r($filterData['overduedate'][0]['follow_up_date']);exit;
		$this->load->view('follow_ups/index',$filterData);
		$this->load->view('template/footer.php');
	}
	else
	{
		//If no session, redirect to login page
		redirect('login', 'refresh');
	}
 }
 
 function fetch($id=null)
 {
	 $get_result = $this->follow_upsmodel->getRecords($_GET);
	 $result = array();
	 $result["sEcho"] = $_GET['sEcho'];
	 
	 $result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
	 $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
	 $items = array();
	 if(!empty($get_result['query_result'])){
		 for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
			$now = strtotime(date('Y-m-d'));
			$followup_date = strtotime($get_result['query_result'][$i]->follow_up_date);
			$datediff = $now - $followup_date;
			 $temp = array();
			 array_push($temp, $i+1);
			 array_push($temp, $get_result['query_result'][$i]->follow_up_date);
			 array_push($temp, round($datediff / (60 * 60 * 24)).' '.(round($datediff / (60 * 60 * 24))==1 ?'day' :'days') );
			 array_push($temp, ($get_result['query_result'][$i]->type == '1' ? 'lead' :'Inquiry'));
			 array_push($temp, $get_result['query_result'][$i]->zone_name);
			 array_push($temp, $get_result['query_result'][$i]->center_name);
			 array_push($temp, $get_result['query_result'][$i]->father_name.'<br>'.$get_result['query_result'][$i]->father_contact_no.'<br>'.$get_result['query_result'][$i]->father_contact_no2);
			 array_push($temp, $get_result['query_result'][$i]->name);
			 array_push($temp, $get_result['query_result'][$i]->lead_category);
			 
			 array_push($temp, $get_result['query_result'][$i]->know_about_us);
			 array_push($temp, $get_result['query_result'][$i]->status_description);
			 array_push($temp, $get_result['query_result'][$i]->status_sub_description);
			 array_push($temp, $get_result['query_result'][$i]->created_on);
			 
			
			 $actionCol1 = "";
			 if($get_result['query_result'][$i]->type != '1'){
				 $actionCol1.= '<a class="btn btn-primary" href="inquiries/addEditFollow?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->lead_id) , '+/', '-_') , '=') . '" title="Follow Up"><i class="fa fa-arrow-up"></i></a> ';
			 }else{
				$actionCol1.= '<a class="btn btn-primary" href="lead/addEditFollow?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->lead_id) , '+/', '-_') , '=') . '" title="Follow Up"><i class="fa fa-arrow-up"></i></a> ';
			 }	
			 
			//  array_push($temp, $actionCol21);
			 array_push($temp, $actionCol1);
			 
			 
			 array_push($items, $temp);
		 }
	 }	

	 $result["aaData"] = $items;
	 echo json_encode($result);
	 exit;
 }
 function getSearchLists(){
	if(!empty($_POST['value']) && !empty($_POST['column_name'])){
		$options = "";
		if($_POST['column_name']  == "zone_id"){
			$centerDetails = $this->follow_upsmodel->getDataCustom("tbl_centers","center_id,center_name",array("status"=>'Active',"zone_id"=>$_POST['value']));
			$options = "<option value=''>Select Center</option>";
			if(!empty($centerDetails)){
				foreach($centerDetails as $key=>$val){
					$options .= "<option value='".$val['center_id']."'>".$val['center_name']."</option>"; 
				}
			}
			echo json_encode(array("status"=>true,"option"=>$options));
			exit;
		}else{
			$courseDetails = $this->follow_upsmodel->getCenterMappedCourses($_POST['value']);
			$options = " <option value=''>Select Class</option>";
			if(!empty($courseDetails)){
				foreach($courseDetails as $key=>$val){
					$options .= "<option value='".$val['course_id']."'>".$val['course_name']."</option>"; 
				}
			}
			echo json_encode(array("status"=>true,"option"=>$options));
			exit;
		}
	}
}

}

?>
