<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Appointment extends CI_Controller{

	function __construct(){
		parent::__construct();
		$this->load->model('appointmentmodel', '', TRUE);
	}

	function index(){
		if (!empty($_SESSION["webadmin"])) {
			$filterData = array();
			$filterData['zoneDetails'] = $this->appointmentmodel->getData("tbl_zones","zone_id,zone_name","status='Active'");
			$this->load->view('template/header.php');
			$this->load->view('index',$filterData );
			$this->load->view('template/footer.php');
		}else {
			redirect('login', 'refresh');
		}
	}

	function fetch(){
		$get_result = $this->appointmentmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);
				array_push($temp, $get_result['query_result'][$i]->enrollment_no);
				array_push($temp, $get_result['query_result'][$i]->student_first_name." ".$get_result['query_result'][$i]->student_last_name);
				array_push($temp, date("d-m-Y", strtotime($get_result['query_result'][$i]->appointment_date)));
				array_push($temp,$get_result['query_result'][$i]->appointment_message);
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}

	function getSearchLists(){
		if(!empty($_POST['value']) && !empty($_POST['column_name'])){
			$options = "";
			if($_POST['column_name']  == "zone_id"){
				$centerDetails = $this->appointmentmodel->getData("tbl_centers","center_id,center_name","status='Active' AND zone_id='".$_POST['value']."' ");
				$options = "<option value=''>Select Center</option>";
				if(!empty($centerDetails)){
					foreach($centerDetails as $key=>$val){
						$options .= "<option value='".$val['center_id']."'>".$val['center_name']."</option>"; 
					}
				}
				echo json_encode(array("status"=>true,"option"=>$options));
				exit;
			}else{
				$courseDetails = $this->appointmentmodel->getCenterMappedCourses($_POST['value']);
				$options = " <option value=''>Select Class</option>";
				if(!empty($courseDetails)){
					foreach($courseDetails as $key=>$val){
						$options .= "<option value='".$val['course_id']."'>".$val['course_name']."</option>"; 
					}
				}
				echo json_encode(array("status"=>true,"option"=>$options));
				exit;
			}
		}
	}
}

?>
