
<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1>Appointment</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>appointment">Appointment</a></li>
        </ul>
      </div>
    </div> 
    <div class="card">
        <div class="col-sm-12" style="clear: both">
         	<div class="box-content form-horizontal product-filter">            	
			 	<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="Zone" class="control-label">Zone</label>
						<select name="sSearch_0" id="sSearch_0" class="searchInput form-control">
							<option value="">Select Zone</option>
							<?php 
								if(!empty($zoneDetails)){
									foreach($zoneDetails as $key=>$val){
							?>
								<option value="<?php echo $val['zone_id']; ?>"><?php echo $val['zone_name'];?></option>
							<?php
									}
								}
							?>
						</select>
					</div>
				</div>
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="Center" class="control-label">Center</label>
						<select name="sSearch_1" id="sSearch_1" class="searchInput form-control" >
							<option value="">Select Center</option>
						</select>
					</div>
				</div>
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Student Name</label>
						<input id="sSearch_2" name="sSearch_2" type="text" class="searchInput form-control" placeholder="Student Name "/>
					</div>
				</div>
				<div class="control-group clearFilter">
					<div class="controls">
						<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
					</div>
				</div>
            </div>
		</div>
		 
		<div class="clearfix"></div>
		<div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                    <table class="dynamicTable display table table-bordered non-bootstrap">
                        <thead>
						<tr>
							<th>Zone</th>
							<th>Center</th>
							<th>ENR No.</th>
							<th>Student Name</th>
							<th>Appointment Date</th>
							<th>Appointment Message</th>
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
		</div>
		<div class="clearfix"></div>
    </div>
</div><!-- end: Content -->
<script>
	$(document).on("change",".searchInput",function(){
		var element = $(this).attr("id");
		var functionName = "";
		var column_name = "";
		var id =  "";
		if(element == "sSearch_0" || element == "sSearch_1"){
			if(element == "sSearch_0"){
				column_name = "zone_id";
				id =  $("#sSearch_0").val();
				$("#sSearch_1").val("");
			}else{
				column_name = "center_id";
				id =  $("#sSearch_1").val();
			}
			if(id != ""){
				$.ajax({
					url: "<?php echo base_url().$this->router->fetch_module();?>/getSearchLists",
					data:{"column_name":column_name,"value":id},
					async: false,
					type: "POST",
					dataType:"JSON",
					success: function(response){
						if(response.status){
							if(element == "sSearch_0"){
								$("#sSearch_1").html(response.option);
							}else{
								$("#sSearch_2").html(response.option);
							}
						}
					}
				});
			}else{
				if(element == "sSearch_0"){
					$("#sSearch_1").html("<option value=''>Select Center</option>");
				}
			}
		}
	})

	document.title = "Apointment";
</script>