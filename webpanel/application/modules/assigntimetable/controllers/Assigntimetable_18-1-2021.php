<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Assigntimetable extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('assigntimetablemodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$result['academicyear'] = $this->assigntimetablemodel->getDropdown("tbl_academic_year_master","*");
			// print_r($result);
			// exit;
			$this->load->view('template/header.php');
			$this->load->view('assigntimetable/index',$result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['academicyear'] = $this->assigntimetablemodel->getDropdown("tbl_academic_year_master","*");
			$result['categories'] = $this->assigntimetablemodel->getDropdown("tbl_categories","category_id,categoy_name");
			$result['zones'] = $this->assigntimetablemodel->getDropdown("tbl_zones","zone_id,zone_name");
			
			$result['details'] = $this->assigntimetablemodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('assigntimetable/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function zoneWiseModify($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			
			$result = "";
			$result['categories'] = $this->assigntimetablemodel->getDropdown("tbl_categories","category_id,categoy_name");
			$result['zones'] = $this->assigntimetablemodel->getDropdown("tbl_zones","zone_id,zone_name");
			
			$this->load->view('template/header.php');
			$this->load->view('assigntimetable/zoneWiseModify', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function getCourses(){
		$result = $this->assigntimetablemodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['course_id']) && !empty($_REQUEST['course_id'])){
			$course_id = $_REQUEST['course_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getThemes(){
		$result = $this->assigntimetablemodel->getOptions("tbl_themes",$_REQUEST['course_id'],"course_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$theme_id = '';
		
		if(isset($_REQUEST['theme_id']) && !empty($_REQUEST['theme_id'])){
			$theme_id = $_REQUEST['theme_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->theme_id == $theme_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->theme_id.'" '.$sel.' >'.$result[$i]->theme_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getWeeks(){
		$result = $this->assigntimetablemodel->getOptions("tbl_weeks",$_REQUEST['theme_id'],"theme_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$week_id = '';
		
		if(isset($_REQUEST['week_id']) && !empty($_REQUEST['week_id'])){
			$week_id = $_REQUEST['week_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->week_id == $week_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->week_id.'" '.$sel.' >'.$result[$i]->week_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getTimetable(){
		$result = $this->assigntimetablemodel->getOptions("tbl_timetable_master",$_REQUEST['theme_id'],"theme_id");
		//echo "<pre>";
		//print_r($result);
		//exit;
		
		$option = '';
		$timetable_id = '';
		
		if(isset($_REQUEST['timetable_id']) && !empty($_REQUEST['timetable_id'])){
			$timetable_id = $_REQUEST['timetable_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->timetable_id == $timetable_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->timetable_id.'" '.$sel.' >'.$result[$i]->timetable_title.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getCenters(){
		$result = $this->assigntimetablemodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		//echo "<pre>";
		//print_r($result);
		//exit;
		
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->center_id == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function submitForm()
	{ 
		// echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			if(empty($_POST['assign_timetable_id'])){
			
				if(empty($_POST['center_id'])){
					echo json_encode(array("success"=>"0",'msg'=>'Please select center!'));
					exit;
				}
				
				if(!empty($_POST['center_id'])){
					for($i=0; $i < sizeof($_POST['center_id']); $i++){
						$condition = " academic_year_id = ".$_POST['academic_year_id']."  && category_id='".$_POST['category_id']."' && course_id='".$_POST['course_id']."' &&  theme_id='".$_POST['theme_id']."' && timetable_id= '".$_POST['timetable_id']."' && center_id='".$_POST['center_id'][$i]."'  ";
						
						if(isset($_POST['assign_timetable_id']) && $_POST['assign_timetable_id'] > 0){
							$condition .= " &&  center_id != ".$_POST['center_id'];
						}
						
						$check_name = $this->assigntimetablemodel->getdata("tbl_assign_timetable",$condition);
						//echo "<pre>";print_r($check_name);exit;
						
						if(!empty($check_name)){
							$getCenterName = $this->assigntimetablemodel->getdata("tbl_centers", "center_id='".$_POST['center_id'][$i]."' ");
							echo json_encode(array("success"=>"0",'msg'=>' '.$getCenterName[0]['center_name'].' Already assign to selected timetable!'));
							exit;
						}
						
					}
				}
				
			}
			
			//exit;
			if (!empty($_POST['assign_timetable_id'])) {
				$data_array = array();			
				$assign_timetable_id = $_POST['assign_timetable_id'];
		 		
				$data_array['start_date'] = (!empty($_POST['start_date'])) ? date("Y-m-d", strtotime($_POST['start_date'])) : '';
				$data_array['end_date'] = (!empty($_POST['end_date'])) ? date("Y-m-d", strtotime($_POST['end_date'])) : '';
				
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->assigntimetablemodel->updateRecord('tbl_assign_timetable', $data_array,'assign_timetable_id',$assign_timetable_id);
				
				
			}else {
				
				$data_array = array();
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
				$data_array['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : '';
				$data_array['theme_id'] = (!empty($_POST['theme_id'])) ? $_POST['theme_id'] : '';
				//$data_array['week_id'] = (!empty($_POST['week_id'])) ? $_POST['week_id'] : '';
				$data_array['timetable_id'] = (!empty($_POST['timetable_id'])) ? $_POST['timetable_id'] : '';
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				
				$data_array['start_date'] = (!empty($_POST['start_date'])) ? date("Y-m-d", strtotime($_POST['start_date'])) : '';
				$data_array['end_date'] = (!empty($_POST['end_date'])) ? date("Y-m-d", strtotime($_POST['end_date'])) : '';
				
				$data_array['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				//echo "<pre>";print_r($data_array);exit;
				
				if(!empty($_POST['center_id'])){
					for($i=0; $i < sizeof($_POST['center_id']); $i++){
						$data_array['center_id'] = (!empty($_POST['center_id'][$i])) ? $_POST['center_id'][$i] : '';
						$result = $this->assigntimetablemodel->insertData('tbl_assign_timetable', $data_array, '1');
						
					}
				}
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	function modifyZoneAssignment()
	{ 
		//echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			$condition = "category_id='".$_POST['category_id']."' && course_id='".$_POST['course_id']."' &&  theme_id='".$_POST['theme_id']."' && timetable_id= '".$_POST['timetable_id']."' ";
				
			$check_name = $this->assigntimetablemodel->getdata("tbl_assign_timetable",$condition);
			
			if(empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>' No timetable assigned!'));
				exit;
			}
			
			//exit;
			
			$data_array = array();
		 		
			$data_array['start_date'] = (!empty($_POST['start_date'])) ? date("Y-m-d", strtotime($_POST['start_date'])) : '';
			$data_array['end_date'] = (!empty($_POST['end_date'])) ? date("Y-m-d", strtotime($_POST['end_date'])) : '';
			
			$data_array['updated_on'] = date("Y-m-d H:i:s");
			$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
			
			$update_condtion = "category_id='".$_POST['category_id']."' && course_id='".$_POST['course_id']."' &&  theme_id='".$_POST['theme_id']."' && timetable_id= '".$_POST['timetable_id']."' && zone_id='".$_POST['zone_id']."' ";
			
			$result = $this->assigntimetablemodel->updateRecordCondition('tbl_assign_timetable', $data_array,$update_condtion);
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		//echo $id;exit;
		$get_result = $this->assigntimetablemodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->academic_year_master_name);
				array_push($temp, $get_result['query_result'][$i]->categoy_name);
				array_push($temp, $get_result['query_result'][$i]->course_name);
				array_push($temp, $get_result['query_result'][$i]->theme_name);
				//array_push($temp, $get_result['query_result'][$i]->week_name);
				array_push($temp, $get_result['query_result'][$i]->timetable_title);
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);
				array_push($temp, $get_result['query_result'][$i]->start_date);
				array_push($temp, $get_result['query_result'][$i]->end_date);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("WeekAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->assign_timetable_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("WeekAddEdit")){
					$actionCol1.= '<a href="assigntimetable/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->assign_timetable_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function changeapproval(){
		$result = "";
		$condition = "1=1 && course_id='".$_GET['id']."' ";
		$result['details'] = $this->assigntimetablemodel->getdata('tbl_courses',$condition);
		//echo "<pre>";print_r($result["details"]);exit;
		$view = $this->load->view('routinesactions/changeapproval',$result,true);
		echo $view;
	}
	
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->assigntimetablemodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('coursesmaster/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	function delRecord()
	{
		
		$id=$_POST['id'];
		
		$appdResult = $this->assigntimetablemodel->delrecord1("tbl_timetable_videos","timetable_video_id",$id);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delDocRecord()
	{
		
		$id=$_POST['id'];
		$get_name = $this->assigntimetablemodel->getdata("tbl_timetable_documents","timetable_document_id='".$id."' ");
		
		if(!empty($get_name[0]['doc_file'])){
			$del_path = DOC_ROOT."/images/timetable_images/".$get_name[0]['doc_file'];
			if(file_exists($del_path)){
				@unlink($del_path);
			}
		}
		
		$appdResult = $this->assigntimetablemodel->delrecord1("tbl_timetable_documents","timetable_document_id",$id);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->assigntimetablemodel->delrecord12("tbl_assign_timetable","assign_timetable_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	private function set_upload_options()
	{   
		//upload an image options //products
		$config = array();
		$config['upload_path'] = "images/timetable_images/";
		$config['allowed_types'] = '*';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;

		return $config;
	}

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
