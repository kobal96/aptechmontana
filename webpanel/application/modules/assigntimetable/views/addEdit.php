<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Assign Timetable</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>assigntimetable">Assign Timetable</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="assign_timetable_id" name="assign_timetable_id" value="<?php if(!empty($details[0]->assign_timetable_id)){echo $details[0]->assign_timetable_id;}?>" />
									<div class="control-group form-group">
										<label for="Zone" class="control-label">Current Academy Year</label>
										<select name="academic_year_id" id="academic_year_id" class=" form-control">
											<option value="">Select Academy year</option>
											<?php if(!empty($academicyear)){
													foreach($academicyear as $key=>$val){
														$sel ='';
															if(!empty($details[0]->academic_year_id)){
																$sel =($details[0]->academic_year_id == $val->academic_year_master_id)?"selected":'';
															}
															?>
												<option value="<?php echo $val->academic_year_master_id; ?>" <?= $sel ?> ><?php echo $val->academic_year_master_name;?></option>
											<?php
													}
												}
											?>
										</select>
									</div>
								<div class="control-group form-group">
									<label class="control-label" for="category_id">Category*</label>
									<div class="controls">
										<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);" <?php if(!empty($details[0]->assign_timetable_id)){?> disabled <?php }?> >
											<option value="">Select Category</option>
											<?php 
												if(isset($categories) && !empty($categories)){
													foreach($categories as $cdrow){
														$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
											?>
												<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
											<?php }}?>
										</select>
									</div>
								</div>
							
								<div class="control-group form-group">
									<label class="control-label"><span>Course*</span></label> 
									<div class="controls">
										<select id="course_id" name="course_id" class="form-control" onchange="getThemes(this.value);" <?php if(!empty($details[0]->assign_timetable_id)){?> disabled <?php }?> >
											<option value="">Select Course</option>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Theme*</span></label> 
									<div class="controls">
										<select id="theme_id" name="theme_id" class="form-control" onchange="getTimetable(this.value);" <?php if(!empty($details[0]->assign_timetable_id)){?> disabled <?php }?> >
											<option value="">Select Theme</option>
										</select>
									</div>
								</div>
								
								<!--<div class="control-group form-group">
									<label class="control-label"><span>Week*</span></label> 
									<div class="controls">
										<select id="week_id" name="week_id" class="form-control"  onchange="getTimetable(this.value);" <?php if(!empty($details[0]->assign_timetable_id)){?> disabled <?php }?> >
											<option value="">Select Week</option>
										</select>
									</div>
								</div>-->
								
								<div class="control-group form-group">
									<label class="control-label"><span>Title*</span></label> 
									<div class="controls">
										<select id="timetable_id" name="timetable_id" class="form-control" <?php if(!empty($details[0]->assign_timetable_id)){?> disabled <?php }?> >
											<option value="">Select Title</option>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label" for="zone_id">Zone*</label>
									<div class="controls">
										<select id="zone_id" name="zone_id" class="form-control select2"  onchange="getCenters(this.value);" <?php if(!empty($details[0]->assign_timetable_id)){?> disabled <?php }?>>
											<option value="">Select Zone</option>
											<?php 
												if(isset($zones) && !empty($zones)){
													foreach($zones as $cdrow){
														$sel = ($cdrow->zone_id == $details[0]->zone_id) ? 'selected="selected"' : '';
											?>
												<option value="<?php echo $cdrow->zone_id;?>" <?php echo $sel; ?>><?php echo $cdrow->zone_name;?></option>
											<?php }}?>
										</select>
									</div>
								</div>
								
								<?php if(!empty($details[0]->center_id)){?>
									<div class="control-group form-group">
										<label class="control-label"><span>Center*</span></label> 
										<div class="controls">
											<select id="center_id" name="center_id" class="form-control" disabled >
												<option value="">Select Center</option>
											</select>
										</div>
									</div>
								<?php }else{?>
									<div class="control-group form-group">
										<label class="control-label"><span>Center*</span></label> 
										<input type="checkbox" id="checkbox" >Select All
										<div class="controls">
											<select id="center_id" name="center_id[]" class="form-control select2" multiple >
												<option value="">Select Center</option>
											</select>
										</div>
									</div>
								<?php }?>	
								
								<div class="control-group form-group">
									<label class="control-label"><span>Start Date*</span></label>
									<div class="controls">
										<input type="text" class="form-control required datepicker" placeholder="Select start date" id="start_date" name="start_date" value="<?php if(!empty($details[0]->start_date)){echo date("d-m-Y", strtotime($details[0]->start_date));}?>" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>End Date*</span></label>
									<div class="controls">
										<input type="text" class="form-control required datepicker" placeholder="Select end date" id="end_date" name="end_date" value="<?php if(!empty($details[0]->end_date)){echo date("d-m-Y", strtotime($details[0]->end_date));}?>" >
									</div>
								</div>
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>assigntimetable" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	
	$("#checkbox").click(function(){
		if($("#checkbox").is(':checked') ){
			$("#center_id > option").prop("selected","selected");
			$("#center_id").trigger("change");
		}else{
			$("#center_id > option").removeAttr("selected");
			$("#center_id").trigger("change");
		}
	});
	
	$(".datepicker").datetimepicker({
		format: 'DD-MM-YYYY'
	});
	
	<?php 
		if(!empty($details[0]->theme_id)){
	?>
		getCourses('<?php echo $details[0]->category_id; ?>', '<?php echo $details[0]->course_id; ?>');
		getThemes('<?php echo $details[0]->course_id; ?>', '<?php echo $details[0]->theme_id; ?>');
		//getWeeks('<?php echo $details[0]->theme_id; ?>', '<?php echo $details[0]->week_id; ?>');
		getTimetable('<?php echo $details[0]->theme_id; ?>', '<?php echo $details[0]->timetable_id; ?>');
		getCenters('<?php echo $details[0]->zone_id; ?>', '<?php echo $details[0]->center_id; ?>');
	<?php }?>
	
});

function getCourses(category_id,course_id = null)
{
	//alert("Val: "+val);return false;
	if(category_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>assigntimetable/getCourses",
			data:{category_id:category_id, course_id:course_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#course_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getThemes(course_id,theme_id = null)
{
	//alert("Val: "+val);return false;
	if(course_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>assigntimetable/getThemes",
			data:{course_id:course_id, theme_id:theme_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#theme_id").html("<option value=''>Select</option>"+res['option']);
						$("#theme_id").select2();
					}
					else
					{
						$("#theme_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#theme_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}


function getWeeks(theme_id,week_id = null)
{
	//alert("Val: "+val);return false;
	if(theme_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>assigntimetable/getWeeks",
			data:{theme_id:theme_id, week_id:week_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#week_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#week_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#week_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getTimetable(theme_id,timetable_id = null)
{
	//alert("Val: "+val);return false;
	if(theme_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>assigntimetable/getTimetable",
			data:{theme_id:theme_id, timetable_id:timetable_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#timetable_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#timetable_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#timetable_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getCenters(zone_id,center_id = null)
{
	//alert("Val: "+val);return false;
	if(zone_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>assigntimetable/getCenters",
			data:{zone_id:zone_id, center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						// $("#center_id").val('');
							// $("#center_id > option").removeAttr("selected");
							// $("#center_id").select2();
							$("#center_id").select2("val", "");
						  $("#checkbox").prop("checked", 0);
						$("#center_id").html(res['option']);
					}
					else
					{
						$("#center_id").html("");
					}
				}
				else
				{	
					$("#center_id").html("");
				}
			}
		});
	}
}

var vRules = {
	academic_year_id:{required:true},
	category_id:{required:true},
	course_id:{required:true},
	theme_id:{required:true},
	//week_id:{required:true},
	timetable_id:{required:true},
	zone_id:{required:true},
	start_date:{required:true},
	end_date:{required:true}
	
};
var vMessages = {
	academic_year_id : {required :"Please select the acdemic year "},
	category_id:{required:"Please select category."},
	course_id:{required:"Please select course."},
	theme_id:{required:"Please select theme."},
	//week_id:{required:"Please select week."},
	timetable_id:{required:"Please select timetable."},
	zone_id:{required:"Please select zone."},
	start_date:{required:"Please select start date."},
	end_date:{required:"Please select end date."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>assigntimetable/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>assigntimetable";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Assign Timetable";

 
</script>					
