<?PHP
class Homemodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	 {
	 	/*echo $tbl_name."<br/>";
	 	print_r($data_array);
	 	echo $sendid;
	 	exit;*/
	 	
	 	/*$this->db->where('UserId', $did);
     	$this->db->delete('test_user_roles');*/

     	/*$this->db->where('UserId', $did);
     	$this->db->delete('test_customer_warehouse_details');*/ 
	 	
	 	$this->db->insert($tbl_name,$data_array);
	 	$result_id = $this->db->insert_id();
	 	
	 	/*echo $result_id;
	 	exit;*/
	 	
	 	if($sendid == 1)
	 	{
	 		//return id
	 		return $result_id;
	 	}
	 	
	 	
	 	
	 }
	 
	 
	 /*function getstudentData(){
		
		$this -> db -> select('sm.*,sd.*');
		$this -> db -> from('tbl_student_master as sm');
		$this -> db -> join('tbl_student_details as sd', 'sm.student_id = sd.student_id ', 'inner');
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
		  return $query->result_array();
		}
		else
		{
		  return false;
		}
		 
	 }*/
	 
	 function getdata($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
       
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }
	
         function getActiveSessions(){
             $query = $this->db->query('Select count(*) as total From tbl_session where status="Active"');
             if($query->num_rows()>0){
                 return $query->result();
             }
             else{
                 return false;
             }
         }
         
         function getTotalOrders(){
             $query = $this->db->query('Select count(*) as total From tbl_orders where order_status="Complete" AND payment_status="Success"');
             if($query->num_rows()>0){
                 return $query->result();
             }
             else{
                 return false;
             }
         }
         
         function getTotalCourseSold(){
             $query = $this->db->query('Select count(Distinct product_id) as total From tbl_shopping_cart c where order_id IN (Select order_id From tbl_orders Where order_status="Complete" AND payment_status="Success") AND product_type = "course"');
             if($query->num_rows()>0){
                 return $query->result();
             }
             else{
                 return false;
             }
         }
         
         function getTotalCount($table){
             $query = $this->db->query('Select * From '.$table.' where status="Active"');
             if($query->num_rows()>0){
                 return $query->result();
             }
             else{
                 return false;
             }
         }
         
         function getTotalUser(){
             $query = $this->db->query('Select created_on From tbl_users where status="Active"');
             if($query->num_rows()>0){
                 return $query->result();
             }
             else{
                 return false;
             }
         }
         
         
         function get_ongoing_services($professional_id = null){
            $sql = $this->db->query('Select o.customer_id,c.baseprice,c.no_of_days,s.profservice_name,o.created_on,cus.customer_first_name,cus.customer_last_name,cus.customer_email	
                              From tbl_orders o
                              Left Join tbl_cart c on (c.order_id = o.order_id)
                              Left Join tbl_professional_price_details s on (s.professional_price_detail_id = c.professional_price_detail_id)
                              Left Join tbl_customers cus on (cus.customer_id = o.customer_id)
                              Where o.status = "Processed" And s.professional_id = '.$this->db->escape($professional_id).'
                              order by o.order_id desc
                              ');
            if($sql->num_rows() > 0){
                  return $sql->result_array();
              }else{
                  return false;
              }
          }
          
          
          function get_messages($from_id,$to_id){
            $sql = $this->db->query('Select *
                              From tbl_messages 
                              Where (from_id = '.$this->db->escape($from_id).' And to_id = '.$this->db->escape($to_id).') 
                              OR
                              (from_id = '.$this->db->escape($to_id).' And to_id = '.$this->db->escape($from_id).')
                              order by message_id desc
                              Limit 5
                              ');
            if($sql->num_rows() > 0){
                  return $sql->result_array();
              }else{
                  return false;
              }
          }
          
          function get_customer_of_unread_message($user_id){
            $sql = $this->db->query('Select *
                                     From tbl_messages
                                     Where ((from_id = '.$user_id.' And from_utype = "prof")
                                            OR (to_id = '.$user_id.' And to_utype = "prof"))
                                            And seen = "0"  
                                            
                              ');
            if($sql->num_rows() > 0){
                  return $sql->result_array();
              }else{
                  return false;
              }
          }
         
         
         function getTotalUsers(){
             $query = $this->db->query('Select count(*) as total FROM tbl_users WHERE status = "Active"');
             if($query->num_rows()>0){
                 return $query->result();
             }
             else{
                 return false;
             }
         }
         
         function getLatestOrders(){
             $query = $this->db->query('Select * FROM tbl_orders ORDER BY  `order_id` DESC Limit 4');
             if($query->num_rows()>0){
                 return $query->result();
             }
             else{
                 return false;
             }
         }
         
         function getUserDetails($user_ids){
             $query = $this->db->query('Select * FROM tbl_users where `user_id` IN('.$user_ids.')');
             if($query->num_rows()>0){
                 return $query->result();
             }
             else{
                 return false;
             }
         }
         
         function getLatestMembers($condition){
        	$this -> db -> select('i.*,ct.center_name,cr.course_name,');
			$this -> db -> from('tbl_student_master as i');
			$this -> db -> join('tbl_centers as ct', 'i.center_id  = ct.center_id', 'left');	
			$this -> db -> join('tbl_student_details as sd', 'i.student_id  = sd.student_id AND i.enrollment_no = sd.enrollment_no', 'left');	
			$this -> db -> join('tbl_courses as cr', 'sd.course_id  = cr.course_id', 'left');
			
    		$this->db->where("($condition)");
    		$this->db->order_by("student_id",'desc');
    		$this->db->group_by("i.student_id");
    		$this->db->limit(5);
    		$query = $this -> db -> get();
    	      
    		if($query -> num_rows() >= 1){
    		
    			return  $query->result_array();
    		}else{
    			return false;
    		}
         }
         
         function getLatestMembersByHo($condition){
        	$this -> db -> select('i.*,ct.center_name,cr.course_name,');
			$this -> db -> from('tbl_student_master as i');
			$this -> db -> join('tbl_admin_users as au', 'au.user_id  = i.created_by', 'left');	
			$this -> db -> join('tbl_centers as ct', 'i.center_id  = ct.center_id', 'left');	
			$this -> db -> join('tbl_student_details as sd', 'i.student_id  = sd.student_id AND i.enrollment_no = sd.enrollment_no', 'left');	
			$this -> db -> join('tbl_courses as cr', 'sd.course_id  = cr.course_id', 'left');
			
    		$this->db->where("($condition)");
    		$this->db->where("au.user_type",'1');
    		
    		$this->db->order_by("student_id",'desc');
    		$this->db->group_by("i.student_id");
    		$this->db->limit(5);
    		$query = $this -> db -> get();
    	      
    		if($query -> num_rows() >= 1){
    		
    			return  $query->result_array();
    		}else{
    			return false;
    		}
         }
         
         function getBrowserUsage($condition){
        	$this -> db -> select('i.*,cr.course_name,count(i.course_id) as course_count');
			$this -> db -> from('tbl_student_details as i');
			$this -> db -> join('tbl_courses as cr', 'i.course_id  = cr.course_id', 'left');
			
    		$this->db->where("($condition)");
    		$this->db->order_by("student_id",'desc');
    		$this->db->group_by("i.course_id");
    		$query = $this -> db -> get();
    	      
    		if($query -> num_rows() >= 1){
    		
    			return  $query->result_array();
    		}else{
    			return false;
    		}
         }
         
         function getCustomerdata($all_customer_ids){
             $query = $this->db->query('Select * FROM tbl_customers where `customer_id` IN('.$all_customer_ids.')');
             if($query->num_rows()>0){
                 return $query->result_array();
             }
             else{
                 return false;
             }
         }
         
         function get_refer_and_wish($customer_id){
            $query = $this->db->query('Select * FROM tbl_refer_and_wish where user_id='.$this->db->escape($customer_id).' And user_type="customer" And status="Active"');
             if($query->num_rows()>0){
                 return $query->result_array();
             }
             else{
                 return false;
             }
         }
         
	function getCustomer(){
	   
	   $this -> db -> select('*');
	   $this -> db -> from('test_users as u');
	   $this -> db -> join('test_user_roles as r', 'u.UserId = r.UserId', 'left');
	   $this -> db -> where('r.RoleId', '4');
	
	   $query = $this -> db -> get();
	   
	   //print_r($query->result());
	   //exit;
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
	}
	
	
	function getLatestRecipts($condition) {
	    
	    	$this -> db -> select('i.*,c.center_name,CONCAT(sm.student_first_name," ",sm.student_last_name) as student_name,sm.enrollment_no,fpd.payment_mode,b.batch_name,g.group_master_name,co.course_name');
			$this -> db -> from("tbl_fees_payment_receipt as i");
			$this -> db -> join("tbl_admission_fees as af","af.admission_fees_id = i.admission_fees_id","left");
			$this -> db -> join("tbl_group_master as g","g.group_master_id = af.group_id","left");
    		$this -> db -> join("tbl_courses as co","co.course_id = af.course_id","left");
			$this -> db -> join("tbl_student_master as sm","sm.student_id = af.student_id","");
			$this -> db -> join("tbl_batch_master as b","b.batch_id = af.batch_id","");
			$this -> db -> join("tbl_centers as c","sm.center_id = c.center_id","");
			$this -> db -> join("tbl_fees_payment_details as fpd","i.receipt_no = fpd.receipt_no AND af.admission_fees_id = fpd.admission_fees_id ","left");
			$this->db->where("($condition)");
    		$this->db->order_by('i.receipt_no','desc');
    		$this->db->limit('5');		
    		$this->db->group_by('i.receipt_no');
		    $query = $this -> db -> get();
    	   if($query -> num_rows() >= 1){
    	     return $query->result_array();
    	   }else{
    	     return false;
    	   }
	}
	
	function getDropdown($tbl_name,$tble_flieds){
	   
	   $this -> db -> select($tble_flieds);
	   $this -> db -> from($tbl_name);
	
	   $query = $this -> db -> get();
	
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
			
	}
	
	function getDropdownSelval($tbl_name,$tbl_id,$tble_flieds,$rec_id=NULL){
		//echo "in condition...";
	   /*echo $tbl_name."<br/>";
	   echo $tbl_id."<br/>";
	   echo $tble_flieds."<br/>";
	   echo $rec_id."<br/>";*/
	   
	   
	   $this -> db -> select($tble_flieds);
	   $this -> db -> from($tbl_name);
	   $this -> db -> where($tbl_id, $rec_id);
	
	   $query = $this -> db -> get();
		
	   //print_r($query->result());
	   //exit;
	   
	   
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
	}
	
	function getUsersdata($UserID){
		
	   $this -> db -> select('u.*,r.RoleId');
	   $this -> db -> from('test_users as u');
	   $this -> db -> join('test_user_roles as r', 'u.UserId = r.UserId', 'left');
	   $this -> db -> where('u.UserId', $UserID);
	
	   $query = $this -> db -> get();
	   
	   //print_r($query->result());
	   //exit;
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
		
	}
	
	
	function getUserswarehouse($UserID){
		
	   $this -> db -> select('w.*');
	   $this -> db -> from('test_customer_warehouse_details as w');
	   $this -> db -> where('w.UserId', $UserID);
	
	   $query = $this -> db -> get();
	   
	   //print_r($query->result());
	   //exit;
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
		
	}
	
	
	//Update customer by id
	 function updateProfessional($datar,$eid)
	 {
		 $this -> db -> where('professional_id', $eid);
		 $this -> db -> update('tbl_professional',$datar);
		 
		 if ($this->db->affected_rows() > 0)
			{
			  return true;
			}
		 else
			{
			  return true;
			} 
		 
	 }
   
   function updatereferwish($datar,$eid)
	 {
		 $this -> db -> where('refer_and_wish_id', $eid);
		 $this -> db -> update('tbl_refer_and_wish',$datar);
		 
		 if ($this->db->affected_rows() > 0)
			{
			  return true;
			}
		 else
			{
			  return true;
			} 
		 
	 }
	 
	 function updateUserRole($tbl_name,$role_data,$id){
	 	$this->db->where('UserId', $id);
     	$this->db->delete('test_user_roles');
     	
     	$this->insertData($tbl_name,$role_data);
     	
	 }
	 
   function updatesession($data,$id){
	 	$this->db->where('session_id', $id);
     	$this->db->update('tbl_session_data',$data);
     	
     	
     	
	 }
	 
	 function updateWarehouseDetail($datar,$eid)
	 {
		 $this -> db -> where('customer_warehouse_id', $eid);
		 $this -> db -> update('test_customer_warehouse_details',$datar);
		 
		 if ($this->db->affected_rows() > 0)
			{
			  return true;
			}
		 else
			{
			  return true;
			} 
		 
	 }
	 
	function delRecord($tbl_name,$tbl_id,$record_id)
	 {
		 $this->db->where($tbl_id, $record_id);
	     $this->db->delete($tbl_name); 
		 if($this->db->affected_rows() >= 1)
	   {
	     return true;
	   }
	   else
	   {
	     return false;
	   }
	 }
	 
	 
	function delcustomer($id)
	 {
		$this->db->where('UserId', $id);
	    $this->db->delete('test_customer_supplier'); 
	     
	    $this->db->where('UserId', $id);
	    $this->db->delete('test_customer_warehouse_details'); 
	     
	 	$this->db->where('UserId', $id);
	    $this->db->delete('test_user_roles');

	    $this->db->where('UserId', $id);
	    $this->db->delete('test_users');
		
	    if($this->db->affected_rows() >= 1)
	    {
	      return true;
	    }
	    else
	    {
	     return false;
	    }
	 }
	
	function getleads($table,$condition)
	{
		$this->db->where($condition);
		$this->db->from($table);
		
		$query = $this->db->count_all_results();
		// print_r($this->db->last_query());exit;
		if($query)
		{
		  return $query;
		}
		else
		{
		  return false;
		}
	}
	
	function getstudentcount($table,$condition)
	{
		$this->db->where($condition);
		$this->db->from($table);
		
		$query = $this->db->count_all_results();
		// print_r($this->db->last_query());exit;
		if($query)
		{
		  return $query;
		}
		else
		{
		  return false;
		}
	}
	
	function getSumValue($table,$condition)
	{
	    $this->db->select( 'SUM(fees_amount_collected) AS Fee_Collected');
	    $this->db->where($condition);
		$this->db->from($table);
		
		$query = $this->db-> get();
		// print_r($this->db->last_query());exit;
		if($query -> num_rows() >= 1)
		{
		  return $query-> result();
		}
		else
		{
		  return false;
		}
	    
	}
	
	function getStudentName($table,$condition)
	{
	    $this->db->select( 'student_first_name AS First_Name , student_last_name AS Last_Name');
	    $this->db->where($condition);
		$this->db->from($table);
		
		$query = $this->db-> get();
		print_r($this->db->last_query());exit;
		if($query -> num_rows() >= 1)
		{
		  return $query-> result();
		}
		else
		{
		  return false;
		}
	    
	}
	 function getReceiptDetails()
	 {
	        $table = "tbl_fees_payment_receipt as i";
	        $this -> db -> select('i.*,c.center_name,CONCAT(sm.student_first_name," ",sm.student_last_name) as student_name,sm.enrollment_no,sm.father_name,sm.mother_name,sm.parent_name,sm.guardian,fpd.payment_mode,fpd.fees_remark,fpd.transaction_id,fpd.bank_name,b.batch_name');
			$this -> db -> from($table);
			$this -> db -> join("tbl_admission_fees as af","af.admission_fees_id = i.admission_fees_id","left");
			$this -> db -> join("tbl_student_master as sm","sm.student_id = af.student_id","left");
			$this -> db -> join("tbl_batch_master as b","b.batch_id = af.batch_id","left");
			$this -> db -> join("tbl_centers as c","sm.center_id = c.center_id","left");
			$this -> db -> join("tbl_fees_payment_details as fpd","i.receipt_no = fpd.receipt_no","left");
	 }
	
	function getcash_flow($table,$condition)
	{
		$this->db->select( 'SUM(total_amount) AS Total_Amount ,SUM(fees_amount_collected) AS Fee_Collected,SUM(fees_remaining_amount) AS Remaining_Amount', FALSE);
		$this->db->where($condition);
		$this->db->from($table);
		$query = $this -> db -> get();
	// 	print_r($this->db->last_query());exit;
	//    print_r($query->result());
	//    exit;
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
	}
 

}
?>
