<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Sectionsubcategory extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('sectionsubcategorymodel','',TRUE);
 }
 
 function index()
 {
   if(!empty($_SESSION["webadmin"]))
   {
		$this->load->view('template/header.php');
		$this->load->view('sectionsubcategory/index');
		$this->load->view('template/footer.php');
   }
   else
   {
		//If no session, redirect to login page
		redirect('login', 'refresh');
   }
 }
 
 function fetch(){
	//print_r($_GET);
	$get_result = $this->sectionsubcategorymodel->getRecords($_GET);
	
	//print_r($get_result['query_result']);
	//echo "Count: ".$get_result['totalRecords'];
	
	$result = array();
	$result["sEcho"]= $_GET['sEcho'];
	
	$result["iTotalRecords"] = $get_result['totalRecords'];									//	iTotalRecords get no of total recors
	$result["iTotalDisplayRecords"]= $get_result['totalRecords'];							//  iTotalDisplayRecords for display the no of records in data table.
	
	$items = array();
	
	for($i=0;$i<sizeof($get_result['query_result']);$i++){
		$temp = array();
		array_push($temp, $get_result['query_result'][$i]->category_name);
		array_push($temp, $get_result['query_result'][$i]->subcategory_name);
		
		$actionCol = "";
		$actionCol .='<a href="sectionsubcategory/addEdit?text='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->subcategory_id), '+/', '-_'), '=').'" title="Edit"><i class="icon-picture icon-edit"></i></a>';
//		$actionCol .='&nbsp;&nbsp;<a href="javascript:void(0);" onclick="deleteData(\''.$get_result['query_result'][$i]->subcategory_id.'\');" title="Delete"><i class="icon-remove-sign"></i></a>';
		
		array_push($temp, $actionCol);
		array_push($items, $temp);
	}
	
	$result["aaData"] = $items;
	echo json_encode($result);
	exit;
	
 }
 
function addEdit($id=NULL)
 {
   if(!empty($_SESSION["webadmin"]))
	{
     
		//print_r($_GET);
		$user_id = "";
		if(!empty($_GET['text']) && isset($_GET['text'])){
			$varr=base64_decode(strtr($_GET['text'], '-_', '+/'));	
			parse_str($varr,$url_prams);
			$user_id = $url_prams['id'];
		}
		
		//echo $user_id;
		$result['categories'] = $this->sectionsubcategorymodel->getDropdown("tbl_sectioncategories","category_id,category_name");
		$result['users'] = $this->sectionsubcategorymodel->getFormdata($user_id);
		
		$this->load->view('template/header.php');
		$this->load->view('sectionsubcategory/addEdit',$result);
		$this->load->view('template/footer.php');
	}
	else
	{
		//If no session, redirect to login page
		redirect('login', 'refresh');
	}
 }
 
	function submitForm(){
		/*print_r($_POST);
		exit;*/
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			
			if(!empty($_POST['subcategory_id'])){
				//update
				/*echo "in update";
				print_r($_POST);
				exit;*/
				
				$data = array();
				$data['category_id'] = $_POST['category_id'];
				$data['subcategory_name'] = $_POST['subcategory_name'];
				
				$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->sectionsubcategorymodel->updateUserId($data,$_POST['subcategory_id']);
					
				if(!empty($result)){
					echo json_encode(array('success'=>'1','msg'=>'Record Updated Successfully'));
					exit;
				}else{
					echo json_encode(array('success'=>'0','msg'=>'Problem in data update.'));
					exit;
				}
				
				
			}else{
				//add
				/*echo "in add";
				print_r($_POST);
				exit;*/
				
				//print_r($_SESSION["webadmin"]);
				//echo $_SESSION["webadmin"][0]->user_id;
				//exit;
				
				$data = array();
				$data['category_id'] = $_POST['category_id'];
				$data['subcategory_name'] = $_POST['subcategory_name'];
				$data['created_on'] = date("Y-m-d H:i:s");
				$data['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->sectionsubcategorymodel->insertData('tbl_sectionsubcategories',$data,'1');
				
				if(!empty($result)){
					echo json_encode(array("success"=>"1",'msg'=>'Record inserted Successfully.'));
					exit;
				}else{
					echo json_encode(array("success"=>"0",'msg'=>'Problem in data insert.'));
					exit;
				}
				
			}
			 
			
		}else{
			return false;
		}
	}
 
//For Delete

function delRecord($id)
 {
	 $appdResult = $this->sectionsubcategorymodel->delrecord("tbl_sectionsubcategories","subcategory_id",$id);
	 
	if($appdResult)
	{
		echo "1";
	}
	else
	{
		echo "2";	
			 
	}	
 }	
	
 function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('auth/login', 'refresh');
 }

}

?>