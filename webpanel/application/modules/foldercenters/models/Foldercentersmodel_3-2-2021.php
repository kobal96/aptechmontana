<?PHP
class Foldercentersmodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
	 	//print_r($this->db->last_query());
	    //exit;
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}
	 
	function insertSize($tbl_name, $data_array, $sendid = NULL)
	{
		$this->db->insert($tbl_name, $data_array);
		$result_id = $this->db->insert_id();
		if ($sendid == 1) {
			return $result_id;
		}
	}
	
	
	function getRecords($get){
		//echo "here...<br/>";
		//echo "<pre>";print_r($get);exit;
		//echo $get['album_id'];exit;
		
		$table = "tbl_folder_centers";
		$table_id = 'i.folder_center_id';
		$default_sort_column = 'i.folder_center_id';
		$default_sort_order = 'desc';
		
		$condition = "1=1 ";
		
		$colArray = array('ay.academic_year_master_name','z.zone_name', 'c.center_name');
		$searchArray = array('ay.academic_year_master_name','z.zone_name', 'c.center_name');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<4;$i++)
		{
			
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $searchArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		//echo "condition ".$condition;exit;
		
		$this -> db -> select('i.*,count(i.center_id) AS center_count,f.folder_name, z.zone_name, c.center_name,ay.academic_year_master_id,ay.academic_year_master_name');
		$this -> db -> from('tbl_folder_centers as i');
		$this -> db -> join('tbl_document_folders as f', 'i.document_folder_id  = f.document_folder_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this -> db -> join('tbl_academic_year_master as ay', 'ay.academic_year_master_id  = i.academic_year_id', 'left');
		$this-> db -> where('i.document_folder_id',$get['document_folder_id']);
		$this->db->where("($condition)");
		
		$this->db->order_by($sort, $order);	
		$this->db->group_by("i.zone_id,i.academic_year_id");	

		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		// print_r($this->db->last_query());
		//exit;
		
		// $this -> db -> select('i.*,f.folder_name, z.zone_name, c.center_name');
		$this -> db -> select('i.*,count(i.center_id) AS center_count,f.folder_name, z.zone_name, c.center_name,ay.academic_year_master_id,ay.academic_year_master_name');
		$this -> db -> from('tbl_folder_centers as i');
		$this -> db -> join('tbl_document_folders as f', 'i.document_folder_id  = f.document_folder_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this -> db -> join('tbl_academic_year_master as ay', 'ay.academic_year_master_id  = i.academic_year_id', 'left');
		$this-> db -> where('i.document_folder_id',$get['document_folder_id']);
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);		
		$this->db->group_by("i.zone_id,i.academic_year_id");	
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
		
		//exit;
	}

	function getCenterZoneWiseRecords($get){
		
		$table = "tbl_folder_centers";
		$table_id = 'i.folder_center_id';
		$default_sort_column = 'i.folder_center_id';
		$default_sort_order = 'desc';
		
		$condition = "1=1 ";
		
		$colArray = array('c.center_name');
		$searchArray = array('c.center_name');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<1;$i++)
		{
			
			$condition .= " && i.document_folder_id ='".$get['document_folder_id']."' && i.zone_id ='".$get['zone_id']."' &&   i.academic_year_id ='".$get['year_id']."' "; 
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $searchArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		//echo "condition ".$condition;exit;
		
		$this -> db -> select('i.*,f.folder_name, c.center_name,ay.academic_year_master_name');
		$this -> db -> from('tbl_folder_centers as i');
		$this -> db -> join('tbl_document_folders as f', 'i.document_folder_id  = f.document_folder_id', 'left');
		// $this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this -> db -> join('tbl_academic_year_master as ay', 'ay.academic_year_master_id  = i.academic_year_id', 'left');
		$this-> db -> where('i.document_folder_id',$get['document_folder_id']);
		$this->db->where("($condition)");
		
		$this->db->order_by($sort, $order);	
		// $this->db->group_by("i.zone_id,i.academic_year_id");	

		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		//exit;
		
		$this -> db -> select('i.*,f.folder_name, c.center_name,ay.academic_year_master_name');
		$this -> db -> from('tbl_folder_centers as i');
		$this -> db -> join('tbl_document_folders as f', 'i.document_folder_id  = f.document_folder_id', 'left');
		// $this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this -> db -> join('tbl_academic_year_master as ay', 'ay.academic_year_master_id  = i.academic_year_id', 'left');
		$this-> db -> where('i.document_folder_id',$get['document_folder_id']);
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);		
		// $this->db->group_by("i.zone_id,i.academic_year_id");	
		$query1 = $this -> db -> get();
		// print_r($this->db->last_query());
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
		
	}


	function getdata1($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition");
		if($sql->num_rows() > 0){
			return $sql->result();
		}else{
			return false;
		}
	}
	
	
	function getDropdown($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}
	
	function getDropdown1($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status="Active";
		$condition = "status='Active' && vendor_id='".$_SESSION["webadmin"][0]->frecord_id."' ";
		
		//$this -> db -> where("status",$status);
		$this -> db -> where("($condition)");
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}
	
	function getDropdownSelval($tbl_name,$tbl_id,$tble_flieds,$rec_id=NULL){
		
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$this -> db -> where($tbl_id, $rec_id);
	
		$query = $this -> db -> get();
		
		//print_r($this->db->last_query());
		//exit;
	   
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getFormdata($ID){
		
		//$condition = "1=1 && i.product_id= ".$ID." ";
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_folder_document as i');
		$this->db->where('i.folder_document_id', $ID);
		//$this->db->where("($condition)");
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}
	
	function getOptions($tbl_name,$tbl_id,$comp_id){
		//echo "Cat ID: ".$category_id;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	function getdata($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }
	
	function getRecords1($tbl_name, $tbl_id, $comp_field, $comp_record){
		$this -> db -> select($tbl_id);
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_field, $comp_record);
		$query = $this -> db -> get();

		if($query -> num_rows() >= 1){
			return $query->result();
		}
		else{
			return false;
		}	
	}
	
	function getRecords2($tbl_name, $tbl_id, $condition= '1=1'){
		$this -> db -> select($tbl_id);
		$this -> db -> from($tbl_name);
		$this -> db -> where("($condition)");
		$query = $this -> db -> get();

		if($query -> num_rows() >= 1){
			return $query->result();
		}
		else{
			return false;
		}	
	}
	
	//Update customer by id
	function updateUserId($datar,$eid)
	{
	 	// echo $_FILES["imagename"]["name"];
	 	// exit;
	 	if (!isset($_FILES["imagename"]["name"])) {
                 
            unset($datar['imagename']); 
            
		}else{
            $imagename  =    $_FILES["imagename"]["name"];
			// unset($data_array['imagename']); 
            // var_dump($data_array);
            

			//echo $_FILES["imagename"]["name"];
			//echo "hi";
			$target_dir =  realpath(APPPATH)."\images/";"<br>";
			$target_file = $target_dir.basename($_FILES["imagename"]["name"]);
			$date = date('Y-m-d H:i:s');
			$name = "CP-".time().".".pathinfo($target_file,PATHINFO_EXTENSION);
			move_uploaded_file($_FILES["imagename"]["tmp_name"], $target_dir.$name);
		}

	 	// if(isset($_FILES["imagename"]["name"])){ 
	 	//  $target_dir =  realpath(APPPATH)."\images/";"<br>";
	 	//  $target_file = $target_dir.basename($_FILES["imagename"]["name"]);
	 	//  $date = date('Y-m-d H:i:s');
	 	//  $name = "CP-".time().".".pathinfo($target_file,PATHINFO_EXTENSION);
		//    move_uploaded_file($_FILES["imagename"]["tmp_name"], $target_dir.$name);
        
		//   }
         
        //unset($_FILES["imagename"]["name"]);
        
	 	if($t1=$this->input->post('product_view_type'))
	 	{
	 		$datar['product_type'] = implode(",",$t1);
	 		//$datar['collection_type'] = implode(",",$this->input->post('collection_type'));
			//$datar['product_view_type'] = implode(",",$this->input->post('product_view_type'));
	 		$datar['product_view_type'] = implode(",",$this->input->post('product_view_type'));
        }


		if (isset($_FILES["imagename"]["name"])){
			$this->db->query("update sha_productimages SET imagename='$name' WHERE product_id='$eid'");
          
        }

		$this -> db -> where('product_id', $eid);
		$this -> db -> update('sha_products',$datar);
		 
		if ($this->db->affected_rows() > 0)
		{
			return true;
		}
		else
		{
			return true;
		} 
		 
	}
	 
	function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		//print_r($this->db->last_query());
		//exit;
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 
	 
	function delrecord($tbl_name,$tbl_id,$record_id,$status)
	{
		$data = array('status' => $status);
		 
		$this->db->where($tbl_id, $record_id);
		$this->db->update($tbl_name,$data); 
		 
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function delrecord12($tbl_name,$tbl_id,$record_id,$status)
	{
		$data = array('status' => $status);
		 
		$this->db->where($tbl_id, $record_id);
		$this->db->update($tbl_name,$data); 
		 
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function delrecord1($tbl_name,$tbl_id,$record_id)
	{
		$this->db->where($tbl_id, $record_id);
	    $this->db->delete($tbl_name); 
		if($this->db->affected_rows() >= 1)
		{
			return true;
	    }
	    else
	    {
			return false;
	    }
	}
	
	function delrecord_condition($tbl_name, $condition)
	{
		//$this->db->where($tbl_id, $record_id);
		$this -> db -> where("($condition)");
	    $this->db->delete($tbl_name); 
		if($this->db->affected_rows() >= 1)
		{
			return true;
	    }
	    else
	    {
			return false;
	    }
	}
	 
	function getFcm($category_id,$course_id,$document_id)
	{
		$this-> db ->select('fc.category_id,fc.course_id,cuc.user_id,adu.fcm_token,c.course_name,cc.center_id,adu.fcm_token');
		$this-> db ->from('tbl_folder_courses
		as fc');
		$this -> db ->join("tbl_center_user_courses as cuc","cuc.category_id = fc.category_id AND cuc.course_id = fc.course_id","inner");
		$this -> db ->join("tbl_center_courses as cc","cc.course_id = fc.course_id AND cc.category_id = fc.category_id","inner");
		$this -> db ->join("tbl_admin_users as adu","adu.user_id = cuc.user_id AND adu.center_id = cc.center_id","inner");
		$this -> db ->join("tbl_courses as c","c.course_id = fc.course_id","inner");
		
		
		$this-> db ->where('fc.category_id',$category_id);
		$this-> db ->where_in('fc.course_id',$course_id);
		$this-> db ->where('fc.document_folder_id',$document_id);
		$query = $this-> db ->get();
		// echo $this -> db->last_query();exit;
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	 
	
	function getFcmParent($category_id,$course_id)
	{
		$this-> db ->select('ps.category_id, ps.promoted_course_id, ps.center_id, ps.student_id, sm.fcm_token');
		$this-> db ->from('tbl_promoted_student  as ps');
		$this -> db ->join("tbl_student_master as sm","ps.student_id = sm.student_id ","inner");
		
		$this-> db ->where('ps.category_id',$category_id);
		$this-> db ->where_in('ps.promoted_course_id',$course_id);
		
		$query = $this-> db ->get();
		// echo $this -> db->last_query();exit;
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
}
?>
