<?php 
//error_reporting(0);
$record_id = $_GET['id'];
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Folder Centers</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>documentfoldermaster">Folders</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="document_folder_id" name="document_folder_id" value="<?php echo $record_id;?>" />
								

								<div class="control-group form-group">
									<label for="Zone" class="control-label">Current Academy Year</label>
									<div class="controls">
									<select name="academic_year_id" id="academic_year_id" class=" form-control">
										<option value="">Select Academy year</option>
										<?php if(!empty($academicyear)){
												foreach($academicyear as $key=>$val){
													// print_r($val);
													$sel ='';
														if(!empty($details[0]->academic_year_id)){
															$sel =($details[0]->academic_year_id == $val->academic_year_master_id)?"selected":'';
														}
														?>
											<option value="<?= $val->academic_year_master_id; ?>" <?= $sel ?> ><?= $val->academic_year_master_name;?></option>
										<?php
												}
											}
										?>
									</select>
									</div>
								</div>

								<div class="control-group form-group">
									<label class="control-label" for="category_id">Zone*</label>
									<div class="controls">
										<select id="zone_id" name="zone_id" class="form-control" onchange="getCenter(this.value)" >
											<option value="">Select Zone</option>
											<?php 
												if(isset($zones) && !empty($zones)){
													foreach($zones as $cdrow){
											?>
												<option value="<?php echo $cdrow->zone_id;?>"><?php echo $cdrow->zone_name;?></option>
											<?php }}?>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label" for="center_id">Center*</label>
									<input type="checkbox" id="checkbox" >Select All
									<div class="controls">
										<select id="center_id" name="center_id[]" class="form-control select2" multiple >
											<option value="">Select Center</option>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Start Date*</span></label>
									<div class="controls">
										<input type="text" class="form-control required datepicker" placeholder="Select start date" id="start_date" name="start_date" value="" >
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>End Date*</span></label>
									<div class="controls">
										<input type="text" class="form-control required datepicker" placeholder="Select end date" id="end_date" name="end_date" value="" >
									</div>
								</div>
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="javascript:history.go(-1)" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>
$( document ).ready(function() {

	$(".datepicker").datetimepicker({
		format: 'DD-MM-YYYY'
	});
	
	$("#checkbox").click(function(){
		if($("#checkbox").is(':checked') ){
			$("#center_id > option").prop("selected","selected");
			$("#center_id").trigger("change");
		}else{
			$("#center_id > option").removeAttr("selected");
			$("#center_id").trigger("change");
		}
	});
	
	<?php 
		if(!empty($details[0]->center_id)){
	?>
		//getCourses('<?php echo $details[0]->category_id; ?>');
	<?php }?>
	
	
	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						//['Source', 'Templates'],
						['Cut','Copy','Paste','PasteText','-','Print', 'SpellChecker', 'Scayt'],
						['Subscript','Superscript'],
						['NumberedList','BulletedList'],
						['BidiLtr', 'BidiRtl' ],
						//['Maximize', 'ShowBlocks'],
						['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						//['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						//['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
	
	$.validator.addMethod("needsSelection", function(value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });
	
});

function getCenter(zone_id)
{
	//alert("center_id: "+center_id);return false;
	if(zone_id != "" )
	{
		//alert();
		$.ajax({
			url:"<?php echo base_url();?>foldercenters/getCenter",
			data:{zone_id:zone_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#center_id").html(res['option']);
						$("#center_id").select2();
					}
					else
					{
						$("#center_id").html("<option value=''>Select</option>");
						$("#center_id").select2();
					}
				}
				else
				{	
					$("#center_id").html("<option value=''>Select</option>");
					$("#center_id").select2();
				}
			}
		});
	}
}

var vRules = {
	"center_id[]":{required:true},
	zone_id:{required:true},
	start_date:{required:true},
	end_date:{required:true},
	academic_year_id:{required:true}
	
};
var vMessages = {
	"center_id[]":{required:"please select the center name"},
	zone_id:{required:"Please select zone."},
	start_date:{required:"Please select start date."},
	end_date:{required:"Please select end date."},
	academic_year_id:{required:"please select academic year "}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>foldercenters/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						//window.location = "<?php echo base_url();?>centermaster";
						//window.location.reload();
						history.go(-1);
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Folder Centers";

 
</script>					
