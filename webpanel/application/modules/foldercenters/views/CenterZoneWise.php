
<!-- start: Content -->
<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1><?= $foldername[0]->folder_name." >> ".$zone_name[0]->zone_name ?> >>  individual Center Mapping </h1> 
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>documentfoldermaster"><?= $foldername[0]->folder_name?></a></li>
        </ul>
      </div>
    </div> 
    <div class="card">
    	<div class="page-title-border">
        	<div class="col-sm-12 col-md-12 left-button-top">
					<p>
					<?php if ($this->privilegeduser->hasPrivilege("DocumentFolderZoneCenterAdd")) {?>	
						<!-- <a href="<?php  echo base_url();?>foldercenters/addEdit?id=<?php echo $record_id; ?>" class="btn btn-primary icon-btn"><i class="fa fa-plus"></i>Add Center</a> -->
					<?php }?>	
            <div class="clearfix"></div>
            </div>
        </div>     
		
        <div class="col-sm-12" style="clear: both">
         	<div class="box-content form-horizontal product-filter">            	
            	
				<!-- <div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Academic year</label>
						<input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
					</div>
				</div>
				 -->
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">center name</label>
						<input id="sSearch_1" name="sSearch_1" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<!-- <div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Center</label>
						<input id="sSearch_2" name="sSearch_2" type="text" class="searchInput form-control"/>
					</div>
				</div> -->
				
				<div class="control-group clearFilter">
					<div class="controls">
						<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
					</div>
				</div>
				
				
            </div>
         </div>
		 
		 
         <div class="clearfix"></div>
         <div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                    <table class="dynamicTable display table table-bordered non-bootstrap" callfunction="<?php echo base_url();?>foldercenters/getCenterZoneWiseFetch?document_folder_id=<?=$foldername[0]->document_folder_id?>&zone_id=<?=$zone_name[0]->zone_id?>&year_id=<?= $academicyear[0]->academic_year_master_id?>">
                        <thead>
                          <tr>
							<th>Academic Year</th>
							<!-- <th>Zone</th> -->
							<th>Center</th>
							<th>Start Date</th>
							<th>End Date</th>
							<th data-bSortable="false" >Action</th>
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
	    
		<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
		    <div class="modal-dialog">
			   <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
			   <div class="modal-content">
				  <div class="modal-body">
					 <img src="" alt="" />
				  </div>
			   </div>
		    </div>
		</div>
	    
	    
    </div>
</div><!-- end: Content -->
			
<script>


	function deleteData(id)
	{
		var r=confirm("Are you sure to delete selected record?");
		if (r==true){
			//window.location.href="users/delete?id="+id;
				$.ajax({
					url: "<?php echo base_url().$this->router->fetch_module();?>/delRecord/",
					data:{"id":id},
					async: false,
					type: "POST",
					success: function(data2){
						data2 = $.trim(data2);
						if(data2 == "1")
						{
							displayMsg("success","Record has been Deleted!");
							setTimeout("location.reload(true);",1000);
							
						}
						else
						{
							displayMsg("error","Oops something went wrong!");
							setTimeout("location.reload(true);",1000);
						}
					}
				});
		}
    }
	
	
	document.title = "Folder - Zone & Centers";
</script>