<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Foldercenters extends CI_Controller

{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('foldercentersmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			//echo "here";exit;

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			// $result['categories'] = $this->foldercoursesmodel->getDropdown("tbl_categories","category_id,categoy_name");
			$result['academicyear'] = $this->foldercentersmodel->getdata1("tbl_academic_year_master","*");
			$result['foldername'] = $this->foldercentersmodel->getdata("tbl_document_folders","document_folder_id= ".$record_id." ");
			//  echo "<pre>"; 
			// print_r($result); exit;
			$this->load->view('template/header.php');
			$this->load->view('foldercenters/index',$result);
			$this->load->view('template/footer.php');
		}
		else {
			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['academicyear'] = $this->foldercentersmodel->getdata1("tbl_academic_year_master","*");
			$result['zones'] = $this->foldercentersmodel->getDropdown("tbl_zones","zone_id,zone_name");
			//$result['details'] = $this->foldercentersmodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('foldercenters/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	

	function addEditCenter($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			// $result['academicyear'] = $this->foldercentersmodel->getdata1("tbl_academic_year_master","*");
			$result['centers'] = $this->foldercentersmodel->getDropdown("tbl_centers","center_id,center_name");
			$result['details'] = $this->foldercentersmodel->getdata("tbl_folder_centers","folder_center_id= ".$record_id." ");
				// print_r($result); exit;
			$this->load->view('template/header.php');
			$this->load->view('foldercenters/addEditCenterIndividual', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	/*new code end*/
	function fetch($id=null)
	{
		$_GET['document_folder_id'] = $id;
		//echo $id;exit;
		$get_result = $this->foldercentersmodel->getRecords($_GET);

		// echo "<pre>";
		// print_r($get_result);
		// exit;
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->academic_year_master_name);
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_count." Center Mapped");
				// array_push($temp, date("d-m-Y", strtotime($get_result['query_result'][$i]->start_date)));
				// array_push($temp, date("d-m-Y", strtotime($get_result['query_result'][$i]->end_date)));
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("DocumentFolderZoneCenterDelete")){
					// $actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData(\'' . $get_result['query_result'][$i]->folder_center_id. '\');" title="">Delete</a>';

					// $actionCol21 .='<a href="foldercenters/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->document_folder_id) , '+/', '-_') , '=') . '" title="Edit" class="btn btn-primary" ><i class="fa fa-edit"></i></a>';
					$actionCol21 .='<a href="getCenterZoneWise?text=' . rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->document_folder_id) , '+/', '-_') , '=') . '&zid='.rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->zone_id) , '+/', '-_') , '=') . '&yid='.rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->academic_year_master_id) , '+/', '-_') , '=') . '" title="Edit" class="btn btn-primary" ><i class="fa fa-edit"></i></a>';
				}
				
				array_push($temp, $actionCol21);
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}


	function getCenterZoneWiseFetch($id=null)
	{
		// $_GET['document_folder_id'] = $id;
		//echo $id;exit;
		$get_result = $this->foldercentersmodel->getCenterZoneWiseRecords($_GET);

		// echo "<pre>";
		// print_r($get_result);
		// exit;
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->academic_year_master_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);
				// array_push($temp, $get_result['query_result'][$i]->center_count." Center Mapped");
				array_push($temp, date("d-m-Y", strtotime($get_result['query_result'][$i]->start_date)));
				array_push($temp, date("d-m-Y", strtotime($get_result['query_result'][$i]->end_date)));
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("DocumentFolderZoneCenterDelete")){
					// $actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData(\'' . $get_result['query_result'][$i]->folder_center_id. '\');" title="">Delete</a>';

					$actionCol21 .='<a href="addEditCenter?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->folder_center_id) , '+/', '-_') , '=') . '" title="Edit" class="btn btn-primary" ><i class="fa fa-edit"></i></a>';
					// $actionCol21 .='<a href="foldercenters/getCenterZoneWise?text=' . rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->document_folder_id) , '+/', '-_') , '=') . '&zid='.rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->zone_id) , '+/', '-_') , '=') . '&yid='.rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->academic_year_master_id) , '+/', '-_') , '=') . '" title="Edit" class="btn btn-primary" ><i class="fa fa-edit"></i></a>';
				}
				
				array_push($temp, $actionCol21);
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	

	function getCenterZoneWise(){
		if (!empty($_SESSION["webadmin"])) {
			//echo "here";exit;

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}

			if (!empty($_GET['zid']) && isset($_GET['zid'])) {
				$varr = base64_decode(strtr($_GET['zid'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$zone_id = $url_prams['id'];
			}

			if (!empty($_GET['yid']) && isset($_GET['yid'])) {
				$varr = base64_decode(strtr($_GET['yid'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$year_id = $url_prams['id'];
			}
			$result = "";
			// $result['categories'] = $this->foldercoursesmodel->getDropdown("tbl_categories","category_id,categoy_name");
			$result['academicyear'] = $this->foldercentersmodel->getdata1("tbl_academic_year_master","*","academic_year_master_id = '".$year_id."' ");
			$result['foldername'] = $this->foldercentersmodel->getdata1("tbl_document_folders","*","document_folder_id= ".$record_id." ");
			$result['zone_name'] = $this->foldercentersmodel->getdata1("tbl_zones","*","zone_id= ".$zone_id." ");
			// $result['zone_name'] = $zone_name;
			//  echo "<pre>"; 
			// print_r($result); exit;
			$this->load->view('template/header.php');
			$this->load->view('foldercenters/CenterZoneWise',$result);
			$this->load->view('template/footer.php');
		}
		else {
			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	public function getCenter(){

		$condition = "status='Active' && zone_id='".$_REQUEST['zone_id']."' ";
		$result = $this->foldercentersmodel->getdata("tbl_centers",$condition);
		// $result = $this->foldercentersmodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		$option = '';
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$option .= '<option value="'.$result[$i]['center_id'].'" >'.$result[$i]['center_name'].'</option>';
			}
		}
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	

	function submitFormCenterIndividual(){
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			$data=array();
			$data['start_date'] =  (!empty($_POST['start_date'])) ? date("Y-m-d", strtotime($_POST['start_date'])) : "";
			$data['end_date'] =  (!empty($_POST['end_date'])) ? date("Y-m-d", strtotime($_POST['end_date'])) : "";
			$result= $this->foldercentersmodel->updateRecord("tbl_folder_centers",$data,'folder_center_id',$_POST['folder_center_id']);
			if($result){
				echo json_encode(array('success' => '1','msg' => 'Record Updated Successfully.'
				));
				exit;
			}else{
				echo json_encode(array("success"=>"0",'msg'=>'Problem in data update.'));
				exit;
			}
		}
	}
	
	function submitForm()
	{ 
		// echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			// $condition = "status='Active' && zone_id='".$_POST['zone_id']."' ";
			
			// $getCenters = $this->foldercentersmodel->getdata("tbl_centers",$condition);
			// //echo "<pre>";print_r($check_name);exit;
			
			// if(empty($getCenters)){
			// 	echo json_encode(array("success"=>"0",'msg'=>' No active center present in selected zone!'));
			// 	exit;
			// }
			
			if(!empty($_POST['center_id'])){
				// $this->foldercentersmodel->delrecord_condition("tbl_folder_centers", " academic_year_id = '".$_POST['academic_year_id']."' && zone_id='".$_POST['zone_id']."' &&  document_folder_id='".$_POST['document_folder_id']."' ");

				
				// echo $this->db->last_query();
				// echo "<pre>";print_r($check_exist);exit;
				
				for($i=0; $i < sizeof($_POST['center_id']); $i++){
					//echo $getCenters[$i]['center_id']."<br/>";
					$check_exist = $this->foldercentersmodel->getdata("tbl_folder_centers", "academic_year_id='".$_POST['academic_year_id']."' && zone_id='".$_POST['zone_id']."' && document_folder_id='".$_POST['document_folder_id']."' && center_id='".$_POST['center_id'][$i]."' ");
					if(empty($check_exist)){
						$data_array = array();
						$data_array['document_folder_id'] = (!empty($_POST['document_folder_id'])) ? $_POST['document_folder_id'] : '';
						$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
						$data_array['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
						$data_array['center_id'] = $_POST['center_id'][$i];
						$data_array['start_date'] = (!empty($_POST['start_date'])) ? date("Y-m-d", strtotime($_POST['start_date'])) : NULL;
						$data_array['end_date'] = (!empty($_POST['end_date'])) ? date("Y-m-d", strtotime($_POST['end_date'])) : NULL;
						$data_array['status'] = 'Active';
						$data_array['created_on'] = date('Y-m-d H:i:s');
						$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
						$data_array['updated_on'] = date('Y-m-d H:i:s');
						$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;

						$result = $this->foldercentersmodel->insertData('tbl_folder_centers', $data_array, '1');
						$center_id_added[] = $_POST['center_id'][$i];
					}
				}
				if(!empty($center_id_added) && isset($center_id_added)){
					$course_id_added = $this->foldercentersmodel->getdata("tbl_center_courses", "center_id IN(".implode(',',$center_id_added).") ");
					// echo $this->db->last_query();
					// echo "<pre>";print_r($course_id_added);exit;
					$check_center_mapping =$this->foldercentersmodel->getdata("tbl_folder_centers", "document_folder_id='".$_POST['document_folder_id']."'  ");

					// for centers mapping
					foreach ($check_center_mapping as $key => $value) {
						$all_centers_id[] =$check_center_mapping[$key]['center_id'];
					}
					if($result){
						foreach ($course_id_added as $key => $value) {

							$document_id = $_POST['document_folder_id'];
							$getteachers = $this->foldercentersmodel->getFcm($value['category_id'],$value['course_id'],$document_id);
							// echo $this->db->last_query();
							// echo "<pre>";print_r($getteachers);
							// for parent notifications
							$getparents = $this->foldercentersmodel->getFcmParent($value['category_id'],$value['course_id']);

							$folder_count = $this->db->where(['document_folder_id'=>$_POST['document_folder_id']])->from("tbl_folder_document")->count_all_results();

							$folder_name =$this->db->select('folder_name')->where(['document_folder_id'=>$_POST['document_folder_id']])->from("tbl_document_folders")->get()->row()->folder_name;

							$course_name =$this->db->select('course_name')->where(['course_id'=>$value['course_id']])->from("tbl_courses")->get()->row()->course_name;
							// folder count yes for parent view
							$folder_count_parent = $this->db->where(['document_folder_id'=>$_POST['document_folder_id'],'parent_show'=>'Yes'])->from("tbl_folder_document")->count_all_results();
								if(!empty($getteachers)){
									for($s=0; $s < sizeof($getteachers); $s++){
										if(in_array($getteachers[$s]['center_id'],$all_centers_id)){							
											$title = 'Assign Document';
											$notification = '';
											$email_content=  '';
											$fcm_token = '';
											$subject='';
											$from_email='';
											$to_email='';
											$cc_email = '';
											$notification_type = array();
										
											// $notification = $folder_count ." Documents added for <b>".$course_name."</b> in <u>".$folder_name."<u>";

											$notification = $folder_count .(($folder_count >1) ? " Documents" : " Document") ." added for <b>".$course_name."</b> in <u>".$folder_name."<u>";
				
											// {UserName} added {Title with underline} for {Course Name}
											$notification_type = array('type'=>'service_request');
											
											$notification_data = array();
											$notification_data['teacher_id'] = $getteachers[$s]['user_id'];
											$notification_data['notification_type'] = 'Resources';
											$notification_data['notification_title'] = $title;
											$notification_data['notification_contents'] = $notification;
											
											$notification_data['created_on'] = date("Y-m-d H:i:s");
											$notification_data['created_by'] = (!empty($_SESSION["webadmin"][0]->user_id)) ? $_SESSION["webadmin"][0]->user_id : '' ;
											$notification_data['updated_on'] = date("Y-m-d H:i:s");
											$notification_data['updated_by'] = (!empty($_SESSION["webadmin"][0]->user_id)) ? $_SESSION["webadmin"][0]->user_id : '' ;
											$this->foldercentersmodel->insertData('tbl_teacher_notifications', $notification_data, 1);
								// 			$notification_batch_teacher[] = $notification_data;
											// echo $this->db->last_query();
								// 			if(!empty($getteachers[$s]['fcm_token'])){
								// 				//echo "h1";
								// 				$fcm_token = $getteachers[$s]['fcm_token'];
								// 				if(!empty($fcm_token)){
								// 					sendNotification($title,$notification,$email_content,$fcm_token,$subject,$from_email,$to_email,$cc_email,$notification_type);
								// 				}	
								// 			}
										}
									}//end of for loop for teacher
								// 	$this->db->insert_batch('tbl_teacher_notifications',$notification_batch_teacher);

								}
								// for parent 
								if(!empty($getparents) && $folder_count_parent >0){
									for($s=0; $s < sizeof($getparents); $s++){
										if(in_array($getparents[$s]['center_id'],$all_centers_id)){							
											$title = 'Assign Document';
											$notification = '';
											$email_content=  '';
											$fcm_token = '';
											$subject='';
											$from_email='';
											$to_email='';
											$cc_email = '';
											$notification_type = array();
										
											// $notification = $folder_count_parent ." Documents added for <b>".$course_name."</b> in <u>".$folder_name."<u>";

											$notification = $folder_count_parent .(($folder_count_parent >1) ? " Documents" : " Document") ." added for <b>".$course_name."</b> in <u>".$folder_name."<u>";
										
											$notification_type = array('type'=>'service_request');
											
											$notification_data = array();
											$notification_data['student_id'] = $getparents[$s]['student_id'];
											$notification_data['notification_type'] = 'Resources';
											$notification_data['notification_title'] = $title;
											$notification_data['notification_contents'] = $notification;
											
											$notification_data['created_on'] = date("Y-m-d H:i:s");
											$notification_data['created_by'] = (!empty($_SESSION["webadmin"][0]->user_id)) ? $_SESSION["webadmin"][0]->user_id : '' ;
											$notification_data['updated_on'] = date("Y-m-d H:i:s");
											$notification_data['updated_by'] = (!empty($_SESSION["webadmin"][0]->user_id)) ? $_SESSION["webadmin"][0]->user_id : '' ;
											$this->foldercentersmodel->insertData('tbl_student_notifications', $notification_data, 1);
								// 			$notification_batch_parent[] = $notification_data;
								// 			if(!empty($getparents[$s]['fcm_token'])){
								// 				//echo "h1";
								// 				$fcm_token = $getparents[$s]['fcm_token'];
								// 				if(!empty($fcm_token)){
								// 					sendNotification($title,$notification,$email_content,$fcm_token,$subject,$from_email,$to_email,$cc_email,$notification_type);
								// 				}	
								// 			}
										}
									}
								}//end for loop for parent
								// $this->db->insert_batch('tbl_student_notifications',$notification_batch_parent);
						}
						
					}
				}
			}else{
				echo json_encode(array("success"=>"0",'msg'=>' No active center present in selected zone!'));
				exit;
			}
			//exit;
			/*
			if(!empty($_POST['center_id'])){
				$this->foldercentersmodel->delrecord_condition("tbl_folder_centers", "zone_id='".$_POST['zone_id']."' &&  document_folder_id='".$_POST['document_folder_id']."' ");
				for($i=0; $i < sizeof($_POST['center_id']); $i++){
					$data_array = array();
					$data_array['document_folder_id'] = (!empty($_POST['document_folder_id'])) ? $_POST['document_folder_id'] : '';
					$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
					$data_array['center_id'] = $_POST['center_id'][$i];
					
					$result = $this->foldercentersmodel->insertData('tbl_folder_centers', $data_array, '1');
				}
			}else{
				echo json_encode(array("success"=>"0",'msg'=>'Kindly select atleast one record!'));
				exit;
			}
			*/
			
			/*$data_array = array();
				
			$data_array['document_folder_id'] = (!empty($_POST['document_folder_id'])) ? $_POST['document_folder_id'] : '';
			$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
			$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
			
			$result = $this->foldercentersmodel->insertData('tbl_folder_centers', $data_array, '1');
			*/
			
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Center is Allready Mapped.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	private function set_upload_options()
	{   
		//upload an image options //products
		$config = array();
		//$config['file_name']     = md5(uniqid("100_ID", true));
		$config['upload_path'] = DOC_ROOT_FRONT."/images/teacher_album_images";
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;

		return $config;
	}
	
	function delRecord()
	{
		$id=$_POST['id'];
		$appdResult = $this->foldercentersmodel->delrecord1("tbl_folder_centers","folder_center_id",$id);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
