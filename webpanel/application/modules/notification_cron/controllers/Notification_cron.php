<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Notification_cron extends CI_Controller

{
	function __construct()
	{
		parent::__construct();

		$this->load->model('Notification_cronmodel', '', TRUE);
	}

	function notifications(){
		$day_now = date("Y-m-d");
		$condition = "tn.created_on BETWEEN '".$day_now.' '."00:00:00". "' AND '".$day_now.' '."23:59:59". "'  AND tn.notification_send =0 ";
		$result['teacher_notification'] = $this->Notification_cronmodel->getdata1($condition);
		// echo $this->db->last_query();exit;
		$condition = "sn.created_on BETWEEN '".$day_now.' '."00:00:00". "' AND '".$day_now.' '."23:59:59". "'  AND sn.notification_send =0";
		$result['parent_notification'] = $this->Notification_cronmodel->getdata2($condition);
		
		// echo $this->db->last_query();
		// echo "<pre>";print_r($result['parent_notification']);exit;

		if(!empty($result['teacher_notification'])){
			foreach ($result['teacher_notification'] as $key => $value) {
				$title = $value->notification_title;
				$notification = $value->notification_contents;
				$email_content=  '';
				$fcm_token = '';
				$subject='';
				$from_email='';
				$to_email='';
				$cc_email = '';
				$fcm_token = $value->fcm_token;
				$notification_type = array('type'=>'service_request');
				if(!empty($fcm_token)){
					sendNotification($title,$notification,$email_content,$fcm_token,$subject,$from_email,$to_email,$cc_email,$notification_type);
				}
				$notification_data['notification_send'] = '1';
				$result= $this->Notification_cronmodel->updateRecord("tbl_teacher_notifications",$notification_data,'teacher_notification_id',$value->teacher_notification_id);
			}
		}

		if(!empty($result['parent_notification'])){
			foreach ($result['parent_notification'] as $key => $value) {
				$title = $value->notification_title;
				$notification = $value->notification_contents;
				$email_content=  '';
				$fcm_token = '';
				$subject='';
				$from_email='';
				$to_email='';
				$cc_email = '';
				$fcm_token = $value->fcm_token;
				$notification_type = array('type'=>'service_request');
				if(!empty($fcm_token)){
					sendNotification($title,$notification,$email_content,$fcm_token,$subject,$from_email,$to_email,$cc_email,$notification_type);
				}
				$notification_data['notification_send'] = '1';
				$result= $this->Notification_cronmodel->updateRecord("tbl_student_notifications",$notification_data,'student_notification_id',$value->student_notification_id);
			}
		}
	}
	
}
?>