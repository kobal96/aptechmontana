<?PHP
class Notification_cronmodel extends CI_Model
{
	function getdata1($condition){
        $sql = $this->db->select('tn.*,adu.fcm_token') ->from('tbl_teacher_notifications as tn') ->where($condition) ->join('tbl_admin_users as adu', 'tn.teacher_id = adu.user_id', 'inner') ->get();
        if($sql->num_rows() > 0){
			return $sql->result();
		}else{
			return false;
        }
    }
    function getdata2($condition){
        $sql = $this->db->select('sn.*,sm.fcm_token') ->from('tbl_student_notifications as sn') ->where($condition) ->join('tbl_student_master as sm', 'sn.student_id = sm.student_id', 'inner') ->get();
        if($sql->num_rows() > 0){
			return $sql->result();
		}else{
			return false;
        }
    }
    function updateRecord($tableName, $data, $column, $value){
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		//print_r($this->db->last_query());
		//exit;
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 
}