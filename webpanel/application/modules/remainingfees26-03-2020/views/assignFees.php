<form class="" id="feesInfoform-validate" method="post" enctype="multipart/form-data">
    <input type="hidden" id="student_id" name="student_id" value="<?php if(!empty($details[0]->student_id)){echo $details[0]->student_id;}?>">
    <input type="hidden" id="category_id" name="category_id" value="<?php if(!empty($otherdetails[0]->category_id)){echo $otherdetails[0]->category_id;}?>">
    <input type="hidden" id="course_id" name="course_id" value="<?php if(!empty($otherdetails[0]->course_id)){echo $otherdetails[0]->course_id;}?>">
    <input type="hidden" id="academic_year_id" name="academic_year_id" value="<?php if(!empty($otherdetails[0]->academic_year_id)){echo $otherdetails[0]->academic_year_id;}?>">

	<div class="button-group-pills" data-toggle="buttons">
		<h5 class="mb-0">
			<button class="collapse-btn btn toggle-btn added_flag_checkbox" data-toggle="collapse" data-target="#assignCourseFeeCollapse" aria-expanded="true" aria-controls="collapseOne" type="button" style="text-align: left;" check-attr="course_added"> <i class="fa pull-right" aria-hidden="true"></i>
				<input type="checkbox" name="is_course_added" id="is_course_added" value="course_added" class="pull-left flag_checkbox">
				ADD Admin/Adhoc/Other Fee
			</button>
		</h5>
		<div id="assignCourseFeeCollapse" class="collapse" aria-labelledby="assignCourseFeeCollapse" style="padding: 20px;border: 4px dashed #3c8dbc;">
			<div class="card-body">
				<div class="row">
					<div class="col-md-6 control-group form-group">
						<label class="control-label">Fees Type*</label>
						<div class="controls">
							<select id="fees_type" name="fees_type" class="form-control required" onchange="getFees(this.value);" title="Please select Fees Type" >
								<option value="">Select Fees Type</option>
								<?php 
								$allowedFees = array("Admin Fee","Ad-hoc Fee","Other Fee");
								if(isset($fees_type_details) && !empty($fees_type_details)){
									foreach($fees_type_details as $key=>$val){
										if(in_Array($val,$allowedFees)){
								?>
										<option value="<?php echo $val;?>"><?php echo $val;?></option>
								<?php 	}
									}
								}?>
							</select>
						</div>
					</div>
					<div class="col-md-6 control-group form-group">
						<label class="control-label"><span>Fees*</span></label> 
						<div class="controls">
							<select id="fees_id" name="fees_id" class="form-control required" onchange="getFeesDetails(this.value);" title="Please select Fees " >
								<option value="">Select Fees </option>
							</select>
						</div>
					</div>
					<div class="col-md-6 control-group form-group">
						<label class="control-label"><span>Course</span></label> 
						<div class="controls">
							<input type="text" name="course_name" id="course_name" class="form-control" value="<?php echo(!empty($details[0]->course_name))?$details[0]->course_name:"";?>" disabled>
						</div>
					</div>
					<div class="col-md-6 control-group form-group">
						<label class="control-label"><span>Admission Date</span></label> 
						<div class="controls">
							<input type="text" name="assign_admission_date" id="assign_admission_date" class="form-control" value="<?php if(!empty($otherdetails[0]->admission_date)){echo date("d-m-Y", strtotime($otherdetails[0]->admission_date));}?>" readonly>
							<input type="hidden" name="assign_programme_start_date" id="assign_programme_start_date" value="<?php if(!empty($otherdetails[0]->programme_start_date)){echo date("d-m-Y", strtotime($otherdetails[0]->programme_start_date));}?>">
							<input type="hidden" name="assign_programme_end_date" id="assign_programme_end_date" value="<?php if(!empty($otherdetails[0]->programme_end_date)){echo date("d-m-Y", strtotime($otherdetails[0]->programme_end_date));}?>">
						</div>
					</div>
				</div>
				<div class="clearfix"></div>		
				<table class="table table-striped component_show" style="border: 1px" cellpadding="10">
					<thead>
						<tr>
							<th>Sequence No</th>
							<th>Component(s)</th>
							<th class="text-right">Component Fees</th>
							<th class="text-right">Discount Amount</th>
							<th class="text-right">GST Amount</th>
							<th class="text-right">Net Fees</th>
						</tr>
					</thead>
					<tbody class="component_table">
					</tbody>
				</table>
				<input type="hidden" name="total_gst_amount" id="total_gst_amount">
				<input type="hidden" name="mandatory_component_amount" id="mandatory_component_amount">
				<input type="hidden" name="component_size" id="component_size">
				<input type="hidden" name="fees_total_amount" id="fees_total_amount">

				<div class="row">
					<div class="col-md-6 control-group form-group">
						<label class="control-label"><span>Payment Pattern</span></label>
						<div class="controls">
							<label for="is_instalment1" style="margin-top:20px;margin-right:30px;">
								<input type="radio"  id="is_instalment1" class="is_instalment" name="is_instalment" value="No" <?php echo (!empty($details[0]->is_instalment) &&  ($details[0]->is_instalment== 'No')) ? "checked" : ""?> checked="checked">Lumpsum
							</label>
							<label for="is_instalment2" style="margin-top:20px;">
								<input type="radio" id="is_instalment2" class="is_instalment install" name="is_instalment" value="Yes" <?php echo (!empty($details[0]->is_instalment) &&  ($details[0]->is_instalment== 'Yes')) ? "checked" : ""?> >Installment
							</label>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="col-sm-6 control-group form-group lumsum">
						<label for="">Lumpsum Date*</label>
						<input type="text" name="course_lumpsum_date" id="course_lumpsum_date" class="form-control monthPicker required" title="Please select the lumpsum date.">
					</div>
					<div class="col-md-6 control-group form-group lumsum">
						<label class="control-label"><span>Discount</span></label>
						<div class="col-sm-12 " style="padding:0px;">
							<div class="col-sm-4 col-lg-4 col-md-4 col-xs-4" style="padding:0px;">
								<select name="discount_type" id="discount_type" class="form-control" style="width:100%;height: 40px" onchange = "changeDiscountType('discount_amount_percentage')">
									<option value="percentage">Discount (in %)</option>
									<option value="rupees">Discount (in ₹)</option>
								</select>
							</div>
							<div class="col-sm-8 col-lg-8 col-md-8 col-xs-8"style="padding-left:0px;    padding-right: 10px;">
								<input type="text" class="form-control number_decimal_only" name="discount_amount_percentage" id="discount_amount_percentage" min="0" style="width:100%">
							</div>
						</div>
					</div>
					<div class="col-md-6 control-group form-group installment">
						<label class="control-label"><span>MaxInstallment Allowed</span></label>
						<div class="controls">
							<input type="text" class="form-control" name="max_installment" value="" readonly="readonly" id="max_installment">
						</div>
					</div>
					<div class="col-md-6 control-group form-group installment">
						<label class="control-label"><span>No Of Installment *</span></label>
						<div class="controls">
							<input type="text" class="form-control no_of_installments onlyNumber" name="no_of_installments" value="<?php if(!empty($details[0]->no_of_installments)){echo $details[0]->no_of_installments;}else{ echo 0;}?>" id="no_of_installments" min="0" max="">
						</div>
						<span class="text-danger installment_error"></span>
					</div>
					<div class="col-md-6 control-group form-group installment">
						<label class="control-label"><span>Discount</span></label>
						<div class="col-sm-12 " style="padding:0px;">
							<div class="col-sm-4 col-lg-4 col-md-4 col-xs-4" style="padding:0px;">
								<select name="discount_type_installment" id="discount_type_installment" class="form-control" style="width:100%;height: 40px" onchange = "changeDiscountType('discount_amount_percentage_installment')">
									<option value="percentage">Discount (in %)</option>
									<option value="rupees">Discount (in ₹)</option>
								</select>
							</div>
							<div class="col-sm-8 col-lg-8 col-md-8 col-xs-8"style="padding-left:0px;padding-right: 10px;">
								<input type="text" class="form-control number_decimal_only" name="discount_amount_percentage_installment" id="discount_amount_percentage_installment" min="0" style="width:100%">
							</div>
						</div>
					</div>
					
					<div class="col-md-6 control-group form-group lumsum">
						<label class="control-label"><span>Discount Amount</span></label>
						<div class="controls">
							<input type="text" class="form-control onlyNumber" name="discount_amount_rsl" id="total_discount_amount" readonly="readonly">
						</div>
					</div>
					<div class="col-md-6 control-group form-group lumsum">
						<label class="control-label"><span>Net Fees</span></label>
						<div class="controls">
							<input type="text" class="form-control" name="total_amountl" value="<?php if(!empty($details[0]->total_amount)){echo $details[0]->total_amount;}?>" readonly="readonly" id="total_amount">
						</div>
					</div>
					<div class="col-md-6 control-group form-group installment">
						<label class="control-label"><span>Discount Amount</span></label>
						<div class="controls">
							<input type="text" class="form-control onlyNumber" name="discount_amount_rs" id="total_discount_amount_installment" readonly="readonly">
						</div>
					</div>
					<div class="col-md-6 control-group form-group installment">
						<label class="control-label"><span>Net Fees</span></label>
						<div class="controls">
							<input type="text" class="form-control" name="total_amount" value="<?php if(!empty($details[0]->total_amount)){echo $details[0]->total_amount;}?>" readonly="readonly" id="total_amount_installment">
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="row">
					<div class=" installment">
						<table class="table table-striped" id="fees_table" style="background: #f9f9f9;float:left">
							<thead>
								<tr>
									<td>#</td>
									<td>Payment Due Date</td>
									<td>Installment Amount</td>
								</tr>
							</thead>
							<tbody class="f_table">
							</tbody>
						</table>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="row">
					<div class="col-md-6 control-group form-group">
						<label class="control-label"><span>Fees Remark</span></label>
						<div class="controls">
							<textarea class="form-control"  name="fees_remark" value="" placeholder="Fees Remark" id="fees_remarki" title="Please Enter Remarks" ><?php if(!empty($details[0]->fees_remark)){echo $details[0]->fees_remark;}?></textarea>
						</div>
					</div>
					<div class="col-md-6 control-group form-group">
						<label class="control-label"><span>Fees Accepted By</span></label>
						<div class="controls">
							<input type="text" class="form-control" name="fees_approved_accepted_by_name" value="<?php echo $login_name;?>" readonly="readonly">
						</div>
					</div>
				</div>

				<div class="row course_amount">
					<div class="col-md-6 control-group form-group">
						<label class="control-label"><span>Course Amount*</span></label>
						<div class="controls">
							<input type="text" class="form-control number_decimal_only" name="fees_amount_collected" value="<?php if(!empty($details[0]->fees_amount_collected)){echo $details[0]->fees_amount_collected;}else{ echo 0;}?>" id="fees_amount_collected1">
						</div>
						<span class="collectedamountError text-danger"></span>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<h5 class="mb-0">
			<button class="collapse-btn btn toggle-btn added_flag_checkbox" data-toggle="collapse" data-target="#assignGroupFeeCollapse" aria-expanded="true" aria-controls="collapseOne" type="button" style="text-align: left;"  check-attr="group_added"><i class="fa pull-right" aria-hidden="true"></i>
				<input type="checkbox" name="is_group_added" id="is_group_added" value="group_added" class="pull-left flag_checkbox">
				Add Group Fees
			</button>
		</h5>
		<div id="assignGroupFeeCollapse" class="collapse" aria-labelledby="assignGroupFeeCollapse">
			<div class="card-body">
				<!-- group div -->
				<input type="hidden" id="zone_id" name="zone_id" value="<?php echo(!empty($details[0]->zone_id))?$details[0]->zone_id:"";?>" />
				<input type="hidden" id="center_id" name="center_id" value="<?php echo(!empty($details[0]->center_id))?$details[0]->center_id:"";?>" />
				<div class="clearfix"></div>
				<div class="col-sm-12">
					<table class="table table-striped " id="groupTable" style="margin-top:15px;">
						<thead>
							<tr>
								<th width="100px;" colspan="2" class="text-right"><button class="btn btn-primary " type="button" onclick="addMoreGroup()"><i class="fa fa-plus"></i> Add Group Fee</button></th>
							</tr>
						</thead>
						<tbody></tbody>
						<tfoot>
							<tr>
								<td colspan="2">
									<button class="btn btn-primary pull-right" type="button" onclick="addMoreGroup()"><i class="fa fa-plus"></i> Add Group Fee</button>
								</td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
    <!-- group div -->
    <div class="clearfix"></div>
    <div class="form-actions form-group text-center" style="display:none;margin-top: 20px;">
        <button type="submit" class="btn btn-primary " name="paymentprocess">Save Fees</button>
        <button type="button" class="btn btn-primary " onclick="closeFeesModel()" >Cancel</button>
    </div>
</form>
<script>
	$(".flag_checkbox").on("change",function(){
		$(".flag_checkbox").each(function(){
			if($(this).is(":checked") == true){
				$(".form-actions").show();
				return false
			}else{
				$(".form-actions").hide();
			}
		})
	})
    function closeFeesModel(){
        if(confirm("Are sure to close this page..?")){
            $("#assignFeesModel").modal("hide");
        }
    }
    /* ------------------------------------------group code start---------------------------------- */
	addMoreGroup();
	function addMoreGroup(){
		var row_count = Number($("#groupTable>tbody>tr:last-child").attr("tr_count"));
		row_count++;
		if(row_count == "" || row_count == undefined || isNaN(row_count)){
			row_count = 0;
		}
		var programme_start_date = $("#assign_programme_start_date").val();
		var programme_end_date = $("#assign_programme_end_date").val();
		var trHtml = '<tr tr_count = "'+row_count+'">'+
						'<td colspan="2">'+
							'<div class="col-sm-12 row group-row">'+
								'<div class="pull-left group-count-cls">Group <span ></span></div><div class="clearfix"></div>'+
								'<button class="btn btn-danger pull-right removeGroup"><i class="fa fa-remove"></i>Remove</button><div class="clearfix"></div>'+
								'<div class="row form-group">'+
									'<div class="control-group col-md-6">'+
										'<label class="text_green"> Group<span class="text-danger">*</span> </label>'+
										'<select class="form-control required groupcheck" name="group_groupid['+row_count+']" grouprow="'+row_count+'" id="groupid_'+row_count+'">'+
											'<option value="" selected="selected" disabled="disabled">Select Group</option>'+
										'</select>'+
									'</div>'+
									'<div class="control-group col-md-6">'+
										'<label class="control-label"><span>Batch*</span></label> '+
										'<div class="controls">'+
											'<select id="batchid_'+row_count+'" name="group_batchid[]" class="form-control required"  onchange="getCenterGroupFees('+row_count+');">'+
												'<option value="">Select Batch</option>'+
											'</select>'+
										'</div>'+
									'</div>'+
								'</div>'+
								'<div class="row form-group">'+
									'<div class="control-group col-md-6">'+
										'<label class="control-label"><span>Fees*</span></label> '+
										'<div class="controls">'+
											'<select id="fees_id_'+row_count+'" name="group_fees_id['+row_count+']" class="form-control required" onchange="getGroupFeesDetails('+row_count+');">'+
												'<option value="">Select Fees</option>'+
											'</select>'+
										'</div>'+
									'</div>'+
									'<div class="control-group col-md-6 frequency_wrapper" id="frequency_wrapper_'+row_count+'">'+
										'<label class="control-label">Payment Frequency*</label>'+
										'<select class="form-control required" name="group_month_id['+row_count+']" onchange="getGroupMonthDiscount('+row_count+')" id="month_id_'+row_count+'">'+
											'<option value="">Select Payment Frequency</option>'+
										'</select>'+
									'</div>'+
								'</div>'+
								'<div class="row form-group">'+
									'<div class="control-group col-md-6">'+
										'<label for="">Group Start Date*</label>'+
										'<input type="text" name="group_start_date['+row_count+']" id="group_start_date_'+row_count+'" class="form-control  required" value="'+programme_start_date+'" title="Please select group start date">'+
									'</div>'+
									'<div class="control-group col-md-6">'+
										'<label for="">Group End Date*</label>'+
										'<input type="text" name="group_end_date['+row_count+']" id="group_end_date_'+row_count+'" class="form-control  required" value="'+programme_end_date+'" title="Please select group end date">'+
										'<input type="hidden" name="days_different['+row_count+']" id="days_'+row_count+'" tr_count="'+row_count+'" value="">'+
									'</div>'+
								'</div>'+
								'<div class="col-sm-12 " style="margin:20px auto;background:#fff;">'+
									'<table class="table table-striped component_show_for_group inner-table" style="border: 1px" id="component_show_for_group_'+row_count+'">'+
										'<thead>'+
											'<tr>'+
												'<th>Sequence No</th>'+
												'<th>Fees Component</th>'+
												'<th class="text-right">Component Fees</th>'+
												'<th class="text-right">Discount Amount</th>'+
												'<th class="text-right">GST Amount</th>'+
												'<th class="text-right">Sub Amount</th>'+
											'</tr>'+
										'</thead>'+
										'<tbody class="" id="component_table_'+row_count+'">'+
										'</tbody>'+
									'</table>'+
								'</div>'+
								'<input type="hidden" name="group_total_gst_amount['+row_count+']" id="total_gst_amount_'+row_count+'">'+
								'<input type="hidden" name="group_mandatory_component_amount['+row_count+']" id="mandatory_component_amount_'+row_count+'">'+
								'<input type="hidden" name="group_component_size['+row_count+']" id="component_size_'+row_count+'">'+
								'<input type="hidden" name="group_fees_total_amount['+row_count+']" id="fees_total_amount_'+row_count+'">'+
								'<input type="hidden" name="group_discount_amount['+row_count+']" id="discountamountpercentage_'+row_count+'">'+
								'<div class="row form-group">'+
									'<div class="control-group col-md-12" id="payement_pattern_wrapper_'+row_count+'">'+
										'<label class="control-label">Payment Pattern*</label>'+
										'<div class="controls" style="margin-top:10px;">'+
											'<label for="lumpsum_'+row_count+'"><input type="radio" name="is_group_installment['+row_count+']" id="lumpsum_'+row_count+'" class="group_payment_pattern_'+row_count+' group_payment_pattern" row_count = "'+row_count+'" value="lumpsum" checked>Lumpsum</label>&nbsp;&nbsp;'+
											'<label for="installment_'+row_count+'"><input type="radio" name="is_group_installment['+row_count+']" id="installment_'+row_count+'" class="group_payment_pattern_'+row_count+' group_payment_pattern" row_count = "'+row_count+'" value="installment">Installment</label>'+
										'</div>'+
									'</div>'+
									'<div class="col-sm-6 control-group form-group" id="group_lumpsum_wrapper_'+row_count+'" >'+
										'<label for="">Lumpsum Date*</label>'+
										'<input type="text" name="group_lumpsum_date['+row_count+']" id="group_lumpsum_date_'+row_count+'" class="form-control monthPicker required" title="Please select the lumpsum date.">'+
									'</div>'+
									'<div id="group_installment_wrapper_'+row_count+'" style="margin-top:20px;">'+
										'<div class="control-group col-md-6" id="group_max_installment_'+row_count+'">'+
											'<label class="control-label">Max Installment </label>'+
											'<input type="text" id="group_max_installment_count_'+row_count+'" name="group_max_installment_count['+row_count+']" class="form-control" readonly>'+
										'</div>'+
										'<div class="control-group col-md-6" id="group_installment_'+row_count+'" >'+
											'<label class="control-label">No of Installment</label>'+
											'<input type="text" id="group_installment_count_'+row_count+'" name="group_installment_count['+row_count+']" class="form-control required onlyNumber" value="0" onkeyup="addGroupInstallment(this);" title="Please enter number of installments" >'+
										'</div><div class="clearfix"></div>'+
										'<div class="control-group col-sm-12 ">'+
											'<table class="table" id="group_installment_table_'+row_count+'" style="background: #fff;display:none;margin-top:30px;">'+
												'<thead>'+
													'<tr>'+
														'<td>#</td>'+
														'<td>Payment Due Date</td>'+
														'<td>Installment Amount</td>'+
													'</tr>'+
												'</thead>'+
												'<tbody id="group_installment_table_tbody_'+row_count+'">'+
												'</tbody>'+
											'</table>'+
										'</div>'+
									'</div>'+
									'<div class="col-md-6 control-group form-group " id="discount_wrapper_'+row_count+'">'+
										'<label class="control-label"><span>Discount</span></label>'+
										'<div class="col-sm-12 " style="padding:0px;">'+
											'<div class="col-sm-4 col-lg-4 col-md-4 col-xs-4" style="padding:0px;">'+
												'<select name="group_discount_type['+row_count+']" id="group_discount_type_'+row_count+'" class="form-control" style="width:100%;height:40px" onchange = "changeGroupDiscountType('+row_count+')">'+
													'<option value="percentage">Discount (in %)</option>'+
													'<option value="rupees">Discount (in ₹)</option>'+
												'</select>'+
											'</div>'+
											'<div class="col-sm-8 col-lg-8 col-md-8 col-xs-8"style="padding-left:0px;padding-right: 10px;">'+
												'<input type="text" class="form-control number_decimal_only" name="group_discount_amount_percentage['+row_count+']" id="group_discount_amount_percentage_'+row_count+'" min="0" style="width:100%" onkeyup="changeGroupDiscountType('+row_count+')">'+
											'</div>'+
										'</div><div class="clearfix"></div>'+
									'</div>'+
									'<div class="col-md-6 control-group">'+
										'<label class="control-label"><span>Discount Amount</span></label>'+
										'<div class="controls">'+
											'<input type="text" class="form-control onlyNumber" name="group_discount_amount_rs['+row_count+']" id="total_discount_amount_'+row_count+'" readonly="readonly">'+
										'</div>'+
									'</div>'+
								'</div>'+
								'<div class="row form-group">'+
									'<div class="col-md-6 control-group">'+
										'<label class="control-label"><span>Net Fees</span></label>'+
										'<div class="controls">'+
											'<input type="text" class="form-control" name="group_total_amount['+row_count+']" value="" readonly="readonly" id="total_amount_'+row_count+'">'+
										'</div>'+
									'</div>'+
									'<div class="col-md-6 control-group">'+
										'<label class="control-label"><span>Fees Accepted By</span></label>'+
										'<div class="controls">'+
											'<input type="text" class="form-control" name="group_fees_approved_accepted_by_name['+row_count+']" value="<?php echo $login_name;?>" readonly="readonly">'+
										'</div>'+
									'</div>'+
									'<div class="col-md-12 control-group">'+
										'<label class="control-label"><span>Fees Remark</span></label>'+
										'<div class="controls">'+
											'<textarea class="form-control " name="group_fees_remark['+row_count+']" value="" placeholder="Fees Remark" id="fees_remark_'+row_count+'"></textarea>'+
										'</div>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</td>'+
					'</tr>';
		$("#groupTable>tbody").append(trHtml);
		$(".monthPicker").datetimepicker({ 
			viewMode: 'months',
			format: 'DD-MM-YYYY',
		});

		$("#group_start_date_"+row_count).datepicker({
			autoclose: true,
			format: 'dd-mm-yyyy'
		}).on('changeDate', function (selected) {
			var days = $("#days_"+row_count).val();
			var minDate = new Date(selected.date.valueOf());
			var maxDate = new Date(selected.date);
			if(days > 0 && days != ""){
				maxDate.setDate(parseInt(maxDate.getDate()) + parseInt(days));
			}else{
				var group_start_date = $("#assign_programme_end_date").val().split("-");
				var day = group_start_date[0];
				var month = parseInt(group_start_date[1])-1;
				var year = group_start_date[2];
				parsedDate = new Date(day,month,year);
				maxDate = new Date(parsedDate)
			}
			$("#group_end_date_"+row_count).datepicker('setStartDate', minDate).datepicker('setEndDate', maxDate).datepicker('setDate', maxDate);
		});

		$("#group_end_date_"+row_count).datepicker({format: 'dd-mm-yyyy',autoclose: true})
			.on('changeDate', function (selected) {
				var maxDate = new Date(selected.date.valueOf());
				// $("#group_start_date_"+row_count).datepicker('setEndDate', maxDate);
		});

		//get group dropdown
		getGroup("#groupid_"+row_count);
		//hide installment wrapper
		$("#group_installment_wrapper_"+row_count).hide();
		var group_count = 1;
		$(".group-count-cls>span").each(function(){
			$(this).html(group_count++);			
		})
	}

	$(document).on("click",".removeGroup",function(){
		$(this).closest("tr").remove();
		var group_count = 1;
		$(".group-count-cls>span").each(function(){
			$(this).html(group_count++);			
		})
	});

	$(document).on("change",'select.groupcheck',function(){
        getCenterUserGroupBatches(this);
	});

	//change group payment pattern
	$(document).on("change",".group_payment_pattern",function(){
		var row_count = $(this).attr("row_count");
		if($(this).val() == "lumpsum"){
			$("#group_installment_wrapper_"+row_count).hide();
			$("#group_lumpsum_wrapper_"+row_count).show();
		}else{
			$("#group_installment_wrapper_"+row_count).show();
			$("#group_lumpsum_wrapper_"+row_count).hide();
		}
	});

	function getGroup(id){
		var student_id = $("#student_id").val();
		var zone_id = $("#zone_id").val();
		var center_id = $("#center_id").val();
		$.ajax({
			url:"<?php echo base_url();?>admission/getStudentAssignedGroups",
			data:{student_id:student_id,zone_id:zone_id,center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$(id).html(res['option']);
					}
				}
			}
		});
	}
	
	function getGroupFeesLevel(row_count){
		var id = $("#fees_level_"+row_count);
		var center_id = $('#center_id').val()
		var zone_id = $('#zone_id').val()
		if(center_id != undefined && zone_id != undefined && center_id != '' && zone_id != '' ){
			$.ajax({
				url:"<?php echo base_url();?>admission/getCenterFeesLevel",
				data:{center_id:center_id,zone_id:zone_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != ""){
							$(id).html(res['option']);
							getCenterGroupFees($(id).closest("tr").attr("tr_count"));
						}else{
							$(id).html("<option value=''>Select</option>");
						}
					}
					else
					{	
						$(id).html("<option value=''>Select</option>");
					}
				}
			});
		}
	}
	
	function getCenterGroupFees(tr_count){
		// fees_level_id = $('#fees_level_'+tr_count).val();
		var group_id = $('#groupid_'+tr_count).val();
		var batch_id = $('#batchid_'+tr_count).val();
		var center_id = $('#center_id').val();
		var zone_id = $('#zone_id').val();
		$("#component_table_"+tr_count).children().remove();
		if( group_id != undefined && center_id != undefined &&   zone_id != undefined && group_id != '' && center_id != '' && zone_id != '' ){
			$.ajax({
				url:"<?php echo base_url();?>admission/getCenterFees",
				data:{group_id:group_id,center_id:center_id,zone_id:zone_id,batch_id:batch_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != ""){
							$("#fees_id_"+tr_count).html(res['option']);
							getGroupFeesDetails(tr_count);
						}else{
							$("#fees_id_"+tr_count).html("<option value=''>Select fees</option>");
						}
					}else{	
						$("#fees_id_"+tr_count).html("<option value=''>Select fees</option>");
					}
				}
			});
		}else{
			$("#fees_id_"+tr_count).html("<option value=''>Select fees</option>");
			$('#total_amount_'+tr_count).val(0);
		}
	}

	function getCenterUserGroupBatches(elem){
		var row_count = $(elem).closest("tr").attr("tr_count");
		$("#fees_id_"+row_count).html("<option value=''>Select Fee</option>");
		$("#frequency_wrapper_"+row_count).hide();
		//group end date and end date set
		var group_end_date =  $("#assign_programme_end_date").val().split("-");
		var day = group_end_date[2];
		var month = parseInt(group_end_date[1])-1;
		var year = group_end_date[0];
		var parsed_date = new Date(day,month,year);
		$("#group_end_date_"+row_count).datepicker({
			'viewMode':'months',
			format:"dd-mm-yyyy"
		}).datepicker('setEndDate', null).datepicker('setStartDate', null).datepicker('setDate',parsed_date);
		var group_id = $(elem).val()
		var zone_id = $("#zone_id").val();
		var center_id = $("#center_id").val();
		if(group_id != "" ){
			$.ajax({
				url:"<?php echo base_url();?>admission/getCenterUserGroupBatches",
				data:{group_id:group_id,zone_id:zone_id,center_id:center_id},
				dataType: 'json',
				method:'post',
				success: function(res){
					if(res['status']=="success"){
						if(res['option'] != ""){
							$("#batchid_"+row_count).html(res['option']);
							getCenterGroupFees(row_count);
						}else{
							$("#batchid_"+row_count).html("<option value=''>Select Batch</option>");
						}
					}else{	
						$("#batchid_"+row_count).html("<option value=''>Select Batch</option>");
					}
				}
			});
		}else{
			$("#batchid_"+row_count).html("<option value=''>Select Batch</option>");
		}
	}

	function getGroupFeesDetails(tr_count){
		var fees_id = $("#fees_id_"+tr_count).val();
		if(fees_id != "" ){
			$("#component_table_"+tr_count).children().remove();
			$.ajax({
				url:"<?php echo base_url();?>admission/getFeesDetails",
				data:{ fees_id:fees_id},
				dataType: 'json',
				method:'post',
				success: function(res){
					if(res['status']=="success" ){
						let total_amount = 0
						let t_amount = 0;
						let total_sub_amount = 0
						let total_gst_amount = 0
						let total_mandatory_amount = 0
						let total_discount_amount = 0
						$('#component_show_for_group_'+tr_count).show()
						var size = res['result'].length
						$('#component_size_'+tr_count).val(size)
						$("#component_table_"+tr_count).children().remove();
						for(let i=0;i<size;i++){
							$("#component_table_"+tr_count).append("<tr>");
							$("#component_table_"+tr_count).append('<td class="text-center">'+Number(i+1)+'</td>');
							$("#component_table_"+tr_count).append('<td>'+res['result'][i]['fees_component_master_name']+'</td>');
							$("#component_table_"+tr_count).append('<td class="text-right"><span id="component_fees_'+tr_count+'_'+i+'">'+res['result'][i]['component_fees']+'</span><br><span id="component_fees_after_frequency_'+tr_count+'_'+i+'"></span><input type="hidden" name="actual_component_fees['+tr_count+']['+i+']" id="actual_component_fees_'+tr_count+"_"+i+'" value="'+res['result'][i]['component_fees']+'" class="text-right"></td>');
							$("#component_table_"+tr_count).append('<td><div class="form-group insidetable-form-group" ><input type="text" class="form-control discount_amount text-right"  id="discountamount_'+tr_count+"_"+i+'"name="discount_amount['+tr_count+']['+i+']" readonly="readonly"></div></td>');
							$("#component_table_"+tr_count).append('<td><div class="form-group insidetable-form-group"><input type="text" class="form-control gst_amount text-right"  id="gstamount_'+tr_count+"_"+i+'"name="gst_amount['+tr_count+']['+i+']" readonly="readonly"></div></td>');
							$("#component_table_"+tr_count).append('<td><div class="form-group insidetable-form-group"><input type="text" class="form-control sub_amount text-right"  id="subamount_'+tr_count+"_"+i+'"name="sub_amount['+tr_count+']['+i+']" readonly="readonly"><input type="hidden" class="form-control component_type"  id="component_type_'+tr_count+"_"+i+'" name="component_type['+tr_count+']['+i+']" value="'+res['result'][i]['component_type']+'"></div></td>');
							$("#component_table_"+tr_count).append('<td><div class="form-group insidetable-form-group"><input type="hidden" class="form-control component_gst_fees"  id="componentgstfees_'+tr_count+"_"+i+'"name="component_gst_fees['+tr_count+']['+i+']" ></div></td>');
							$("#component_table_"+tr_count).append('<td><div class="form-group"><input type="hidden" class="form-control is_discountable"  id="isdiscountable_'+tr_count+"_"+i+'"name="is_discountable['+tr_count+']['+i+']"></div></td>');
							$("#component_table_"+tr_count).append('<td><div class="form-group"><input type="hidden" class="form-control is_gst"  id="isgst_'+tr_count+"_"+i+'"name="is_gst['+tr_count+']['+i+']"></div></td>');
							$("#component_table_"+tr_count).append('<td><div class="form-group"><input type="hidden" class="form-control fees"  id="fees_'+tr_count+"_"+i+'"name="fees['+tr_count+']['+i+']"></div></td>');
							$("#component_table_"+tr_count).append('<td><div class="form-group"><input type="hidden" class="form-control tax"  id="tax_'+tr_count+"_"+i+'"name="tax['+tr_count+']['+i+']"></div></td>');
							$("#component_table_"+tr_count).append('<td><div class="form-group"><input type="hidden" class="form-control component_new_fees"  id="componentnewfees_'+tr_count+"_"+i+'"name="component_new_fees['+tr_count+']['+i+']"></div></td>');
							$("#component_table_"+tr_count).append('<td><div class="form-group"><input type="hidden" class="form-control component_fees"  id="componentfees_'+tr_count+"_"+i+'"name="component_fees[]"></div></td>');
							$("#component_table_"+tr_count).append('<td><div class="form-group"><input type="hidden" class="form-control original_gst_fees"  id="originalgstfees_'+tr_count+"_"+i+'"name="original_gst_fees['+tr_count+']['+i+']"></div></td>');
							$("#component_table_"+tr_count).append("</tr>");
							$("#component_table_"+tr_count).append("</br>");
							$('#isdiscountable_'+tr_count+"_"+i).val(res['result'][i]['is_discountable'])
							$('#isgst_'+tr_count+"_"+i).val(res['result'][i]['is_gst'])
							$("#discountamount_"+tr_count+"_"+i).val(0);
							$('#fees_'+tr_count+"_"+i).val(parseFloat(res['result'][i]['component_fees']))
							$('#tax_'+tr_count+"_"+i).val(res['result'][i]['tax'])
							var frequancy = 1;
							$("#month_id_"+tr_count).val('');
							// if($("#month_id_"+tr_count).val() != "" && $("#month_id_"+tr_count).is(":visible")==false){
							// 	frequancy = $("#month_id_"+tr_count+" option:selected").attr('month_frequency');
							// }
							
							if(res['result'][i]['is_gst'] == 'Yes'){
								let gst_amount = ((parseFloat(res['result'][i]['component_fees']) * parseFloat(res['result'][i]['tax']))/100).toFixed(2);
								
								$('#gstamount_'+tr_count+"_"+i).val(parseFloat(gst_amount))
								let componentgstfees = (parseFloat(res['result'][i]['component_fees']) - parseFloat(gst_amount)).toFixed(2); //gst added amount
								$('#componentgstfees_'+tr_count+"_"+i).val(parseFloat(componentgstfees)*parseFloat(frequancy))
								$('#originalgstfees_'+tr_count+"_"+i).val(parseFloat(gst_amount))
								$('#subamount_'+tr_count+"_"+i).val(parseFloat(componentgstfees).toFixed(2))
							}else{
								$('#componentgstfees_'+tr_count+"_"+i).val(res['result'][i]['component_fees'])
								var componentgstfees = ($('#componentgstfees_'+tr_count+"_"+i).val()!="")?$('#componentgstfees_'+tr_count+"_"+i).val():0;
								
								$('#subamount_'+tr_count+"_"+i).val((parseFloat(componentgstfees)*parseFloat(frequancy)).toFixed(2))
								$('#gstamount_'+tr_count+"_"+i).val(0)
								$('#originalgstfees_'+i).val(0)
							}
							if(res['result'][i]['is_mandatory'] == 'Yes'){
								total_mandatory_amount = total_mandatory_amount + parseFloat(res['result'][i]['component_fees'])
							}
							$('#componentfees_'+tr_count+"_"+i).val(res['result'][i]['component_fees'])
							var componentgstfees = ($('#componentgstfees_'+tr_count+"_"+i).val()!="")?$('#componentgstfees_'+tr_count+"_"+i).val():0;
							total_amount = parseFloat(total_amount) + parseFloat(componentgstfees);
							total_gst_amount = parseFloat(total_gst_amount) + parseFloat(res['result'][i]['gst_amount'])
							$('#total_gst_amount_'+tr_count).val(parseFloat(total_gst_amount))
							t_amount = t_amount + parseFloat(res['result'][i]['component_fees'])//total amount column display
							let subamountval = $('#subamount_'+tr_count+"_"+i).val()
							total_sub_amount = parseFloat(total_sub_amount) + parseFloat(subamountval)
							$('#total_amount_'+tr_count).val(parseFloat(total_sub_amount).toFixed(2))
						}

						$("#component_table_"+tr_count).append("<tr>");
						$("#component_table_"+tr_count).append('<td><b>Total</b></td>');
						$("#component_table_"+tr_count).append('<td></td>');
						$("#component_table_"+tr_count).append('<td class="text-right" id="t_amount_'+tr_count+'"></td>');
						$("#component_table_"+tr_count).append('<td></td>');
						$("#component_table_"+tr_count).append('<td></td>');
						$("#component_table_"+tr_count).append('<td class="text-right" id="total_sub_amount_'+tr_count+'"></td>');
						$("#component_table_"+tr_count).append("</tr>");


						$('#total_sub_amount_'+tr_count).text(parseFloat(total_sub_amount).toFixed(2))

						$('#t_amount_'+tr_count).text((t_amount).toFixed(2))
						$('#total_discount_amount_'+tr_count).val(parseFloat(total_discount_amount))
						$('#mandatory_component_amount_'+tr_count).val(parseFloat(total_mandatory_amount).toFixed(2))
						$('#fees_total_amount_'+tr_count).val(parseFloat(t_amount).toFixed(2));
						$('#fees_amount_collected_'+tr_count).val(parseFloat(total_sub_amount))
						$('#fees_remaining_amount_'+tr_count).val(0)
						$('#group_max_installment_count_'+tr_count).val(res['result'][0]['installment_no']);
						
						if(res['result'][0]['allow_frequency'] == "Yes"){
							$.ajax({
								url:"<?php echo base_url();?>admission/getFrequencyData",
								data:{fees_id:fees_id},
								dataType: 'json',
								method:'post',
								success: function(res){
									$("#month_id_"+tr_count).html(res.option);
								}
							})
							$("#frequency_wrapper_"+tr_count).show();
							$("#payement_pattern_wrapper_"+tr_count).hide();
							$("#discount_wrapper_"+tr_count).hide();
							$("#group_installment_wrapper_"+tr_count).hide();
							$("#group_installment_table_tbody_"+tr_count).children().remove();
							$("#group_installment_count_"+tr_count).val("");
						}else{
							$("#frequency_wrapper_"+tr_count).hide();
							$("#payement_pattern_wrapper_"+tr_count).show();
							$("#discount_wrapper_"+tr_count).show();
						}
					}
					else
					{	
						$('#fees_total_amount_'+tr_count).val(0)
						$('#fees_remaining_amount_'+tr_count).val(0)
						$('#total_amount_'+tr_count).val(0)
						$("#component_table_"+tr_count).children().remove();
						$('#group_max_installment_count_'+tr_count).val(0);
					}
				}
			});
		}else{
			$("#component_table_"+tr_count).children().remove();
		}
		changeGroupDiscountType(tr_count)
	}

	function getGroupMonthDiscount(tr_count){
		var month_id = $("#month_id_"+tr_count).val();
		if(month_id != '' && month_id != undefined){
			$.ajax({
				url:"<?php echo base_url();?>admission/getMonthDiscount",
				data:{month_id:month_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					//group start date and end date set
					$("#days_"+tr_count).val(parseInt(res['days']));
					var group_start_date = $("#group_start_date_"+tr_count).val().split("-");
					var day = group_start_date[2];
					var month = parseInt(group_start_date[1])-1;
					var year = group_start_date[0];
					var parsed_date = new Date(day,month,year);
					var maxdate = new Date(parsed_date);
					maxdate.setDate(parseInt(parsed_date.getDate()) + parseInt(res['days']));
					$("#group_end_date_"+tr_count).datepicker({
						'viewMode':'months',
						format:"dd-mm-yyyy"
					}).datepicker('setEndDate', maxdate).datepicker('setStartDate', parsed_date).datepicker('setDate',maxdate);

					if(res['option'] == false){
						var discount_amount = 0
					}
					else{
						var discount_amount = res['option'][0]['discount'];
					}
					$('#discountamountpercentage_'+tr_count).val(discount_amount)
					var fees_total_amount = $('#fees_total_amount_'+tr_count).val();
					if(fees_total_amount == undefined){
					}else{
						let total_discount_amount = 0; //sum of total discount amount
						let total_sub_amount = 0
						let total_gst_amount = 0;
						let component_size = $('#component_size_'+tr_count).val()
						for(let i=0;i<component_size;i++){
							var frequancy = 1;
							// if($('#component_type_'+tr_count+'_'+i).val() == "Annual" && $("#month_id_"+tr_count).val() != "" ){
							if($("#month_id_"+tr_count).val() != "" ){
								// var frequancy_array = $("#month_id_"+tr_count+" option:selected").text().split(" ");
								// frequancy = frequancy_array[0];
								frequancy = $("#month_id_"+tr_count+" option:selected").attr('month_frequency');
								frequancy_name = $("#month_id_"+tr_count+" option:selected").text();
							}
							$('#component_fees_after_frequency_'+tr_count+'_'+i).html("<span class='text-danger'>( "+$('#actual_component_fees_'+tr_count+'_'+i).val()+"  X  "+frequancy_name+" </span>)");
							if(discount_amount != 0){
								var is_discount = $('#isdiscountable_'+tr_count+'_'+i).val()		
								if(is_discount == 'Yes'){
									let component_fees = $('#fees_'+tr_count+'_'+i).val()
									let discount_amount_inpercentage = (parseFloat(component_fees) * parseFloat(discount_amount))/100;
									$('#discountamount_'+tr_count+'_'+i).val(parseFloat(discount_amount_inpercentage)*parseFloat(frequancy));
									if(discount_amount_inpercentage <= component_fees){
										total_discount_amount = parseFloat(total_discount_amount) + discount_amount_inpercentage
										$('#total_discount_amount_'+tr_count).val(parseFloat(total_discount_amount)*parseFloat(frequancy))
										let is_gst = $('#isgst_'+tr_count+'_'+i).val()
										if(is_gst == 'Yes'){
											let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage))
											$('#componentnewfees_'+tr_count+'_'+i).val(parseFloat(component_new_fees)*parseFloat(frequancy))
											let tax = $('#tax_'+tr_count+'_'+i).val()
											let component_gst_fees = ((parseFloat(component_new_fees) * parseFloat(tax))/100).toFixed(2);
											$('#componentgstfees_'+tr_count+'_'+i).val(parseFloat(component_gst_fees).toFixed(2))
											$('#gstamount_'+tr_count+'_'+i).val(component_gst_fees)
										}else{
											let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage))
											$('#componentnewfees_'+tr_count+'_'+i).val(parseFloat(component_new_fees).toFixed(2))
											let component_gst_fees = 0
											$('#componentgstfees_'+tr_count+'_'+i).val(parseFloat(component_gst_fees).toFixed(2))
											$('#gstamount_'+tr_count+'_'+i).val(0)
											$('#originalgstfees_'+tr_count+'_'+i).val(0)
										}
										let component_new_fees = $('#componentnewfees_'+tr_count+'_'+i).val()
										let component_gst_fees = $('#componentgstfees_'+tr_count+'_'+i).val()
										let subamount = ((parseFloat(component_new_fees) - parseFloat(component_gst_fees))).toFixed(2)
										$('#subamount_'+tr_count+'_'+i).val(parseFloat(subamount)*parseFloat(frequancy))
										$('#component_fees_'+tr_count+'_'+i).html(parseFloat($('#actual_component_fees_'+tr_count+'_'+i).val())*parseFloat(frequancy))
									}else{
										$('#discountamount_'+tr_count+'_'+i).val(0);
										let component_fees = $('#componentgstfees_'+tr_count+'_'+i).val()
										let gst_amount = ($('#originalgstfees_'+tr_count+'_'+i).val()!="")?$('#originalgstfees_'+tr_count+'_'+i).val():0;
										$("#gstamount_"+tr_count+'_'+i).val(gst_amount)
										let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
										$('#subamount_'+tr_count+'_'+i).val(parseFloat(subamount)*parseFloat(frequancy))
										$('#component_fees_'+tr_count+'_'+i).html(parseFloat($('#actual_component_fees_'+tr_count+'_'+i).val())*parseFloat(frequancy))
									}
								}else{
									$('#discountamount_'+tr_count+'_'+i).val(0);
									let component_fees = $('#componentgstfees_'+tr_count+'_'+i).val()
									let gst_amount = ($('#originalgstfees_'+tr_count+'_'+i).val()!="")?$('#originalgstfees_'+tr_count+'_'+i).val():0;
									$("#gstamount_"+tr_count+'_'+i).val(gst_amount)
									let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
									$('#subamount_'+tr_count+'_'+i).val(parseFloat(subamount)*parseFloat(frequancy))
									$('#component_fees_'+tr_count+'_'+i).html(parseFloat($('#actual_component_fees_'+tr_count+'_'+i).val())*parseFloat(frequancy))
								}
							}else{
								$('#total_discount_amount_'+tr_count).val('')
								$('#discountamount_'+tr_count+'_'+i).val(0);
								let component_fees = $('#componentfees_'+tr_count+'_'+i).val()
								let gst_amount = ($('#originalgstfees_'+tr_count+'_'+i).val()!="")?$('#originalgstfees_'+tr_count+'_'+i).val():0;
								$("#gstamount_"+tr_count+'_'+i).val(gst_amount)
								let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
								$('#subamount_'+tr_count+'_'+i).val(parseFloat(subamount)*parseFloat(frequancy))
								$('#component_fees_'+tr_count+'_'+i).html(parseFloat($('#actual_component_fees_'+tr_count+'_'+i).val())*parseFloat(frequancy))
							}
							let gst_amount = ($('#gstamount_'+tr_count+'_'+i).val()!="")?$('#gstamount_'+tr_count+'_'+i).val():0;
							total_gst_amount = total_gst_amount + parseFloat(gst_amount)
							$('#total_gst_amount_'+tr_count).val(total_gst_amount)
							let sub_amount = $('#subamount_'+tr_count+'_'+i).val()
							total_sub_amount = total_sub_amount + parseFloat(sub_amount)
							$('#total_sub_amount_'+tr_count).text((total_sub_amount).toFixed(2))
							$('#t_amount_'+tr_count).text((parseFloat($('#actual_component_fees_'+tr_count+'_'+i).val())*parseFloat(frequancy)).toFixed(2))
						}
					}
					var amount_after_discount = Number(fees_total_amount) - Number(discount_amount);
					let totalsubamount = $('#total_sub_amount_'+tr_count).text()
					$('#total_amount_'+tr_count).val(totalsubamount)
					$('#fees_amount_collected_'+tr_count).val(totalsubamount)
					let total_discount_amount = $('#total_discount_amount_'+tr_count).val()
					$('#total_discount_amount_'+tr_count).val(total_discount_amount)
					let discount_amount_rs = $('#total_discount_amount_'+tr_count).val()
					let mandatory_component_amount = $('#mandatory_component_amount_'+tr_count).val()
					let mandatory_discount_amount = parseFloat(mandatory_component_amount) - parseFloat(discount_amount_rs)
					$('#mandatory_component_amount_'+tr_count).val(mandatory_discount_amount)
				}
			});
		}else{
			let total_discount_amount = 0; //sum of total discount amount
			let total_sub_amount = 0
			let total_gst_amount = 0;
			let discount_amount = 0
			let frequancy  = 1;
			let component_size = $('#component_size_'+tr_count).val()
			for(let i=0;i<component_size;i++){
				if(discount_amount == 0){
					$('#total_discount_amount_'+tr_count).val('')
					$('#discountamount_'+tr_count+'_'+i).val(0)
					let component_fees = $('#componentfees_'+tr_count+'_'+i).val()
					let gst_amount = ($('#originalgstfees_'+tr_count+'_'+i).val()!="")?$('#originalgstfees_'+tr_count+'_'+i).val():0;
					$("#gstamount_"+tr_count+'_'+i).val(gst_amount)
					let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
					$('#subamount_'+tr_count+'_'+i).val(parseFloat(subamount)*parseFloat(frequancy))
				}
				$('#component_fees_'+tr_count+'_'+i).html($('#actual_component_fees_'+tr_count+'_'+i).val())
				$('#component_fees_after_frequency_'+tr_count+'_'+i).html("");
				let gst_amount = ($('#gstamount_'+tr_count+'_'+i).val()!="")?$('#gstamount_'+tr_count+'_'+i).val():0;
				total_gst_amount = total_gst_amount + parseFloat(gst_amount)
				$('#total_gst_amount_'+tr_count).val(total_gst_amount)
				let sub_amount = $('#subamount_'+tr_count+'_'+i).val()
				total_sub_amount = total_sub_amount + parseFloat(sub_amount)
				$('#total_sub_amount_'+tr_count).text((total_sub_amount).toFixed(2))
			}
			var amount_after_discount = Number(fees_total_amount) - Number(discount_amount);
			totalsubamount = $('#total_sub_amount_'+tr_count).text()
			$('#total_amount_'+tr_count).val(totalsubamount)
			$('#fees_amount_collected_'+tr_count).val(totalsubamount)
			total_discount_amount = $('#total_discount_amount_'+tr_count).val()
			$('#total_discount_amount_'+tr_count).val(total_discount_amount)
			let discount_amount_rs = $('#total_discount_amount_'+tr_count).val()
			let mandatory_component_amount = $('#mandatory_component_amount_'+tr_count).val()
			let mandatory_discount_amount = parseFloat(mandatory_component_amount) - parseFloat(discount_amount_rs)
			$('#mandatory_component_amount_'+tr_count).val(mandatory_discount_amount)
		}
	}

	//for group installment 
	function addGroupInstallment(elem){
		var row_count = $(elem).closest("tr").attr("tr_count");
		$("#group_installment_table_tbody_"+row_count).children().remove();
		var max_installment = $('#group_max_installment_count_'+row_count).val()
		var installmentNo = $(elem).val()
		if(parseInt(installmentNo) > parseInt(max_installment)){
			alert("Installment No should not greater than Max Installment.")
			$(elem).val("");
			elem.prevenDefault()
			return false;
		}
		
		if(installmentNo == ''){
			$("#group_installment_table_tbody_"+row_count).children().remove();
			$("#group_installment_table_"+row_count).hide();
		}else{

			if(installmentNo > max_installment){
				$('.installment_error').text('Installment No should not greater than Max Installment');
				return false;
			}else{
				$('.installment_error').text('') 
				for(let i=0;i<installmentNo;i++){
					let totalsubamnt = $('#total_sub_amount_'+row_count).text()
					let installment_amount = (totalsubamnt/installmentNo).toFixed(2)
					var due_html = '<tr>'+
								'<td >'+
									'<span>'+(i+1)+'</span>'+
								'</td>'+
								'<td>'+
									'<div class="form-group">'+
										'<input type="text" class="form-control monthPicker" placeholder="Select Date" id="group_installmentdate_'+row_count+'_'+i+'" name="group_instalment_due_date['+row_count+']['+i+']">'+
									'</div>'+
									'<span class="installmenterror_'+row_count+'_'+i+'" text-danger"></span>'+
								'</td>'+
								'<td>'+
									'<div class="form-group">'+
										'<input type="text" class="form-control" id="group_instalment_amount_'+row_count+'_'+i+'"  name="group_instalment_amount['+row_count+']['+i+']" readonly="readonly">'+
									'</div>'+
								'</td>'+
							'</tr>';
					$("#group_installment_table_"+row_count).show();
					$("#group_installment_table_tbody_"+row_count).append(due_html);
					$("#group_instalment_amount_"+row_count+"_"+i).val(installment_amount)
				}
				$(".monthPicker").datetimepicker({ 
					viewMode: 'months',
					format: 'DD-MM-YYYY',
				});
			}
		}
	}
	
/* ------------------------------------------group code end---------------------------------- */
	$( document ).ready(function() {
		$('.component_show').hide()
		$('.save').hide()
		$('.course_amount').hide()
		$('.paymentprocess').hide()
		$('.skip').click(function(){
			let month_array = []
			let installmentNo = $('.no_of_installments').val()
			for(let i=0;i<installmentNo;i++){
				if($('#installmentdate_'+i).val() == ''){
					$('.installmenterror_'+i).text('Select Date');
					return 
				}
				else{
					let monthValue = $('#installmentdate_'+i).val()
					month_array.push(monthValue)
				}
			}
			if($('.install').is(':checked') == false){
				$('.course_amount').show()
			}
			$(".f_table").children().remove();
			$('.save').show()
			$(this).hide()
			$('.grpFeesBtn').hide()
			$('.paymentprocess').show()
			$('.cheque').hide();
			$('.transaction').hide();
			$('.both').hide();

			// if($('.install').is(':checked') == true){
			// 	for(let i=0;i<installmentNo;i++){
			// 		let totalsubamnt = $('.total_sub_amount').text()
			// 		let installment_amount = (totalsubamnt/installmentNo).toFixed(2)
			// 	var due_html = '<tr>'+
			// 						'<td>'+
			// 							'<span>'+(i+1)+'</span>'+
			// 						'</td>'+
			// 						'<td>'+
			// 							'<div class="form-group">'+
			// 								'<input type="text" class="form-control monthPicker installment-month-picker" placeholder="Select Date" id="installmentdate_'+i+'" name="instalment_due_date[]">'+
			// 							'</div>'+
			// 							'<span class="installmenterror_'+i+' text-danger"></span>'+
			// 						'</td>'+
			// 						'<td>'+
			// 							'<div class="form-group">'+
			// 								'<input type="text" class="form-control instalment_amount"  name="instalment_amount" readonly="readonly">'+
			// 							'</div>'+
			// 						'</td>'+
			// 						'<td>'+
			// 							'<div class="form-group">'+
			// 								'<input type="text" class="form-control instalment_collected_amount number_decimal_only" id="instalmentcollectedamount_'+i+'" name="instalment_collected_amount[]">'+
			// 							'</div>'+
			// 						'</td>'+
			// 						'<td>'+
			// 							'<div class="form-group">'+
			// 								'<input type="text" class="form-control instalment_remaining_amount"  id="instalmentremainingamount_'+i+'"name="instalment_remaining_amount[]" readonly="readonly">'+
			// 							'</div>'+
			// 						'</td>'+
			// 					'</tr>';
			// 		$(".f_table").append(due_html);
			// 		// $('#installmentdate_'+i).val(month_array[i])
			// 		$('.instalment_amount').val(installment_amount)
			// 		$('.instalment_collected_amount').val(0)
			// 		$('.instalment_remaining_amount').val(installment_amount)
			// 		$('.instalment_collected_amount').keyup(function(){
			// 			let collected_amount = $(this).val()
			// 			let installment_amount = $('.instalment_amount').val()

			// 			let remaining_amount_input = $(this).parent().parent().next().find('.instalment_remaining_amount')
			// 			if(parseInt(collected_amount) > parseInt(installment_amount)){
			// 				remaining_amount_input.val(installment_amount);
			// 			}
			// 			else{				
			// 				let remaining_amount = ($('.instalment_amount').val() - collected_amount).toFixed(2)
			// 				remaining_amount_input.val(remaining_amount);
			// 			}
			// 		})
			// 		$("#installmentdate_"+i).datetimepicker({ 
			// 			viewMode: 'months',
			// 			format: 'DD-MM-YYYY',
			// 		});
			// 	}
			// }
		});
		$(document).on("change",".installment-month-picker",function(){
			var installmentNo = $('.no_of_installments').val()
		})

		$("#programme_start_date").datepicker({
			format: "dd-mm-yyyy",
			autoclose: true
		});
		$("#dob").datepicker({
			format: "dd-mm-yyyy",
			autoclose: true
		});
		$('#programme_start_date').change(function(){
			var dob = $('#dob').val();
			$('#age_on').val($(this).val())
			calcDate($(this).val(),dob)
		})


		$('#dob').change(function(){
			var age_on = $('#age_on').val();

			calcDate(age_on,$(this).val())
		})
		<?php
			if(!empty($details[0]->student_id)){
		?>
			getCenters('<?php echo $details[0]->zone_id; ?>', '<?php echo $details[0]->center_id; ?>');
			getCourses('<?php echo $otherdetails[0]->category_id; ?>', '<?php echo $otherdetails[0]->course_id; ?>');
			getBatch('<?php echo $otherdetails[0]->course_id; ?>','<?php echo $details[0]->center_id; ?>','<?php echo $otherdetails[0]->batch_id; ?>');
			getBatchDetails('<?php echo $otherdetails[0]->batch_id; ?>');
			$("#s2id_state_id a .select2-chosen").text('<?php echo $getStateName[0]->state_name?>');
			$("#s2id_city_id a .select2-chosen").text('<?php echo $getCityName[0]->city_name?>');
		<?php
		} ?>
		<?php
			if($details[0]->has_attended_preschool_before == 'Yes'){ ?>
				$('.preschoolNameDiv').show();
			<?php
			}else{ ?>
				$('.preschoolNameDiv').hide();
			<?php
			} 
		?>
		$('.has_attended_preschool_before').change(function(){
			var has_attended_preschool_before = $(this).val();
			if(has_attended_preschool_before == 'Yes'){
				$('.preschoolNameDiv').show();
			}
			else{
				$('.preschoolNameDiv').hide();
			}
		})
		$(".datepicker").datetimepicker({
			format: 'DD-MM-YYYY',
		});
		$(".monthPicker").datetimepicker({ 
			viewMode: 'months',
			format: 'DD-MM-YYYY',
		});
		installmentprocess()
	});

	$(document).on('keypress','.number_decimal_only',function(e){
		if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	})

	$('#fees_amount_collected1').keyup(function(){
		let collected_amount = $(this).val()
		if($('.install').is(':checked') == true){
			let total_amount = $('#total_amount_installment').val()
		}
		else{
			let total_amount = $('#total_amount').val()
		}
		if(collected_amount != '' || collected_amount != undefined){
			if(parseFloat(collected_amount) > total_amount){
				$('.collectedamountError').text('Course Amount not greater than Total Amount')
				return
			}
			else{
				$('.collectedamountError').text('')
			}
		}
	})

	function changeDiscountType(id){
		$('#'+id).trigger("keyup");
	}

	//change discount for multiple group
	function changeGroupDiscountType(row_count){
		var discount_amount  = parseFloat($("#group_discount_amount_percentage_"+row_count).val());
		$('#total_discount_amount_'+row_count).val(0);
		if(discount_amount !="undefined"){
			if(isNaN(discount_amount)){
				discount_amount = 0;
			}
			if($("#group_discount_type_"+row_count).val() == "percentage"){
				if(discount_amount > 100){
					alert("Percentage cant be greater than 100");
					$("#group_installment_table_"+row_count).hide();
					$("#group_installment_count_"+row_count).val("");
					$("#group_discount_amount_percentage_"+row_count).val(0);
					getGroupFeesDetails(row_count)
					return false;
				}
				var fees_total_amount = parseFloat($('#total_amount_'+row_count).val());
				if(fees_total_amount == undefined){
					return false;
				}else{
					let total_discount_amount = 0; //sum of total discount amount
					let total_sub_amount = 0;
					let total_gst_amount = 0;
					let component_size = $('#component_size_'+row_count).val();
					for(let i=0;i<component_size;i++){
						if(discount_amount != 0 && discount_amount != ""){
							var is_discount = $('#isdiscountable_'+row_count+'_'+i).val();
							if(is_discount == 'Yes'){
								let component_fees = $('#fees_'+row_count+'_'+i).val();
								let discount_amount_inpercentage = (parseFloat(component_fees) * parseFloat(discount_amount))/100;
								$('#discountamount_'+row_count+'_'+i).val(parseFloat(discount_amount_inpercentage))
								if(discount_amount_inpercentage <= component_fees){
									total_discount_amount = parseFloat(total_discount_amount) + parseFloat(discount_amount_inpercentage);
									$('#total_discount_amount_'+row_count).val(parseFloat(total_discount_amount).toFixed(2))
									let is_gst = $('#isgst_'+row_count+'_'+i).val();
									if(is_gst == 'Yes'){
										let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage));
										$('#componentnewfees_'+row_count+'_'+i).val(component_new_fees);
										let tax = $('#tax_'+row_count+'_'+i).val();
										let component_gst_fees = ((parseFloat(component_new_fees) * parseFloat(tax))/100).toFixed(2);
										$('#componentgstfees_'+row_count+'_'+i).val(component_gst_fees);
										$('#gstamount_'+row_count+'_'+i).val(component_gst_fees);
									}
									else{
										let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage));
										$('#componentnewfees_'+row_count+'_'+i).val(component_new_fees);
										let component_gst_fees = 0;
										$('#componentgstfees_'+row_count+'_'+i).val(component_gst_fees);
										$('#gstamount_'+row_count+'_'+i).val(0);
										$('#originalgstfees_'+row_count+'_'+i).val(0);
									}
									let component_new_fees = $('#componentnewfees_'+row_count+'_'+i).val();
									let component_gst_fees = ($('#componentgstfees_'+row_count+'_'+i).val() != "")?$('#componentgstfees_'+row_count+'_'+i).val():0;
									let subamount = ((parseFloat(component_new_fees) - parseFloat(component_gst_fees))).toFixed(2);
									$('#subamount_'+row_count+'_'+i).val(parseFloat(subamount).toFixed(2));
								}else{
									$('#discountamount_'+row_count+'_'+i).val(0);
									let component_fees = $('#componentgstfees_'+row_count+'_'+i).val();
									let gst_amount = ($('#originalgstfees_'+row_count+'_'+i).val() != "")?$('#originalgstfees_'+row_count+'_'+i).val():0;
									$("#gstamount_"+i).val(gst_amount);
									let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2);
									$('#subamount_'+row_count+'_'+i).val(parseFloat(subamount).toFixed(2));
								}
							}
							else{
								$('#discountamount_'+row_count+'_'+i).val(0);
								let component_fees = $('#componentgstfees_'+row_count+'_'+i).val();
								let gst_amount = ($('#originalgstfees_'+row_count+'_'+i).val()!=""?$('#originalgstfees_'+row_count+'_'+i).val():0);
								$('#gstamount_'+row_count+'_'+i).val(parseFloat(gst_amount));
								let subamount = (parseFloat(component_fees) - parseFloat(gst_amount)).toFixed(2);
								$('#subamount_'+row_count+'_'+i).val(parseFloat(subamount).toFixed(2));
							}
						}
						else{
							$('#total_discount_amount_'+row_count).val('');
							$('#discountamount_'+row_count+'_'+i).val(0);
							let component_fees = $('#componentfees_'+row_count+'_'+i).val();
							let gst_amount = ($('#originalgstfees_'+row_count+'_'+i).val() != "")?$('#originalgstfees_'+row_count+'_'+i).val():0;
							$('#gstamount_'+row_count+'_'+i).val(parseFloat(gst_amount));
							let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2);
							$('#subamount_'+row_count+'_'+i).val(parseFloat(subamount).toFixed(2));
						}
						let gst_amount = $('#gstamount_'+row_count+'_'+i).val();
						total_gst_amount = parseFloat(total_gst_amount) + parseFloat(gst_amount);
						$('#total_gst_amount_'+row_count).val(parseFloat(total_gst_amount));
						let sub_amount = $('#subamount_'+row_count+'_'+i).val();
						total_sub_amount = parseFloat(total_sub_amount) + parseFloat(sub_amount);
						$('#total_sub_amount_'+row_count).text((parseFloat(total_sub_amount)).toFixed(2))
					}
				}
				var amount_after_discount = parseFloat(fees_total_amount) - parseFloat(discount_amount);
				let totalsubamount = $('#total_sub_amount_'+row_count).text();
				$('#total_amount_'+row_count).val(parseFloat(totalsubamount).toFixed(2));
				let installmentNo = $('#group_installment_count_'+row_count).val();
				let installment_amount = (parseFloat(amount_after_discount)/parseFloat(installmentNo)).toFixed(2);
				$('#fees_amount_collected').val(parseFloat(totalsubamount));
				let total_discount_amount = $('#total_discount_amount_'+row_count).val();
				$('#total_discount_amount_'+row_count).val(parseFloat(total_discount_amount).toFixed(2));
				let discount_amount_rs = $('#total_discount_amount_'+row_count).val();
				let mandatory_component_amount = $('#mandatory_component_amount_'+row_count).val();
				let mandatory_discount_amount = parseFloat(mandatory_component_amount) - parseFloat(discount_amount_rs);
				$('#mandatory_component_amount_'+row_count).val(parseFloat(mandatory_discount_amount));
				let installmentno = $('#group_installment_count_'+row_count).val();
				let installamount = (totalsubamount/installmentno).toFixed(2);
				for(let i=0;i<installmentNo;i++){
					$('#group_instalment_amount_'+row_count+'_'+i).val(parseFloat(installamount).toFixed(2));
				}
			}else if($("#group_discount_type_"+row_count).val() == "rupees"){
				var fees_total_amount = parseFloat($('#total_amount_'+row_count).val());
				if(fees_total_amount == undefined){
					return false;
				}else{
					if(discount_amount > parseFloat($('#t_amount_'+row_count).html())){
						alert("Discount cant greater than Total Amount");
						$("#group_discount_amount_percentage_"+row_count).val("");
						getGroupFeesDetails(row_count);
						return false;
					}
					let total_discount_amount = 0; //sum of total discount amount
					let total_sub_amount = 0;
					let total_gst_amount = 0;
					let component_size = $('#component_size_'+row_count).val();
					discount_amount_per_component = 0;
					for(let d=0;d<component_size;d++){
						var is_discount = $('#isdiscountable_'+row_count+'_'+d).val()
						if(is_discount == 'Yes'){
							var discount_amount_per_component = (discount_amount_per_component + parseFloat($('#component_fees_'+row_count+'_'+d).html()));
						}
					}
					for(let i=0;i<component_size;i++){
						if(discount_amount != 0 && discount_amount != ""){
							var is_discount = $('#isdiscountable_'+row_count+'_'+i).val();
							if(is_discount == 'Yes'){
								let component_fees = $('#fees_'+row_count+'_'+i).val();
								// var discount_amount_per_component = parseFloat($('#t_amount_'+row_count).html());
								let discount_amount_inpercentage = ((parseFloat(component_fees)/discount_amount_per_component) * discount_amount);
								$('#discountamount_'+row_count+'_'+i).val(discount_amount_inpercentage.toFixed(2));
								if(discount_amount_inpercentage <= component_fees){
									total_discount_amount = parseFloat(total_discount_amount) + discount_amount_inpercentage;
									$('#total_discount_amount_'+row_count).val(total_discount_amount.toFixed(2));
									let is_gst = $('#isgst_'+row_count+'_'+i).val();
									if(is_gst == 'Yes'){
										let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage));
										$('#componentnewfees_'+row_count+'_'+i).val(component_new_fees);
										let tax = $('#tax_'+row_count+'_'+i).val();
										let component_gst_fees = ((parseFloat(component_new_fees) * parseFloat(tax))/100).toFixed(2);
										$('#componentgstfees_'+row_count+'_'+i).val(component_gst_fees);
										$('#gstamount_'+row_count+'_'+i).val(component_gst_fees);
									}
									else{
										let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage));
										$('#componentnewfees_'+row_count+'_'+i).val(component_new_fees);
										let component_gst_fees = 0;
										$('#componentgstfees_'+row_count+'_'+i).val(component_gst_fees);
										$('#gstamount_'+row_count+'_'+i).val(0);
										$('#originalgstfees_'+row_count+'_'+i).val(0);
									}
									let component_new_fees = $('#componentnewfees_'+row_count+'_'+i).val();
									let component_gst_fees = $('#componentgstfees_'+row_count+'_'+i).val();
									let subamount = ((parseFloat(component_new_fees) - parseFloat(component_gst_fees))).toFixed(2);
									$('#subamount_'+row_count+'_'+i).val(subamount);
								}
								else{
									$('#discountamount_'+row_count+'_'+i).val(0);
									let component_fees = $('#componentgstfees_'+row_count+'_'+i).val();
									let gst_amount = $('#originalgstfees_'+row_count+'_'+i).val();
									$("#gstamount_"+i).val(gst_amount);
									let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2);
									$('#subamount_'+row_count+'_'+i).val(subamount);
								}
							}else{
								$('#discountamount_'+row_count+'_'+i).val(0);
								let component_fees = $('#componentgstfees_'+row_count+'_'+i).val();
								let gst_amount = ($('#originalgstfees_'+row_count+'_'+i).val()!=""?$('#originalgstfees_'+row_count+'_'+i).val():0);
								$("#gstamount_"+i).val(parseFloat(gst_amount));
								let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2);
								$('#subamount_'+row_count+'_'+i).val(parseFloat(subamount));
							}
						}else{
							$('#total_discount_amount'+row_count).val('');
							$('#discountamount_'+row_count+'_'+i).val(0);
							let component_fees = $('#componentfees_'+row_count+'_'+i).val();
							let gst_amount = ($('#originalgstfees_'+row_count+'_'+i).val() != "")?$('#originalgstfees_'+row_count+'_'+i).val():0;
							$('#gstamount_'+row_count+'_'+i).val(gst_amount);
							let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2);
							$('#subamount_'+row_count+'_'+i).val(parseFloat(subamount).toFixed(2));
						}
						let gst_amount = $('#gstamount_'+row_count+'_'+i).val();
						total_gst_amount = parseFloat(total_gst_amount) + parseFloat(gst_amount);
						$('#total_gst_amount_'+row_count).val(parseFloat(total_gst_amount));
						let sub_amount = $('#subamount_'+row_count+'_'+i).val();
						total_sub_amount = parseFloat(total_sub_amount) + parseFloat(sub_amount);
						$('#total_sub_amount_'+row_count).text((parseFloat(total_sub_amount)).toFixed(2))
					}
				}
				var amount_after_discount = parseFloat(fees_total_amount) - parseFloat(discount_amount);
				let totalsubamount = $('#total_sub_amount_'+row_count).text();
				$('#total_amount_'+row_count).val(parseFloat(totalsubamount).toFixed(2));
				let installmentNo = $('#group_installment_count_'+row_count).val();
				let installment_amount = (amount_after_discount/installmentNo).toFixed(2);
				$('#fees_amount_collected_'+row_count).val(totalsubamount);
				let total_discount_amount = $('#total_discount_amount_'+row_count).val();
				$('#total_discount_amount_'+row_count).val(parseFloat(total_discount_amount).toFixed(2));
				let discount_amount_rs = $('#total_discount_amount_'+row_count).val();
				$('#group_instalment_amount_'+row_count+'_0').val(parseFloat(installment_amount).toFixed(2));
				let mandatory_component_amount = $('#mandatory_component_amount_'+row_count).val();
				let mandatory_discount_amount = parseFloat(mandatory_component_amount) - parseFloat(discount_amount_rs);
				$('#mandatory_component_amount_'+row_count).val(mandatory_discount_amount);
				let installmentno = $('#group_installment_count_'+row_count).val();
				let installamount = (totalsubamount/installmentno).toFixed(2);
				let component_size = $('#component_size_'+row_count).val();
				for(let i=0;i<installmentNo;i++){
					$('#group_instalment_amount_'+row_count+'_'+i).val(parseFloat(installamount).toFixed(2));
				}
			}
		}
	}

	function installmentprocess(){
		$('.cheque').hide();
		$('.transaction').hide();
		$('.both').hide();

		$(".datepicker").datetimepicker({
			format: 'DD-MM-YYYY',
		});

		$('.installment').hide();
		
        $('.is_instalment').change(function(){

			$('#discount_amount_percentage_installment').val('');
			$('#total_discount_amount_installment').val('');
			$('#no_of_installments').val('');
			   
			$('#discount_amount_percentage').val('');
			$('#total_discount_amount').val('');
			var is_instalment = $(this).val();
			if(is_instalment == 'Yes'){
				$('.installment').show();
				$('.lumsum').hide();
			}
			else{
				$('.installment').hide();
				$('.lumsum').show();
			}
		});

		$('#discount_amount_percentage').keyup(function(){
			var discount_amount = parseFloat($("#discount_amount_percentage").val());
			if(discount_amount != "undefined"){
				if(isNaN(discount_amount)){
					discount_amount = 0;
				}
				if($("#discount_type").val() == "percentage"){
					if($(this).val() > 100){
						alert("Percentage cant be greater than 100");
						$(this).val("");
						getFeesDetails($("#fees_id").val());
						return false;
					}
					$('#fees_amount_collected1').val('')
					var fees_total_amount = parseFloat($('#fees_total_amount').val());
					if(fees_total_amount == undefined){
						return false;
					}else{
						let total_discount_amount = 0; //sum of total discount amount
						let total_sub_amount = 0
						let total_gst_amount = 0;
						let component_size = $('#component_size').val()
						for(let i=0;i<component_size;i++){
							if(discount_amount != 0 && discount_amount != ""){
								var is_discount = $('#isdiscountable_'+i).val()
								if(is_discount == 'Yes'){
									let component_fees = $('#fees_'+i).val()
									let discount_amount_inpercentage = (parseFloat(component_fees) * parseFloat(discount_amount))/100;
									$('#discountamount_'+i).val(parseFloat(discount_amount_inpercentage))
									if(discount_amount_inpercentage <= component_fees){
										
										total_discount_amount = parseFloat(total_discount_amount) + parseFloat(discount_amount_inpercentage);
										$('#total_discount_amount').val(parseFloat(total_discount_amount).toFixed(2))
										let is_gst = $('#isgst_'+i).val()
										if(is_gst == 'Yes'){
											let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage))
											$('#componentnewfees_'+i).val(component_new_fees)
											let tax = $('#tax_'+i).val()
											let component_gst_fees = ((parseFloat(component_new_fees) * parseFloat(tax))/100).toFixed(2);
											$('#componentgstfees_'+i).val(component_gst_fees)
											$('#gstamount_'+i).val(component_gst_fees)
										}
										else{
											let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage))
											$('#componentnewfees_'+i).val(component_new_fees)
											let component_gst_fees = 0
											$('#componentgstfees_'+i).val(component_gst_fees)
											$('#gstamount_'+i).val(0)
											$('#originalgstfees_'+i).val(0)
										}
										let component_new_fees = $('#componentnewfees_'+i).val()
										let component_gst_fees = ($('#componentgstfees_'+i).val() != "")?$('#componentgstfees_'+i).val():0;
										let subamount = ((parseFloat(component_new_fees) - parseFloat(component_gst_fees))).toFixed(2)
										$('#subamount_'+i).val(parseFloat(subamount).toFixed(2))
									}
									else{
										$('#discountamount_'+i).val(0)
										let component_fees = $('#componentgstfees_'+i).val()
										let gst_amount = ($('#originalgstfees_'+i).val() != "")?$('#originalgstfees_'+i).val():0;
										$("#gstamount_"+i).val(gst_amount)
										let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
										$('#subamount_'+i).val(parseFloat(subamount).toFixed(2))
									}
								}
								else{
									$('#discountamount_'+i).val(0)
									let component_fees = $('#componentgstfees_'+i).val()
									let gst_amount = ($('#originalgstfees_'+i).val()!=""?$('#originalgstfees_'+i).val():0);
									$("#gstamount_"+i).val(parseFloat(gst_amount))
									let subamount = (parseFloat(component_fees) - parseFloat(gst_amount)).toFixed(2);
									$('#subamount_'+i).val(parseFloat(subamount).toFixed(2))
								}
							}
							else{
								$('#total_discount_amount').val('')
								$('#discountamount_'+i).val(0)
								let component_fees = $('#componentfees_'+i).val()
								let gst_amount = ($('#originalgstfees_'+i).val() != "")?$('#originalgstfees_'+i).val():0;
								// if(gst_amount == '' || gst_amount == undefined){
								// 	let gst_amount = '0'
								// }
								$("#gstamount_"+i).val(parseFloat(gst_amount))
								let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
								$('#subamount_'+i).val(parseFloat(subamount).toFixed(2))
							}
							let gst_amount = $('#gstamount_'+i).val()
							total_gst_amount = parseFloat(total_gst_amount) + parseFloat(gst_amount)
							$('#total_gst_amount').val(parseFloat(total_gst_amount))
							let sub_amount = $('#subamount_'+i).val()
							total_sub_amount = parseFloat(total_sub_amount) + parseFloat(sub_amount)
							$('.total_sub_amount').text((parseFloat(total_sub_amount)).toFixed(2))
						}
					}
					var amount_after_discount = parseFloat(fees_total_amount) - parseFloat(discount_amount);
					let totalsubamount = $('.total_sub_amount').text()
					$('#total_amount').val(parseFloat(totalsubamount).toFixed(2))
					let installmentNo = $('.no_of_installments').val()
					let installment_amount = (parseFloat(amount_after_discount)/parseFloat(installmentNo)).toFixed(2)
					$('#fees_amount_collected').val(parseFloat(totalsubamount))
					let total_discount_amount = $('#total_discount_amount').val()
					$('#total_discount_amount').val(parseFloat(total_discount_amount).toFixed(2))
					let discount_amount_rs = $('#total_discount_amount').val()
					$('.instalment_amount').val(parseFloat(installment_amount).toFixed(2))
					let mandatory_component_amount = $('#mandatory_component_amount').val()
					let mandatory_discount_amount = parseFloat(mandatory_component_amount) - parseFloat(discount_amount_rs)
					$('#mandatory_component_amount').val(parseFloat(mandatory_discount_amount))
					// $('.instalment_collected_amount').val(0)
					// $('.instalment_remaining_amount').val(installment_amount)
					let installmentno = $('.no_of_installments').val()
					let installamount = (totalsubamount/installmentno).toFixed(2)
					$('.instalment_amount').val(parseFloat(installamount).toFixed(2))
				}else if($("#discount_type").val() == "rupees"){
					$('#fees_amount_collected1').val('')
					var fees_total_amount = parseFloat($('#fees_total_amount').val());
					if(fees_total_amount == undefined){
						return false;
					}else{
						if($(this).val() > parseFloat($(".t_amount").html())){
							alert("Discount cant greater than Total Amount");
							getFeesDetails($("#fees_id").val());
							$(this).val("");
							return false;
						}
						let total_discount_amount = 0; //sum of total discount amount
						let total_sub_amount = 0
						let total_gst_amount = 0;
						let component_size = $('#component_size').val();
						var discount_amount_per_component = 0;
						for(let d=0;d<component_size;d++){
							var is_discount = $('#isdiscountable_'+d).val()
							if(is_discount == 'Yes'){
								var discount_amount_per_component = (discount_amount_per_component + parseFloat($("#actual_component_fee_"+d).html()));
							}
						}
						for(let i=0;i<component_size;i++){
							if(discount_amount != 0 && discount_amount != ""){
								var is_discount = $('#isdiscountable_'+i).val()
								if(is_discount == 'Yes'){
									let component_fees = $('#fees_'+i).val();
									// var discount_amount_per_component = parseFloat($(".t_amount").html());
									// alert(discount_amount_per_component)
									let discount_amount_inpercentage = ((parseFloat(component_fees)/discount_amount_per_component) * discount_amount);
									// alert(discount_amount_inpercentage);
									$('#discountamount_'+i).val(discount_amount_inpercentage.toFixed(2))
									if(discount_amount_inpercentage <= component_fees){
										total_discount_amount = parseFloat(total_discount_amount) + discount_amount_inpercentage
										$('#total_discount_amount').val(total_discount_amount.toFixed(2))
										let is_gst = $('#isgst_'+i).val()
										if(is_gst == 'Yes'){
											let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage))
											$('#componentnewfees_'+i).val(component_new_fees)
											let tax = $('#tax_'+i).val()
											let component_gst_fees = ((parseFloat(component_new_fees) * parseFloat(tax))/100).toFixed(2);
											$('#componentgstfees_'+i).val(component_gst_fees)
											$('#gstamount_'+i).val(component_gst_fees)
										}
										else{
											let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage))
											$('#componentnewfees_'+i).val(component_new_fees)
											let component_gst_fees = 0
											$('#componentgstfees_'+i).val(component_gst_fees)
											$('#gstamount_'+i).val(0)
											$('#originalgstfees_'+i).val(0)
										}
										let component_new_fees = $('#componentnewfees_'+i).val()
										let component_gst_fees = $('#componentgstfees_'+i).val()
										let subamount = ((parseFloat(component_new_fees) - parseFloat(component_gst_fees))).toFixed(2)
										$('#subamount_'+i).val(subamount)
									}
									else{
										$('#discountamount_'+i).val(0)
										let component_fees = $('#componentgstfees_'+i).val()
										let gst_amount = $('#originalgstfees_'+i).val()
										$("#gstamount_"+i).val(gst_amount)
										let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
										$('#subamount_'+i).val(subamount)
									}
								}else{
									$('#discountamount_'+i).val(0)
									let component_fees = $('#componentgstfees_'+i).val()
									let gst_amount = ($('#originalgstfees_'+i).val()!=""?$('#originalgstfees_'+i).val():0);
									$("#gstamount_"+i).val(parseFloat(gst_amount))
									let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
									$('#subamount_'+i).val(parseFloat(subamount))
								}
							}
							else{
								$('#total_discount_amount').val('')
								$('#discountamount_'+i).val(0)
								let component_fees = $('#componentfees_'+i).val()
								let gst_amount = ($('#originalgstfees_'+i).val() != "")?$('#originalgstfees_'+i).val():0;
								$("#gstamount_"+i).val(gst_amount)
								let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
								$('#subamount_'+i).val(parseFloat(subamount).toFixed(2))
							}
							let gst_amount = $('#gstamount_'+i).val()
							total_gst_amount = parseFloat(total_gst_amount) + parseFloat(gst_amount)
							$('#total_gst_amount').val(parseFloat(total_gst_amount))
							let sub_amount = $('#subamount_'+i).val()
							total_sub_amount = parseFloat(total_sub_amount) + parseFloat(sub_amount)
							$('.total_sub_amount').text((parseFloat(total_sub_amount)).toFixed(2))
						}
					}
					var amount_after_discount = parseFloat(fees_total_amount) - parseFloat(discount_amount);
					let totalsubamount = $('.total_sub_amount').text()
					$('#total_amount').val(parseFloat(totalsubamount).toFixed(2))
					let installmentNo = $('.no_of_installments').val()
					let installment_amount = (amount_after_discount/installmentNo).toFixed(2)
					$('#fees_amount_collected').val(totalsubamount)
					let total_discount_amount = $('#total_discount_amount').val()
					$('#total_discount_amount').val(parseFloat(total_discount_amount).toFixed(2))
					let discount_amount_rs = $('#total_discount_amount').val()
					$('.instalment_amount').val(parseFloat(installment_amount).toFixed(2))
					let mandatory_component_amount = $('#mandatory_component_amount').val()
					let mandatory_discount_amount = parseFloat(mandatory_component_amount) - parseFloat(discount_amount_rs)
					$('#mandatory_component_amount').val(mandatory_discount_amount)
					$('.instalment_collected_amount').val(0)
					$('.instalment_remaining_amount').val(installment_amount)
					let installmentno = $('.no_of_installments').val()
					let installamount = (totalsubamount/installmentno).toFixed(2)
					$('.instalment_amount').val(parseFloat(installamount).toFixed(2))
				}
			}
		})

		$('#discount_amount_percentage_installment').keyup(function(){
			var discount_amount = parseFloat($("#discount_amount_percentage_installment").val());
			if(discount_amount !="undefined"){
				if(isNaN(discount_amount)){
					discount_amount = 0;
				}
				if($("#discount_type_installment").val() == "percentage"){
					if($(this).val() > 100){
						alert("Percentage cant be greater than 100");
						var discount_amount = 0;
						$(this).val("");
						getFeesDetails($("#fees_id").val());
						return false;
					}
					$('#fees_amount_collected1').val('')
					var fees_total_amount = parseFloat($('#fees_total_amount').val());
					if(fees_total_amount == undefined){
						return false;
					}else{
						let total_discount_amount = 0; //sum of total discount amount
						let total_sub_amount = 0
						let total_gst_amount = 0;
						let component_size = $('#component_size').val()
						for(let i=0;i<component_size;i++){
							if(discount_amount != 0 && discount_amount != ""){
								var is_discount = $('#isdiscountable_'+i).val()
								if(is_discount == 'Yes'){
									let component_fees = $('#fees_'+i).val()
									let discount_amount_inpercentage = (parseFloat(component_fees) * parseFloat(discount_amount))/100;
									$('#discountamount_'+i).val(discount_amount_inpercentage)
									if(discount_amount_inpercentage <= component_fees){

										total_discount_amount = parseFloat(total_discount_amount) + discount_amount_inpercentage
										$('#total_discount_amount_installment').val(total_discount_amount.toFixed(2))
										let is_gst = $('#isgst_'+i).val()
										if(is_gst == 'Yes'){
											let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage))
											$('#componentnewfees_'+i).val(component_new_fees)
											let tax = $('#tax_'+i).val()
											let component_gst_fees = ((parseFloat(component_new_fees) * parseFloat(tax))/100).toFixed(2);
											$('#componentgstfees_'+i).val(component_gst_fees)
											$('#gstamount_'+i).val(component_gst_fees)
										}
										else{
											let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage))
											$('#componentnewfees_'+i).val(component_new_fees)
											let component_gst_fees = 0
											$('#componentgstfees_'+i).val(component_gst_fees)
											$('#gstamount_'+i).val(0)
											$('#originalgstfees_'+i).val(0)
										}
										let component_new_fees = $('#componentnewfees_'+i).val()
										let component_gst_fees = $('#componentgstfees_'+i).val()
										let subamount = ((parseFloat(component_new_fees) - parseFloat(component_gst_fees))).toFixed(2)
										$('#subamount_'+i).val(subamount)
									}
									else{
										$('#discountamount_'+i).val(0)
										let component_fees = $('#componentgstfees_'+i).val()
										let gst_amount = $('#originalgstfees_'+i).val()
										$("#gstamount_"+i).val(gst_amount)
										let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
										$('#subamount_'+i).val(subamount)
									}
								}
								else{
									$('#discountamount_'+i).val(0)
									let component_fees = $('#componentgstfees_'+i).val()
									let gst_amount = ($('#originalgstfees_'+i).val()!=""?$('#originalgstfees_'+i).val():0);
									$("#gstamount_"+i).val(parseFloat(gst_amount))
									let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
									$('#subamount_'+i).val(parseFloat(subamount))
								}
							}
							else{
								$('#total_discount_amount_installment').val('')
								$('#discountamount_'+i).val(0)
								let component_fees = $('#componentfees_'+i).val()
								let gst_amount = ($('#originalgstfees_'+i).val() != "")?$('#originalgstfees_'+i).val():0;
								$("#gstamount_"+i).val(gst_amount)
								let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
								$('#subamount_'+i).val(parseFloat(subamount).toFixed(2));
							}
							let gst_amount = $('#gstamount_'+i).val()
							total_gst_amount = total_gst_amount + parseFloat(gst_amount)
							$('#total_gst_amount').val(total_gst_amount)
							let sub_amount = $('#subamount_'+i).val()
							total_sub_amount = total_sub_amount + parseFloat(sub_amount)
							$('.total_sub_amount').text((total_sub_amount).toFixed(2))
						}
					}
					var amount_after_discount = parseFloat(fees_total_amount) - parseFloat(discount_amount);
					let totalsubamount = $('.total_sub_amount').text()
					$('#total_amount_installment').val(parseFloat(totalsubamount).toFixed(2))
					let installmentNo = $('.no_of_installments').val()
					let installment_amount = (amount_after_discount/installmentNo).toFixed(2)
					$('#fees_amount_collected').val(parseFloat(totalsubamount).toFixed(2))
					let total_discount_amount = $('#total_discount_amount_installment').val()
					$('#total_discount_amount_installment').val(parseFloat(total_discount_amount).toFixed(2))
					let discount_amount_rs = $('#total_discount_amount_installment').val()
					$('.instalment_amount').val(parseFloat(installment_amount).toFixed(2))
					let mandatory_component_amount = $('#mandatory_component_amount').val()
					let mandatory_discount_amount = parseFloat(mandatory_component_amount) - parseFloat(discount_amount_rs)
					$('#mandatory_component_amount').val(parseFloat(mandatory_discount_amount).toFixed(2))
					$('.instalment_collected_amount').val(0)
					$('.instalment_remaining_amount').val(parseFloat(installment_amount).toFixed(2))
					let installmentno = $('.no_of_installments').val()
					let installamount = (parseFloat(totalsubamount)/parseFloat(installmentno)).toFixed(2)
					$('.instalment_amount').val(parseFloat(installamount).toFixed(2))
				}else if($("#discount_type_installment").val() == "rupees"){
					$('#fees_amount_collected1').val('')
					var fees_total_amount = parseFloat($('#fees_total_amount').val());
					if(fees_total_amount == undefined){
						return false;
					}else{
						if($(this).val() > parseFloat($(".t_amount").html())){
							alert("Discount cant greater than Total Amount");
							$(this).val("");
							getFeesDetails($("#fees_id").val());
							return false;
						}
						let total_discount_amount = 0; //sum of total discount amount
						let total_sub_amount = 0
						let total_gst_amount = 0;
						let component_size = $('#component_size').val()
						var discount_amount_per_component = 0;
						for(let d=0;d<component_size;d++){
							var is_discount = $('#isdiscountable_'+d).val()
							if(is_discount == 'Yes'){
								var discount_amount_per_component = (discount_amount_per_component + parseFloat($("#actual_component_fee_"+d).html()));
							}
						}
						for(let i=0;i<component_size;i++){
							if(discount_amount != 0 && discount_amount != ""){
								var is_discount = $('#isdiscountable_'+i).val()
								if(is_discount == 'Yes'){
									let component_fees = $('#fees_'+i).val()
									// var discount_amount_per_component = parseFloat($(".t_amount").html());
									let discount_amount_inpercentage = ((parseFloat(component_fees)/discount_amount_per_component) * parseFloat(discount_amount));
									$('#discountamount_'+i).val(discount_amount_inpercentage.toFixed(2))
									if(discount_amount_inpercentage <= component_fees){
										total_discount_amount = parseFloat(total_discount_amount) + discount_amount_inpercentage
										$('#total_discount_amount_installment').val(parseFloat(total_discount_amount).toFixed(2))
										let is_gst = $('#isgst_'+i).val()
										if(is_gst == 'Yes'){
											let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage))
											$('#componentnewfees_'+i).val(component_new_fees)
											let tax = $('#tax_'+i).val()
											let component_gst_fees = ((parseFloat(component_new_fees) * parseFloat(tax))/100).toFixed(2);
											$('#componentgstfees_'+i).val(component_gst_fees)
											$('#gstamount_'+i).val(component_gst_fees)
										}
										else{
											let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage))
											$('#componentnewfees_'+i).val(component_new_fees)
											let component_gst_fees = 0
											$('#componentgstfees_'+i).val(component_gst_fees)
											$('#gstamount_'+i).val(0)
											$('#originalgstfees_'+i).val(0)
										}
										let component_new_fees = $('#componentnewfees_'+i).val()
										let component_gst_fees = $('#componentgstfees_'+i).val()
										let subamount = ((parseFloat(component_new_fees) - parseFloat(component_gst_fees))).toFixed(2)
										$('#subamount_'+i).val(subamount)
									}
									else{
										$('#discountamount_'+i).val(0)
										let component_fees = $('#componentgstfees_'+i).val()
										let gst_amount = $('#originalgstfees_'+i).val()
										$("#gstamount_"+i).val(gst_amount)
										let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
										$('#subamount_'+i).val(subamount)
									}
								}
								else{
									$('#discountamount_'+i).val(0)
									let component_fees = $('#componentgstfees_'+i).val()
									let gst_amount = ($('#originalgstfees_'+i).val()!=""?$('#originalgstfees_'+i).val():0);
									$("#gstamount_"+i).val(parseFloat(gst_amount))
									let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
									$('#subamount_'+i).val(parseFloat(subamount))
								}
							}
							else{
								$('#total_discount_amount_installment').val('')
								$('#discountamount_'+i).val(0)
								let component_fees = $('#componentfees_'+i).val()
								let gst_amount = ($('#originalgstfees_'+i).val() !="" )?$('#originalgstfees_'+i).val():0;
								$("#gstamount_"+i).val(parseFloat(gst_amount))
								let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
								$('#subamount_'+i).val(parseFloat(subamount).toFixed(2))
								
							}
							let gst_amount = $('#gstamount_'+i).val()
							total_gst_amount = parseFloat(total_gst_amount) + parseFloat(gst_amount)
							$('#total_gst_amount').val(parseFloat(total_gst_amount))
							let sub_amount = $('#subamount_'+i).val()
							total_sub_amount = parseFloat(total_sub_amount) + parseFloat(sub_amount)
							$('.total_sub_amount').text(parseFloat(total_sub_amount).toFixed(2))
						}
					}
					var amount_after_discount = parseFloat(fees_total_amount) - parseFloat(discount_amount);
					let totalsubamount = $('.total_sub_amount').text()
					$('#total_amount_installment').val(parseFloat(totalsubamount).toFixed(2))
					let installmentNo = $('.no_of_installments').val()
					let installment_amount = (amount_after_discount/installmentNo).toFixed(2)
					$('#fees_amount_collected').val(parseFloat(totalsubamount))
					let total_discount_amount = $('#total_discount_amount_installment').val()
					$('#total_discount_amount_installment').val(parseFloat(total_discount_amount).toFixed(2))
					let discount_amount_rs = $('#total_discount_amount_installment').val()
					$('.instalment_amount').val(parseFloat(installment_amount).toFixed(2))
					let mandatory_component_amount = $('#mandatory_component_amount').val()
					let mandatory_discount_amount = parseFloat(mandatory_component_amount) - parseFloat(discount_amount_rs)
					$('#mandatory_component_amount').val(parseFloat(mandatory_discount_amount))
					$('.instalment_collected_amount').val(0)
					$('.instalment_remaining_amount').val(parseFloat(installment_amount))
					let installmentno = $('.no_of_installments').val()
					let installamount = (parseFloat(totalsubamount)/parseFloat(installmentno)).toFixed(2)
					$('.instalment_amount').val(parseFloat(installamount).toFixed(2))
				}
			}
		})

		$('.no_of_installments').keyup(function(event){
			$(".f_table").children().remove();
			var max_installment = $('#max_installment').val()
			var installmentNo = $(this).val()
			if(parseInt(installmentNo) > parseInt(max_installment)){
				alert("Installment No should not greater than Max Installment.")
				$(this).val("");
				// event.prevenDefault()
				return false;
			}
			
			var max_installment = $('#max_installment').val()
			if(installmentNo == ''){
				$(".f_table").children().remove();
			}else{
				for(let i=0;i<installmentNo;i++){
					let totalsubamnt = $('.total_sub_amount').text()
					let installment_amount = (totalsubamnt/installmentNo).toFixed(2)
					var due_html = '<tr>'+
										'<td >'+
											'<span>'+(i+1)+'</span>'+
										'</td>'+
										'<td>'+
											'<div class="col-sm-12 form-group ">'+
												'<input type="text" class="form-control monthPicker" placeholder="Select Date" id="installmentdate_'+i+'" name="instalment_due_date[]" data-date-container="#installmentdate_'+i+'">'+
											'</div>'+
										'</td>'+
										'<td>'+
											'<div class="form-group">'+
												'<input type="text" class="form-control instalment_amount"  name="instalment_amount[]" readonly="readonly">'+
											'</div>'+
										'</td>'+
									'</tr>';
							$(".f_table").append(due_html);
							$('.instalment_amount').val(installment_amount)
							$('.instalment_collected_amount').val(0)
							$('.instalment_remaining_amount').val(installment_amount)
							$("#installmentdate_"+i).datetimepicker({ 
								viewMode: 'months',
								format: 'DD-MM-YYYY',
							});
				}
				$('.instalment_collected_amount').keyup(function(){
					let collected_amount = $(this).val()
					let installment_amount = $('.instalment_amount').val()

					let remaining_amount_input = $(this).parent().parent().next().find('.instalment_remaining_amount')
					if(parseInt(collected_amount) > parseInt(installment_amount)){
						remaining_amount_input.val(installment_amount);
					}
					else{				
						let remaining_amount = ($('.instalment_amount').val() - collected_amount).toFixed(2)
						remaining_amount_input.val(remaining_amount);
					}
				})
			}
		})
	}

	function calcDate(ageOn,dob){
		if(dob != '' && ageOn != ''){
			var ageOnsplit = ageOn.split('-');
			var ageOndate = ageOnsplit[2]+'-'+ageOnsplit[1]+'-'+ageOnsplit[0];
			var dobsplit = dob.split('-');
			var dobdate = dobsplit[2]+'-'+dobsplit[1]+'-'+dobsplit[0];	
			var diff = Math.abs(new Date(ageOndate).getTime() - new Date(dobdate).getTime());
			var day = 1000 * 60 * 60 * 24;

			var days = Math.ceil(diff/day);
			var monthvalue = Math.floor(days/30)
			$('#dob_month').val(Math.floor(days/30));
			var yearvalue = Math.floor(monthvalue/12)
			$('#dob_year').val(Math.floor(monthvalue/12));
			if(yearvalue > 0){
				var yeardays = days - (365 * yearvalue);
				let dobmonthfloor = Math.floor(yeardays/30);
				$('#dob_month').val(Math.abs(dobmonthfloor))
			}
		}
	}

	function getCourses(category_id,course_id = null){
		$("#batch_id").html("<option value=''>Select</option>");
		if(category_id != "" ){
			$.ajax({
				url:"<?php echo base_url();?>admission/getCourses",
				data:{category_id:category_id, course_id:course_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != '')
						{
							$("#course_id").html("<option value=''>Select</option>"+res['option']);
						}
						else
						{
							$("#course_id").html("<option value=''>Select</option>");
						}
					}
					else
					{	
						$("#course_id").html("<option value=''>Select</option>");
					}
				}
			});
		}
	}

	function getBatch(course_id,center_id,batch_id = null){
		<?php 
			if(!empty($details[0]->student_id)){
		?>
			if($('#course_id').val() == ''){
				var course_id = '<?php echo $otherdetails[0]->course_id; ?>';
			}
			else{
				var course_id = $('#course_id').val();
			}
			if($('#center_id').val() == ''){
				var center_id = '<?php echo $details[0]->center_id; ?>';
			}
			else{
				var center_id = $('#center_id').val();
			}
		<?php 
			}
			else{ 
		?>
			var course_id = $('#course_id').val();
			var center_id = $('#center_id').val();
		<?php
			} 
		?>
		if(course_id != "")
		{
			$.ajax({
				url:"<?php echo base_url();?>admission/getBatch",
				data:{center_id:center_id, course_id:course_id,batch_id:batch_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != '')
						{
							$("#batch_id").html("<option value=''>Select</option>"+res['option']);
						}
						else
						{
							$("#batch_id").html("<option value=''>Select</option>");
						}
					}
					else
					{	
						$("#batch_id").html("<option value=''>Select</option>");
					}
				}
			});
		}
	}

	function getBatchDetails(batch_id){
		if(batch_id != "")
		{
			$.ajax({
				url:"<?php echo base_url();?>admission/getBatchDetails",
				data:{ batch_id:batch_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status'] == 'success'){
						if(res['result'][0]['start_date'] != null && res['result'][0]['end_date'] != null){
							let startdate = res['result'][0]['start_date'].split('-')
							let enddate = res['result'][0]['end_date'].split('-')
							$('.batchNote').text('Course start & End Date Between '+startdate[2]+'-'+startdate[1]+'-'+startdate[0]+' - '+enddate[2]+'-'+enddate[1]+'-'+enddate[0])
							$('#batchstartdate').val(startdate[2]+'-'+startdate[1]+'-'+startdate[0]);
							$('#batchenddate').val(enddate[2]+'-'+enddate[1]+'-'+enddate[0]);

						}
						else{
							$('.batchNote').text('');
						}
					}
					else{
						$('.batchNote').text('');
					}
				}
			});
		}

	}

    let fees_id = $('#fees_id').val()
    getFees(fees_id)
    getStudentCourse();

	function getFees(fees_id = null){
		var fees_type = $('#fees_type').val();
		var category_id = $('#category_id').val();
		var course_id = $('#course_id').val();
		var center_id = $('#center_id').val();
		var zone_id = $('#zone_id').val();
		var batch_start_date = $("#batchstartdate").val();
		var batch_end_date = $("#batchenddate").val();
		if(center_id != "" && zone_id != '' && batch_start_date != "" && batch_end_date != "" )
		{
			$.ajax({
				url:"<?php echo base_url();?>remainingfees/getFees",
				data:{center_id:center_id, zone_id:zone_id,fees_type:fees_type,categoryid:category_id,courseid:course_id,feesselectiontype:'Class',batch_start_date:batch_start_date,batch_end_date:batch_end_date},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success"){
						if(res['option'] != ""){
							$("#fees_id").html(res['option']);
							let fees_id = $('#fees_id').val()
							getFeesDetails(fees_id)
						}else{
							$("#fees_id").html("<option value=''>Select Fees</option>");
                            $('#fees_total_amount').val(0)
                            $('#fees_remaining_amount').val(0)
                            $('#total_amount').val(0)
                            $('#total_amount_installment').val(0)
                            $('#max_installment').val(0)
                            $('#no_of_installments').attr("max",0)
                            $(".component_table").children().remove();
                            $('.paymentprocess').hide()
						}
					}else{	
						$("#fees_id").html("<option value=''>Select Fees</option>");
					}
				}
			});
		}
	}

	function getFeesDetails(fees_id){
		if(fees_id != "" ){
			var batch_start_date = $("#batchstartdate").val();
			var batch_end_date = $("#batchenddate").val();
			$(".component_table").children().remove();
			$.ajax({
				url:"<?php echo base_url();?>admission/getFeesDetails",
				data:{ fees_id:fees_id,batch_start_date:batch_start_date,batch_end_date:batch_end_date},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						$('#max_installment').val(res['result'][0]['installment_no'])
						$('#no_of_installments').attr("max",res['result'][0]['installment_no']);
						let total_amount = 0
						let t_amount = 0;
						let total_sub_amount = 0
						let total_gst_amount = 0
						let total_mandatory_amount = 0
						let total_discount_amount = 0
						$('.component_show').show()
						var size = res['result'].length
						$('#component_size').val(size)
						for(let i=0;i<size;i++){
							$(".component_table").append("<tr>");
								$(".component_table").append('<td>'+Number(i+1)+'</td>');
								$(".component_table").append('<td>'+res['result'][i]['fees_component_master_name']+'</td>');
								$(".component_table").append('<td class="text-right"  id="actual_component_fee_'+i+'" >'+res['result'][i]['component_fees']+'</td>');
								$(".component_table").append('<td><div class="form-group"><input type="text" class="form-control discount_amount text-right"  id="discountamount_'+i+'"name="discount_amount[]" readonly="readonly"></div></td>');
								$(".component_table").append('<td><div class="form-group"><input type="text" class="form-control gst_amount text-right"  id="gstamount_'+i+'"name="gst_amount[]" readonly="readonly"></div></td>');
								$(".component_table").append('<td><div class="form-group"><input type="text" class="form-control sub_amount text-right"  id="subamount_'+i+'" name="sub_amount[]" readonly="readonly"></div></td>');
								$(".component_table").append('<td><div class="form-group"><input type="hidden" class="form-control component_gst_fees"  id="componentgstfees_'+i+'"name="component_gst_fees[]"></div></td>');
								$(".component_table").append('<td><div class="form-group"><input type="hidden" class="form-control is_discountable"  id="isdiscountable_'+i+'"name="is_discountable[]"></div></td>');
								$(".component_table").append('<td><div class="form-group"><input type="hidden" class="form-control is_gst"  id="isgst_'+i+'"name="is_gst[]"></div></td>');
								$(".component_table").append('<td><div class="form-group"><input type="hidden" class="form-control fees"  id="fees_'+i+'"name="fees[]"></div></td>');
								$(".component_table").append('<td><div class="form-group"><input type="hidden" class="form-control tax"  id="tax_'+i+'"name="tax[]"></div></td>');
								$(".component_table").append('<td><div class="form-group"><input type="hidden" class="form-control component_new_fees"  id="componentnewfees_'+i+'"name="component_new_fees[]"></div></td>');
								$(".component_table").append('<td><div class="form-group"><input type="hidden" class="form-control component_fees"  id="componentfees_'+i+'"name="component_fees[]"></div></td>');
								$(".component_table").append('<td><div class="form-group"><input type="hidden" class="form-control original_gst_fees"  id="originalgstfees_'+i+'"name="original_gst_fees[]"></div></td>');
							$(".component_table").append("</tr>");
							$('#isdiscountable_'+i).val(res['result'][i]['is_discountable'])
							$('#isgst_'+i).val(res['result'][i]['is_gst'])
							$("#discountamount_"+i).val(0);
							$('#fees_'+i).val(res['result'][i]['component_fees'])
							$('#tax_'+i).val(res['result'][i]['tax'])
							if(res['result'][i]['is_gst'] == 'Yes'){
								let gst_amount = ((parseFloat(res['result'][i]['component_fees']) * parseFloat(res['result'][i]['tax']))/100).toFixed(2);
								$('#gstamount_'+i).val(gst_amount)
								let componentgstfees = (parseFloat(res['result'][i]['component_fees']) - parseFloat(gst_amount)).toFixed(2); //gst added amount
								$('#componentgstfees_'+i).val(parseFloat(componentgstfees))
								$('#originalgstfees_'+i).val(parseFloat(gst_amount))
								$('#subamount_'+i).val(parseFloat(componentgstfees))
							}
							else{
								$('#componentgstfees_'+i).val(res['result'][i]['component_fees'])
								var componentgstfees = $('#componentgstfees_'+i).val()
								$('#subamount_'+i).val(parseFloat(componentgstfees))
								$('#gstamount_'+i).val(0)
							}
							if(res['result'][i]['is_mandatory'] == 'Yes'){
								total_mandatory_amount = total_mandatory_amount + parseFloat(res['result'][i]['component_fees'])
							}
							$('#componentfees_'+i).val(res['result'][i]['component_fees'])
							var componentgstfees = $('#componentgstfees_'+i).val()
							total_amount = total_amount + parseFloat(componentgstfees)
							total_gst_amount = total_gst_amount + parseFloat(res['result'][i]['gst_amount'])
							$('#total_gst_amount').val(total_gst_amount)
                            t_amount = t_amount + parseFloat(res['result'][i]['component_fees'])//total
							let subamountval = $('#subamount_'+i).val()
							total_sub_amount = total_sub_amount + parseFloat(subamountval)
							$('#total_amount').val((total_sub_amount).toFixed(2))
							$('#total_amount_installment').val((total_sub_amount).toFixed(2))
						}

						$(".component_table").append("<tr>");
						$(".component_table").append('<td><b>Total</b></td>');
						$(".component_table").append('<td></td>');
						$(".component_table").append('<td class="t_amount text-right"></td>');
						$(".component_table").append('<td></td>');
						$(".component_table").append('<td></td>');
						$(".component_table").append('<td class="total_sub_amount text-right"></td>');
						$(".component_table").append("</tr>");


						$('.total_sub_amount').text((total_sub_amount).toFixed(2))
						$('.t_amount').text((total_sub_amount).toFixed(2))
						$('#total_discount_amount').val(total_discount_amount.toFixed(2))
						$('#total_discount_amount_installment').val(parseFloat(total_discount_amount).toFixed(2))
						$('#mandatory_component_amount').val((total_mandatory_amount).toFixed(2))
						$('#fees_total_amount').val((t_amount).toFixed(2));
						$('#fees_amount_collected').val(total_sub_amount)
						$('#fees_remaining_amount').val(0)
					}
					else
					{	
						$('#fees_total_amount').val(0)
						$('#fees_remaining_amount').val(0)
						$('#total_amount').val(0)
						$('#total_amount_installment').val(0)
						$('#max_installment').val(0)
						$('#no_of_installments').attr("max",0)
						$(".component_table").children().remove();
					}
				}
			});
		}
		else{
			$(".component_table").children().remove();
		}
	}

	$("#feesInfoform-validate").validate({
		// rules: vRules,
		// messages: vMessages,
		submitHandler: function(form) 
		{
			$('#loadingmessage').show();
			var act = "<?php echo base_url();?>remainingfees/submitfeesInfoForm";
			$("#feesInfoform-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				cache: false,
				clearForm: false,
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1"){
                        displayMsg("success",res['msg']);
						$('#loadingmessage').hide();
						var student_id = btoa(res['student_id'])
                        $("#assignFeesModel").modal("hide");
					}else{	
						$('#loadingmessage').hide();
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
		}
	});

	function getStudentCourse(){
		var student_id = "";
        student_id = $("#student_id").val();
		if(student_id != ""){
			$.ajax({
				url:"<?php echo base_url();?>admission/getStudentCourse",
				data:{student_id:student_id},
				dataType: 'json',
				method:'post',
				success: function(res){
					if(res.status){
						$('#course_name').val(res.course_name);
						$('#assign_admission_date').val(res.admission_date);
						$('#assign_programme_start_date').val(res.programme_start_date);
						$('#assign_programme_end_date').val(res.programme_end_date);
					}
				}
			});
		}
	}

	function getFeesLevel(){
		var student_id = "";
        student_id = $("#student_id").val();
		if(student_id != ""){
			$.ajax({
				url:"<?php echo base_url();?>admission/getFeesLevelDropdown",
				data:{student_id:student_id},
				dataType: 'json',
				method:'post',
				success: function(res){
					$('#fees_level_id').html(res.option);
				}
			});
		}
	}

	$(document).on('keypress','.onlyNumber',function(event){
		if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
			event.preventDefault(); 
		}
	});

	document.title = "AddEdit - Admission"; 
</script>					
<style>
ul li{list-style:none !important;}
.group-row{
	padding:20px;
	/* text-align:center; */
	margin: 0px auto;
	padding-top:50px;
}
#groupTable>tbody>tr{
	border: 4px dashed #3c8dbc !important;
}
.removeGroup{
	position:absolute;
	top: 0;
    right: 0;
}
.inner-table{
	border: 3px solid #fff !important;
    background: #fff !important;
    margin: 20px auto !important;
}
.insidetable-form-group{
	padding:20px 10px;
}
.group_installment_wrapper,.frequency_wrapper{
	display:none;
}
.control-group{
	margin-top:10px !important;
}
table tbody.component_table td input.form-control{
	width: 80%;
    float: right;
}
.group-count-cls{
	position: absolute !important;
    top: 0 !important;
    background: #cdcdcd !important;
	padding: 10px 20px;
    border-radius: 20px;
    left: 0 !important;
}
.group-count-cls span{
	padding: 3px 10px;
    background: #3c8dbc;
    color: #fff;
    border-radius: 20px;
}
[data-toggle="collapse"] .fa:before {  
  content: "\f139";
}

[data-toggle="collapse"].collapsed .fa:before {
  content: "\f13a";
}
.collapse-btn{
    background: #3c8dbc;
    color: #fff;
    width: 100%;
    font-size: 18px;
}
.toggle-btn:hover{
	color:#fff !important;
}

.button-group-pills .btn {
  line-height: 1.2;
  margin-bottom: 15px;
  border-color: #3c8dbc;
  background-color: #FFF;
  color: #14a4be;
}
.button-group-pills .btn.active {
  border-color: #3c8dbc;
  background-color: #3c8dbc;
  color: #FFF;
  box-shadow: none;
}
.button-group-pills .btn:hover {
  border-color: #3c8dbc;
  background-color: #3c8dbc;
  color: #FFF;
}
</style>