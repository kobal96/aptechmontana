<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Foldercenters extends CI_Controller

{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('foldercentersmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			//echo "here";exit;

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			// $result['categories'] = $this->foldercoursesmodel->getDropdown("tbl_categories","category_id,categoy_name");
			$result['academicyear'] = $this->foldercentersmodel->getdata1("tbl_academic_year_master","*");
			$result['foldername'] = $this->foldercentersmodel->getdata("tbl_document_folders","document_folder_id= ".$record_id." ");
			//  echo "<pre>"; 
			// print_r($result); exit;
			$this->load->view('template/header.php');
			$this->load->view('foldercenters/index',$result);
			$this->load->view('template/footer.php');
		}
		else {
			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['academicyear'] = $this->foldercentersmodel->getdata1("tbl_academic_year_master","*");
			$result['zones'] = $this->foldercentersmodel->getDropdown("tbl_zones","zone_id,zone_name");
			//$result['details'] = $this->foldercentersmodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('foldercenters/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		$_GET['document_folder_id'] = $id;
		//echo $id;exit;
		$get_result = $this->foldercentersmodel->getRecords($_GET);

		// echo "<pre>";
		// print_r($get_result);
		// exit;
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->academic_year_master_name);
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_count." Center Mapped");
				// array_push($temp, date("d-m-Y", strtotime($get_result['query_result'][$i]->start_date)));
				// array_push($temp, date("d-m-Y", strtotime($get_result['query_result'][$i]->end_date)));
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("DocumentFolderZoneCenterDelete")){
					// $actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData(\'' . $get_result['query_result'][$i]->folder_center_id. '\');" title="">Delete</a>';

					$actionCol21 .='<a href="foldercenters/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->document_folder_id) , '+/', '-_') , '=') . '" title="Edit" class="btn btn-primary" ><i class="fa fa-edit"></i></a>';
				}
				
				array_push($temp, $actionCol21);
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	public function getCenter(){

		$condition = "status='Active' && zone_id='".$_REQUEST['zone_id']."' ";
		$result = $this->foldercentersmodel->getdata("tbl_centers",$condition);
		// $result = $this->foldercentersmodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		$option = '';
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$option .= '<option value="'.$result[$i]['center_id'].'" >'.$result[$i]['center_name'].'</option>';
			}
		}
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	
	function submitForm()
	{ 
		// echo "<pre>";print_r($_POST);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			// $condition = "status='Active' && zone_id='".$_POST['zone_id']."' ";
			
			// $getCenters = $this->foldercentersmodel->getdata("tbl_centers",$condition);
			// //echo "<pre>";print_r($check_name);exit;
			
			// if(empty($getCenters)){
			// 	echo json_encode(array("success"=>"0",'msg'=>' No active center present in selected zone!'));
			// 	exit;
			// }
			
			if(!empty($_POST['center_id'])){
				$this->foldercentersmodel->delrecord_condition("tbl_folder_centers", " academic_year_id = '".$_POST['academic_year_id']."' && zone_id='".$_POST['zone_id']."' &&  document_folder_id='".$_POST['document_folder_id']."' ");
				for($i=0; $i < sizeof($_POST['center_id']); $i++){
					//echo $getCenters[$i]['center_id']."<br/>";
					$data_array = array();
					$data_array['document_folder_id'] = (!empty($_POST['document_folder_id'])) ? $_POST['document_folder_id'] : '';
					$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
					$data_array['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
					$data_array['center_id'] = $_POST['center_id'][$i];
					$data_array['start_date'] = (!empty($_POST['start_date'])) ? date("Y-m-d", strtotime($_POST['start_date'])) : NULL;
					$data_array['end_date'] = (!empty($_POST['end_date'])) ? date("Y-m-d", strtotime($_POST['end_date'])) : NULL;
					$data_array['status'] = 'Active';
					$data_array['created_on'] = date('Y-m-d H:i:s');
					$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
					$data_array['updated_on'] = date('Y-m-d H:i:s');
					$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;


					$result = $this->foldercentersmodel->insertData('tbl_folder_centers', $data_array, '1');
				}
			}else{
				echo json_encode(array("success"=>"0",'msg'=>' No active center present in selected zone!'));
				exit;
			}
			//exit;
			/*
			if(!empty($_POST['center_id'])){
				$this->foldercentersmodel->delrecord_condition("tbl_folder_centers", "zone_id='".$_POST['zone_id']."' &&  document_folder_id='".$_POST['document_folder_id']."' ");
				for($i=0; $i < sizeof($_POST['center_id']); $i++){
					$data_array = array();
					$data_array['document_folder_id'] = (!empty($_POST['document_folder_id'])) ? $_POST['document_folder_id'] : '';
					$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
					$data_array['center_id'] = $_POST['center_id'][$i];
					
					$result = $this->foldercentersmodel->insertData('tbl_folder_centers', $data_array, '1');
				}
			}else{
				echo json_encode(array("success"=>"0",'msg'=>'Kindly select atleast one record!'));
				exit;
			}
			*/
			
			/*$data_array = array();
				
			$data_array['document_folder_id'] = (!empty($_POST['document_folder_id'])) ? $_POST['document_folder_id'] : '';
			$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
			$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
			
			$result = $this->foldercentersmodel->insertData('tbl_folder_centers', $data_array, '1');
			*/
			
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	private function set_upload_options()
	{   
		//upload an image options //products
		$config = array();
		//$config['file_name']     = md5(uniqid("100_ID", true));
		$config['upload_path'] = DOC_ROOT_FRONT."/images/teacher_album_images";
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;

		return $config;
	}
	
	function delRecord()
	{
		$id=$_POST['id'];
		$appdResult = $this->foldercentersmodel->delrecord1("tbl_folder_centers","folder_center_id",$id);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
