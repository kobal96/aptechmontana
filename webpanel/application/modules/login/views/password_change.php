		<section class="material-half-bg">
			<div class="cover"></div>
		</section>
		<section class="login-content">
			<div class="logo">
				<h1><img src="<?PHP echo base_url();?>images/prochat-logo-large.png"/></h1>
			</div>
			<div class="login-box">
         <?php if(!$error){ ?>
				<form class="login-form" id="form-validate" method="post">
            <input type="hidden" name="email" value="<?php echo $email; ?>">
            <input type="hidden" name="verification_code" value="<?php echo $verification_code; ?>">
				  <h3 class="login-head"><i class="fa fa-key" aria-hidden="true"></i>Change Password</h3>
				  <div class="form-group">
					<label class="control-label">New Password</label>
					<input class="form-control" name="password" id="password" type="password" placeholder="New Password" autofocus>
				  </div>
				  <div class="form-group">
					<label class="control-label">Confirm Password</label>
					<input class="form-control" name="confirm_password" id="confirm_password" type="password" placeholder="Confirm password">
				  </div>
				  
				  <div class="form-group btn-container button-login">
					<button type="submit" class="btn btn-primary btn-block">Submit <i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
				  </div>
                  <div class="clearfix"></div>
                  <div id="show_msg"></div>
				</form>
         <?php }else{ ?>
           <center style="color:red;margin-top: 50%;"><?php echo $msg; ?></center>
         <?php } ?>
           <center><a href="<?php echo base_url()?>login">Login</a></center>
		    </div>
		</section>	
<script>

var vRules = {
		password: {required: true, minlength: 8, maxlength: 15},
    confirm_password: {required: true, equalTo: "#password"},
};
var vMessages = {
		password: {required: "Please enter password.", minlength: "Minimum password length is 8.", maxlength: "Maximum password length is 15."},
    confirm_password: {required: "Please enter confirm password.", equalTo: "Password & confirm password do not match."},
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>login/update_password";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				//alert("jlf: "+ res['success']);
				if(res['success'] == "1")
				{
					$("#show_msg").html('<span style="color:#339900;">'+res['msg']+'</span>');
					setTimeout(function(){
						window.location = "<?php echo base_url();?>login";
					},2000);

				}
				else
				{	
					$("#show_msg").html('<span style="color:#ff0000;">'+res['msg']+'</span>');
					return false;
				}
			}
		});
	}
});

var vvRules = {
		email_id:{required:true,email:true},
		
};
var vvMessages = {
		email_id:{required:"Please enter email."}
};
$("#formforgot-validate").validate({
	rules: vvRules,
	messages: vvMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>login/forgotpassword";
		$("#formforgot-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				//alert("jlf: "+ res['success']);
				if(res['success'] == "1")
				{
					$("#show_msg1").html('<span style="color:#339900;">'+res['msg']+'</span>');
					setTimeout(function(){
						window.location = "<?php echo base_url();?>login";
					},2000);

				}
				else
				{	
					$("#show_msg1").html('<span style="color:#ff0000;">'+res['msg']+'</span>');
					return false;
				}
			}
		});
	}
});

</script>