<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Home extends CI_Controller {

 function __construct()
 {
   parent::__construct();
   $this->load->model('homemodel','',TRUE);
 }
 
 function index()
 {
//   echo '<pre>';
//   print_r($_SESSION["webadmin"]);
//   exit;
   if(!empty($_SESSION["webadmin"]))
	{   
	
	 $result = array();
		$today =date("Y-m-d 00:00:00");
		$todayend =date("Y-m-d 23:59:59");
		$yesterday =date('Y-m-d 00:00:00',strtotime("-1 days"));
		$last7days =date('Y-m-d 00:00:00',strtotime("-7 days"));
		$lastmonth =date('Y-m-d 00:00:00',strtotime("-30 days"));
		$lastyear =date('Y-m-d 00:00:00',strtotime("-365 days"));
		//lead for class
			$condition_today ="created_on between '$today' AND '$todayend' and rec_type='Lead' AND category_id='0'";
			$condition_yesyterday ="created_on between '$yesterday' AND '$todayend' and rec_type='Lead' AND category_id='0'";
			$condition_last7day ="created_on between '$last7days' AND '$todayend' and rec_type='Lead' AND category_id='0'";
			$condition_lastmonth ="created_on between '$lastmonth' AND '$todayend' and rec_type='Lead' AND category_id='0'";
			$condition_lastyear ="created_on between '$lastyear' AND '$todayend' and rec_type='Lead' AND category_id='0'";

			$result['today_lead'] = $this->homemodel->getleads("tbl_inquiry_master","$condition_today");
			$result['yesterday_lead'] = $this->homemodel->getleads("tbl_inquiry_master","$condition_yesyterday");
			$result['last7days_lead'] = $this->homemodel->getleads("tbl_inquiry_master","$condition_last7day");
			$result['lastmonth_lead'] = $this->homemodel->getleads("tbl_inquiry_master"," $condition_lastmonth");
			$result['lastyear_lead'] = $this->homemodel->getleads("tbl_inquiry_master"," $condition_lastyear");
		//Enquiry for class
			$enquery_today ="created_on between '$today' AND '$todayend' and rec_type='Inquiry' AND category_id='0'";
			$enquery_yesyterday ="created_on between '$yesterday' AND '$todayend' and rec_type='Inquiry' AND category_id='0'";
			$enquery_last7day ="created_on between '$last7days' AND '$todayend' and rec_type='Inquiry' AND category_id='0'";
			$enquery_lastmonth ="created_on between '$lastmonth' AND '$todayend' and rec_type='Inquiry' AND category_id='0'";
			$enquery_lastyear ="created_on between '$lastyear' AND '$todayend' and rec_type='Inquiry' AND category_id='0'";

			$result['today_enquery'] = $this->homemodel->getleads("tbl_inquiry_master","$enquery_today");
			$result['yesterday_enquery'] = $this->homemodel->getleads("tbl_inquiry_master","$enquery_yesyterday");
			$result['last7days_enquery'] = $this->homemodel->getleads("tbl_inquiry_master","$enquery_last7day");
			$result['lastmonth_enquery'] = $this->homemodel->getleads("tbl_inquiry_master"," $enquery_lastmonth");
			$result['lastyear_enquery'] = $this->homemodel->getleads("tbl_inquiry_master"," $enquery_lastyear");
		//ENR for class
			$ENR_today ="created_on between '$today' AND '$todayend'  AND category_id='0' and inquiry_no IS not NULL";
			$ENR_yesyterday ="created_on between '$yesterday' AND '$todayend'  AND category_id='0' and inquiry_no IS not NULL";
			$ENR_last7day ="created_on between '$last7days' AND '$todayend'  AND category_id='0' and inquiry_no IS not NULL";
			$ENR_lastmonth ="created_on between '$lastmonth' AND '$todayend'  AND category_id='0' and inquiry_no IS not NULL";
			$ENR_lastyear ="created_on between '$lastyear' AND '$todayend'  AND category_id='0' and inquiry_no IS not NULL";

			$result['today_ENR'] = $this->homemodel->getleads("tbl_inquiry_master","$ENR_today");
			$result['yesterday_ENR'] = $this->homemodel->getleads("tbl_inquiry_master","$ENR_yesyterday");
			$result['last7days_ENR'] = $this->homemodel->getleads("tbl_inquiry_master","$ENR_last7day");
			$result['lastmonth_ENR'] = $this->homemodel->getleads("tbl_inquiry_master"," $ENR_lastmonth");
			$result['lastyear_ENR'] = $this->homemodel->getleads("tbl_inquiry_master"," $ENR_lastyear");

			//lead for group
			$condition_todaygroup ="created_on between '$today' AND '$todayend' and rec_type='Lead' AND category_id='1'";
			$condition_yesyterdaygroup ="created_on between '$yesterday' AND '$todayend' and rec_type='Lead' AND category_id='1'";
			$condition_last7daygroup ="created_on between '$last7days' AND '$todayend' and rec_type='Lead' AND category_id='1'";
			$condition_lastmonthgroup ="created_on between '$lastmonth' AND '$todayend' and rec_type='Lead' AND category_id='1'";
			$condition_lastyeargroup ="created_on between '$lastyear' AND '$todayend' and rec_type='Lead' AND category_id='1'";

			$result['today_lead_group'] = $this->homemodel->getleads("tbl_inquiry_master","$condition_todaygroup");
			$result['yesterday_lead_group'] = $this->homemodel->getleads("tbl_inquiry_master","$condition_yesyterdaygroup");
			$result['last7days_lead_group'] = $this->homemodel->getleads("tbl_inquiry_master","$condition_last7daygroup");
			$result['lastmonth_lead_group'] = $this->homemodel->getleads("tbl_inquiry_master"," $condition_lastmonthgroup");
			$result['lastyear_lead_group'] = $this->homemodel->getleads("tbl_inquiry_master"," $condition_lastyeargroup");
		//Enquiry for group
			$enquery_today_group ="created_on between '$today' AND '$todayend' and rec_type='Inquiry' AND category_id='1'";
			$enquery_yesyterday_group ="created_on between '$yesterday' AND '$todayend' and rec_type='Inquiry' AND category_id='1'";
			$enquery_last7day_group ="created_on between '$last7days' AND '$todayend' and rec_type='Inquiry' AND category_id='1'";
			$enquery_lastmonth_group ="created_on between '$lastmonth' AND '$todayend' and rec_type='Inquiry' AND category_id='1'";
			$enquery_lastyear_group ="created_on between '$lastyear' AND '$todayend' and rec_type='Inquiry' AND category_id='1'";

			$result['today_enquery_group'] = $this->homemodel->getleads("tbl_inquiry_master","$enquery_today_group");
			$result['yesterday_enquery_group'] = $this->homemodel->getleads("tbl_inquiry_master","$enquery_yesyterday_group");
			$result['last7days_enquery_group'] = $this->homemodel->getleads("tbl_inquiry_master","$enquery_last7day_group");
			$result['lastmonth_enquery_group'] = $this->homemodel->getleads("tbl_inquiry_master"," $enquery_lastmonth_group");
			$result['lastyear_enquery_group'] = $this->homemodel->getleads("tbl_inquiry_master"," $enquery_lastyear_group");
		//ENR for group
			$ENR_today_group ="created_on between '$today' AND '$todayend'  AND category_id='1' and inquiry_no IS not NULL";
			$ENR_yesyterday_group ="created_on between '$yesterday' AND '$todayend'  AND category_id='1' and inquiry_no IS not NULL";
			$ENR_last7day_group ="created_on between '$last7days' AND '$todayend'  AND category_id='1' and inquiry_no IS not NULL";
			$ENR_lastmonth_group ="created_on between '$lastmonth' AND '$todayend'  AND category_id='1' and inquiry_no IS not NULL";
			$ENR_lastyear_group ="created_on between '$lastyear' AND '$todayend'  AND category_id='1' and inquiry_no IS not NULL";

			$result['today_ENR_group'] = $this->homemodel->getleads("tbl_inquiry_master","$ENR_today_group");
			$result['yesterday_ENR_group'] = $this->homemodel->getleads("tbl_inquiry_master","$ENR_yesyterday_group");
			$result['last7days_ENR_group'] = $this->homemodel->getleads("tbl_inquiry_master","$ENR_last7day_group");
			$result['lastmonth_ENR_group'] = $this->homemodel->getleads("tbl_inquiry_master"," $ENR_lastmonth_group");
			$result['lastyear_ENR_group'] = $this->homemodel->getleads("tbl_inquiry_master"," $ENR_lastyear_group");

		// cash flow table data
			$jan_start = date("Y-01-01 00:00:00");
			$jan_end =date("Y-12-31 23:59:59");
			$fab_start = date("Y-02-01 00:00:00");
			$fab_end =date("Y-02-29 23:59:59");
			$march_start = date("Y-03-01 00:00:00");
			$march_end =date("Y-03-31 23:59:59");
			$april_start = date("Y-04-01 00:00:00");
			$april_end =date("Y-04-30 23:59:59");
			$may_start = date("Y-05-01 00:00:00");
			$may_end =date("Y-05-31 23:59:59");
			$jun_start = date("Y-06-01 00:00:00");
			$jun_end =date("Y-06-30 23:59:59");
			$july_start = date("Y-07-01 00:00:00");
			$july_end =date("Y-07-31 23:59:59");
			$aug_start = date("Y-08-01 00:00:00");
			$aug_end =date("Y-08-31 23:59:59");
			$sep_start = date("Y-09-01 00:00:00");
			$sep_end =date("Y-09-30 23:59:59");
			$oct_start = date("Y-10-01 00:00:00");
			$oct_end =date("Y-10-31 23:59:59");
			$nov_start = date("Y-11-01 00:00:00");
			$nov_end =date("Y-11-30 23:59:59");
			$dec_start = date("Y-12-01 00:00:00");
			$dec_end =date("Y-12-31 23:59:59");
			
			$jan_collection ="created_on between '$jan_start' AND '$jan_end'";
			$fab_collection ="created_on between '$fab_start' AND '$fab_end'";
			$march_collection ="created_on between '$march_start' AND '$march_end'";
			$april_collection ="created_on between '$april_start' AND '$april_end'";
			$may_collection ="created_on between '$may_start' AND '$may_end'";
			$jun_collection ="created_on between '$jun_start' AND '$jun_end'";
			$july_collection ="created_on between '$july_start' AND '$july_end'";
			$aug_collection ="created_on between '$aug_start' AND '$aug_end'";
			$sep_collection ="created_on between '$sep_start' AND '$sep_end'";
			$oct_collection ="created_on between '$oct_start' AND '$oct_end'";
			$nov_collection ="created_on between '$nov_start' AND '$nov_end'";
			$dec_collection ="created_on between '$dec_start' AND '$dec_end'";
			

			$result['jan'] = $this->homemodel->getcash_flow("tbl_admission_fees","$jan_collection");
			$result['fab'] = $this->homemodel->getcash_flow("tbl_admission_fees","$fab_collection");
			$result['march'] = $this->homemodel->getcash_flow("tbl_admission_fees","$march_collection");
			$result['april'] = $this->homemodel->getcash_flow("tbl_admission_fees","$april_collection");
			$result['may'] = $this->homemodel->getcash_flow("tbl_admission_fees","$may_collection");
			$result['jun'] = $this->homemodel->getcash_flow("tbl_admission_fees","$jun_collection");
			$result['july'] = $this->homemodel->getcash_flow("tbl_admission_fees","$july_collection");
			$result['aug'] = $this->homemodel->getcash_flow("tbl_admission_fees","$aug_collection");
			$result['sep'] = $this->homemodel->getcash_flow("tbl_admission_fees","$sep_collection");
			$result['oct'] = $this->homemodel->getcash_flow("tbl_admission_fees","$oct_collection");
			$result['nov'] = $this->homemodel->getcash_flow("tbl_admission_fees","$nov_collection");
			$result['dec'] = $this->homemodel->getcash_flow("tbl_admission_fees","$dec_collection");

			//print_r($result['jan']);exit;
		  // print_r($result['lastyear']);exit;       
		$this->load->view('template/header.php');
		$this->load->view('home/index',$result);
		$this->load->view('template/footer.php');
	}
	else
	{
		//If no session, redirect to login page
		redirect('login', 'refresh');
	}
 }
 
 /*function getallenrolledstudent(){

	$getstudents =  $this->homemodel->getstudentData();
	foreach ($getstudents as $key => $value) {
		$data = array();
		$data['current_academic_year_id'] = $value['academic_year_id'];
		$data['zone_id'] = $value['zone_id'];
		$data['center_id'] = $value['center_id'];
		$data['category_id'] = $value['category_id'];
		$data['course_id'] = $value['course_id'];
		$data['batch_id'] = $value['batch_id'];
		$data['student_id'] = $value['student_id'];
		$data['promoted_academic_year_id'] = $value['academic_year_id'];
		$data['promoted_course_id'] = $value['course_id'];
		$data['promoted_batch_id'] = $value['batch_id'];
		$data['programme_start_date'] = $value['programme_start_date'];
		$data['programme_end_date'] = $value['programme_end_date'];
		$data['created_on'] = date("Y-m-d H:i:s");
		$data['created_by'] = $_SESSION["webadmin"][0]->user_id;
		$data['updated_on'] = date("Y-m-d H:i:s");
		$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
		$result = $this->homemodel->insertData("tbl_promoted_student",$data);
		// echo "inserted at".$result;
	}
}*/
 
function addEdit($id=NULL)
 {
   if($this->session->userdata('logged_in'))
   {
     $session_data = $this->session->userdata('logged_in');
	 //$comp_details = comp_details();
	 
	 //$data['name'] = $session_data['name'];
	 
     //echo $id;
     $resulte['roles'] = $this->customermodel->getDropdown("roles","RoleId,Role");     
     $resulte['suppliers'] = $this->customermodel->getDropdown("vendor_master","VendorId,Name");
     
     $resulte['users'] = $this->customermodel->getUsersdata($id);
     $resulte['warehouse'] = $this->customermodel->getUserswarehouse($id);
     
     $sel_roles = $this->customermodel->getDropdownSelval("test_user_roles","UserId","RoleId",$id); 
     $sel_suppliers = $this->customermodel->getDropdownSelval("test_customer_supplier","UserId","supplier_id",$id);
     //echo "<pre>";
     //print_r($resulte['sel_suppliers']);
     
     $roles_ids = array();
     $supplier_ids = array();
	 
     if(!empty($sel_roles)){
	     foreach ($sel_roles as $key => $value) {
		    $roles_ids[] = $value->RoleId;
		 }
		 $resulte['selusers'] = $roles_ids;	
     }
   	 
	 if(!empty($sel_suppliers)){
		 foreach ($sel_suppliers as $key => $value) {
		    $supplier_ids[] = $value->supplier_id;
		 }
		 $resulte['selsuppliers'] = $supplier_ids;	
	 }
     
	 
	 
	 
	 //print_r($supplier_ids);
     
     
     
     
     //exit;
     
     //print_r($resulte);
     //print_r($resulte['warehouse']);
     //print_r($resulte['suppliers']);
     
     
	 $this->load->view('template/header.php');
     $this->load->view('customer_addEdit_view',$resulte);
     $this->load->view('template/footer.php');
   }
   else
   {
     //If no session, redirect to login page
     redirect('auth/login', 'refresh');
   }
 }
 
 
 
 
//For file upload

function uploadimage()
 {
	 
	/*echo "in condition";
	exit;*/ 
 	$path = "images/customer_logo/";
	 
	 $valid_formats = array("jpg", "JPG", "JPEG", "jpeg", "png", "PNG", "gif", "GIF", "bmp", "BMP");
	 
	 $name = $_FILES['customer_logo']['name'];
			$size = $_FILES['customer_logo']['size'];
			
			if(strlen($name))
				{
					list($txt, $ext) = explode(".", $name);
					if(in_array($ext,$valid_formats))
					{
					if($size<(1024*1024))
						{
							//$actual_image_name = time().substr(str_replace(" ", "_", $txt), 5).".".$ext;
							$rndname = uniqid();
							$actual_image_name = time().$rndname.".".$ext;
							$tmp = $_FILES['customer_logo']['tmp_name'];
							if(move_uploaded_file($tmp, $path.$actual_image_name))
								{
									$imgpath = base_url()."images/customer_logo/".$actual_image_name;
									return $actual_image_name;
								
									
								}
							else
								echo '<div class="alert alert-danger fade in"><strong>File Upload failed!!!</strong></div>';
						}
						else
						echo '<div class="alert alert-danger fade in"><strong>Image file size max 1 MB!!!</strong></div>';					
						}
						else
						echo '<div class="alert alert-danger fade in"><strong>Invalid file format..</strong></div>';	
				}
				
			else
				echo '<div class="alert alert-warning fade in"><strong>Please select a file to upload..!</strong></div>';
	 
 }
	
//End file upload	
	
	
	
	

 function logout()
 {
   //$this->session->unset_userdata('logged_in');
   /*$session_id = $_SESSION["webadmin"][0]->session_id;
   $session_data = array(
                          'last_active' => date('Y-m-d H:i:s')
   );*/
   session_destroy();
   redirect('login', 'refresh');
 }

}

?>
