<?php 
//session_start();
//print_r($_SESSION["webadmin"]);
//echo $tot_profile_percentage;exit;
?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="js/jquery.min.js"></script>
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="css/jquery-jvectormap.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<!--<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>-->


<script type="text/javascript" src="js/loader.js"></script>

<!-- SlimScroll -->
<script src="js/jquery.slimscroll.min.js"></script>

<!-- jvectormap  -->
<script src="js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="js/jquery-jvectormap-world-mill-en.js"></script>
<!-- chart  
<script src="js/Chart.js"></script>
<script src="js/dashboard2.js"></script>-->

<!-- Sparkline -->
<script src="js/jquery.sparkline.min.js"></script>

<!-- start: Content -->
<div id="content" class="content-wrapper">
    <div class="page-title">
      <div>
        <h1>Dashboard</h1>            
      </div>
      <div>

        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="#">Dashboard</a></li></a></li>
        </ul>
      </div>


    </div>
    <!--dash board tables start----------->
    <div class ="container"style="max-width:1050px" >
		<div class="row">
		<div class="col-sm-6">
		<h2 class="dashboard-title">CLASS DETAILS</h2>
		<div class="table-responsive box-shadow">
		<table cellpadding="0" cellspacing="0" width="100%" class="table table-hover detailstable">
          <thead>
            <!--<tr>
              <th  class="text-center" colspan="6" >CLASS DETAILS</th>
            </tr>-->
            <tr class="column-title">
              <th>&nbsp;</th>
              <th><span>Today</span></th>
              <th><span>Yesterday</span></th>
              <th><span>Last 7 days</span></th>
              <th><span>Current Month</span></th>
              <th><span>Ytd</span></th>
            </tr>
          </thead>
          <tbody>
            <tr class="row-title">
				<th><span>Lead</span></th>
                <td><?php echo  (!empty($today_lead)) ?  $today_lead : '0'?></td>
                <td><?php echo (!empty($yesterday_lead)) ? $yesterday_lead : '0'?></td>
                <td><?php echo (!empty($last7days_lead)) ? $last7days_lead : '0'?></td>
                <td><?php echo (!empty($lastmonth_lead)) ? $lastmonth_lead : '0'?></td>
                <td><?php echo (!empty($lastyear_lead)) ? $lastyear_lead : '0'?></td>
            </tr>
            <tr class="row-title">
				<th><span>Enquiry</span></th>
                <td><?php echo  (!empty($today_enquery)) ?  $today_enquery : '0'?></td>
                <td><?php echo (!empty($yesterday_enquery)) ? $yesterday_enquery : '0'?></td>
                <td><?php echo (!empty($last7days_enquery)) ? $last7days_enquery : '0'?></td>
                <td><?php echo (!empty($lastmonth_enquery)) ? $lastmonth_enquery : '0'?></td>
                <td><?php echo (!empty($lastyear_enquery)) ? $lastyear_enquery : '0'?></td>
            </tr>
            <tr class="row-title">
				<th><span>ENR</span></th>
                <td><?php echo  (!empty($today_ENR)) ?  $today_ENR : '0'?></td>
                <td><?php echo (!empty($yesterday_ENR)) ? $yesterday_ENR : '0'?></td>
                <td><?php echo (!empty($last7days_ENR)) ? $last7days_ENR : '0'?></td>
                <td><?php echo (!empty($lastmonth_ENR)) ? $lastmonth_ENR : '0'?></td>
                <td><?php echo (!empty($lastyear_ENR)) ? $lastyear_ENR : '0'?></td>
            </tr>
            <tr class="row-title">
				<th><span>Booking</span></th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr class="row-title">
              <th><span>Billing - receipt</span></th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
          </tbody>
        </table>
		</div>
		</div>
		<div class="col-sm-6">
		<h2 class="dashboard-title">GROUP DETAILS</h2>
		<div class="table-responsive box-shadow">
        <table cellpadding="0" cellspacing="0" width="100%" class="table table-hover detailstable">
          <thead>
            <!--<tr>
              <th  class="text-center" colspan="6" >GROUP DETAILS</th>
            </tr>-->
            <tr class="column-title">
              <th>&nbsp;</th>
              <th><span>today</span></th>
              <th><span>yesterday</span></th>
              <th><span>last 7 days</span></th>
              <th><span>current month</span></th>
              <th><span>ytd</span></th>
            </tr>
          </thead>
          <tbody>
            <tr class="row-title">
              <th>Lead</th>
              <td><?php echo  (!empty($today_lead_group)) ?  $today_lead_group : '0'?></td>
                <td><?php echo (!empty($yesterday_lead_group)) ? $yesterday_lead_group : '0'?></td>
                <td><?php echo (!empty($last7days_lead_group)) ? $last7days_lead_group : '0'?></td>
                <td><?php echo (!empty($lastmonth_lead_group)) ? $lastmonth_lead_group : '0'?></td>
                <td><?php echo (!empty($lastyear_lead_group)) ? $lastyear_lead_group : '0'?></td>
            </tr>
            <tr class="row-title">
              <th>Enquiry</th>
              <td><?php echo  (!empty($today_enquery_group)) ?  $today_enquery_group : '0'?></td>
                <td><?php echo (!empty($yesterday_enquery_group)) ? $yesterday_enquery_group : '0'?></td>
                <td><?php echo (!empty($last7days_enquery_group)) ? $last7days_enquery_group : '0'?></td>
                <td><?php echo (!empty($lastmonth_enquery_group)) ? $lastmonth_enquery_group : '0'?></td>
                <td><?php echo (!empty($lastyear_enquery_group)) ? $lastyear_enquery_group : '0'?></td>
            </tr>
            <tr class="row-title">
              <th>ENR</th>
              <td><?php echo  (!empty($today_ENR_group)) ?  $today_ENR_group : '0'?></td>
                <td><?php echo (!empty($yesterday_ENR_group)) ? $yesterday_ENR_group : '0'?></td>
                <td><?php echo (!empty($last7days_ENR_group)) ? $last7days_ENR_group : '0'?></td>
                <td><?php echo (!empty($lastmonth_ENR_group)) ? $lastmonth_ENR_group : '0'?></td>
                <td><?php echo (!empty($lastyear_ENR_group)) ? $lastyear_ENR_group : '0'?></td>
            </tr>
            <tr class="row-title">
              <th>Booking</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr class="row-title">
              <th>Billing - receipt</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
          </tbody>
        </table>
		</div>
		</div>
		</div>
	</div>

    <div class ="container" style="max-width:1050px;overflow-x:auto;">
		<h2 class="dashboard-title">CASHFLOW</h2>	
		<div class="table-responsive box-shadow">
        <table class="table table-hover detailstable" width="99%">
          <thead>
            <!--<tr>
              <th  class="text-center" colspan="14" >CASHFLOW</th>
            </tr>-->
            <tr class="column-title">
              <th>&nbsp;</th>
              <th><span>Apr</span></th>
              <th><span>May</span></th>
              <th><span>June</span></th>
              <th><span>July</span></th>
              <th><span>August</span></th>
              <th><span>September</span></th>
              <th><span>October</span></th>
              <th><span>November</span></th>
              <th><span>December</span></th>
              <th><span>January</span></th>
              <th><span>February</span></th>
              <th><span>March</span></th>
              <th><span>Total</span></th>
            </tr>
          </thead>
          <tbody>
            <tr class="row-title">
              <th>Expected Collection Plan</th>
              <?php
              $collected_total =$april[0]->Total_Amount+$may[0]->Total_Amount+$jun[0]->Total_Amount+$july[0]->Total_Amount+$aug[0]->Total_Amount+$sep[0]->Total_Amount+$oct[0]->Total_Amount+$nov[0]->Total_Amount+$dec[0]->Total_Amount+$jan[0]->Total_Amount+$fab[0]->Total_Amount+$march[0]->Total_Amount;
              ?>
                <td>&#8377; <?= (!empty($april[0]->Total_Amount)) ? $april[0]->Total_Amount : '0'?></td>
                <td>&#8377; <?= (!empty($may[0]->Total_Amount)) ? $may[0]->Total_Amount : '0'?></td>
                <td>&#8377; <?= (!empty($jun[0]->Total_Amount)) ? $jun[0]->Total_Amount : '0'?></td>
                <td>&#8377; <?= (!empty($july[0]->Total_Amount)) ? $july[0]->Total_Amount : '0'?></td>
                <td>&#8377; <?= (!empty($aug[0]->Total_Amount)) ? $aug[0]->Total_Amount : '0'?></td>
                <td>&#8377; <?= (!empty($sep[0]->Total_Amount)) ? $sep[0]->Total_Amount : '0'?></td>
                <td>&#8377; <?= (!empty($oct[0]->Total_Amount)) ? $oct[0]->Total_Amount : '0'?></td>
                <td>&#8377; <?= (!empty($nov[0]->Total_Amount)) ? $nov[0]->Total_Amount : '0'?></td>
                <td>&#8377; <?= (!empty($dec[0]->Total_Amount)) ? $dec[0]->Total_Amount : '0'?></td>
                <td>&#8377; <?= (!empty($jan[0]->Total_Amount)) ? $jan[0]->Total_Amount : '0'?></td>
                <td>&#8377; <?= (!empty($fab[0]->Total_Amount)) ? $fab[0]->Total_Amount : '0'?></td>
                <td>&#8377; <?= (!empty($march[0]->Total_Amount)) ? $march[0]->Total_Amount : '0'?></td>
                <td>&#8377; <?= $collected_total?></td>
            </tr>
            <tr class="row-title">
              <th>Actual Collection</th>
              <?php
              $collected_total_fee =$april[0]->Fee_Collected+$may[0]->Fee_Collected+$jun[0]->Fee_Collected+$july[0]->Fee_Collected+$aug[0]->Fee_Collected+$sep[0]->Fee_Collected+$oct[0]->Fee_Collected+$nov[0]->Fee_Collected+$dec[0]->Fee_Collected+$jan[0]->Fee_Collected+$fab[0]->Fee_Collected+$march[0]->Fee_Collected;
              ?>
                <td>&#8377; <?= (!empty($april[0]->Fee_Collected)) ? $april[0]->Fee_Collected : '0'?></td>
                <td>&#8377; <?= (!empty($may[0]->Fee_Collected)) ? $may[0]->Fee_Collected : '0'?></td>
                <td>&#8377; <?= (!empty($jun[0]->Fee_Collected)) ? $jun[0]->Fee_Collected : '0'?></td>
                <td>&#8377; <?= (!empty($july[0]->Fee_Collected)) ? $july[0]->Fee_Collected : '0'?></td>
                <td>&#8377; <?= (!empty($aug[0]->Fee_Collected)) ? $aug[0]->Fee_Collected : '0'?></td>
                <td>&#8377; <?= (!empty($sep[0]->Fee_Collected)) ? $sep[0]->Fee_Collected : '0'?></td>
                <td>&#8377; <?= (!empty($oct[0]->Fee_Collected)) ? $oct[0]->Fee_Collected : '0'?></td>
                <td>&#8377; <?= (!empty($nov[0]->Fee_Collected)) ? $nov[0]->Fee_Collected : '0'?></td>
                <td>&#8377; <?= (!empty($dec[0]->Fee_Collected)) ? $dec[0]->Fee_Collected : '0'?></td>
                <td>&#8377; <?= (!empty($jan[0]->Fee_Collected)) ? $jan[0]->Fee_Collected : '0'?></td>
                <td>&#8377; <?= (!empty($fab[0]->Fee_Collected)) ? $fab[0]->Fee_Collected : '0'?></td>
                <td>&#8377; <?= (!empty($march[0]->Fee_Collected)) ? $march[0]->Fee_Collected : '0'?></td>
                <td>&#8377; <?= $collected_total_fee?></td>
            </tr>
            <tr class="row-title">
              <th>Diff</th>
                <td>&#8377; <?=$april[0]->Total_Amount - $april[0]->Fee_Collected?></td>
                <td>&#8377; <?=$may[0]->Total_Amount - $may[0]->Fee_Collected?></td>
                <td>&#8377; <?=$jun[0]->Total_Amount - $jun[0]->Fee_Collected?></td>
                <td>&#8377; <?=$july[0]->Total_Amount - $july[0]->Fee_Collected?></td>
                <td>&#8377; <?=$aug[0]->Total_Amount - $aug[0]->Fee_Collected?></td>
                <td>&#8377; <?=$sep[0]->Total_Amount - $sep[0]->Fee_Collected?></td>
                <td>&#8377; <?=$oct[0]->Total_Amount - $oct[0]->Fee_Collected?></td>
                <td>&#8377; <?=$nov[0]->Total_Amount - $nov[0]->Fee_Collected?></td>
                <td>&#8377; <?=$dec[0]->Total_Amount - $dec[0]->Fee_Collected?></td>
                <td>&#8377; <?=$jan[0]->Total_Amount - $jan[0]->Fee_Collected?></td>
                <td>&#8377; <?=$fab[0]->Total_Amount - $fab[0]->Fee_Collected?></td>
                <td>&#8377; <?=$march[0]->Total_Amount - $march[0]->Fee_Collected?></td>
                <td>&#8377; <?=$collected_total - $collected_total_fee ?></td>
            </tr>
          </tbody>
        </table>
		</div>		
	</div>

    <div class ="container"style="max-width:1050px" >
		<div class="row">
		<div class="col-sm-6">
		<h2 class="dashboard-title">ACTIVITY LOGS CLASS</h2>
		<div class="table-responsive box-shadow">
        <table cellpadding="0" cellspacing="0" width="100%" class="table table-hover detailstable">
          <thead>
            <!--<tr>
              <th  class="text-center" colspan="6" >Activity logs CLASS</th>
            </tr>-->
            <tr class="column-title">
              <th>&nbsp;</th>
              <th><span>No of Active ENR</span></th>
              <th><span>Today’s Count</span></th>
              <th><span>Yesterday’s Count</span></th>
              <th><span>Last Entry date</span></th>              
            </tr>
          </thead>
          <tbody>
            <tr class="row-title">
				<th>Attendance</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                
            </tr>
            <tr class="row-title">
				<th>Current CCTV usage</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                
            </tr class="row-title">
            <tr class="row-title">
				<th>Image uploaded</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                
            </tr>
            <tr class="row-title">
              <th>Communication sent</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                         
			</tr>
            <!--<tr class="row-title">
				<th>&nbsp;</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                
            </tr>-->
          </tbody>
        </table>
		</div>
		</div>
		<div class="col-sm-6">
		<div class="table-responsive box-shadow">
		<h2 class="dashboard-title">ACTIVITY LOGS GROUP</h2>
        <table cellpadding="0" cellspacing="0" width="100%" class="table table-hover detailstable">
          <thead>
              <!--<tr>
                <th  class="text-center" colspan="6" >Activity logs Group</th>
              </tr>-->
              <tr class="column-title">
                <th>&nbsp;</th>
                <th><span>No of Active ENR</span></th>
                <th><span>Today’s Count</span></th>
                <th><span>Yesterday’s Count</span></th>
                <th><span>Last Entry date</span></th>                
              </tr>
            </thead>
            <tbody>
            <tr class="row-title">
              <th>Attendance</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                
            </tr>
            <tr class="row-title">
              <th>Current CCTV usage</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>               
            </tr>
            <tr class="row-title">
              <th>Image uploaded</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                
            </tr>
            <tr class="row-title" >
              <th>Communication sent</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                
            </tr>
            <!--<tr class="row-title">
              <th></th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>                
            </tr>-->
          </tbody>
        </table>
		</div>
		</div>
		</div>
	</div>

    <!------dashboard tables end---------->
    <!-- Main content -->
    <!-- <div class="container-fluid">
        <section class="statistics">
            <div class="">
                <div class="row">
         
          <?php  if($_SESSION["webadmin"][0]->user_type == 1 ){?>
          <div class="row">
          <?php 
              if ($this->privilegeduser->hasPrivilege("InquiryList") || $this->privilegeduser->hasPrivilege("InquiryExport")) {
            ?>
                <a href="<?php echo base_url();?>inquiries">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                      <i class="fa fa-question fa-fw danger"></i>
                      <div class="info">

                      <h5><span style="font-size: 100%;">Enquiry</span></h5></div>
                      </div>
                  </div>
                </a>
            <?php
              }
            ?>


                <a href="<?php echo base_url();?>admission">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-graduation-cap fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Admission </span></h5></div>
                     </div>
                  </div>
                </a>

            <?php 
              if ($this->privilegeduser->hasPrivilege("FeesLevelList") ) {
            ?>
                <a href="<?php echo base_url();?>feeslevelmaster">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-money fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Create Fee Levels</span></h5></div>
                     </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <a href="<?php echo base_url();?>remainingfees">
              <div class="col-md-3 col-xs-6 dash-menu">
                <div class="box">
                 <i class="fa fa-money fa-fw danger"></i>
                 <div class="info">

                 <h5><span style="font-size: 100%;">Fees Payment</span></h5></div>
                 </div>
              </div>
            </a>
            <?php 
              if ($this->privilegeduser->hasPrivilege("FeesComponentList") ) {
            ?>
                <a href="<?php echo base_url();?>feescomponentmaster">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-money fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Fee Components</span></h5></div>
                     </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("FeesMasterList") ) {
            ?>
                <a href="<?php echo base_url();?>feesmaster">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                      <i class="fa fa-money fa-fw danger"></i>
                      <div class="info">

                      <h5><span style="font-size: 100%;">Fee Config</span></h5></div>
                      </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("AssignFeesList") ) {
            ?>
                <a href="<?php echo base_url();?>assignfees">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-money fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Assign Fees</span></h5></div>
                     </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <a href="<?php echo base_url();?>childactivityfeesmonth">
              <div class="col-md-3 col-xs-6 dash-menu">
                <div class="box">
                 <i class="fa fa-money fa-fw danger"></i>
                 <div class="info">

                 <h5><span style="font-size: 100%;">Child Activity Fees Month</span></h5></div>
                 </div>
              </div>
            </a>
            <a href="<?php echo base_url();?>feesdiscountmaster">
              <div class="col-md-3 col-xs-6 dash-menu">
                <div class="box">
                 <i class="fa fa-money fa-fw danger"></i>
                 <div class="info">

                 <h5><span style="font-size: 100%;">Fees Discount Master</span></h5></div>
                 </div>
              </div>
            </a>
            <a href="<?php echo base_url();?>gstmaster">
              <div class="col-md-3 col-xs-6 dash-menu">
                <div class="box">
                 <i class="fa fa-money fa-fw danger"></i>
                 <div class="info">

                 <h5><span style="font-size: 100%;">GST Master</span></h5></div>
                 </div>
              </div>
            </a>
            <?php 
              if ($this->privilegeduser->hasPrivilege("ThemeAddEdit") || $this->privilegeduser->hasPrivilege("ThemeList")) {
            ?>
                <a href="<?php echo base_url();?>themesmaster">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                      <i class="fa fa-calendar fa-fw danger"></i>
                      <div class="info">

                      <h5><span style="font-size: 100%;">Create Theme </span></h5></div>
                      </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("TimeTableAddEdit") || $this->privilegeduser->hasPrivilege("TimeTableList")) {
            ?>
                <a href="<?php echo base_url();?>timetablemaster">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-calendar fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Create Timetable </span></h5></div>
                     </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("CenterTimetableList") ) {
            ?>
                <a href="<?php echo base_url();?>centertimetables">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                    <i class="fa fa-calendar fa-fw danger"></i>
                    <div class="info">

                        <h5><span style="font-size: 100%;">Time Tables </span></i></h5>
                    </div>
                  </div>
                  </div>
                </a>
            <?php }?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("AssignTimeTableAddEdit") || $this->privilegeduser->hasPrivilege("AssignTimeTableList")) {
            ?>
                <a href="<?php echo base_url();?>assigntimetable">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-calendar fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Assign Timetable </span></h5></div>
                     </div>
                  </div>
                </a>
            <?php 
              if ($this->privilegeduser->hasPrivilege("DocumentFolderAddEdit") || $this->privilegeduser->hasPrivilege("DocumentFolderList")) {
            ?>
                <a href="<?php echo base_url();?>documentfoldermaster">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-folder fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Add/Map Folder</span></h5></div>
                     </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("CenterDocumentFolderList") ) {
            ?>
                <a href="<?php echo base_url();?>documentfolders">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                    <i class="fa fa-folder fa-fw danger"></i>
                    <div class="info">

                    <h5><span style="font-size: 100%;">View Documents </span></h5>
                    </div>
                    </div>
                    </div>
                </a>
            <?php }?>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("CenterCameraSettingsList") ) {
            ?>
                <a href="<?php echo base_url();?>camerasettings">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-camera fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Centre Camera Settings</span></h5></div>
                     </div>
                    </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("CCTVCumulativeSettingsList") ) {
            ?>
                <a href="<?php echo base_url();?>cumulativesettings">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                      <i class="fa fa-camera fa-fw danger"></i>
                      <div class="info">

                      <h5><span style="font-size: 100%;">Cumulative Time Settings</span></h5></div>
                      </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("CameraList") ) {
            ?>
                <a href="<?php echo base_url();?>camera">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-camera fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Add Camera</span></h5></div>
                     </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("AssignCameraList") ) {
            ?>
                <a href="<?php echo base_url();?>assigncamera">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                      <i class="fa fa-camera fa-fw danger"></i>
                      <div class="info">

                      <h5><span style="font-size: 100%;">Assign Camera</span></h5></div>
                      </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("NewsLettersList") ) {
            ?>
                <a href="<?php echo base_url();?>newsletters">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-newspaper-o fa-fw danger"></i>
                     <div class="info">

                     <h5><span
                     style="font-size: 100%;">Create Newsletter</span></h5></div>
                     </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("AssignNewsletterList") ) {
            ?>
                <a href="<?php echo base_url();?>assignnewsletter">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-newspaper-o fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Assign Newsletters</span></h5></div>
                     </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("CommunicationCategoriesList") ) {
            ?>
                <a href="<?php echo base_url();?>communicationcategories">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                      <i class="fa fa-users fa-fw danger"></i>
                      <div class="info">

                      <h5><span style="font-size: 100%;">Communication Category</span></h5></div>
                      </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <a href="<?php echo base_url();?>communication">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                      <i class="fa fa-users fa-fw danger"></i>
                      <div class="info">

                      <h5><span style="font-size: 100%;">Add Communication</span></h5></div>
                      </div>
                  </div>
                </a>
            <?php 
                if ($this->privilegeduser->hasPrivilege("PermissionAddEdit") || $this->privilegeduser->hasPrivilege("PermissionList")) {
              ?>
                <a href="<?php echo base_url();?>permission">
                    <div class="col-md-3 col-xs-6 dash-menu">
                        <div class="box">
                        <i class="fa fa-lock fa-fw danger"></i>
                        <div class="info">

                        <h5> Permissions</h5></div>
                        </div>
                    </div>
                </a>
            <?php
              }
            ?>
            <?php 
                if ($this->privilegeduser->hasPrivilege("RoleAddEdit") || $this->privilegeduser->hasPrivilege("RoleList")) {
              ?>
                <a href="<?php echo base_url();?>roles">
                    <div class="col-md-3 col-xs-6 dash-menu">
                        <div class="box">
                        <i class="fa fa-user fa-fw danger"></i>
                        <div class="info">

                        <h5> Roles</h5></div>
                        </div>
                    </div>
                </a>
            <?php
              }
            ?>
            <?php 
                if ($this->privilegeduser->hasPrivilege("UserAddEdit") || $this->privilegeduser->hasPrivilege("UserList")) {
              ?>
                <a href="<?php echo base_url();?>users">
                    <div class="col-md-3 col-xs-6 dash-menu">
                        <div class="box">
                        <i class="fa fa-user fa-fw danger"></i>
                        <div class="info">

                        <h5> Admin Users</h5></div>
                        </div>
                    </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("CenterUserAddEdit") || $this->privilegeduser->hasPrivilege("CenterUserList")) {
            ?>
                <a href="<?php echo base_url();?>centerusers">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                      <i class="fa fa-user fa-fw danger"></i>
                      <div class="info">

                      <h5><span style="font-size: 100%;">Center Users </span></h5></div>
                      </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("AcademicYearList") ) {
            ?>
                <a href="<?php echo base_url();?>academicyear">
                    <div class="col-md-3 col-xs-6 dash-menu">
                        <div class="box">
                       <i class="fa fa-calendar fa-fw danger"></i>
                       <div class="info">

                       <h5><span style="font-size: 100%;">Academic Year Master</span></h5></div>
                       </div>
                    </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("CategoryAddEdit") || $this->privilegeduser->hasPrivilege("CategoryList")) {
            ?>
                <a href="<?php echo base_url();?>categories">
                    <div class="col-md-3 col-xs-6 dash-menu">
                        <div class="box">
                            <i class="fa fa-laptop fa-fw danger"></i>
                            <div class="info">
                                <h5><span style="font-size: 100%;">Category Master</span></h5>
                            </div>
                        </div>
                    </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("CourseAddEdit") || $this->privilegeduser->hasPrivilege("CourseList")) {
            ?>
                <a href="<?php echo base_url();?>coursesmaster">
                    <div class="col-md-3 col-xs-6 dash-menu">
                        <div class="box">
                         <i class="fa fa-laptop fa-fw danger"></i>
                         <div class="info">

                         <h5><span style="font-size: 100%;">Course Master</span></h5></div>
                         </div>
                    </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("BatchList") ) {
            ?>
                <a href="<?php echo base_url();?>batchmaster">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-laptop fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Batch Master</span></h5></div>
                     </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("ZoneAddEdit") || $this->privilegeduser->hasPrivilege("ZoneList")) {
            ?>
                <a href="<?php echo base_url();?>zonemaster">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                      <i class="fa fa-building fa-fw danger"></i>
                      <div class="info">

                      <h5><span style="font-size: 100%;">Zone Master </span></h5></div>
                      </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("CenterAddEdit") || $this->privilegeduser->hasPrivilege("CenterList")) {
            ?>
                <a href="<?php echo base_url();?>centermaster">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                      <i class="fa fa-building fa-fw danger"></i>
                      <div class="info">

                      <h5><span style="font-size: 100%;">Center Master </span></h5></div>
                      </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("GroupList") ) {
            ?>
                <a href="<?php echo base_url();?>groupmaster">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                      <i class="fa fa-users fa-fw danger"></i>
                      <div class="info">

                      <h5><span style="font-size: 100%;">Group Master</span></h5></div>
                      </div>
                  </div>
                </a>
            <?php
              }
            ?> 
            <?php 
              if ($this->privilegeduser->hasPrivilege("FeedbackList") || $this->privilegeduser->hasPrivilege("FeedbackExport")) {
            ?>
                <a href="<?php echo base_url();?>feedbacks">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-comment fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Feedbacks</span></h5></div>
                     </div>
                  </div>
                </a>
            <?php
              }
            ?>
              <a href="<?php echo base_url();?>album">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-file-image-o fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Album </span></h5></div>
                     </div>
                  </div>
                </a>

                <a href="<?php echo base_url();?>assignalbum">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-file-image-o fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Assign Album </span></h5></div>
                     </div>
                  </div>
                </a>
            <?php 
              if ($this->privilegeduser->hasPrivilege("SubroutinesList") ) {
            ?>
                <a href="<?php echo base_url();?>subroutines">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                      <i class="fa fa-clock-o fa-fw danger"></i>
                      <div class="info">

                      <h5><span style="font-size: 100%;">Subroutines</span></h5></div>
                      </div>
                  </div>
                </a>
            <?php
              }
            ?>
            <?php 
              if ($this->privilegeduser->hasPrivilege("RoutineActionList") ) {
            ?>
                <a href="<?php echo base_url();?>routinesactions">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                     <i class="fa fa-clock-o fa-fw danger"></i>
                     <div class="info">

                     <h5><span style="font-size: 100%;">Routine Actions</span></h5></div>
                     </div>
                  </div>
                </a>
            <?php
              }
            ?>
          <?php }else{?>
                <a href="<?php echo base_url();?>centertimetables">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                    <i class="fa fa-calendar fa-fw danger"></i>
                    <div class="info">

                    <h5><span style="font-size: 100%;">Time Tables </span></h5>
                    </div>
                  </div>
                  </div>
                </a>
                <a href="<?php echo base_url();?>documentfolders">
                  <div class="col-md-3 col-xs-6 dash-menu">
                    <div class="box">
                    <i class="fa fa-folder fa-fw danger"></i>
                    <div class="info">

                    <h5><span style="font-size: 100%;">View Documents </span></h5>
                    </div>
                  </div>
                    </div>
                </a>
            
          <?php }?>
          </div>
          </div>
          </div>
        </section>
    </div> -->
    
  </div>

<!-- end: Content --> 

<script>
$(document).ready(function(){
	$('.recent-msg-txt').click(function(){
		$(this).next().fadeToggle(100);
	});	
});

$(function() {
    $('#toggle-event').change(function() {
      var status = $(this).prop('checked');
      if(status){
        status='yes';
      }else{
        status='no';
      }
      $.ajax({
				url: "<?php echo base_url()?>home/onlineStatus",
				async: false,
        data:{status:status},
				type: "POST",
        dataType:"json",
				success: function(data){
					//return false;
					
					if(data['status'])
					{
						displayMsg("success",data['msg']);
						
					}
					else
					{
						displayMsg("error","Something Went Worng!");
						setTimeout("location.reload(true);",1000);
					}
				},
        error:function(){
          displayMsg("error","Something Went Worng!");
						setTimeout("location.reload(true);",1000);
        }
                
			});
    })
  })
	
</script>

<!-- Vertical Chart -->
<script>
google.charts.load('current', {packages: ['corechart', 'bar']});
google.charts.setOnLoadCallback(drawMultSeries);

function drawMultSeries() {
      var data = new google.visualization.DataTable();
      data.addColumn('timeofday', 'Time of Day');
      data.addColumn('number', 'Motivation Level');
      data.addColumn('number', 'Energy Level');

      data.addRows([
        [{v: [8, 0, 0], f: '8 am'}, 1, .25],
        [{v: [9, 0, 0], f: '9 am'}, 2, .5],
        [{v: [10, 0, 0], f:'10 am'}, 3, 1],
        [{v: [11, 0, 0], f: '11 am'}, 4, 2.25],
        [{v: [12, 0, 0], f: '12 pm'}, 5, 2.25],
        [{v: [13, 0, 0], f: '1 pm'}, 6, 3],
        [{v: [14, 0, 0], f: '2 pm'}, 7, 4],
        [{v: [15, 0, 0], f: '3 pm'}, 8, 5.25],
        [{v: [16, 0, 0], f: '4 pm'}, 9, 7.5],
        [{v: [17, 0, 0], f: '5 pm'}, 10, 10],
      ]);

      var options = {
        title: 'Motivation and Energy Level Throughout the Day',
        hAxis: {
          title: 'Time of Day',
          format: 'h:mm a',
          viewWindow: {
            min: [7, 30, 0],
            max: [17, 30, 0]
          }
        },
        vAxis: {
          title: 'Rating (scale of 1-10)'
        }
      };

      var chart = new google.visualization.ColumnChart(
        document.getElementById('chart_div'));

      chart.draw(data, options);
    }
</script>

<!-- Pie Chart -->
<script>
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var options = {
          title: 'Visitors Report'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }

</script>  

<!-- Donut -->
<script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var options = {
          title: 'My Daily Activities',
          pieHole: 0.4,
        };

        var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
        chart.draw(data, options);
      }
    </script>
