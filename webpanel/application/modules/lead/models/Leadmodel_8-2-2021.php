<?PHP
class Leadmodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL){
	 	$this->db->insert($tbl_name,$data_array);
	 	$result_id = $this->db->insert_id();
	 	/*echo $result_id;
	 	exit;*/
	 	if($sendid == 1){
	 		//return id
	 		return $result_id;
	 	}
	}

	
	function getOptions($tbl_name,$tbl_id,$comp_id){
		//echo "Cat ID: ".$category_id;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getdata_orderby_limit1($table, $fields, $condition = '1=1', $order_by){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition $order_by limit 1");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getData($table,$fields,$condition){
		$this -> db -> select($fields);
		$this -> db -> from($table);
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	 
	function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		//print_r($this->db->last_query());
		//exit;
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 
	 
	function getRecords($get){
		//echo "here...<br/>";
		//print_r($get);
		
		$table = "tbl_inquiry_master";
		$table_id = 'i.inquiry_master_id';
		$default_sort_column = 'i.inquiry_master_id';
		$default_sort_order = 'desc';
		$condition = "  i.rec_type= 'Lead' ";
		$condition .= " && i.inquiry_master_id NOT IN (select lead_id from tbl_inquiry_master where rec_type = 'Inquiry')";
		// $condition = "1=1  ";

		$colArray = array('lead_date','lead_no','i.inquiry_date','i.student_first_name','cu.course_name','i.how_know_about_school','z.zone_name','c.center_name','i.inquiry_progress');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		
		if(isset($get['sSearch_0']) && $get['sSearch_0']!=''){
			$condition .= " && z.zone_id ='".$_GET['sSearch_0']."'";
		}
		if(isset($get['sSearch_1']) && $get['sSearch_1']!=''){
			$condition .= " && c.center_id ='".$_GET['sSearch_1']."'";
		}
		if(isset($get['sSearch_2']) && $get['sSearch_2']!=''){
			$condition .= " && cu.course_id = '".$_GET['sSearch_2']."'";
		}
		if(isset($get['sSearch_3']) && $get['sSearch_3']!=''){
			$condition .= " && i.how_know_about_school = '".$_GET['sSearch_3']."'";
		}
		if(isset($get['sSearch_4']) && $get['sSearch_4']!=''){
			$condition .= " && i.inquiry_progress = '".$_GET['sSearch_4']."'";
		}
		/* if(isset($get['sSearch_5']) && $get['sSearch_5']!=''){
			$condition .= " && (select inquiry_no FROM tbl_inquiry_master as im WHERE i.lead_id = im.inquiry_master_id) LIKE '%".$_GET['sSearch_5']."%' || i.student_first_name LIKE '%".$_GET['sSearch_5']."%' || i.student_last_name LIKE '%".$_GET['sSearch_5']."%'";
		} */
		if(isset($get['sSearch_5']) && $get['sSearch_5']!=''){
			$condition .= " && (i.inquiry_master_id = '".$_GET['sSearch_5']."' || i.student_first_name LIKE '%".$_GET['sSearch_5']."%' || i.student_last_name LIKE '%".$_GET['sSearch_5']."%' )";
		}

		
		// echo "Condition: ".$condition;
		//exit;
		// $this -> db -> select('i.*,(select inquiry_no FROM tbl_inquiry_master as im WHERE i.lead_id = im.inquiry_master_id) as lead_no,(select created_on FROM tbl_inquiry_master as im WHERE i.lead_id = im.inquiry_master_id) as lead_date,a.academic_year_master_name, z.zone_name, c.center_name, ct.categoy_name, cu.course_name');
// 
		$this -> db -> select('i.*,i.inquiry_master_id as lead_no,i.created_on as lead_date,a.academic_year_master_name, z.zone_name, c.center_name, ct.categoy_name, cu.course_name');

		$this -> db -> from('tbl_inquiry_master as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_master_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cu', 'i.course_id  = cu.course_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);
		
		$query = $this -> db -> get();
		
		// print_r($this->db->last_query());
		// exit;
		
		// $this -> db -> select('i.*,(select inquiry_no FROM tbl_inquiry_master as im WHERE i.lead_id = im.inquiry_master_id) as lead_no,(select created_on FROM tbl_inquiry_master as im WHERE i.lead_id = im.inquiry_master_id) as lead_date,a.academic_year_master_name, z.zone_name, c.center_name, ct.categoy_name, cu.course_name');

		$this -> db -> select('i.*,i.inquiry_master_id as lead_no,i.created_on as lead_date,a.academic_year_master_name, z.zone_name, c.center_name, ct.categoy_name, cu.course_name');

		$this -> db -> from('tbl_inquiry_master as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_master_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cu', 'i.course_id  = cu.course_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		
		$query1 = $this -> db -> get();
		//echo "total: ".$query1 -> num_rows();
		//exit;
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
		
		
		//exit;
	}
	
	function getFormdata($ID){
		$this -> db -> select('i.*, a.academic_year_master_name, z.zone_name, c.center_name, ct.categoy_name, cu.course_name, co.country_name, s.state_name, cts.city_name');
		$this -> db -> from('tbl_inquiry_master as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_master_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cu', 'i.course_id  = cu.course_id', 'left');
		$this -> db -> join('tbl_countries as co', 'i.country_id  = co.country_id', 'left');
		$this -> db -> join('tbl_states as s', 'i.state_id  = s.state_id', 'left');
		$this -> db -> join('tbl_cities as cts', 'i.city_id  = cts.city_id', 'left');
		$this -> db -> where('i.inquiry_master_id', $ID);
	   	$query = $this -> db -> get();
	   	if($query -> num_rows() >= 1){
	     	return $query->result();
	   	}else{
	     	return false;
	   	}
	}
		
	function checkRecord($tbl_name,$POST,$condition){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this->db->where("($condition)");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}
	
	function getExportRecords($get){
		$table = "tbl_inquiry_master";
		$table_id = 'i.inquiry_master_id';
		$default_sort_column = 'i.inquiry_master_id';
		$default_sort_order = 'desc';
		$condition = "  i.rec_type= 'Lead' ";
		$condition .= " && i.inquiry_master_id NOT IN (select lead_id from tbl_inquiry_master where rec_type = 'Inquiry')";
		$colArray =  array('lead_date','lead_no','i.inquiry_date','i.student_first_name','cu.course_name','i.how_know_about_school','z.zone_name','c.center_name','i.inquiry_progress');
		$searchArray = array('lead_date','lead_no','i.inquiry_date','i.student_first_name','cu.course_name','i.how_know_about_school','z.zone_name','c.center_name','i.inquiry_progress');
		// sort order by column
		$sort = $default_sort_column;  
		$order = $default_sort_order;

		if(isset($get['sSearch_0']) && $get['sSearch_0']!=''){
			$condition .= " && z.zone_id ='".$get['sSearch_0']."'";
		}
		if(isset($get['sSearch_1']) && $get['sSearch_1']!=''){
			$condition .= " && c.center_id ='".$get['sSearch_1']."'";
		}
		if(isset($get['sSearch_2']) && $get['sSearch_2']!=''){
			$condition .= " && cu.course_id = '".$get['sSearch_2']."'";
		}
		if(isset($get['sSearch_3']) && $get['sSearch_3']!=''){
			$condition .= " && i.how_know_about_school = '".$get['sSearch_3']."'";
		}
		if(isset($get['sSearch_4']) && $get['sSearch_4']!=''){
			$condition .= " && i.inquiry_progress = '".$get['sSearch_4']."'";
		}
		/*if(isset($get['sSearch_5']) && $get['sSearch_5']!=''){
			$condition .= " && (select inquiry_no FROM tbl_inquiry_master as im WHERE i.lead_id = im.inquiry_master_id) LIKE '%".$get['sSearch_5']."%' || i.student_first_name LIKE '%".$get['sSearch_5']."%' || i.student_last_name LIKE '%".$get['sSearch_5']."%'";
		}*/
		if(isset($get['sSearch_5']) && $get['sSearch_5']!=''){
			$condition .= " && (i.inquiry_master_id = '".$_GET['sSearch_5']."' || i.student_first_name LIKE '%".$_GET['sSearch_5']."%' || i.student_last_name LIKE '%".$_GET['sSearch_5']."%' )";
		}
		
		// $this -> db -> select('i.*,(select inquiry_no FROM tbl_inquiry_master as im WHERE i.lead_id = im.inquiry_master_id) as lead_no,(select created_on FROM tbl_inquiry_master as im WHERE i.lead_id = im.inquiry_master_id) as lead_date,a.academic_year_master_name, z.zone_name, c.center_name, ct.categoy_name, cu.course_name');
		$this -> db -> select('i.*,i.inquiry_master_id as lead_no,i.created_on as lead_date,a.academic_year_master_name, z.zone_name, c.center_name, ct.categoy_name, cu.course_name');

		$this -> db -> from('tbl_inquiry_master as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_master_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cu', 'i.course_id  = cu.course_id', 'left');
		$this->db->where("($condition)");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			$totcount = $query -> num_rows();
			return array("query_result" => $query->result(), "totalRecords" => $totcount);
		}else{
			return array("totalRecords" => 0);
		}
	}

	function getCenterMappedCourses($center_id){
		$this -> db -> select('tc.course_id,tc.course_name');
		$this -> db -> from('tbl_center_courses as cc');
		$this -> db -> join('tbl_courses as tc', 'tc.course_id  = cc.course_id', 'left');
		$condition = array("cc.center_id"=>$center_id);
		$this->db->where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	function getdata_log($ID){
		
		$this -> db -> select('i.*,ls.status_description,lss.status_sub_description,ds.status_description default_status_description,les.lead_enquiry_status_description');
		$this -> db -> from('tbl_lead_enquiry_log as i');
		$this -> db -> join('tbl_lead_status as ls', 'ls.lead_status_id  = i.lead_status_id', 'inner');
		$this -> db -> join('tbl_lead_sub_status as lss', 'lss.lead_sub_status  = i.lead_sub_status_id', 'inner');
		$this -> db -> join('tbl_default_status as ds', 'ds.default_status_id  = i.lead_planned_status_id', 'inner');
		$this -> db -> join('tbl_lead_enquiry_status as les', 'les.lead_enquiry_status_id  = i.lead_enquiry_status_id', 'inner');
		$this -> db -> where('i.lead_id', $ID);
		$this -> db -> where('i.type', 1);
		$this -> db -> order_by('i.id','desc');
	   $query = $this -> db -> get();
	   
	//    print_r($this->db->last_query());
	//    exit;
	   
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
		
	}
	function getlastdata($condition){
		
		$this -> db -> select('i.*,ls.status_description,lss.status_sub_description,ds.status_description as default_status_description,les.lead_enquiry_status_description');
		$this -> db -> from('tbl_lead_enquiry_log as i');
		$this -> db -> join('tbl_lead_status as ls', 'ls.lead_status_id  = i.lead_status_id', 'inner');
		$this -> db -> join('tbl_lead_sub_status as lss', 'lss.lead_sub_status  = i.lead_sub_status_id', 'inner');
		$this -> db -> join('tbl_default_status as ds', 'ds.default_status_id  = i.lead_planned_status_id', 'inner');
		$this -> db -> join('tbl_lead_enquiry_status as les', 'les.lead_enquiry_status_id  = i.lead_enquiry_status_id', 'inner');
		$this -> db -> where($condition);
		$this -> db -> where('i.type', 1);
		$this -> db -> order_by('i.id','desc');
		$this -> db ->limit(1);
		
	   $query = $this -> db -> get();
	//    echo $this->db->last_query();exit;
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
		
	}
	function getDataDescLimit($table,$fields,$condition){
		$this -> db -> select($fields);
		$this -> db -> from($table);
		$this->db->where($condition);
		$this -> db -> order_by('ID','desc');
		$this ->db ->limit(1);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

}
?>