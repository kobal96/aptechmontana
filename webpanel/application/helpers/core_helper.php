<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function curlFunction($url, $post_fiels=null){
	//echo $url."<br/>";
	//echo "<pre>";print_r($post_fiels);exit;
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fiels);
	curl_setopt($ch, CURLOPT_ENCODING, 'gzip');
	
	// execute!
	$response = curl_exec($ch);

	// close the connection, release resources used
	curl_close($ch);
	
	return $response;
}

function genderDropdown($selected = null){
	$option_array = array('Male'=>'Male','Female'=>'Female');
	return ArrayToHTMLOptions($option_array, $selected);
}

function nameTitleDropdown($selected = null){
	$option_array = array('Mr'=>'Mr','Mrs'=>'Mrs','Miss'=>'Miss','Dr'=>'Dr','Ms'=>'Ms');
	return ArrayToHTMLOptions($option_array, $selected);
}

function StrLeft($s1, $s2) {
	return substr($s1, 0, strpos($s1, $s2));
}
				
function SelfURL(){
	$s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
	$protocol = Core::StrLeft(strtolower($_SERVER["SERVER_PROTOCOL"]), "/").$s;
	$port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":".$_SERVER["SERVER_PORT"]);
	return $protocol."://".$_SERVER['SERVER_NAME'].$port;
}

function GenRandomStr($length) {
	$characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	$string = '';    
	for ($p = 0; $p < $length; $p++) {
		$string.= $characters[mt_rand(0, strlen($characters)-1)];
	}
	return $string;
}


function seoUrl($string) {
    //Lower case everything
    $string = strtolower($string);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    
    return $string;
  }
  
  
function ArrayToHTMLOptions($option_array, $selected = null){
	$options = "";
	foreach ($option_array as $key => $val){
			
		$options .= (!is_null($selected) && $key == $selected) ? '<option value="'.$key.'" selected="selected">'.$val.'</option>' : '<option value="'.$key.'">'.$val.'</option>';
	}
	return $options;
}

function PrintArray($data=array()){
	echo "<pre>";
	print_r($data);
	echo "</pre>";
}

function PrependNullOption($option_list){
	return "<option value=''>----------Select-------</option>".$option_list;
}

function DisplayMessage($msg,$msg_type = 0, $autohide = 1) {
	$html= $class = $title = '';
	switch ($msg_type){
		case 1;
			$title="Success Message";
			$html = "<script type='text/javascript'>$(function(){ $.pnotify({type: 'success',title: '".$title."',text: '".$msg."',icon: 'picon icon16 iconic-icon-check-alt white',opacity: 0.95,hide:false,history: false,sticker: false});});</script>";
			break;
		case 2;
			$title="Notice Message";
			$html = "<script type='text/javascript'>$(function(){ $.pnotify({type: 'info',title: '".$title."',text: '".$msg."',icon: 'picon icon16 brocco-icon-info white',opacity: 0.95,hide:false,history: false,sticker: false});});</script>";
			break;			
		case 0;
		default:
			$title="Error Message";
			$html = "<script type='text/javascript'>$(function(){ $.pnotify({type: 'error',title: '".$title."',text: '".$msg."',icon: 'picon icon24 typ-icon-cancel white',opacity: 0.95,hide:false,history: false,sticker: false});});</script>";
			break;
	}
	return $html;
}

function FilterNullValues($array = array(), $filter_zero = false){
	
	return ($filter_zero===true) ? array_filter($array) : array_filter($array,'strlen');
}

function uploadFile($fieldname, $maxsize, $uploadpath, $extensions=false, $ref_name=false) {

	$upload_field_name = $_FILES[$fieldname]['name'];

	

	if(empty($upload_field_name) || $upload_field_name == 'NULL' ) {			

		return array('status'=>'error', 'msg'=>'Please upload the file ');

	}
	$value = explode(".",$upload_field_name);
	$file_extension = strtolower(end($value));

	

//		$file_extension = strtolower(pathinfo($upload_field_name, PATHINFO_EXTENSION));

	

	if($extensions !== false && is_array($extensions) ) {

		if(!in_array($file_extension,$extensions) ) {

			return array('status'=>'error', 'msg'=>'Please upload the valid file');

		}			

	}

	$file_size = @filesize($_FILES[$fieldname]["tmp_name"]);
	
	if ($file_size > $maxsize) {

		return array('status'=>'error', 'msg'=>'File Exceeds maximum limit');

	}

	if(isset($upload_field_name)) {

		if ($_FILES[$fieldname]["error"] > 0) {

			return array('status'=>'error', 'msg'=>'Error: '.$_FILES[$fieldname]['error']);

		}

	}

	if($ref_name == false ) {

		$file_name = time().str_replace(" ","_",$upload_field_name);

	} else {

		$file_name = str_replace(" ", "_",$ref_name).".".$file_extension;

	}
	if(!is_dir($uploadpath))
	{
		mkdir($uploadpath,0777);
	}
	if(move_uploaded_file($_FILES[$fieldname]["tmp_name"], $uploadpath.$file_name)) {			

		return array('status'=>'true', 'msg'=>$file_name);

	} else {

		return array('status'=>'error', 'msg'=>'Sorry unable to upload your file, Please try after some time.');			

	}

}

function UploadSingleFile($fieldname, $maxsize, $uploadpath, $extensions=false, $ref_name=false) {
	$upload_field_name = $_FILES[$fieldname]['name'];
	if(empty($upload_field_name) || $upload_field_name == 'NULL' ) {			
		return array('file'=>$_FILES[$fieldname]["name"], 'status'=>false, 'msg'=>'Please upload a file');
	}
	//$file_extension = strtolower(end(explode(".",$upload_field_name)));
	$file_extension = strtolower(pathinfo($upload_field_name, PATHINFO_EXTENSION));
	
	if($extensions !== false && is_array($extensions) ) {
		if(!in_array($file_extension,$extensions) ) {
			return array('file'=>$_FILES[$fieldname]["name"], 'status'=>false, 'msg'=>'Please upload valid file');
		}			
	}
	$file_size = @filesize($_FILES[$fieldname]["tmp_name"]);
	if ($file_size > $maxsize) {
		return array('file'=>$_FILES[$fieldname]["name"], 'status'=>false, 'msg'=>'File Exceeds maximum limit');
	}
	if(isset($upload_field_name)) {
		if ($_FILES[$fieldname]["error"] > 0) {
			return array('file'=>$_FILES[$fieldname]["name"], 'status'=>false, 'msg'=>'Error: '.$_FILES[$fieldname]['error']);
		}
	}
	if($ref_name == false ) {
		//$file_name = time().'_'.str_replace(" ","_",$upload_field_name);
		
		$file_name_without_ext =  $this->FileNameWithoutExt($upload_field_name);
		$file_name = time().'_'.Core::RenameUploadFile($file_name_without_ext).".".$file_extension;
	} else {
		$file_name = str_replace(" ", "_",$ref_name).".".$file_extension;
	}
	if(!is_dir($uploadpath))
	{
		mkdir($uploadpath,0777);
	}
	if(move_uploaded_file($_FILES[$fieldname]["tmp_name"], $uploadpath.$file_name)) {			
		return array('file'=>$_FILES[$fieldname]["name"], 'status'=>true, 'msg'=>'File Uploaded Successfully!', 'filename'=>$file_name);
	} else {
		return array('file'=>$_FILES[$fieldname]["name"], 'status'=>false, 'msg'=>'Sorry unable to upload your file, Please try after some time.');			
	}
}
function UploadMultipleFile($fieldname, $maxsize, $uploadpath, $index, $extensions=false, $ref_name=false) 
{
	$upload_field_name = $_FILES[$fieldname]['name'][$index];
	if(empty($upload_field_name) || $upload_field_name == 'NULL' ) {			
		return array('file'=>$_FILES[$fieldname]["name"][$index], 'status'=>false, 'msg'=>'Please upload a file');
	}
	
	//$file_extension = strtolower(end(explode(".",$upload_field_name)));
	$file_extension = strtolower(pathinfo($upload_field_name, PATHINFO_EXTENSION));
	
	if($extensions !== false && is_array($extensions) ) {
		if(!in_array($file_extension,$extensions) ) {
			return array('file'=>$_FILES[$fieldname]["name"][$index], 'status'=>false, 'msg'=>'Please upload valid file');
		}			
	}
	$file_size = @filesize($_FILES[$fieldname]["tmp_name"][$index]);
	if ($file_size > $maxsize) {
		return array('file'=>$_FILES[$fieldname]["name"][$index],'status'=>false, 'msg'=>'File Exceeds maximum limit');
	}
	if(isset($upload_field_name)) {
		if ($_FILES[$fieldname]["error"][$index] > 0) {
			return array('file'=>$_FILES[$fieldname]["name"][$index],'status'=>false, 'msg'=>'Error: '.$_FILES[$fieldname]['error']);
		}
	}
	$file_name = "";
	if($ref_name == false ) {
		$file_name_without_ext =  $this->FileNameWithoutExt($upload_field_name);
		$file_name = time().'_'.Core::RenameUploadFile($file_name_without_ext).".".$file_extension;
	} else {
		$file_name = Core::RenameUploadFile($ref_name).".".$file_extension;
	}
	if(!is_dir($uploadpath))
	{
		mkdir($uploadpath,0777);
	}
	if(move_uploaded_file($_FILES[$fieldname]["tmp_name"][$index], $uploadpath.$file_name)) {
		return array('file'=>$_FILES[$fieldname]["name"][$index], 'status'=>true, 'msg'=>'File Uploaded Successfully!', 'filename'=>$file_name);
	} else {
		return array('file'=>$_FILES[$fieldname]["name"][$index], 'status'=>false, 'msg'=>'Sorry unable to upload your file, Please try after some time.');			
	}
}

/**
 * @author : Rajan Rawal
 * @desc: This function filters the uploaded file name and properly rename it 
 * @param: $data : data string
 * changes : Other 4 characters are added 
 */
function RenameUploadFile($data) {
	$search = array("'"," ","(",")",".","&","-","\"","\\","?",":","/");
	$replace = array("","_","","","","","","","","","","");
	$new_data=str_replace($search, $replace, $data);
	return strtolower($new_data);
}

function FileNameWithoutExt($filename){
	return substr($filename, 0, (strlen ($filename)) - (strlen (strrchr($filename,'.'))));
}

function PadString($number,$total_length, $prefix_text = '', $postfix_text = '',$padding_char = "0", $pad_side = 'left'){
	
	$string = '';
	switch ($pad_side){
		case 'right':
			$string = str_pad($number, $total_length, $padding_char, STR_PAD_RIGHT);
			break;
		default:
		case 'left':
			$string = str_pad($number, $total_length, $padding_char, STR_PAD_LEFT);
			break;
	}
	return $prefix_text.$string.$postfix_text;
}
function PageRedirect($page) 
{
	print "<script type='text/javascript'>";
	print "window.location = '$page'";
	print "</script>";
	@header ("Location : $page");
	exit;
}
function RedirectTo($page)
{
	if (!headers_sent())
	{
		header("Location: ".$page);
		exit;
	}
	else
	{
		echo '<script type="text/javascript">';
		echo 'window.location.href="'.$page.'";';
		echo '</script>';
		echo '<noscript>';
		echo '<meta http-equiv="refresh" content="0;url='.$page.'" />';
		echo '</noscript>'; 
		exit;
	}	
}

function array_diff_multidimensional($session, $post) { 
	$result = array();
	foreach($session as $sKey => $sValue){
		foreach($post as $pKey => $pValue) {
			if((string) $sKey == (string) $pKey) {
				$result[$sKey] = array_diff($sValue, $pValue);
			}
		}
	}
	return $result;
}
function array_search2d($needle, $haystack) 
{
	for ($i = 0, $l = count($haystack); $i < $l; ++$i) {
		if (in_array($needle, $haystack[$i])) return $i;
	}
	return false;
}

function YMDToDMY($ymd, $show_his = false){
	return ($show_his) ? date('d-m-Y h:i:s A',strtotime($ymd)) : date('d-m-Y',strtotime($ymd));
}

function DMYToYMD($dmy, $show_his = false){
	return date('Y-m-d',strtotime($dmy));
}

function aasort (&$array, $key) 
{
	$sorter=array();
	$ret=array();
	reset($array);
	foreach ($array as $ii => $va) {
		if(!empty($va[$key]))
		{
			$sorter[$ii]=$va[$key];
		}else{
			$sorter[$ii]="";
		}
	}
	asort($sorter);
	foreach ($sorter as $ii => $va) {
		if(!empty($array[$ii]))
		{
			$ret[$ii]=$array[$ii];
		}else{
			$ret[$ii]="";
		}
	}
	$array=$ret;
}
function getExcelColumns($obj,$collength)
{
	$colNumber = PHPExcel_Cell::columnIndexFromString($obj->getActiveSheet()->getHighestDataColumn());
	if($collength != $colNumber)
	{
		return false;
	}else{
		return true;
	}
}
function getExcelRows($obj,$minrows)
{
	$rows = $obj->getActiveSheet()->getHighestRow();
	if($rows < $minrows)
	{
		return false;
	}else{
		return true;
	}
	
}

function CreateWhereForSingleTable($search){
	
	$new_array_without_nulls = Core::FilterNullValues($search);
//	echo "<pre>";
//	print_r($new_array_without_nulls);
//	echo "</pre>";
		
	$condition = "";
	foreach ($new_array_without_nulls as $key => $val){
		
		$match_cond = (is_numeric($val)) ? "$key=$val" : ((strtotime($val)) ? "$key='$val'" : "$key like '%$val%'");
		$condition .= ($condition=='') ? " $match_cond" : " && $match_cond";	
	}
	return $condition;
}

function DaysDiffFromToday($date){
	
	$now = time(); // or your date as well
	$your_date = strtotime($date);
	$datediff = $now - $your_date;
	return floor($datediff/(60*60*24));
}

function DaysDiffBetweenTwoDays($startdate,$enddate){
	
	$now = strtotime($startdate); // or your date as well
	$your_date = strtotime($enddate);
	$datediff = $now - $your_date;
	return floor($datediff/(60*60*24));
}
/* creates a compressed zip file */
function CreateZip($files = array(),$destination = '',$overwrite = false) {
	//if the zip file already exists and overwrite is false, return false
	if(file_exists($destination) && !$overwrite) { return false; }
	//vars
	$valid_files = array();
	//if files were passed in...
	if(is_array($files)) {
		//cycle through each file
		foreach($files as $file) {
			//make sure the file exists
			if(file_exists($file)) {
				$valid_files[] = $file;
			}
		}
	}
	//if we have good files...
	if(count($valid_files)) {
		//create the archive
		$zip = new ZipArchive();
		if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
			return false;
		}
		//add the files
		foreach($valid_files as $file) {
			$new_filename = substr($file,strrpos($file,'/') + 1);
			$zip->addFile($file,$new_filename);
		}
		//debug
		//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
		
		//close the zip -- done!
		$zip->close();
		
		//check to make sure the file exists
		return file_exists($destination);
	}
	else
	{
		return false;
	}
}

function checklogin(){
    if(empty($_SESSION["webadmin"]))
   {
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            echo json_encode(array('success'=>false,
                                      'msg'=>'redirect'));
            exit();
        }
        else{
            redirect('login', 'refresh');
            exit();
        }
   }
}

function get_payable_price_after_taxes($tax_array=array(),$base_price=0,$percent_commission = 0,$fix_commission=0){
  $total_taxable_price = 0;
  if(is_array($tax_array) && count($tax_array > 0)){
    foreach($tax_array as $tax_percent){
      $total_taxable_price+=$base_price*$tax_percent/100;
    }
  }
  // calculate commission from $percent_commission -- Start
  $commission_from_percent = $base_price*$percent_commission/100;
  // calculate commission from $percent_commission -- End
  $commission_amount = 0;
  
  // Check if fix comission
  if($commission_from_percent >= $fix_commission){
    $commission_amount = $commission_from_percent;
  }else{
    $commission_amount = $fix_commission;
  }
  
  $total_payable_amount = $base_price - ($commission_amount+$total_taxable_price);
  if($total_payable_amount < 0){
    $total_payable_amount = 0;
  }
  
  return array($base_price,$total_taxable_price,$commission_amount);
}

function get_payable_price_after_taxes_for_service_new($tax_array=array(),$base_price=0,$percent_commission = 0,$fix_commission=0){
  $total_taxable_price = 0;
  if(is_array($tax_array) && count($tax_array > 0)){
    foreach($tax_array as $tax_percent){
      $total_taxable_price+=$base_price*$tax_percent/100;
    }
  }
  // calculate commission from $percent_commission -- Start
  $commission_from_percent = $base_price*$percent_commission/100;
  // calculate commission from $percent_commission -- End
  $commission_amount = 0;
  
  // Check if fix comission
  if($commission_from_percent >= $fix_commission){
    $commission_amount = $commission_from_percent;
  }else{
    $commission_amount = $fix_commission;
  }
  
  $total_payable_amount = $base_price + ($commission_amount+$total_taxable_price);
  if($total_payable_amount < 0){
    $total_payable_amount = 0;
  }
  
  return array($total_payable_amount,$total_taxable_price,$commission_amount);
}


function sendNotification($title='',$notification = '',$email_content=  '',$fcm_token = '',$subject='',$from_email='',$to_email='',$cc_email = '',$notification_type = array()){
		$CI =& get_instance();
		/*$rr = array (
						'to' => $fcm_token,
						'notification' => array (
						"body" => $notification,
						"title" => $title,
						"icon" => "myicon"
					));
		return $rr;exit;*/
		if(!empty($fcm_token) && !empty($notification)){
      
			// Send mobile notification
				
			// API access key from Google API's Console
			//$api_access_key =  'AIzaSyAyq66X77tVJ2VNzCOGhsN1tCo0hPzbqN8' ;
			$api_access_key = 'AAAARS5W8ng:APA91bESBZvNP6R4TkDcMeMlxM-h4jwj4Xayz5O8pxrsz_ZakMnCB2WGVfamFNNvLMHH5VmBJYlbdIrtLE2pGEHpnhUMnT9IM4oRZ1ml_LWWh5xXsuKUC8MyDsN9ZXxUUUaQgavB7o34';
			
		 
			$url = 'https://fcm.googleapis.com/fcm/send';
			$fields = array (
						'to' => $fcm_token,
						'notification' => array (
						"body" => $notification,
						"title" => $title,
						"icon" => "myicon"
					),
					'data'=> $notification_type
			);
		 
			$fields = json_encode ( $fields );
			$headers = array (
				'Authorization: key=' . $api_access_key,
				'Content-Type: application/json'
			);
		 
			$ch = curl_init ();
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_POST, true );
			curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

			$result = curl_exec ( $ch );
			curl_close ( $ch );
      
		}else if(!empty($fcm_token) && empty($notification)){
			// Send mobile notification
				
			// API access key from Google API's Console
			//$api_access_key =  'AIzaSyAyq66X77tVJ2VNzCOGhsN1tCo0hPzbqN8' ;
			$api_access_key = 'AAAARS5W8ng:APA91bESBZvNP6R4TkDcMeMlxM-h4jwj4Xayz5O8pxrsz_ZakMnCB2WGVfamFNNvLMHH5VmBJYlbdIrtLE2pGEHpnhUMnT9IM4oRZ1ml_LWWh5xXsuKUC8MyDsN9ZXxUUUaQgavB7o34';
				
			$ios_epoctime = time()+120;
			$url = 'https://fcm.googleapis.com/fcm/send';
			$fields = array (
						'to' => $fcm_token,
						/*'notification' => array (
						"body" => $notification,
						"title" => $title,
						"icon" => "myicon"
						),*/
					'data' => $notification_type,
					'android' => array(
						"ttl" => "120s",
						"priority"=>"high"
					),
					'apns' => array(
						"notification" => array (
						"body" => "For IOS calling123",
						"title" => "Test Title",
						"icon" => "myicon"
						)
					)
			);
		 
			$fields = json_encode ( $fields );
			$headers = array (
				'Authorization: key=' . $api_access_key,
				'Content-Type: application/json'
			);
		 
			$ch = curl_init ();
			curl_setopt ( $ch, CURLOPT_URL, $url );
			curl_setopt ( $ch, CURLOPT_POST, true );
			curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
			curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
			curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );

			$result = curl_exec ( $ch );
			curl_close ( $ch );
		}
		
    if(!empty($email_content)&& !empty($from_email)&& !empty($to_email) && !empty($subject)){
						
			$CI->email->from($from_email); // change it to yours
			$CI->email->to($to_email); // change it to yours
      if(!empty($cc_email)){
        $CI->email->cc($cc_email);
      }
			$CI->email->subject($subject);
			$CI->email->message($email_content);
			
			if($CI->email->send()){
				
			} else {
				show_error($CI->email->print_debugger());                
			}
		}
	}
	
	
	function send_sms2($feed,$mobile,$message,$time,$jobname=null)
{

    $url = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?async=1&username=9920097917&password=panav2015&feedid=$feed&To=$mobile&Text=".urlencode($message)."&time=$time";

    if (!empty($jobname)) $url .= "&jobname=$jobname";

    $bulkpush_response = call_url($url);

    $response = ((array)simplexml_load_string($bulkpush_response));
    return $response['@attributes']['REQID'];
}

function call_url($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}  
	   
     
