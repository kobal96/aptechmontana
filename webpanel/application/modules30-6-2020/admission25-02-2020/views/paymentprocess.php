<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
		<div>
			<h1>Payment Process</h1>            
		</div>
    </div>
    <div class="card">       
    <div id='loadingmessage' style='display:none'>
		<img src='<?php echo base_url("/images/loading.gif"); ?>'/>
    </div>
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="feesInfoform-validate" method="post" enctype="multipart/form-data">
						<input type="hidden" id="student_id" name="student_id" value="<?php if(!empty($student_id)){echo $student_id;}?>" />
						<table class="table table-striped sort-table" style="border: 1px">
							<thead>
								<th>Course/Group Name</th>
								<th>Payment Due Date</th>
								<th style="padding:10px;text-align:center;">Installment Due</th>
								<th style="padding:10px;text-align:center;">Amount Paid<span class="text-danger">*</span></th>
								<th style="padding:10px;text-align:center;">Balance Amount</th>
							</thead>
							<tbody>
								<?php
								$single_count = -1;
								if(!empty($paymentinfo_single_array)){
									$payment_key = -1;
									$admission_fees_id = 0;
									foreach($paymentinfo_single_array as $key=>$val){
										$single_count++;
								?>
									<tr class="Entries" >
										<td><?php echo !empty($val['course_group'])?$val['course_group']:""; ?></td>
										<td data-order="<?php echo strtotime($val['instalment_due_date']);?>"><?php echo $val['instalment_due_date']?></td>
										<td><input type="text" class="form-control instalment_amount"  name="instalment_amount[<?php echo $key; ?>]" style="text-align:right;" readonly="readonly" id="instalment_amount_<?php echo $single_count;?>" value="<?php echo $val['instalment_amount']?>" row_count="<?php echo $single_count; ?>" ></td>
										<td><input type="text" class="form-control instalment_collected_amount number_decimal_only collection" id="instalmentcollectedamount_<?php echo $single_count;?>" style="text-align:right;" name="instalment_collected_typewise_amount[<?php echo $val['fees_type'];?>][<?php echo (!empty($val['group_id']))?$val['group_id']:$val['course_id'] ;?>][<?php echo $key; ?>]" row_count="<?php echo $single_count;?>" value="" amount_type="<?php echo $val['fees_type'];?>" custom_id="<?php echo (!empty($val['group_id']))?$val['group_id']:$val['course_id'] ;?>" placeholder="0.00"></td>
										<td><input type="text" class="form-control instalment_remaining_amount"  id="instalmentremainingamount_<?php echo $single_count;?>" style="text-align:right;"  name="instalment_remaining_amount[<?php echo $key; ?>]" readonly="readonly" value="<?php echo $val['instalment_amount']?>"></td>

										<input type="hidden" name="admission_fees_instalment_id[<?php echo $key; ?>]" id="admission_fees_instalment_id_<?php echo $single_count;?>" value="<?php echo $val['admission_fees_instalment_id']?>">
										<input type="hidden" name="admission_fees_id[<?php echo $single_count; ?>]" value="<?php echo $val['admission_fees_id']?>">
										<input type="hidden" name="admission_fees_typewise_id[<?php echo $val['fees_type'];?>][<?php echo (!empty($val['group_id']))?$val['group_id']:$val['course_id'] ;?>][<?php echo $key; ?>]" value="<?php echo $val['admission_fees_id']?>">
										<input type="hidden" id="installment_collected_actual_amount_<?php echo $single_count;?>" name="installment_collected_actual_amount[<?php echo $single_count;?>]" value="">
									<tr>
								<?php 
									}
								}
								?>
								<tr>
									<td><b>Total</b></td>
									<td></td>
									<td class="total_installment_amount" style="text-align:right;"><?php echo $total_instalment_amount;?></td>
									<td class="total_collected_amount" style="text-align:right;"></td>
									<td class="total_remaining_amount" style="text-align:right;"></td>
								</tr>
							</tbody>
						</table>
						<h3>Receipt Details</h3>
						<div class="row form-group">
							<div class="col-md-6 control-group">
								<label for="">Receipt Date</label>
								<input type="text" class="form-control datepicker required" id="receipt_date" name="receipt_date" value="" title="Please select receipt date.">
							</div>
							<div class="col-md-6 control-group">
								<label for="">Issue Receipt for</label>
								<label for="guardianfather">
									<input type="radio" class="required receipt_for" id="guardianfather" name="receipt_for" value="father" title="Please select reciept behalf." <?php echo(!empty($student_common_details[0]['guardian']) && $student_common_details[0]['guardian'] == "father")?"checked":""; ?>>Father
								</label>
								<label for="guardianmother">
									<input type="radio" class="required receipt_for" id="guardianmother" name="receipt_for" value="mother" title="Please select reciept behalf." <?php echo(!empty($student_common_details[0]['guardian']) && $student_common_details[0]['guardian'] == "mother")?"checked":""; ?>>Mother
								</label>
								<label for="guardianguardian">
									<input type="radio" class="required receipt_for" id="guardianguardian" name="receipt_for" value="guardian" title="Please select reciept behalf." <?php echo(!empty($student_common_details[0]['guardian']) && $student_common_details[0]['guardian'] == "guardian")?"checked":""; ?> >Guardian
								</label>
								<div class="controls" >
									<?php 
										$receipt_for_name = "";
										if($student_common_details[0]['guardian'] == "mother"){
											$receipt_for_name = $student_common_details[0]['mother_name'];
										}else if($student_common_details[0]['guardian'] == "father"){
											$receipt_for_name = $student_common_details[0]['father_name'];
										}else{
											$receipt_for_name = $student_common_details[0]['parent_name'];
										}
									?>
									<input type="text" class="form-control required" name="receipt_for_name" id="receipt_for_name" value="<?php echo $receipt_for_name;?>" title = "This Value is Required." readonly>
								</div>
							</div>
						</div>
						<h3>Fill Payment Details</h3>
						<div class="row form-group">
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Payment Mode</span></label>
								<div class="controls" style="margin-top:10px;">
									<label for="payment_modeCheque">
										<input type="radio" id="payment_modeCheque" class="payment_mode" name="payment_mode" value="Cheque" <?php echo (!empty($details[0]->payment_mode) &&  ($details[0]->payment_mode== 'Cheque')) ? "checked" : ""?> checked="checked" >Cheque
									</label>
									<label for="payment_modeCash">
										<input type="radio" id="payment_modeCash" class="payment_mode" name="payment_mode" value="Cash" <?php echo (!empty($details[0]->payment_mode) &&  ($details[0]->payment_mode== 'Cash')) ? "checked" : ""?> >Cash
									</label>
									<label for="payment_modeNetbanking">
										<input type="radio" id="payment_modeNetbanking" class="payment_mode" name="payment_mode" value="Netbanking" <?php echo (!empty($details[0]->payment_mode) &&  ($details[0]->payment_mode== 'Netbanking')) ? "checked" : ""?> >Netbanking
									</label>
								</div>
							</div>
							<div class="col-md-6 control-group both">
								<label class="control-label"><span>Bank Name*</span></label>
								<div class="controls">
									<input type="text" class="form-control" name="bank_name" value="<?php if(!empty($details[0]->bank_name)){echo $details[0]->bank_name;}?>" id="bank_name">
								</div>
							</div>
						</div>

						<div class="row form-group cheque">
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Cheque No*</span></label>
								<div class="controls">
									<input type="text" class="form-control onlyNumber" name="cheque_no" value="<?php if(!empty($details[0]->cheque_no)){echo $details[0]->cheque_no;}?>" id="cheque_no" min="0">
								</div>
							</div>
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Cheque Date*</span></label>
								<div class="controls">
									<input type="text" class="form-control datepicker" placeholder="Select transaction_date date" id="transaction_date" name="cheque_date" value="<?php if(!empty($details[0]->transaction_date)){echo date("d-m-Y", strtotime($otherdetails[0]->transaction_date));}?>">
								</div>
							</div>
						</div>

						<div class="row form-group transaction">
							<div class="col-md-6 control-group cheque transaction">
								<label class="control-label"><span>Transaction No*</span></label>
								<div class="controls">
									<input type="text" class="form-control" name="transaction_id" value="<?php if(!empty($details[0]->transaction_id)){echo $details[0]->transaction_id;}?>" id="transaction_id" ">
								</div>
							</div>
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Transaction Date*</span></label>
								<div class="controls">
									<input type="text" class="form-control datepicker" placeholder="Select transaction_date date" id="transaction_date" name="transaction_date" value="<?php if(!empty($details[0]->transaction_date)){echo date("d-m-Y", strtotime($otherdetails[0]->transaction_date));}?>">
								</div>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Receipt Amount*</span></label>
								<div class="controls">
									<input type="text" class="form-control required" placeholder="Total Collected Amount" id="total_transaction_amount" name="total_transaction_amount" value="" title="Please enter the Receipt amount.">
								</div>
							</div>
						</div>
						<div style="clear:both; margin-bottom: 2%;"></div>
						<div class="form-actions">
							<button type="submit" class="btn btn-primary save">Save</button>
							<a href="<?php echo base_url();?>admission" class="btn btn-primary">Cancel</a>
						</div>
					</form>
                </div>
            <div class="clearfix"></div>
            </div>
         </div>
    </div>        
	</div><!-- end: Content -->								
<script>
$(document).on('keypress','.onlyNumber',function(event){
	if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
		event.preventDefault(); 
	}
});
 
$( document ).ready(function() {
	$('.cheque').hide();
	$('.transaction').hide();
	$('.both').hide();
	calculateSum();
	$(".collection").on("keydown keyup", function() {
        calculateSum();
	});
	$('.payment_mode').trigger("change");
});
$(document).on('keypress','.number_decimal_only',function(e){
    if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
        return false;
    }
})
var remaining_amount = 0;
var total_installment_amount = 0
$(".instalment_remaining_amount").each(function(){
	total_installment_amount = (parseFloat(total_installment_amount) + parseFloat($(this).val()));
});
$(".total_installment_amount").text(parseFloat(total_installment_amount).toFixed(2));
function calculateSum() {
    var sum = 0;
    $(".collection").each(function() {
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);
        }
    });

    $(".total_collected_amount").text(sum.toFixed(2));
    let total_amount = "<?php  echo $total_instalment_amount;?>";
	remaining_amount = 0;
	$(".instalment_remaining_amount").each(function(){
		remaining_amount = (parseFloat(remaining_amount) + parseFloat($(this).val()));
	});

	$('.total_remaining_amount').text(parseFloat(remaining_amount).toFixed(2));
}

let total_collected_amount = 0
$('.instalment_collected_amount').keyup(function(){
	let installmentid = $(this).attr('row_count');
	let collected_amount = $(this).val()
	$("#installment_collected_actual_amount_"+installmentid).val(collected_amount);
	let installment_amount = $('#instalment_amount_'+installmentid).val()
	let remaining_amount_input = $('#instalmentremainingamount_'+installmentid).val()
	if(collected_amount != ""){
		if(parseInt(collected_amount) > parseInt(installment_amount)){
			$(this).val('0')
			$("#installment_collected_actual_amount_"+installmentid).val("0");
			$('#instalmentremainingamount_'+installmentid).val(installment_amount);
		}
		else{			
			let remaining_amount = ($('#instalment_amount_'+installmentid).val() - collected_amount).toFixed(2)
			$('#instalmentremainingamount_'+installmentid).val(remaining_amount);
		}
	}else{
		$(this).val("0")
		$("#installment_collected_actual_amount_"+installmentid).val("0");
		$('#instalmentremainingamount_'+installmentid).val(installment_amount);
	}
})

$('.fees_collected_amount').keyup(function(){
	let collected_amount = $(this).val()
	let installmentid = $(this).attr('row_count');
	$("#installment_collected_actual_amount_"+installmentid).val(collected_amount);
	let installment_amount = $('#total_amount_'+installmentid).val();
	let remaining_amount_input = $(this).parent().parent().next().find('.fees_remaining_amount')
	if(collected_amount != ""){
		if(parseInt(collected_amount) > parseInt(installment_amount)){
			$(this).val('0')
			$("#installment_collected_actual_amount_"+installmentid).val("0");
			$('#feesremainingamount_'+installmentid).val(installment_amount);
		}else{			
			let remaining_amount = ($('#total_amount_'+installmentid).val() - collected_amount).toFixed(2)
			$('#feesremainingamount_'+installmentid).val(remaining_amount);
		}
	}else{
		$(this).val("0");
		$("#installment_collected_actual_amount_"+installmentid).val("0");
		$('#feesremainingamount_'+installmentid).val(installment_amount);
	}
})
$('.payment_mode').change(function(){
	// var payment_mode = $(this).val();
	var payment_mode = $("input[name='payment_mode']:checked"). val();
	if(payment_mode == 'Cheque'){
		$('.cheque').show();
		$('.transaction').hide();
		$('.both').show();
	}
	else if(payment_mode == 'Netbanking'){
		$('.cheque').hide();
		$('.transaction').show();
		$('.both').show();
	}
	else{
		$('.cheque').hide();
		$('.transaction').hide();
		$('.both').hide();
	}
})

$('.receipt_for').change(function(){
	var receipt_for_value = $("input[name='receipt_for']:checked"). val();
	if(receipt_for_value == "mother"){
		$("#receipt_for_name").val("<?php echo $student_common_details[0]['mother_name']; ?>");
	}else if(receipt_for_value == "father"){
		$("#receipt_for_name").val("<?php echo $student_common_details[0]['father_name']; ?>");
	}else{
		$("#receipt_for_name").val("<?php echo $student_common_details[0]['parent_name']; ?>");
	}
});


$(".datepicker").datetimepicker({
	format: 'DD-MM-YYYY',
});


$("#feesInfoform-validate").validate({
	submitHandler: function(form){
		$('#loadingmessage').show();
		var act = "<?php echo base_url();?>admission/admissionGroupFees";
		var receipt_date = $("#receipt_date").val();
		var recipt_for = $("input[name='receipt_for']:checked").val();
		$("#feesInfoform-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			beforeSubmit : function(arr, $form, options){
				var course_amount_flag = false;
				var group_amount_flag = false;
				var group_id = [];
				var temp_group_flag = [];
				$(".collection").each(function(){
					if($(this).attr("amount_type") == "Group" ){
						group_id.push($(this).attr("custom_id"));
					}
				});
				$(".collection").each(function(){
				amountobj = this;
					if($(amountobj).attr("amount_type") == "Class" && course_amount_flag == false){
						if($(amountobj).val() >= 500){
							course_amount_flag = true
						}
					}
					
					if($(amountobj).attr("amount_type") == "Group"){
						$.each(group_id,function(index,value){
						if(value == $(amountobj).attr("custom_id")){
							if($(amountobj).val() >= 500 && temp_group_flag[value] != true){
								temp_group_flag[value] = true;
							}
						}
						})
					}
				})
				if(course_amount_flag == false){
					$('#loadingmessage').hide();
					alert("Course amount should not empty or less than Rs.500 ");
					return false;
				}
				var group_error_flag = false;
				$.each(group_id,function(index,value){
					if(temp_group_flag[value] != true){
						$('#loadingmessage').hide();
						group_error_flag= true;
						alert("Please fill atleast one installment for each group.");
						return false;
					}
				});
				if(group_error_flag == true){
					return false;
				}
				// return false;
			},success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1"){
					$('#loadingmessage').hide();
					displayMsg("success",res['msg']);
						setTimeout(function(){
							let student_id = res['student_id'];
							
							$.ajax({
								url:"<?php echo base_url();?>admission/getPayemtDetails",
								data:{student_id:student_id,receipt_date:receipt_date,recipt_for:recipt_for},
								dataType: 'json',
								method:'post',
								success: function(data)
								{
									if(data['status_code'] == 200){
										let student_id = btoa(data['body']);
										$('#loadingmessage').hide();
										window.location = "<?php echo base_url();?>admission/viewAllInvoice?text="+student_id;
									}
									else{
										$('#loadingmessage').hide();
									}
								}
							});
						},1000);
				}else{	
					$('#loadingmessage').hide();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});
 
</script>					
<style>
.appended .appenddia{
	border-bottom: 1px solid black;
}
</style>
