<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');
session_start(); //we need to call PHP's session object to access it through CI
class Email extends CI_Controller

{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');

		$this->load->model('emailmodel', '', TRUE);
    if(!$this->privilegeduser->hasPrivilege("ListEmailContent")){
     redirect('home');
   }
	}

	function index()
	{
		if(!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('email/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function addEdit($id = NULL)
	{
		if(!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->emailmodel->getFormdata($record_id);
			$this->load->view('template/header.php');
			$this->load->view('email/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function fetch()
	{
		$get_result = $this->emailmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
			$temp = array();
			array_push($temp, $get_result['query_result'][$i]->title);
			array_push($temp, $get_result['query_result'][$i]->fromname);
			array_push($temp, $get_result['query_result'][$i]->subject);
			$actionCol = "";
			if($this->privilegeduser->hasPrivilege("EditEmailContent")){
				$actionCol.= '<a href="email/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->eid) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a>';
			}
			//$actionCol.= '&nbsp;&nbsp;<a href="javascript:void(0);" onclick="deleteData(\'' . $get_result['query_result'][$i]->eid . '\');" title="Delete"><i class="icon-remove-sign"></i></a>';
			array_push($temp, $actionCol);
			array_push($items, $temp);
		}

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}

	function submitForm()
	{
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			if (!empty($_POST['eid'])) {
				$result = $this->emailmodel->updateRecord($_POST, $_POST['eid']);
				if (!empty($result)) {
					echo json_encode(array(
						'success' => '1',
						'msg' => 'Record Updated Successfully.'
					));
					exit;
				}
				else {
					echo json_encode(array(
						'success' => '0',
						'msg' => 'Problem in data update.'
					));
					exit;
				}
			}
			else {
				$result = $this->emailmodel->insertData('tbl_emailcontents', $_POST, '1');
				if (!empty($result)) {
					echo json_encode(array(
						"success" => "1",
						'msg' => 'Record inserted Successfully.'
					));
					exit;
				}
				else {
					echo json_encode(array(
						"success" => "0",
						'msg' => 'Problem in data insert.'
					));
					exit;
				}
			}
		}
		else {
			return false;
		}
	}

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}

	function delRecord($id)
 {
	 $appdResult = $this->emailmodel->delrecord("tbl_emailcontents","eid",$id);
	 
	if($appdResult)
	{
		echo "1";
	}
	else
	{
		echo "2";	
			 
	}	
 }	
}

?>