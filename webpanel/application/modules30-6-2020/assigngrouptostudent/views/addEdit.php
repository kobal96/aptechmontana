 <?php 
$selcourses1 = array();
if(!empty($selcourses)){
	for($i=0; $i < sizeof($selcourses); $i++){
		$selcourses1[] = $selcourses[$i]->course_id;
	}
}
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
	.select2{
		width:98% !important;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
		<div>
		<h1>Assign To Groups</h1>            
		</div>
		<div>
		<ul class="breadcrumb">
			<li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
			<li><a href="<?php echo base_url();?>documentfoldermaster">Assign To Groups</a></li>
		</ul>
		</div>
	</div>
	<div class="card">       
		<div class="card-body">             
		<div class="box-content">
			<div class="col-sm-6 col-md-12">
				<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
					<?php if(!empty($edit) && $edit == '1'){?>
						<input type="hidden" class="control-label" name="academic_year_id" id="academic_year_id" value="<?= $student[0]['academic_year_id']?>">
						<input type="hidden" class="control-label" name="zone_id" id="zone_id" value="<?= $student[0]['zone_id']?>">
						<input type="hidden" class="control-label" name="hidden_center_id" id="hidden_center_id" value="<?= $student[0]['center_id']?>">
					<?php } ?> 
						<!--listing of all academic year --> 
					<div  id="academic_year_idmain" style="display:<?= ($edit == '1'?"none":"block") ?>">
						<div class="col-md-6 control-group form-group">
							<label class="control-label" >Select Academic Year*</label>
							<div class="controls">
								<select id="academic_year_id" name="academic_year_id" class="select2" <?= ($edit == '1'?"disabled":"") ?> >
									<option value="">Select Academic Year</option>
									<?php 
										if(isset($academic_details) && !empty($academic_details)){
											foreach($academic_details as $cdrow){
												$sel = ($cdrow->academic_year_master_id == $student[0]['academic_year_id']) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $cdrow->academic_year_master_id;?>" <?=$sel?>><?php echo $cdrow->academic_year_master_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<!--listing of all ZONE  --> 
					<div  id="zone_idmain" style="display:<?= ($edit == '1'?"none":"block") ?>">
						<div class="col-md-6 control-group form-group">
							<label class="control-label" >Select Zone*</label>
							<div class="controls">
								<select id="zone_id" name="zone_id" class="select2" onchange="getcenterlist(this.value)" <?= ($edit == '1'?"disabled":"") ?>>
									<option value="">Select Zone</option>
									<?php 
										if(isset($zone_details) && !empty($zone_details)){
											foreach($zone_details as $cdrow){
												$sel = ($cdrow->zone_id == $student[0]['zone_id']) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $cdrow->zone_id;?>"<?=$sel?>><?php echo $cdrow->zone_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<!--listing of all center  --> 
					<div  id="center_idmain" style="display:<?= ($edit == '1'?"none":"block") ?>">
						<div class="col-md-6 control-group form-group">
							<label class="control-label" >Select Center*</label>
							<div class="controls">
								<select id="center_id" name="center_id" onchange="getStudentlist(this.value);getCenterGroup()" class="select2" <?= ($edit == '1'?"disabled":"")?> >
									<option value="">Select Center </option>
								</select>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<?php if(empty($edit) && $edit == '0') {?>
						<div class="col-md-6 control-group form-group">
							<label class="control-label" >Select Student*</label>
							<div class="controls">
								<select id="student_id" name="student_id" class="select2" >
									<option value="">Select Student</option>
								<!--	<?php 
										if(isset($student) && !empty($student)){
											foreach($student as $cdrow){
									?>
										<option value="<?php echo $cdrow['student_id'];?>"><?php echo $cdrow['student_name'];?></option>
									<?php }}?>-->
								</select>
							</div>
						</div>
					<?php }else{?>
						<div class="col-md-12 control-group form-group">
							<div class="col-sm-6 col-xs-12" style="padding:0px;">
								<label class="control-label" >Student Name</label>
								<div class="controls">
									<input type="hidden" class="control-label" name="student_id" id="student_id" value="<?= $student[0]['student_id']?>">
									<input type="text" class=" form-control " id="student_name" value="<?= $student[0]['student_name']?>" disabled style="width:95%;">
							</div>
							</div>
							<div class="col-sm-6 col-xs-12" style="padding:0px;margin-top: 30px;">
								<button type="button" onclick="addstudenttobatch(<?= $student[0]['student_id']?>)" class="btn btn-primary pull-right" id="assignGroupButton" style="display:<?= ($edit == "0"?"none":"block")?>"><i class="fa fa-plus"></i> Assign New Group </button>
							</div>
						</div>
					<?php }?>
					<div class="clearfix"></div>
					<!--listing of all group --> 
					<div  id="group_idmain" style="display:<?= ($edit == '1'?"none":"block") ?>">
						<div class="col-md-6 control-group form-group">
							<label class="control-label" >Select Group*</label>
							<div class="controls">
								<select id="group_id" name="group_id" class="select2" onchange="getbacthlist(this.value)">
									<option value="">Select Group</option>
									<?php 
										/* if(isset($group_details) && !empty($group_details)){
											foreach($group_details as $cdrow){
									?>
										<option value="<?php echo $cdrow->group_master_id;?>"><?php echo $cdrow->group_master_name;?></option>
									<?php }} */?>
								</select>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<!--listing of all Batch --> 
					<div   id="batch_idmain" style="display:<?= ($edit == '1'?"none":"block") ?>">
						<div class="col-md-6 control-group form-group">
							<label class="control-label" >Select Batch*</label>
							<div class="controls">
								<select id="batch_id" name="batch_id" class="select2" onchange="">
									<option value="">Select Batch</option>
								</select>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="form-actions form-group col-sm-12">
						<button type="submit" class="btn btn-primary" id="submit_main"  style="display:<?= ($edit == "1"?"none":"")?>" >Submit</button>
						<a href="<?php echo base_url();?>assigngrouptostudent" id="cancel_main" class="btn btn-primary" style="display:<?= ($edit == "1"?"none":"")?>">Cancel</a>
					</div>
					<div class="clearfix"></div>
					<?php if($edit =='1'){?>
					<div id="tabwrapper"  style="margin-top:30px;">
					<!--group_detail_show-->
						<div class="table-responsive">
							<table class="table table-bordered ">
								<tr>
									<!--<th>Student Name</th>-->
									<th>Group Name</th>
									<th>Batch Name</th>
									<th>Action</th>
								</tr>
								<tbody class="c_table"></tbody>
								<?php
									foreach($student as $value){?>
											<tr>
												<!--<th><?= $value['student_name']; ?></th>-->
												<th><?= $value['group_master_name']; ?></th>
												<th><?= $value['batch_name']; ?></th>
												<th><button  type="button"class="btn btn-danger btn-sm" href="JavaScript:void(0);" title="delete" class="text-center"onclick="delete_user(<?= $value['student_id']; ?>,<?= $value['group_id']; ?>,<?= $value['batch_id']; ?>,this)" ><i class="fa fa-remove"></i> Delete</button></th>
											</tr>
									<?php }?>
							</table>
						</div>
					</div>
					<?php }?>
					</div>
					<div class="form-actions form-group col-sm-12">
						<a href="<?php echo base_url();?>assigngrouptostudent" id="cancel_main_edit" class="btn btn-primary" style="display:<?= ($edit != "1"?"none":"")?>">Cancel</a>
					</div>
				</form>
			</div>
		<div class="clearfix"></div>
		</div>
		</div>
	</div>        
</div><!-- end: Content -->								
<script>
$( document ).ready(function() {
<?php if(!empty($edit) && $edit == '1'){?>
    getcenterlist(<?= $student[0]['zone_id']?>,<?= $student[0]['center_id']?>);
    <?php 
        $group_ids = array();
        // $batch_ids = array();
        
        foreach($student as $value){
            array_push($group_ids,$value['group_id']);
        }
    ?>
    getstudentgroupnotin(<?= json_encode($group_ids) ?>);
<?php } ?>
});


function delete_user(student_id,group_id,batch_id,event){
    if(confirm("Are you sure to remove this group from student..?")){
		if(student_id != "" && group_id !=""){
			$.ajax({
				url:"<?php echo base_url();?>assigngrouptostudent/deleteStudent",
				data:{student_id:student_id,group_id:group_id,batch_id:batch_id},
				dataType: 'json',
				method:'post',
				success: function(response){
					console.log(response)
					if(response.success == "true"){
						displayMsg("success",response.msg);
						$(event).closest("tr").remove();
					}else{	
						displayMsg("error",response.msg);
						return false;
					}
				}
			});
		}
	}
    
}

function getStudentlist(center_id){
    if(center_id != ""){
			$.ajax({
			url:"<?php echo base_url();?>assigngrouptostudent/getStudentlist",
			data:{center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res.success){
				    $("#student_id").html("<option value=''></option>"+res.option);
				    $("#student_id").select2();
				}else{
					
				}
			}
		});
	}
    
}

function getbacthlist(group_id){
    
     <?php if(!empty($edit) && $edit == '1'){
       $batch_name_master_ids = array();
        foreach($student as $value){
            array_push($batch_name_master_ids,$value['batch_name_master_id']);
        }   
     }else{
         $batch_name_master_ids=null;
     } ?>

    	if(group_id != ""){
		
			$.ajax({
				url:"<?php echo base_url();?>assigngrouptostudent/getbacthlist",
				data:{group_id:group_id,batch_name_master_ids:<?= json_encode($batch_name_master_ids) ?>},
				dataType: 'json',
				method:'post',
				success: function(res){
					if(res.success){
					    $("#batch_id").html("<option value=''>Select Student</option>"+res.option);
					    $("#batch_id").select2();
					}else{
						
					}
				}
			});
		}
}

function getstudentgroupnotin(group_ids){
    if(student_id != ""){
		$.ajax({
			url:"<?php echo base_url();?>assigngrouptostudent/getstudentgroupnotin",
			data:{group_ids:group_ids},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res.success){
					$("#group_id").html("<option value=''>Select Group Name</option>"+res.option);
					$("#group_id").select2();
				}else{
					
				}
			}
		});
	}
}

function getCenterGroup(){
	var center_id = $("#center_id").val();
    if(center_id != ""){
		$.ajax({
			url:"<?php echo base_url();?>assigngrouptostudent/getCenterGroup",
			data:{center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res.success){
					$("#group_id").html("<option value=''>Select Group Name</option>"+res.option);
					$("#group_id").select2();
				}else{
					
				}
			}
		});
	}
}


/*function getstudentbatchnotin(batch_ids){
    if(student_id != ""){
		
			$.ajax({
				url:"<?php echo base_url();?>assigngrouptostudent/getstudentbatchnotin",
				data:{batch_ids:batch_ids},
				dataType: 'json',
				method:'post',
				success: function(res){
					if(res.success){
					    $("#batch_id").html("<option value=''>Select Group Name</option>"+res.option);
					    $("#batch_id").select2();
					}else{
						
					}
				}
			});
		}
}*/

function getcenterlist(zone_id,center_id=null){
    	if(zone_id != ""){
			$.ajax({
				url:"<?php echo base_url();?>assigngrouptostudent/getzonelist",
				data:{zone_id:zone_id,center_id:center_id},
				dataType: 'json',
				method:'post',
				success: function(res){
					if(res.success){
					    $("#center_id").html("<option value=''>Select Center</option>"+res.option);
					    $("#center_id").select2();
					}else{
						
					}
				}
			});
		}
    
}

function addstudenttobatch(student_id){
    $('#batch_idmain').css({"display": "block"});
    $('#group_idmain').css({"display": "block"});
    $('#academic_year_idmain').css({"display": "block"});
    $('#zone_idmain').css({"display": "block"});
    $('#center_idmain').css({"display": "block"});
    $('#submit_main').css({"display": "inline-block"});
    $('#cancel_main').css({"display": "inline-block"});
    $('#cancel_main_edit').hide();
    $("#assignGroupButton").hide();
}
/*function getStudentDetails(student_id){
	$(".c_table").html("");
    student_id = $('#student_id').val();
   // alert(student_id);
	if(student_id != ""){
		
			$.ajax({
				url:"<?php echo base_url();?>assigngrouptostudent/getStudentDetails",
				data:{student_id:student_id},
				dataType: 'json',
				method:'post',
				success: function(res){
					if(res['status']=="success"){
						$('.group_detail_show').show()
					
							$tr_html = "";
							
							
								$(".c_table").append($tr_html);
							
					
						
						
					}else{
						$('.group_detail_show').hide()
					}
				}
			});
		}else{
		$("#tabwrapper").slideUp();
	}
}*/
var vRules = {
	student_id:{required:true},
	group_id:{required:true},
	batch_id:{required:true},
	academic_year_id:{required:true},
	zone_id:{required:true},
	center_id:{required:true},
};
var vMessages = {
	student_id:{required:"Please select student name."},
	group_id:{required:"Please select group name."},
	batch_id:{required:"Please select batch name."},
	academic_year_id:{required:"Please select Academic Year."},
	zone_id:{required:"Please select zone name."},
	center_id:{required:"Please select center name."},
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>assigngrouptostudent/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res.success)
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>assigngrouptostudent";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Assign Group Student";

 
</script>					



