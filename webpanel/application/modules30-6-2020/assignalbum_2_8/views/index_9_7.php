
<!-- start: Content -->
<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1>Assign Album</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>assignalbum">Assign Album</a></li>
        </ul>
      </div>
    </div> 
    <div class="card">
    	<div class="page-title-border">
        	<div class="col-sm-12 col-md-12 left-button-top">
            	<?php 
					//if ($this->privilegeduser->hasPrivilege("UserAddEdit")) {
				?>
				
					<p>
						<a href="<?php  echo base_url();?>assignalbum/addEdit" class="btn btn-primary icon-btn"><i class="fa fa-plus"></i>Assign Album</a>
						
						
            <div class="clearfix"></div>
            </div>
        </div>     
		
        <div class="col-sm-12">
         	<div class="box-content form-horizontal product-filter">          
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="zonename" class="control-label">Zone Name</label>
						<input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="centername" class="control-label">Center Name</label>
						<input id="sSearch_1" name="sSearch_1" type="text" class="searchInput form-control"/>
					</div>
				</div>

				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="selectiontype" class="control-label">Selection Type</label>
						<input id="sSearch_2" name="sSearch_2" type="text" class="searchInput form-control"/>
					</div>
				</div>

				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="albumname" class="control-label">Album Name</label>
						<input id="sSearch_3" name="sSearch_3" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<div class="control-group clearFilter">
					<div class="controls">
						<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
					</div>
				</div>
				
				
            </div>
         </div>
		 
		 
         <div class="clearfix"></div>
         <div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                    <table class="dynamicTable display table table-bordered non-bootstrap" >
                        <thead>
                          <tr>
							<th>Zone Name</th>
							<th>Center Name</th>
							<th>Selection Type</th>
							<th>Album Name</th>
							<th data-bSortable="false">Status</th>
							<th data-bSortable="false">Action</th>
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
    </div>
</div><!-- end: Content -->
			
<script>
	$( document ).ready(function() {

		clearSearchFilters();
	});	
	
	
	function deleteData1(id,status)
	{
		var sta="";
		var sta1=" ";
		
		if(status=='Active')
		{
		
			sta="In-active";
			
		}
		else{
			
			sta="Active";
		}
		
		
		
    	var r=confirm("Are you sure to " +sta);
    	if (r==true)
   		{
    		//window.location.href="users/delete?id="+id;
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/delrecord12/",
				data:{"id":id,"status":sta},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Updated!");
						setTimeout("location.reload(true);",1000);
						
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});
    	}
    }
	
	document.title = "Assign Album";
</script>