<?PHP
class Assignfeesmodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
	 	//print_r($this->db->last_query());
	    //exit;
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}
	
	function getRecords($get=null){
		$table = "tbl_assign_fees";
		$table_id = 'f.assign_fees_id';
		$default_sort_column = 'f.assign_fees_id';
		$default_sort_order = 'desc';
		
		$condition = "";
		$condition .= "  1=1 ";
		
		$colArray = array('a.academic_year_master_name','f.fees_selection_type','fe.fees_level_name','fn.fees_name','z.zone_name','f.status');
		$searchArray = array('a.academic_year_master_name','f.fees_selection_type','fe.fees_level_name','fn.fees_name','z.zone_name','f.status');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<6;$i++)
		{
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $searchArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		//echo "condition ".$condition;exit;
		
		$this -> db -> select('f.*,a.academic_year_master_name, fe.fees_level_name,fn.fees_name, z.zone_name');
		$this -> db -> from('tbl_assign_fees as f');
		$this -> db -> join('tbl_academic_year_master as a', 'f.academic_year_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_fees_level_master as fe', 'f.fees_level_id  = fe.fees_level_id', 'left');
		$this -> db -> join('tbl_fees_master as fn', 'f.fees_id  = fn.fees_id', 'left');
		$this -> db -> join('tbl_zones as z', 'f.zone_id  = z.zone_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		$this -> db -> select('f.*,a.academic_year_master_name, fe.fees_level_name,fn.fees_name,z.zone_name');
		$this -> db -> from('tbl_assign_fees as f');
		$this -> db -> join('tbl_academic_year_master as a', 'f.academic_year_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_fees_level_master as fe', 'f.fees_level_id  = fe.fees_level_id', 'left');
		$this -> db -> join('tbl_fees_master as fn', 'f.fees_id  = fn.fees_id', 'left');
		$this -> db -> join('tbl_zones as z', 'f.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as ct', 'f.center_id  = ct.center_id', 'left');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);		
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
		
		
		//exit;
	}
	
	
	function getDropdown($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}
	
	function getFormdata($ID){
		
		//$condition = "1=1 && i.product_id= ".$ID." ";
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_assign_fees as i');
		$this->db->where('i.assign_fees_id', $ID);
		//$this->db->where("($condition)");
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}
	
	function getOptions($tbl_name,$tbl_id,$comp_id){
		//echo "Cat ID: ".$category_id;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptionFees($tbl_name,$tbl_id,$comp_id,$tbl_id1,$comp_id1){
		//echo "Cat ID: ".$category_id;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$this -> db -> where($comp_id1,$tbl_id1);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}
	
	
	function getdata($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }
	
	
	function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		//print_r($this->db->last_query());
		//exit;
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	}  
	
	function delrecord12($tbl_name,$tbl_id,$record_id,$status)
	{
		$data = array('status' => $status);
		 
		$this->db->where($tbl_id, $record_id);
		$this->db->update($tbl_name,$data); 
		 
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}	

	function enum_select($table, $field){
		$query = " SHOW COLUMNS FROM `$table` LIKE '$field' ";
		$row = $this->db->query(" SHOW COLUMNS FROM `$table` LIKE '$field' ")->row()->Type;
		$regex = "/'(.*?)'/";
		preg_match_all( $regex , $row, $enum_array );
		$enum_fields = $enum_array[1];
		return( $enum_fields );
	}

	function getMappedCourses($fees_id){
		$condition = array("cm.fees_id"=>$fees_id,"c.status"=>"Active");
		$this -> db -> select('c.course_name,c.course_id');
		$this -> db -> from('tbl_courses as c');
		$this -> db -> join('tbl_fees_master_course_mapping as cm', 'c.course_id  = cm.course_id', 'left');
		$this -> db -> where($condition);
		$this -> db -> group_by("c.course_id");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}

	function getMappedGroups($fees_id){
		$condition = array("gm.fees_id"=>$fees_id,"g.status"=>"Active");
		$this -> db -> select('g.group_master_name,g.group_master_id');
		$this -> db -> from('tbl_group_master as g');
		$this -> db -> join('tbl_fees_master_group_mapping as gm', 'g.group_master_id  = gm.group_master_id', 'left');
		$this -> db -> where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}

	function deletedata($tableName, $column, $value){
		$this->db->where("$column", $value);
		$this->db->delete($tableName);
		return true;
	}

	function getFeeLevelCenters($fees_level_id,$zone_id){
		$condition = array("c.zone_id"=>$zone_id,"c.status"=>"Active","cm.fees_level_id"=>$fees_level_id);
		$this -> db -> select('c.center_id,c.center_name');
		$this -> db -> from('tbl_centers as c');
		$this -> db -> join('tbl_fees_level_center_mapping as cm', 'cm.center_id  = c.center_id', 'left');
		$this -> db -> where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}

}
?>
