<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Fees Level Master</h1>            
          </div>
          <div>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
              <li><a href="<?php echo base_url();?>feeslevelmaster">Fees Level Master</a></li>
            </ul>
          </div>
    </div>
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
						<input type="hidden" id="fees_level_id" name="fees_level_id" value="<?php if(!empty($details[0]->fees_level_id)){echo $details[0]->fees_level_id;}?>" />
						<div class="control-group form-group">
							<label class="control-label"><span>Fees Level Name*</span></label>
							<div class="controls">
								<input type="text" class="form-control required" placeholder="Enter Name" id="fees_level_name" name="fees_level_name" value="<?php if(!empty($details[0]->fees_level_name)){echo $details[0]->fees_level_name;}?>"  maxlength="50" >
							</div>
						</div>
						<div class="control-group form-group">
							<label class="control-label"><span>Center*</span></label> 
							<div class="controls">
								<select id="center_id" name="center_id[]" multiple class="form-control center_id selectpicker" multiple data-width="98%"   data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Search ..." >
									<?php 
										if(isset($centers) && !empty($centers)){
											foreach($centers as $cdrow){
												$sel = "";
												if(!empty($selected_centers) && in_array($cdrow->center_id,$selected_centers)){
													$sel = 'selected';
												}
									?>
										<option value="<?php echo $cdrow->center_id;?>" <?php echo $sel; ?> ><?php echo $cdrow->center_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						<div class="control-group form-group">
							<label class="control-label" for="status">Status*</label>
							<div class="controls">
								<select name="status" id="status" class="form-control">
									<option value="Active" <?php if(!empty($details[0]->status) && $details[0]->status == "Active"){?> selected <?php }?>>Active</option>
									<option value="In-active" <?php if(!empty($details[0]->status) && $details[0]->status == "In-active"){?> selected <?php }?>>In-active</option>
								</select>
							</div>
						</div>
						<div style="clear:both; margin-bottom: 2%;"></div>
						
						<div class="form-actions form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
							<a href="<?php echo base_url();?>feeslevelmaster" class="btn btn-primary">Cancel</a>
						</div>
					</form>
                </div>
            <div class="clearfix"></div>
            </div>
         </div>
    </div>        
	</div><!-- end: Content -->								
<script>
var vRules = {
	fees_level_name:{required:true, alphanumericwithspace:true}
};
var vMessages = {
	fees_level_name:{required:"Please enter fees level name."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>feeslevelmaster/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>feeslevelmaster";
					},2000);

				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			},
			error:function(){
				$("#msg_display").html("");
				$("#display_msg").html("");
				displayMsg("error",res['msg']);
			}
		});
	}
});

document.title = "AddEdit - Fees Level";

 
</script>					
