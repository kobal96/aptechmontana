<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Fees Config</h1>            
          </div>
          <div>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
              <li><a href="<?php echo base_url();?>feesmaster">Fees Master</a></li>
            </ul>
          </div>
    </div>
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
						<input type="hidden" id="fees_id" name="fees_id" value="<?php if(!empty($details[0]->fees_id)){echo $details[0]->fees_id;}?>" />
						<input type="hidden" id="fees_component_data_id" name="fees_component_data_id" value="<?php if(!empty($details[0]->fees_component_data_id)){echo $details[0]->fees_component_data_id;}?>" />
						
						<div class="control-group form-group">
							<label class="control-label">Academic Year*</label>
							<div class="controls">
								<select id="academic_year_id" name="academic_year_id" class="form-control">
									<option value="" >Select Academic Year</option>
									<?php 
										if(isset($academic_year) && !empty($academic_year)){
											foreach($academic_year as $cdrow){
												$sel = ($cdrow->academic_year_master_id == $details[0]->academic_year_id) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $cdrow->academic_year_master_id;?>" <?php echo $sel; ?> ><?php echo $cdrow->academic_year_master_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>

						<?php if(empty($details[0]->fees_id)){?>
							<div class="control-group form-group">
								<label class="control-label"><span>Select Class / Group*</span></label>
								<div class="controls">
									<label for="class_type" style="margin-right: 60px;margin-top: 20px;">
										<input type="radio" class="fees_selection_type" name="fees_selection_type" value="Class" <?php echo (!empty($details[0]->fees_selection_type) &&  ($details[0]->fees_selection_type== 'Class')) ? "checked" : ""?>  id="class_type" checked="checked">Class
									</label>
									<label for="group_type" style="margin-right: 60px;margin-top: 20px;">
										<input type="radio" class="fees_selection_type" name="fees_selection_type" value="Group" <?php echo (!empty($details[0]->fees_selection_type) &&  ($details[0]->fees_selection_type== 'Group')) ? "checked" : ""?>  id="group_type">Group
									</label>
								</div>
							</div>
						<?php } else{?>
							<div class="control-group form-group">
								<label class="control-label"><span>Selected Class / Group *</span></label>
								<div class="controls">
								<?php if(!empty($details[0]->fees_selection_type) &&  ($details[0]->fees_selection_type== 'Class')){?>
										<label for="" style="margin-top:20px">
											<input type="radio" class="fees_selection_type" name="fees_selection_type" value="Class" checked="checked">Class
										</label>
									<?php } else{?>
										<label for="" style="margin-top:20px">
											<input type="radio" class="fees_selection_type" name="fees_selection_type" value="Group" checked="checked">Group
										</label>
									<?php }?>
								</div>
							</div>
						<?php }?>

						<div class="control-group form-group class_div">
							<label class="control-label" for="category_id">Category*</label>
							<div class="controls">
								<!-- <select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);"> -->
								<select id="category_id" name="category_id" class="form-control"  >
									<?php 
										if(!empty($categories)){
											foreach($categories as $cdrow){
												if(strtolower($cdrow->categoy_name) == "preschool"){
												$sel = "selected";
									?>
										<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
									<?php }
										}
									}?>
								</select>
							</div>
						</div>
						<div class="control-group form-group">
							<label class="control-label">Fees Level*</label>
							<div class="controls">
								<select id="fees_level_id" name="fees_level_id" class="form-control">
									<option value="">Select Fees Level</option>
									<?php 
										if(isset($fees_level) && !empty($fees_level)){
											foreach($fees_level as $cdrow){
												$sel = ($cdrow->fees_level_id == $details[0]->fees_level_id) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $cdrow->fees_level_id;?>" <?php echo $sel; ?>><?php echo $cdrow->fees_level_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						<div class="control-group form-group">
							<label class="control-label">Fees Type*</label>
							<div class="controls">
								<select id="fees_type" name="fees_type" class="form-control">
									<option value="">Select Fees Type</option>
									<?php 
										if(isset($fees_type_details) && !empty($fees_type_details)){
											foreach($fees_type_details as $key=>$val){
												$sel = ($val == $details[0]->fees_type) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $val;?>" <?php echo $sel; ?>><?php echo $val;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						<!-- <div class="control-group form-group">
							<label class="control-label"><span>Fees Type*</span></label>
							<div class="controls">
								<input type="radio" class="form-control fees_type" name="fees_type" value="LUMSPUM" <?php echo (!empty($details[0]->fees_type) &&  ($details[0]->fees_type== 'LUMSPUM')) ? "checked" : ""?> checked="checked">LUMSPUM
								<input type="radio" class="form-control fees_type" name="fees_type" value="INSTALLMENT" <?php echo (!empty($details[0]->fees_type) &&  ($details[0]->fees_type== 'INSTALLMENT')) ? "checked" : ""?> >INSTALLMENT
							</div>
						</div> -->

						<div class="control-group form-group">
							<label class="control-label"><span>Fees Name*</span></label>
							<div class="controls">
								<input type="text" class="form-control required" placeholder="Enter Name" id="fees_name" name="fees_name" value="<?php if(!empty($details[0]->fees_name)){echo $details[0]->fees_name;}?>" maxlength="150" >
							</div>
						</div>
						<div class="control-group form-group class_div">
							<label class="control-label"><span>Course*</span></label> 
							<div class="controls">
								<select id="course_id" name="course_id[]" class="form-control width100 selectpicker required" multiple data-width="98%"   data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Search ..." >
									<?php 
										if(isset($courses) && !empty($courses)){
											foreach($courses as $cdrow){
												$sel = "";
												if(!empty($selected_courses) && in_array($cdrow['course_id'],$selected_courses)){
													$sel = "selected";
												}
									?>
										<option value="<?php echo $cdrow['course_id'];?>" <?php echo $sel; ?>><?php echo $cdrow['course_name'];?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						<!--<div class="control-group form-group class_div">
							<label class="control-label"><span>Batch*</span></label> 
							<div class="controls">
								<select id="course_id" name="course_id[]" class="form-control width100 selectpicker required" multiple data-width="98%"   data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Search ..." >
									<?php 
									/*	if(isset($courses) && !empty($courses)){
											foreach($courses as $cdrow){
												$sel = "";
												if(!empty($selected_courses) && in_array($cdrow['course_id'],$selected_courses)){
													$sel = "selected";
												}
									?>
										<option value="<?php echo $cdrow['course_id'];?>" <?php echo $sel; ?>><?php echo $cdrow['course_name'];?></option>
									<?php }}*/?>
								</select>
							</div>
						</div>-->
						<div class="control-group form-group group_div">
							<label class="control-label" for="category_id">Group*</label>
							<div class="controls">
								<!-- <select id="group_id" name="group_id[]" class="form-control width100 selectpicker" multiple data-width="98%"   data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Search ..." onchange="getCourses(this.value);"> -->
								<select id="group_id" name="group_id[]" class="form-control width100 selectpicker required" multiple data-width="98%"   data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Search ..." >
									<?php 
										if(isset($groups) && !empty($groups)){
											foreach($groups as $cdrow){
												$sel = "";
												if(!empty($selected_groups) && in_array($cdrow->group_master_id,$selected_groups)){
													$sel = "selected";
												}
									?>
										<option value="<?php echo $cdrow->group_master_id;?>" <?php echo $sel; ?>><?php echo $cdrow->group_master_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						<div class="clearfix"></div>
						<!-- <?php if(!empty($details[0]->fees_id)){?>
						<div class="control-group form-group">
							<label class="control-label"><span>Components*</span></label> 
							<div class="controls">
								<select id="fees_component_id" name="fees_component_id[]" class="form-control fees_component_id">
									<option value="">Select Components</option>
									<?php 
										if(isset($fees_components) && !empty($fees_components)){
											foreach($fees_components as $cdrow){
												$sel = (in_array($cdrow->fees_component_master_id, $c_id_array)) ? 'selected="selected"' : '';
										?>
										<option value="<?php echo $cdrow->fees_component_master_id;?>" <?php echo $sel; ?>><?php echo $cdrow->fees_component_master_name;?></option>
										<?php }}?>
								</select>
							</div>
						</div>
						<?php }else{?>
							
						<div class="control-group form-group">
							<label class="control-label"><span>Components*</span></label> 
							<input type="checkbox" id="checkbox" >Select All
							<div class="controls">
								<select id="fees_component_id" name="fees_component_id[]" class="form-control select2 fees_component_id" multiple onchange="calcamount();">
									<option value="">Select Components</option>
									<?php 
									if(isset($fees_components) && !empty($fees_components)){
										foreach($fees_components as $cdrow){
											$sel = (in_array($cdrow->fees_component_master_id, $c_id_array)) ? 'selected="selected"' : '';
									?>
									<option value="<?php echo $cdrow->fees_component_master_id;?>" <?php echo $sel; ?>><?php echo $cdrow->fees_component_master_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						 <?php }?> -->

						
						<div class="input-daterange">
							<div class="control-group form-group ">
								<label class="control-label"><span>Fee Applicable FROM Batch Start Date*</span></label>
								<div class="controls">
									<input type="text" class="form-control required text-left" placeholder="Fee Applicable FROM Batch Start Date" id="fees_from_date" name="fees_from_date" value="<?php if(!empty($details[0]->fees_from_date)){echo date("d-m-Y",strtotime($details[0]->fees_from_date));}else{echo "";}?>" style="text-align:left;">
								</div>
							</div>

							<div class="control-group form-group ">
								<label class="control-label"><span>Fee Applicable To Batch Start Date*</span></label>
								<div class="controls">
									<input type="text" class="form-control required text-left" placeholder="Fee Applicable FROM Batch Start Date" id="fees_to_date" name="fees_to_date" value="<?php if(!empty($details[0]->fees_to_date)){echo date("d-m-Y",strtotime($details[0]->fees_to_date));}else{echo "";}?>" style="text-align:left;">
								</div>
							</div>
						</div>
						
						<div class="control-group form-group group_div">
							<label class="control-label"><span>Allow frequency*</span></label>
							<div class="controls">
								<label for="allow_frequency1">
									<input type="radio" class="required allow_frequency" id="allow_frequency1" name="allow_frequency" value="Yes" <?php echo(!empty($details[0]->allow_frequency) && $details[0]->allow_frequency == "Yes")?"checked":"checked";?>> Yes
								</label> &nbsp;&nbsp;&nbsp;
								<label for="allow_frequency2">
									<input type="radio" class="required allow_frequency" id="allow_frequency2" name="allow_frequency" value="No" <?php echo(!empty($details[0]->allow_frequency) && $details[0]->allow_frequency == "No")?"checked":"";?>> No
								</label>
							</div>
						</div>
						
						<div class="control-group form-group installment-wrapper <?php echo(!empty($details[0]->allow_frequency) && $details[0]->allow_frequency == "Yes")?"display-none":"";?>" >
							<label class="control-label"><span>Max No Of Installment*</span></label>
							<div class="controls">
								<input type="number" class="form-control required" placeholder="Enter Max No Of Installment" id="installment_no" name="installment_no" value="<?php if(!empty($details[0]->installment_no)){echo $details[0]->installment_no;}?>" >
							</div>
						</div>
						<div class="clearfix"></div>
						<div class="control-group form-group frequency-wrapper <?php echo(!empty($details[0]->allow_frequency) && $details[0]->allow_frequency == "No")?"display-none":"";?>">
							<label class="control-label"><span>Frequency*</span></label>
							<div class="controls">
								<select id="frequency" name="frequency[]" class="form-control width100 selectpicker required" multiple data-width="98%"   data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Search ..." >
										<?php 
											if(isset($frequency_details) && !empty($frequency_details)){
												foreach($frequency_details as $frow){
													$sel = "";
													if(!empty($selected_frequency) && in_array($frow['month_id'],$selected_frequency)){
														$sel = "selected";
													}
										?>
										<option value="<?php echo $frow['month_id'];?>" <?php echo $sel; ?>><?php echo $frow['name']." - [ ".$frow['discount']." % ]" ;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						<div class="clearfix"></div>
						<a class="minia-icon-file-add btn btn-primary tip" style="float: none; margin-right: 0%; margin-top: 3%; margin-bottom: 2%;" onclick="addFiles();" title="" aria-describedby="ui-tooltip-1"> Add Component </a>
						<div class="table-responsive scroll-table" style="overflow-x:auto;width:100%;">
							<table class="display table table-bordered non-bootstrap" id="testtable">
								<thead>
									<tr>
										<th style="width: 1%">Sequence</th>
										<th style="width: 20%">Fees Components</th>
										<th style="width: 5%">Component Type</th>
										<th style="width: 15%">Components Fees</th>
										<th style="width: 10%">Printed On Receipt</th>
										<th style="width: 1%">Is Discountable</th>
										<th style="width: 15%">GST</th>
										<th style="width: 17%">Select GST Rate</th>
										<!--<th>Start Date</th>
										<th>End Date</th>-->
										<th style="width: 3%">Action</th>
									</tr>
								</thead>
								<tbody id="tableBody"></tbody>
								<tfoot>
									<tr>
										<td colspan="3">Total</td>
										<td colspan="6" class="fees-total-cls">00.00</td>
									</tr>
								</tfoot>
							</table>					
						</div>
						<div style="clear:both; margin-bottom: 2%;"></div>
						<div class="form-actions form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
							<a href="<?php echo base_url();?>feesmaster" class="btn btn-primary">Cancel</a>
						</div>
					</form>
                </div>
            <div class="clearfix"></div>
            </div>
         </div>
    </div>        
	</div><!-- end: Content -->								
<script>
$(document).ready(function(){
	$("#fees_from_date").datepicker({
		// startDate: new Date(),
		autoclose: true,
		todayHighlight: true,
		format:"dd-mm-yyyy",
    }).on('changeDate', function (selected) {
        var startDate = new Date(selected.date.valueOf());
        $('#fees_to_date').datepicker('setStartDate', startDate);
    });

    $("#fees_to_date").datepicker({
		startDate: new Date(),
		autoclose: true,
		format:"dd-mm-yyyy",
	}).on('changeDate', function (selected) {
		var endDate = new Date(selected.date.valueOf());
		$('#fees_from_date').datepicker('setEndDate', endDate);
	});

	//refresh all selectpicker
	$(".selectpicker").selectpicker("refresh");
})


$(document).on("keyup",".fees-val-cls",function(){
	feesTotal();
})
function feesTotal(){
	var total = 00;
	$(".fees-val-cls").each(function(){
		if($(this).val() != "" && !isNaN($(this).val())){
			total = (total+parseFloat($(this).val()));
		}
	});
	if(isNaN(total)){
		total = 00;
	}
	$(".fees-total-cls").html(total.toFixed(2));
}
$( document ).ready(function() {
	$('#installment_div').hide();
	$('.group_div').hide();
	$("#checkbox").click(function(){
		if($("#checkbox").is(':checked') ){
			$("#fees_component_id > option").prop("selected","selected");
			$("#fees_component_id").trigger("change");
			calcamount();
		}else{
			$("#fees_component_id > option").removeAttr("selected");
			$("#fees_component_id").trigger("change");
		}
	});
	// getCourses($("#category_id").val())
	<?php 
		if(!empty($details[0]->fees_id)){
	?>
		// getCourses('<?php echo $details[0]->category_id; ?>', '<?php echo $details[0]->course_id; ?>');
		<?php 
		if($details[0]->fees_selection_type == 'Class'){ ?>
			$('.class_div').show();
			$('.group_div').hide();
		<?php
		}
		else{ ?>
			$('.class_div').hide();
			$('.group_div').show();
		<?php
		}?>
		editFiles()
	<?php } else{?>

		count = 1;
		addFiles()
	<?php }?>

	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
		toolbar_Full:
		[
				//['Source', 'Templates'],
			['Cut','Copy','Paste','PasteText','-','Print', 'SpellChecker', 'Scayt'],
			['Subscript','Superscript'],
			['NumberedList','BulletedList'],
			['BidiLtr', 'BidiRtl' ],
			//['Maximize', 'ShowBlocks'],
			['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
			['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
			//['SelectAll','RemoveFormat'],'/',
			['Styles','Format','Font','FontSize'],
			['TextColor','BGColor'],								
			//['Image','Flash','Table','HorizontalRule','Smiley'],
		],
		 width: "620px"
	};
	$('.editor').ckeditor(config);

	$(".datepicker").datetimepicker({
		format: 'DD-MM-YYYY'
	});


	// $('.fees_type').change(function(){
	// 	var fees_type = $(this).val();
	// 	if(fees_type == 'INSTALLMENT'){
	// 		$('#installment_div').show();
	// 	}
	// 	else{
	// 		$('#installment_div').hide();
	// 	}
	// })
	if($(".fees_selection_type:checked").val() == "Group"){
		if($(".allow_frequency:checked").val() == "Yes"){
			$('.frequency-wrapper').removeClass("display-none");
		}else{
			$('.frequency-wrapper').addClass("display-none");
		}
	}else{
		$('.frequency-wrapper').addClass("display-none");
	}
	$('.fees_selection_type').change(function(){
		var fees_selection_type = $(this).val();
		if(fees_selection_type == 'Class'){
			$('.class_div').show();
			$('.group_div').hide();
			$('.installment-wrapper').removeClass("display-none");
			$('.frequency-wrapper').addClass("display-none");
		}else{
			$('.group_div').show();
			$('.class_div').hide();
			if($(".allow_frequency:checked").val() == "Yes"){
				$('.installment-wrapper').addClass("display-none");
				$('.frequency-wrapper').removeClass("display-none");
			}else{
				$('.installment-wrapper').removeClass("display-none");
				$('.frequency-wrapper').addClass("display-none");
			}
		}
	})
	$(".allow_frequency").change(function(){
		if($(this).val() == "Yes"){
			$('.installment-wrapper').addClass("display-none");
			$('.frequency-wrapper').removeClass("display-none");
		}else{
			$('.installment-wrapper').removeClass("display-none");
			$('.frequency-wrapper').addClass("display-none");
		}
	})
});

<?php 
		if(empty($details[0]->fees_id)){
	?>
var count = 0;
<?php }else{?>
	var count = <?php echo sizeof($componentdetails)?>;
	if(count <= 1){
		count = 0
	}
<?php }?>
function addFiles()
{
	count++;
	var sequence_count = parseInt($(".sequence-cls:last").html());
	sequence_count++;
	if(isNaN(sequence_count)){
		sequence_count = 1;
	}
	var fileCount = count - 1;
	var text = '<tr id="divTD_'+count+'">'+
						'<td class="sequence-cls">'+sequence_count+'</td>'+
					'<td>'+
						'<select id="fees_component_id_'+count+'" name="fees_component_id['+count+']" class="required form-control fees_component_id" onchange="getComponentReceiptName('+count+')" title="Please select option."><option value="">Select Components</option>'
									<?php 
										if(isset($fees_components) && !empty($fees_components)){
											foreach($fees_components as $cdrow){
										?>
										+'<option value="<?php echo $cdrow->fees_component_master_id;?>"><?php echo $cdrow->fees_component_master_name;?></option>'
										<?php }}?>
								+'</select>'+
					'</td>'+
					'<td>'+
						// '<select name="component_type['+count+']" id="component_type_'+count+'" class="required form-control" title="Please select option.">'+
						// 	'<option value=""> Select Type</option>'+
						// 	'<option value="Course"> Course</option>'+
						// 	'<option value="One time"> One time</option>'+
						// 	'<option value="Admin"> Admin</option>'+
						// 	'<option value="Ad-hoc"> Ad-hoc</option>'+
						// '</select>'
						'<input type="text" name="component_type['+count+']" id="component_type_'+count+'" class="required form-control" value="" readonly="readonly">'+
					'</td>'+
					'<td >'+
						'<input type="number" name="component_fees['+count+']" id="component_fees_'+count+'" class="required form-control fees-val-cls" title="Enter Component Fees." min="0"/>'+
					'</td>'+
					'<td>'+
						'<input type="text" name="receipt_name['+count+']" id="receipt_name_'+count+'" class="required form-control" title="Enter Receipt Name ." readonly="readonly"/>'+
					'</td>'+
					'<td>'+
						'<select name="is_discountable['+count+']" id="is_discountable_'+count+'" class="required form-control" title="Please select option.">'+
							'<option value="Yes"> Yes</option>'+
							'<option value="No"> No</option>'+
						'</select>'+
					'</td>'+
					'<td>'+
						'<select name="is_gst['+count+']" id="is_gst_'+count+'" class="required form-control" title="Please select option." onchange="getGstData('+count+')">'+
							'<option value="Yes"> Yes</option>'+
							'<option value="No"> No</option>'+
						'</select>'+
					'</td>'+
					'<td>'+
						'<select id="gst_tax_'+count+'" name="gst_tax_id['+count+']" class="form-control gst_tax" title="Please select option."><option value="">Select GST Rate</option>'
									<?php 
										if(isset($gsttax) && !empty($gsttax)){
											foreach($gsttax as $cdrow){
										?>
										+'<option value="<?php echo $cdrow->gst_master_id;?>" class="gst_data_show_'+count+'"><?php echo $cdrow->tax;?></option>'
										<?php }}?>
								+'</select>'+
					'</td>'+
					/*'<td>'+
						'<input type="text" name="start_date['+count+']" id="start_date_'+count+'" class="required form-control datepicker" title="Please select start date." />'+
					'</td>'+
					'<td>'+
						'<input type="text" name="end_date['+count+']" id="end_date_'+count+'" class="required form-control datepicker" title="Please select end date." />'+
					'</td>'+*/
					'<td><a class="minia-icon-file-remove btn tip" title="Remove" onclick="removeFiles(\'divTD_'+count+'\',0);">Remove</a>'+
					'</td>'+
				'</tr>';	
	$("#tableBody").append(text);
	$(".datepicker").datetimepicker({
		format: 'DD-MM-YYYY'
	});
	feesTotal();
	// count++;
}



function editFiles()
{
	var fileCount = count - 1;
	<?php
		if(!empty($componentdetails)){
			for($i=0;$i<sizeof($componentdetails);$i++){
	?>
	var text = '<tr id="divTD_'+<?php echo $i+1;?>+'">'+
						'<td class="sequence-cls">'+<?php echo $i+1;?>+'</td>'+
					'<td>'+
						'<select id="fees_component_id_'+<?php echo $i+1;?>+'" name="fees_component_id['+<?php echo $i+1;?>+']" class="required form-control fees_component_id" onchange="getComponentReceiptName('+<?php echo $i+1;?>+')" title="Please select option."><option value="">Select Components</option>'
									<?php 
										if(isset($fees_components) && !empty($fees_components)){
											foreach($fees_components as $cdrow){
												$sel = ($cdrow->fees_component_master_id == $componentdetails[$i]->fees_component_id) ? 'selected="selected"' : '';
										?>
										+'<option value="<?php echo $cdrow->fees_component_master_id;?>" <?php echo $sel; ?>><?php echo $cdrow->fees_component_master_name;?></option>'
										<?php }}?>
								+'</select>'+
					'</td>'+
					'<td>'+
						// '<select name="component_type['+<?php echo $i+1;?>+']" id="component_type_'+<?php echo $i+1;?>+'" class="required form-control" title="Please select option.">'+
						// 	'<option value=""> Select Type</option>'+
						// 	'<option value="Course" <?php if(!empty($componentdetails[$i]->component_type) && $componentdetails[$i]->component_type == 'Course') echo 'selected'?>> Course</option>'+
						// 	'<option value="One time" <?php if(!empty($componentdetails[$i]->component_type) && $componentdetails[$i]->component_type == 'One time') echo 'selected'?>> One time</option>'+
						// 	'<option value="Admin" <?php if(!empty($componentdetails[$i]->component_type) && $componentdetails[$i]->component_type == 'Admin') echo 'selected'?>> Admin</option>'+
						// 	'<option value="Ad-hoc" <?php if(!empty($componentdetails[$i]->component_type) && $componentdetails[$i]->component_type == 'Ad-hoc') echo 'selected'?>> Ad-hoc</option>'+
						// '</select>'
						'<input type="text" name="component_type['+<?php echo $i+1;?>+']" id="component_type_'+<?php echo $i+1;?>+'" class="required form-control" value="<?php if($componentdetails[$i]->component_type) echo $componentdetails[$i]->component_type?>" readonly="readonly">'+
					'</td>'+
					'<td>'+
						'<input type="number" name="component_fees['+<?php echo $i+1;?>+']" id="component_fees_'+<?php echo $i+1;?>+'" class="required form-control fees-val-cls" title="Enter Component Fees." min="0" value="<?php if($componentdetails[$i]->component_fees) echo $componentdetails[$i]->component_fees?>"/>'+
					'</td>'+
					'<td>'+
						'<input type="text" name="receipt_name['+<?php echo $i+1;?>+']" id="receipt_name_'+<?php echo $i+1;?>+'" class="required form-control" title="Enter Receipt Name ." value="<?php if($componentdetails[$i]->receipt_name) echo $componentdetails[$i]->receipt_name?>" readonly="readonly"/>'+
					'</td>'+
					'<td>'+
						'<select name="is_discountable['+<?php echo $i+1;?>+']" id="is_discountable_'+<?php echo $i+1;?>+'" class="required form-control" title="Please select option.">'+
							'<option value="Yes" <?php if(!empty($componentdetails[$i]->is_discountable) && $componentdetails[$i]->is_discountable == 'Yes') echo 'selected'?>> Yes</option>'+
							'<option value="No" <?php if(!empty($componentdetails[$i]->is_discountable) && $componentdetails[$i]->is_discountable == 'No') echo 'selected'?>> No</option>'+
						'</select>'+
					'</td>'+
					'<td>'+
						'<select name="is_gst['+<?php echo $i+1;?>+']" id="is_gst_'+<?php echo $i+1;?>+'" class="required form-control" title="Please select option." onchange="getEditGstData('+<?php echo $i+1;?>+')">'+
							'<option value="Yes" <?php if(!empty($componentdetails[$i]->is_gst) && $componentdetails[$i]->is_gst == 'Yes') echo 'selected'?>> Yes</option>'+
							'<option value="No" <?php if(!empty($componentdetails[$i]->is_gst) && $componentdetails[$i]->is_gst == 'No') echo 'selected'?>> No</option>'+
						'</select>'+
					'</td>'+
					'<td>'+
						'<select id="gst_tax_'+<?php echo $i+1;?>+'" name="gst_tax_id['+<?php echo $i+1;?>+']" class="form-control gst_tax" title="Please select option."><option value="">Select GST Rate</option>'
									<?php 
										if(isset($gsttax) && !empty($gsttax)){
											foreach($gsttax as $cdrow){
												$sel = ($cdrow->gst_master_id == $componentdetails[$i]->gst_tax_id) ? 'selected="selected"' : '';
											if($componentdetails[$i]->is_gst == 'Yes'){
										?>
										+'<option value="<?php echo $cdrow->gst_master_id;?>" <?php echo $sel; ?> class="gst_data_show_'+<?php echo $i+1;?>+'"><?php echo $cdrow->tax;?></option>'
										<?php }}}?>
								+'</select>'+
					'</td>'+
					/*'<td>'+
						'<input type="text" name="start_date['+count+']" id="start_date_'+count+'" class="required form-control datepicker" title="Please select start date." />'+
					'</td>'+
					'<td>'+
						'<input type="text" name="end_date['+count+']" id="end_date_'+count+'" class="required form-control datepicker" title="Please select end date." />'+
					'</td>'+*/
					'<td><a class="minia-icon-file-remove btn tip" title="Remove" onclick="removeFiles(\'divTD_'+<?php echo $i+1;?>+'\',0);">Remove</a>'+
					'</td>'+
				'</tr>';	
	$("#tableBody").append(text);
		<?php
	} } ?>
	feesTotal();
	count++;
}

function getComponentReceiptName(record_id){
	var component_id = $("#fees_component_id_"+record_id).val()
	$.ajax({
		url:"<?php echo base_url();?>feesmaster/getComponentReceiptName",
		data:{component_id:component_id},
		dataType: 'json',
		method:'post',
		success: function(res)
		{
			if(res['status']=="success")
			{
				if(res['option'] != "")
				{
					$("#receipt_name_"+record_id).val(res['option'][0]['receipt_name']);
					$('#component_type_'+record_id).val(res['option'][0]['fees_type']);
				}
				else
				{
					$("#receipt_name_"+record_id).val('');
					$("#component_type_"+record_id).val('');
				}
			}
			else
			{	
				$("#receipt_name_"+record_id).val('');
				$("#component_type_"+record_id).val('');
			}
		}
	});
}

function getEditGstData(record_id){
	var gst_id = $("#is_gst_"+record_id).val()
	$.ajax({
		url:"<?php echo base_url();?>feesmaster/getGstRate",
		data:{gst_id:gst_id},
		dataType: 'json',
		method:'post',
		success: function(res)
		{
			if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#gst_tax_"+record_id).html("<option value='' disabled='disabled'>Select</option>"+res['option']);
					}
					else
					{
						$("#gst_tax_"+record_id).html("<option value='' disabled='disabled'>Select</option>");
					}
				}
				else
				{	
					$("#gst_tax_"+record_id).html("<option value='' disabled='disabled'>Select</option>");
				}
		}
	});
}

function getGstData(record_id){
	var gst_id = $("#is_gst_"+record_id).val()
	if(gst_id == 'Yes'){
		$('.gst_data_show_'+record_id).show()
	}
	else{
		$('.gst_data_show_'+record_id).hide()
	}
}


function removeFiles(divId,record_id)
{
    //alert(record_id);
	//return false;
	var r=confirm("Are you sure you want to delete this record?");
	if (r==true)
   	{
	
		if(record_id == '0'){
			//alert("in");
			$("#"+divId).remove();
			
		}
		var sequence = 1;
		$(".sequence-cls").each(function(){
			$(this).html(sequence++);
		})
		feesTotal()
	}
}

var vRules = {
	academic_year_id:{required:true},
	category_id:{required:true},
	// "course_id[]":{required:true},
	// "group_id[]":{required:true},
	fees_level_id:{required:true},
	fees_type:{required:true},
	center_id:{required:true},
	fees_name:{required:true},
	fees_from_date:{required:true},
	fees_to_date:{required:true},
	installment_no:{required:true, integer:true}
	
};
var vMessages = {
	academic_year_id:{required:"Please select academic year."},
	category_id:{required:"Please select category."},
	// "course_id[]":{required:"Please select course."},
	// "group_id[]":{required:"Please select group."},
	fees_level_id:{required:"Please select fees level."},
	fees_type:{required:"Please select fees type."},
	center_id:{required:"Please select center."},
	fees_name:{required:"Please enter fees name."},
	fees_from_date:{required:"Please select the fees start date"},
	fees_to_date:{required:"Please select the fees end date"},
	installment_no:{required:"Please enter installment no."}
	
};
$("#form-validate").validate({
	// ignore:"",
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>feesmaster/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>feesmaster";
					},2000);

				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			},
			error:function(){
				$("#msg_display").html("");
				$("#display_msg").html("");
				displayMsg("error",res['msg']);
			}
		});
	}
});


/* getCourses($("#category_id").val());
function getCourses(category_id,course_id = null)
{
	
	if(category_id != "" )
	{
		$.ajax({
			url:"<?php //echo base_url();?>feesmaster/getCourses",
			data:{category_id:category_id, course_id:course_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#course_id").html(res['option']);
						if($("#course_id").hasClass("selectpicker")){
							$('#course_id').selectpicker('refresh');
						}
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
						if($("#course_id").hasClass("selectpicker")){
							$('#course_id').selectpicker('refresh');
						}
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
					if($("#course_id").hasClass("selectpicker")){
						$('#course_id').selectpicker('refresh');
					}
				}
			}
		});
	}
} */

function calcamount(){
	var components_id_array = $('#fees_component_id').val();
	if(components_id_array != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>feesmaster/getCalcamount",
			data:{components_id:components_id_array},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['sum'] != "")
					{
						$("#amount").val(res['sum']);
					}
				}
			}
		});
	}
	if(components_id_array == null ){
		$("#amount").val(0);
	}
}




document.title = "AddEdit - Fees";

 
</script>					
<style>
.datepicker table tr td.today {
	color: #ffffff;
    background-color: #3c8dbc;
    border-color: #3c8dbc;
}
.course-wrapper{
	border:1px dotted #cdcdcd;
	float:left;
	padding:10px;
	width:101%;
}
.width100{
	width:100%;
}
.display-none{
	display:none;
}
</style>