<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Assignnewsletter extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('assignnewslettermodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('assignnewsletter/index');
			$this->load->view('template/footer.php');
		}
		else {
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = [];
			$result['academic_year'] = $this->assignnewslettermodel->getDropdown("tbl_academic_year_master","academic_year_master_id,academic_year_master_name");
			$result['categories'] = $this->assignnewslettermodel->getDropdown("tbl_categories","category_id,categoy_name");
			$result['groups'] = $this->assignnewslettermodel->getDropdown("tbl_group_master","group_master_id,group_master_name");
			$result['newsletters'] = $this->assignnewslettermodel->getDropdown("tbl_newsletters","newsletter_id,newsletter_title");
			$result['zones'] = $this->assignnewslettermodel->getDropdown("tbl_zones","zone_id,zone_name");
			$result['details'] = $this->assignnewslettermodel->getFormdata($record_id);

			$this->load->view('template/header.php');
			$this->load->view('assignnewsletter/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {
			redirect('login', 'refresh');
		}
	}
	
	public function getCourses(){
		$result = $this->assignnewslettermodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['course_id']) && !empty($_REQUEST['course_id'])){
			$course_id = $_REQUEST['course_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getFees(){
		$result = $this->assignnewslettermodel->getOptions("tbl_fees_master",$_REQUEST['fees_level_id'],"fees_level_id");
		// echo "<pre>";
		// print_r($result);
		// exit;
		
		$option = '';
		$fees_id = '';
		
		if(isset($_REQUEST['fees_id']) && !empty($_REQUEST['fees_id'])){
			$fees_id = $_REQUEST['fees_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->fees_id == $fees_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->fees_id.'" '.$sel.' >'.$result[$i]->fees_name.' ['.$result[$i]->amount.']</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getCenters(){
		$result = $this->assignnewslettermodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		//echo "<pre>";
		//print_r($result);
		//exit;
		
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->center_id == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function submitForm()
	{ 
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
				if(!isset($_POST['center_id'])){
					echo json_encode(array("success"=>"0",'msg'=>'Please select center!'));
					exit;
				}
				
				if(empty($_POST['assign_newsletter_id'])){
					if(!empty($_POST['center_id'])){
						for($i=0; $i < sizeof($_POST['center_id']); $i++){
							$condition = "academic_year_id='".$_POST['academic_year_id']."'  && zone_id= '".$_POST['zone_id']."' && newsletter_id='".$_POST['newsletter_id']."' && center_id='".$_POST['center_id'][$i]."' ";
							
							if($_POST['selection_type'] == 'Class'){
								$condition .=" && category_id='".$_POST['category_id']."' && course_id='".$_POST['course_id']."'  ";
							}else{
								$condition .=" && group_id='".$_POST['group_id']."' ";
							}
							
							if(isset($_POST['assign_newsletter_id']) && $_POST['assign_newsletter_id'] > 0){
								$condition .= " &&  assign_newsletter_id != ".$_POST['assign_newsletter_id'];
							}
							
							$check_name = $this->assignnewslettermodel->getdata("tbl_assign_newsletters",$condition);
							
							if(!empty($check_name)){
								$getCenterName = $this->assignnewslettermodel->getdata("tbl_centers", "center_id='".$_POST['center_id'][$i]."' ");
								echo json_encode(array("success"=>"0",'msg'=>' '.$getCenterName[0]['center_name'].' Already assign to selected center!'));
								exit;
							}
							
						}
					}
				}
				
			if (!empty($_POST['assign_newsletter_id'])) {
				
				$condition = "academic_year_id='".$_POST['academic_year_id']."' &&  newsletter_id='".$_POST['newsletter_id']."' && zone_id= '".$_POST['zone_id']."' && center_id='".$_POST['center_id']."' ";
				
				if($_POST['selection_type'] == 'Class'){
					$condition .=" && category_id='".$_POST['category_id']."' && course_id='".$_POST['course_id']."'  ";
				}else{
					$condition .=" && group_id='".$_POST['group_id']."' ";
				}
							
				if(isset($_POST['assign_newsletter_id']) && $_POST['assign_newsletter_id'] > 0){
					$condition .= " &&  assign_newsletter_id != ".$_POST['assign_newsletter_id'];
				}
				
				$check_name = $this->assignnewslettermodel->getdata("tbl_assign_newsletters",$condition);
				
				if(!empty($check_name)){
					$getCenterName = $this->assignnewslettermodel->getdata("tbl_centers", "center_id='".$_POST['center_id']."' ");
					echo json_encode(array("success"=>"0",'msg'=>' '.$getCenterName[0]['center_name'].' Already assign to selected center!'));
					exit;
				}
				
				
				
				$data_array = array();			
				$assign_newsletter_id = $_POST['assign_newsletter_id'];
				$data_array['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
				$data_array['selection_type'] = (!empty($_POST['selection_type'])) ? $_POST['selection_type'] : '';
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0;
				$data_array['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : 0;
				$data_array['group_id'] = (!empty($_POST['group_id'])) ? $_POST['group_id'] : 0;
				$data_array['newsletter_id'] = (!empty($_POST['newsletter_id'])) ? $_POST['newsletter_id'] : '';
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->assignnewslettermodel->updateRecord('tbl_assign_newsletters', $data_array,'assign_newsletter_id',$assign_newsletter_id);
				
				
			}else {
				
				$data_array = array();
				$data_array['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
				$data_array['selection_type'] = (!empty($_POST['selection_type'])) ? $_POST['selection_type'] : '';
				$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0;
				$data_array['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : 0;
				$data_array['group_id'] = (!empty($_POST['group_id'])) ? $_POST['group_id'] : 0;
				$data_array['newsletter_id'] = (!empty($_POST['newsletter_id'])) ? $_POST['newsletter_id'] : '';
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				
				
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				//get news letter details
				$newsletterDetails = $this->assignnewslettermodel->getdata('tbl_newsletters', "newsletter_id='".$_POST['newsletter_id']."' ");
				
				
				//echo "<pre>";print_r($data_array);exit;
				
				if(!empty($_POST['center_id'])){
					for($i=0; $i < sizeof($_POST['center_id']); $i++){
						$data_array['center_id'] = (!empty($_POST['center_id'][$i])) ? $_POST['center_id'][$i] : '';
						$result = $this->assignnewslettermodel->insertData('tbl_assign_newsletters', $data_array, '1');
						if(!empty($_POST['selection_type']) && $_POST['selection_type'] == 'Class'){
							$condition ="  d.academic_year_id='".$_POST['academic_year_id']."' && d.category_id='".$_POST['category_id']."' && d.course_id='".$_POST['course_id']."' && s.zone_id='".$_POST['zone_id']."' && s.center_id='".$_POST['center_id'][$i]."' && s.status='Active' ";
						}else{
							$condition ="  d.academic_year_id='".$_POST['academic_year_id']."' && d.group_id='".$_POST['group_id']."' && d.zone_id='".$_POST['zone_id']."' && d.center_id='".$_POST['center_id'][$i]."' && s.status='Active' ";
						}
						
						//get center students
						$getStudentList = $this->assignnewslettermodel->getStudentList($condition);
						
						if(!empty($getStudentList)){
							$email_data = $this->assignnewslettermodel->get_email_data(1);
							for($s=0; $s < sizeof($getStudentList); $s++){
								$title = '';
								$notification = '';
								$email_content=  '';
								$fcm_token = '';
								$subject='';
								$from_email='';
								$to_email='';
								$cc_email = '';
								$notification_type = array();
								
								if(!empty($getStudentList[$s]['fcm_token'])){
									//echo "h1";
									$fcm_token = $getStudentList[$s]['fcm_token'];
									if(!empty($fcm_token)){
										if(is_array($email_data) ){
											/*$title = "Newsletter - ".$newsletterDetails[0]['newsletter_title']." added.";
											$notification = $email_data[0]['fcm_notification'];
											*/
											
											$title = "Newsletter";
											$notification = " ".$newsletterDetails[0]['newsletter_title']." is added.";
											
											$notification_type = array('type'=>'service_request');
										}
										
										//echo "title: ".$title." notification: ".$notification." fcm_token: ".$fcm_token;exit;
										sendNotification($title,$notification,$email_content,$fcm_token,$subject,$from_email,$to_email,$cc_email,$notification_type);
									}

									$notification_data = array();
									$notification_data['student_id'] = $getStudentList[$s]['student_id'];
									$notification_data['notification_type'] = 'Newsletter';
									$notification_data['notification_title'] = $title;
									$notification_data['notification_contents'] = $notification;
									
									$notification_data['created_on'] = date("Y-m-d H:i:s");
									$notification_data['created_by'] = $_SESSION["webadmin"][0]->user_id;
									$notification_data['updated_on'] = date("Y-m-d H:i:s");
									$notification_data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
									$this->assignnewslettermodel->insertData('tbl_student_notifications', $notification_data, 1);
									
								}
								
							}
						}
						
						
					}
				}
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		$get_result = $this->assignnewslettermodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->academic_year_master_name);
				array_push($temp, $get_result['query_result'][$i]->selection_type);
				array_push($temp, $get_result['query_result'][$i]->newsletter_title);
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("AssignNewsletterAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->assign_newsletter_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("AssignNewsletterAddEdit")){
					$actionCol1.= '<a href="assignnewsletter/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->assign_newsletter_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->assignnewslettermodel->delrecord12("tbl_assign_newsletters","assign_newsletter_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	
}

?>
