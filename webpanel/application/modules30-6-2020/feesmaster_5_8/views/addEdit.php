<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Fees Master</h1>            
          </div>
          <div>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
              <li><a href="<?php echo base_url();?>feesmaster">Fees Master</a></li>
            </ul>
          </div>
    </div>
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
						<input type="hidden" id="fees_id" name="fees_id" value="<?php if(!empty($details[0]->fees_id)){echo $details[0]->fees_id;}?>" />
						
						<div class="control-group form-group">
							<label class="control-label">Academic Year*</label>
							<div class="controls">
								<select id="academic_year_id" name="academic_year_id" class="form-control">
									<option value="">Select Academic Year</option>
									<?php 
										if(isset($academic_year) && !empty($academic_year)){
											foreach($academic_year as $cdrow){
												$sel = ($cdrow->academic_year_master_id == $details[0]->academic_year_id) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $cdrow->academic_year_master_id;?>" <?php echo $sel; ?>><?php echo $cdrow->academic_year_master_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>

						<div class="control-group form-group">
							<label class="control-label"><span>Fees Selection Type*</span></label>
							<div class="controls">
								<input type="radio" class="form-control fees_selection_type" name="fees_selection_type" value="Class" <?php echo (!empty($details[0]->fees_selection_type) &&  ($details[0]->fees_selection_type== 'Class')) ? "checked" : ""?> checked="checked">Class
								<input type="radio" class="form-control fees_selection_type" name="fees_selection_type" value="Group" <?php echo (!empty($details[0]->fees_selection_type) &&  ($details[0]->fees_selection_type== 'Group')) ? "checked" : ""?> >Group
							</div>
						</div>

						<div class="control-group form-group class_div">
							<label class="control-label" for="category_id">Category*</label>
							<div class="controls">
								<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);">
									<option value="">Select Category</option>
									<?php 
										if(isset($categories) && !empty($categories)){
											foreach($categories as $cdrow){
												$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>

						<div class="control-group form-group class_div">
							<label class="control-label"><span>Course*</span></label> 
							<div class="controls">
								<select id="course_id" name="course_id" class="form-control">
									<option value="">Select Course</option>
								</select>
							</div>
						</div>

						<div class="control-group form-group group_div">
							<label class="control-label" for="category_id">Group*</label>
							<div class="controls">
								<select id="group_id" name="group_id" class="form-control"  onchange="getCourses(this.value);">
									<option value="">Select Group</option>
									<?php 
										if(isset($groups) && !empty($groups)){
											foreach($groups as $cdrow){
												$sel = ($cdrow->group_master_id == $details[0]->group_id) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $cdrow->group_master_id;?>" <?php echo $sel; ?>><?php echo $cdrow->group_master_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>

						<div class="control-group form-group">
							<label class="control-label"><span>Components*</span></label> 
							<input type="checkbox" id="checkbox" >Select All
							<div class="controls">
								<select id="fees_component_id" name="fees_component_id[]" class="form-control select2 fees_component_id" multiple onchange="calcamount();">
									<option value="">Select Components</option>
									<?php 
									if(isset($fees_components) && !empty($fees_components)){
										foreach($fees_components as $cdrow){
											$sel = (in_array($cdrow->fees_component_master_id, $c_id_array)) ? 'selected="selected"' : '';
									?>
									<option value="<?php echo $cdrow->fees_component_master_id;?>" <?php echo $sel; ?>><?php echo $cdrow->fees_component_master_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>

						<!-- <?php if(!empty($details[0]->fees_id)){?>
						<div class="control-group form-group">
							<label class="control-label"><span>Components*</span></label> 
							<div class="controls">
								<select id="fees_component_id" name="fees_component_id[]" class="form-control fees_component_id">
									<option value="">Select Components</option>
									<?php 
										if(isset($fees_components) && !empty($fees_components)){
											foreach($fees_components as $cdrow){
												$sel = (in_array($cdrow->fees_component_master_id, $c_id_array)) ? 'selected="selected"' : '';
										?>
										<option value="<?php echo $cdrow->fees_component_master_id;?>" <?php echo $sel; ?>><?php echo $cdrow->fees_component_master_name;?></option>
										<?php }}?>
								</select>
							</div>
						</div>
						<?php }else{?>
							
						<div class="control-group form-group">
							<label class="control-label"><span>Components*</span></label> 
							<input type="checkbox" id="checkbox" >Select All
							<div class="controls">
								<select id="fees_component_id" name="fees_component_id[]" class="form-control select2 fees_component_id" multiple onchange="calcamount();">
									<option value="">Select Components</option>
									<?php 
									if(isset($fees_components) && !empty($fees_components)){
										foreach($fees_components as $cdrow){
											$sel = (in_array($cdrow->fees_component_master_id, $c_id_array)) ? 'selected="selected"' : '';
									?>
									<option value="<?php echo $cdrow->fees_component_master_id;?>" <?php echo $sel; ?>><?php echo $cdrow->fees_component_master_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						 <?php }?> -->

						<div class="control-group form-group">
							<label class="control-label">Fees Level*</label>
							<div class="controls">
								<select id="fees_level_id" name="fees_level_id" class="form-control">
									<option value="">Select Fees Level</option>
									<?php 
										if(isset($fees_level) && !empty($fees_level)){
											foreach($fees_level as $cdrow){
												$sel = ($cdrow->fees_level_id == $details[0]->fees_level_id) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $cdrow->fees_level_id;?>" <?php echo $sel; ?>><?php echo $cdrow->fees_level_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						
						<!-- <div class="control-group form-group">
							<label class="control-label"><span>Fees Type*</span></label>
							<div class="controls">
								<input type="radio" class="form-control fees_type" name="fees_type" value="LUMSPUM" <?php echo (!empty($details[0]->fees_type) &&  ($details[0]->fees_type== 'LUMSPUM')) ? "checked" : ""?> checked="checked">LUMSPUM
								<input type="radio" class="form-control fees_type" name="fees_type" value="INSTALLMENT" <?php echo (!empty($details[0]->fees_type) &&  ($details[0]->fees_type== 'INSTALLMENT')) ? "checked" : ""?> >INSTALLMENT
							</div>
						</div> -->

						<div class="control-group form-group">
							<label class="control-label"><span>Fees Name*</span></label>
							<div class="controls">
								<input type="text" class="form-control required" placeholder="Enter Name" id="fees_name" name="fees_name" value="<?php if(!empty($details[0]->fees_name)){echo $details[0]->fees_name;}?>" maxlength="150" >
							</div>
						</div>

						<div class="control-group form-group">
							<label class="control-label"><span>Amount*</span></label>
							<div class="controls">
								<input type="text" class="form-control required" placeholder="Enter Amount" id="amount" name="amount" value="<?php if(!empty($details[0]->amount)){echo $details[0]->amount;}?>" readonly>
							</div>
						</div>

						<div class="control-group form-group">
							<label class="control-label"><span>Description</span></label>
							<br/>(Editor is for text contens only kindly don't put any image)
							<div class="controls">
								<textarea name="description" id="description" placeholder="Enter Description" class="form-control editor"><?php if(!empty($details[0]->description)){echo $details[0]->description;}?></textarea>
							</div>
						</div>

						<div class="control-group form-group">
							<label class="control-label"><span>Receipt Print Name*</span></label>
							<div class="controls">
								<input type="text" class="form-control required" placeholder="Enter Amount" id="receipt_print_name" name="receipt_print_name" value="<?php if(!empty($details[0]->receipt_print_name)){echo $details[0]->receipt_print_name;}?>" maxlength="100">
							</div>
						</div>

						<div class="control-group form-group" id="installment_div">
							<label class="control-label"><span>Installment No*</span></label>
							<div class="controls">
								<input type="text" class="form-control required" placeholder="Enter Installment No" id="installment_no" name="installment_no" value="<?php if(!empty($details[0]->installment_no)){echo $details[0]->installment_no;}?>" maxlength="100">
							</div>
						</div>

						<div class="control-group form-group">
							<label class="control-label"><span>Start Date*</span></label>
							<div class="controls">
								<input type="text" class="form-control required datepicker" placeholder="Select start date" id="start_date" name="start_date" value="<?php if(!empty($details[0]->start_date)){echo date("d-m-Y", strtotime($details[0]->start_date));}?>" >
							</div>
						</div>
						
						<div class="control-group form-group">
							<label class="control-label"><span>End Date*</span></label>
							<div class="controls">
								<input type="text" class="form-control required datepicker" placeholder="Select end date" id="end_date" name="end_date" value="<?php if(!empty($details[0]->end_date)){echo date("d-m-Y", strtotime($details[0]->end_date));}?>" >
							</div>
						</div>
						<div style="clear:both; margin-bottom: 2%;"></div>
						
						<div class="form-actions form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
							<a href="<?php echo base_url();?>feesmaster" class="btn btn-primary">Cancel</a>
						</div>
					</form>
                </div>
            <div class="clearfix"></div>
            </div>
         </div>
    </div>        
	</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	$('#installment_div').hide();
	$('.group_div').hide();
	$("#checkbox").click(function(){
		if($("#checkbox").is(':checked') ){
			$("#fees_component_id > option").prop("selected","selected");
			$("#fees_component_id").trigger("change");
			calcamount();
		}else{
			$("#fees_component_id > option").removeAttr("selected");
			$("#fees_component_id").trigger("change");
		}
	});
	<?php 
		if(!empty($details[0]->fees_id)){
	?>
		getCourses('<?php echo $details[0]->category_id; ?>', '<?php echo $details[0]->course_id; ?>');
		<?php 
		if($details[0]->fees_type == 'INSTALLMENT'){ ?>
			$('#installment_div').show();
		<?php
		}
		else{ ?>
			$('#installment_div').hide();
		<?php
		}
		if($details[0]->fees_selection_type == 'Class'){ ?>
			$('.class_div').show();
			$('.group_div').hide();
		<?php
		}
		else{ ?>
			$('.class_div').hide();
			$('.group_div').show();
		<?php
		}
	} ?>

	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
		toolbar_Full:
		[
				//['Source', 'Templates'],
			['Cut','Copy','Paste','PasteText','-','Print', 'SpellChecker', 'Scayt'],
			['Subscript','Superscript'],
			['NumberedList','BulletedList'],
			['BidiLtr', 'BidiRtl' ],
			//['Maximize', 'ShowBlocks'],
			['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
			['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
			//['SelectAll','RemoveFormat'],'/',
			['Styles','Format','Font','FontSize'],
			['TextColor','BGColor'],								
			//['Image','Flash','Table','HorizontalRule','Smiley'],
		],
		 width: "620px"
	};
	$('.editor').ckeditor(config);

	$(".datepicker").datetimepicker({
		format: 'DD-MM-YYYY'
	});


	$('.fees_type').change(function(){
		var fees_type = $(this).val();
		if(fees_type == 'INSTALLMENT'){
			$('#installment_div').show();
		}
		else{
			$('#installment_div').hide();
		}
	})
	$('.fees_selection_type').change(function(){
		var fees_selection_type = $(this).val();
		if(fees_selection_type == 'Class'){
			$('.class_div').show();
			$('.group_div').hide();
		}
		else{
			$('.class_div').hide();
			$('.group_div').show();
		}
	})
});


var vRules = {
	academic_year_id:{required:true},
	category_id:{required:true},
	course_id:{required:true},
	fees_level_id:{required:true},
	center_id:{required:true},
	fees_name:{required:true, alphanumericwithspace:true},
	receipt_print_name:{required:true, alphanumericwithspace:true},
	installment_no:{required:true, integer:true},
	start_date:{required:true},
	end_date:{required:true}
	
};
var vMessages = {
	academic_year_id:{required:"Please select academic year."},
	category_id:{required:"Please select category."},
	course_id:{required:"Please select course."},
	fees_level_id:{required:"Please select fees level."},
	center_id:{required:"Please select center."},
	fees_name:{required:"Please enter fees name."},
	receipt_print_name:{required:"Please enter receipt print name."},
	installment_no:{required:"Please enter installment no."},
	start_date:{required:"Please select start date."},
	end_date:{required:"Please select end date."}
	
};

function getCourses(category_id,course_id = null)
{
	//alert("Val: "+val);return false;
	if(category_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>feesmaster/getCourses",
			data:{category_id:category_id, course_id:course_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#course_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function calcamount(){
	var components_id_array = $('#fees_component_id').val();
	if(components_id_array != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>feesmaster/getCalcamount",
			data:{components_id:components_id_array},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['sum'] != "")
					{
						$("#amount").val(res['sum']);
					}
				}
			}
		});
	}
	if(components_id_array == null ){
		$("#amount").val(0);
	}
}

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>feesmaster/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>feesmaster";
					},2000);

				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			},
			error:function(){
				$("#msg_display").html("");
				$("#display_msg").html("");
				displayMsg("error",res['msg']);
			}
		});
	}
});


document.title = "AddEdit - Fees";

 
</script>					
