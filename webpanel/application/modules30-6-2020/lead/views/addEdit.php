

<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Lead Details</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>centermaster">Lead Details</a></li>
                    </ul>
                  </div>
                </div>

                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-12 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								 <input type="hidden" id="lead_id" name="lead_id" value="<?php if(!empty($details[0]->lead_id)){echo $details[0]->lead_id;}?>" />
								 <input type="hidden" id="rectype" name="rectype" value="Lead" />

                                <!-- <h3><center>CHILD INFORMATION :</center></h3> -->
								<a href="javascript:void(0)" class="btn btn-primary">CHILD INFORMATION </a><hr>
								<div class="row">
										<div class="col-sm-6 control-group form-group">
                                            <label class="control-label" for="center_id">Select Zone*</label>
                                            <div class="controls">
                                            <select id="zone_id" name="zone_id" class="form-control selectpicker required show-tick "  onchange="getCenters(this.value)" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select zone...">
                                                    <option value="">Select Zone</option>
                                                    <?php 
                                                        if(isset($zones) && !empty($zones)){
                                                            foreach($zones as $cdrow){
                                                                $sel='';
                                                                if(!empty($details[0]->zone_id)){
                                                                    $sel = ($cdrow['zone_id'] == $details[0]->zone_id)? 'selected="selected"' : '';?>
																<?php }
																// else{
                                                                //     $sel = ($cdrow['zone_name']== 'India')?'selected="selected"':''?>
                                                                 <?php //}?>
                                                        <option value="<?php echo $cdrow['zone_id'];?>" <?php echo $sel; ?>><?php echo $cdrow['zone_name'];?></option>
                                                    <?php }}?>
                                                </select>
                                            </div>
                                        </div>

										<div class="col-sm-6 control-group form-group" style="padding-left: 23px;">
											<label class="control-label" for="lead_sub_status">Select Center*</label>
											<div class="controls">
												<select id="center_id" name="center_id" class="form-control show-tick required"  data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select center ..." >
													<option value="">Select Center</option>
												</select>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>First Name*</span></label>
										<div class="controls">
											<input type="text" class="form-control required " placeholder="Enter first name" name="student_first_name" value="<?php if(!empty($details[0]->student_first_name)){echo $details[0]->student_first_name;}?>"  maxlength="50">
										</div>
									</div>

									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>Last Name*</span></label>
										<div class="controls">
											<input type="text" class="form-control required " placeholder="Enter last name" name="student_last_name" value="<?php if(!empty($details[0]->student_last_name)){echo $details[0]->student_last_name;}?>"  maxlength="50">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-sm-6 control-group form-group" style="margin-top: 10px;" >
										<label class="control-label" style="margin-left: 5px;"><span>Gender*</span></label>
										<div class="controls">
										<input type="radio"    name="gender" class="required "  value="Male" <?= (!empty($details[0]->gender) && $details[0]->gender == 'Male'?'checked':'')?> ><label>Male</label>
										<input type="radio"   name="gender" class="required "  value="Female" <?= (!empty($details[0]->gender) && $details[0]->gender == 'Female'?'checked':'')?>
										><label>Female</label>
										</div>
									</div>

									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>Date of birth*</span></label>
										<div class="controls">
											<input type="text" class="form-control required " placeholder="choose date of birth"  id="student_dob" name="student_dob" value="<?php if(!empty($details[0]->student_dob)){echo date('d-m-Y', strtotime($details[0]->student_dob));}?>"  maxlength="10" minlength="10" readonly>
										</div>
									</div>
								</div>

								<!-- <div class="row">
									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>Email Id*</span></label>
										<div class="controls">
											<input type="email" class="form-control required " placeholder="Enter Email id" id="email_id" name="email_id" value="<?php if(!empty($details[0]->email_id)){echo $details[0]->email_id;}?>"  maxlength="50">
										</div>
									</div>
                                </div> -->

                                     <!-- <h3><center>FAMILY INFORMATION:</center></h3> -->
									 <a href="javascript:void(0)" class="btn btn-primary">FAMILY INFORMATION </a><hr>
                                    <div class="row">
                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>Father Name*</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required " placeholder="Enter Father  name" name="father_name" value="<?php if(!empty($details[0]->father_name)){echo $details[0]->father_name;}?>"  maxlength="50">
                                            </div>
                                        </div>

                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>Father Contant Number*</span></label>
                                            <div class="controls">
                                                <input type="text" class="form-control required " placeholder="Enter Father contact number" name="father_contact_no" value="<?php if(!empty($details[0]->father_contant_number)){echo $details[0]->father_contant_number;}?>"  maxlength="10" minlength="10" >
                                            </div>
                                        </div>

                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label"><span>Father's Email Id*</span></label>
                                            <div class="controls">
                                                <input type="email" class="form-control required " placeholder="Enter Father  name" name="father_emailid" value="<?php if(!empty($details[0]->father_emailid)){echo $details[0]->father_emailid;}?>"  maxlength="50">
                                            </div>
                                        </div>
                                        
                                        <div class="col-sm-6 control-group form-group">
                                            <label class="control-label" for="center_id">Country*</label>
                                            <div class="controls">
                                            <select id="country_id" name="country_id" class="form-control selectpicker required show-tick "  onchange="getstate(this.value)" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select Country...">
                                                    <option value="">Select Country</option>
                                                    <?php 
                                                        if(isset($countries) && !empty($countries)){
                                                            foreach($countries as $cdrow){
                                                                $sel='';
                                                                if(!empty($details[0]->country_id)){
                                                                    $sel = ($cdrow['country_id'] == $details[0]->country_id)? 'selected="selected"' : '';?>
																<?php }
																// else{
                                                                //     $sel = ($cdrow['country_name']== 'India')?'selected="selected"':''?>
                                                                 <?php //}?>
                                                        <option value="<?php echo $cdrow['country_id'];?>" <?php echo $sel; ?>><?php echo $cdrow['country_name'];?></option>
                                                    <?php }}?>
                                                </select>
                                            </div>
                                        </div>
                                     </div>   
                                     
								

								<div class="row">
									<div class="col-sm-6 control-group form-group">
										<label class="control-label" for="center_id">state*</label>
										<div class="controls">
										<select id="state_id" name="state_id" class="form-control show-tick required" onchange="getcity(this.value)" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select State..." >
												<option value="">Select state</option>
												
											</select>
										</div>
									</div>

									<div class="col-sm-6 control-group form-group">
										<label class="control-label" for="center_id">City*</label>
										<div class="controls">
										<select id="city_id" name="city_id" class="form-control show-tick required" data-style="btn-primary" data-live-search="true" data-live-search-placeholder="Select City..."  >
												<option value="">Select City</option>
											</select>
										</div>
									</div>
								</div>							
                                
								<div class="row">
									<div class="col-sm-6 control-group form-group">
										<label class="control-label"><span>Pincode*</span></label>
										<div class="controls">
											<input type="text" class="form-control required " placeholder="Enter pincode" id="student_pincode" name="student_pincode" value="<?php if(!empty($details[0]->student_pincode)){echo $details[0]->student_pincode;}?>"  maxlength="6" minlength="6">
										</div>
									</div>

                                    <div class="col-sm-6 control-group form-group">
										<?php $leadstatus = array('Hot','Worm','Cold');?>
										<label class="control-label" for="leadstatus">Lead Status*</label>
										<div class="controls">
										<select id="leadstatus" name="leadstatus" class="form-control show-tick required"  data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Choose  ..." >
												<option value="">select Lead Status</option>
													<?php 
														$stored_leadstatus = explode(",",$details[0]->know_about_us);
														foreach ($leadstatus as $key => $value) {
															$sel='';
															$sel= (in_array($value,$stored_leadstatus)?'selected':'');
														?>
														<option value="<?=$value?>" <?= $sel?> ><?=$value?></option>
													<?php } ?>		
											</select>
										</div>
									</div>
								</div>	
                                
								<div class="row">
                                <div class="col-sm-6 control-group form-group">
										<?php $leadprogress = array('YTC - Yet to Call','To Call Back & follow up','Walked-in for Enquiry','Interested/will Walk-in','Interested/Expected to Enroll','Will enroll later -Child is below age Criteria','Not Interested Now/To Follow up Later','Lost to Competition','Enrolled in a High School','Wrong Contact details');?>
										<label class="control-label" for="leadprogress">Lead Progress*</label>
										<div class="controls">
										<select id="leadprogress" name="leadprogress" class="form-control show-tick required"  data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Choose Lead Progress ..." >
														<option value="">select Lead Progress</option>
													<?php 
														$stored_leadprogress = explode(",",$details[0]->know_about_us);
														foreach ($leadprogress as $key => $value) {
															$sel='';
															$sel= (in_array($value,$stored_leadprogress)?'selected':'');
														?>
														<option value="<?=$value?>" <?= $sel?> ><?=$value?></option>
													<?php } ?>		
											</select>
										</div>
									</div>	

									<div class="col-sm-6 control-group form-group" style="padding-left: 23px;">
										<label class="control-label" for="lead_enquiry_status">Remark (For Office Use)*</label>
										<div class="controls">
										<textarea name="inquiry_remark" id="inquiry_remark" cols="62" rows="2"></textarea>
										</div>
									</div>
								</div>						
								
								<div class="row">
									<div class="col-sm-12 col-md-12 form-actions form-group">
										<button type="submit" class="btn btn-primary">Submit</button>
										<a href="<?php echo base_url();?>lead" class="btn btn-primary">Cancel</a>
									</div>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

$( function() {
    $( "#student_dob" ).datepicker();
  } );

$('document').ready(function(){
	// $(".selectpicker").selectpicker("refresh");
	$(".number_only").keypress(function (e) {
    	if (e.which != 8 && e.which != 0 && e.which != 46 &&(e.which < 48|| e.which > 57)) {
      	return false;
    	}
  	});

      <?php if(!empty($details) && isset($details)){?>
        getstate(<?= $details[0]->country_id?>,<?= $details[0]->state_id?>);
        getcity(<?= $details[0]->state_id?>,<?= $details[0]->city_id?>);
		getSubStatus(<?= $details[0]->lead_status_id?>,<?= $details[0]->lead_sub_status_id?>);
		getLeadEnquiyStage(<?= $details[0]->lead_status_id?>,<?= $details[0]->lead_sub_status_id?>,<?= $details[0]->lead_enquiry_status_id?>);
		<?php }else{?>
			/* this is for 1st time when we want country should be india */
			getstate($('#country_id').val());
		<?php } ?>
})
$( document ).ready(function() {
	
	var config = {enterMode : CKEDITOR.ENTER_BR, height:200, filebrowserBrowseUrl: '../js/ckeditor/filemanager/index.html', scrollbars:'yes',
			toolbar_Full:
			[
						//['Source', 'Templates'],
						['Cut','Copy','Paste','PasteText','-','Print', 'SpellChecker', 'Scayt'],
						['Subscript','Superscript'],
						['NumberedList','BulletedList'],
						['BidiLtr', 'BidiRtl' ],
						//['Maximize', 'ShowBlocks'],
						['Undo','Redo'],['Bold','Italic','Underline','Strike'],			
						['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],			
						//['SelectAll','RemoveFormat'],'/',
						['Styles','Format','Font','FontSize'],
						['TextColor','BGColor'],								
						//['Image','Flash','Table','HorizontalRule','Smiley'],
					],
					 width: "620px"
			};
	$('.editor').ckeditor(config);
	
	$.validator.addMethod("needsSelection", function(value, element) {
        return $(element).multiselect("getChecked").length > 0;
    });
	
});

$('.career_in').on('change', function() {
   var careerin = $("input[name='career_in']:checked").val(); 
   if(careerin == '2'){
	   $('#career_main_div').fadeIn();
   }else{
	$('#career_main_div').fadeOut()
   }
});



function getstate(country_id,state_id=null){
	if(country_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>lead/getStates",
			data:{country_id:country_id,state_id:state_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#state_id").html("<option value=''>Select State</option>"+res['option']);
						$("#state_id").selectpicker("refresh");
					}else{
						$("#state_id").html("<option value=''>No state found</option>");
						$("#state_id").selectpicker("refresh");
					}
				}
				/* else{	
					$("#state_id").html("<option value=''>Select</option>");
					$("#state_id").selectpicker("refresh");
				} */
			}
		});
	}
}

function getLeadEnquiyStage(status_id=null,sub_status_id=null,lead_enquiry_status_id=null){
	// alert(sub_status_id);
	<?php if(empty($details)){?>
		var status_id = $('#lead_status').val();
		var sub_status_id =$('#lead_sub_status').val() ;
		// alert("sub_status_id");
	<?php }?>
	if(status_id && sub_status_id ){
		$.ajax({
			url:"<?php echo base_url();?>lead/getLeadEnquiyStage",
			data:{status_id:status_id,sub_status_id:sub_status_id,lead_enquiry_status_id:lead_enquiry_status_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#lead_enquiry_status").html("<option value=''>Select lead enquiry status</option>"+res['option']);
						$("#lead_enquiry_status").selectpicker("refresh");
					}else{
						$("#lead_enquiry_status").html("<option value=''>lead enquiry status</option>");
						// $("#lead_enquiry_status").selectpicker("refresh");
					}
				}
				/* else{	
					$("#lead_enquiry_status").html("<option value=''>Select lead enquiry status</option>");
					$("#lead_enquiry_status").selectpicker("refresh");
				} */
			}
		});
	}
}

function getcity(state_id,city_id=null){
	if(state_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>lead/getCities",
			data:{state_id:state_id,city_id:city_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						$("#city_id").html("<option value=''>Select City</option>"+res['option']);
						$("#city_id").selectpicker("refresh");
					}else{
						$("#city_id").html("<option value=''>No state found</option>");
						$("#city_id").selectpicker("refresh");
					}
				}else{	
					$("#city_id").html("<option value=''>Select</option>");
					$("#city_id").selectpicker("refresh");
				}
			}
		});
	}
}
function getSubStatus(status_id,sub_status_id=null){
	if(status_id != "" ){
		$.ajax({
			url:"<?php echo base_url();?>lead/getSubStatus",
			data:{status_id:status_id,sub_status_id:sub_status_id},
			dataType: 'json',
			method:'post',
			success: function(res){
				if(res['status']=="success"){
					if(res['option'] != ""){
						
						$("#lead_enquiry_status").html("<option value=''>Select lead enquiry status</option>");
						$("#lead_enquiry_status").selectpicker("refresh");
						$("#lead_sub_status").html("<option value=''>Select Sub status</option>"+res['option']);
						$("#lead_sub_status").selectpicker("refresh");
					}else{
						$("#lead_sub_status").html("<option value=''>No Sub status found</option>");
						$("#lead_sub_status").selectpicker("refresh");
					}
				}
				/* else{	
					$("#lead_sub_status").html("<option value=''>Select Sub status</option>");
					$("#lead_sub_status").selectpicker("refresh");
				} */
			}
		});
	}
}

function getCenters(zone_id,center_id = null)
	{
		//alert("Val: "+val);return false;
		if(zone_id != "" )
		{
			$.ajax({
				url:"<?php echo base_url();?>lead/getCenters",
				data:{zone_id:zone_id, center_id:center_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != "")
						{
							$("#center_id").html("<option value=''>Select</option>"+res['option']);
						}
						else
						{
							$("#center_id").html("<option value=''>Select</option>");
						}
					}
					else
					{	
						$("#center_id").html("<option value=''>Select</option>");
					}
				}
			});
		}
	}



var vRules = {
	student_first_name:{required:true},
	student_last_name:{required:true},
	gender:{required:true},
	lead_status :{required:true},
	lead_program :{required:true},
	planned_status:{required:true},
    country_id:{required:true},
    state_id:{required:true},
    city_id:{required:true},
};
var vMessages = {
	student_first_name:{required:"please enter first name"},
	student_last_name:{required:"please enter last name"},
	gender:{required:"Please select the gender "},
	country_id:{required:"select country "},
	lead_status :{required:"select  lead status "},
	lead_program :{required:"select lead sub status"},
    state_id:{required:"select state "},
    city_id:{required:"select city "},
	
};



$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>lead/submitLeaddata";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>lead";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Center";

 
</script>					
