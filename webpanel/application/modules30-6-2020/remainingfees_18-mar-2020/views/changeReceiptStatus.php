<form name="changeStatusForm" id="changeStatusForm"  method="post" >
    <div class="col-sm-12">
        <input type="hidden" name="fees_payment_receipt_id" id="fees_payment_receipt_id" value="<?php echo($receipt_details[0]['fees_payment_receipt_id'])?$receipt_details[0]['fees_payment_receipt_id']:"";?> ">
        <div class="form-group">
            <label for="">Status<span class="text-danger">*</span></label>
            <select name="status" id="status" class="form-control" onchange="onchangeStatus()">
                <?php if(!empty($status)){ 
                    foreach($status as $key=>$val){
                        $sel = "";
                        if($val == $receipt_details[0]['status']){
                            $sel = "selected";
                        }
                ?>
                    <option value="<?php echo $val; ?>" <?php echo  $sel; ?>><?php echo $val; ?></option>
                <?php 
                    }
                } ?>
            </select>
        </div>
        <div class="cancel-wrapper display-none">
            <div class="form-group">
                <label for="">Reason<span class="text-danger">*</span></label>
                <select name="reason" id="reason" class="form-control">
                    <?php if(!empty($reasons)){
                        foreach($reasons as $key=>$val){
                            $sel = "";
                            if(!empty($receipt_details[0]['reason']) && $receipt_details[0]['reason'] == $val){
                                $sel = "selected";
                            }
                    ?>
                        <option value="<?php echo $val; ?>" <?php echo $sel; ?> ><?php echo $val; ?></option>
                    <?php 
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="form-group <?php echo(!empty($receipt_details[0]['reason']) && $receipt_details[0]['reason'] =="Others")?"":"display-none";?> other-reson-wrapper">
                <label for="">Other Reason<span class="text-danger">*</span></label>
                <textarea name="other_reason" id="other_reason" class="form-control"><?php echo(!empty($receipt_details[0]['other_reason']))?$receipt_details[0]['other_reason']:""; ?></textarea>
            </div>
            <div class="form-group">
                <label for="">Cancelled Date<span class="text-danger">*</span></label>
                <input type="text" name="cancelled_date" id="cancelled_date" value="<?php echo (!empty($receipt_details[0]['cancelled_date']) && $receipt_details[0]['cancelled_date'] != "0000-00-00 00:00:00")?date("d-m-Y",strtotime($receipt_details[0]['cancelled_date'])):date("d-m-Y");?>" class="form-control datepicker">
            </div>
        </div>
        <div class="clear-wrapper display-none">
            <div class="form-group">
                <label for="">Clearance Date<span class="text-danger">*</span></label>
                <input type="text" name="cleared_date" id="cleared_date" value="<?php echo (!empty($receipt_details[0]['cleared_date']) && $receipt_details[0]['cleared_date'] != "0000-00-00 00:00:00")?date("d-m-Y",strtotime($receipt_details[0]['cleared_date'])):date("d-m-Y");?>" class="form-control datepicker">
            </div>
        </div>
        <div class="form-actions form-group text-center" style="margin-top: 20px;">
            <button type="submit" class="btn btn-primary " name="changestatusbtn">Save</button>
            <button type="button" class="btn btn-primary " onclick="closeChangeStatusModel()" >Cancel</button>
        </div>
        <div class="clearfix"></div>
    </div>
</form>
<div class="clearfix"></div>
<script>
//assign datepicker
$(".datepicker").datetimepicker({
    format: 'DD-MM-YYYY',   
});
//close change status
function closeChangeStatusModel(){
	$("#changeStatusModel").modal("hide");
}
//change status
onchangeStatus();
function onchangeStatus(){
    if($("#status").val() == "Cancel"){
        $(".cancel-wrapper").slideDown();
        $(".clear-wrapper").hide();
    }else if($("#status").val() == "Cleared"){
        $(".cancel-wrapper").hide();
        $(".clear-wrapper").slideDown();
    }else{
        $(".cancel-wrapper,.clear-wrapper").hide();
    }
}


//change cancel reson
$("#reason").on("change",function(){
    if($(this).val() == "Others"){
        $(".other-reson-wrapper").slideDown();
    }else{
        $(".other-reson-wrapper").hide();
    }
})

//form submit
var vRules = {
    status:{required:true},
    reason:{required:true},
    other_reson:{required:true},
    cancelled_date:{required:true},
    cleared_date:{required:true},
}
var vMessages = {
    status:{required:"Please select status."},
    reason:{required:"Please select reason for Cancel."},
    other_reson:{required:"Please write reason other reason for cancelling."},
    cancelled_date:{required:"Please select cancelled date."},
    cleared_date:{required:"Please select cleared date."},
}
$("#changeStatusForm").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			$('#loadingmessage').show();
			var act = "<?php echo base_url();?>remainingfees/changeReceiptStatusSubmit";
			$("#changeStatusForm").ajaxSubmit({
				url: act, 
				type: 'post',
				cache: false,
				clearForm: false,
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1"){
                        displayMsg("success",res['msg']);
						$('#loadingmessage').hide();
                        $("#changeStatusModel").modal("hide");
                        clearSearchFilters();
					}else{	
						$('#loadingmessage').hide();
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
		}
	});
</script>
<style>
    .display-none{
        display:none;
    }
</style>