<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Communication extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('communicationmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('communication/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function add($id = NULL)
	{
		// print_r();exit();
		if (!empty($_SESSION["webadmin"])) {
			// $record_id = "";

			// if (!empty($_GET['text']) && isset($_GET['text'])) {
			// 	$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
			// 	parse_str($varr, $url_prams);
			// 	$record_id = $url_prams['id'];
			// }
			$result = [];
			$result['categories'] = $this->communicationmodel->getDropdownCategory("tbl_categories","category_id,categoy_name");
			$result['communication_categories'] = $this->communicationmodel->getDropdown("tbl_communication_categories","communication_category_id,communication_categoy_name");
			$result['academicyear'] = $this->communicationmodel->getDropdown("tbl_academic_year_master","academic_year_master_id,academic_year_master_name");
			// $result['allcourses'] = $this->communicationmodel->getDropdownCategory("tbl_courses","course_id,course_name");//for assign to all
			// $result['allbatches'] = $this->communicationmodel->getDropdownCategory("tbl_batch_master","batch_id,batch_name");//for assign to all
			$result['zones'] = $this->communicationmodel->getDropdown("tbl_zones","zone_id,zone_name");
			$result['batch'] = $this->communicationmodel->getDropdown("tbl_batch_master","batch_id,batch_name");
			$result['album'] = $this->communicationmodel->getDropdown("tbl_album","album_id,album_name");
			// $result['details'] = $this->communicationmodel->getFormdata($record_id);

			$this->load->view('template/header.php');
			$this->load->view('communication/add', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function getCourses(){
		$result = $this->communicationmodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['course_id']) && !empty($_REQUEST['course_id'])){
			$course_id = $_REQUEST['course_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getBatch(){
		if($_REQUEST['course_id'] == 'on'){
			$result = $this->communicationmodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
			$option = '';
			$course_id = '';
			
			if(!empty($result)){
				for($i=0;$i<sizeof($result);$i++){
					$course_id_array[] = $result[$i]->course_id;
				}
			}
			
			$allcourseid = $course_id_array;
			$_REQUEST['course_id'] = $allcourseid;
		}
		$result = $this->communicationmodel->getOptions1("tbl_batch_master",$_REQUEST['center_id'],"center_id",$_REQUEST['course_id'],"course_id");
		
		$option = '';
		$batch_id = '';
		
		if(isset($_REQUEST['batch_id']) && !empty($_REQUEST['batch_id'])){
			$batch_id = $_REQUEST['batch_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->batch_id == $batch_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->batch_id.'" '.$sel.' >'.$result[$i]->batch_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getGroup(){
		$result = $this->communicationmodel->getOptionsGroup1($_REQUEST['center_id'],"center_id");
		
		$option = '';
		$group_id = '';
		
		if(isset($_REQUEST['group_id']) && !empty($_REQUEST['group_id'])){
			$group_id = $_REQUEST['group_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->group_master_id == $group_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->group_master_id.'" '.$sel.' >'.$result[$i]->group_master_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getGroupBatch(){
		if($_REQUEST['group_id'] == 'on'){
			$result = $this->communicationmodel->getOptions("tbl_batch_master",$_REQUEST['center_id'],"center_id");
			$group_id = '';
			if(!empty($result)){
				for($i=0;$i<sizeof($result);$i++){
					// print_r($result);exit();
					$group_id_array[] = $result[$i]->group_master_id;
				}
			}
			
			$allgroupid = $group_id_array;
			$_REQUEST['group_id'] = $allgroupid;
		}
		$result = $this->communicationmodel->getOptions1("tbl_batch_master",$_REQUEST['center_id'],"center_id",$_REQUEST['group_id'],"group_id");
		$option = '';
		$batch_id = '';
		
		if(isset($_REQUEST['batch_id']) && !empty($_REQUEST['batch_id'])){
			$batch_id = $_REQUEST['batch_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->batch_id == $batch_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->batch_id.'" '.$sel.' >'.$result[$i]->batch_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getCenters(){
		$result = $this->communicationmodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->center_id == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function getStudent(){
		// print_r($_REQUEST);exit();
		if($_REQUEST['course_id'] == 'on'){
			$result = $this->communicationmodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
			$option = '';
			$course_id = '';
			
			if(!empty($result)){
				for($i=0;$i<sizeof($result);$i++){
					$course_id_array[] = $result[$i]->course_id;
				}
			}
			
			$allcourseid = $course_id_array;
			$_REQUEST['course_id'] = $allcourseid;
		}
		if($_REQUEST['batch_id'] == 'on'){
			$result1 = $this->communicationmodel->getOptions2("tbl_batch_master",$_REQUEST['category_id'],"category_id",$_REQUEST['center_id'],"center_id",$_REQUEST['course_id'],"course_id");
			$option = '';
			$batch_id = '';
			
			if(!empty($result1)){
				for($i=0;$i<sizeof($result1);$i++){
					$batch_id_array[] = $result1[$i]->batch_id;
				}
			}
			
			$allbatchid = $batch_id_array;
			$_REQUEST['batch_id'] = $allbatchid;
		}
		$result = $this->communicationmodel->getStudents($_REQUEST['center_id'],"center_id",$_REQUEST['course_id'],"course_id",$_REQUEST['category_id'],"category_id",$_REQUEST['batch_id'],"batch_id",$_REQUEST['academic_year_id'],'academic_year_id');
		// print_r($result);
		// exit();
		$option = '';
		$student_id = '';
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$option .= '<option value="'.$result[$i]['student_id'].'"  >'.$result[$i]['enrollment_no'].' ( '.$result[$i]['student_first_name'].' '.$result[$i]['student_last_name'].' ) </option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function getGroupStudent(){
		// print_r($_REQUEST);exit();
		if($_REQUEST['group_id'] == 'on'){
			$result = $this->communicationmodel->getOptions("tbl_batch_master",$_REQUEST['center_id'],"center_id");
			// print_r($result);exit();
			$group_id = '';
			if(!empty($result)){
				for($i=0;$i<sizeof($result);$i++){
					// print_r($result);exit();
					$group_id_array[] = $result[$i]->group_id;
				}
			}
			
			$allgroupid = $group_id_array;
			$_REQUEST['group_id'] = $allgroupid;
		}
		if($_REQUEST['batch_id'] == 'on'){
			$result1 = $this->communicationmodel->getOptions1("tbl_batch_master",$_REQUEST['center_id'],"center_id",$_REQUEST['group_id'],"group_id");
			$option = '';
			$batch_id = '';
			
			if(!empty($result1)){
				for($i=0;$i<sizeof($result1);$i++){
					$batch_id_array[] = $result1[$i]->batch_id;
				}
			}
			
			$allbatchid = $batch_id_array;
			$_REQUEST['batch_id'] = $allbatchid;
		}
		// print_r($_REQUEST);exit();
		$result = $this->communicationmodel->getGroupStudents($_REQUEST['center_id'],"d.center_id",$_REQUEST['group_id'],"group_id",$_REQUEST['batch_id'],"batch_id",$_REQUEST['academic_year_id'],'academic_year_id');
		// print_r($result);
		// exit();
		$option = '';
		$student_id = '';
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$option .= '<option value="'.$result[$i]['student_id'].'"  >'.$result[$i]['enrollment_no'].' ( '.$result[$i]['student_first_name'].' '.$result[$i]['student_last_name'].' ) </option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function submitForm()
	{ 
		// print_r($_POST);exit();
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {	

				if($_POST['selection_type'] == 'Class' && !isset($_POST['assigntoallcourse'])){
					if(empty($_POST['course_id'])){
						echo json_encode(array("success"=>"0",'msg'=>'Please select course!'));
						exit;
					}	
				}
				if($_POST['selection_type'] == 'Class' && !isset($_POST['assigntoallbatch'])){
					if(empty($_POST['batch_id'])){
						echo json_encode(array("success"=>"0",'msg'=>'Please select batch!'));
						exit;
					}	
				}
				if($_POST['selection_type'] == 'Group' && !isset($_POST['assigntoallgroup'])){
					if(empty($_POST['group_id'])){
						echo json_encode(array("success"=>"0",'msg'=>'Please select group!'));
						exit;
					}	
				}
				if($_POST['selection_type'] == 'Group' && !isset($_POST['assigntoallgroupbatch'])){
					if(empty($_POST['group_batch_id'])){
						echo json_encode(array("success"=>"0",'msg'=>'Please select batch!'));
						exit;
					}	
				}
				if(!empty($_POST['notice_file_type'])){
					if(!isset($_FILES["notice_file"]["name"])){
						echo json_encode(array("success"=>"0",'msg'=>'Please upload notice file!'));
						exit;
					}	
				}
				//echo DOC_ROOT_FRONT_API."/images/communication_files/";exit;
				$notice_file = "";
				if(isset($_FILES) && isset($_FILES["notice_file"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT_API."/images/communication_files/";
					if($_POST['notice_file_type'] == 'Image'){
						$config['max_size']    = '1000000';
						$config['allowed_types'] = 'gif|jpg|png|jpeg';
					}
					else{
						$config['allowed_types'] = 'pdf';
					}
					$config['file_name']     = md5(uniqid("100_ID", true));
					//$config['file_name']     = $_FILES["cover_image"]["name"].;
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("notice_file"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$notice_file = $image_data['upload_data']['file_name'];
					}
					
					// /* Unlink previous category image */
					// if(!empty($_POST['document_folder_id']))
					// {
					// 	$image = $this->documentfoldermastermodel->getFormdata($_POST['document_folder_id']);
					// 	if(is_array($image) && !empty($image[0]->cover_image) && file_exists(DOC_ROOT_FRONT."/images/document_folder_images/".$image[0]->cover_image))
					// 	{
					// 		@unlink(DOC_ROOT_FRONT."/images/document_folder_images/".$image[0]->cover_image);
					// 	}
					// }
					
				}
				else
				{
					$notice_file = $_POST['notice_file'];
				}

				if(!empty($_POST['student_id'])){
					for($k=0;$k < sizeof($_POST['student_id']); $k++){
						if(isset($_POST['assigntoallcourse']) && $_POST['selection_type'] == 'Class'){
							$getAllCourseData = $this->communicationmodel->getOptionsClass("tbl_batch_master",$_REQUEST['center_id'],"center_id");
							// print_r($getAllCourseData);
							for ($i=0; $i <sizeof($getAllCourseData) ; $i++) { 
								$course_data[] = $getAllCourseData[$i]->course_id;
							}
							// $allcourseid = implode(',', $course_data);
							// $condition = "course_id IN ('".$allcourseid."')";
							$data1 = $course_data;
						}
						else{
							if($_POST['selection_type'] == 'Class'){
								// $condition = "course_id IN ('".$_POST['course_id']."')";
								$course_data[] = $_POST['course_id'];
								$data1 = $course_data;
							}
						}
						if(isset($_POST['assigntoallbatch']) && $_POST['selection_type'] == 'Class'){
							$getAllBatchData = $this->communicationmodel->getOptionsClass("tbl_batch_master",$_REQUEST['center_id'],"center_id");
							for ($i=0; $i <sizeof($getAllBatchData) ; $i++) { 
								$batch_data[] = $getAllBatchData[$i]->batch_id;
							}
							// $allbatchid = implode(',',$batch_data);
							// $condition .= "&& batch_id IN ('".$allbatchid."')";
							$data2 = $batch_data;
						}
						else{
							if($_POST['selection_type'] == 'Class'){
								// $condition .= "&& batch_id IN ('".$_POST['batch_id']."' )";
								$batch_data[] = $_POST['batch_id'];
								$data2 = $batch_data;
							}
						}
						if(isset($_POST['assigntoallgroup']) && $_POST['selection_type'] == 'Group'){
							$getAllGroupData = $this->communicationmodel->getOptionsGroup("tbl_batch_master",$_REQUEST['center_id'],"center_id");
							for ($i=0; $i <sizeof($getAllGroupData) ; $i++) { 
								$group_data[] = $getAllGroupData[$i]->group_id;
							}
							// $allgroupid = implode(',', $group_data);
							// $condition = "group_id IN ('".$allgroupid."' )";
							$data1 = $group_data;
						}
						else{
							if($_POST['selection_type'] == 'Group'){
								// $condition = "group_id IN ('".$_POST['group_id']."' )";
								$group_data[] = $_POST['group_id'];
								$data1 = $group_data;
							}
						}
						if(isset($_POST['assigntoallgroupbatch']) && $_POST['selection_type'] == 'Group'){
							$getAllGroupBatchData = $this->communicationmodel->getOptions1("tbl_batch_master",$_POST['center_id'],"center_id",$group_data,"group_id");
							for ($i=0; $i <sizeof($getAllGroupBatchData) ; $i++) { 
								$batch_data[] = $getAllGroupBatchData[$i]->batch_id;
							}
							// $allbatchid = implode(',',$batch_data);
							// $condition .= "&& batch_id IN ('".$allbatchid."')";
							$data2 = $batch_data;
						}
						else{
							if($_POST['selection_type'] == 'Group'){
								$batch_data[] = $_POST['group_batch_id'];
								$data2 = $batch_data;
							}
						}
						$condition = "category_id='".$_POST['category_id']."' && academic_year_id='".$_POST['academic_year_id']."' && communication_category_id='".$_POST['communication_category_id']."'  &&  zone_id='".$_POST['zone_id']."' &&  center_id='".$_POST['center_id']."' && student_id='".$_POST['student_id'][$k]."'  ";
						// if($_POST['selection_type'] == 'Class'){
						// 	$check_name = $this->communicationmodel->getdataCondition($condition,$data1,"course_id",$data2,"batch_id");
						// }
						// else{
						// 	$check_name = $this->communicationmodel->getdataCondition($condition,$data1,"group_id",$data2,"batch_id");
						// }
						// if(!empty($check_name)){
						// 	if(!isset($_POST['assigntoallcourse']) && !isset($_POST['assigntoallbatch']) && !isset($_POST['assigntoallgroup']) && !isset($_POST['assigntoallgroupbatch'])){
						// 		$getCommunication = $this->communicationmodel->getdata("tbl_student_master", "student_id='".$_POST['student_id'][$k]."' ");
						// 		echo json_encode(array("success"=>"0",'msg'=>' '.$getCommunication[0]['student_first_name'].' Already assign to selected communication!'));
						// 		exit;
						// 	}
						// 	// else{
						// 	// 	$getCommunication = $this->communicationmodel->getdata("tbl_student_master", "student_id='".$_POST['student_id'][$k]."' ");
						// 	// 	echo json_encode(array("success"=>"0",'msg'=>' '.$getCommunication[0]['student_first_name'].' Already assign to selected communication!'));
						// 	// }
						// }
						if($_POST['selection_type'] == 'Class'){
							$getAllData =  $this->communicationmodel->getBatchCourseData("tbl_batch_master",$_REQUEST['center_id'],"center_id",$course_data,"course_id",$batch_data,"batch_id");
						}
						else{
							$getAllData = $this->communicationmodel->getBatchCourseData("tbl_batch_master",$_REQUEST['center_id'],"center_id",$group_data,"group_id",$batch_data,"batch_id");
						}
						for ($j=0; $j <sizeof($getAllData) ; $j++) { 
							if($_POST['selection_type'] == 'Class'){
								$studentassignBatch =  $this->communicationmodel->getdata1("tbl_student_details", "student_id", "batch_id='".$getAllData[$j]->batch_id."' && course_id='".$getAllData[$j]->course_id."' && student_id='".$_POST['student_id'][$k]."' ");
							}
							if($_POST['selection_type'] == 'Group'){
								$studentassignBatch =  $this->communicationmodel->getdata1("tbl_student_group", "student_id", "batch_id='".$getAllData[$j]->batch_id."' && group_id='".$getAllData[$j]->group_id."' && student_id='".$_POST['student_id'][$k]."' ");
							}
							// print_r($check_name);exit();
							// if(empty($check_name)){
								if(!empty($studentassignBatch)){
									$data_array = array();
									$data_array['communication_category_id'] = (!empty($_POST['communication_category_id'])) ? $_POST['communication_category_id'] : '';
									$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
									$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
									$data_array['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
									$data_array['selection_type'] = (!empty($_POST['selection_type'])) ? $_POST['selection_type'] : '';
									$data_array['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0;
									$data_array['course_id'] = (!empty($getAllData[$j]->course_id)) ? $getAllData[$j]->course_id : 0;
									$data_array['batch_id'] = (!empty($getAllData[$j]->batch_id)) ? $getAllData[$j]->batch_id : 0;
									$data_array['group_id'] = (!empty($getAllData[$j]->group_id)) ? $getAllData[$j]->group_id : 0;
									$data_array['noticetitle'] = (!empty($_POST['noticetitle'])) ? $_POST['noticetitle'] : '';
									$data_array['noticedescription'] = (!empty($_POST['noticedescription'])) ? $_POST['noticedescription'] : '';
									$data_array['notice_file_type'] = (!empty($_POST['notice_file_type'])) ? $_POST['notice_file_type'] : 'Image';
									$data_array['notice_file'] = $notice_file;
									$data_array['teacher_id'] = (!empty($_POST['teacher_id'])) ? $_POST['teacher_id'] : 0;
									
									$data_array['created_on'] = date("Y-m-d H:i:s");
									$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
									$data_array['updated_on'] = date("Y-m-d H:i:s");
									$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;

									$result = $this->communicationmodel->insertData('tbl_student_notices', $data_array, '1');


									if(!empty($result)){
										$notice_data = array();
										$notice_data['student_notice_id'] = $result;
										$notice_data['student_id'] = $_POST['student_id'][$k];
										$result1 = $this->communicationmodel->insertData('tbl_notice_students', $notice_data, 1);
										
										
										$get_fcm_data = $this->communicationmodel->get_fcm_data($_POST['student_id'][$k]);
										$email_data = $this->communicationmodel->get_email_data(3);
										
										$title = '';
										$notification = '';
										$email_content=  '';
										$fcm_token = '';
										$subject='';
										$from_email='';
										$to_email='';
										$cc_email = '';
										$notification_type = array();
										
										if(is_array($email_data) ){
											if(!empty($$_POST['noticetitle'])){
												$title = $$_POST['noticetitle'];
											}else{
												$title = $email_data[0]['title'];
											}
											
											$notification = $_POST['noticedescription'];
											$notification_type = array('type'=>'service_request');
											
										}
										
										$notification_data = array();
										$notification_data['student_id'] = $_POST['student_id'][$k];
										$notification_data['notification_type'] = 'Communication';
										$notification_data['notification_title'] = $title;
										$notification_data['notification_contents'] = $notification;
										
										$notification_data['created_on'] = date("Y-m-d H:i:s");
										$notification_data['created_by'] = $_SESSION["webadmin"][0]->user_id;
										$notification_data['updated_on'] = date("Y-m-d H:i:s");
										$notification_data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
										$this->communicationmodel->insertData('tbl_student_notifications', $notification_data, 1);
										
										if(!empty($get_fcm_data[0]['fcm_token'])){
											//echo "h1";
											$fcm_token = $get_fcm_data[0]['fcm_token'];
											if(!empty($fcm_token)){
												//echo "h2";
												//echo $fcm_token;exit;
												//echo "title: ".$title." notification: ".$notification." fcm_token: ".$fcm_token;exit;
												sendNotification($title,$notification,$email_content,$fcm_token,$subject,$from_email,$to_email,$cc_email,$notification_type);
												//echo "<pre>";print_r($res);exit;
												
											}	
										}
										
										
									}else{
										echo json_encode(array("status_code" => "400", "msg" => "Problem in data insertion.", "body" => NULL));
										exit;
									}
								}
							// }
							// else{
							// 	$result1 = 1;
							// }
						}
					}


				}
				// if(empty($result) && $result1 == '1'){
				// 	// echo json_encode(array(
				// 	// 	'success' => '0',
				// 	// 	'msg' => 'Record Already Present.'
				// 	// ));
				// 	exit;
				// }else{
					if (!empty($result)) {
						echo json_encode(array(
							'success' => '1',
							'msg' => 'Record Added/Updated Successfully.'
						));
						exit;
					}
					else{
						echo json_encode(array(
							'success' => '0',
							'msg' => 'Problem in data update.'
						));
						exit;
					}
				// }
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		//echo $id;exit;
		$get_result = $this->communicationmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->student_first_name);
				array_push($temp, $get_result['query_result'][$i]->student_last_name);
				array_push($temp, $get_result['query_result'][$i]->communication_categoy_name);
				array_push($temp, $get_result['query_result'][$i]->batch_name);
				array_push($temp, $get_result['query_result'][$i]->noticetitle);
				array_push($temp, $get_result['query_result'][$i]->noticedescription);
				array_push($temp, $get_result['query_result'][$i]->academic_year_master_name);
				$actionCol21="";
				if($get_result['query_result'][$i]->notice_file_type == 'Image'){
					$image_path = FRONT_API_URL."/images/communication_files/".$get_result['query_result'][$i]->notice_file;
					if(!empty($get_result['query_result'][$i]->notice_file)){
						$actionCol21 .= '<img style="width: 150px;height:150px;padding-top:5px;" src="'.$image_path.'"></img>';	
					}
				}
				elseif($get_result['query_result'][$i]->notice_file_type == 'PDF'){
					$image_path = FRONT_API_URL."/images/communication_files/".$get_result['query_result'][$i]->notice_file;
					if(!empty($get_result['query_result'][$i]->notice_file)){
						$actionCol21 .= '<a href="'.$image_path.'" target="_blank">View Pdf</a>';	
					}
				}
				else{
					$actionCol21 .= '';	
				}
				
				// $actionCol1 = "";
				// $actionCol1.= '<a href="assignalbum/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->assign_album_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				
				array_push($temp, $actionCol21);
				// array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	// function delrecord12()
	// {
	// 	//echo $_POST['status'];exit;
	// 	$id=$_POST['id'];
	// 	$status=$_POST['status'];
		
	// 	$appdResult = $this->communicationmodel->delrecord12("tbl_assign_album","assign_album_id",$id,$status);
	// 	if($appdResult)
	// 	{
	// 		echo "1";
	// 	}
	// 	else
	// 	{
	// 		echo "2";	
	// 	}	
	// }
	
	
}

?>
