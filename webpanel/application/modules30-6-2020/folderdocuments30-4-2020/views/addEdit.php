<?php 
error_reporting(0);
if (!empty($_GET['text']) && isset($_GET['text'])) {
	$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
	//echo $_GET['text'];exit;
	parse_str($varr, $url_prams);
	$record_id = $url_prams['id'];
}
//echo $record_id;exit;
//echo DOC_ROOT_FRONT;exit;

//echo "<pre>";
//print_r($details);
//echo $details[0]->is_downloadable;
//exit;
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<style type="text/css" media="print">
    * { display: none; }
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Folder Document</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>documentfolders">Document Folder </a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
						
						<div class="col-sm-12 text-center pdf-all-btn" >
							<?php 
							if($details[0]->is_downloadable == "Yes"){
							?>	
							<a href ='<?php echo base_url()."images/folder_documents/".$details[0]->document_field_value; ?>' title="Download"download >
								<button class="btn btn-sm pdf-btn" type="button">
									<i class="fa fa-download" > </i>
								</button>
							</a>
							<?php } ?>
							<button class="btn btn-sm pdf-btn" id="zoominbutton" type="button">
								<i class="fa fa-search-plus"></i>
							</button>
							<button class="btn btn-sm pdf-btn" id="zoomoutbutton" type="button">
								<i class="fa fa-search-minus"></i>
							</button>	
											
							<button class="btn btn-sm pdf-btn" id="prev"><i class="fa fa-angle-left" style="font-size:18px;"></i></button>
							<button class="btn btn-sm pdf-btn" id="next"><i class="fa fa-angle-right" style="font-size:18px;"></i></button>
							&nbsp; &nbsp;
							<span class="page-text">Page: <span id="page_num"></span> / <span id="page_count"></span></span>
						</div>

						<div id="parent_pdf_view" class="col-sm-12 text-center" style="border-top:30px;padding-top:15px;overflow-x:auto;min-height:0.01%;height:800px;overflow-y:auto;">
							<div class="pdf-border-div">
								<canvas id="the-canvas"></canvas>
							</div>
							
						</div>
						
						
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->		
<script type="text/javascript" src="<?PHP echo base_url();?>js/plugins/pdf.js"></script>	 

<script type="text/javascript"> 
	$(document).ready(function() {
		$(window).keyup(function(e){
			if(e.keyCode == 44){
				$("body").hide();
			}
		}); 
    }); 
    
	$(document).bind("contextmenu",function(e){
		return false;
	});
</script>
    
<script>
$( "body" ).on( "keydown", function( event ) {
	if(event.which == 39){
		onNextPage()
	}else if(event.which == 37){
		onPrevPage()
	}
});
/* right -39 */
/* left -37 */
</script>			
			
<script type="text/javascript">	

// If absolute URL from the remote server is provided, configure the CORS
// header on that server.
// var url = '//cdn.mozilla.net/pdfjs/tracemonkey.pdf';
var url = '<?php echo base_url()."images/folder_documents/".$details[0]->document_field_value; ?>';

// Loaded via <script> tag, create shortcut to access PDF.js exports.
var pdfjsLib = window['pdfjs-dist/build/pdf'];

// The workerSrc property shall be specified.
// pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';
pdfjsLib.GlobalWorkerOptions.workerSrc = '<?php echo base_url(); ?>js/plugins/pdf.worker.js';

var pdfDoc = null,
    pageNum = 1,
    pageRendering = false,
    pageNumPending = null,
    scale = 0.8,
    canvas = document.getElementById('the-canvas'),
    ctx = canvas.getContext('2d');

if($(window).width() >= 1024){
	scale = 0.8;
}
// alert(scale);
/**
 * Get page info from document, resize canvas accordingly, and render page.
 * @param num Page number.
 */
function renderPage(num) {
  pageRendering = true;
  // Using promise to fetch the page
  pdfDoc.getPage(num).then(function(page) {
    var viewport = page.getViewport(scale);
    canvas.height = viewport.height;
    canvas.width = viewport.width;

    // Render PDF page into canvas context
    var renderContext = {
      canvasContext: ctx,
      viewport: viewport
    };
    var renderTask = page.render(renderContext);

    // Wait for rendering to finish
    renderTask.promise.then(function() {
      pageRendering = false;
      if (pageNumPending !== null) {
        // New page rendering is pending
        renderPage(pageNumPending);
        pageNumPending = null;
      }
    });
  });

  // Update page counters
  document.getElementById('page_num').textContent = num;
}

/**
 * If another page rendering in progress, waits until the rendering is
 * finised. Otherwise, executes rendering immediately.
 */
function queueRenderPage(num) {
  if (pageRendering) {
    pageNumPending = num;
  } else {
    renderPage(num);
  }
}

/**
 * Displays previous page.
 */
function onPrevPage() {
  if (pageNum <= 1) {
    return;
  }
  pageNum--;
  queueRenderPage(pageNum);
}
document.getElementById('prev').addEventListener('click', onPrevPage);

/**
 * Displays next page.
 */
function onNextPage() {
  if (pageNum >= pdfDoc.numPages) {
    return;
  }
  pageNum++;
  queueRenderPage(pageNum);
}
document.getElementById('next').addEventListener('click', onNextPage);

function displayPage(pdf, num) {
            pdf.getPage(num).then(function getPage(page) { renderPage(page); });
         }

var zoominbutton = document.getElementById("zoominbutton");
zoominbutton.onclick = function() {
	scale = scale + 0.25;
	renderPage(pageNum);
	// displayPage(shownPdf, pageNum);
	// alert(scale);
}

var zoomoutbutton = document.getElementById("zoomoutbutton");
zoomoutbutton.onclick = function() {
	if (scale <= 0.25) {
	   return;
	}
	scale = scale - 0.25;
	// displayPage(shownPdf, pageNum);
	renderPage(pageNum);
	// alert(scale);
}


/**
 * Asynchronously downloads PDF.
 */
pdfjsLib.getDocument(url).then(function(pdfDoc_) {
  pdfDoc = pdfDoc_;
  document.getElementById('page_count').textContent = pdfDoc.numPages;

  // Initial/first page rendering
  renderPage(pageNum);
});

</script>

			
<script>

 
$( document ).ready(function() {
	
	<?php 
		if(!empty($details[0]->newsletter_id)){
	?>
		//getRegion('<?php echo $details[0]->zone_text; ?>', '<?php echo $details[0]->region_text; ?>');
		//getArea('<?php echo $details[0]->region_text; ?>', '<?php echo $details[0]->area_text; ?>');
		//getCenter('<?php echo $details[0]->area_text; ?>', '<?php echo $details[0]->center_id; ?>');
	<?php }?>

	
});


var vRules = {
	zone_text:{required:true},
	region_text:{required:true},
	area_text:{required:true},
	center_id:{required:true},
	newsletter_title:{required:true, alphanumericwithspace:true},
	newsletter_description:{required:true},
	cover_image:{extension: "gif,jpg,png,jpeg",filesize: 2}
	
};
var vMessages = {
	zone_text:{required:"Please select zone."},
	region_text:{required:"Please select region."},
	area_text:{required:"Please select area."},
	center_id:{required:"Please select center."},
	newsletter_title:{required:"Please enter title."},
	newsletter_description:{required:"Please enter contents."}
	
};

$("#form-validate").validate({
	success: function(error) { 
        error.removeClass("error");  // <- no, no, no!!
    },
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>albumimages/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						//window.location = "<?php echo base_url();?>albumimages";
						location.reload();
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});



$(document).ready(function(){
		
 	var count = 2;
	$(".addProductImage").click(function(){
		$("#divProductImage").append("<input type='hidden' name='mhid["+count+"]' value='1' /><strong>Image</strong>: <input type='file' name='product_image["+count+"]' /> <br/>");
		count++;
	});
	
	
});


document.title = "AddEdit - Album Images";

 
</script>					
