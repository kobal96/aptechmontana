<!-- <?php 
error_reporting(0);
if (!empty($_GET['text']) && isset($_GET['text'])) {
	$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
	//echo $_GET['text'];exit;
	parse_str($varr, $url_prams);
	$record_id = $url_prams['id'];
}
?> -->

<?php 
//error_reporting(0);
?>

<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1>Promote</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>admission">Promote</a></li>
        </ul>
      </div>
    </div>
	<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
		<div class="card">  
			<div class="" id="earlier-details" style="border: 4px dashed #3c8dbc;padding-left: 20px;padding-top: 20px;padding-bottom: 20px;">  
				<!-- <div class="row">
					<strong><center>Existing Data<center></strong><br>
				</div> -->
				<div class="row">
					<div class="col-sm-6">
						<label for="Zone" class="control-label">Current Academy Year</label>
						<select name="current_academic_year_id" id="current_academic_year_id" class=" form-control">
							<option value="">Select Academy year</option>
							<?php if(!empty($academicyear)){
									foreach($academicyear as $key=>$val){
										$sel ='';
											if(!empty($details[0]->current_academic_year_id)){
												$sel =($details[0]->current_academic_year_id == $val->academic_year_master_id)?"selected":'';
											}
											?>
								<option value="<?php echo $val->academic_year_master_id; ?>" <?= $sel ?> ><?php echo $val->academic_year_master_name;?></option>
							<?php
									}
								}
							?>
						</select>
					</div>

					<div class="col-sm-6">
						<label for="Zone" class="control-label">Zone</label>
						<select name="zone_id" id="zone_id" class=" form-control" onchange="getCenters(this.value)">
							<option value="">Select Zone Year</option>
							<?php if(!empty($zones)){
									foreach($zones as $key=>$val){
										$sel ='';
										if(!empty($details[0]->zone_id)){
											$sel =($details[0]->zone_id == $val->zone_id)?"selected":'';
										}
										?>
								<option value="<?php echo $val->zone_id; ?>" <?= $sel ?> ><?php echo $val->zone_name;?></option>
							<?php
									}
								}
							?>
						</select>
					</div>			
				</div> 
				<br>
				<div class="row">
					<div class="col-sm-6">
						<label for="Zone" class="control-label">Center</label>
						<select name="center_id" id="center_id" class=" form-control">
							<option value="">Select Center Name </option>

						</select>
					</div>

					<div class="col-sm-6">
						<label for="Zone" class="control-label">Category*</label>
						<select name="category_id" id="category_id" class=" form-control" onchange="getCourses(this.value)">
							<option value="">Select Category</option>
						<?php if(!empty($categories)){
									foreach($categories as $key=>$val){
										$sel ='';
										if(!empty($details[0]->category_id)){
											$sel =($details[0]->category_id == $val->category_id)?"selected":'';
										}
										?>
								<option value="<?php echo $val->category_id; ?>" <?= $sel ?> ><?php echo $val->categoy_name;?></option>
							<?php
									}
								} 
							?>
						</select>
					</div>
				</div>

				<br>
				<div class="row">
					<div class="col-sm-6">
						<label for="Zone" class="control-label">Current Course*</label>
						<select name="course_id" id="course_id" class=" form-control" onchange="getBatch(this.value)">
							<option value="">Select Course  </option>
						</select>
					</div>

					<div class="col-sm-6">
						<label for="Zone" class="control-label">Batch*</label>
						<select name="batch_id" id="batch_id" class=" form-control" >
							<option value="">Select Batch </option>

						</select>
					</div>
				</div> 
				<br>	
			


				<?php if(!empty($details)){
					$class ="disabled";
				}else{
					$calss = "";
				} ?>	
				<div class="row">
					<div class="col-sm-6">
							<label for="promoted yeat" class="control-label">To be Promoted Year</label>
							<select name="promoted_academic_year_id" id="promoted_academic_year_id" class=" form-control" onchange="getStudents(this.value)">
								<option value="">Select Promoted Academy year</option>
								<?php if(!empty($upcomingacademicyear)){
										foreach($upcomingacademicyear as $key=>$val){
											$sel ='';
											if(!empty($details[0]->promoted_academic_year_id)){
												$sel =($details[0]->promoted_academic_year_id == $val->academic_year_master_id)?"selected":'';
											}
											?>
									<option value="<?php echo $val->academic_year_master_id;?>"<?= $sel ?> ><?php echo $val->academic_year_master_name;?></option>
								<?php } } ?>
						</select>
					</div>


					<div class="col-sm-6">
						<label for="Zone" class="control-label">Students</label>
						<select name="student_id[]" id="student_id" class="form-control selectpicker" multiple data-width="98%"   data-style="btn-primary" data-actions-box="true" data-live-search="true" data-live-search-placeholder="Select Student ..." <?= $class ?> >
							<option value="">Select Students </option>
							
						</select>
					</div>
				</div> 

			<!-- </div>  -->
			<br>
			<!-- <div class="" id="promote-details" style="border: 4px dashed #3c8dbc;padding-left: 20px;padding-top: 20px;padding-bottom: 20px;">  -->
			<input type="hidden" id="promoted_id" name="promoted_id" value="<?= $details[0]->promoted_id?>" /> 
			<!-- 	<div class="row">
					<strong><center>To Be Promote</center></strong><br>
				</div>		 -->			
				<div class="row">


				<div class="col-sm-6">
					<label for="Zone" class="control-label">Promoted Course</label>
					<select name="promoted_course_id" id="promoted_course_id" class=" form-control">
						<option value="">Select Promoted Course</option>
						
					</select>
				</div>
				<div class="col-sm-6">
					<label for="promoted batch year" class="control-label">Promoted Batch*</label>
					<select name="promoted_batch_id" id="promoted_batch_id" class=" form-control">
						<option value="">Select Promoted Batch </option>
					</select>
				</div>
				</div>
				<br>
				<div class="row">
					<div class="col-sm-6">
						<label for="Zone" class="control-label">Course start Date*</label>
						<div class="controls">
							<input type="text" class="form-control" placeholder="Select Course start date" id="programme_start_date" name="programme_start_date" value="<?php if(!empty($details[0]->programme_start_date)){echo date("d-m-Y", strtotime($details[0]->programme_start_date));}?>" >
						</div>
					</div>

					<div class="col-sm-6">
						<label for="Zone" class="control-label">Course end Date*</label>
						<div class="controls">
							<input type="text" class="form-control required datepicker" placeholder="Select Course end date" id="programme_end_date" name="programme_end_date" value="<?php if(!empty($details[0]->programme_end_date)){echo date("d-m-Y", strtotime($details[0]->programme_end_date));}?>" >
						</div>
					</div>			
				</div>


				<div class="row">
					<div class="col-sm-6">
						<label for="Zone" class="control-label">Promotion Date*</label>
						<div class="controls">
							<input type="text" class="form-control required datepicker" placeholder="Select promotion date" id="promotion_date" name="promotion_date" value="<?php if(!empty($details[0]->promotion_date)){echo date("d-m-Y", strtotime($details[0]->promotion_date));}?>" >
						</div>
					</div>			
				</div>


			</div> 

			<div class="form-actions form-group" style="padding-top:20px">
				<button type="submit" class="btn btn-primary" tabindex="96">Submit</button>
				<a href="<?= base_url('promote') ?>" class="btn btn-primary" tabindex="97">Cancel</a>
			</div>	

		</div><!-- end: Content -->
	</form>
<style>
	.custome-tab-nav{
		background:#ecf0f5;
	}
	.admission_tab{
		border:1px solid #ecf0f5;
		min-height:20px !important;
	}
	.admission_tab .tab-content{
		padding: 30px 25px 10px 35px !important;
	}
	.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus{
		color: #fff;
		background-color: #3c8dbc !important;
		border-radius:0px !important;
	}
	.nav-pills>li>a{
		color:#000;
		font-size:16px;
	}
	.nav-pills>li.active:after{
		content: '';
		position: absolute;
		top: 100%;
		left: 50%;
		margin-left: -10px;
		width: 0;
		height: 0;
		border-top: solid 10px #3c8dbc;
		border-left: solid 10px transparent;
		border-right: solid 10px transparent;
	}
	.nav-pills>li>a:hover, .nav-pills>li>a:focus {
		border-radius:0px !important;
	}
	.nav.nav-pills>li>a {
		position: relative;
		display: block;
		padding: 20px 3px;
		text-align:center;
	}
	.nav.nav-pills>li{
		padding-left: 0px;
		padding-right:0px;
	}
	.table.component_show tbody td{
		padding:10px 0px !important;
	}
</style>					
<script>

	$( document ).ready(function() {
		$('.component_show').hide()
		$('.save').hide()
		$('.course_amount').hide()
		$('.paymentprocess').hide()
		
		$(document).on("change",".installment-month-picker",function(){
			var installmentNo = $('.no_of_installments').val()
		})

		$("#programme_start_date").datepicker({
			format: "dd-mm-yyyy",
			autoclose: true
		});
		$("#dob").datepicker({
			format: "dd-mm-yyyy",
			autoclose: true
		});
		// $('#programme_start_date').change(function(){
		// 	var dob = $('#dob').val();
		// 	$('#age_on').val($(this).val())
		// 	calcDate($(this).val(),dob)
		// })


		$('#dob').change(function(){
			var age_on = $('#age_on').val();

			calcDate(age_on,$(this).val())
		})
		<?php if(!empty($details[0]->student_id)){?>
			getCenters('<?php echo $details[0]->zone_id; ?>', '<?php echo $details[0]->center_id; ?>');
			getCourses('<?php echo $details[0]->category_id; ?>', '<?php echo $details[0]->course_id; ?>');
			getPromotedCourses('<?php echo $details[0]->category_id; ?>', '<?php echo $details[0]->course_id; ?>','<?php echo $details[0]->promoted_course_id; ?>');
			
			getBatch('<?php echo $details[0]->course_id; ?>','<?php echo $details[0]->center_id; ?>','<?php echo $details[0]->batch_id; ?>');

			getPromotedBatch('<?php echo $details[0]->course_id; ?>','<?php echo $details[0]->center_id; ?>','<?php echo $details[0]->promoted_batch_id; ?>');
			getStudents('<?php echo $details[0]->promoted_academic_year_id; ?>','<?php echo $details[0]->batch_id; ?>','<?php echo $details[0]->current_academic_year_id; ?>','<?php echo $details[0]->zone_id; ?>', '<?php echo $details[0]->category_id; ?>', '<?php echo $details[0]->course_id; ?>','<?php echo $details[0]->student_id; ?>');
			
			// getBatchDetails('<?php //echo $details[0]->batch_id; ?>');
			// $("#s2id_state_id a .select2-chosen").text('<?php echo $getStateName[0]->state_name?>');
			// getState('<?php //echo $details[0]->country_id; ?>', '<?php echo $details[0]->state_id; ?>');
			// $("#s2id_city_id a .select2-chosen").text('<?php echo $getCityName[0]->city_name?>');
			// getCity('<?php //echo $details[0]->state_id; ?>','<?php echo $details[0]->city_id; ?>');
		<?php
		}?>
		<?php
			if($details[0]->has_attended_preschool_before == 'Yes'){ ?>
				$('.preschoolNameDiv').show();
			<?php
			}
			else{ ?>
				$('.preschoolNameDiv').hide();
			<?php
			} 
		?>
		$('.has_attended_preschool_before').change(function(){
			var has_attended_preschool_before = $(this).val();
			if(has_attended_preschool_before == 'Yes'){
				$('.preschoolNameDiv').show();
			}
			else{
				$('.preschoolNameDiv').hide();
			}
		})
		$(".datepicker").datetimepicker({
			format: 'DD-MM-YYYY',

		});
		// installmentprocess()
	});
	$(document).on('keypress','.number_decimal_only',function(e){
		if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
			return false;
		}
	})

	



	function calcDate(ageOn,dob){
		if(dob != '' && ageOn != ''){
			var ageOnsplit = ageOn.split('-');
			var ageOndate = ageOnsplit[2]+'-'+ageOnsplit[1]+'-'+ageOnsplit[0];
			var dobsplit = dob.split('-');
			var dobdate = dobsplit[2]+'-'+dobsplit[1]+'-'+dobsplit[0];	
			var diff = Math.abs(new Date(ageOndate).getTime() - new Date(dobdate).getTime());
			var day = 1000 * 60 * 60 * 24;

			var days = Math.ceil(diff/day);
			var monthvalue = Math.floor(days/30)
			$('#dob_month').val(Math.floor(days/30));
			var yearvalue = Math.floor(monthvalue/12)
			$('#dob_year').val(Math.floor(monthvalue/12));
			if(yearvalue > 0){
				var yeardays = days - (365 * yearvalue);
				let dobmonthfloor = Math.floor(yeardays/30);
				$('#dob_month').val(Math.abs(dobmonthfloor))
			}
		}
	}


	function getCenters(zone_id,center_id = null)
	{
		//alert("Val: "+val);return false;
		if(zone_id != "" )
		{
			$.ajax({
				url:"<?php echo base_url();?>promote/getCenters",
				data:{zone_id:zone_id, center_id:center_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != "")
						{
							$("#center_id").html("<option value=''>Select</option>"+res['option']);
						}
						else
						{
							$("#center_id").html("<option value=''>Select</option>");
						}
					}
					else
					{	
						$("#center_id").html("<option value=''>Select</option>");
					}
				}
			});
		}
	}

	function getCourses(category_id,course_id = null)
	{
		$("#batch_id").html("<option value=''>Select</option>");
		//alert("Val: "+val);return false;
		var center_id = $('#center_id').val();
		if(category_id != "" )
		{
			$.ajax({
				url:"<?php echo base_url();?>promote/getCourses",
				data:{category_id:category_id, course_id:course_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != '')
						{
							$("#course_id").html("<option value=''>Select Course</option>"+res['option']);
							$("#promoted_course_id").html("<option value=''>Select</option>"+res['option']);
							
							

						}
						else
						{
							$("#course_id").html("<option value=''>Select Course</option>");
							$("#promoted_course_id").html("<option value=''>Select Promoted Course</option>");
						}
					}
					else
					{	
						$("#course_id").html("<option value=''>Select</option>");
						$("#promoted_course_id").html("<option value=''>Select Promoted Course</option>");
					}
				}
			});
		}else{
			alert("Please select Center Name & Category first");
		}
	}

	function getPromotedCourses(category_id,course_id = null,promoted_course_id=null)
	{
		$("#batch_id").html("<option value=''>Select</option>");
		//alert("Val: "+val);return false;
		var center_id = $('#center_id').val();
		if(category_id != "" )
		{
			$.ajax({
				url:"<?php echo base_url();?>promote/getPromotedCourses",
				data:{category_id:category_id, course_id:course_id, promoted_course_id:promoted_course_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != '')
						{
							// $("#course_id").html("<option value=''>Select Course</option>"+res['option']);
							$("#promoted_course_id").html("<option value=''>Select</option>"+res['option']);
							
							

						}
						else
						{
							// $("#course_id").html("<option value=''>Select Course</option>");
							$("#promoted_course_id").html("<option value=''>Select Promoted Course</option>");
						}
					}
					else
					{	
						// $("#course_id").html("<option value=''>Select</option>");
						$("#promoted_course_id").html("<option value=''>Select Promoted Course</option>");
					}
				}
			});
		}else{
			alert("Please select Center Name & Category first");
		}
	}

	function getBatch(course_id,center_id=null,batch_id = null)
	{
		<?php if(empty($details)){?>
			var center_id = $('#center_id').val();
		<?php }?>
			// alert(center_id);
		if(course_id != ""){
			$.ajax({
				url:"<?php echo base_url();?>promote/getBatch",
				data:{center_id:center_id, course_id:course_id,batch_id:batch_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != '')
						{
							// $("#batch_id").html("<option value=''>Select</option>"+res['option']);
							$("#batch_id").html("<option value=''>Select current batch</option>"+res['option']);
							$("#promoted_batch_id").html("<option value=''>Select Promoted Batch</option>"+res['option']);
						}
						else
						{
							$("#batch_id").html("<option value=''>Select</option>");
							$("#promoted_batch_id").html("<option value=''>Select</option>");

						}
					}
					else
					{	
						$("#batch_id").html("<option value=''>Select</option>");
						$("#promoted_batch_id").html("<option value=''>Select</option>");
					}
				}
			});
		}
	}

	function getPromotedBatch(course_id,center_id=null,promoted_batch_id = null)
	{
		<?php if(empty($details)){?>
			var center_id = $('#center_id').val();
		<?php }?>
			// alert(center_id);
		if(course_id != ""){
			$.ajax({
				url:"<?php echo base_url();?>promote/getPromotedBatch",
				data:{center_id:center_id, course_id:course_id,promoted_batch_id:promoted_batch_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != '')
						{
							// $("#batch_id").html("<option value=''>Select</option>"+res['option']);
							// $("#batch_id").html("<option value=''>Select current batch</option>"+res['option']);
							$("#promoted_batch_id").html("<option value=''>Select Promoted Batch</option>"+res['option']);
						}
						else
						{
							$("#promoted_batch_id").html("<option value=''>Select</option>");
						}
					}
					else
					{	
						$("#promoted_batch_id").html("<option value=''>Select</option>");
					}
				}
			});
		}
	}

	function getBatchDetails(batch_id){
		if(batch_id != "")
		{
			$.ajax({
				url:"<?php echo base_url();?>admission/getBatchDetails",
				data:{ batch_id:batch_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status'] == 'success'){
						if(res['result'][0]['start_date'] != null && res['result'][0]['end_date'] != null){
							let startdate = res['result'][0]['start_date'].split('-')
							let enddate = res['result'][0]['end_date'].split('-')
							$('.batchNote').text('Course start & End Date Between '+startdate[2]+'-'+startdate[1]+'-'+startdate[0]+' - '+enddate[2]+'-'+enddate[1]+'-'+enddate[0])
							$('#batchstartdate').val(startdate[2]+'-'+startdate[1]+'-'+startdate[0]);
							$('#batchenddate').val(enddate[2]+'-'+enddate[1]+'-'+enddate[0]);

						}
						else{
							$('.batchNote').text('');
						}
					}
					else{
						$('.batchNote').text('');
					}
				}
			});
		}

	}

	function getState(country_id,state_id = null)
	{
		if(country_id != "" )
		{
			$.ajax({
				url:"<?php echo base_url();?>admission/getState",
				data:{country_id:country_id, state_id:state_id},
				dataType: 'json',
				method:'post',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != '')
						{
							$("#state_id").html("<option value=''>Select</option>"+res['option']);
							$("#s2id_state_id a .select2-chosen").text('<?php echo $getStateName[0]->state_name?>');
						}
						else
						{
							$("#state_id").html("<option value=''>Select</option>");
						}
					}
					else
					{	
						$("#state_id").html("<option value=''>Select</option>");
					}
				}
			});
		}
	}

	function getCity(state_id,city_id = null)
	{	
		$.ajax({
			url:"<?php echo base_url();?>admission/getCity",
			data:{state_id:state_id,city_id:city_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#city_id").html("<option value=''>Select</option>"+res['option']);
						$("#s2id_city_id a .select2-chosen").text('<?php echo $getCityName[0]->city_name?>');
					}
					else
					{
						$("#city_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#city_id").html("<option value=''>Select</option>");
				}
			}
		});
	}

	function getStudents(promoted_academic_year_id,batch_id=null,academic_year_id=null,zone_id=null,category_id=null,course_id=null,student_id=null){
		// alert(batch_id)
		<?php if(empty($details)){?>
			var promoted_academic_year_id = promoted_academic_year_id;
			var batch_id = $('#batch_id').val();
			var academic_year_id = $('#current_academic_year_id').val();
			var zone_id = $('#zone_id').val();
			var category_id = $('#category_id').val();
			var course_id = $('#course_id').val();
			<?php }?>
		
		
		// alert(course_id);
		if(academic_year_id!='' && zone_id!='' && category_id!=''  &&  course_id!='' && batch_id!='' &&  promoted_academic_year_id !=''){
			
			if(student_id){
				var url = "<?php echo base_url();?>promote/getStudent";
			}else{
				var url = "<?php echo base_url();?>promote/getStudents";
			}
			
			$.ajax({
				url:url,
				data:{academic_year_id,zone_id,category_id,course_id,batch_id,student_id,promoted_academic_year_id},
				dataType: 'json',
				method:'POST',
				success: function(res)
				{
					if(res['status']=="success")
					{
						if(res['option'] != '')
						{
							$("#student_id").html("<option value=''>Select Student</option>"+res['option']);
							$('#student_id').selectpicker('refresh');
							
						}
						else
						{
							$("#student_id").html("<option value=''>Select</option>");
						}
					}
					else
					{	
						$("#student_id").html("<option value=''>Select</option>");
					}
				}
			});
		}else{
			alert("please select the dependent fields")
		}

	}


	var vRules = {
		current_academic_year_id : {required:true},
		inquiry_date:{required:true},
		lead_date:{required:true},
		academic_year_id :  {required:true},
		category_id:{required:true},
		course_id:{required:true},
		zone_id:{required:true},
		center_id :  {required:true},
		batch_id :  {required:true},
		promotion_date:{required:true},
		"student_id[]":{required:true},
		promoted_academic_year_id:{required:true},
		promoted_course_id:{required:true},
		programme_start_date:{required:true},
		promoted_batch_id:{required:true},
		programme_end_date:{required:true}
	};
	var vMessages = {
		current_academic_year_id:{required:"Please select Current year."},
		admission_date:{required:"Please select admission date."},
		academic_year_id:{required:"Please select academic year."},
		category_id:{required:"Please select category."},
		course_id:{required:"Please select course."},
		zone_id:{required:"Please select zone."},
		center_id:{required:"Please select center."},
		batch_id:{required:"Please select batch."},
		promotion_date:{required:"Please select the promotion date"},
		"student_id[]":{required:"please select the student"},
		promoted_academic_year_id:{required:"Please select  promoted year"},
		programme_start_date:{required:"please select program start date"},
		programme_end_date:{required:"please select program end date"},
		promoted_course_id:{required:"please select new course"},
		promoted_batch_id:{required:"Please select PRomoted Batch"},
	};

	$("#form-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) {
			$("#student_id").prop('disabled', false);
			var act = "<?php echo base_url();?>promote/submitForm";
			$("#form-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				cache: false,
				clearForm: false,
				beforeSubmit : function(arr, $form, options){
				
				},success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1"){
						displayMsg("success",res['msg']);
						setTimeout(function(){
						window.location = "<?php echo base_url();?>promote";
					},2000);
					}else{	
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
		}
	});

	$("#familyInfoform-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			var act = "<?php echo base_url();?>admission/submitFamilyInfoForm";
			$("#familyInfoform-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				cache: false,
				clearForm: false,
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1"){
						displayMsg("success",res['msg']);
						setTimeout(function(){
							$('#childInfoTab').removeClass('active');
							$('#childInfoTab_link').removeClass('active');
							$('#familyInfoTab').removeClass('active');
							$('#familyInfoTab_link').removeClass('active');
							$('#contactInfoTab').addClass('active');
							$('#contactInfoTab_link').addClass('active');
						},2000);
					}else{	
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
		}
	});

	$("#contactInfoform-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			var act = "<?php echo base_url();?>admission/submitContactInfoForm";
			$("#contactInfoform-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				cache: false,
				clearForm: false,
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1"){
						displayMsg("success",res['msg']);
						setTimeout(function(){
							$('#childInfoTab').removeClass('active');
							$('#childInfoTab_link').removeClass('active');
							$('#familyInfoTab').removeClass('active');
							$('#familyInfoTab_link').removeClass('active');
							$('#contactInfoTab').removeClass('active');
							$('#contactInfoTab_link').removeClass('active');
							$('#authorizedPersonInfoTab').addClass('active');
							$('#authorizedPersonInfoTab_link').addClass('active')
						},2000);
					}else{	
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
		}
	});

	$("#authorizedInfoform-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			var act = "<?php echo base_url();?>admission/submitAutorizedInfoForm";
			$("#authorizedInfoform-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				cache: false,
				clearForm: false,
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1")
					{
						displayMsg("success",res['msg']);
						setTimeout(function(){
							$('#childInfoTab').removeClass('active');
							$('#childInfoTab_link').removeClass('active');
							$('#familyInfoTab').removeClass('active');
							$('#familyInfoTab_link').removeClass('active');
							$('#contactInfoTab').removeClass('active');
							$('#contactInfoTab_link').removeClass('active');
							$('#authorizedPersonInfoTab').removeClass('active');
							$('#authorizedPersonInfoTab_link').removeClass('active')
							$('#documentInfoTab').addClass('active');
							$('#documentInfoTab_link').addClass('active')
						},2000);
					}else{	
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
		}
	});

	$("#documentInfoform-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			var act = "<?php echo base_url();?>admission/submitdocumentInfoForm";
			$("#documentInfoform-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				cache: false,
				clearForm: false,
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1")
					{

						$('#doc1').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc1']);
						$('#doc_value1').val(res['doc1']);
						$('#doc2').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc2']);
						$('#doc_value2').val(res['doc2']);
						$('#doc_3').attr("src",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc3']);
						$('#doc_value3').val(res['doc3']);
						$('#doc_4').attr("src",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc4']);
						$('#doc_value4').val(res['doc4']);
						$('#doc_5').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc5']);
						$('#doc_value5').val(res['doc5']);
						$('#doc_6').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc6']);
						$('#doc_value6').val(res['doc6']);
						displayMsg("success",res['msg']);
						<?php if($flag == 0){?>
							setTimeout(function(){
								$('#childInfoTab').removeClass('active');
								$('#childInfoTab_link').removeClass('active');
								$('#familyInfoTab').removeClass('active');
								$('#familyInfoTab_link').removeClass('active');
								$('#contactInfoTab').removeClass('active');
								$('#contactInfoTab_link').removeClass('active');
								$('#authorizedPersonInfoTab').removeClass('active');
								$('#authorizedPersonInfoTab_link').removeClass('active')
								$('#documentInfoTab').removeClass('active');
								$('#documentInfoTab_link').removeClass('active')
								// $('#feesInfoTab').addClass('active');
								// $('#feesInfoTab_link').addClass('active');
							},2000);
						<?php }else{?>
							setTimeout(function(){
								$('#childInfoTab').removeClass('active');
								$('#childInfoTab_link').removeClass('active');
								$('#familyInfoTab').removeClass('active');
								$('#familyInfoTab_link').removeClass('active');
								$('#contactInfoTab').removeClass('active');
								$('#contactInfoTab_link').removeClass('active');
								$('#authorizedPersonInfoTab').removeClass('active');
								$('#authorizedPersonInfoTab_link').removeClass('active')
								$('#documentInfoTab').removeClass('active');
								$('#documentInfoTab_link').removeClass('active')
							},2000);
							window.location = "<?php echo base_url();?>admission";
						<?php }?>
					}
					else
					{	
						//$("#error_msg").show();
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
		}
	});

	$("#feesInfoform-validate").validate({
		rules: vRules,
		messages: vMessages,
		submitHandler: function(form) 
		{
			$('#loadingmessage').show();
			var act = "<?php echo base_url();?>admission/submitfeesInfoForm";
			$("#feesInfoform-validate").ajaxSubmit({
				url: act, 
				type: 'post',
				cache: false,
				clearForm: false,
				success: function (response) {
					var res = eval('('+response+')');
					if(res['success'] == "1"){
						$('#loadingmessage').hide();
						var student_id = btoa(res['student_id'])
						window.location = "<?php echo base_url();?>admission/paymentprocess?text="+student_id;
					}else{	
						$('#loadingmessage').hide();
						displayMsg("error",res['msg']);
						return false;
					}
				}
			});
		}
	});

	<?php
		if(isset($_SESSION['student_id']))
		{
			?>
			var student_id = "<?php echo $_SESSION['student_id'];?>"
			$.ajax({
				url: "<?php echo base_url();?>admission/getchildInfo",
				data:{"student_id":student_id},
				async: false,
				type: "POST",
				success: function(response){
					var data2 = eval('('+response+')');
					$('#inquiry_master_id').val(data2['result'][0]['inquiry_master_id']);
					var admissiondatesplit = data2['otherresult'][0]['admission_date'].split('-');
					var admissiondate = admissiondatesplit[2]+'-'+admissiondatesplit[1]+'-'+admissiondatesplit[0];
					$('#admission_date').val(admissiondate);
					$('#academic_year_id').val(data2['otherresult'][0]['academic_year_id']);
					$('#enrollment_no').val(data2['result'][0]['enrollment_no']);
					$('#zone_id').val(data2['result'][0]['zone_id']);
					$('#center_id').val(data2['result'][0]['center_id']);
					$('#category_id').val(data2['otherresult'][0]['category_id']);
					$('#course_id').val(data2['otherresult'][0]['course_id']);
					$('#batch_id').val(data2['otherresult'][0]['batch_id']);
					
					var startdatesplit = data2['otherresult'][0]['programme_start_date'].split('-');
					var startdate = startdatesplit[2]+'-'+startdatesplit[1]+'-'+startdatesplit[0];
					$('#programme_start_date').val(startdate);
					var enddatesplit = data2['otherresult'][0]['programme_end_date'].split('-');
					var enddate = enddatesplit[2]+'-'+enddatesplit[1]+'-'+enddatesplit[0];
					$('#programme_end_date').val(enddate);
					$('#manual_ref_no').val(data2['result'][0]['manual_ref_no']);
					$('#firstname').val(data2['result'][0]['student_first_name']);
					$('#lastname').val(data2['result'][0]['student_last_name']);			
					var dobsplit = data2['result'][0]['dob'].split('-');
					var dob = dobsplit[2]+'-'+dobsplit[1]+'-'+dobsplit[0];	
					$('#dob').val(dob);			
					$('#age_on').val(startdate);			
					$('#dob_year').val(data2['result'][0]['dob_year']);		
					$('#dob_month').val(data2['result'][0]['dob_month']);		
					$('#nationality').val(data2['result'][0]['nationality']);		
					$('#religion').val(data2['result'][0]['religion']);		
					$('#mother_tongue').val(data2['result'][0]['mother_tongue']);		
					$('#other_languages').val(data2['result'][0]['other_languages']);	
					if(data2['result'][0]['has_attended_preschool_before'] == 'Yes'){
						$('.preschoolNameDiv').show();
					}
					else{
						$('.preschoolNameDiv').hide();
					}	
					$('#preschool_name').val(data2['result'][0]['preschool_name']);
					$('#student_id').val(data2['result'][0]['student_id']);
					getCenters(data2['result'][0]['zone_id'], data2['result'][0]['center_id']);
					getCourses(data2['otherresult'][0]['category_id'], data2['otherresult'][0]['course_id']);
					var course_id = data2['otherresult'][0]['course_id']
					var center_id = data2['result'][0]['center_id']
					var batch_id = data2['otherresult'][0]['batch_id']
					getBatchDetails(batch_id)
					if(course_id != "")
					{
						$.ajax({
							url:"<?php echo base_url();?>admission/getBatch",
							data:{center_id:center_id, course_id:course_id,batch_id:batch_id},
							dataType: 'json',
							method:'post',
							success: function(res)
							{
								if(res['status']=="success")
								{
									if(res['option'] != '')
									{
										$("#batch_id").html("<option value=''>Select</option>"+res['option']);
									}
									else
									{
										$("#batch_id").html("<option value=''>Select</option>");
									}
								}
								else
								{	
									$("#batch_id").html("<option value=''>Select</option>");
								}
							}
						});
					}
					if(data2['result'][0]['father_name'] != ' '){
						$('#father_name').val(data2['result'][0]['father_name'])
					}
					else{
						$('#father_name').val('<?php echo $getStudentdetails[0]->father_name ?>')
					}
					if(data2['result'][0]['mother_name'] != null){
						$('#mother_name').val(data2['result'][0]['mother_name'])
					}else{
						$('#mother_name').val('<?php echo $getStudentdetails[0]->mother_name?>')
					}
					if(data2['result'][0]['father_prof'] != ' '){
						$('#father_prof').val(data2['result'][0]['father_prof'])
					}else{
						$('#father_prof').val('<?php echo $getStudentdetails[0]->father_profession?>')
					}
					if(data2['result'][0]['mother_prof'] != null){
						$('#mother_prof').val(data2['result'][0]['mother_prof'])
					}
					else{
						$('#mother_prof').val('<?php echo $getStudentdetails[0]->mother_profession?>')
					}
					$('#father_languages').val(data2['result'][0]['father_languages'])
					$('#mother_languages').val(data2['result'][0]['mother_languages'])
					$('#father_nationality').val(data2['result'][0]['father_nationality'])
					$('#mother_nationality').val(data2['result'][0]['mother_nationality'])
					if(data2['result'][0]['sibling1_id'] != 0){
						$('#sibling1_id').val(data2['result'][0]['sibling1_id'])
					}
					if(data2['result'][0]['sibling2_id'] != 0){
						$('#sibling2_id').val(data2['result'][0]['sibling2_id'])
					}
					if(data2['result'][0]['present_address'] != null){
						$('#present_address').val(data2['result'][0]['present_address'])
					}
					else{
						$('#present_address').val('<?php echo $getStudentdetails[0]->present_address?>')
					}
					if(data2['result'][0]['address1'] != null){
						$('#address1').val(data2['result'][0]['address1'])
					}
					else{
						$('#address1').val('<?php echo $getStudentdetails[0]->address1?>')
					}
					if(data2['result'][0]['address2'] != null){
						$('#address2').val(data2['result'][0]['address2'])
					}
					else{
						$('#address2').val('<?php echo $getStudentdetails[0]->address2?>')
					}
					if(data2['result'][0]['address3'] != null){
						$('#address3').val(data2['result'][0]['address3'])
					}
					else{
						$('#address3').val('<?php echo $getStudentdetails[0]->address3?>')
					}
					if(data2['result'][0]['country_id'] != null){
						$("#country_id").select2().select2("val", data2['result'][0]['country_id']);
						getState(data2['result'][0]['country_id'], data2['result'][0]['state_id']);
						getCity(data2['result'][0]['state_id'],data2['result'][0]['city_id']);
					}
					else{
						$('#country_id').val('<?php echo $getStudentdetails[0]->country_id?>')
						getState('<?php echo $getStudentdetails[0]->country_id?>', '<?php echo $getStudentdetails[0]->state_id?>');
						getCity('<?php echo $getStudentdetails[0]->state_id?>', '<?php echo $getStudentdetails[0]->city_id?>');
					}
					if(data2['getStateName'] != false){
						$("#s2id_state_id a .select2-chosen").text('<?php echo $getStateName[0]->state_name?>');
					}
					if(data2['getCityName'] != false){
						$("#s2id_city_id a .select2-chosen").text('<?php echo $getCityName[0]->city_name?>');
					}
					if(data2['result'][0]['pincode'] != null){
						$('#pincode').val(data2['result'][0]['pincode'])
					}
					else{
						$('#pincode').val('<?php echo $getStudentdetails[0]->pincode?>')
					}
					if(data2['result'][0]['father_email_id'] != null){
						$('#father_email_id').val(data2['result'][0]['father_email_id'])
					}
					else{
						$('#father_email_id').val('<?php echo $getStudentdetails[0]->father_emailid?>')
					}
					if(data2['result'][0]['mother_email_id'] != null){
						$('#mother_email_id').val(data2['result'][0]['mother_email_id'])
					}
					else{
						$('#mother_email_id').val('<?php echo $getStudentdetails[0]->mother_emailid?>')
					}
					if(data2['result'][0]['father_mobile_contact_no'] != null){
						$('#father_mobile_contact_no').val(data2['result'][0]['father_mobile_contact_no'])
					}
					else{
						$('#father_mobile_contact_no').val('<?php echo $getStudentdetails[0]->father_contact_no?>')
					}
					if(data2['result'][0]['mother_mobile_contact_no'] != null){
						$('#mother_mobile_contact_no').val(data2['result'][0]['father_mobile_contact_no'])
					}
					else{
						$('#mother_mobile_contact_no').val('<?php echo $getStudentdetails[0]->mother_contact_no?>')
					}
					$('#father_home_contact_no').val(data2['result'][0]['father_home_contact_no'])
					$('#mother_home_contact_no').val(data2['result'][0]['mother_home_contact_no'])
					$('#father_office_contact_no').val(data2['result'][0]['father_office_contact_no'])
					$('#mother_office_contact_no').val(data2['result'][0]['mother_office_contact_no'])
					$('#emergency_contact_name').val(data2['result'][0]['emergency_contact_name'])
					$('#emergency_contact_mobile_no').val(data2['result'][0]['emergency_contact_mobile_no'])
					$('#emergency_contact_relationship').val(data2['result'][0]['emergency_contact_relationship'])
					$('#emergency_contact_tel_no').val(data2['result'][0]['emergency_contact_tel_no'])
					$('#auth_person1_to_collect').val(data2['result'][0]['auth_person1_to_collect'])
					$('#auth_person1_to_collect_relation').val(data2['result'][0]['auth_person1_to_collect_relation'])
					$('#auth_person1_to_collect_gender').val(data2['result'][0]['auth_person1_to_collect_gender'])
					$('#img_1').attr("src",'<?php echo FRONT_URL; ?>/images/authorized_person_image/'+data2['result'][0]['authperson1_img']);
					$('#prev_value1').val(data2['result'][0]['authperson1_img']);
					$('#auth_person2_to_collect').val(data2['result'][0]['auth_person2_to_collect'])
					$('#auth_person2_to_collect_relation').val(data2['result'][0]['auth_person2_to_collect_relation'])
					$('#auth_person2_to_collect_gender').val(data2['result'][0]['auth_person2_to_collect_gender'])
					$('#img_2').attr("src",'<?php echo FRONT_URL; ?>/images/authorized_person_image/'+data2['result'][0]['authperson2_img']);
					$('#prev_value2').val(data2['result'][0]['authperson2_img']);
					$('#auth_person3_to_collect').val(data2['result'][0]['auth_person3_to_collect'])
					$('#auth_person3_to_collect_relation').val(data2['result'][0]['auth_person3_to_collect_relation'])
					$('#auth_person3_to_collect_gender').val(data2['result'][0]['auth_person3_to_collect_gender'])
					$('#img_3').attr("src",'<?php echo FRONT_URL; ?>/images/authorized_person_image/'+data2['result'][0]['authperson3_img']);
					$('#prev_value3').val(data2['result'][0]['authperson3_img']);
					$('#doc_1').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+data2['result'][0]['duly_filled_admission_form']);
					$('#doc_value1').val(data2['result'][0]['duly_filled_admission_form']);
					$('#doc_2').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+data2['result'][0]['birth_certificate']);
					$('#doc_value2').val(data2['result'][0]['birth_certificate']);
					$('#doc_3').attr("src",'<?php echo FRONT_URL; ?>/images/admission_documents/'+data2['result'][0]['profile_pic']);
					$('#doc_value3').val(data2['result'][0]['profile_pic']);
					$('#doc_4').attr("src",'<?php echo FRONT_URL; ?>/images/admission_documents/'+data2['result'][0]['family_photo']);
					$('#doc_value4').val(data2['result'][0]['family_photo']);
					$('#doc_5').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+data2['result'][0]['address_proof']);
					$('#doc_value5').val(data2['result'][0]['address_proof']);
					$('#doc_6').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+data2['result'][0]['duly_filled_child_profile_form']);
					$('#doc_value6').val(data2['result'][0]['duly_filled_child_profile_form']);
					installmentprocess()
				}
			});
			<?php
		}
		?>

	//function for restric max number enter 
	// $("#no_of_installments").keyup(function(){
	// 	var maxval = $(this).attr("max");
	// 	var current_val = $(this).val();
	// 	if(current_val > maxval){
	// 		alert("Installment No should not greater than Max Installment.")
	// 		$(this).val("");
	// 	} 
	// })

	function getStudentCourse(){
		var student_id = "";
		if("<?php echo $_SESSION['student_id'];?>" != ""){
			student_id = "<?php echo $_SESSION['student_id'];?>";
		}else{
			student_id = $("#student_id").val();
		}
		if(student_id != ""){
			$.ajax({
				url:"<?php echo base_url();?>admission/getStudentCourse",
				data:{student_id:student_id},
				dataType: 'json',
				method:'post',
				success: function(res){
					if(res.status){
						$('#course_name').val(res.course_name);
						$('#assign_admission_date').val(res.admission_date);
						$('#assign_programme_start_date').val(res.programme_start_date);
						$('#assign_programme_end_date').val(res.programme_end_date);
					}
				}
			});
		}
	}

	function getFeesLevel(){
		var student_id = "";
		if("<?php echo $_SESSION['student_id'];?>" != ""){
			student_id = "<?php echo $_SESSION['student_id'];?>";
		}else{
			student_id = $("#student_id").val();
		}
		if(student_id != ""){
			$.ajax({
				url:"<?php echo base_url();?>admission/getFeesLevelDropdown",
				data:{student_id:student_id},
				dataType: 'json',
				method:'post',
				success: function(res){
					$('#fees_level_id').html(res.option);
				}
			});
		}
	}

	$(document).on('keypress','.onlyNumber',function(event){
		if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
			event.preventDefault(); 
		}
	});

	document.title = "AddEdit - Admission";

</script>					
<style>
ul li{list-style:none !important;}
.group-row{
	padding:20px;
	/* text-align:center; */
	margin: 0px auto;
	padding-top:50px;
}
#groupTable>tbody>tr{
	/* border: 4px dashed #3c8dbc !important; */
	border:1px solid #c1c1c1 !important;
}
.removeGroup{
	position:absolute;
	top: 0;
    right: 0;
}
.inner-table{
	border: 3px solid #fff !important;
    background: #fff !important;
    margin: 20px auto !important;
}
.insidetable-form-group{
	padding:20px 10px;
}
.group_installment_wrapper,.frequency_wrapper{
	display:none;
}
.control-group{
	margin-top:10px !important;
}
table tbody.c_table td input.form-control{
	width: 80%;
    float: right;
}
.group-count-cls{
	position: absolute !important;
    top: 0 !important;
    background: #cdcdcd !important;
	padding: 10px 20px;
    border-radius: 20px;
    left: 0 !important;
}
.group-count-cls span{
	padding: 3px 10px;
    background: #3c8dbc;
    color: #fff;
    border-radius: 20px;
}
.course-wrapper{
	border: 4px dashed #3c8dbc;
    padding-left: 20px;
    padding-top: 20px;
    padding-right: 10px;
    margin-left: -10px;
}
</style>