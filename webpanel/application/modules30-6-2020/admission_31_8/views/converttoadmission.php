<!-- <?php 
error_reporting(0);
if (!empty($_GET['text']) && isset($_GET['text'])) {
	$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
	//echo $_GET['text'];exit;
	parse_str($varr, $url_prams);
	$record_id = $url_prams['id'];
}
?> -->

<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1>Admission</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>admission">Admission</a></li>
        </ul>
      </div>
    </div>
    <div class="card">       
     <div class="card-body">             
        <div class="box-content">
            <div class="col-sm-8 col-md-12">
            	<div id="Tab" class="admission_tab"> 
                    <ul  class="nav nav-pills row p-15">
                        <li class="col-md-2 childInfoTab tab p-l-0 active" id="childInfoTab"><a  href="#childInfoTab_link" data-toggle="tab">Child Information</a>
                        </li>
                       	<li class="col-md-2 familyInfoTab tab" id="familyInfoTab"><a href="#familyInfoTab_link" data-toggle="tab">Family Information</a>
                        </li>
                        <li class="col-md-2 contactInfoTab tab" id="contactInfoTab"><a href="#contactInfoTab_link" data-toggle="tab">Contact Information</a>
                        </li>
                        <li class="col-md-2 authorizedPersonInfoTab tab" id="authorizedPersonInfoTab"><a href="#authorizedPersonInfoTab_link" data-toggle="tab">Authorized Person</a>
                        </li>
                        <li class="col-md-2 documentInfoTab tab" id="documentInfoTab"><a href="#documentInfoTab_link" data-toggle="tab">Documents</a>
                        </li>
                        <li class="col-md-2 feesInfoTab tab" id="feesInfoTab"><a href="#feesInfoTab_link" data-toggle="tab">Pay Fees</a>
                    </ul>

                    <div class="tab-content col-md-12">
                        <div class="tab-pane row active" id="childInfoTab_link">
							<form class="" id="childInfoform-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="student_id" name="student_id" value="<?php if(!empty($details[0]->student_id)){echo $details[0]->student_id;}?>" />
								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label">Enquiry*</label>
										<div class="controls">
											<select id="inquiry_master_id" name="inquiry_master_id" class="form-control" readonly="readonly">
												<option value="<?php echo $details[0]->inquiry_master_id;?>"><?php echo $getInquiryNo[0]->inquiry_no .'( '.$details[0]->student_first_name.' '.$details[0]->student_last_name.' )';?></option>
											</select>
										</div>
									</div>

									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Admission Date*</span></label>
										<div class="controls">
											<input type="text" class="form-control datepicker" placeholder="Select Admission date" id="admission_date" name="admission_date" value="<?php if(!empty($otherdetails[0]->admission_date)){echo date("d-m-Y", strtotime($otherdetails[0]->admission_date));}?>" >
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label">Academic Year*</label>
										<div class="controls">
											<select id="academic_year_id" name="academic_year_id" class="form-control" readonly="readonly">
												<option value="">Select Academic Year</option>
												<?php 
													if(isset($academicyear) && !empty($academicyear)){
														foreach($academicyear as $cdrow){
															$sel = ($cdrow->academic_year_master_id == $details[0]->academic_year_master_id) ? 'selected="selected"' : '';
												?>
													<option value="<?php echo $cdrow->academic_year_master_id;?>" <?php echo $sel; ?>><?php echo $cdrow->academic_year_master_name;?></option>
												<?php }}?>
											</select>
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Enrollment No</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="enrollment_no" value="<?php if(!empty($details[0]->enrollment_no)){echo $details[0]->enrollment_no;}else{echo $enrollment_no;}?>" readonly="readonly" id="enrollment_no">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label" for="zone_id">Zone*</label>
										<div class="controls">
											<select id="zone_id" name="zone_id" class="form-control"  onchange="getCenters(this.value);" >
												<option value="">Select Zone</option>
												<?php 
													if(isset($zones) && !empty($zones)){
														foreach($zones as $cdrow){
															$sel = ($cdrow->zone_id == $details[0]->zone_id) ? 'selected="selected"' : '';
												?>
													<option value="<?php echo $cdrow->zone_id;?>" <?php echo $sel; ?>><?php echo $cdrow->zone_name;?></option>
												<?php }}?>
											</select>
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Center*</span></label> 
										<div class="controls">
											<select id="center_id" name="center_id" class="form-control"  onchange="getBatch(this.value);" >
												<option value="">Select Center</option>
											</select>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label" for="category_id">Category*</label>
										<div class="controls">
											<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);" >
												<option value="">Select Category</option>
												<?php 
													if(isset($categories) && !empty($categories)){
														foreach($categories as $cdrow){
															$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
												?>
													<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
												<?php }}?>
											</select>
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Course*</span></label> 
										<div class="controls">
											<select id="course_id" name="course_id" class="form-control" onchange="getBatch(this.value);">
												<option value="">Select Course</option>
											</select>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label" for="batch_id">Batch*</label>
										<div class="controls">
											<select id="batch_id" name="batch_id" class="form-control" onchange="getBatchDetails(this.value)">
												<option value="">Select Batch</option>
											</select>
										</div>
										<span class="text-danger batchNote"></span>
										<input type="hidden" name="batch_start_date" id="batchstartdate">
										<input type="hidden" name="batch_end_date" id="batchenddate">
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Program Start Date*</span></label>
										<div class="controls">
											<input type="text" class="form-control" placeholder="Select Program start date" id="programme_start_date" name="programme_start_date" value="<?php if(!empty($otherdetails[0]->programme_start_date)){echo date("d-m-Y", strtotime($otherdetails[0]->programme_start_date));}?>" >
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Program End Date*</span></label>
										<div class="controls">
											<input type="text" class="form-control required datepicker" placeholder="Select Program end date" id="programme_end_date" name="programme_end_date" value="<?php if(!empty($otherdetails[0]->programme_end_date)){echo date("d-m-Y", strtotime($otherdetails[0]->programme_end_date));}?>" >
										</div>
									</div>
								</div>

								<h3>Child Information</h3>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>First Name*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="student_first_name" value="<?php if(!empty($details[0]->student_first_name)){echo $details[0]->student_first_name;}?>" placeholder="First Name" id="firstname">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Last Name*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="student_last_name" value="<?php if(!empty($details[0]->student_last_name)){echo $details[0]->student_last_name;}?>" placeholder="Last Name" id="lastname">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-3 control-group form-group">
										<label class="control-label"><span>Date Of Birth*</span></label>
										<div class="controls">
											<input type="text" class="form-control required" placeholder="Select Date Of Birth" id="dob" name="dob" value="<?php if(!empty($details[0]->student_dob)){echo date("d-m-Y", strtotime($details[0]->student_dob));}?>" id="dob">
										</div>
									</div>
									<div class="col-md-3 control-group form-group">
										<label class="control-label"><span>Age as on AY Start Date</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="age_on" value="<?php if(!empty($details[0]->age_on)){echo $details[0]->age_on;}?>" readonly="readonly" id="age_on">
										</div>
									</div>
									<div class="col-md-3 control-group form-group">
										<label class="control-label"><span>Years</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="dob_year" value="<?php if(!empty($details[0]->dob_year)){echo $details[0]->dob_year;}?>" readonly="readonly" id="dob_year">
										</div>
									</div>
									<div class="col-md-3 control-group form-group">
										<label class="control-label"><span>Months</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="dob_month" value="<?php if(!empty($details[0]->dob_month)){echo $$details[0]->dob_month;}?>" readonly="readonly" id="dob_month">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Nationality*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="nationality" value="<?php if(!empty($details[0]->nationality)){echo $details[0]->nationality;}?>" placeholder="Nationality" id="nationality">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Religion*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="religion" value="<?php if(!empty($details[0]->religion)){echo $details[0]->religion;}?>" placeholder="Religion" id="religion">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Mother Tongue*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="mother_tongue" value="<?php if(!empty($details[0]->mother_tongue)){echo $details[0]->mother_tongue;}?>" placeholder="Mother Tongue" id="mother_tongue">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Other Language(s)</span></label>
										<div class="controls">
											<textarea class="form-control" name="other_languages" value="" placeholder="Other Language" id="other_languages"><?php if(!empty($details[0]->other_languages)){echo $details[0]->other_languages;}?></textarea>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Has Child attend preschool before?</span></label>
										<div class="controls">
											<input type="radio" class="form-control has_attended_preschool_before" name="has_attended_preschool_before" value="No" <?php echo (!empty($details[0]->has_attended_preschool_before) &&  ($details[0]->has_attended_preschool_before== 'No')) ? "checked" : ""?> checked="checked">No
											<input type="radio" class="form-control has_attended_preschool_before" name="has_attended_preschool_before" value="Yes" <?php echo (!empty($details[0]->has_attended_preschool_before) &&  ($details[0]->has_attended_preschool_before== 'Yes')) ? "checked" : ""?> >Yes
										</div>
									</div>
									<div class="col-md-6 control-group form-group preschoolNameDiv">
										<label class="control-label"><span>Preschool Name*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="preschool_name" value="<?php if(!empty($details[0]->preschool_name)){echo $details[0]->preschool_name;}?>" placeholder="Preschool Name" id="preschool_name">
										</div>
									</div>
								</div>

								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>admission" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                        <div class="tab-pane row" id="familyInfoTab_link">
                       		<form class="" id="familyInfoform-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="student_id" name="student_id" value="<?php if(!empty($details[0]->student_id)){echo $details[0]->student_id;}?>" />

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Father's Name*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="father_name" value="<?php if(!empty($details[0]->father_name)){echo $details[0]->father_name;}?>" placeholder="Father Name" id="father_name">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Mother's Name*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="mother_name" value="<?php if(!empty($details[0]->mother_name)){echo $details[0]->mother_name;}?>" placeholder="Mother Name" id="mother_name">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Father Profession*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="father_prof" value="<?php if(!empty($details[0]->father_profession)){echo $details[0]->father_profession;}?>" placeholder="Father Profession" id="father_prof">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Mother Profession*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="mother_prof" value="<?php if(!empty($details[0]->mother_profession)){echo $details[0]->mother_profession;}?>" placeholder="Mother Profession" id="mother_prof">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Father Language(s)*</span></label>
										<div class="controls">
											<textarea class="form-control" name="father_languages" value="" id="father_languages"><?php if(!empty($details[0]->father_languages)){echo $details[0]->father_languages;}?></textarea>
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Mother Language(s)*</span></label>
										<div class="controls">
											<textarea class="form-control" name="mother_languages" value="" id="mother_languages"><?php if(!empty($details[0]->mother_languages)){echo $details[0]->mother_languages;}?></textarea>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Father Nationality*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="father_nationality" value="<?php if(!empty($details[0]->father_nationality)){echo $details[0]->father_nationality;}?>" placeholder="Father Nationality" id="father_nationality">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Mother Nationality*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="mother_nationality" value="<?php if(!empty($details[0]->mother_nationality)){echo $details[0]->mother_nationality;}?>" placeholder="Mother Nationality" id="mother_nationality">
										</div>
									</div>
								</div>

								<h3>Siblings Name and Date of Birth :</h3>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Enter Enrollment No</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="sibling1_enrollment" value="" placeholder="Enter Sibling Enrollment No" id="sibling1_enrollment">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Enter Enrollment No</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="sibling2_enrollment" value="" placeholder="Enter Sibling Enrollment No" id="sibling2_enrollment">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Sibling Name</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="sibling1_name" value="" placeholder="" readonly="readonly" id="sibling1_id">
											<input type="hidden" class="form-control" name="sibling1_id" value="" id="sibling1_id_hidden">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Sibling Name</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="sibling2_name" value="" placeholder="" readonly="readonly" id="sibling2_id">
											<input type="hidden" class="form-control" name="sibling2_id" value="" id="sibling2_id_hidden">
										</div>
									</div>
								</div>

								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>admission" class="btn btn-primary">Cancel</a>
								</div>
							</form>	
                        </div>
                        <div class="tab-pane" id="contactInfoTab_link" >
                        	<form class="" id="contactInfoform-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="student_id" name="student_id" value="<?php if(!empty($details[0]->student_id)){echo $details[0]->student_id;}?>" />

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>House/Bldg/Apt*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="present_address" value="<?php if(!empty($details[0]->present_address)){echo $details[0]->present_address;}?>" id="present_address" placeholder="Enter House/Bldg/Apt">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Street/Road/Lane*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="address1" value="<?php if(!empty($details[0]->address1)){echo $details[0]->address1;}?>" id="address1" placeholder="Enter Street/Road/Lane">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Landmark*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="address2" value="<?php if(!empty($details[0]->address2)){echo $details[0]->address2;}?>" id="address2" placeholder="Enter Landmark">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Area/Locality/Center*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="address3" value="<?php if(!empty($details[0]->address3)){echo $details[0]->address3;}?>" id="address3" placeholder="Enter Area/Locality/Center">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label" for="category_id">Country*</label>
										<div class="controls">
											<select id="country_id" name="country_id" class="form-control select2"  onchange="getState(this.value);" >
												<option value="">Select Country</option>
												<?php 
													if(isset($countries) && !empty($countries)){
														foreach($countries as $cdrow){
															$sel = ($cdrow->country_id == $details[0]->country_id) ? 'selected="selected"' : '';
												?>
													<option value="<?php echo $cdrow->country_id;?>" <?php echo $sel; ?>><?php echo $cdrow->country_name;?></option>
												<?php }}?>
											</select>
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>State*</span></label> 
										<div class="controls">
											<select id="state_id" name="state_id" class="form-control select2" onchange="getCity(this.value);">
												<option value="">Select State</option>
											</select>
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label" for="batch_id">City*</label>
										<div class="controls">
											<select id="city_id" name="city_id" class="form-control select2">
												<option value="">Select City</option>
											</select>
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Pincode*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="pincode" value="<?php if(!empty($details[0]->pincode)){echo $details[0]->pincode;}?>" placeholder="Pincode" id="pincode">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Father's Email Id*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="father_email_id" value="<?php if(!empty($details[0]->father_emailid)){echo $details[0]->father_emailid;}?>" placeholder="Enter Father Email Id" id="father_email_id">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Mother's Email Id*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="mother_email_id" value="<?php if(!empty($details[0]->mother_emailid)){echo $details[0]->mother_emailid;}?>" placeholder="Enter Mother  Email Id" id="mother_email_id">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Father Mobile Contact No*</span></label>
										<div class="controls">
											<input type="number" class="form-control" name="father_mobile_contact_no" value="<?php if(!empty($details[0]->father_contact_no)){echo $details[0]->father_contact_no;}?>" placeholder="Enter Father Mobile Contact No" id="father_mobile_contact_no" min="0">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Mother Mobile Contact No*</span></label>
										<div class="controls">
											<input type="number" class="form-control" name="mother_mobile_contact_no" value="<?php if(!empty($details[0]->mother_contact_no)){echo $details[0]->mother_contact_no;}?>" placeholder="Enter Mother Mobile Contact No" id="mother_mobile_contact_no" min="0">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Father Home Contact No</span></label>
										<div class="controls">
											<input type="number" class="form-control" name="father_home_contact_no" value="<?php if(!empty($details[0]->father_home_contact_no)){echo $details[0]->father_home_contact_no;}?>" placeholder="Enter Father Home Contact No" id="father_home_contact_no" min="0">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Mother Home Contact No</span></label>
										<div class="controls">
											<input type="number" class="form-control" name="mother_home_contact_no" value="<?php if(!empty($details[0]->mother_home_contact_no)){echo $details[0]->mother_home_contact_no;}?>" placeholder="Enter Mother Home Contact No" id="mother_home_contact_no" min="0">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Father Office Contact No</span></label>
										<div class="controls">
											<input type="number" class="form-control" name="father_office_contact_no" value="<?php if(!empty($details[0]->father_office_contact_no)){echo $details[0]->father_office_contact_no;}?>" placeholder="Enter Father Office Contact No" id="father_office_contact_no" min="0">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Mother Office Contact No</span></label>
										<div class="controls">
											<input type="number" class="form-control" name="mother_office_contact_no" value="<?php if(!empty($details[0]->mother_office_contact_no)){echo $details[0]->mother_office_contact_no;}?>" placeholder="Enter Mother Office Contact No" id="mother_office_contact_no" min="0">
										</div>
									</div>
								</div>

								<h3>Emergency Contact:</h3>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Name*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="emergency_contact_name" value="<?php if(!empty($details[0]->emergency_contact_name)){echo $details[0]->emergency_contact_name;}?>" placeholder="Enter Name" id="emergency_contact_name">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Relationship*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="emergency_contact_relationship" value="<?php if(!empty($details[0]->emergency_contact_relationship)){echo $details[0]->emergency_contact_relationship;}?>" placeholder="Enter Relationship" id="emergency_contact_relationship">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Mobile*</span></label>
										<div class="controls">
											<input type="number" class="form-control" name="emergency_contact_mobile_no" value="<?php if(!empty($details[0]->emergency_contact_mobile_no)){echo $details[0]->emergency_contact_mobile_no;}?>" placeholder="Enter Mobile No" id="emergency_contact_mobile_no" min="0">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Home Telephone</span></label>
										<div class="controls">
											<input type="number" class="form-control" name="emergency_contact_tel_no" value="<?php if(!empty($details[0]->emergency_contact_tel_no)){echo $details[0]->emergency_contact_tel_no;}?>" placeholder="Enter Home Telephone No" id="emergency_contact_tel_no" min="0">
										</div>
									</div>
								</div>

								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>admission" class="btn btn-primary">Cancel</a>
								</div>
							</form>	
                        </div>
                        <div class="tab-pane" id="authorizedPersonInfoTab_link">
                        	<form class="" id="authorizedInfoform-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="student_id" name="student_id" value="<?php if(!empty($details[0]->student_id)){echo $details[0]->student_id;}?>" />

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Name*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="auth_person1_to_collect" value="<?php if(!empty($details[0]->auth_person1_to_collect)){echo $details[0]->auth_person1_to_collect;}?>" placeholder="Enter Name" id="auth_person1_to_collect">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Relationship*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="auth_person1_to_collect_relation" value="<?php if(!empty($details[0]->auth_person1_to_collect_relation)){echo $details[0]->auth_person1_to_collect_relation;}?>" placeholder="Enter Relationship" id="auth_person1_to_collect_relation">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label" for="category_id">Gender</label>
										<div class="controls">
											<input type="radio" class="form-control auth_person1_to_collect_gender" name="auth_person1_to_collect_gender" value="Male" <?php echo (!empty($details[0]->auth_person1_to_collect_gender) &&  ($details[0]->auth_person1_to_collect_gender== 'Male')) ? "checked" : ""?> checked="checked">Male
											<input type="radio" class="form-control auth_person1_to_collect_gender" name="auth_person1_to_collect_gender" value="Female" <?php echo (!empty($details[0]->auth_person1_to_collect_gender) &&  ($details[0]->auth_person1_to_collect_gender== 'Female')) ? "checked" : ""?> >Female
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Profile Pic*</span></label> 
										<div class="controls">
											<?php
												if(!empty($details[0]->authperson1_img) || isset($_SESSION['student_id'])){
													if(!empty($details[0]->authperson1_img)){
													?>
													<div id="img1_load">
														<img src="<?php echo FRONT_URL; ?>/images/authorized_person_image/<?php echo $details[0]->authperson1_img; ?>" height="200px" id="img_1">
													</div>
												<?php }else{
													if(!empty($studentImageDetails[0]->authperson1_img)){
													?>
													<div id="img1_load">
														<img src="<?php echo FRONT_URL; ?>/images/authorized_person_image/<?php echo $studentImageDetails[0]->authperson1_img; ?>" height="200px" id="img_1">
													</div>
												<?php }
												}}
											?>
											<input type="file" name="img_file1" id="img_file1" class="form-control" accept="image/jpg,image/jpeg,image/png" onchange="img1Change()" />
											<input type="hidden" name="prev_value1" id="prev_value1" value="<?php if(!empty($details[0]->authperson1_img)){echo $details[0]->authperson1_img;}?>" />
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Name</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="auth_person2_to_collect" value="<?php if(!empty($details[0]->auth_person2_to_collect)){echo $details[0]->auth_person2_to_collect;}?>" placeholder="Enter Name" id="auth_person2_to_collect">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Relationship</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="auth_person2_to_collect_relation" value="<?php if(!empty($details[0]->auth_person2_to_collect_relation)){echo $details[0]->auth_person2_to_collect_relation;}?>" placeholder="Enter Relationship" id="auth_person2_to_collect_relation">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label" for="category_id">Gender</label>
										<div class="controls">
											<input type="radio" class="form-control auth_person2_to_collect_gender" name="auth_person2_to_collect_gender" value="Male" <?php echo (!empty($details[0]->auth_person2_to_collect_gender) &&  ($details[0]->auth_person2_to_collect_gender== 'Male')) ? "checked" : ""?> checked="checked">Male
											<input type="radio" class="form-control auth_person2_to_collect_gender" name="auth_person2_to_collect_gender" value="Female" <?php echo (!empty($details[0]->auth_person2_to_collect_gender) &&  ($details[0]->auth_person2_to_collect_gender== 'Female')) ? "checked" : ""?> >Female
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Profile Pic</span></label> 
										<div class="controls">
											<?php
												if(!empty($details[0]->authperson2_img) || isset($_SESSION['student_id']) ){
													if(!empty($details[0]->authperson2_img)){
													?>
													<img src="<?php echo FRONT_URL; ?>/images/authorized_person_image/<?php echo $details[0]->authperson2_img; ?>" height="200px" id="img_2">
												<?php }else{
													if(!empty($studentImageDetails[0]->authperson2_img)){
													?>
													<div id="img2_load">
														<img src="<?php echo FRONT_URL; ?>/images/authorized_person_image/<?php echo $studentImageDetails[0]->authperson2_img; ?>" height="200px" id="img_2">
													</div>
												<?php }
												} }
											?>
											<input type="file" name="img_file2" id="img_file2" class="form-control" accept="image/jpg,image/jpeg,image/png" / value="" onchange="img2Change()">
											<input type="hidden" name="prev_value2" id="prev_value2" value="<?php if(!empty($details[0]->authperson2_img)){echo $details[0]->authperson2_img;}?>" />
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Name</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="auth_person3_to_collect" value="<?php if(!empty($details[0]->auth_person3_to_collect)){echo $details[0]->auth_person3_to_collect;}?>" placeholder="Enter Name" id="auth_person3_to_collect">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Relationship</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="auth_person3_to_collect_relation" value="<?php if(!empty($details[0]->auth_person3_to_collect_relation)){echo $details[0]->auth_person3_to_collect_relation;}?>" placeholder="Enter Relationship" id="auth_person3_to_collect_relation">
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label" for="category_id">Gender</label>
										<div class="controls">
											<input type="radio" class="form-control auth_person3_to_collect_gender" name="auth_person3_to_collect_gender" value="Male" <?php echo (!empty($details[0]->auth_person3_to_collect_gender) &&  ($details[0]->auth_person3_to_collect_gender== 'Male')) ? "checked" : ""?> checked="checked">Male
											<input type="radio" class="form-control auth_person3_to_collect_gender" name="auth_person3_to_collect_gender" value="Female" <?php echo (!empty($details[0]->auth_person3_to_collect_gender) &&  ($details[0]->auth_person3_to_collect_gender== 'Female')) ? "checked" : ""?> >Female
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Profile Pic</span></label> 
										<div class="controls">
											<?php
												if(!empty($details[0]->authperson3_img) || isset($_SESSION['student_id'])){
													if(!empty($details[0]->authperson3_img)){
													?>
													<img src="<?php echo FRONT_URL; ?>/images/authorized_person_image/<?php echo $details[0]->authperson3_img; ?>" height="200px" id="img_3">
												<?php } else{
													if(!empty($studentImageDetails[0]->authperson3_img)){
													?>
													<div id="img3_load">
														<img src="<?php echo FRONT_URL; ?>/images/authorized_person_image/<?php echo $studentImageDetails[0]->authperson3_img; ?>" height="200px" id="img_3">
													</div>
												<?php }
												}}
											?>
											<input type="file" name="img_file3" id="img_file3" class="form-control" accept="image/jpg,image/jpeg,image/png" / value="" onchange="img3Change()">
											<input type="hidden" name="prev_value3" id="prev_value3" value="<?php if(!empty($details[0]->authperson3_img)){echo $details[0]->authperson3_img;}?>" />
										</div>
									</div>
								</div>

								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>admission" class="btn btn-primary">Cancel</a>
								</div>
							</form>	
                        </div>
                        <div class="tab-pane" id="documentInfoTab_link">
                        	<form class="" id="documentInfoform-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="student_id" name="student_id" value="<?php if(!empty($details[0]->student_id)){echo $details[0]->student_id;}?>" />

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Immunization Cerificate*</span></label> 
										<div class="controls">
											<?php
												if(!empty($details[0]->duly_filled_admission_form) || isset($_SESSION['student_id'])){
													if(!empty($details[0]->duly_filled_admission_form)){
													?>
													<div id="doc1_load">
														<a href="<?php echo FRONT_URL; ?>/images/admission_documents/<?php echo $details[0]->duly_filled_admission_form; ?>" id="doc_1" target="_blank">View Certificate</a>
													</div>
												<?php }else{
													if(!empty($studentImageDetails[0]->duly_filled_admission_form)){
													?>
													<div id="doc1_load">
														<a href="<?php echo FRONT_URL; ?>/images/admission_documents/<?php echo $studentImageDetails[0]->duly_filled_admission_form; ?>" id="doc_1" target="_blank">View Certificate</a>
													</div>
												<?php }
												} }
											?>
											<input type="file" name="doc_file1" id="doc_file1" class="form-control" accept="application/pdf" onchange="doc1Change()" />
											<input type="hidden" name="doc_value1" id="doc_value1" value="<?php if(!empty($details[0]->duly_filled_admission_form)){echo $details[0]->duly_filled_admission_form;}?>" />
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Birth Cerificate*</span></label> 
										<div class="controls">
											<?php
												if(!empty($details[0]->birth_certificate) || isset($_SESSION['student_id'])){
													if(!empty($details[0]->birth_certificate)){
													?>
													<div id="doc2_load">
														<a href="<?php echo FRONT_URL; ?>/images/admission_documents/<?php echo $details[0]->birth_certificate; ?>" id="doc_2" target="_blank">View Certificate</a>
													</div>
												<?php }else{
													if(!empty($studentImageDetails[0]->birth_certificate)){
													?>
													<div id="doc2_load">
														<a href="<?php echo FRONT_URL; ?>/images/admission_documents/<?php echo $studentImageDetails[0]->birth_certificate; ?>" id="doc_2" target="_blank">View Certificate</a>
													</div>
												<?php }
												}}
											?>
											<input type="file" name="doc_file2" id="doc_file2" class="form-control" accept="application/pdf" onchange="doc2Change()" />
											<input type="hidden" name="doc_value2" id="doc_value2" value="<?php if(!empty($details[0]->birth_certificate)){echo $details[0]->birth_certificate;}?>" />
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Child Photo*</span></label> 
										<div class="controls">
											<?php
												if(!empty($details[0]->profile_pic) || isset($_SESSION['student_id'])){
													if(!empty($details[0]->profile_pic)){
													?>
													<div id="doc3_load">
														<img src="<?php echo FRONT_URL; ?>/images/admission_documents/<?php echo $details[0]->profile_pic; ?>" height="200px" id="doc_3">
													</div>
												<?php }else{
													if(!empty($studentImageDetails[0]->birth_certificate)){
													?>
													<div id="doc3_load">
														<img src="<?php echo FRONT_URL; ?>/images/admission_documents/<?php echo $studentImageDetails[0]->profile_pic; ?>" height="200px" id="doc_3">
													</div>
												<?php }
												}}
											?>
											<input type="file" name="doc_file3" id="doc_file3" class="form-control" accept="image/jpg,image/jpeg,image/png" onchange="doc3Change()" />
											<input type="hidden" name="doc_value3" id="doc_value3" value="<?php if(!empty($details[0]->profile_pic)){echo $details[0]->profile_pic;}?>" />
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Family Photo*</span></label> 
										<div class="controls">
											<?php
												if(!empty($details[0]->family_photo) || isset($_SESSION['student_id'])){
													if(!empty($details[0]->family_photo)){
													?>
													<div id="doc4_load">
														<img src="<?php echo FRONT_URL; ?>/images/admission_documents/<?php echo $details[0]->family_photo; ?>" height="200px" id="doc_4">
													</div>
												<?php }else{
													if(!empty($studentImageDetails[0]->birth_certificate)){
													?>
													<div id="doc4_load">
														<img src="<?php echo FRONT_URL; ?>/images/admission_documents/<?php echo $studentImageDetails[0]->family_photo; ?>" height="200px" id="doc_4">
													</div>
												<?php }
												}}
											?>
											<input type="file" name="doc_file4" id="doc_file4" class="form-control" accept="image/jpg,image/jpeg,image/png" onchange="doc4Change()" />
											<input type="hidden" name="doc_value4" id="doc_value4" value="<?php if(!empty($details[0]->family_photo)){echo $details[0]->family_photo;}?>" />
										</div>
									</div>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Address Proof*</span></label> 
										<div class="controls">
											<?php
												if(!empty($details[0]->address_proof) || isset($_SESSION['student_id'])){
													if(!empty($details[0]->address_proof)){
													?>
													<div id="doc5_load">
														<a href="<?php echo FRONT_URL; ?>/images/admission_documents/<?php echo $details[0]->address_proof; ?>" id="doc_5" target="_blank">View Certificate</a>
													</div>
												<?php }else{
													if(!empty($studentImageDetails[0]->birth_certificate)){
													?>
													<div id="doc5_load">
														<a href="<?php echo FRONT_URL; ?>/images/admission_documents/<?php echo $studentImageDetails[0]->address_proof; ?>" id="doc_5" target="_blank">View Certificate</a>
													</div>
												<?php }
												}}
											?>
											<input type="file" name="doc_file5" id="doc_file5" class="form-control" accept="application/pdf" onchange="doc5Change()" />
											<input type="hidden" name="doc_value5" id="doc_value5" value="<?php if(!empty($details[0]->address_proof)){echo $details[0]->address_proof;}?>" />
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Profile Form*</span></label> 
										<div class="controls">
											<?php
												if(!empty($details[0]->duly_filled_child_profile_form) || isset($_SESSION['student_id'])){
													if(!empty($details[0]->duly_filled_child_profile_form)){
													?>
													<div id="doc6_load">
														<a href="<?php echo FRONT_URL; ?>/images/admission_documents/<?php echo $details[0]->duly_filled_child_profile_form; ?>" height="200px" id="doc_6" target="_blank">View Certificate</a>
													</div>
												<?php }else{
													if(!empty($studentImageDetails[0]->birth_certificate)){
													?>
													<div id="doc6_load">
														<a href="<?php echo FRONT_URL; ?>/images/admission_documents/<?php echo $studentImageDetails[0]->duly_filled_child_profile_form; ?>" height="200px" id="doc_6" target="_blank">View Certificate</a>
													</div>
												<?php }
												}}
											?>
											<input type="file" name="doc_file6" id="doc_file6" class="form-control" accept="application/pdf" onchange="doc6Change()" />
											<input type="hidden" name="doc_value6" id="doc_value6" value="<?php if(!empty($details[0]->duly_filled_child_profile_form)){echo $details[0]->duly_filled_child_profile_form;}?>" />
										</div>
									</div>
								</div>

								<div class="form-actions form-group">
									<input type="submit" class="btn btn-primary" name="Submit" value="Submit">
									<a href="<?php echo base_url();?>admission" class="btn btn-primary">Cancel</a>
								</div>
							</form>	
                        </div>
                        <div class="tab-pane row" id="feesInfoTab_link">
							<form class="" id="feesInfoform-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="student_id" name="student_id" value="<?php if(!empty($details[0]->student_id)){echo $details[0]->student_id;}?>">
								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label" for="category_id">Fees Level*</label>
										<div class="controls">
											<select id="fees_level_id" name="fees_level_id" class="form-control"  onchange="getFees(this.value);">
												<option value="">Select Fees Level</option>
												<?php 
													if(isset($fees_level) && !empty($fees_level)){
														foreach($fees_level as $cdrow){
															$sel = ($cdrow->fees_level_id == $details[0]->fees_level_id) ? 'selected="selected"' : '';
												?>
													<option value="<?php echo $cdrow->fees_level_id;?>" <?php echo $sel; ?>><?php echo $cdrow->fees_level_name;?></option>
												<?php }}?>
											</select>
										</div>
									</div>

									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Fees*</span></label> 
										<div class="controls">
											<select id="fees_id" name="fees_id" class="form-control" onchange="getFessDetails(this.value);">
												<option value="">Select Fees</option>
											</select>
										</div>
									</div>
								</div>

								<table class="table table-striped component_show" style="border: 1px">
									<thead>
										<tr>
											<th>Sequence No</th>
						        			<th>Fees Component</th>
						        			<th>Component Fees</th>
						        			<th>Discount Amount</th>
						        			<th>GST Amount</th>
						        			<th>Sub Amount</th>
										</tr>
									</thead>
									<tbody class="c_table">
									</tbody>
								</table>
								<input type="hidden" name="total_gst_amount" id="total_gst_amount">
								<input type="hidden" name="mandatory_component_amount" id="mandatory_component_amount">
								<input type="hidden" name="component_size" id="component_size">
								<input type="hidden" name="fees_total_amount" id="fees_total_amount">

								<div class="row">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Pattern Type</span></label>
										<div class="controls">
											<input type="radio" class="form-control is_instalment" name="is_instalment" value="No" <?php echo (!empty($details[0]->is_instalment) &&  ($details[0]->is_instalment== 'No')) ? "checked" : ""?> checked="checked">Lumsum
											<input type="radio" class="form-control is_instalment install" name="is_instalment" value="Yes" <?php echo (!empty($details[0]->is_instalment) &&  ($details[0]->is_instalment== 'Yes')) ? "checked" : ""?> >Installment
										</div>
									</div>
									<div class="col-md-6 control-group form-group lumsum">
										<label class="control-label"><span>Course Amount</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="fees_amount_collected" value="<?php if(!empty($details[0]->fees_amount_collected)){echo $details[0]->fees_amount_collected;}else{ echo 0;}?>" readonly="readonly" id="fees_amount_collected">
										</div>
									</div>
									<div class="col-md-6 control-group form-group installment">
										<label class="control-label"><span>No Of Installment *</span></label>
										<div class="controls">
											<input type="number" class="form-control no_of_installments" name="no_of_installments" value="<?php if(!empty($details[0]->no_of_installments)){echo $details[0]->no_of_installments;}else{ echo 0;}?>" id="no_of_installments" min="0">
										</div>
										<span class="text-danger installment_error"></span>
									</div>
								</div>
								
								<div class="row">
									<div class="col-md-6 control-group form-group ">
										<label class="control-label"><span>Discount (in %)</span></label>
										<div class="controls">
											<input type="text" class="form-control number_decimal_only" name="discount_amount_percentage" id="discount_amount_percentage" min="0">
										</div>
									</div>
									<div class="col-md-6 control-group form-group installment">
										<label class="control-label"><span>MaxInstallment Allowed</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="max_installment" value="" readonly="readonly" id="max_installment">
										</div>
									</div>
									<div class="col-md-6 control-group form-group lumsum">
										<label class="control-label"><span>Discount Amount</span></label>
										<div class="controls">
											<input type="number" class="form-control" name="discount_amount_rs" id="total_discount_amount" readonly="readonly">
										</div>
									</div>
									<!-- <div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Total Amount</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="fees_total_amount" value="<?php if(!empty($details[0]->fees_total_amount)){echo $details[0]->fees_total_amount;}?>" readonly="readonly" id="fees_total_amount">
										</div>
									</div> -->
								</div>
								
								<div class="row">
									<div class="col-md-6 control-group form-group installment">
										<label class="control-label"><span>Discount Amount</span></label>
										<div class="controls">
											<input type="number" class="form-control" name="discount_amount_rs" id="total_discount_amount" readonly="readonly">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Amount After Discount</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="total_amount" value="<?php if(!empty($details[0]->total_amount)){echo $details[0]->total_amount;}?>" readonly="readonly" id="total_amount">
										</div>
									</div>
									<div class="col-md-6 control-group form-group lumsum">
										<label class="control-label"><span>Fees Remark*</span></label>
										<div class="controls">
											<textarea class="form-control" name="fees_remarkl" value="" placeholder="Fees Remark" id="fees_remarkl"><?php if(!empty($details[0]->fees_remark)){echo $details[0]->fees_remark;}?></textarea>
										</div>
									</div>
								</div>
								
								<div class="table-responsive installment">
									<?php echo $_SESSION['installment_no'];?>
									<table class="table table-striped" id="fees_table">
										<thead>
											<tr>
												<td>Payment Due Date</td>
												<td>Insatllment Amount</td>
												<td class="paymentprocess">Paying Amount</td>
												<td class="paymentprocess">Remaining Amount</td>
											</tr>
										</thead>
										<tbody class="f_table">
										</tbody>
									</table>
								</div>

								<div class="row">
									<div class="col-md-6 control-group form-group installment">
										<label class="control-label"><span>Fees Remark*</span></label>
										<div class="controls">
											<textarea class="form-control" name="fees_remark" value="" placeholder="Fees Remark" id="fees_remarki"><?php if(!empty($details[0]->fees_remark)){echo $details[0]->fees_remark;}?></textarea>
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Fees Accepted By</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="fees_approved_accepted_by_name" value="<?php echo $login_name;?>" readonly="readonly">
										</div>
									</div>
								</div>

								<h3 class="paymentprocess">Fill Payment Details</h3>

								<div class="row paymentprocess">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Payment Mode</span></label>
										<div class="controls">
											<input type="radio" class="form-control payment_mode" name="payment_mode" value="Cheque" <?php echo (!empty($details[0]->payment_mode) &&  ($details[0]->payment_mode== 'Cheque')) ? "checked" : ""?>>Cheque
											<input type="radio" class="form-control payment_mode" name="payment_mode" value="Cash" <?php echo (!empty($details[0]->payment_mode) &&  ($details[0]->payment_mode== 'Cash')) ? "checked" : ""?> checked="checked" >Cash
											<input type="radio" class="form-control payment_mode" name="payment_mode" value="Netbanking" <?php echo (!empty($details[0]->payment_mode) &&  ($details[0]->payment_mode== 'Netbanking')) ? "checked" : ""?> >Netbanking
										</div>
									</div>
									<div class="col-md-6 control-group form-group both">
										<label class="control-label"><span>Bank Name*</span></label>
										<div class="controls">
											<input type="text" class="form-control" name="bank_name" value="<?php if(!empty($details[0]->bank_name)){echo $details[0]->bank_name;}?>" id="bank_name">
										</div>
									</div>
								</div>

								<div class="row cheque paymentprocess">
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Cheque No*</span></label>
										<div class="controls">
											<input type="number" class="form-control" name="cheque_no" value="<?php if(!empty($details[0]->cheque_no)){echo $details[0]->cheque_no;}?>" id="cheque_no" min="0">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Cheque Date*</span></label>
										<div class="controls">
											<input type="text" class="form-control datepicker" placeholder="Select transaction_date date" id="transaction_date" name="cheque_date" value="<?php if(!empty($details[0]->transaction_date)){echo date("d-m-Y", strtotime($otherdetails[0]->transaction_date));}?>">
										</div>
									</div>
								</div>

								<div class="row transaction paymentprocess">
									<div class="col-md-6 control-group form-group cheque transaction">
										<label class="control-label"><span>Transaction No*</span></label>
										<div class="controls">
											<input type="number" class="form-control" name="transaction_id" value="<?php if(!empty($details[0]->transaction_id)){echo $details[0]->transaction_id;}?>" id="transaction_id" min="0">
										</div>
									</div>
									<div class="col-md-6 control-group form-group">
										<label class="control-label"><span>Transaction Date*</span></label>
										<div class="controls">
											<input type="text" class="form-control datepicker" placeholder="Select transaction_date date" id="transaction_date" name="transaction_date" value="<?php if(!empty($details[0]->transaction_date)){echo date("d-m-Y", strtotime($otherdetails[0]->transaction_date));}?>">
										</div>
									</div>
								</div>

								<div class="form-actions form-group">
									<button type="button" class="btn btn-primary skip">Skip To Payment</button>
									<button type="submit" class="btn btn-primary save" name="paymentprocess">Save</button>
									<a href="<?php echo base_url();?>admission" class="btn btn-primary">Cancel</a>
									<button type="submit" class="btn btn-primary" name="group">Add Group Fees</button>
								</div>
							</form>
                        </div>
                    </div>
                </div>
            </div>
        <div class="clearfix"></div>
        </div>
     </div>
    </div>        
</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	$('.component_show').hide()
	$('.save').hide()
	$('.paymentprocess').hide()
	$('.skip').click(function(){
		let month_array = []
		let installmentNo = $('.no_of_installments').val()
		for(let i=0;i<installmentNo;i++){
			if($('#installmentdate_'+i).val() == ''){
				$('.installmenterror_'+i).text('Select Date');
				return 
			}
			else{
				let monthValue = $('#installmentdate_'+i).val()
				month_array.push(monthValue)
			}
		}
		$(".f_table").children().remove();
		$('.save').show()
		$(this).hide()
		$('.paymentprocess').show()
		$('.cheque').hide();
		$('.transaction').hide();
		$('.both').hide();

		if($('.install').is(':checked') == true){
			for(let i=0;i<installmentNo;i++){
				let totalsubamnt = $('.total_sub_amount').text()
				let installment_amount = (totalsubamnt/installmentNo).toFixed(2)
				$(".f_table").append("<tr>");
				$(".f_table").append('<td><div class="form-group"><input type="text" class="form-control " placeholder="Select Date" id="installmentdate_'+i+'" name="instalment_due_date[]" readonly="readonly"></div></td>');
				$(".f_table").append('<td><div class="form-group"><input type="text" class="form-control instalment_amount"  name="instalment_amount" readonly="readonly"></div></td>');
				$(".f_table").append('<td><div class="form-group"><input type="text" class="form-control instalment_collected_amount" id="instalmentcollectedamount_'+i+'" name="instalment_collected_amount[]"></div></td>');
				$(".f_table").append('<td><div class="form-group"><input type="text" class="form-control instalment_remaining_amount"  id="instalmentremainingamount_'+i+'"name="instalment_remaining_amount[]" readonly="readonly"></div></td>');
				$(".f_table").append("</tr>");
				$(".f_table").append("</br>");
				$('#installmentdate_'+i).val(month_array[i])
				$('.instalment_amount').val(installment_amount)
				$('.instalment_collected_amount').val(0)
				$('.instalment_remaining_amount').val(installment_amount)
				$('.instalment_collected_amount').keyup(function(){
					let collected_amount = $(this).val()
					let installment_amount = $('.instalment_amount').val()

					let remaining_amount_input = $(this).parent().parent().next().find('.instalment_remaining_amount')
					if(parseInt(collected_amount) > parseInt(installment_amount)){
						remaining_amount_input.val(installment_amount);
					}
					else{				
						let remaining_amount = ($('.instalment_amount').val() - collected_amount).toFixed(2)
						remaining_amount_input.val(remaining_amount);
					}
				})
			}
		}
	})
	$("#programme_start_date").datepicker({
		format: "dd-mm-yyyy",
		autoclose: true
	});
	$("#dob").datepicker({
		format: "dd-mm-yyyy",
		autoclose: true
	});
	$('#programme_start_date').change(function(){
		var dob = $('#dob').val();
		$('#age_on').val($(this).val())
		calcDate($(this).val(),dob)
	})

	$('#dob').change(function(){
		var age_on = $('#age_on').val();

		calcDate(age_on,$(this).val())
	})
	<?php 
		if(!empty($details[0]->inquiry_master_id)){
	?>
		getCourses('<?php echo $details[0]->category_id; ?>', '<?php echo $details[0]->course_id; ?>');
		getBatch('<?php echo $details[0]->course_id; ?>','<?php echo $details[0]->center_id; ?>','<?php echo $details[0]->batch_id; ?>');
		getBatchDetails('<?php echo $details[0]->batch_id; ?>');
		getCenters('<?php echo $details[0]->zone_id; ?>', '<?php echo $details[0]->center_id; ?>');
		$("#s2id_state_id a .select2-chosen").text('<?php echo $getStateName[0]->state_name?>');
		getState('<?php echo $details[0]->country_id; ?>', '<?php echo $details[0]->state_id; ?>');
		$("#s2id_city_id a .select2-chosen").text('<?php echo $getCityName[0]->city_name?>');
		getCity('<?php echo $details[0]->state_id; ?>','<?php echo $details[0]->city_id; ?>');
	<?php
	}?>
	<?php 
		if(!empty($details[0]->student_id)){
	?>
		getCenters('<?php echo $details[0]->zone_id; ?>', '<?php echo $details[0]->center_id; ?>');
		getCourses('<?php echo $otherdetails[0]->category_id; ?>', '<?php echo $otherdetails[0]->course_id; ?>');
		getBatch('<?php echo $otherdetails[0]->course_id; ?>','<?php echo $details[0]->center_id; ?>','<?php echo $otherdetails[0]->batch_id; ?>');
		getBatchDetails('<?php echo $otherdetails[0]->batch_id; ?>');
		getState('<?php echo $details[0]->country_id; ?>', '<?php echo $details[0]->state_id; ?>');
		getCity('<?php echo $details[0]->state_id; ?>','<?php echo $details[0]->city_id; ?>');

	<?php
	}?>
	<?php
		if($details[0]->has_attended_preschool_before == 'Yes'){ ?>
			$('.preschoolNameDiv').show();
		<?php
		}
		else{ ?>
			$('.preschoolNameDiv').hide();
		<?php
		} 
	?>
	$('.has_attended_preschool_before').change(function(){
		var has_attended_preschool_before = $(this).val();
		if(has_attended_preschool_before == 'Yes'){
			$('.preschoolNameDiv').show();
		}
		else{
			$('.preschoolNameDiv').hide();
		}
	})
	$(".datepicker").datetimepicker({
		format: 'DD-MM-YYYY',

	});
	installmentprocess()
});

function installmentprocess(){
	$('.cheque').hide();
	$('.transaction').hide();
	$('.both').hide();
	$('.payment_mode').change(function(){
		var payment_mode = $(this).val();
		if(payment_mode == 'Cheque'){
			$('.cheque').show();
			$('.transaction').hide();
			$('.both').show();
		}
		else if(payment_mode == 'Netbanking'){
			$('.cheque').hide();
			$('.transaction').show();
			$('.both').show();
		}
		else{
			$('.cheque').hide();
			$('.transaction').hide();
			$('.both').hide();
		}
	})
	$(".datepicker").datetimepicker({
		format: 'DD-MM-YYYY',

	});
	$('.installment').hide();
	$('.is_instalment').change(function(){
		var is_instalment = $(this).val();
		if(is_instalment == 'Yes'){
			$('.installment').show();
			$('.lumsum').hide();
		}
		else{
			$('.installment').hide();
			$('.lumsum').show();
		}
	})
	$('#discount_amount_percentage').keyup(function(){
		var discount_amount = $(this).val();
		var fees_total_amount = $('#fees_total_amount').val();
		if(fees_total_amount == undefined){
		} 
		else{
			let total_discount_amount = 0; //sum of total discount amount
			let total_sub_amount = 0
			let total_gst_amount = 0;
			let component_size = $('#component_size').val()
			for(let i=0;i<component_size;i++){
				if(discount_amount != 0){
					console.log(1)
					var is_discount = $('#isdiscountable_'+i).val()
					if(is_discount == 'Yes'){
						let component_fees = $('#fees_'+i).val()
						let discount_amount_inpercentage = (parseFloat(component_fees) * parseFloat(discount_amount))/100;
						$('#discountamount_'+i).val(discount_amount_inpercentage)
						if(discount_amount_inpercentage <= component_fees){

							total_discount_amount = parseFloat(total_discount_amount) + discount_amount_inpercentage
							$('#total_discount_amount').val(total_discount_amount)
							let is_gst = $('#isgst_'+i).val()
							if(is_gst == 'Yes'){
								let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage))
								$('#componentnewfees_'+i).val(component_new_fees)
								let tax = $('#tax_'+i).val()
								let component_gst_fees = ((parseFloat(component_new_fees) * parseFloat(tax))/100).toFixed(2);
								$('#componentgstfees_'+i).val(component_gst_fees)
								$('#gstamount_'+i).val(component_gst_fees)
							}
							else{
								let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage))
								$('#componentgstfees_'+i).val(0)
								$('#gstamount_'+i).val(0)
							}
							let component_new_fees = $('#componentnewfees_'+i).val()
							let component_gst_fees = $('#componentgstfees_'+i).val()
							let subamount = ((parseFloat(component_new_fees) - parseFloat(component_gst_fees))).toFixed(2)
							$('#subamount_'+i).val(subamount)
						}
						else{
							$('#discountamount_'+i).val(0)
							let component_fees = $('#componentgstfees_'+i).val()
							let gst_amount = $('#originalgstfees_'+i).val()
							$("#gstamount_"+i).val(gst_amount)
							let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
							$('#subamount_'+i).val(subamount)
						}
					}
					else{
						$('#discountamount_'+i).val(0)
						let component_fees = $('#componentgstfees_'+i).val()
						let gst_amount = $('#originalgstfees_'+i).val()
						$("#gstamount_"+i).val(gst_amount)
						let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
						$('#subamount_'+i).val(subamount)
					}
				}
				else{
					$('#total_discount_amount').val('')
					$('#discountamount_'+i).val(0)
					let component_fees = $('#componentfees_'+i).val()
					let gst_amount = $('#originalgstfees_'+i).val()
					$("#gstamount_"+i).val(gst_amount)
					let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
					$('#subamount_'+i).val(subamount)
				}
				let gst_amount = $('#gstamount_'+i).val()
				total_gst_amount = total_gst_amount + parseFloat(gst_amount)
				$('#total_gst_amount').val(total_gst_amount)
				let sub_amount = $('#subamount_'+i).val()
				total_sub_amount = total_sub_amount + parseFloat(sub_amount)
				$('.total_sub_amount').text((total_sub_amount).toFixed(2))
			}
		}
		var amount_after_discount = Number(fees_total_amount) - Number(discount_amount);
		let totalsubamount = $('.total_sub_amount').text()
		$('#total_amount').val(totalsubamount)
		let installmentNo = $('.no_of_installments').val()
		let installment_amount = (amount_after_discount/installmentNo).toFixed(2)
		$('#fees_amount_collected').val(totalsubamount)
		let total_discount_amount = $('#total_discount_amount').val()
		$('#total_discount_amount').val(total_discount_amount)
		let discount_amount_rs = $('#total_discount_amount').val()
		$('.instalment_amount').val(installment_amount)
		let mandatory_component_amount = $('#mandatory_component_amount').val()
		let mandatory_discount_amount = parseFloat(mandatory_component_amount) - parseFloat(discount_amount_rs)
		$('#mandatory_component_amount').val(mandatory_discount_amount)
		$('.instalment_collected_amount').val(0)
		$('.instalment_remaining_amount').val(installment_amount)
		let installmentno = $('.no_of_installments').val()
		let installamount = (totalsubamount/installmentno).toFixed(2)
		$('.instalment_amount').val(installamount)
	})

	$('.no_of_installments').keyup(function(){
		var installmentNo = $(this).val()
		var max_installment = $('#max_installment').val()
		if(installmentNo == ''){
			$(".f_table").children().remove();
		}
		else{
			if(installmentNo > max_installment){
				$('.installment_error').text('Installment No should not greater than Max Installment') 
			}
			else{
				$('.installment_error').text('') 
				for(let i=0;i<installmentNo;i++){
					let totalsubamnt = $('.total_sub_amount').text()
					let installment_amount = (totalsubamnt/installmentNo).toFixed(2)
					$(".f_table").append("<tr>");
					$(".f_table").append('<td><div class="form-group"><input type="text" class="form-control monthPicker" placeholder="Select Date" id="installmentdate_'+i+'" name="instalment_due_date[]"></div><span class="installmenterror_'+i+' text-danger"></span></td>');
					$(".f_table").append('<td><div class="form-group"><input type="text" class="form-control instalment_amount"  name="instalment_amount" readonly="readonly"></div><span></span></td>');
					$(".f_table").append("</tr>");
					$(".f_table").append("</br>");
					$('.instalment_amount').val(installment_amount)
					$('.instalment_collected_amount').val(0)
					$('.instalment_remaining_amount').val(installment_amount)
				}
				$(".monthPicker").datetimepicker({ 
					format: 'DD-MM-YYYY',
				});
				$('.instalment_collected_amount').keyup(function(){
					let collected_amount = $(this).val()
					let installment_amount = $('.instalment_amount').val()

					let remaining_amount_input = $(this).parent().parent().next().find('.instalment_remaining_amount')
					if(parseInt(collected_amount) > parseInt(installment_amount)){
						remaining_amount_input.val(installment_amount);
					}
					else{				
						let remaining_amount = ($('.instalment_amount').val() - collected_amount).toFixed(2)
						remaining_amount_input.val(remaining_amount);
					}
				})
			}
		}
	})
}

$(document).on('keypress','.number_decimal_only',function(e){
    if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) {
        return false;
    }
})

function calcDate(ageOn,dob){
	if(dob != '' && ageOn != ''){
    	var ageOnsplit = ageOn.split('-');
    	var ageOndate = ageOnsplit[2]+'-'+ageOnsplit[1]+'-'+ageOnsplit[0];
    	var dobsplit = dob.split('-');
    	var dobdate = dobsplit[2]+'-'+dobsplit[1]+'-'+dobsplit[0];	
    	var diff = Math.abs(new Date(ageOndate).getTime() - new Date(dobdate).getTime());
	    var day = 1000 * 60 * 60 * 24;

	    var days = Math.ceil(diff/day);
	    var monthvalue = Math.floor(days/30)
	    $('#dob_month').val(Math.floor(days/30));
	    var yearvalue = Math.floor(monthvalue/12)
	    $('#dob_year').val(Math.floor(monthvalue/12));
	    if(yearvalue > 0){
	    	var yeardays = days - (365 * yearvalue);
	    	let dobmonthfloor = Math.floor(yeardays/30);
	    	$('#dob_month').val(Math.abs(dobmonthfloor))
	    }
    }
}


function getCenters(zone_id,center_id = null)
{
	//alert("Val: "+val);return false;
	if(zone_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>admission/getCenters",
			data:{zone_id:zone_id, center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#center_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#center_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#center_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getCourses(category_id,course_id = null)
{
	$("#batch_id").html("<option value=''>Select</option>");
	//alert("Val: "+val);return false;
	if(category_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>admission/getCourses",
			data:{category_id:category_id, course_id:course_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#course_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getBatch(course_id,center_id,batch_id = null)
{
	// $("#batch_id").html("<option value=''>Select</option>");
	if(center_id == '' || center_id == null){
		center_id = $('#center_id').val();
	}
	$.ajax({
		url:"<?php echo base_url();?>admission/getBatch",
		data:{center_id:center_id, course_id:course_id,batch_id:batch_id},
		dataType: 'json',
		method:'post',
		success: function(res)
		{
			if(res['status']=="success")
			{
				if(res['option'] != '')
				{
					$("#batch_id").html("<option value=''>Select</option>"+res['option']);
				}
				else
				{
					$("#batch_id").html("<option value=''>Select</option>");
				}
			}
			else
			{	
				$("#batch_id").html("<option value=''>Select</option>");
			}
		}
	});
}

function getBatchDetails(batch_id){
	if(batch_id != "")
	{
		$.ajax({
			url:"<?php echo base_url();?>admission/getBatchDetails",
			data:{ batch_id:batch_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status'] == 'success'){
					if(res['result'][0]['start_date'] != null && res['result'][0]['end_date'] != null){
						let startdate = res['result'][0]['start_date'].split('-')
						let enddate = res['result'][0]['end_date'].split('-')
						$('.batchNote').text('Program Start & End Date Between '+startdate[2]+'-'+startdate[1]+'-'+startdate[0]+' - '+enddate[2]+'-'+enddate[1]+'-'+enddate[0])
						$('#batchstartdate').val(startdate[2]+'-'+startdate[1]+'-'+startdate[0]);
						$('#batchenddate').val(enddate[2]+'-'+enddate[1]+'-'+enddate[0]);

					}
					else{
						$('.batchNote').text('');
					}
				}
				else{
					$('.batchNote').text('');
				}
			}
		});
	}

}

function getState(country_id,state_id = null)
{
	console.log(state_id)
	if(country_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>admission/getState",
			data:{country_id:country_id, state_id:state_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#state_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#state_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#state_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getCity(state_id,city_id = null)
{	
	$.ajax({
		url:"<?php echo base_url();?>admission/getCity",
		data:{state_id:state_id,city_id:city_id},
		dataType: 'json',
		method:'post',
		success: function(res)
		{
			if(res['status']=="success")
			{
				if(res['option'] != '')
				{
					$("#city_id").html("<option value=''>Select</option>"+res['option']);
				}
				else
				{
					$("#city_id").html("<option value=''>Select</option>");
				}
			}
			else
			{	
				$("#city_id").html("<option value=''>Select</option>");
			}
		}
	});
}

function img1Change(){
	var act = "<?php echo base_url();?>admission/submitAutorizedInfoForm";
	$("#authorizedInfoform-validate").ajaxSubmit({
		url: act, 
		type: 'post',
		cache: false,
		clearForm: false,
		success: function (response) {
			var res = eval('('+response+')');
			console.log(res)
			if(res['success'] == "1")
			{
				
				$('#img_1').attr("src",'<?php echo FRONT_URL; ?>/images/authorized_person_image/'+res['img1']);
				$('#prev_value1').val(res['img1']);
			}
			else
			{	
				//$("#error_msg").show();
				displayMsg("error",res['msg']);
				return false;
			}
		}
	});
}

function img2Change(){
	var act = "<?php echo base_url();?>admission/submitAutorizedInfoForm";
	$("#authorizedInfoform-validate").ajaxSubmit({
		url: act, 
		type: 'post',
		cache: false,
		clearForm: false,
		success: function (response) {
			var res = eval('('+response+')');
			console.log(res)
			if(res['success'] == "1")
			{
				
				$('#img_2').attr("src",'<?php echo FRONT_URL; ?>/images/authorized_person_image/'+res['img2']);
				$('#prev_value2').val(res['img2']);
			}
			else
			{	
				//$("#error_msg").show();
				displayMsg("error",res['msg']);
				return false;
			}
		}
	});
}

function img3Change(){
	var act = "<?php echo base_url();?>admission/submitAutorizedInfoForm";
	$("#authorizedInfoform-validate").ajaxSubmit({
		url: act, 
		type: 'post',
		cache: false,
		clearForm: false,
		success: function (response) {
			var res = eval('('+response+')');
			console.log(res)
			if(res['success'] == "1")
			{
				
				$('#img_3').attr("src",'<?php echo FRONT_URL; ?>/images/authorized_person_image/'+res['img3']);
				$('#prev_value3').val(res['img3']);
			}
			else
			{	
				//$("#error_msg").show();
				displayMsg("error",res['msg']);
				return false;
			}
		}
	});
}

function doc1Change(){
	var act = "<?php echo base_url();?>admission/submitdocumentInfoForm";
	$("#documentInfoform-validate").ajaxSubmit({
		url: act, 
		type: 'post',
		cache: false,
		clearForm: false,
		success: function (response) {
			var res = eval('('+response+')');
			console.log(res)
			if(res['success'] == "1")
			{
				$('#doc_1').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc1']);
				$('#doc_value1').val(res['doc1']);
			}
			else
			{	
				//$("#error_msg").show();
				displayMsg("error",res['msg']);
				return false;
			}
		}
	});
}

function doc2Change(){
	var act = "<?php echo base_url();?>admission/submitdocumentInfoForm";
	$("#documentInfoform-validate").ajaxSubmit({
		url: act, 
		type: 'post',
		cache: false,
		clearForm: false,
		success: function (response) {
			var res = eval('('+response+')');
			console.log(res)
			if(res['success'] == "1")
			{
				$('#doc_2').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc2']);
				$('#doc_value2').val(res['doc2']);
			}
			else
			{	
				//$("#error_msg").show();
				displayMsg("error",res['msg']);
				return false;
			}
		}
	});
}

function doc3Change(){
	var act = "<?php echo base_url();?>admission/submitdocumentInfoForm";
	$("#documentInfoform-validate").ajaxSubmit({
		url: act, 
		type: 'post',
		cache: false,
		clearForm: false,
		success: function (response) {
			var res = eval('('+response+')');
			console.log(res)
			if(res['success'] == "1")
			{
				$('#doc_3').attr("src",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc3']);
				$('#doc_value3').val(res['doc3']);
			}
			else
			{	
				//$("#error_msg").show();
				displayMsg("error",res['msg']);
				return false;
			}
		}
	});
}

function doc4Change(){
	console.log(1)
	var act = "<?php echo base_url();?>admission/submitdocumentInfoForm";
	$("#documentInfoform-validate").ajaxSubmit({
		url: act, 
		type: 'post',
		cache: false,
		clearForm: false,
		success: function (response) {
			var res = eval('('+response+')');
			console.log(res)
			if(res['success'] == "1")
			{
				$('#doc_4').attr("src",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc4']);
				$('#doc_value4').val(res['doc4']);
			}
			else
			{	
				//$("#error_msg").show();
				displayMsg("error",res['msg']);
				return false;
			}
		}
	});
}

function doc5Change(){
	var act = "<?php echo base_url();?>admission/submitdocumentInfoForm";
	$("#documentInfoform-validate").ajaxSubmit({
		url: act, 
		type: 'post',
		cache: false,
		clearForm: false,
		success: function (response) {
			var res = eval('('+response+')');
			console.log(res)
			if(res['success'] == "1")
			{
				$('#doc_5').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc5']);
				$('#doc_value5').val(res['doc5']);
			}
			else
			{	
				//$("#error_msg").show();
				displayMsg("error",res['msg']);
				return false;
			}
		}
	});
}

function doc6Change(){
	var act = "<?php echo base_url();?>admission/submitdocumentInfoForm";
	$("#documentInfoform-validate").ajaxSubmit({
		url: act, 
		type: 'post',
		cache: false,
		clearForm: false,
		success: function (response) {
			var res = eval('('+response+')');
			console.log(res)
			if(res['success'] == "1")
			{
				$('#doc_6').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc6']);
				$('#doc_value6').val(res['doc6']);
			}
			else
			{	
				//$("#error_msg").show();
				displayMsg("error",res['msg']);
				return false;
			}
		}
	});
}

// function getInquiryDetails(inquiry_master_id){
// 	if(inquiry_master_id != "" )
// 	{
// 		$.ajax({
// 			url:"<?php echo base_url();?>admission/getInquiryDetails",
// 			data:{inquiry_master_id:inquiry_master_id},
// 			dataType: 'json',
// 			method:'post',
// 			success: function(res)
// 			{
// 				if(res['status']=="success")
// 				{
// 					$('#firstname').val(res['result'][0]['student_first_name']);
// 					$('#lastname').val(res['result'][0]['student_last_name']);
// 					$('#academic_year_id').val(res['result'][0]['academic_year_master_id'])
// 					$('#category_id').val(res['result'][0]['category_id'])
// 					$('#course_id').val(res['result'][0]['course_id'])
// 					getCourses(res['result'][0]['category_id'],res['result'][0]['course_id'])
// 					var dobsplit = res['result'][0]['student_dob'].split('-');
// 					var dob = dobsplit[2]+'-'+dobsplit[1]+'-'+dobsplit[0];
// 					$('#dob').val(dob)
// 					calcDate($('#age_on').val(),dob)
// 					$('#father_name').val(res['result'][0]['father_name'])
// 					$('#mother_name').val(res['result'][0]['mother_name'])
// 					$('#father_prof').val(res['result'][0]['father_profession'])
// 					$('#mother_prof').val(res['result'][0]['mother_profession'])
// 					$('#present_address').val(res['result'][0]['present_address'])
// 					$("#country_id").select2().select2("val", res['result'][0]['country_id']);
// 					getState(res['result'][0]['country_id'], res['result'][0]['state_id']);
// 					$("#s2id_state_id a .select2-chosen").text(res['getStateName'][0]['state_name']);
// 					getCity(res['result'][0]['state_id'],res['result'][0]['city_id']);
// 					$("#s2id_city_id a .select2-chosen").text(res['getCityName'][0]['city_name']);
// 					$('#pincode').val(res['result'][0]['pincode'])
// 					$('#father_email_id').val(res['result'][0]['father_email_id'])
// 					$('#mother_email_id').val(res['result'][0]['mother_email_id'])
// 					$('#father_mobile_contact_no').val(res['result'][0]['father_mobile_contact_no'])
// 					$('#mother_mobile_contact_no').val(res['result'][0]['mother_mobile_contact_no'])
// 				}
// 			}
// 		});
// 	}
// }
function getFees(fees_level_id,fees_id = null)
{
	//alert("Val: "+val);return false;
	var category_id = $('#category_id').val()
	var course_id = $('#course_id').val()
	var center_id = $('#center_id').val()
	var zone_id = $('#zone_id').val()
	if(center_id != "" && zone_id != '' && fees_level_id != '')
	{
		$.ajax({
			url:"<?php echo base_url();?>admission/getFees",
			data:{center_id:center_id, zone_id:zone_id,feeslevelid:fees_level_id,categoryid:category_id,courseid:course_id,feesselectiontype:'Class'},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				console.log(res)
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#fees_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#fees_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#fees_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getFessDetails(fees_id){
	if(fees_id != "" )
	{
		$(".c_table").children().remove();
		$.ajax({
			url:"<?php echo base_url();?>admission/getFessDetails",
			data:{ fees_id:fees_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				console.log(res)
				if(res['status']=="success")
				{
					// $('#fees_total_amount').val(res['result'][0]['amount'])
					// $('#fees_remaining_amount').val(res['result'][0]['amount'])
					// $('#total_amount').val(res['result'][0]['amount'])
					$('#max_installment').val(res['result'][0]['installment_no'])
					let total_amount = 0
			    	let t_amount = 0;
			    	let total_sub_amount = 0
			    	let total_gst_amount = 0
			    	let total_mandatory_amount = 0
			    	let total_discount_amount = 0





					$('.component_show').show()
					var size = res['result'].length
					$('#component_size').val(size)
					for(let i=0;i<size;i++){
						// let installment_amount = ($('#fees_total_amount').val()/installmentNo).toFixed(2)
						$(".c_table").append("<tr>");
						$(".c_table").append('<td>'+Number(i+1)+'</td>');
						$(".c_table").append('<td>'+res['result'][i]['fees_component_master_name']+'</td>');
						$(".c_table").append('<td>'+res['result'][i]['component_fees']+'</td>');
						$(".c_table").append('<td><div class="form-group"><input type="text" class="form-control discount_amount"  id="discountamount_'+i+'"name="discount_amount[]" readonly="readonly"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="text" class="form-control gst_amount"  id="gstamount_'+i+'"name="gst_amount[]" readonly="readonly"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="text" class="form-control sub_amount"  id="subamount_'+i+'"name="sub_amount[]" readonly="readonly"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control component_gst_fees"  id="componentgstfees_'+i+'"name="component_gst_fees[]"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control is_discountable"  id="isdiscountable_'+i+'"name="is_discountable[]"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control is_gst"  id="isgst_'+i+'"name="is_gst[]"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control fees"  id="fees_'+i+'"name="fees[]"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control tax"  id="tax_'+i+'"name="tax[]"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control component_new_fees"  id="componentnewfees_'+i+'"name="component_new_fees[]"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control component_fees"  id="componentfees_'+i+'"name="component_fees[]"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control original_gst_fees"  id="originalgstfees_'+i+'"name="original_gst_fees[]"></div></td>');
						$(".c_table").append("</tr>");
						$(".c_table").append("</br>");
						$('#isdiscountable_'+i).val(res['result'][i]['is_discountable'])
						$('#isgst_'+i).val(res['result'][i]['is_gst'])
						$("#discountamount_"+i).val(0);
						$('#fees_'+i).val(res['result'][i]['component_fees'])
						$('#tax_'+i).val(res['result'][i]['tax'])
						if(res['result'][i]['is_gst'] == 'Yes'){
							let gst_amount = ((parseFloat(res['result'][i]['component_fees']) * parseFloat(res['result'][i]['tax']))/100).toFixed(2);
							$('#gstamount_'+i).val(gst_amount)
							let componentgstfees = (parseFloat(res['result'][i]['component_fees']) - parseFloat(gst_amount)).toFixed(2); //gst added amount
							$('#componentgstfees_'+i).val(componentgstfees)
							$('#originalgstfees_'+i).val(gst_amount)
							$('#subamount_'+i).val(componentgstfees)
						}
						else{
							$('#componentgstfees_'+i).val(res['result'][i]['component_fees'])
							var componentgstfees = $('#componentgstfees_'+i).val()
							$('#subamount_'+i).val(componentgstfees)
							$('#gstamount_'+i).val(0)
						}
						if(res['result'][i]['is_mandatory'] == 'Yes'){
			    			total_mandatory_amount = total_mandatory_amount + parseFloat(res['result'][i]['component_fees'])
			    		}
			    		$('#componentfees_'+i).val(res['result'][i]['component_fees'])
			    		var componentgstfees = $('#componentgstfees_'+i).val()
			    		total_amount = total_amount + parseFloat(componentgstfees)
			    		total_gst_amount = total_gst_amount + parseFloat(res['result'][i]['gst_amount'])
			    		$('#total_gst_amount').val(total_gst_amount)
			    		t_amount = t_amount + parseFloat(res['result'][i]['component_fees'])//total amount column display
			    		let subamountval = $('#subamount_'+i).val()
			    		total_sub_amount = total_sub_amount + parseFloat(subamountval)
			    		$('#total_amount').val((total_sub_amount).toFixed(2))
						// $('.instalment_amount').val(installment_amount)
						// $('.instalment_collected_amount').val(0)
						// $('.instalment_remaining_amount').val(installment_amount)
					}

					$(".c_table").append("<tr>");
					$(".c_table").append('<td><b>Total</b></td>');
					$(".c_table").append('<td></td>');
					$(".c_table").append('<td class="t_amount"></td>');
					$(".c_table").append('<td></td>');
					$(".c_table").append('<td></td>');
					$(".c_table").append('<td class="total_sub_amount"></td>');
					$(".c_table").append("</tr>");


			    	$('.total_sub_amount').text((total_sub_amount).toFixed(2))

			    	$('.t_amount').text((t_amount).toFixed(2))
					$('#total_discount_amount').val(total_discount_amount)
					console.log(total_discount_amount)
			    	$('#mandatory_component_amount').val((total_mandatory_amount).toFixed(2))
			    	$('#fees_total_amount').val((t_amount).toFixed(2));
			    	$('#fees_amount_collected').val(total_sub_amount)
			    	$('#fees_remaining_amount').val(0)
				}
				else
				{	
					$('#fees_total_amount').val(0)
					$('#fees_remaining_amount').val(0)
					$('#total_amount').val(0)
					$('#max_installment').val(0)
					$(".c_table").children().remove();
				}
			}
		});
	}
	else{
		$(".c_table").children().remove();
	}
}
$('#sibling1_enrollment').keyup(function(){
	var sibling1_enrollment = $(this).val()
	$.ajax({
			url:"<?php echo base_url();?>admission/getSiblingName",
			data:{enrollment_no:sibling1_enrollment},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['result']!=false)
				{
					$('#sibling1_id').val(res['result'][0]['student_first_name']+' '+res['result'][0]['student_last_name'])
					$('#sibling1_id_hidden').val(res['result'][0]['student_id'])
				}
				else{
					$('#sibling1_id').val('')
					$('#sibling1_id_hidden').val('')
				}
			}
		});
})

$('#sibling2_enrollment').keyup(function(){
	var sibling2_enrollment = $(this).val()
	$.ajax({
			url:"<?php echo base_url();?>admission/getSiblingName",
			data:{enrollment_no:sibling2_enrollment},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['result']!=false)
				{
					$('#sibling2_id').val(res['result'][0]['student_first_name']+' '+res['result'][0]['student_last_name'])
					$('#sibling2_id_hidden').val(res['result'][0]['student_id'])
				}
				else{
					$('#sibling2_id').val('')
					$('#sibling2_id_hidden').val('')
				}
			}
		});
})

var vRules = {
	inquiry_master_id : {required:true},
	admission_date :  {required:true},
	academic_year_id :  {required:true},
	category_id:{required:true},
	course_id:{required:true},
	zone_id:{required:true},
	center_id :  {required:true},
	batch_id :  {required:true},
	programme_start_date :  {required:true},
	programme_end_date :  {required:true},
	student_first_name :  {required:true},
	student_last_name :  {required:true},
	dob :  {required:true},
	nationality :  {required:true},
	religion :  {required:true},
	mother_tongue :  {required:true},
	father_name : {required:true},
	mother_name : {required:true},
	father_prof : {required:true},	
	mother_prof : {required:true},	
	father_languages : {required:true},
	mother_languages : {required:true},
	father_nationality : {required:true},
	mother_nationality : {required:true},
	present_address : {required:true},
	address1 : {required:true},
	address2 : {required:true},
	address3 : {required:true},
	country_id : {required:true},
	state_id : {required:true},
	city_id : {required:true},
	pincode : {required:true},
	father_email_id : {required:true},
	mother_email_id : {required:true},
	father_mobile_contact_no : {required:true},
	mother_mobile_contact_no : {required:true},
	emergency_contact_name : {required:true},
	emergency_contact_relationship : {required:true},
	emergency_contact_mobile_no : {required:true},
	auth_person1_to_collect : {required:true},
	auth_person1_to_collect_relation : {required:true},
	prev_value1  : {required:true},
	doc_value1 : {required:true},
	doc_value2 : {required:true},
	doc_value3 : {required:true},
	doc_value4 : {required:true},
	doc_value5 : {required:true},
	doc_value6 : {required:true},
	fees_level_id : {required:true},
	fees_id : {required:true},
	fees_remarkl : {required:true},
	fees_remark : {required:true},
	
};
var vMessages = {
	inquiry_master_id:{required:"Please select enquiry."},
	admission_date:{required:"Please select admission date."},
	academic_year_id:{required:"Please select academic year."},
	category_id:{required:"Please select category."},
	course_id:{required:"Please select course."},
	zone_id:{required:"Please select zone."},
	center_id:{required:"Please select center."},
	batch_id:{required:"Please select batch."},
	programme_start_date:{required:"Please select program start date."},
	programme_end_date:{required:"Please select program end date."},
	student_first_name:{required:"Please enter first name."},
	student_last_name:{required:"Please enter last name."},
	dob:{required:"Please select dob."},
	nationality:{required:"Please enter nationality."},
	religion:{required:"Please enter religion."},
	mother_tongue:{required:"Please enter mother tongue."},
	father_name:{required:"Please enter father name."},
	mother_name:{required:"Please enter mother name."},
	father_prof:{required:"Please enter father profession."},
	mother_prof:{required:"Please enter mother profession."},
	father_languages:{required:"Please enter father languages."},
	mother_languages:{required:"Please enter mother languages."},
	father_nationality:{required:"Please enter father nationality."},
	mother_nationality:{required:"Please enter mother nationality."},
	present_address:{required:"Please enter present address."},
	address1:{required:"Please enter street/road/lane."},
	address2:{required:"Please enter landmark."},
	address3:{required:"Please enter area.locality/center."},
	country_id:{required:"Please select country."},
	state_id:{required:"Please select state."},
	city_id:{required:"Please select city."},
	pincode:{required:"Please enter pincode."},
	father_email_id:{required:"Please enter father email id."},
	mother_email_id:{required:"Please enter mother email id."},
	father_mobile_contact_no:{required:"Please enter father mobile no."},
	mother_mobile_contact_no:{required:"Please enter mother mobile no."},
	emergency_contact_name:{required:"Please enter name."},
	emergency_contact_relationship:{required:"Please enter relationship."},
	emergency_contact_mobile_no:{required:"Please enter mobile no."},
	auth_person1_to_collect:{required:"Please enter name."},
	auth_person1_to_collect:{required:"Please enter relation."},
	prev_value1 : {required:"Please upload profile pic."},
	doc_value1 : {required:"Please upload immunization certificate."},
	doc_value2 : {required:"Please upload birth certificate."},
	doc_value3 : {required:"Please upload child photo."},
	doc_value4 : {required:"Please upload family photo."},
	doc_value5 : {required:"Please upload address proof."},
	doc_value6 : {required:"Please upload profile form."},
	fees_level_id : {required:"Please select fees level."},
	fees_id : {required:"Please select fees."},
	fees_remarkl : {required:"Please enter fees remark."},
	fees_remark : {required:"Please enter fees remark."},
};

$("#childInfoform-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>admission/submitForm";
		$("#childInfoform-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				console.log(res)
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						$('#childInfoTab').removeClass('active');
						$('#childInfoTab_link').removeClass('active');
						$('#familyInfoTab').addClass('active');
						$('#familyInfoTab_link').addClass('active');
					},2000);
				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

$("#familyInfoform-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>admission/submitFamilyInfoForm";
		$("#familyInfoform-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				console.log(res)
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						$('#childInfoTab').removeClass('active');
						$('#childInfoTab_link').removeClass('active');
						$('#familyInfoTab').removeClass('active');
						$('#familyInfoTab_link').removeClass('active');
						$('#contactInfoTab').addClass('active');
						$('#contactInfoTab_link').addClass('active');
					},2000);
				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

$("#contactInfoform-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>admission/submitContactInfoForm";
		$("#contactInfoform-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				console.log(res)
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						$('#childInfoTab').removeClass('active');
						$('#childInfoTab_link').removeClass('active');
						$('#familyInfoTab').removeClass('active');
						$('#familyInfoTab_link').removeClass('active');
						$('#contactInfoTab').removeClass('active');
						$('#contactInfoTab_link').removeClass('active');
						$('#authorizedPersonInfoTab').addClass('active');
						$('#authorizedPersonInfoTab_link').addClass('active')
					},2000);
				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

$("#authorizedInfoform-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>admission/submitAutorizedInfoForm";
		$("#authorizedInfoform-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				console.log(res)
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						$('#childInfoTab').removeClass('active');
						$('#childInfoTab_link').removeClass('active');
						$('#familyInfoTab').removeClass('active');
						$('#familyInfoTab_link').removeClass('active');
						$('#contactInfoTab').removeClass('active');
						$('#contactInfoTab_link').removeClass('active');
						$('#authorizedPersonInfoTab').removeClass('active');
						$('#authorizedPersonInfoTab_link').removeClass('active')
						$('#documentInfoTab').addClass('active');
						$('#documentInfoTab_link').addClass('active')
					},2000);
				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

$("#documentInfoform-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>admission/submitdocumentInfoForm";
		$("#documentInfoform-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					$('#doc1').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc1']);
					$('#doc_value1').val(res['doc1']);
					$('#doc2').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc2']);
					$('#doc_value2').val(res['doc2']);
					$('#doc_3').attr("src",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc3']);
					$('#doc_value3').val(res['doc3']);
					$('#doc_4').attr("src",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc4']);
					$('#doc_value4').val(res['doc4']);
					$('#doc_5').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc5']);
					$('#doc_value5').val(res['doc5']);
					$('#doc_6').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+res['doc6']);
					$('#doc_value6').val(res['doc6']);
					displayMsg("success",res['msg']);
					setTimeout(function(){
						$('#childInfoTab').removeClass('active');
						$('#childInfoTab_link').removeClass('active');
						$('#familyInfoTab').removeClass('active');
						$('#familyInfoTab_link').removeClass('active');
						$('#contactInfoTab').removeClass('active');
						$('#contactInfoTab_link').removeClass('active');
						$('#authorizedPersonInfoTab').removeClass('active');
						$('#authorizedPersonInfoTab_link').removeClass('active')
						$('#documentInfoTab').removeClass('active');
						$('#documentInfoTab_link').removeClass('active')
						$('#feesInfoTab').addClass('active');
						$('#feesInfoTab_link').addClass('active');
					},2000);
				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

$("#feesInfoform-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>admission/submitfeesInfoForm";
		$("#feesInfoform-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				console.log(res)
				if(res['success'] == "1")
				{
					if(res['flag'] == 0){
						var student_id = btoa(res['student_id'])
						window.location = "<?php echo base_url();?>admission/groupfees?text="+student_id;
					}
					else{
						displayMsg("success",res['msg']);
						setTimeout(function(){
							let admission_fees_id = res['payfeesdataresult']
							$.ajax({
								url:"<?php echo base_url();?>admission/getPayemtDetails",
								data:{admission_fees_id:admission_fees_id},
								dataType: 'json',
								method:'post',
								success: function(data)
								{
									if(data['status_code'] == 200){
										window.open(data['body'], '_blank');
										window.location = "<?php echo base_url();?>admission";
									}
								}
							});
						},2000);
					}
				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});
<?php
	if(isset($_SESSION['student_id']))
	{
		?>
		var student_id = "<?php echo $_SESSION['student_id'];?>"
		$.ajax({
			url: "<?php echo base_url();?>admission/getchildInfo",
			data:{"student_id":student_id},
			async: false,
			type: "POST",
			success: function(response){
				var data2 = eval('('+response+')');
				console.log(data2)
				$('#inquiry_master_id').val(data2['result'][0]['inquiry_master_id']);
				var admissiondatesplit = data2['otherresult'][0]['admission_date'].split('-');
				var admissiondate = admissiondatesplit[2]+'-'+admissiondatesplit[1]+'-'+admissiondatesplit[0];
				$('#admission_date').val(admissiondate);
				$('#academic_year_id').val(data2['otherresult'][0]['academic_year_id']);
				$('#enrollment_no').val(data2['result'][0]['enrollment_no']);
				$('#zone_id').val(data2['result'][0]['zone_id']);
				$('#center_id').val(data2['result'][0]['center_id']);
				$('#category_id').val(data2['otherresult'][0]['category_id']);
				$('#course_id').val(data2['otherresult'][0]['course_id']);
				$('#batch_id').val(data2['otherresult'][0]['batch_id']);
				
				var startdatesplit = data2['otherresult'][0]['programme_start_date'].split('-');
				var startdate = startdatesplit[2]+'-'+startdatesplit[1]+'-'+startdatesplit[0];
				$('#programme_start_date').val(startdate);
				var enddatesplit = data2['otherresult'][0]['programme_end_date'].split('-');
				var enddate = enddatesplit[2]+'-'+enddatesplit[1]+'-'+enddatesplit[0];
				$('#programme_end_date').val(enddate);
				$('#firstname').val(data2['result'][0]['student_first_name']);
				$('#lastname').val(data2['result'][0]['student_last_name']);			
				var dobsplit = data2['result'][0]['dob'].split('-');
				var dob = dobsplit[2]+'-'+dobsplit[1]+'-'+dobsplit[0];	
				$('#dob').val(dob);			
				$('#age_on').val(startdate);			
				$('#dob_year').val(data2['result'][0]['dob_year']);		
				$('#dob_month').val(data2['result'][0]['dob_month']);		
				$('#nationality').val(data2['result'][0]['nationality']);		
				$('#religion').val(data2['result'][0]['religion']);		
				$('#mother_tongue').val(data2['result'][0]['mother_tongue']);		
				$('#other_languages').val(data2['result'][0]['other_languages']);	
				if(data2['result'][0]['has_attended_preschool_before'] == 'Yes'){
					$('.preschoolNameDiv').show();
				}
				else{
					$('.preschoolNameDiv').hide();
				}	
				$('#preschool_name').val(data2['result'][0]['preschool_name']);
				$('#student_id').val(data2['result'][0]['student_id']);
				getCenters(data2['result'][0]['zone_id'], data2['result'][0]['center_id']);
				getCourses(data2['otherresult'][0]['category_id'], data2['otherresult'][0]['course_id']);
				var course_id = data2['otherresult'][0]['course_id']
				var center_id = data2['result'][0]['center_id']
				var batch_id = data2['otherresult'][0]['batch_id']
				getBatchDetails(batch_id)
				if(course_id != "")
				{
					$.ajax({
						url:"<?php echo base_url();?>admission/getBatch",
						data:{center_id:center_id, course_id:course_id,batch_id:batch_id},
						dataType: 'json',
						method:'post',
						success: function(res)
						{
							if(res['status']=="success")
							{
								if(res['option'] != '')
								{
									$("#batch_id").html("<option value=''>Select</option>"+res['option']);
								}
								else
								{
									$("#batch_id").html("<option value=''>Select</option>");
								}
							}
							else
							{	
								$("#batch_id").html("<option value=''>Select</option>");
							}
						}
					});
				}
				if(data2['result'][0]['father_name'] != ' '){
					$('#father_name').val(data2['result'][0]['father_name'])
				}
				if(data2['result'][0]['mother_name'] != null){
					$('#mother_name').val(data2['result'][0]['mother_name'])
				}
				$('#father_prof').val(data2['result'][0]['father_profession'])
				$('#mother_prof').val(data2['result'][0]['mother_profession'])
				$('#father_languages').val(data2['result'][0]['father_languages'])
				$('#mother_languages').val(data2['result'][0]['mother_languages'])
				$('#father_nationality').val(data2['result'][0]['father_nationality'])
				$('#mother_nationality').val(data2['result'][0]['mother_nationality'])
				if(data2['result'][0]['sibling1_id'] != 0){
					$('#sibling1_id').val(data2['result'][0]['sibling1_id'])
				}
				if(data2['result'][0]['sibling2_id'] != 0){
					$('#sibling2_id').val(data2['result'][0]['sibling2_id'])
				}
				if(data2['result'][0]['present_address'] !=  null){
					$('#present_address').val(data2['result'][0]['present_address'])
				}
				if(data2['result'][0]['address1'] !=  null){
					$('#address1').val(data2['result'][0]['address1'])
				}
				if(data2['result'][0]['address2'] !=  null){
					$('#address2').val(data2['result'][0]['address2'])
				}
				if(data2['result'][0]['address3'] !=  null){
					$('#address3').val(data2['result'][0]['address3'])
				}
				if(data2['result'][0]['country_id'] != null){
					$("#country_id").select2().select2("val", data2['result'][0]['country_id']);
				}
				getState(data2['result'][0]['country_id'], data2['result'][0]['state_id']);
				if(data2['getStateName'] != false){
					$("#s2id_state_id a .select2-chosen").text(data2['getStateName'][0]['state_name']);
				}
				getCity(data2['result'][0]['state_id'],data2['result'][0]['city_id']);
				if(data2['getCityName'] != false){
					$("#s2id_city_id a .select2-chosen").text(data2['getCityName'][0]['city_name']);
				}
				if(data2['result'][0]['pincode'] != null){
					$('#pincode').val(data2['result'][0]['pincode'])
				}
				if(data2['result'][0]['father_email_id'] != null){
					$('#father_email_id').val(data2['result'][0]['father_email_id'])
				}
				if(data2['result'][0]['mother_email_id'] != null){
					$('#mother_email_id').val(data2['result'][0]['mother_email_id'])
				}
				if(data2['result'][0]['father_mobile_contact_no'] != null){
					$('#father_mobile_contact_no').val(data2['result'][0]['father_mobile_contact_no'])
				}
				if(data2['result'][0]['mother_mobile_contact_no'] != null){
					$('#mother_mobile_contact_no').val(data2['result'][0]['father_mobile_contact_no'])
				}
				$('#father_home_contact_no').val(data2['result'][0]['father_home_contact_no'])
				$('#mother_home_contact_no').val(data2['result'][0]['mother_home_contact_no'])
				$('#father_office_contact_no').val(data2['result'][0]['father_office_contact_no'])
				$('#mother_office_contact_no').val(data2['result'][0]['mother_office_contact_no'])
				$('#emergency_contact_name').val(data2['result'][0]['emergency_contact_name'])
				$('#emergency_contact_mobile_no').val(data2['result'][0]['emergency_contact_mobile_no'])
				$('#emergency_contact_relationship').val(data2['result'][0]['emergency_contact_relationship'])
				$('#emergency_contact_tel_no').val(data2['result'][0]['emergency_contact_tel_no'])
				$('#auth_person1_to_collect').val(data2['result'][0]['auth_person1_to_collect'])
				$('#auth_person1_to_collect_relation').val(data2['result'][0]['auth_person1_to_collect_relation'])
				$('#auth_person1_to_collect_gender').val(data2['result'][0]['auth_person1_to_collect_gender'])
				$('#img_1').attr("src",'<?php echo FRONT_URL; ?>/images/authorized_person_image/'+data2['result'][0]['authperson1_img']);
				$('#prev_value1').val(data2['result'][0]['authperson1_img']);
				$('#auth_person2_to_collect').val(data2['result'][0]['auth_person2_to_collect'])
				$('#auth_person2_to_collect_relation').val(data2['result'][0]['auth_person2_to_collect_relation'])
				$('#auth_person2_to_collect_gender').val(data2['result'][0]['auth_person2_to_collect_gender'])
				$('#img_2').attr("src",'<?php echo FRONT_URL; ?>/images/authorized_person_image/'+data2['result'][0]['authperson2_img']);
				$('#prev_value2').val(data2['result'][0]['authperson1_img']);
				$('#auth_person3_to_collect').val(data2['result'][0]['auth_person3_to_collect'])
				$('#auth_person3_to_collect_relation').val(data2['result'][0]['auth_person3_to_collect_relation'])
				$('#auth_person3_to_collect_gender').val(data2['result'][0]['auth_person3_to_collect_gender'])
				$('#img_3').attr("src",'<?php echo FRONT_URL; ?>/images/authorized_person_image/'+data2['result'][0]['authperson3_img']);
				$('#prev_value3').val(data2['result'][0]['authperson1_img']);
				$('#doc_1').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+data2['result'][0]['duly_filled_admission_form']);
				$('#doc_value1').val(data2['result'][0]['duly_filled_admission_form']);
				$('#doc_2').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+data2['result'][0]['birth_certificate']);
				$('#doc_value2').val(data2['result'][0]['birth_certificate']);
				$('#doc_3').attr("src",'<?php echo FRONT_URL; ?>/images/admission_documents/'+data2['result'][0]['profile_pic']);
				$('#doc_value3').val(data2['result'][0]['profile_pic']);
				$('#doc_4').attr("src",'<?php echo FRONT_URL; ?>/images/admission_documents/'+data2['result'][0]['family_photo']);
				$('#doc_value4').val(data2['result'][0]['family_photo']);
				$('#doc_5').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+data2['result'][0]['address_proof']);
				$('#doc_value5').val(data2['result'][0]['address_proof']);
				$('#doc_6').attr("href",'<?php echo FRONT_URL; ?>/images/admission_documents/'+data2['result'][0]['duly_filled_child_profile_form']);
				$('#doc_value6').val(data2['result'][0]['duly_filled_child_profile_form']);

				installmentprocess()
			}
		});
		<?php
	}
	?>
document.title = "AddEdit - Admission";

 
</script>					
