<?php  
//session_start();
//print_r($_SESSION["webadmin"]);
?>
<!-- start: Content -->
<div id="content" class="content-wrapper">
    <div class="page-title">
      <div>
        <h1>Permissions</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>permission">Permissions</a></li>
        </ul>
      </div>
    </div>        	      	
    <div class="card">
      <div class="page-title-border">
      <div class="col-sm-12 col-md-6">
        <div class="box-content form-inline">
            <div class="dataTables_filter searchFilterClass form-group">
                <label for="firstname" class="control-label">Name</label>
                <input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
            </div>
            <div class="control-group clearFilter form-group" style="margin-left:5px;">
                <div class="controls">
                    <a href="#" href="#" onclick="clearSearchFilters();"><button class="btn btn-primary">Clear Search</button></a>
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
      </div>
      <div class="col-sm-12 col-md-6 right-button-top">
         <?php 
            if($this->privilegeduser->hasPrivilege("PermissionAddEdit")){
        ?>
        <p><a href="<?php echo base_url();?>permission/addEdit" class="btn btn-primary icon-btn"><i class="fa fa-plus"></i>Add Permission</a></p>
        <?php }?>
        <div class="clearfix"></div>
      </div>
      </div>
      <div class="clearfix"></div>
      <div class="card-body" style="clear: both">
        <div class="box-content">
            <div class="table-responsive scroll-table">
                <table class="dynamicTable display table table-bordered non-bootstrap">
                    <thead>
                        <tr>
                            <th>Permissions</th>
                            <!--<th>Actions</th>-->
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    </tfoot>
                </table> 
            </div>           
        </div> 
      </div>
    </div>        
    
</div><!-- end: Content -->			
<script>
$( document ).ready(function() {
	
});

	function deleteData(id)
	{
    	var r=confirm("Are you sure you want to delete this record?");
    	if (r==true)
   		{
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/permission/delRecord/"+id,
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Deleted!");
						setTimeout("location.reload(true);",1000);
						
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});
    	}
    }
	document.title = "Permissions";
</script>