<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Group Master</h1>            
          </div>
          <div>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
              <li><a href="<?php echo base_url();?>groupmaster">Group Master</a></li>
            </ul>
          </div>
    </div>
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
						<input type="hidden" id="group_master_id" name="group_master_id" value="<?php if(!empty($details[0]->group_master_id)){echo $details[0]->group_master_id;}?>" />

						<?php if(empty($details[0]->group_master_id)){?>
						<div class="control-group form-group">
							<label class="control-label" for="course_id">Course</label>
							<div class="controls">
								<select id="course_id" name="course_id" class="form-control"  onchange="putGroupName(this.value);">
									<option value="">Select Course</option>
									<?php 
										if(isset($courses) && !empty($courses)){
											foreach($courses as $cdrow){
												$sel = ($cdrow->course_name == $details[0]->course_name) ? 'selected="selected"' : '';
									?>
										<option value="<?php echo $cdrow->course_name;?>" <?php echo $sel; ?>><?php echo $cdrow->course_name;?></option>
									<?php }}?>
								</select>
							</div>
						</div>
						<?php }?>

						<div class="control-group form-group">
							<label class="control-label"><span>Group Name*</span></label>
							<div class="controls">
								<input type="text" class="form-control required" placeholder="Enter Name" id="group_master_name" name="group_master_name" value="<?php if(!empty($details[0]->group_master_name)){echo $details[0]->group_master_name;}?>"  maxlength="200" >
							</div>
						</div>
						

						<div class="control-group form-group">
							<label class="control-label" for="status">Status*</label>
							<div class="controls">
								<select name="status" id="status" class="form-control">
									<option value="Active" <?php if(!empty($details[0]->status) && $details[0]->status == "Active"){?> selected <?php }?>>Active</option>
									<option value="In-active" <?php if(!empty($details[0]->status) && $details[0]->status == "In-active"){?> selected <?php }?>>In-active</option>
								</select>
							</div>
						</div>
						<div style="clear:both; margin-bottom: 2%;"></div>
						
						<div class="form-actions form-group">
							<button type="submit" class="btn btn-primary">Submit</button>
							<a href="<?php echo base_url();?>groupmaster" class="btn btn-primary">Cancel</a>
						</div>
					</form>
                </div>
            <div class="clearfix"></div>
            </div>
         </div>
    </div>        
	</div><!-- end: Content -->								
<script>

var vRules = {
	group_master_name:{required:true, alphanumericwithspace:true}
};
var vMessages = {
	group_master_name:{required:"Please enter group name."}
	
};

function putGroupName(groupname){
	$('#group_master_name').val(groupname)
}

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>groupmaster/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>groupmaster";
					},2000);

				}
				else
				{	
					displayMsg("error",res['msg']);
					return false;
				}
			},
			error:function(){
				$("#msg_display").html("");
				$("#display_msg").html("");
				displayMsg("error",res['msg']);
			}
		});
	}
});


document.title = "AddEdit - Group Master";

 
</script>					
