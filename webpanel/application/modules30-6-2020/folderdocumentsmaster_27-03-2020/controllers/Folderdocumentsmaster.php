<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Folderdocumentsmaster extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('folderdocumentsmastermodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('folderdocumentsmaster/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['document_folders'] = $this->folderdocumentsmastermodel->getDropdown("tbl_document_folders","document_folder_id,folder_name");
			
			$result['details'] = $this->folderdocumentsmastermodel->getFormdata($record_id);
			/*$result['videos'] = $this->folderdocumentsmastermodel->getdata("tbl_timetable_videos", "timetable_id='".$record_id."' ");
			$result['documents'] = $this->folderdocumentsmastermodel->getdata("tbl_timetable_documents", "timetable_id='".$record_id."' ");
			*/
			//echo "<pre>";print_r($result['videos']);exit;
			
			$this->load->view('template/header.php');
			$this->load->view('folderdocumentsmaster/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function getCourses(){
		$result = $this->folderdocumentsmastermodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['course_id']) && !empty($_REQUEST['course_id'])){
			$course_id = $_REQUEST['course_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getThemes(){
		$result = $this->folderdocumentsmastermodel->getOptions("tbl_themes",$_REQUEST['course_id'],"course_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$theme_id = '';
		
		if(isset($_REQUEST['theme_id']) && !empty($_REQUEST['theme_id'])){
			$theme_id = $_REQUEST['theme_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->theme_id == $theme_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->theme_id.'" '.$sel.' >'.$result[$i]->theme_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getWeeks(){
		$result = $this->folderdocumentsmastermodel->getOptions("tbl_weeks",$_REQUEST['theme_id'],"theme_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$week_id = '';
		
		if(isset($_REQUEST['week_id']) && !empty($_REQUEST['week_id'])){
			$week_id = $_REQUEST['week_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->week_id == $week_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->week_id.'" '.$sel.' >'.$result[$i]->week_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function submitForm()
	{ 
		// echo "<pre>";print_r($_FILES);exit;
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			
			//exit;
			if (!empty($_POST['folder_document_id'])) {
				$data_array = array();			
				$folder_document_id = $_POST['folder_document_id'];
				$data_array['document_title'] = (!empty($_POST['document_title'])) ? $_POST['document_title'] : '';
				
				if(!empty($_POST['doc_type']) && $_POST['doc_type'] == 'video'){
					$data_array['document_field_value'] = (!empty($_POST['video_url'])) ? $_POST['video_url'] : '';
				}else if(!empty($_POST['doc_type']) && $_POST['doc_type'] == 'doc'){
					
					$doc_file_value = "";
					if(isset($_FILES) && isset($_FILES["doc_file"]["name"]))
					{
						$config['upload_path'] = DOC_ROOT_FRONT."/images/folder_documents/";
						$config['max_size']    = '1000000000';
						$config['allowed_types'] = '*';
						//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
						$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["doc_file"]["name"]);
						
						$this->load->library('upload', $config);
						if (!$this->upload->do_upload("doc_file"))
						{
							$image_error = array('error' => $this->upload->display_errors());
							echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
							exit;
						}
						else
						{
							$image_data = array('upload_data' => $this->upload->data());
							$doc_file_value = $image_data['upload_data']['file_name'];
						}
						
						$image = $this->folderdocumentsmastermodel->getFormdata($_POST['folder_document_id']);
						if(is_array($image) && !empty($image[0]->document_field_value) && file_exists(DOC_ROOT_FRONT."/images/folder_documents/".$image[0]->document_field_value))
						{
							@unlink(DOC_ROOT_FRONT."/images/folder_documents/".$image[0]->document_field_value);
						}
						
					}else{
						$doc_file_value = $_POST['prev_value'];
					}
					
					$data_array['document_field_value'] = $doc_file_value;
					
				}else if(!empty($_POST['doc_type']) && $_POST['doc_type'] == 'image'){
					
					$doc_file_value = "";
					if(isset($_FILES) && isset($_FILES["image_file"]["name"]))
					{
						$config['upload_path'] = DOC_ROOT_FRONT."/images/folder_documents/";
						$config['max_size']    = '1000000000';
						$config['allowed_types'] = '*';
						//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
						$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["image_file"]["name"]);
						
						$this->load->library('upload', $config);
						if (!$this->upload->do_upload("image_file"))
						{
							$image_error = array('error' => $this->upload->display_errors());
							echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
							exit;
						}
						else
						{
							$image_data = array('upload_data' => $this->upload->data());
							$doc_file_value = $image_data['upload_data']['file_name'];
						}
						
						$image = $this->folderdocumentsmastermodel->getFormdata($_POST['folder_document_id']);
						if(is_array($image) && !empty($image[0]->document_field_value) && file_exists(DOC_ROOT_FRONT."/images/folder_documents/".$image[0]->document_field_value))
						{
							@unlink(DOC_ROOT_FRONT."/images/folder_documents/".$image[0]->document_field_value);
						}
						
					}else{
						$doc_file_value = $_POST['prev_value'];
					}
					
					$data_array['document_field_value'] = $doc_file_value;
					
				}else{
				
					$doc_excel_file_value = "";
					if(isset($_FILES) && isset($_FILES["doc_excel_file"]["name"]))
					{
						$config['upload_path'] = DOC_ROOT_FRONT."/images/folder_documents/";
						$config['max_size']    = '1000000000';
						$config['allowed_types'] = '*';
						//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_excel_file']['name'][$key]);
						$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["doc_excel_file"]["name"]);
						
						$this->load->library('upload', $config);
						if (!$this->upload->do_upload("doc_excel_file"))
						{
							$image_error = array('error' => $this->upload->display_errors());
							echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
							exit;
						}
						else
						{
							$image_data = array('upload_data' => $this->upload->data());
							$doc_excel_file_value = $image_data['upload_data']['file_name'];
						}
						
						$image = $this->folderdocumentsmastermodel->getFormdata($_POST['folder_document_id']);
						if(is_array($image) && !empty($image[0]->document_field_value) && file_exists(DOC_ROOT_FRONT."/images/folder_documents/".$image[0]->document_field_value))
						{
							@unlink(DOC_ROOT_FRONT."/images/folder_documents/".$image[0]->document_field_value);
						}
						
					}else{
						$doc_excel_file_value = $_POST['prev_value'];
					}
					
					$data_array['document_field_value'] = $doc_excel_file_value;
					
				}
				
				
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->folderdocumentsmastermodel->updateRecord('tbl_folder_document', $data_array,'folder_document_id',$folder_document_id);
				
				
			}else {
				
				$this->load->library('upload');
				
				if(!empty($_POST['doc_type']) && isset($_POST['doc_type'])){
					$files = $_FILES;
					foreach($_POST['doc_type'] as $key=>$value){
						$doc_data = array();
						$doc_data['document_folder_id'] = $_POST['document_folder_id'];
						$doc_data['doc_type'] = $_POST['doc_type'][$key];
						$doc_data['document_title'] = $_POST['document_title'][$key];
						$doc_data['is_downloadable'] = $_POST['is_downloadable'][$key];
						$doc_data['start_date'] = (!empty($_POST['start_date'][$key])) ? date("Y-m-d", strtotime($_POST['start_date'][$key])) : '2019-04-08';
						$doc_data['end_date'] = (!empty($_POST['start_date'][$key])) ? date("Y-m-d", strtotime($_POST['end_date'][$key])) : '2019-04-08';
						
						if($_POST['doc_type'][$key] == 'video'){
							$doc_data['document_field_value'] = $_POST['video_url'][$key];
						}else if($_POST['doc_type'][$key] == 'doc'){
							if(!empty($files['doc_file']['name'][$key]) && isset($files['doc_file']['name'][$key])){
							
								//$file_name	= md5(uniqid("100_ID", true));
								$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
								//$file_name = str_replace(' ', '-', $files['doc_file']['name'][$key]);
								
								$_FILES['doc_file']['name']= $file_name;
								$_FILES['doc_file']['type']= $files['doc_file']['type'][$key];
								$_FILES['doc_file']['tmp_name']= $files['doc_file']['tmp_name'][$key];
								$_FILES['doc_file']['error']= $files['doc_file']['error'][$key];
								$_FILES['doc_file']['size']= $files['doc_file']['size'][$key];    
								
								$this->upload->initialize($this->set_upload_options());
							
								$this->upload->do_upload('doc_file');
								
								$doc_data['document_field_value'] = $file_name;
								
							}
						}else if($_POST['doc_type'][$key] == 'image'){
							if(!empty($files['image_file']['name'][$key]) && isset($files['image_file']['name'][$key])){
							
								//$file_name	= md5(uniqid("100_ID", true));
								$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['image_file']['name'][$key]);
								//$file_name = str_replace(' ', '-', $files['doc_file']['name'][$key]);
								
								$_FILES['image_file']['name']= $file_name;
								$_FILES['image_file']['type']= $files['image_file']['type'][$key];
								$_FILES['image_file']['tmp_name']= $files['image_file']['tmp_name'][$key];
								$_FILES['image_file']['error']= $files['image_file']['error'][$key];
								$_FILES['image_file']['size']= $files['image_file']['size'][$key];    
								
								$this->upload->initialize($this->set_upload_options());
							
								$this->upload->do_upload('image_file');
								
								$doc_data['document_field_value'] = $file_name;
								
							}
						}else if($_POST['doc_type'][$key] == 'doc_excel'){
							if(!empty($files['doc_excel_file']['name'][$key]) && isset($files['doc_excel_file']['name'][$key])){
							
								//$file_name	= md5(uniqid("100_ID", true));
								$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_excel_file']['name'][$key]);
								//$file_name = str_replace(' ', '-', $files['doc_file']['name'][$key]);
								
								$_FILES['doc_excel_file']['name']= $file_name;
								$_FILES['doc_excel_file']['type']= $files['doc_excel_file']['type'][$key];
								$_FILES['doc_excel_file']['tmp_name']= $files['doc_excel_file']['tmp_name'][$key];
								$_FILES['doc_excel_file']['error']= $files['doc_excel_file']['error'][$key];
								$_FILES['doc_excel_file']['size']= $files['doc_excel_file']['size'][$key];    
								
								$this->upload->initialize($this->set_upload_options());
							
								$this->upload->do_upload('doc_excel_file');
								
								$doc_data['document_field_value'] = $file_name;
								
							}
							
						}
						
						$doc_data['created_on'] = date("Y-m-d H:i:s");
						$doc_data['created_by'] = $_SESSION["webadmin"][0]->user_id;
						$doc_data['updated_on'] = date("Y-m-d H:i:s");
						$doc_data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
												
						$result = $this->folderdocumentsmastermodel->insertData('tbl_folder_document',$doc_data,'1');
					}
				}
				
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.'
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		//echo $id;exit;
		// print_r($id);exit();
		$_GET['document_folder_id'] = $id;
		$_GET['iDisplayLength'] = 200;
		$get_result = $this->folderdocumentsmastermodel->getRecords($_GET);
		// echo "<pre>";print_r($get_result);exit;
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		//$result["iDisplayStart"] = $_GET['iDisplayStart'];
		//$result["iDisplayLength"] = $_GET['iDisplayLength'];
		
		//$result["recordsTotal"] = $get_result['totalRecords'];
		//$result["recordsFiltered"] = $get_result['totalRecords'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		
		
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->folder_name);
				$doc_type = "";
				$doc_value = "";
				if(!empty($get_result['query_result'][$i]->doc_type) && $get_result['query_result'][$i]->doc_type == 'video'){
					$doc_type = "Video";
					$doc_value = '<iframe src="'.$get_result['query_result'][$i]->document_field_value.'" style="border:0;height:200;width:350px;max-width: 100%" allowFullScreen="true" allow="encrypted-media"></iframe>';
				}
				if(!empty($get_result['query_result'][$i]->doc_type) && $get_result['query_result'][$i]->doc_type == 'image'){
					$doc_type = "image";
					$p_info = pathinfo(FRONT_URL."/images/folder_documents/".$get_result['query_result'][$i]->document_field_value);
					$doc_value = '<img src="'.FRONT_URL.'/images/folder_documents/'.$get_result['query_result'][$i]->document_field_value.' " height="100px" width="100px">';
				}else if(!empty($get_result['query_result'][$i]->doc_type) && $get_result['query_result'][$i]->doc_type != 'video' && $get_result['query_result'][$i]->doc_type != 'image'){
					$p_info = pathinfo(FRONT_URL."/images/folder_documents/".$get_result['query_result'][$i]->document_field_value);
					$ext_val = (!empty($p_info['extension'])) ? $p_info['extension'] : '';
					$doc_type = "Document";
					$doc_value = '<a href="'.FRONT_URL.'/images/folder_documents/'.$get_result['query_result'][$i]->document_field_value.' " target="_blank"><i class="fa fa-file" style="font-size:60px;margin-bottom:2px;"></i></a><br/>
					'.$ext_val.' document.';
				}
				
				array_push($temp, $doc_type);
				array_push($temp, $get_result['query_result'][$i]->document_title);
				array_push($temp, $doc_value);
				
				array_push($temp, date("d-m-Y H:i:s", strtotime($get_result['query_result'][$i]->created_on)));
				//array_push($temp, $get_result['query_result'][$i]->is_downloadable);
				$actionCol22="";
				$actionCol22 .= '<a href="javascript:void(0);" onclick="is_downloadable(\'' . $get_result['query_result'][$i]->folder_document_id. '\',\'' . $get_result['query_result'][$i]->is_downloadable. '\');" title="">'.$get_result['query_result'][$i]->is_downloadable.'</a><br/>';
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("DocumentFolderAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->folder_document_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a><br/>';
					
					$actionCol21.= '<a href="folderdocumentsmaster/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->folder_document_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a>';
					
					
				}	
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("DocumentFolderAddEdit")){
					$actionCol1.= '<a href="javascript:void(0);" onclick="deleteData(\'' . $get_result['query_result'][$i]->folder_document_id. '\');" title="">Delete</a>';
					$actionCol1 .='<input type="checkbox" name="check[]"  class="payment_checkbox" value="'.$get_result['query_result'][$i]->folder_document_id.'" /></label>';
				}
				array_push($temp, $actionCol22);
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function changeapproval(){
		$result = "";
		$condition = "1=1 && course_id='".$_GET['id']."' ";
		$result['details'] = $this->folderdocumentsmastermodel->getdata('tbl_courses',$condition);
		//echo "<pre>";print_r($result["details"]);exit;
		$view = $this->load->view('routinesactions/changeapproval',$result,true);
		echo $view;
	}
	
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->folderdocumentsmastermodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('coursesmaster/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	function delRecord()
	{
		
		$id=$_POST['id'];
		
		$appdResult = $this->folderdocumentsmastermodel->delrecord1("tbl_timetable_videos","timetable_video_id",$id);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	// delete the records single and multiple 
	function delete(){
		if(!empty($_POST['record_ids']) && $_POST['record_ids']!=' ')
		{
			foreach ($_POST['record_ids'] as $key => $value) {
			
				$get_name = $this->folderdocumentsmastermodel->getdata("tbl_folder_document","folder_document_id='".$value."' ");
		
				if(!empty($get_name[0]['document_field_value'])){
					$del_path = DOC_ROOT."/images/folder_documents/".$get_name[0]['document_field_value'];
					if(file_exists($del_path)){
						@unlink($del_path);
					}
				}
			
				
				$del = $this->folderdocumentsmastermodel->delrecord1("tbl_folder_document","folder_document_id", $value);	
			}
		}
		
		if($del){
			echo json_encode(array('success'=>true, 'msg'=>'Record Deleted Successfully.'));
			exit;	
		}else{
			echo json_encode(array('success'=>false, 'msg'=>'Erro in Record Deleting...'));
			exit;	
		}
	}
	
	function delDocRecord()
	{
		
		$id=$_POST['id'];
		$get_name = $this->folderdocumentsmastermodel->getdata("tbl_folder_document","folder_document_id='".$id."' ");
		
		if(!empty($get_name[0]['document_field_value']) && $get_name[0]['doc_type'] == 'doc'){
			$del_path = DOC_ROOT."/images/folder_documents/".$get_name[0]['document_field_value'];
			if(file_exists($del_path)){
				@unlink($del_path);
			}
		}
		
		$appdResult = $this->folderdocumentsmastermodel->delrecord1("tbl_folder_document","folder_document_id",$id);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->folderdocumentsmastermodel->delrecord12("tbl_folder_document","folder_document_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	function is_downloadable()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$value=$_POST['value'];
		
		$appdResult = $this->folderdocumentsmastermodel->is_downloadable("tbl_folder_document","folder_document_id",$id,$value);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	private function set_upload_options()
	{   
		//upload an image options //products
		$config = array();
		$config['upload_path'] = "images/folder_documents/";
		$config['allowed_types'] = '*';
		//$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf|xlsx|csv|xls|txt|doc|docx';
		$config['max_size']      = '0';
		$config['overwrite']     = FALSE;

		return $config;
	}

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
