<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Centerusers extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('centerusersmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('centerusers/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['zones'] = $this->centerusersmodel->getDropdown("tbl_zones","zone_id,zone_name");
			$result['categories'] = $this->centerusersmodel->getDropdown("tbl_categories","category_id,categoy_name");
			$result['details'] = $this->centerusersmodel->getFormdata($record_id);
			//echo "<pre>";print_r($result);exit;
			
			$this->load->view('template/header.php');
			$this->load->view('centerusers/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	public function getCenters(){
		$result = $this->centerusersmodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->center_id == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	
	public function getCourses(){
		$result = $this->centerusersmodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['course_idval']) && !empty($_REQUEST['course_idval'])){
			$course_id = $_REQUEST['course_idval'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function submitForm(){
			/*print_r($_POST);
			exit;*/
			
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
		{
			
			$condition = "user_name= '".$_POST['user_name']."'";
			if(isset($_POST['user_id']) && $_POST['user_id'] > 0)
			{
				$condition .= " &&  user_id !='".$_POST['user_id']."'  ";
			}
			
			$check_name = $this->centerusersmodel->checkRecord($_POST,$condition);
			if(!empty($check_name[0]->user_id)){
				echo json_encode(array("success"=>"0",'msg'=>'User Already Present!'));
				exit;
			}
			//exit;
			
			if(!empty($_POST['user_id'])){
				//update
				/*echo "in update";
				print_r($_POST);
				exit;*/
				
				$data = array();
				$data['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				$data['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
				//$data['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
				//$data['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : '';
				$data['is_franchise'] = (!empty($_POST['is_franchise'])) ? $_POST['is_franchise'] : '';
				$data['user_type'] = (!empty($_POST['user_type'])) ? $_POST['user_type'] : '';
				$data['first_name'] = (!empty($_POST['first_name'])) ? $_POST['first_name'] : '';
				$data['last_name'] = (!empty($_POST['last_name'])) ? $_POST['last_name'] : '';
				$data['contact_no'] = (!empty($_POST['contact_no'])) ? $_POST['contact_no'] : '';
				
				$data['user_name'] = (!empty($_POST['user_name'])) ? $_POST['user_name'] : '';
				
				//$data['user_type'] = 2;
				
				if(!empty($_POST['password'])){
					$data['password'] = md5($_POST['password']);
				}
				
				$data['status'] = $_POST['status'];
				
				$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->centerusersmodel->updateUserId($data,$_POST['user_id']);
					
				if(!empty($result)){
					echo json_encode(array('success'=>'1','msg'=>'Record Updated Successfully'));
					exit;
				}else{
					echo json_encode(array('success'=>'0','msg'=>'Problem in data update.'));
					exit;
				}
				
				
			}else{
				//add
				/*echo "in add";
				print_r($_POST);
				exit;*/
				
				//print_r($_SESSION["webadmin"]);
				//echo $_SESSION["webadmin"][0]->user_id;
				//exit;
				
				$data = array();
				//$data['is_active'] = $_POST['is_active'];
				
				$data['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				$data['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
				//$data['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
				//$data['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : '';
				$data['user_type'] = (!empty($_POST['user_type'])) ? $_POST['user_type'] : '';
				$data['is_franchise'] = (!empty($_POST['is_franchise'])) ? $_POST['is_franchise'] : '';
				$data['first_name'] = (!empty($_POST['first_name'])) ? $_POST['first_name'] : '';
				$data['last_name'] = (!empty($_POST['last_name'])) ? $_POST['last_name'] : '';
				$data['contact_no'] = (!empty($_POST['contact_no'])) ? $_POST['contact_no'] : '';
				
				$data['user_name'] = (!empty($_POST['user_name'])) ? $_POST['user_name'] : '';
				
				//$data['user_type'] = 2;
				
				//if(!empty($_POST['password'])){
				$data['password'] = md5($_POST['password']);
				//}
				
				$data['status'] = $_POST['status'];
				
				$data['created_on'] = date("Y-m-d H:i:s");
				
				$data['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->centerusersmodel->insertData('tbl_admin_users',$data,'1');
				
				
				if(!empty($result)){
					
					$message = "";
					$message  = '<html><body>';		
					$message .= '<table border="0" cellpadding="0" cellspacing="0" style="background:#fff;width:100%;">
										<tbody>
											<tr>
												<td align="center" valign="top">
													<table border="0" cellpadding="0" cellspacing="0" style="background:#fff;max-width:700px;border:1px solid #ccc;" width="100%">
														<tbody>
															<tr>
																<td align="center" valign="top">
																	<table border="0" cellpadding="0" cellspacing="0" style="background:#fff;max-width:700px;" width="100%">
																		<tbody>
																			<tr>
																				<td style="height:100px;text-align:left;text-align:center;border-bottom:2px solid #0075bc">Montana - CRM</td>
																			</tr>
																			<tr>
																				<td style="padding:20px;">
																					<p style="margin-bottom:10px;">
																						Hello '.$_POST['first_name'].' '.$_POST['last_name'].',</p>
																					<p style="margin-bottom:10px;">
																						You have successfully registered as a Center User on Montana - CRM.</p>
																					<p style="margin-bottom:10px;">
																						Your Details are below</p>
																					<p style="margin-bottom:10px;">
																						Link: http://attoinfotech.in/demo/aptechmontanacrm</p>
																					<p style="margin-bottom:10px;">
																						Username: '.$_POST['user_name'].'</p>
																					<p style="margin-bottom:10px;">
																						Password: '.$_POST['password'].'<br />
																						<br />
																					</p>
																					<p style="margin-bottom:2px;">
																						Regards,</p>
																					<p style="margin:2px 0 2px 0">
																						Support Team</p>
																					<p style="margin:2px 0 2px 0">
																						Montana - CRM</p>
																				</td>
																			</tr>
																		</tbody>
																	</table>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
											</tr>
										</tbody>
									</table>
';
					$message .=  '</body></html>';
					
					$this->email->clear();
					$this->email->from("info@attoinfotech.in"); // change it to yours
					$subject = "";
					$subject = "Montana CRM - Center User Credentials";
					$this->email->to($_POST['user_name']); // change it to yours
					$this->email->subject($subject);
					$this->email->message($message);
					$checkemail = $this->email->send();
					$this->email->clear(TRUE);
					
					echo json_encode(array("success"=>"1",'msg'=>'Record Added Successfully.'));
					exit;
				}else{
					echo json_encode(array("success"=>"0",'msg'=>'Problem in data insert.'));
					exit;
				}
				
			}
			 
			
		}else{
			return false;
		}
	}
	
	/*new code end*/
	function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		//echo $id;exit;
		$get_result = $this->centerusersmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);
				array_push($temp, $get_result['query_result'][$i]->first_name);
				array_push($temp, $get_result['query_result'][$i]->last_name);
				array_push($temp, $get_result['query_result'][$i]->user_name);
				$user_type = "";
				if($get_result['query_result'][$i]->user_type == 2){
					$user_type = "Teacher";
				}else if($get_result['query_result'][$i]->user_type == 3){
					$user_type = "Center Head";
				}else{
					$user_type = "Counselor";
				}
				array_push($temp, $user_type);
				
				$viewCourses="";
				if($this->privilegeduser->hasPrivilege("CenterUserCategoryCoursesList")){
					$viewCourses.= '<a href="usercourses/index?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->user_id) , '+/', '-_') , '=') . '" title="Courses Mapping">View Courses Mapping</a>';
				}	
				
				array_push($temp, $viewCourses);

				$viewBatches="";
				// if($this->privilegeduser->hasPrivilege("")){
					$viewBatches.= '<a href="userbatches/index?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->user_id) , '+/', '-_') , '=') . '" title="Batches Mapping">View Batches Mapping</a>';
				// }	
				
				array_push($temp, $viewBatches);

				$viewGroup="";
				// if($this->privilegeduser->hasPrivilege("")){
					$viewGroup.= '<a href="usergroups/index?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->user_id) , '+/', '-_') , '=') . '" title="Group Mapping">View Group Mapping</a>';
				// }	
				
				array_push($temp, $viewGroup);
				
				$actionCol21="";
				if($this->privilegeduser->hasPrivilege("CenterUserAddEdit")){
					$actionCol21 .= '<a href="javascript:void(0);" onclick="deleteData1(\'' . $get_result['query_result'][$i]->user_id. '\',\'' . $get_result['query_result'][$i]->status. '\');" title="">'.$get_result['query_result'][$i]->status.'</a>';
				}	
				
				$actionCol1 = "";
				if($this->privilegeduser->hasPrivilege("CenterUserAddEdit")){
					$actionCol1.= '<a href="centerusers/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->user_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				}	
				
				array_push($temp, $actionCol21);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}
	
	
	function changeapproval(){
		$result = "";
		$condition = "1=1 && course_id='".$_GET['id']."' ";
		$result['details'] = $this->centerusersmodel->getdata('tbl_courses',$condition);
		//echo "<pre>";print_r($result["details"]);exit;
		$view = $this->load->view('routinesactions/changeapproval',$result,true);
		echo $view;
	}
	
	function view($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			// print_r($_GET);

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = "";
			$result['details'] = $this->centerusersmodel->getFormdata($record_id);
			
			$this->load->view('template/header.php');
			$this->load->view('centerusers/view', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	function delRecord()
	{
		
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->centerusersmodel->delrecord("tbl_admin_users","user_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->centerusersmodel->delrecord12("tbl_admin_users","user_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}

	function logout()
	{
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('auth/login', 'refresh');
	}
	
}

?>
