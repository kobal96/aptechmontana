<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Center Users</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>centerusers">Center Users</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="user_id" name="user_id" value="<?php if(!empty($details[0]->user_id)){echo $details[0]->user_id;}?>" />
								
								<div class="control-group form-group">
									<label class="control-label" for="zone_id">Zone*</label>
									<div class="controls">
										<select id="zone_id" name="zone_id" class="form-control"  onchange="getCenters(this.value);">
											<option value="">Select Zone</option>
											<?php 
												if(isset($zones) && !empty($zones)){
													foreach($zones as $cdrow){
														$sel = ($cdrow->zone_id == $details[0]->zone_id) ? 'selected="selected"' : '';
											?>
												<option value="<?php echo $cdrow->zone_id;?>" <?php echo $sel; ?>><?php echo $cdrow->zone_name;?></option>
											<?php }}?>
										</select>
									</div>
								</div>
							
								<div class="control-group form-group">
									<label class="control-label"><span>Center*</span></label> 
									<div class="controls">
										<select id="center_id" name="center_id" class="form-control" >
											<option value="">Select Center</option>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>User Type*</span></label> 
									<div class="controls">
										<select id="user_type" name="user_type" class="form-control" >
											<option value="">Select User Type</option>
											<option value="3" <?php if(!empty($details[0]->user_type) && $details[0]->user_type == '3'){?> selected <?php }?>>Center Head</option>
											<option value="2" <?php if(!empty($details[0]->user_type) && $details[0]->user_type == '2'){?> selected <?php }?>>Teacher</option>
											<option value="4" <?php if(!empty($details[0]->user_type) && $details[0]->user_type == '4'){?> selected <?php }?>>Counselor</option>
										</select>
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label"><span>Is Franchise?*</span></label> 
									<div class="controls">
										<select id="is_franchise" name="is_franchise" class="form-control" >
											<option value="">Select</option>
											<option value="Yes" <?php if(!empty($details[0]->is_franchise) && $details[0]->is_franchise == 'Yes'){?> selected <?php }?>>Yes</option>
											<option value="No" <?php if(!empty($details[0]->is_franchise) && $details[0]->is_franchise == 'No'){?> selected <?php }?>>No</option>
										</select>
									</div>
								</div>
								
								<!--
								<div class="control-group form-group">
									<label class="control-label" for="category_id">Category*</label>
									<div class="controls">
										<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);">
											<option value="">Select Category</option>
											<?php 
												if(isset($categories) && !empty($categories)){
													foreach($categories as $cdrow){
														$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
											?>
												<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
											<?php }}?>
										</select>
									</div>
								</div>
							
								<div class="control-group form-group">
									<label class="control-label"><span>Course*</span></label> 
									<div class="controls">
										<select id="course_id" name="course_id" class="form-control" >
											<option value="">Select Course</option>
										</select>
									</div>
								</div>
								-->
								<div class="control-group form-group">
									<label class="control-label" for="focusedInput">First Name*</label>
									<div class="controls">
										<input class="input-xlarge form-control" id="first_name" name="first_name" type="text" value="<?php if(!empty($details[0]->first_name)){echo $details[0]->first_name;}?>">
									</div>
								</div>
								<div class="control-group form-group">
									<label class="control-label" for="focusedInput">Last Name*</label>
									<div class="controls">
										<input class="input-xlarge form-control" id="last_name" name="last_name" type="text" value="<?php if(!empty($details[0]->last_name)){echo $details[0]->last_name;}?>">
									</div>
								</div>
								<div class="control-group form-group">
									<label class="control-label" for="contact_no">Contact No*</label>
									<div class="controls">
										<input class="input-xlarge form-control" id="contact_no" name="contact_no" type="text" value="<?php if(!empty($details[0]->contact_no)){echo $details[0]->contact_no;}?>">
									</div>
								</div>
								
								<div class="control-group form-group">
									<label class="control-label" for="focusedInput">User Name*</label>
									<div class="controls">
										<input class="input-xlarge form-control" id="user_name" name="user_name" type="text" value="<?php if(!empty($details[0]->user_name)){echo $details[0]->user_name;}?>">
									</div>
								</div>
								
								<?php 
									$change_pwd_display = "block";
									if(!empty($details[0]->user_id)){
										$change_pwd_display = "none";
										?>
									<div class="control-group form-group">
										<label class="control-label" for="focusedInput"><input class="" id="change_pwd" name="change_pwd" type="checkbox"> Change Password</label>
									</div>
									<?php } ?>
						
									<div class="cls_change_pwd" style="display:<?php echo $change_pwd_display; ?>" id="change_password_div">
										<div class="control-group form-group change_pwd">
											<label class="control-label" for="focusedInput">Password*</label>
											<div class="controls">
												<input class="input-xlarge form-control" id="password" name="password" type="password"  autocomplete="off">
											</div>
										</div>
										
										<div class="control-group form-group">
											<label class="control-label" for="focusedInput">Confirm Password*</label>
											<div class="controls">
												<input class="input-xlarge form-control" id="confirm_password" name="confirm_password" type="password">
											</div>
										</div>
									</div>
									<div class="control-group form-group">
										<label class="control-label" for="status">Status*</label>
										<div class="controls">
											<select name="status" id="status" class="form-control">
												<option value="Active" <?php if(!empty($details[0]->status) && $details[0]->status == "Active"){?> selected <?php }?>>Active</option>
												<option value="In-active" <?php if(!empty($details[0]->status) && $details[0]->status == "In-active"){?> selected <?php }?>>In-active</option>
											</select>
										</div>
									</div>
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>centerusers" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	<?php 
		if(!empty($details[0]->user_id)){
	?>
		getCenters('<?php echo $details[0]->zone_id; ?>', '<?php echo $details[0]->center_id; ?>');
	<?php }?>
});


function getCenters(zone_id,center_id = null)
{
	//alert("zone_id: "+zone_id);return false;
	if(zone_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>centerusers/getCenters",
			data:{zone_id:zone_id, center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#center_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#center_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#center_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getCourses(category_id,course_id = null)
{
	//alert("Val: "+val);return false;
	if(category_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>centerusers/getCourses",
			data:{category_id:category_id, course_idval:course_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#course_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

$("#change_pwd").on("change", function(){
	$("#password").val("");
	$("#confirm_password").val("");
	
	if(this.checked) {
		var returnVal = confirm("Are you sure?");
		$(this).prop("checked", returnVal);
		if(returnVal){
			$('.cls_change_pwd').show();  
		}
	}else{
		$(this).prop("checked", false);
		$('.cls_change_pwd').hide();  
	}   
});

var vRules = {
	zone_id:{required:true},
	center_id:{required:true},
	user_type:{required:true},
	is_franchise:{required:true},
	first_name:{required:true},
	last_name:{required:true},
	contact_no:{required:true, digits:true},
	user_name:{required:true, email:true},
	<?php if(empty($users[0]->user_id)){ ?>
	password:{required:true},
	confirm_password:{required:true,equalTo:password},
	<?php }else{ ?>
	password:{required:"#change_pwd:checked"},
	confirm_password:{required:"#change_pwd:checked",equalTo:password},
	<?php } ?>
	role_id:{required:true}
};
var vMessages = {
	zone_id:{required:"Please select zone."},
	center_id:{required:"Please select center."},
	user_type:{required:"Please select user type."},
	is_franchise:{required:"Please select value."},
	first_name:{required:"Please enter first name."},
	last_name:{required:"Please enter last name."},
	contact_no:{required:"Please enter contact number."},
	user_name:{required:"Please enter user name."},
	password:{required:"Please enter password."},
	confirm_password:{required:"Please enter confirm password.",equalTo:"Password does not match."},
	role_id:{required:"Please select role."}
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>centerusers/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>centerusers";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Center User";

 
</script>					
