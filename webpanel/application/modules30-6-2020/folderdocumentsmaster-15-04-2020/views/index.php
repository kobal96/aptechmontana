<?php 
//session_start();
//print_r($_SESSION["webadmin"]);
//echo "<pre>";
//print_r($roles);exit;

if (!empty($_GET['text']) && isset($_GET['text'])) {
	$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
	//echo $_GET['text'];exit;
	parse_str($varr, $url_prams);
	$record_id = $url_prams['id'];
}
?>
<!-- start: Content -->
<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1>Folder Documents</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>documentfoldermaster">Folders</a></li>
        </ul>
      </div>
    </div> 
    <div class="card">
    	<div class="page-title-border">
        	<div class="col-sm-12 col-md-12 left-button-top">
            	<?php 
					//if ($this->privilegeduser->hasPrivilege("UserAddEdit")) {
				?>
				
					<p>
					<?php if ($this->privilegeduser->hasPrivilege("WeekAddEdit")) {?>	
						<a href="<?php  echo base_url();?>folderdocumentsmaster/addEdit" class="btn btn-primary icon-btn"><i class="fa fa-plus"></i>Add Documents</a>
					<?php }?>	
						
						
            <div class="clearfix"></div>
            </div>
        </div>     
		
        <div class="col-sm-12" style="clear: both">
         	<div class="box-content form-horizontal product-filter">            	
            	
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Folder Name</label>
						<input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Document Type</label>
						<select id="sSearch_1" name="sSearch_1" class="searchInput form-control ">
							<option value="">Select Type</option>
							<option value="video">Video</option>
							<option value="doc">PDF Document</option>
							<option value="doc_excel">Excel/Doc/PPT Document</option>
							<option value="image">Image</option>
						</select>
					</div>
				</div>
				
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="firstname" class="control-label">Document Title</label>
						<input id="sSearch_2" name="sSearch_2" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				
				
				<div class="control-group clearFilter">
					<div class="controls">
						<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
					</div>
				</div>
				
            </div>
         </div>
		 
		 
         <div class="clearfix"></div>
         <div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                    <table class="dynamicTable display table table-bordered non-bootstrap" callfunction="<?php echo base_url();?>folderdocumentsmaster/fetch/<?php echo $record_id;?>" noofrecords="200">
                        <thead>
                          <tr>
							<th>Folder Name</th>
							<th>Document Type</th>
							<th>Document Title</th>
							<th>Document</th>
							<th>Uploaded On</th>
							<th>Is Downloadable</th>
							<th>Parent show</th>
							<th data-bSortable="false">Status</th>
							<th data-bSortable="false">Action / 
							<br/>
							<a href="#" onclick="deleteAll(0)" title="delete All Payment Record">Delete Records</a>
							<input type="checkbox" id="checkAll" class="payment_checkbox"/>
							</th>
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
    </div>
</div><!-- end: Content -->
			
<script>
	$( document ).ready(function() {

		clearSearchFilters();
		
		$("#checkAll").change(function () {
			$("input:checkbox").prop('checked', $(this).prop("checked"));
			/* alert( $(this).prop("checked")) */;
		});
		
		
	});	
	
	function deleteAll(id){

		var validate=$('.payment_checkbox:checked').val();
		if(validate == undefined)
		{
			alert('Not a single check box is checked');
		}
		else{
			var ans = confirm("Are you sure you want to delete this record?");
				if(ans){
					$('#LoadingModal .modal-body .process_name').html("Deleting...");
					$('#LoadingModal').modal('show');
					var record_ids = [];
					$(".payment_checkbox:checked").each(function() {
						record_ids.push($(this).val());
						
					});
					//alert(JSON.stringify(record_ids));
					//return false;
					$.ajax({
						url: "<?php echo base_url();?>folderdocumentsmaster/delete/",
						async: false,
						type: "POST",
						data:{record_ids:record_ids},
						dataType: "json",
						success: function (response) {
							if(response.success){
								$('#LoadingModal').modal('hide');	
								$('#alertModal .modal-body .process_name').html("Record has been deleted!");
								$('#alertModal').modal('show');
								setTimeout(function(){
									//window.location = "<?php echo base_url("folderdocumentsmaster");?>";
									window.location.reload();
								},2000);
							}else{	
								$('#LoadingModal').modal('hide');	
								$('#alertModal .modal-body .process_name').html("Oops ! Problem in deleting record.");
								$('#alertModal').modal('show');
							}
						}
					});
				}//alert confirm end here 
		}
		
		
	}//checkbox function end here 
	
	
	function deleteData1(id,status)
	{
		var sta="";
		var sta1=" ";
		if(status=='Active')
		{
		
			sta="In-active";
		}
		else{
			
			sta="Active";
		}
    	var r=confirm("Are you sure to " +sta);
    	if (r==true)
   		{
    		//window.location.href="users/delete?id="+id;
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/delrecord12/",
				data:{"id":id,"status":sta},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Updated!");
						setTimeout("location.reload(true);",1000);
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});
    	}
    }
    
	function is_downloadable(id,value)
	{
		var sta="";
		var sta1=" ";
		if(value=='Yes')
		{
		
			sta="No";
		}
		else{
			
			sta="Yes";
		}
    	var r=confirm("Are you sure to " +sta);
    	if (r==true)
   		{
    		//window.location.href="users/delete?id="+id;
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/is_downloadable/",
				data:{"id":id,"value":sta},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Updated!");
						setTimeout("location.reload(true);",1000);
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});
    	}
    }
    
	function parent_show(id,value)
	{
		var sta="";
		var sta1=" ";
		if(value=='Yes')
		{
		
			sta="No";
		}
		else{
			
			sta="Yes";
		}
    	var r=confirm("Are you sure to " +sta);
    	if (r==true)
   		{
    		//window.location.href="users/delete?id="+id;
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/parent_show/",
				data:{"id":id,"value":sta},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been Updated!");
						setTimeout("location.reload(true);",1000);
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});
    	}
    }
    function deleteData(id)
	{
		
    	var r=confirm("Are you sure to delete selected record?");
    	if (r==true)
   		{
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/delDocRecord/",
				data:{"id":id},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1")
					{
						displayMsg("success","Record has been deleted!");
						setTimeout("location.reload(true);",1000);
						
					}
					else
					{
						displayMsg("error","Oops something went wrong!");
						setTimeout("location.reload(true);",1000);
					}
				}
			});
    	}
    }
	
	document.title = "Folder Documents Master";
</script>