<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1>Assign Album</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>assignalbum">Assign Album</a></li>
        </ul>
      </div>
    </div>
    <div class="card">       
     <div class="card-body">             
        <div class="box-content">
            <div class="col-sm-8 col-md-12">
				<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
					<input type="hidden" id="assign_album_id" name="assign_album_id" value="<?php if(!empty($details[0]->assign_album_id)){echo $details[0]->assign_album_id;}?>" />
					<div class="control-group form-group">
						<label for="Zone" class="control-label">Current Academy Year</label>
						<select name="academic_year_id" id="academic_year_id" class=" form-control">
							<option value="">Select Academy year</option>
							<?php if(!empty($academicyear)){
									foreach($academicyear as $key=>$val){
										$sel ='';
											if(!empty($details[0]->academic_year_id)){
												$sel =($details[0]->academic_year_id == $val->academic_year_master_id)?"selected":'';
											}
											?>
								<option value="<?php echo $val->academic_year_master_id; ?>" <?= $sel ?> ><?php echo $val->academic_year_master_name;?></option>
							<?php
									}
								}
							?>
						</select>
					</div>

					<div class="control-group form-group">
						<label class="control-label" for="zone_id">Zone*</label>
						<div class="controls">
							<select id="zone_id" name="zone_id" class="form-control"  onchange="getCenters(this.value);getGroup(this.value)" >
								<option value="">Select Zone</option>
								<?php 
									if(isset($zones) && !empty($zones)){
										foreach($zones as $cdrow){
											$sel = ($cdrow->zone_id == $details[0]->zone_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->zone_id;?>" <?php echo $sel; ?>><?php echo $cdrow->zone_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label"><span>Center*</span></label> 
						<div class="controls">
							<select id="center_id" name="center_id" class="form-control"  onchange="getBatch(this.value);getGroup(this.value)" >
								<option value="">Select Center</option>
							</select>
						</div>
					</div>
					
					<div class="control-group form-group">
						<label class="control-label"><span>Selection Type*</span></label>
						<div class="controls">
							<input type="radio" class="form-control album_selection_type classchecked" name="album_selection_type" value="Class" <?php echo (!empty($details[0]->album_selection_type) &&  ($details[0]->album_selection_type== 'Class')) ? "checked" : ""?> checked="checked">Class
							<input type="radio" class="form-control album_selection_type groupchecked" name="album_selection_type" value="Group" <?php echo (!empty($details[0]->album_selection_type) &&  ($details[0]->album_selection_type== 'Group')) ? "checked" : ""?> >Group
						</div>
					</div>

					<!-- <div class="control-group form-group assigntoallclass">
						<label class="control-label" for="assign_category_id">Category*</label>
						<div class="controls">
							<select id="assign_category_id" name="assign_category_id[]" class="form-control select2 assign_category_id" multiple onchange="getCourses(this.value);">
								<option value="" disabled="disabled">Select Category</option>
								<?php 
								if(isset($categories) && !empty($categories)){
									foreach($categories as $cdrow){
										$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
							?>
								<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
							<?php }}?>
							</select>
						</div>
					</div> -->

					<div class="control-group form-group class_div">
						<label class="control-label" for="category_id">Category*</label>
						<div class="controls">
							<select id="category_id" name="category_id" class="form-control"  onchange="getCourses(this.value);" >
								<option value="">Select Category</option>
								<?php 
									if(isset($categories) && !empty($categories)){
										foreach($categories as $cdrow){
											$sel = ($cdrow->category_id == $details[0]->category_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->category_id;?>" <?php echo $sel; ?>><?php echo $cdrow->categoy_name;?></option>
								<?php }}?>
							</select>
						</div>
					</div>

					<!-- <div class="control-group form-group assigntoallclass">
						<label class="control-label" for="assign_course_id">Course*</label>
						<div class="controls">
							<select id="assign_course_id" name="assign_course_id[]" class="form-control select2 assign_course_id" multiple  onchange="getBatch(this.value);">
								<option value="" disabled="disabled">Select Course</option>
								<?php 
								if(isset($allcourses) && !empty($allcourses)){
									foreach($allcourses as $cdrow){
										$sel = ($cdrow->course_id == $details[0]->course_id) ? 'selected="selected"' : '';
							?>
								<option value="<?php echo $cdrow->course_id;?>" <?php echo $sel; ?>><?php echo $cdrow->course_name;?></option>
							<?php }}?>
							</select>
						</div>
					</div> -->
				
					<?php if(!empty($details[0]->assign_album_id)){?>
					<div class="control-group form-group class_div">
						<label class="control-label"><span>Course*</span></label> 
						<div class="controls">
							<select id="course_id" name="course_id" class="form-control" onchange="getBatch(this.value);">
								<option value="">Select Course</option>
							</select>
						</div>
					</div>
					<?php }else{ ?>
					<div class="control-group form-group class_div">
						<label class="control-label"><span>Course*</span></label> 
						<br>
						<input type="checkbox" id="assigntoallcourse" name="assigntoallcourse" onchange="getBatch(this.value);">Assign To All
						<br>
						<div class="controls course_div">
							<select id="course_id" name="course_id[]" class="form-control select2" onchange="getBatch(this.value);" multiple="multiple">
								<option value="">Select Course</option>
							</select>
						</div>
					</div>
					<?php } ?>

					<!-- <div class="control-group form-group assigntoallgroup">
						<label class="control-label" for="assign_group_id">Group*</label>
						<div class="controls">
							<select id="assign_group_id" name="assign_group_id[]" class="form-control select2 assign_group_id" multiple>
								<option value="" disabled="disabled">Select Group</option>
								<?php 
								if(isset($groups) && !empty($groups)){
									foreach($groups as $cdrow){
										$sel = ($cdrow->group_master_id == $details[0]->group_id) ? 'selected="selected"' : '';
							?>
								<option value="<?php echo $cdrow->group_master_id;?>" <?php echo $sel; ?>><?php echo $cdrow->group_master_name;?></option>
							<?php }}?>
							</select>
						</div>
					</div> -->
					<?php if(!empty($details[0]->assign_album_id)){?>
						<div class="control-group form-group group_div">
							<label class="control-label" for="group_id">Group*</label>
							<div class="controls">
								<select id="group_id" name="group_id" class="form-control" onchange="getGroupbatch(this.value)">
									<option value="">Select Group</option>
								</select>
							</div>
						</div>
					<?php }else{ ?>
						<div class="control-group form-group group_div">
							<label class="control-label" for="group_id">Group*</label>
							<br>
							<input type="checkbox" id="assigntoallgroup" name="assigntoallgroup" onchange="getGroupbatch(this.value);">Assign To All
							<br>
							<div class="controls group_hide">
								<select id="group_id" name="group_id[]" class="form-control select2" onchange="getGroupbatch(this.value)" multiple="multiple">
									<option value="">Select Group</option>
								</select>
							</div>
						</div>
					<?php } ?>
					
					<!-- <div class="control-group form-group assigntoallclass">
						<label class="control-label" for="assign_batch_id">Batch*</label>
						<div class="controls">
							<select id="assign_batch_id" name="assign_batch_id[]" class="form-control select2 assign_batch_id" multiple  onchange="getBatch(this.value);">
								<option value="" disabled="disabled">Select Course</option>
								<?php 
								if(isset($allbatches) && !empty($allbatches)){
									foreach($allbatches as $cdrow){
										$sel = ($cdrow->batch_id == $details[0]->batch_id) ? 'selected="selected"' : '';
							?>
								<option value="<?php echo $cdrow->batch_id;?>" <?php echo $sel; ?>><?php echo $cdrow->batch_name;?></option>
							<?php }}?>
							</select>
						</div>
					</div> -->

					<?php if(!empty($details[0]->assign_album_id)){?>
						<div class="control-group form-group class_div">
							<label class="control-label" for="batch_id">Batch*</label>
							<div class="controls batch_div">
								<select id="batch_id" name="batch_id" class="form-control">
									<option value="">Select Batch</option>
								</select>
							</div>
						</div>
					<?php } else{ ?>
						<div class="control-group form-group class_div">
						<label class="control-label" for="batch_id">Batch*</label>
						<br>
						<input type="checkbox" id="assigntoallbatch" name="assigntoallbatch">Assign To All
						<br>
						<div class="controls batch_div">
							<select id="batch_id" name="batch_id[]" class="form-control select2" multiple="multiple">
								<option value="">Select Batch</option>
							</select>
						</div>
					</div>
					<?php } ?>

					<?php if(!empty($details[0]->assign_album_id)){?>
						<div class="control-group form-group group_div">
							<label class="control-label" for="group_batch_id">Batch*</label>
							<div class="controls">
								<select id="group_batch_id" name="batch_group_id" class="form-control">
									<option value="">Select Batch</option>
								</select>
							</div>
						</div>
					<?php } else{ ?>
						<div class="control-group form-group group_div">
							<label class="control-label" for="group_batch_id">Batch*</label>
							<br>
							<input type="checkbox" id="assigntoallbatchgroup" name="assigntoallbatchgroup">Assign To All
							<br>
							<div class="controls batchgroup_hide">
								<select id="group_batch_id" name="batch_group_id[]" class="form-control select2" multiple="multiple">
									<option value="">Select Batch</option>
								</select>
							</div>
						</div>
					<?php } ?>

					<?php if(!empty($details[0]->assign_album_id)){?>
						<div class="control-group form-group">
							<label class="control-label"><span>Album*</span></label> 
							<div class="controls">
								<select id="album_id" name="album_id" class="form-control album_id">
									<option value="">Select Album</option>
									<?php 
									if(isset($album) && !empty($album)){
										foreach($album as $cdrow){
											$sel = ($cdrow->album_id == $details[0]->album_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->album_id;?>" <?php echo $sel; ?>><?php echo $cdrow->album_name;?></option>
								<?php }}?>
								</select>
							</div>
						</div>
					<?php }else{?>
						<div class="control-group form-group">
							<label class="control-label"><span>Album*</span></label> 
							<input type="checkbox" id="checkbox" >Select All
							<div class="controls">
								<select id="album_id" name="album_id[]" class="form-control select2 album_id" multiple >
									<option value="" disabled="disabled">Select Album</option>
									<?php 
									if(isset($album) && !empty($album)){
										foreach($album as $cdrow){
											$sel = ($cdrow->album_id == $details[0]->album_id) ? 'selected="selected"' : '';
								?>
									<option value="<?php echo $cdrow->album_id;?>" <?php echo $sel; ?>><?php echo $cdrow->album_name;?></option>
								<?php }}?>
								</select>
							</div>
						</div>
					<?php }?>
					
					<div class="form-actions form-group">
						<button type="submit" class="btn btn-primary">Submit</button>
						<a href="<?php echo base_url();?>assignalbum" class="btn btn-primary">Cancel</a>
					</div>
				</form>
            </div>
        <div class="clearfix"></div>
        </div>
     </div>
    </div>        
</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	$('.group_div').hide();
	// $('.assigntoallclass').hide();
	// $('.assigntoallgroup').hide();
	$("#checkbox").click(function(){
		if($("#checkbox").is(':checked') ){
			$("#album_id > option").prop("selected","selected");
			$("#album_id").trigger("change");
		}else{
			$("#album_id > option").removeAttr("selected");
			$("#album_id").trigger("change");
		}
	});
	$("#assigntoallcourse").click(function(){
		if($("#assigntoallcourse").is(':checked') ){
			$('.course_div').hide();
		}else{
			$('.course_div').show();
		}
	});
	$("#assigntoallbatch").click(function(){
		if($("#assigntoallbatch").is(':checked') ){
			$('.batch_div').hide();
		}else{
			$('.batch_div').show();
		}
	});
	$("#assigntoallgroup").click(function(){
		if($("#assigntoallgroup").is(':checked') ){
			$('.group_hide').hide();
		}else{
			$('.group_hide').show();
		}
	});
	$("#assigntoallbatchgroup").click(function(){
		if($("#assigntoallbatchgroup").is(':checked') ){
			$('.batchgroup_hide').hide();
		}else{
			$('.batchgroup_hide').show();
		}
	});
	// $("#assigntoall").click(function(){
	// 	if($(".classchecked").is(':checked') ){
	// 		if($("#assigntoall").is(':checked') ){
	// 			// $('.assigntoallclass').show();
	// 			$('.class_div').hide();
	// 			// $("#assign_category_id > option").prop("selected","selected");
	// 			// $("#assign_category_id").trigger("change");
	// 			// $("#assign_course_id > option").prop("selected","selected");
	// 			// $("#assign_course_id").trigger("change");
	// 			// $("#assign_batch_id > option").prop("selected","selected");
	// 			// $("#assign_batch_id").trigger("change");
	// 		}else{
	// 			// $('.assigntoallclass').hide();
	// 			$('.class_div').show();
	// 			// $("#assign_category_id > option").removeAttr("selected");
	// 			// $("#assign_category_id").trigger("change");
	// 			// $("#assign_course_id > option").removeAttr("selected");
	// 			// $("#assign_course_id").trigger("change");
	// 			// $("#assign_batch_id > option").removeAttr("selected");
	// 			// $("#assign_batch_id").trigger("change");
	// 		}
	// 	}
	// 	else{
	// 		if($("#assigntoall").is(':checked') ){
	// 			// $('.assigntoallgroup').show();
	// 			$('.group_div').hide();
	// 			// $("#assign_group_id > option").prop("selected","selected");
	// 			// $("#assign_group_id").trigger("change");
	// 		}else{
	// 			// $('.assigntoallgroup').hide();
	// 			$('.group_div').show();
	// 			// $("#assign_group_id > option").removeAttr("selected");
	// 			// $("#assign_group_id").trigger("change");
	// 		}
	// 	}
	// });
	<?php 
		if(!empty($details[0]->assign_album_id)){
	?>
		getCourses('<?php echo $details[0]->category_id; ?>', '<?php echo $details[0]->course_id; ?>');
		getBatch('<?php echo $details[0]->course_id; ?>','<?php echo $details[0]->center_id; ?>','<?php echo $details[0]->batch_id; ?>');
		getCenters('<?php echo $details[0]->zone_id; ?>', '<?php echo $details[0]->center_id; ?>');
		getGroupbatch('<?php echo $details[0]->center_id; ?>','<?php echo $details[0]->group_id; ?>', '<?php echo $details[0]->batch_id; ?>');
		getGroup('<?php echo $details[0]->center_id; ?>','<?php echo $details[0]->group_id; ?>');
	<?php 
		if($details[0]->album_selection_type == 'Class'){ ?>
			$('.class_div').show();
			$('.group_div').hide();
		<?php
		}
		else{ ?>
			$('.class_div').hide();
			$('.group_div').show();
		<?php
		} 
	}?>
	$('.album_selection_type').change(function(){
		var album_selection_type = $(this).val();
		if(album_selection_type == 'Class'){
			$('.class_div').show();
			$('.group_div').hide();
			// $('.assigntoallgroup').hide();
			if($("#assigntoall").is(':checked') ){
				// $('.assigntoallclass').show();
				$('.class_div').hide();
				// $("#assign_category_id > option").prop("selected","selected");
				// $("#assign_category_id").trigger("change");
				// $("#assign_course_id > option").prop("selected","selected");
				// $("#assign_course_id").trigger("change");
				// $("#assign_batch_id > option").prop("selected","selected");
				// $("#assign_batch_id").trigger("change");
			}else{
				// $('.assigntoallclass').hide();
				$('.class_div').show();
				// $("#assign_category_id > option").removeAttr("selected");
				// $("#assign_category_id").trigger("change");
				// $("#assign_course_id > option").removeAttr("selected");
				// $("#assign_course_id").trigger("change");
				// $("#assign_batch_id > option").removeAttr("selected");
				// $("#assign_batch_id").trigger("change");
			}
		}
		else{
			$('.class_div').hide();
			$('.group_div').show();
			// $('.assigntoallclass').hide();
			if($("#assigntoall").is(':checked') ){
				// $('.assigntoallgroup').show();
				$('.group_div').hide();
				// $("#assign_group_id > option").prop("selected","selected");
				// $("#assign_group_id").trigger("change");
			}else{
				// $('.assigntoallgroup').hide();
				$('.group_div').show();
				// $("#assign_group_id > option").removeAttr("selected");
				// $("#assign_group_id").trigger("change");
			}
		}
	})
});

function getCourses(category_id,course_id = null)
{
	if(category_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>assignalbum/getCourses",
			data:{category_id:category_id, course_id:course_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#course_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#course_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#course_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}
function getBatch(course_id,center_id,batch_id = null)
{
	var category_id = $('#category_id').val();
	$('#s2id_batch_id').children().find('.select2-search-choice').remove()
	<?php 
		if(!empty($details[0]->assign_album_id)){
	?>
		if($('#course_id').val() == '' || $('#course_id').val() == null){
			var course_id = '<?php echo $details[0]->course_id; ?>';
		}
		else{
			var course_id = $('#course_id').val();
		}
		if($('#center_id').val() == '' || $('#center_id').val() == null){
			var center_id = '<?php echo $details[0]->center_id; ?>';
		}
		else{
			var center_id = $('#center_id').val();
		}
	<?php 
		}
		else{ 
	?>
		var course_id = $('#course_id').val();
		var center_id = $('#center_id').val();
		if($('#assigntoallcourse').is(':checked')){
			var course_id = 'on';
		}
	<?php
		} 
	?>
	if(course_id != "" && course_id != null)
	{
		$.ajax({
			url:"<?php echo base_url();?>assignalbum/getBatch",
			data:{center_id:center_id, course_id:course_id,batch_id:batch_id,category_id:category_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#batch_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#batch_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#batch_id").html("<option value=''>Select</option>");
				}
			}
		});
	}else{
		$("#batch_id").html("<option value=''>Select</option>");
	}
}

function getGroup(center_id,group_id = null){
	$("#group_batch_id").html("<option value=''>Select</option>");
	$('#s2id_group_id').children().find('.select2-search-choice').remove()
	if(center_id != null && center_id != ''){
		$.ajax({
			url:"<?php echo base_url();?>assignalbum/getGroup",
			data:{center_id:center_id,group_id:group_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != '')
					{
						$("#group_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#group_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#group_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
	else{
		$("#group_id").html("<option value=''>Select</option>");
	}
}

function getCenters(zone_id,center_id = null)
{
	//alert("Val: "+val);return false;
	if(zone_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>assignalbum/getCenters",
			data:{zone_id:zone_id, center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#center_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#center_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#center_id").html("<option value='' >Select</option>");
				}
			}
		});
	}
}

function getGroupbatch(center_id,group_id,batch_id = null)
{
	// $('#s2id_group_id').children().find('.select2-search-choice').remove()
	$('#s2id_group_batch_id').children().find('.select2-search-choice').remove()
	<?php 
		if(!empty($details[0]->assign_album_id)){
	?>
		if($('#center_id').val() == '' || $('#center_id').val() == null){
			var center_id = '<?php echo $details[0]->center_id; ?>';
		}
		else{
			var center_id = $('#center_id').val();
		}
		if($('#group_id').val() == '' || $('#group_id').val() == null){
			var group_id = '<?php echo $details[0]->group_id; ?>';
		}
		else{
			var group_id = $('#group_id').val();
		}
	<?php 
		}
		else{ 
	?>
		var group_id = $('#group_id').val();
		var center_id = $('#center_id').val();
		if($('#assigntoallgroup').is(':checked')){
			var group_id = 'on';
		}
	<?php
		} 
	?>
	if(group_id != "" && group_id != null )
	{
		$.ajax({
			url:"<?php echo base_url();?>assignalbum/getGroupbatch",
			data:{center_id:center_id,group_id:group_id, batch_id:batch_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#group_batch_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#group_batch_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#group_batch_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
	else{
		$("#group_batch_id").html("<option value=''>Select</option>");
	}
}

var vRules = {
	academic_year_id:{required:true},
	zone_id:{required:true},
	center_id:{required:true},
	category_id:{required:true},
	
};
var vMessages = {
	academic_year_id : {required :"Please select the acdemic year "},
	zone_id:{required:"Please select zone."},
	center_id:{required:"Please select center."},
	category_id:{required:"Please select category."},
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>assignalbum/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>assignalbum";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Assign Album";

 
</script>					
