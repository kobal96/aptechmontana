<?PHP
class Admissionmodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
		$this->db->insert($tbl_name,$data_array);
	 	//print_r($this->db->last_query());
	    //exit;
		$result_id = $this->db->insert_id();
		
		if($sendid == 1)
	 	{
	 		return $result_id;
	 	}
	}

	function getRecords($get=null){
		$table = "tbl_student_master";
		$table_id = 'i.student_id';
		$default_sort_column = 'i.student_id';
		$default_sort_order = 'desc';
		
		$condition = "";
		$condition .= "  1=1 ";
		
		$colArray = array('i.student_first_name','i.student_last_name','i.enrollment_no','z.zone_name','ct.center_name');
		$searchArray = array('i.student_first_name','i.student_last_name','i.enrollment_no','z.zone_name','ct.center_name');
		
		$page = $get['iDisplayStart'];											// iDisplayStart starting offset of limit funciton
		$rows = $get['iDisplayLength'];											// iDisplayLength no of records from the offset
		
		// sort order by column
		$sort = isset($get['iSortCol_0']) ? strval($colArray[$get['iSortCol_0']]) : $default_sort_column;  
		$order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

		for($i=0;$i<6;$i++)
		{
			
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!='')
			{
				$condition .= " && $searchArray[$i] like '%".$_GET['sSearch_'.$i]."%'";
			}
			
		}
		
		//echo "condition ".$condition;exit;
		
		$this -> db -> select('i.*,f.admission_fees_id,z.zone_name,ct.center_name');
		$this -> db -> from('tbl_student_master as i');
		$this -> db -> join('tbl_admission_fees as f', 'i.student_id  = f.student_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as ct', 'i.center_id  = ct.center_id', 'left');	
		// $this->db->where('e.inquiry_no != ""');

		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		// print_r($query->result());
		// exit();
		// for($i=0; $i<$query->num_rows();$i++){
		// 	$this -> db -> select('e.inquiry_no');
		// 	$this -> db -> from('tbl_inquiry_master as e');
		// 	$this->db->where('inquiry_master_id',$query->result()[$i]->lead_id);
		// 	$query2 = $this -> db -> get()->result_array();
		// 	// $query->result()[$i]->inquiry_no = $query2->result()[0]->inquiry_no;
		// 	print_r($query);
		// 	exit();
		// }

		
		$this -> db -> select('i.*,f.admission_fees_id,z.zone_name,ct.center_name');
		$this -> db -> from('tbl_student_master as i');
		$this -> db -> join('tbl_admission_fees as f', 'i.student_id  = f.student_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as ct', 'i.center_id  = ct.center_id', 'left');	
		$this->db->where("($condition)");
		$this->db->order_by($sort, $order);	

		$query1 = $this -> db -> get();
		
		// for($i=0; $i<$query1->num_rows();$i++){
		// 	$this -> db -> select('e.inquiry_no');
		// 	$this -> db -> from('tbl_inquiry_master as e');
		// 	$this->db->where('inquiry_master_id',$query1->result()[$i]->lead_id);
		// 	$query3 = $this -> db -> get();
		// 	$query1->result()[$i]->inquiry_no = $query3->result()[0]->inquiry_no;
		// }
		// print_r($query1->result());

		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
		
		
		//exit;
	} 

	function getCenterFeesLevel($condition){
		$this -> db -> select('f.fees_level_name,f.fees_level_id');
		$this -> db -> from('tbl_assign_fees as i');
		$this -> db -> join('tbl_fees_level_master as f', 'i.fees_level_id  = f.fees_level_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->group_by("i.fees_level_id");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		
	}

	function getMonths($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition order by cast(month_name as unsigned)");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getCenterFees($condition){
		
		$this -> db -> select('f.fees_id,f.fees_name, f.installment_no as max_installment_allowed');
		$this -> db -> from('tbl_assign_fees as i');
		$this -> db -> join('tbl_fees_master as f', 'i.fees_id  = f.fees_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('f.fees_name', 'asc');
		$this->db->group_by('f.fees_id');
		$query = $this -> db -> get();
		// echo "<pre>";
		// print_r($query->result_array());exit();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

	function getFeesDetails($condition){
		
		$this -> db -> select('fc.*,f.fees_component_master_name,f.is_mandatory,fm.installment_no');
		$this -> db -> from('tbl_fees_component_data as fc');
		$this -> db -> join('tbl_fees_component_master as f', 'fc.fees_component_id  = f.fees_component_master_id', 'left');
		$this->db->where("($condition)");
		$this -> db -> join('tbl_fees_master as fm', 'fc.fees_master_id  = fm.fees_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('fc.fees_component_data_id','asc');
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

	function getCenterGroups($condition){
		$this -> db -> select('i.*,g.group_master_name');
		$this -> db -> from('tbl_center_user_groups as i');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		
		$this->db->where("($condition)");
		// $this->db->group_by("i.group_id");

		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		
	}

	function getExportRecords($get){
		$table = "tbl_admission_fees";
		$table_id = 'i.admission_fees_id';
		$default_sort_column = 'i.admission_fees_id';
		$default_sort_order = 'desc';
		$condition = "";
		$condition .= "  1=1 ";
		
		//$colArray = array('feedback_name','feedback_email','feedback_contact','created_on');
		$colArray = array('s.student_first_name','s.student_last_name','s.enrollment_no','e.inquiry_no','z.zone_name','ct.center_name');
		$searchArray = array('s.student_first_name','s.student_last_name','s.enrollment_no','e.inquiry_no','z.zone_name','ct.center_name');
		
		// sort order by column
		$sort = $default_sort_column;  
		$order = $default_sort_order;
		
		for($i=0;$i<6;$i++){
			if(isset($get['sSearch_'.$i]) && $get['sSearch_'.$i]!=''){
				$condition .= " AND $colArray[$i] like '%".$get['sSearch_'.$i]."%'";
			}
		}
		
		$this -> db -> select('i.*, s.student_first_name,s.student_last_name,z.zone_name, s.enrollment_no,ct.center_name,e.inquiry_no');
		$this -> db -> from('tbl_admission_fees as i');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_zones as z', 'i.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as ct', 'i.center_id  = ct.center_id', 'left');	
		$this -> db -> join('tbl_inquiry_master as e', 's.inquiry_master_id  = e.inquiry_master_id', 'left');
		
		$this->db->where("($condition)");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			$totcount = $query -> num_rows();
			return array("query_result" => $query->result(), "totalRecords" => $totcount);
		}else{
			return array("totalRecords" => 0);
		}
	}
	
	function delrecord12($tbl_name,$tbl_id,$record_id,$status)
	{
		$data = array('status' => $status);
		 
		$this->db->where($tbl_id, $record_id);
		$this->db->update($tbl_name,$data); 
		 
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// function getFormdata($ID){
		
	// 	//$condition = "1=1 && i.product_id= ".$ID." ";
	// 	$this -> db -> select('i.*');
	// 	$this -> db -> from('tbl_inquiry_master as i');
	// 	$this->db->where('i.inquiry_master_id', $ID);
	// 	//$this->db->where("($condition)");
	
	// 	$query = $this -> db -> get();
	   
	// 	//print_r($this->db->last_query());
	// 	//exit;
	   
	// 	if($query -> num_rows() >= 1)
	// 	{
	// 		return $query->result();
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
		
	// }

	function getFormdata($ID){
		
		//$condition = "1=1 && i.product_id= ".$ID." ";
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_student_master as i');
		$this->db->where('i.student_id', $ID);
		//$this->db->where("($condition)");
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}	

	function getFormdata1($ID){
		
		//$condition = "1=1 && i.product_id= ".$ID." ";
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_student_details as i');
		$this->db->where('i.student_id', $ID);
		//$this->db->where("($condition)");
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}	


	function getFormdata2($table,$col,$ID){
		
		//$condition = "1=1 && i.product_id= ".$ID." ";
		$this -> db -> select('*');
		$this -> db -> from($table);
		$this->db->where($col, $ID);
		//$this->db->where("($condition)");
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}	

	function getDropdown($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$rec_type = "Inquiry";
		$this -> db -> where("rec_type",$rec_type);
	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}

	function getDropdown1($tbl_name,$tble_flieds){
	   
		$this -> db -> select($tble_flieds);
		$this -> db -> from($tbl_name);
		$status = "Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}

	function getLastEnrollmentNo($tbl_name){
		$this -> db -> select('enrollment_no');
		$this -> db -> from($tbl_name);
		$this->db->order_by("enrollment_no", "desc");
		$this->db->limit(1);  
	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getFormdataInquiryNo($ID){
		//$condition = "1=1 && i.product_id= ".$ID." ";
		$this -> db -> select('i.inquiry_no');
		$this -> db -> from('tbl_inquiry_master as i');
		$this->db->where('i.inquiry_master_id', $ID);
		//$this->db->where("($condition)");
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
	}	

	function getOptions($tbl_name,$tbl_id,$comp_id){
		//echo "Cat ID: ".$category_id;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptions1($tbl_name,$tbl_id,$comp_id,$tbl_id1,$comp_id1){
		//echo "Cat ID: ".$category_id;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
		$this -> db -> where($comp_id1,$tbl_id1);
		$status="Active";
		$this -> db -> where("status",$status);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptions2($tbl_name,$tbl_id,$comp_id){
		//echo "Cat ID: ".$category_id;
		//exit;
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getOptions3($tbl_name,$tbl_id,$comp_id){
	   
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where('fees_selection_type','Class');
		$this -> db -> where($comp_id,$tbl_id);
	
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
			
	}

	function getDetails($tbl_name,$tbl_id,$comp_id){
		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		$this -> db -> where($comp_id,$tbl_id);
	
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
	}

	function getdata($table,$condition = '1=1'){
        $sql = $this->db->query("Select * from $table where $condition");
        if($sql->num_rows()>0){
            return $sql->result_array();
        }else{
            return false;
        }
    }

    function getdata1($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	// function getFeesComponents($condition){
		
	// 	$this -> db -> select('i.*,f.fees_component_master_id, f.fees_component_master_name');
	// 	$this -> db -> from('tbl_fees_component_data as i');
	// 	$this -> db -> join('tbl_fees_component_master as f', 'i.fees_component_id  = f.fees_component_master_id', 'left');
		
	// 	$this->db->where("($condition)");
		
	// 	$query = $this -> db -> get();
		
	// 	if($query -> num_rows() >= 1)
	// 	{
	// 		return $query->result_array();
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
		
	// }

    function getPayemtDetails($condition){
		
		$this -> db -> select('i.*, s.student_id, s.student_first_name, s.student_last_name,s.enrollment_no, s.father_name, s.present_address, s.father_mobile_contact_no, s.father_email_id, ct.categoy_name, cr.course_name, b.batch_name, g.group_master_name, a.academic_year_master_name, cn.center_name, cn.center_address, cn.center_contact_no, cn.center_email_id, cn.center_code,fp.*');
		$this -> db -> from('tbl_admission_fees as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cr', 'i.course_id  = cr.course_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id = g.group_master_id', 'left');
		$this -> db -> join('tbl_centers as cn', 'i.center_id = cn.center_id', 'left');
		$this -> db -> join('tbl_fees_payment_details as fp', 'i.admission_fees_id = fp.admission_fees_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

	function getdata_orderby_limit1($table, $fields, $condition = '1=1', $order_by){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition $order_by limit 1");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getFeesComponents($condition){
		
		$this -> db -> select('i.*,f.fees_component_master_id, f.fees_component_master_name');
		$this -> db -> from('tbl_fees_component_data as i');
		$this -> db -> join('tbl_fees_component_master as f', 'i.fees_component_id  = f.fees_component_master_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}

    function updateRecord($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 
}
?>
