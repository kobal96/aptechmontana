<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Admission extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		// $this->load->helper('erp_setting');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);

		$this->load->model('admissionmodel', '', TRUE);
	}

	function index()
	{
		if (!empty($_SESSION["webadmin"])) {
			$this->load->view('template/header.php');
			$this->load->view('admission/index');
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}
	
	
	/*new code end*/
	function fetch($id=null)
	{
		//$_GET['album_id'] = $_GET['id'];
		//echo $id;exit;
		$get_result = $this->admissionmodel->getRecords($_GET);
		$result = array();
		$result["sEcho"] = $_GET['sEcho'];
		
		$result["iTotalRecords"] = $get_result['totalRecords']; //	iTotalRecords get no of total recors
		$result["iTotalDisplayRecords"] = $get_result['totalRecords']; //  iTotalDisplayRecords for display the no of records in data table.
		$items = array();
		if(!empty($get_result['query_result'])){
			for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
				$temp = array();
				array_push($temp, $get_result['query_result'][$i]->student_first_name);
				array_push($temp, $get_result['query_result'][$i]->student_last_name);
				array_push($temp, $get_result['query_result'][$i]->enrollment_no);
				array_push($temp, $get_result['query_result'][$i]->zone_name);
				array_push($temp, $get_result['query_result'][$i]->center_name);	
				
				$viewPaymentDetails="";
				// if($this->privilegeduser->hasPrivilege("")){
					$viewPaymentDetails.= '<a href="paymentdetails/index?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->admission_fees_id) , '+/', '-_') , '=') . '" title="Payment Details">View Payment Details</a>';
				// }

				$actionCol1 = "";
				$actionCol1.= '<a href="admission/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->student_id) , '+/', '-_') , '=') . '" title="Edit"><i class="fa fa-edit"></i></a> ';
				
				array_push($temp, $viewPaymentDetails);
				array_push($temp, $actionCol1);
				
				
				array_push($items, $temp);
			}
		}	

		$result["aaData"] = $items;
		echo json_encode($result);
		exit;
	}


	function addEdit($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = [];
			$result1 = [];
			$inquiryNo = [];
			$result['inquiries'] = $this->admissionmodel->getDropdown("tbl_inquiry_master","inquiry_master_id,lead_id,student_first_name,student_last_name");
			$result['academicyear'] = $this->admissionmodel->getDropdown1("tbl_academic_year_master","academic_year_master_id,academic_year_master_name");
			for($i=0;$i<sizeof($result['inquiries']);$i++){
				$result1['inquiry_nos'] = $this->admissionmodel->getFormdataInquiryNo($result['inquiries'][$i]->lead_id);
				$result['no'][] = $result1['inquiry_nos'];
			}
			$getenrollmentNo = $this->admissionmodel->getLastEnrollmentNo("tbl_student_master");
			if(!empty($getenrollmentNo)){
				$enrollmentNoPrefix = $getenrollmentNo[0]->enrollment_no +1;
				$result['enrollment_no'] = sprintf("%06d", $enrollmentNoPrefix);
			}
			else{
				$result['enrollment_no'] = sprintf("%06d",000001);
			}
			if(isset($_SESSION['student_id']))
			{
				$inquiry_id = $this->admissionmodel->getFormdata2('tbl_student_master','student_id',$_SESSION['student_id']);
				$result['studentImageDetails'] = $inquiry_id;
				$result['getStudentdetails'] = $this->admissionmodel->getFormdata2('tbl_inquiry_master','inquiry_master_id',$inquiry_id[0]->inquiry_master_id);
				$result['getStateName'] = $this->admissionmodel->getDetails("tbl_states",$result['getStudentdetails'][0]->state_id,"state_id");
				$result['getCityName'] = $this->admissionmodel->getDetails("tbl_cities",$result['getStudentdetails'][0]->city_id,"city_id");
			}
			$result['zones'] = $this->admissionmodel->getDropdown1("tbl_zones","zone_id,zone_name");
			$result['categories'] = $this->admissionmodel->getDropdown1("tbl_categories","category_id,categoy_name");
			$result['batch'] = $this->admissionmodel->getDropdown1("tbl_batch_master","batch_id,batch_name");
			$result['countries'] = $this->admissionmodel->getDropdown1("tbl_countries","country_id,country_name");
			// print_r($record_id);
			// exit();
			$result['details'] = $this->admissionmodel->getFormdata($record_id);
			// print_r($result);
			// exit();
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$result['getStateName'] = $this->admissionmodel->getDetails("tbl_states",$result['details'][0]->state_id,"state_id");
				$result['getCityName'] = $this->admissionmodel->getDetails("tbl_cities",$result['details'][0]->city_id,"city_id");
			}

			$result['otherdetails'] = $this->admissionmodel->getFormdata1($record_id);
			$result['fees_level'] = $this->admissionmodel->getDropdown1("tbl_fees_level_master","fees_level_id,fees_level_name");
			$result['login_name'] = $_SESSION["webadmin"][0]->first_name .' '. $_SESSION["webadmin"][0]->last_name;
			
			$this->load->view('template/header.php');
			$this->load->view('admission/addEdit', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function convertToAdmission($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";
			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $url_prams['id'];
			}
			$result = [];
			if(isset($_SESSION['student_id']))
			{
				$inquiry_id = $this->admissionmodel->getFormdata2('tbl_student_master','student_id',$_SESSION['student_id']);
				$result['studentImageDetails'] = $inquiry_id;
			}
			$result['inquiries'] = $this->admissionmodel->getDropdown("tbl_inquiry_master","inquiry_master_id,lead_id,student_first_name,student_last_name");
			$result['academicyear'] = $this->admissionmodel->getDropdown1("tbl_academic_year_master","academic_year_master_id,academic_year_master_name");
			for($i=0;$i<sizeof($result['inquiries']);$i++){
				$result1['inquiry_nos'] = $this->admissionmodel->getFormdataInquiryNo($result['inquiries'][$i]->lead_id);
				$result['no'][] = $result1['inquiry_nos'];
			}
			$getenrollmentNo = $this->admissionmodel->getLastEnrollmentNo("tbl_student_master");
			if(!empty($getenrollmentNo)){
				$enrollmentNoPrefix = $getenrollmentNo[0]->enrollment_no +1;
				$result['enrollment_no'] = sprintf("%06d", $enrollmentNoPrefix);
			}
			else{
				$result['enrollment_no'] = sprintf("%06d",000001);
			}
			$result['zones'] = $this->admissionmodel->getDropdown1("tbl_zones","zone_id,zone_name");
			$result['categories'] = $this->admissionmodel->getDropdown1("tbl_categories","category_id,categoy_name");
			$result['batch'] = $this->admissionmodel->getDropdown1("tbl_batch_master","batch_id,batch_name");
			// print_r($record_id);
			// exit();
			$result['details'] = $this->admissionmodel->getDetails("tbl_inquiry_master",$record_id,"inquiry_master_id");
			// print_r($result);
			// exit();
			if(!empty($result['details'][0]->lead_id)){
				$result['getInquiryNo'] = $this->admissionmodel->getDetails("tbl_inquiry_master",$result['details'][0]->lead_id,"inquiry_master_id");
			}
			$result['countries'] = $this->admissionmodel->getDropdown1("tbl_countries","country_id,country_name");
			$result['getStateName'] = $this->admissionmodel->getDetails("tbl_states",$result['details'][0]->state_id,"state_id");
			$result['getCityName'] = $this->admissionmodel->getDetails("tbl_cities",$result['details'][0]->city_id,"city_id");
			$result['fees_level'] = $this->admissionmodel->getDropdown1("tbl_fees_level_master","fees_level_id,fees_level_name");
			// $result['otherdetails'] = $this->admissionmodel->getFormdata1($record_id);
			// print_r($result);
			// exit();
			$this->load->view('template/header.php');
			$this->load->view('admission/converttoadmission', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	public function getCenters(){
		$result = $this->admissionmodel->getOptions("tbl_centers",$_REQUEST['zone_id'],"zone_id");
		//echo "<pre>";
		//print_r($result);
		//exit;
		
		$option = '';
		$center_id = '';
		
		if(isset($_REQUEST['center_id']) && !empty($_REQUEST['center_id'])){
			$center_id = $_REQUEST['center_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->center_id == $center_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->center_id.'" '.$sel.' >'.$result[$i]->center_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getCourses(){
		$result = $this->admissionmodel->getOptions("tbl_courses",$_REQUEST['category_id'],"category_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$course_id = '';
		
		if(isset($_REQUEST['course_id']) && !empty($_REQUEST['course_id'])){
			$course_id = $_REQUEST['course_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->course_id == $course_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->course_id.'" '.$sel.' >'.$result[$i]->course_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getBatch(){
		// print_r($_REQUEST);
		// exit();
		$result = $this->admissionmodel->getOptions1("tbl_batch_master",$_REQUEST['center_id'],"center_id",$_REQUEST['course_id'],"course_id");
		
		$option = '';
		$batch_id = '';
		
		if(isset($_REQUEST['batch_id']) && !empty($_REQUEST['batch_id'])){
			$batch_id = $_REQUEST['batch_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->batch_id == $batch_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->batch_id.'" '.$sel.' >'.$result[$i]->batch_name.'</option>';
			}
		}
		// print_r($option);exit();
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function getBatchDetails(){
		$result = $this->admissionmodel->getOptions("tbl_batch_master",$_REQUEST['batch_id'],"batch_id");

		echo json_encode(array("status"=>"success","result"=>$result));
		exit;
	}

	function getState(){
		$result = $this->admissionmodel->getOptions("tbl_states",$_REQUEST['country_id'],"country_id");
		/*echo "<pre>";
		print_r($result);
		exit;*/
		
		$option = '';
		$state_id = '';
		
		if(isset($_REQUEST['state_id']) && !empty($_REQUEST['state_id'])){
			$state_id = $_REQUEST['state_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->state_id == $state_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->state_id.'" '.$sel.' >'.$result[$i]->state_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}
	public function getCity(){
		// print_r($_REQUEST);
		// exit();
		$result = $this->admissionmodel->getOptions2("tbl_cities",$_REQUEST['state_id'],"state_id");
		
		$option = '';
		$city_id = '';
		
		if(isset($_REQUEST['city_id']) && !empty($_REQUEST['city_id'])){
			$city_id = $_REQUEST['city_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]->city_id == $city_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]->city_id.'" '.$sel.' >'.$result[$i]->city_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getFees(){
		// print_r($_REQUEST);exit();
		$condition = "f.fees_selection_type='".$_REQUEST['feesselectiontype']."'  && i.fees_level_id='".$_REQUEST['feeslevelid']."' && i.zone_id='".$_REQUEST['zone_id']."' && i.center_id='".$_REQUEST['center_id']."' && i.status='Active' "; 
					
		if($_REQUEST['feesselectiontype'] == 'Class'){
			$condition .=" && i.category_id='".$_REQUEST['categoryid']."' && i.course_id='".$_REQUEST['courseid']."' ";
		}else{
			$condition .=" && i.group_id='".$_REQUEST['groupid']."'  ";
		}
		
		$result = $this->admissionmodel->getCenterFees($condition);
		// print_r($result);exit();
		$option = '';
		$fees_id = '';
		
		if(isset($_REQUEST['fees_id']) && !empty($_REQUEST['fees_id'])){
			$fees_id = $_REQUEST['fees_id'];
		}
		
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$sel = ($result[$i]['fees_id'] == $fees_id) ? 'selected="selected"' : '';
				$option .= '<option value="'.$result[$i]['fees_id'].'" '.$sel.' >'.$result[$i]['fees_name'].' </option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	public function getFessDetails(){
		$condition = "fees_master_id='".$_REQUEST['fees_id']."' && f.status='Active' "; 
		$result = $this->admissionmodel->getFeesDetails($condition);
		foreach ($result as $key => $value) {
			$getGstDetail = $this->admissionmodel->getdata1("tbl_gst_master", "gst_master_id, tax", "status='Active' &&  gst_master_id='".$result[$key]['gst_tax_id']."'");
			$result[$key]['tax'] = $getGstDetail[0]['tax'];
		}
		// print_r($result);exit();
		echo json_encode(array("status"=>"success","result"=>$result));
		exit;
	}

	function getInquiryDetails(){
		$result = $this->admissionmodel->getDetails("tbl_inquiry_master",$_REQUEST['inquiry_master_id'],"inquiry_master_id");

		$getStateName = $this->admissionmodel->getDetails("tbl_states",$result[0]->state_id,"state_id");
		$getCityName = $this->admissionmodel->getDetails("tbl_cities",$result[0]->city_id,"city_id");
		echo json_encode(array("status"=>"success","result"=>$result,'getStateName'=>$getStateName,'getCityName'=>$getCityName));
		exit;
	}

	function getchildInfo(){
		$result = $this->admissionmodel->getDetails("tbl_student_master",$_REQUEST['student_id'],"student_id");
		// print_r($result);
		// exit();
		$otherresult = $this->admissionmodel->getDetails("tbl_student_details",$_REQUEST['student_id'],"student_id");
		$getStateName = $this->admissionmodel->getDetails("tbl_states",$result[0]->state_id,"state_id");
		$getCityName = $this->admissionmodel->getDetails("tbl_cities",$result[0]->city_id,"city_id");
		echo json_encode(array("status"=>"success","result"=>$result,"otherresult"=>$otherresult,'getStateName'=>$getStateName,'getCityName'=>$getCityName));
		exit;
	}

	function getSiblingName(){
		$result = $this->admissionmodel->getDetails("tbl_student_master",$_REQUEST['enrollment_no'],"enrollment_no");
		// print_r($result);
		// exit();
		echo json_encode(array("status"=>"success","result"=>$result));
		exit;
	}

	function submitForm()
	{ 
		// print_r($_POST);
		// exit();
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			$condition = "zone_id='".$_POST['zone_id']."' &&  center_id='".$_POST['center_id']."'  &&  inquiry_master_id='".$_POST['inquiry_master_id']."' ";
			if(isset($_POST['student_id']) && $_POST['student_id'] > 0)
			{
				$condition .= " &&  student_id != ".$_POST['student_id'];
			}
			
			$check_name = $this->admissionmodel->getdata("tbl_student_master",$condition);
			if(!empty($check_name)){
				echo json_encode(array("success"=>"0",'msg'=>'Record Already Present!'));
				exit;
			}

			if($_POST['batch_start_date']!= '' && $_POST['batch_end_date'] != ''){
				if(strtotime($_POST['programme_start_date']) >= strtotime($_POST['batch_start_date']) && strtotime($_POST['programme_start_date']) <= strtotime($_POST['batch_end_date'])){
				}
				else{
					echo json_encode(array("success"=>"0",'msg'=>'Program start date not correct!'));
					exit;
				}
				if(strtotime($_POST['programme_end_date']) >= strtotime($_POST['batch_start_date']) && strtotime($_POST['programme_end_date']) <= strtotime($_POST['batch_end_date'])){
				}
				else{
					echo json_encode(array("success"=>"0",'msg'=>'Program end date not correct!'));
					exit;
				}
			}
			if(strtotime($_POST['programme_start_date']) > strtotime($_POST['programme_end_date'])){
				echo json_encode(array("success"=>"0",'msg'=>'Program start date not greater than program end date!'));
				exit;
			}

			if($_POST['has_attended_preschool_before'] == 'Yes'){
				if($_POST['preschool_name'] == '' || $_POST['preschool_name'] == null){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Preschool Name!'));
					exit;
				}
			}
			
			if (!empty($_POST['student_id'])) {
				$data_array = array();			
				$student_id = $_POST['student_id'];
		 		
				$data_array['inquiry_master_id'] = (!empty($_POST['inquiry_master_id'])) ? $_POST['inquiry_master_id'] : '';
				$data_array['enrollment_no'] = (!empty($_POST['enrollment_no'])) ? $_POST['enrollment_no'] : '';
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
				$data_array['manual_ref_no'] = (!empty($_POST['manual_ref_no'])) ? $_POST['manual_ref_no'] : 0;
				$data_array['student_first_name'] = (!empty($_POST['student_first_name'])) ? $_POST['student_first_name'] : '';
				$data_array['student_last_name'] = (!empty($_POST['student_last_name'])) ? $_POST['student_last_name'] : '';
				$data_array['dob'] = (!empty($_POST['dob'])) ? date("Y-m-d", strtotime($_POST['dob'])) : '';
				$data_array['age_on'] = (!empty($_POST['programme_start_date'])) ? date("Y-m-d", strtotime($_POST['programme_start_date'])) : '';
				$data_array['dob_year'] = (!empty($_POST['dob_year'])) ? $_POST['dob_year'] : 0;
				$data_array['dob_month'] = (!empty($_POST['dob_month'])) ? $_POST['dob_month'] : 0;
				$data_array['nationality'] = (!empty($_POST['nationality'])) ? $_POST['nationality'] : '';				
				$data_array['religion'] = (!empty($_POST['religion'])) ? $_POST['religion'] : '';			
				$data_array['mother_tongue'] = (!empty($_POST['mother_tongue'])) ? $_POST['mother_tongue'] : '';
				$data_array['other_languages'] = (!empty($_POST['other_languages'])) ? $_POST['other_languages'] : '';
				$data_array['has_attended_preschool_before'] = (!empty($_POST['has_attended_preschool_before'])) ? $_POST['has_attended_preschool_before'] : '';
				$data_array['preschool_name'] = (!empty($_POST['preschool_name'])) ? $_POST['preschool_name'] : '';
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->admissionmodel->updateRecord('tbl_student_master', $data_array,'student_id',$student_id);


				$data_array1 = array();
				$data_array1['admission_date'] = (!empty($_POST['admission_date'])) ? date("Y-m-d", strtotime($_POST['admission_date'])) : '';
				$data_array1['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
				$data_array1['enrollment_no'] = (!empty($_POST['enrollment_no'])) ? $_POST['enrollment_no'] : '';
				$data_array1['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
				$data_array1['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : '';
				$data_array1['batch_id'] = (!empty($_POST['batch_id'])) ? $_POST['batch_id'] : '';
				$data_array1['programme_start_date'] = (!empty($_POST['programme_start_date'])) ? date("Y-m-d", strtotime($_POST['programme_start_date'])) : '';
				$data_array1['programme_end_date'] = (!empty($_POST['programme_end_date'])) ? date("Y-m-d", strtotime($_POST['programme_end_date'])) : '';
				$data_array1['updated_on'] = date("Y-m-d H:i:s");
				$data_array1['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result1 = $this->admissionmodel->updateRecord('tbl_student_details', $data_array1,'student_id',$student_id);
				
				
			}else {
				
				$data_array = array();
				$data_array['inquiry_master_id'] = (!empty($_POST['inquiry_master_id'])) ? $_POST['inquiry_master_id'] : '';
				$data_array['enrollment_no'] = (!empty($_POST['enrollment_no'])) ? $_POST['enrollment_no'] : '';
				$data_array['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
				$data_array['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
				$data_array['manual_ref_no'] = (!empty($_POST['manual_ref_no'])) ? $_POST['manual_ref_no'] : 0;
				$data_array['student_first_name'] = (!empty($_POST['student_first_name'])) ? $_POST['student_first_name'] : '';
				$data_array['student_last_name'] = (!empty($_POST['student_last_name'])) ? $_POST['student_last_name'] : '';
				$data_array['dob'] = (!empty($_POST['dob'])) ? date("Y-m-d", strtotime($_POST['dob'])) : '';
				$data_array['age_on'] = (!empty($_POST['programme_start_date'])) ? date("Y-m-d", strtotime($_POST['programme_start_date'])) : '';
				$data_array['dob_year'] = (!empty($_POST['dob_year'])) ? $_POST['dob_year'] : 0;
				$data_array['dob_month'] = (!empty($_POST['dob_month'])) ? $_POST['dob_month'] : 0;
				$data_array['nationality'] = (!empty($_POST['nationality'])) ? $_POST['nationality'] : '';				
				$data_array['religion'] = (!empty($_POST['religion'])) ? $_POST['religion'] : '';			
				$data_array['mother_tongue'] = (!empty($_POST['mother_tongue'])) ? $_POST['mother_tongue'] : '';
				$data_array['other_languages'] = (!empty($_POST['other_languages'])) ? $_POST['other_languages'] : '';
				$data_array['has_attended_preschool_before'] = (!empty($_POST['has_attended_preschool_before'])) ? $_POST['has_attended_preschool_before'] : '';
				$data_array['preschool_name'] = (!empty($_POST['preschool_name'])) ? $_POST['preschool_name'] : '';
				$data_array['created_on'] = date("Y-m-d H:i:s");
				$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$result = $this->admissionmodel->insertData('tbl_student_master', $data_array, '1');
				$data_array1 = array();
				$data_array1['student_id'] = (!empty($result)) ? $result : '';
				$data_array1['admission_date'] = (!empty($_POST['admission_date'])) ? date("Y-m-d", strtotime($_POST['admission_date'])) : '';
				$data_array1['academic_year_id'] = (!empty($_POST['academic_year_id'])) ? $_POST['academic_year_id'] : '';
				$data_array1['enrollment_no'] = (!empty($_POST['enrollment_no'])) ? $_POST['enrollment_no'] : '';
				$data_array1['category_id'] = (!empty($_POST['category_id'])) ? $_POST['category_id'] : '';
				$data_array1['course_id'] = (!empty($_POST['course_id'])) ? $_POST['course_id'] : '';
				$data_array1['batch_id'] = (!empty($_POST['batch_id'])) ? $_POST['batch_id'] : '';
				$data_array1['programme_start_date'] = (!empty($_POST['programme_start_date'])) ? date("Y-m-d", strtotime($_POST['programme_start_date'])) : '';
				$data_array1['programme_end_date'] = (!empty($_POST['programme_end_date'])) ? date("Y-m-d", strtotime($_POST['programme_end_date'])) : '';
				$data_array1['created_on'] = date("Y-m-d H:i:s");
				$data_array1['created_by'] = $_SESSION["webadmin"][0]->user_id;
				$data_array1['updated_on'] = date("Y-m-d H:i:s");
				$data_array1['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				$result1 = $this->admissionmodel->insertData('tbl_student_details', $data_array1, '1');
				
			}
		
		
			if (!empty($result)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.',
				));
				if (empty($_POST['student_id'])) {
					$_SESSION['student_id'] = $result;
				}
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	
	}

	function submitFamilyInfoForm(){
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			if(!isset($_SESSION['student_id']) && empty($_POST['student_id']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Enter Child Information First!'));
				exit;
			}
			
			if (isset($_SESSION['student_id']) || !empty($_POST['student_id'])) {
				$data_array = array();	
				if(isset($_SESSION['student_id'])){		
					$student_id = $_SESSION['student_id'];
				}else{
					$student_id = $_POST['student_id'];
				}
		 		
				$data_array['father_name'] = (!empty($_POST['father_name'])) ? $_POST['father_name'] : '';
				$data_array['father_prof'] = (!empty($_POST['father_prof'])) ? $_POST['father_prof'] : '';
				$data_array['father_languages'] = (!empty($_POST['father_languages'])) ? $_POST['father_languages'] : '';
				$data_array['father_nationality'] = (!empty($_POST['father_nationality'])) ? $_POST['father_nationality'] : '';
				$data_array['mother_name'] = (!empty($_POST['mother_name'])) ? $_POST['mother_name'] : '';
				$data_array['mother_prof'] = (!empty($_POST['mother_prof'])) ? $_POST['mother_prof'] : '';
				$data_array['mother_languages'] = (!empty($_POST['mother_languages'])) ? $_POST['mother_languages'] : '';
				$data_array['mother_nationality'] = (!empty($_POST['mother_nationality'])) ? $_POST['mother_nationality'] : '';
				$data_array['sibling1_id'] = (!empty($_POST['sibling1_id'])) ? $_POST['sibling1_id'] : 0;
				$data_array['sibling2_id'] = (!empty($_POST['sibling2_id'])) ? $_POST['sibling2_id'] : 0;
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$familydataresult = $this->admissionmodel->updateRecord('tbl_student_master', $data_array,'student_id',$student_id);
				
			}
		
			if (!empty($familydataresult)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.',
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	}

	function submitContactInfoForm(){
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			if(!isset($_SESSION['student_id']) && empty($_POST['student_id']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Enter Child Information First!'));
				exit;
			}
			
			if (isset($_SESSION['student_id']) || !empty($_POST['student_id'])) {
				$data_array = array();	
				if(isset($_SESSION['student_id'])){		
					$student_id = $_SESSION['student_id'];
				}else{
					$student_id = $_POST['student_id'];
				}
		 		
				$data_array['present_address'] = (!empty($_POST['present_address'])) ? $_POST['present_address'] : '';
				$data_array['address1'] = (!empty($_POST['address1'])) ? $_POST['address1'] : '';
				$data_array['address2'] = (!empty($_POST['address2'])) ? $_POST['address2'] : '';
				$data_array['address3'] = (!empty($_POST['address3'])) ? $_POST['address3'] : '';
				$data_array['country_id'] = (!empty($_POST['country_id'])) ? $_POST['country_id'] : '';
				$data_array['state_id'] = (!empty($_POST['state_id'])) ? $_POST['state_id'] : '';
				$data_array['city_id'] = (!empty($_POST['city_id'])) ? $_POST['city_id'] : '';
				$data_array['pincode'] = (!empty($_POST['pincode'])) ? $_POST['pincode'] : '';
				$data_array['mother_home_contact_no'] = (!empty($_POST['mother_home_contact_no'])) ? $_POST['mother_home_contact_no'] : '';
				$data_array['mother_office_contact_no'] = (!empty($_POST['mother_office_contact_no'])) ? $_POST['mother_office_contact_no'] : '';
				$data_array['mother_mobile_contact_no'] = (!empty($_POST['mother_mobile_contact_no'])) ? $_POST['mother_mobile_contact_no'] : '';
				$data_array['mother_email_id'] = (!empty($_POST['mother_email_id'])) ? $_POST['mother_email_id'] : '';
				$data_array['father_home_contact_no'] = (!empty($_POST['father_home_contact_no'])) ? $_POST['father_home_contact_no'] : '';
				$data_array['father_office_contact_no'] = (!empty($_POST['father_office_contact_no'])) ? $_POST['father_office_contact_no'] : '';
				$data_array['father_mobile_contact_no'] = (!empty($_POST['father_mobile_contact_no'])) ? $_POST['father_mobile_contact_no'] : '';
				$data_array['father_email_id'] = (!empty($_POST['father_email_id'])) ? $_POST['father_email_id'] : '';
				$data_array['emergency_contact_name'] = (!empty($_POST['emergency_contact_name'])) ? $_POST['emergency_contact_name'] : '';
				$data_array['emergency_contact_tel_no'] = (!empty($_POST['emergency_contact_tel_no'])) ? $_POST['emergency_contact_tel_no'] : '';				$data_array['emergency_contact_mobile_no'] = (!empty($_POST['emergency_contact_mobile_no'])) ? $_POST['emergency_contact_mobile_no'] : '';				$data_array['emergency_contact_relationship'] = (!empty($_POST['emergency_contact_relationship'])) ? $_POST['emergency_contact_relationship'] : '';
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$familydataresult = $this->admissionmodel->updateRecord('tbl_student_master', $data_array,'student_id',$student_id);
				
			}
		
			if (!empty($familydataresult)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.',
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	}

	function submitAutorizedInfoForm(){
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			if(empty($_POST['student_id']) && empty($_FILES["img_file1"]["name"]) && empty($_POST['prev_value1']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Please upload profile pic!'));
				exit;
			}

			if(!isset($_SESSION['student_id']) && empty($_POST['student_id']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Enter Child Information First!'));
				exit;
			}

			if (isset($_SESSION['student_id']) || !empty($_POST['student_id'])) {
				$data_array = array();	
				if(isset($_SESSION['student_id'])){		
					$student_id = $_SESSION['student_id'];
				}else{
					$student_id = $_POST['student_id'];
				}
				$img_file_value1 = "";
				if(isset($_FILES) && isset($_FILES["img_file1"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/authorized_person_image/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["img_file1"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("img_file1"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$img_file_value1 = $image_data['upload_data']['file_name'];
					}
					$image1 = $this->admissionmodel->getFormdata($student_id);
					if(is_array($image1) && !empty($image1[0]->authperson1_img) && file_exists(DOC_ROOT_FRONT."/images/authorized_person_image/".$image1[0]->authperson1_img))
					{
						@unlink(DOC_ROOT_FRONT."/images/authorized_person_image/".$image1[0]->authperson1_img);
					}				
				}else{
					$img_file_value1 = $_POST['prev_value1'];
				}

				$img_file_value2 = "";
				if(isset($_FILES) && isset($_FILES["img_file2"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/authorized_person_image/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["img_file2"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("img_file2"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$img_file_value2 = $image_data['upload_data']['file_name'];
					}
					$image2 = $this->admissionmodel->getFormdata($student_id);
					if(is_array($image2) && !empty($image2[0]->authperson2_img) && file_exists(DOC_ROOT_FRONT."/images/authorized_person_image/".$image2[0]->authperson2_img))
					{
						@unlink(DOC_ROOT_FRONT."/images/authorized_person_image/".$image2[0]->authperson2_img);
					}				
				}else{
					$img_file_value2 = $_POST['prev_value2'];
				}

				$img_file_value3 = "";
				if(isset($_FILES) && isset($_FILES["img_file3"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/authorized_person_image/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["img_file3"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("img_file3"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$img_file_value3 = $image_data['upload_data']['file_name'];
					}
					$image3 = $this->admissionmodel->getFormdata($student_id);
					if(is_array($image3) && !empty($image3[0]->authperson3_img) && file_exists(DOC_ROOT_FRONT."/images/authorized_person_image/".$image3[0]->authperson3_img))
					{
						@unlink(DOC_ROOT_FRONT."/images/authorized_person_image/".$image3[0]->authperson3_img);
					}				
				}else{
					$img_file_value3 = $_POST['prev_value3'];
				}
				$data_array['authperson1_img'] = $img_file_value1;
				$data_array['authperson2_img'] = $img_file_value2;
				$data_array['authperson3_img'] = $img_file_value3;

				$data_array['auth_person1_to_collect'] = (!empty($_POST['auth_person1_to_collect'])) ? $_POST['auth_person1_to_collect'] : '';
				$data_array['auth_person1_to_collect_relation'] = (!empty($_POST['auth_person1_to_collect_relation'])) ? $_POST['auth_person1_to_collect_relation'] : '';
				$data_array['auth_person1_to_collect_gender'] = (!empty($_POST['auth_person1_to_collect_gender'])) ? $_POST['auth_person1_to_collect_gender'] : '';

				$data_array['auth_person2_to_collect'] = (!empty($_POST['auth_person2_to_collect'])) ? $_POST['auth_person2_to_collect'] : '';
				$data_array['auth_person2_to_collect_relation'] = (!empty($_POST['auth_person2_to_collect_relation'])) ? $_POST['auth_person2_to_collect_relation'] : '';
				$data_array['auth_person2_to_collect_gender'] = (!empty($_POST['auth_person2_to_collect_gender'])) ? $_POST['auth_person2_to_collect_gender'] : '';

				$data_array['auth_person3_to_collect'] = (!empty($_POST['auth_person3_to_collect'])) ? $_POST['auth_person3_to_collect'] : '';
				$data_array['auth_person3_to_collect_relation'] = (!empty($_POST['auth_person3_to_collect_relation'])) ? $_POST['auth_person3_to_collect_relation'] : '';
				$data_array['auth_person3_to_collect_gender'] = (!empty($_POST['auth_person3_to_collect_gender'])) ? $_POST['auth_person3_to_collect_gender'] : '';
				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$authorizeddataresult = $this->admissionmodel->updateRecord('tbl_student_master', $data_array,'student_id',$student_id);
				
			}
			if (!empty($authorizeddataresult)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.',
					'img1'=>$img_file_value1,
					'img2'=>$img_file_value2,
					'img3'=>$img_file_value3,
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	}

	function submitdocumentInfoForm(){
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		
			if(empty($_POST['student_id']) && empty($_FILES["doc_file1"]["name"]) && empty($_POST['doc_value1']) && isset($_POST['Submit']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Please upload immunization certificate!'));
				exit;
			}

			if(empty($_POST['student_id']) && empty($_FILES["doc_file2"]["name"]) && empty($_POST['doc_value2']) && isset($_POST['Submit']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Please upload birth certificate!'));
				exit;
			}

			if(empty($_POST['student_id']) && empty($_FILES["doc_file3"]["name"]) && empty($_POST['doc_value3']) && isset($_POST['Submit']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Please upload child photo!'));
				exit;
			}

			if(empty($_POST['student_id']) && empty($_FILES["doc_file4"]["name"]) && empty($_POST['doc_value4']) && isset($_POST['Submit']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Please upload  family photo!'));
				exit;
			}

			if(empty($_POST['student_id']) && empty($_FILES["doc_file5"]["name"]) && empty($_POST['doc_value5']) && isset($_POST['Submit']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Please upload address proof!'));
				exit;
			}

			if(empty($_POST['student_id']) && empty($_FILES["doc_file6"]["name"]) && empty($_POST['doc_value6']) && isset($_POST['Submit']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Please upload profile form!'));
				exit;
			}

			if(!isset($_SESSION['student_id']) && empty($_POST['student_id']))
			{
				echo json_encode(array("success"=>"0",'msg'=>'Enter Child Information First!'));
				exit;
			}
			
			if (isset($_SESSION['student_id']) || !empty($_POST['student_id'])) {
				$data_array = array();	
				if(isset($_SESSION['student_id'])){		
					$student_id = $_SESSION['student_id'];
				}else{
					$student_id = $_POST['student_id'];
				}
				$doc_value1 = "";
				if(isset($_FILES) && isset($_FILES["doc_file1"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/admission_documents/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["doc_file1"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("doc_file1"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$doc_value1 = $image_data['upload_data']['file_name'];
					}
					$doc1 = $this->admissionmodel->getFormdata($student_id);
					if(is_array($doc1) && !empty($doc1[0]->duly_filled_admission_form) && file_exists(DOC_ROOT_FRONT."/images/admission_documents/".$doc1[0]->duly_filled_admission_form))
					{
						@unlink(DOC_ROOT_FRONT."/images/admission_documents/".$doc1[0]->duly_filled_admission_form);
					}				
				}else{
					$doc_value1 = $_POST['doc_value1'];
				}
				$doc_value2 = "";
				if(isset($_FILES) && isset($_FILES["doc_file2"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/admission_documents/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["doc_file2"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("doc_file2"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$doc_value2 = $image_data['upload_data']['file_name'];
					}
					$doc2 = $this->admissionmodel->getFormdata($student_id);
					if(is_array($doc2) && !empty($doc2[0]->birth_certificate) && file_exists(DOC_ROOT_FRONT."/images/admission_documents/".$doc2[0]->birth_certificate))
					{
						@unlink(DOC_ROOT_FRONT."/images/admission_documents/".$doc2[0]->birth_certificate);
					}				
				}else{
					$doc_value2 = $_POST['doc_value2'];
				}
				$doc_value3 = "";
				if(isset($_FILES) && isset($_FILES["doc_file3"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/admission_documents/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["doc_file3"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("doc_file3"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$doc_value3 = $image_data['upload_data']['file_name'];
					}
					$doc3 = $this->admissionmodel->getFormdata($student_id);
					if(is_array($doc3) && !empty($doc3[0]->profile_pic) && file_exists(DOC_ROOT_FRONT."/images/admission_documents/".$doc3[0]->profile_pic))
					{
						@unlink(DOC_ROOT_FRONT."/images/admission_documents/".$doc3[0]->profile_pic);
					}				
				}else{
					$doc_value3 = $_POST['doc_value3'];
				}
				$doc_value4 = "";
				if(isset($_FILES) && isset($_FILES["doc_file4"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/admission_documents/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["doc_file4"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("doc_file4"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$doc_value4 = $image_data['upload_data']['file_name'];
					}
					$doc4 = $this->admissionmodel->getFormdata($student_id);
					if(is_array($doc4) && !empty($doc4[0]->family_photo) && file_exists(DOC_ROOT_FRONT."/images/admission_documents/".$doc4[0]->family_photo))
					{
						@unlink(DOC_ROOT_FRONT."/images/admission_documents/".$doc4[0]->family_photo);
					}				
				}else{
					$doc_value4 = $_POST['doc_value4'];
				}
				$doc_value5 = "";
				if(isset($_FILES) && isset($_FILES["doc_file5"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/admission_documents/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["doc_file5"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("doc_file5"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$doc_value5 = $image_data['upload_data']['file_name'];
					}
					$doc5 = $this->admissionmodel->getFormdata($student_id);
					if(is_array($doc5) && !empty($doc5[0]->address_proof) && file_exists(DOC_ROOT_FRONT."/images/admission_documents/".$doc5[0]->address_proof))
					{
						@unlink(DOC_ROOT_FRONT."/images/admission_documents/".$doc5[0]->address_proof);
					}				
				}else{
					$doc_value5 = $_POST['doc_value5'];
				}
				$doc_value6 = "";
				if(isset($_FILES) && isset($_FILES["doc_file6"]["name"]))
				{
					$config['upload_path'] = DOC_ROOT_FRONT."/images/admission_documents/";
					$config['max_size']    = '1000000000';
					$config['allowed_types'] = '*';
					//$file_name = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $files['doc_file']['name'][$key]);
					$config['file_name']     = md5(uniqid("100_ID", true)) . str_replace(' ', '-', $_FILES["doc_file6"]["name"]);
					
					$this->load->library('upload', $config);
					if (!$this->upload->do_upload("doc_file6"))
					{
						$image_error = array('error' => $this->upload->display_errors());
						echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
						exit;
					}
					else
					{
						$image_data = array('upload_data' => $this->upload->data());
						$doc_value6 = $image_data['upload_data']['file_name'];
					}
					$doc6 = $this->admissionmodel->getFormdata($student_id);
					if(is_array($doc6) && !empty($doc6[0]->duly_filled_child_profile_form) && file_exists(DOC_ROOT_FRONT."/images/admission_documents/".$doc6[0]->duly_filled_child_profile_form))
					{
						@unlink(DOC_ROOT_FRONT."/images/admission_documents/".$doc6[0]->duly_filled_child_profile_form);
					}				
				}else{
					$doc_value6 = $_POST['doc_value6'];
				}
				$data_array['duly_filled_admission_form'] = $doc_value1;
				$data_array['birth_certificate'] = $doc_value2;
				$data_array['profile_pic'] = $doc_value3;
				$data_array['family_photo'] = $doc_value4;
				$data_array['address_proof'] = $doc_value5;
				$data_array['duly_filled_child_profile_form'] = $doc_value6;

				$data_array['updated_on'] = date("Y-m-d H:i:s");
				$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
				
				$documentresult = $this->admissionmodel->updateRecord('tbl_student_master', $data_array,'student_id',$student_id);
				
			}
			if (!empty($documentresult)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.',
					'doc1'=>$doc_value1,
					'doc2'=>$doc_value2,
					'doc3'=>$doc_value3,
					'doc4'=>$doc_value4,
					'doc5'=>$doc_value5,
					'doc6'=>$doc_value6,
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	}

	function submitfeesInfoForm(){
		// print_r($_POST);
		// exit();
		if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
			
			if(!isset($_SESSION['student_id']) && !$_POST['student_id'])
			{
				echo json_encode(array("success"=>"0",'msg'=>'Enter Child Information First!'));
				exit;
			}


			if($_POST['is_instalment'] == 'Yes'){
				if($_POST['no_of_installments'] > $_POST['max_installment'])
				{
					echo json_encode(array("success"=>"0",'msg'=>'Installment No should not greater than Max Installment!'));
					exit;
				}
				for ($i=0; $i < $_POST['no_of_installments'] ; $i++) { 
					if($_POST['instalment_due_date'][$i] == ''){
						echo json_encode(array("success"=>"0",'msg'=>'Please Enter Installment Due date!'));
					exit;
					}
				}
				for ($i=0; $i < $_POST['no_of_installments'] ; $i++) { 
					if($_POST['instalment_collected_amount'][0] == 0){
						echo json_encode(array("success"=>"0",'msg'=>'Please Enter Installment Amount!'));
					exit;
					}
				}
			}
			else{
				$_POST['fees_remark'] = $_POST['fees_remarkl'];
			}

			if(!isset($_POST['payment_mode']) == 'Cheque')
			{
				if(empty($_POST['bank_name'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Bank Name!'));
				exit;
				}
				if(empty($_POST['cheque_no'])){
					echo json_encode(array("success"=>"0",'msg'=>'EnterCheque No!'));
				exit;
				}
				if(empty($_POST['cheque_date'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Cheque Date!'));
				exit;
				}
			}

			if(!isset($_POST['payment_mode']) == 'Netbanking')
			{
				if(empty($_POST['bank_name'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Bank Name!'));
				exit;
				}
				if(empty($_POST['transaction_id'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Transaction No!'));
				exit;
				}
				if(empty($_POST['transaction_date'])){
					echo json_encode(array("success"=>"0",'msg'=>'Enter Transaction Date!'));
				exit;
				}
			}
			
			if (isset($_SESSION['student_id']) || $_POST['student_id']) {
				$data_array = array();	
				if(isset($_SESSION['student_id'])){		
					$student_id = $_SESSION['student_id'];
				}
				else{
					$student_id = $_POST['student_id'];
				}

				$getCenter = $this->admissionmodel->getDetails("tbl_student_master",$student_id,"student_id");
				$getCategory = $this->admissionmodel->getDetails("tbl_student_details",$student_id,"student_id");
				if(!empty($getCenter) && !empty($getCategory)){
		 		
					$data_array['academic_year_id'] = $getCategory[0]->academic_year_id;
					$data_array['student_id'] = $student_id;
					$data_array['fees_id'] = (!empty($_POST['fees_id'])) ? $_POST['fees_id'] : '';
					$data_array['fees_type'] = 'Class';
					$data_array['zone_id'] = $getCenter[0]->zone_id ;
					$data_array['center_id'] = $getCenter[0]->center_id;
					$data_array['category_id'] = $getCategory[0]->category_id ;
					$data_array['course_id'] = $getCategory[0]->course_id ;
					$data_array['batch_id'] = $getCategory[0]->batch_id;
					$data_array['group_id'] = 0 ;
					$data_array['is_instalment'] = (!empty($_POST['is_instalment'])) ? $_POST['is_instalment'] : '';
					$total_collected_amount = 0;
					$total_remainig_amount = 0;
					if($_POST['is_instalment'] == 'Yes'){
						$data_array['no_of_installments'] = (!empty($_POST['no_of_installments'])) ? $_POST['no_of_installments'] : '';
						for ($i=0; $i < $_POST['no_of_installments'] ; $i++) { 
							$total_collected_amount = $total_collected_amount + $_POST['instalment_collected_amount'][$i];
							$total_remainig_amount = $total_remainig_amount + $_POST['instalment_remaining_amount'][$i];
						}
					// print_r($total_collected_amount);exit();
						$data_array['fees_amount_collected'] = (!empty($total_collected_amount)) ? $total_collected_amount : 0;
						$data_array['fees_remaining_amount'] = (!empty($total_remainig_amount)) ? $total_remainig_amount : 0;
					}
					else{
						$data_array['no_of_installments'] = 0;
						$data_array['fees_amount_collected'] = (!empty($_POST['total_amount'])) ? $_POST['total_amount'] : '';
						$data_array['fees_remaining_amount'] =  0;
					}
					$data_array['fees_total_amount'] = (!empty($_POST['fees_total_amount'])) ? $_POST['fees_total_amount'] : '';
					$data_array['discount_amount'] = (!empty($_POST['discount_amount_rs'])) ? $_POST['discount_amount_rs'] : 0;
					$total_gst_amount = 0;
					for ($g=0; $g <sizeof($_POST['gst_amount']) ; $g++) {
						// print_r($_POST['gst_amount'][$g]);
						$total_gst_amount = $total_gst_amount + $_POST['gst_amount'][$g];
					}
					// print_r($total_gst_amount);exit();
					$data_array['gst_amount'] = $total_gst_amount;
					$data_array['total_amount'] = (!empty($_POST['total_amount'])) ? $_POST['total_amount'] : '';
					// $data_array['fees_amount_collected'] = (!empty($_POST['fees_amount_collected'])) ? $_POST['fees_amount_collected'] : '';
					$data_array['fees_remark'] = (!empty($_POST['fees_remark'])) ? $_POST['fees_remark'] : '';
					$data_array['fees_approved_accepted_by_name'] = (!empty($_POST['fees_approved_accepted_by_name'])) ? $_POST['fees_approved_accepted_by_name'] : '';
					$data_array['created_on'] = date("Y-m-d H:i:s");
					$data_array['created_by'] = $_SESSION["webadmin"][0]->user_id;
					$data_array['updated_on'] = date("Y-m-d H:i:s");
					$data_array['updated_by'] = $_SESSION["webadmin"][0]->user_id;
					
					$payfeesdataresult = $this->admissionmodel->insertData('tbl_admission_fees', $data_array, '1');
					//saving in component
					if(!empty($payfeesdataresult)){
						//get fess components
						$component_condition = "f.status='Active' && i.fees_master_id='".$_POST['fees_id']."' ";
						$getFeesComponents = $this->admissionmodel->getFeesComponents($component_condition);
						// echo "<pre>";
						// print_r($getFeesComponents);exit();
						
						if(!empty($getFeesComponents)){
							for($i=0; $i < sizeof($getFeesComponents); $i++){
								$getGstDetail = $this->admissionmodel->getdata1("tbl_gst_master", "gst_master_id, tax", "status='Active' &&  gst_master_id='".$getFeesComponents[$i]['gst_tax_id']."' ");
								$fees_component = array();
								$fees_component['admission_fees_id'] = $payfeesdataresult;
								$fees_component['fees_id'] = $_POST['fees_id'];
								$fees_component['fees_component_id'] = $getFeesComponents[$i]['fees_component_master_id'];
								$fees_component['fees_component_id_value'] = $getFeesComponents[$i]['component_fees'];
								// print_r($_POST);exit();
								if($getFeesComponents[$i]['is_discountable'] == 'Yes'){
									if($_POST['discount_amount_percentage'] != ''){
										$discount_amount = ($getFeesComponents[$i]['component_fees'] * $_POST['discount_amount_percentage'])/100;
									}
									else{
										$discount_amount = 0;
									}
									if($getFeesComponents[$i]['component_fees'] < $discount_amount){
										$fees_component['discount_amount'] = 0;
										$fees_component['discount_percentage'] = 0;
									}
									else{
										if($_POST['discount_amount_percentage'] == ''){
											$_POST['discount_amount_percentage'] = 0;
										}
										$fees_component['discount_amount'] = ($getFeesComponents[$i]['component_fees'] * $_POST['discount_amount_percentage'])/100;
										$fees_component['discount_percentage'] = $_POST['discount_amount_percentage'];
									}
								}
								else{
									$fees_component['discount_amount'] = 0;
									$fees_component['discount_percentage'] = 0;
								}
								$discountamount = $getFeesComponents[$i]['component_fees'] - $fees_component['discount_amount'];
								// print_r($getGstDetail[0]['tax']);exit();
								if($getFeesComponents[$i]['is_gst'] == 'Yes'){
									$fees_component['gst_amount'] = ($discountamount * $getGstDetail[0]['tax'])/100;
									$tax = explode(' ', $getGstDetail[0]['tax']);
									$fees_component['gst_percentage'] = $tax[0];
								}
								else{
									$fees_component['gst_amount'] = 0;
									$fees_component['gst_percentage'] = 0;
								}
								$fees_component['sub_total'] = ($discountamount) - $fees_component['gst_amount'];
								// if(!empty($get_data)){
								// 	$this->teachersapimodel->updateRecord('tbl_admission_fees_components', $fees_component, "admission_fees_component_id='".$fees_component['admission_fees_id']."' ");
								// }
								// else{
									$this->admissionmodel->insertData('tbl_admission_fees_components', $fees_component, 1);
								// }
							}
						}
					}
					if($_POST['is_instalment'] == 'Yes'){
						$data_array2 = array();
						for($j=0;$j<$_POST['no_of_installments'];$j++){
							$data_array2['academic_year_id'] = $getCategory[0]->academic_year_id;
							$data_array2['student_id'] = $student_id;
							$data_array2['admission_fees_id'] = (!empty($payfeesdataresult)) ? $payfeesdataresult : '';
							$data_array2['fees_id'] = (!empty($_POST['fees_id'])) ? $_POST['fees_id'] : '';
							$data_array2['fees_type'] = 'Class';
							$installment_due_date = explode('-',$_POST['instalment_due_date'][$j]);
							// print_r($installment_due_date);
							// exit();
							$data_array2['instalment_due_date'] = (!empty($_POST['instalment_due_date'][$j])) ?  $installment_due_date[2].'-'.$installment_due_date[1].'-'.$installment_due_date[0] : '';
							$data_array2['instalment_amount'] = (!empty($_POST['instalment_amount'])) ? $_POST['instalment_amount'] : '';
							$data_array2['instalment_collected_amount'] = (!empty($_POST['instalment_collected_amount'][$j])) ? $_POST['instalment_collected_amount'][$j] : 0;
							$data_array2['instalment_remaining_amount'] = (!empty($_POST['instalment_remaining_amount'][$j])) ? $_POST['instalment_remaining_amount'][$j] : '';
							if($_POST['instalment_remaining_amount'][$j] == 0){
								$data_array2['instalment_status'] = 'Paid';
							}
							else if($_POST['instalment_collected_amount'][$j] == 0 ||  $_POST['instalment_collected_amount'][$j] == 0.00){
								$data_array2['instalment_status'] = 'Pending';
							}
							else if($_POST['instalment_amount'] > $_POST['instalment_collected_amount'][$j]){
								$data_array2['instalment_status'] = 'Partial';
							}
							$data_array2['created_on'] = date("Y-m-d H:i:s");
							$data_array2['created_by'] = $_SESSION["webadmin"][0]->user_id;
							$data_array2['updated_on'] = date("Y-m-d H:i:s");
							$data_array2['updated_by'] = $_SESSION["webadmin"][0]->user_id;
							$result2 = $this->admissionmodel->insertData('tbl_admission_fees_instalments', $data_array2, '1');
							if(isset($_POST['paymentprocess'])){
								// print_r($total_collected_amount);exit();
								$data_array1 = array();
								$data_array1['admission_fees_id'] = (!empty($payfeesdataresult)) ? $payfeesdataresult : '';
								if($_POST['is_instalment'] == 'No'){
									$data_array1['admission_fees_instalment_id'] = 0;
								}
								else{
									$data_array1['admission_fees_instalment_id'] = $result2;
								}
								$data_array1['payment_mode'] = (!empty($_POST['payment_mode'])) ? $_POST['payment_mode'] : '';
								if($_POST['payment_mode'] == 'Cheque'){
									$data_array1['transaction_date'] = (!empty($_POST['cheque_date'])) ? date("Y-m-d", strtotime($_POST['cheque_date'])) : '';
								}
								elseif($_POST['payment_mode'] == 'Netbanking'){
									$data_array1['transaction_date'] = (!empty($_POST['transaction_date'])) ? date("Y-m-d", strtotime($_POST['transaction_date'])) : '';
								}
								else{
									$data_array1['transaction_date'] =  date("Y-m-d");
								}
								$data_array1['cheque_no'] = (!empty($_POST['cheque_no'])) ? $_POST['cheque_no'] : '';
								
								$data_array1['transaction_id'] = (!empty($_POST['transaction_id'])) ? $_POST['transaction_id'] : '';
								$data_array1['fees_remark'] = (!empty($_POST['fees_remark'])) ? $_POST['fees_remark'] : '';
								$data_array1['fees_approved_accepted_by_name'] = (!empty($_POST['fees_approved_accepted_by_name'])) ? $_POST['fees_approved_accepted_by_name'] : '';
								$data_array1['bank_name'] = (!empty($_POST['bank_name'])) ? $_POST['bank_name'] : '';
								$data_array1['created_on'] = date("Y-m-d H:i:s");
								$data_array1['created_by'] = $_SESSION["webadmin"][0]->user_id;
								$data_array1['updated_on'] = date("Y-m-d H:i:s");
								$data_array1['updated_by'] = $_SESSION["webadmin"][0]->user_id;
								$result1 = $this->admissionmodel->insertData('tbl_fees_payment_details', $data_array1, '1');

								if(!empty($payfeesdataresult)){
									$student_array = array();
									if($total_collected_amount < $_POST['mandatory_component_amount']){
										$student_array['status'] = 'In-active';
									}
									else{
										$status_data = $this->admissionmodel->getdata1("tbl_student_master", "status", "student_id='".$student_id."' ");
										if($status_data[0]['status'] != 'In-active'){
											$student_array['status'] = 'Active';
										}else{
											$student_array['status'] = 'In-active';
										}
									}
									$this->admissionmodel->updateRecord('tbl_student_master', $student_array, "student_id",$student_id);
									
									$student_array1 = array();
									if($total_collected_amount < $_POST['mandatory_component_amount']){
										$student_array1['status'] = 'In-active';
									}
									else{
										$status_data = $this->admissionmodel->getdata1("tbl_student_details", "status", "student_id='".$student_id."' ");
										if($status_data[0]['status'] != 'In-active'){
											$student_array1['status'] = 'Active';
										}else{
											$student_array1['status'] = 'In-active';
										}
									}
									$this->admissionmodel->updateRecord('tbl_student_details', $student_array1, "student_id",$student_id);
								}
							}
							else{
								$flag = 0;
							}
						}
					}
					if(isset($_POST['paymentprocess'])){
						if($_POST['is_instalment'] == 'No'){
							$data_array1 = array();
							$data_array1['admission_fees_id'] = (!empty($payfeesdataresult)) ? $payfeesdataresult : '';
							if($_POST['is_instalment'] == 'No'){
								$data_array1['admission_fees_instalment_id'] = 0;
							}
							else{
								$data_array1['admission_fees_instalment_id'] = $result2;
							}
							$data_array1['payment_mode'] = (!empty($_POST['payment_mode'])) ? $_POST['payment_mode'] : '';
							if($_POST['payment_mode'] == 'Cheque'){
								$data_array1['transaction_date'] = (!empty($_POST['cheque_date'])) ? date("Y-m-d", strtotime($_POST['cheque_date'])) : '';
							}
							elseif($_POST['payment_mode'] == 'Netbanking'){
								$data_array1['transaction_date'] = (!empty($_POST['transaction_date'])) ? date("Y-m-d", strtotime($_POST['transaction_date'])) : '';
							}
							else{
								$data_array1['transaction_date'] =  date("Y-m-d");
							}
							$data_array1['cheque_no'] = (!empty($_POST['cheque_no'])) ? $_POST['cheque_no'] : '';
							
							$data_array1['transaction_id'] = (!empty($_POST['transaction_id'])) ? $_POST['transaction_id'] : '';
							$data_array1['fees_remark'] = (!empty($_POST['fees_remark'])) ? $_POST['fees_remark'] : '';
							$data_array1['fees_approved_accepted_by_name'] = (!empty($_POST['fees_approved_accepted_by_name'])) ? $_POST['fees_approved_accepted_by_name'] : '';
							$data_array1['bank_name'] = (!empty($_POST['bank_name'])) ? $_POST['bank_name'] : '';
							$data_array1['created_on'] = date("Y-m-d H:i:s");
							$data_array1['created_by'] = $_SESSION["webadmin"][0]->user_id;
							$data_array1['updated_on'] = date("Y-m-d H:i:s");
							$data_array1['updated_by'] = $_SESSION["webadmin"][0]->user_id;
							$result1 = $this->admissionmodel->insertData('tbl_fees_payment_details', $data_array1, '1');
							if(!empty($payfeesdataresult)){
									$student_array = array();
									if($_POST['fees_amount_collected'] < $_POST['mandatory_component_amount']){
										$student_array['status'] = 'In-active';
									}
									else{
										$status_data = $this->admissionmodel->getdata1("tbl_student_master", "status", "student_id='".$student_id."' ");
										if($status_data[0]['status'] != 'In-active'){
											$student_array['status'] = 'Active';
										}else{
											$student_array['status'] = 'In-active';
										}
									}
									$this->admissionmodel->updateRecord('tbl_student_master', $student_array, "student_id",$student_id);
									
									$student_array1 = array();
									if($_POST['fees_amount_collected'] < $_POST['mandatory_component_amount']){
										$student_array1['status'] = 'In-active';
									}
									else{
										$status_data = $this->admissionmodel->getdata1("tbl_student_details", "status", "student_id='".$student_id."' ");
										if($status_data[0]['status'] != 'In-active'){
											$student_array1['status'] = 'Active';
										}else{
											$student_array1['status'] = 'In-active';
										}
									}
									$this->admissionmodel->updateRecord('tbl_student_details', $student_array1, "student_id",$student_id);
								}
						}
					}
					else{
						$flag = 0;
					}
				}
				
			}
		
			if (!empty($payfeesdataresult)) {
				echo json_encode(array(
					'success' => '1',
					'msg' => 'Record Added/Updated Successfully.',
					'payfeesdataresult' => $payfeesdataresult,
					'flag' => $flag,
					'student_id' => $student_id
				));
				exit;
			}
			else{
				echo json_encode(array(
					'success' => '0',
					'msg' => 'Problem in data update.'
				));
				exit;
			}
			
		}
		else {
			return false;
		}
	}

	function getPayemtDetails(){
		$getPayemtDetails = $this->admissionmodel->getPayemtDetails("i.admission_fees_id='".$_REQUEST['admission_fees_id']."' ");

		$student_name = (!empty($getPayemtDetails[0]['student_first_name'])) ? $getPayemtDetails[0]['student_first_name']." ".$getPayemtDetails[0]['student_last_name'] : '';
			$academic_year = (!empty($getPayemtDetails[0]['academic_year_master_name'])) ? $getPayemtDetails[0]['academic_year_master_name'] : '';
			$category_name = (!empty($getPayemtDetails[0]['categoy_name'])) ? $getPayemtDetails[0]['categoy_name'] : '';
			$course_name = (!empty($getPayemtDetails[0]['course_name'])) ? $getPayemtDetails[0]['course_name'] : '';
			$batch_name = (!empty($getPayemtDetails[0]['batch_name'])) ? $getPayemtDetails[0]['batch_name'] : '';
			$group_name = (!empty($getPayemtDetails[0]['group_master_name'])) ? $getPayemtDetails[0]['group_master_name'] : '';
			$center_name = (!empty($getPayemtDetails[0]['center_name'])) ? $getPayemtDetails[0]['center_name'] : '';
			$center_address = (!empty($getPayemtDetails[0]['center_address'])) ? $getPayemtDetails[0]['center_address'] : '';
			$center_contact_no = (!empty($getPayemtDetails[0]['center_contact_no'])) ? $getPayemtDetails[0]['center_contact_no'] : '';
			$center_email_id = (!empty($getPayemtDetails[0]['center_email_id'])) ? $getPayemtDetails[0]['center_email_id'] : '';
			$center_code = (!empty($getPayemtDetails[0]['center_code'])) ? $getPayemtDetails[0]['center_code'] : '';
			
			$intallment_yes_no = (!empty($getPayemtDetails[0]['is_instalment'])) ? $getPayemtDetails[0]['is_instalment'] : '';
			$installment_nos = (!empty($getPayemtDetails[0]['no_of_installments'])) ? $getPayemtDetails[0]['no_of_installments'] : '';
			$discount_amount = (!empty($getPayemtDetails[0]['discount_amount'])) ? $getPayemtDetails[0]['discount_amount'] : '';
			$total_fees = (!empty($getPayemtDetails[0]['fees_total_amount'])) ? $getPayemtDetails[0]['fees_total_amount'] : '';
			$total_amount = (!empty($getPayemtDetails[0]['total_amount'])) ? $getPayemtDetails[0]['total_amount'] : '';
			$collected_fees = (!empty($getPayemtDetails[0]['fees_amount_collected'])) ? $getPayemtDetails[0]['fees_amount_collected'] : '';
			$remaining_fees = (!empty($getPayemtDetails[0]['fees_remaining_amount'])) ? $getPayemtDetails[0]['fees_remaining_amount'] : '';
			
			$getRecieptNo = $this->admissionmodel->getdata_orderby_limit1("tbl_fees_payment_receipt", "fees_payment_receipt_id", "1=1", "order by fees_payment_receipt_id desc");
			
			$reciept_id = (!empty($getRecieptNo)) ? ($getRecieptNo[0]['fees_payment_receipt_id'] + 1) : 1;
			
			$reciept_no = "";
			if(!empty($center_code)){
				$reciept_no .= $center_code."/";
			}
			if(!empty($academic_year)){
				$reciept_no .= $academic_year."/";
			}
			
			$reciept_no .= $reciept_id;
			
			
			if(!empty($getPayemtDetails[0]['father_email_id'])){
			
			
			$message = '';
			$message  = '<html><body>';	
			$message .= '<div style="width:750px;padding:0 30px;">
								<div style="margin:10px;">
								<img src="https://www.aptechmontanapreschool.com/wp-content/uploads/2018/06/aptech-logo-1.png" alt="The Montana International Pre school Pvt. Ltd." title="The Montana International Pre school Pvt. Ltd." style="width:180px;"/>
								</div>';
								$message .= '<div>
									<h1 style="text-align:center;color:#000;font-size:25px;margin:10px 0;padding:0;">'.$center_name.'</h1>
									<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;">'.$center_address.'</p>
									<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><a href="mailto:'.$center_email_id.' " style="color:#000;text-decoration:none;">'.$center_email_id.'</a></p>
								</div>';
								
								$message .= '<div style="height:1px;background:#000;margin:10px 0">&nbsp;</div>
								<h4 style="text-align: center;font-size:20px;margin:10px 0;padding:0">Booking Confirmation</h4>
								<table cellpadding="0" cellspacing="0" border="0" style="border:0px none;width:700px;">
									<tr>
										<td style="width:350px;padding:10px;text-align:left;">
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Name:</strong>'.$getPayemtDetails[0]['student_first_name'].' '.$getPayemtDetails[0]['student_last_name'].'</p> 
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Address:</strong>'.$getPayemtDetails[0]['present_address'].'</p> 
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Student ID:</strong>'.$getPayemtDetails[0]['enrollment_no'].'</p> 
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Program Family:</strong>'.$getPayemtDetails[0]['categoy_name'].'</p>
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Program Name:</strong>'.$getPayemtDetails[0]['course_name'].'</p>
										</td>
										<td style="width:350px;padding:10px;text-align:left;">
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Number:</strong>'.$getPayemtDetails[0]['father_mobile_contact_no'].'</p> 
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Date:</strong>'.date("d-M-Y").'</p> 
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Email ID:</strong>'.$getPayemtDetails[0]['father_email_id'].'</p> 
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Currency:</strong>INR</p>
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Receipt Number: '.$reciept_no.'</strong></p>
											<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Receipt Date: '.date("d-M-Y").'</strong></p>
										</td>
									</tr>
								</table>
								<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
									<tr>
										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Fee Component	</th>
										<th style="padding:10px;text-align:left;border-top:1px solid #000;border-bottom:1px solid #000">Amount</th>	
									</tr>';
									//get fess components
									$component_condition = "f.status='Active' && i.fees_master_id='".$getPayemtDetails[0]['fees_id']."' ";
									$getFeesComponents = $this->admissionmodel->getFeesComponents($component_condition);
									if(!empty($getFeesComponents)){
										for($i=0; $i < sizeof($getFeesComponents); $i++){
											$message .= '<tr>						
												<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getFeesComponents[$i]['fees_component_master_name'].'</strong></td>
												<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getFeesComponents[$i]['component_fees'].'</strong></td>					
											</tr>';
										}
									}
										
									
									$message .= '<tr>
										<td style="padding:10px;text-align:left;font-size:14px"><strong>Total Fees Amount</strong></td>
										<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$total_fees.' /-</strong></td>						
									</tr>
									
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px"><strong>Discount Amount</strong></td>
										<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$discount_amount.' /-</strong></td>						
									</tr>
									
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px"><strong>Total Amount</strong></td>
										<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$total_amount.' /-</strong></td>						
									</tr>
									
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px"><strong>Collected Amount</strong></td>
										<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$collected_fees.' /-</strong></td>						
									</tr>
									
									<tr>
										<td style="padding:10px;text-align:left;font-size:14px"><strong>Remaining Amount</strong></td>
										<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$remaining_fees.' /-</strong></td>						
									</tr>
									
								</table>';
								
								$message .= '
									
									<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:750px;margin:20px auto;">
											<tr>
												<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Installment Number</th>
												<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Planned Installment Date</th>
												<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Installment Amount</th>
												<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Paid Amount</th>		
												<th style="padding:5px;text-align:center;font-size:14px;border-bottom:1px solid #000">Remaining Amount</th>
											</tr>
											<tr>
												<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>
												<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>
												<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">(In INR)</td>		
												<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">(In INR)</td>		
												<td style="padding:5px;text-align:right;font-size:14px;border-bottom:1px solid #000">(In INR)</td>
											</tr>';
										//get installment details
										$getPayemtInstallmentDetails = $this->admissionmodel->getdata("tbl_admission_fees_instalments", "admission_fees_id='".$_REQUEST['admission_fees_id']."' " );	
										if(!empty($getPayemtInstallmentDetails)){
											$count = 1;
											for($i=0; $i < sizeof($getPayemtInstallmentDetails); $i++){
												
												$message .= '<tr>
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.$count.'</td>
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.date("M Y", strtotime($getPayemtInstallmentDetails[$i]['instalment_due_date'])).'</td>
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.$getPayemtInstallmentDetails[$i]['instalment_amount'].'</td>	
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.$getPayemtInstallmentDetails[$i]['instalment_collected_amount'].'</td>		
													<td style="padding:5px;text-align:right;font-size:14px;border-bottom:1px solid #000">'.$getPayemtInstallmentDetails[$i]['instalment_remaining_amount'].'</td>
												</tr>';
												$count++;
											}
										}
										
										$message .= '</table>';
								
								$message .= '<p style="font-size:15px;margin:4px 0;">*CHEQUES SUBJECT TO REALISATION</p>
								<p style="font-size:15px;margin:4px 0;">*NO CHANGE OR CASH CAN BE TAKEN IN EXCHANGE OF GIFT VOUCHER</p>
								<p style="font-size:15px;margin:4px 0;">THIS RECEIPT MUST BE PRODUCED WHEN DEMANDED</p>
								<p style="font-size:15px;margin:4px 0;">FEES ONCE PAID ARE NOT REFUNDABLE</p>
								<p style="font-size:15px;margin:4px 0;">SUBJECT TO TERMS AND CONDITION PRINTED OVERLEAF THE BOOKING CONFIRMATION</p>
								<p style="font-size:15px;margin:4px 0;">1) This Booking Confirmation provides provisional admission to the child.</p>
								<p style="font-size:15px;margin:4px 0;">2) Ensure timely payments as mentioned in the Booking confirmation.</p>
								<p style="font-size:15px;margin:4px 0;">3) Taxes, as applicable, at the time of payment will be charged extra.</p>
								<p style="font-size:15px;margin:4px 0;">4) Parents are required to inform the school if there is any change in their address and telephone numbers.</p>
								<p style="font-size:15px;margin:4px 0;">5) The parents are responsible for timely drop and pick up of child.</p>
								<p style="font-size:15px;margin:4px 0;">6) Please refer detailed code of conduct and policy Guidelines sent on your mail.</p>
								<p style="font-size:15px;margin:4px 0 50px 0;">I have read and understood the code of conduct and payment terms / installment plan mentioned above and agree to abide by them and also the terms and conditions printed overleaf.</p>
								<div style="width:200px;float:right;text-align:center">
									<p style="font-size:15px;margin:4px 0;">The Montana <br/>International Preschool<br/> Pvt Ltd </p>
								</div>
								<div style="clear:both;margin-bottom:80px">&nbsp;</div>
								<div style="width:200px;float:left;text-align:center">
									<p style="font-size:15px;margin:4px 0;"><strong>Signature of Parent / Guardian </strong></p>
								</div>
								<div style="width:200px;float:right;text-align:center">
									<p style="font-size:15px;margin:4px 0 ;"><strong>AUTHORISED <br/>SIGNATORY</strong></p>
								</div>
								<div style="clear:both;">&nbsp;</div>
									<p style="font-size:15px;margin:15px 0;">For any feedback or suggestions please write to us at <a href="mailto:feedback@montanapreschool.com" style="color:#000;text-decoration:none;">feedback@montanapreschool.com</a></p>
									<p style="font-size:15px;margin:15px 0;">Registered Office : 402, Sagar Tech Plaza, B wing, Andheri-Kurla Road, Sakinaka, Andheri East, Mumbai - 400 072 Maharashtra, India, </p>
									<p style="font-size:15px;margin:15px 0;"><a href="https://www.facebook.com/montanapreschool/" target="_blank" style="color:#000;text-decoration:none;">https://www.facebook.com/montanapreschool/</a>
									<p style="font-size:15px;margin:4px 0;">Disclaimer : Taxes will be charged extra, as applicable, on the date of payment.</p>
									</p>
								</div>';
			$message .=  '</body></html>';
			
			$this->email->clear();
			$this->email->from("info@attoinfotech.in"); // change it to yours
			$subject = "";
			$subject = "Aptech Montana - Admission Confirmation";
			$this->email->to($getPayemtDetails[0]['father_email_id']);
			//$this->email->to("danish.akhtar@attoinfotech.com");
			$this->email->subject($subject);
			$this->email->message($message);
			$bfilePath = DOC_ROOT."/images/M_attachment.pdf";
			$this->email->attach($bfilePath);
			$checkemail = $this->email->send();
			$this->email->clear(TRUE);

			//genrate invoice pdf
			ini_set('memory_limit','100M');
			//echo "here";exit;
			//$this->load->library('m_pdf');
			//echo "here1";exit;
			$date_v = date("d_m_Y_H_i_s");
			$pdf_filename = "paymentinvoice_".$date_v.".pdf";
			$invoice_pdfFilePath = DOC_ROOT."images/payment_invoices/$pdf_filename";
			$file_path = base_url()."/images/payment_invoices/$pdf_filename";
			
			$html_invoice = '';
			$html_invoice = '<div style="width:750px;padding:0 30px;">
								<div style="margin:10px;">
									<img src="https://www.aptechmontanapreschool.com/wp-content/uploads/2018/06/aptech-logo-1.png" alt="The Montana International Pre school Pvt. Ltd." title="The Montana International Pre school Pvt. Ltd." style="width:180px;"/>
								</div>
								<div>';
								//get center information
								
									$html_invoice .= '<h1 style="text-align:center;color:#000;font-size:25px;margin:10px 0;padding:0;">'.$center_name.'</h1>
									<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;">'.$center_address.' Helpline: '.$center_contact_no.'</p>
									<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><a href="mailto:'.$center_email_id.' " style="color:#000;text-decoration:none;">'.$center_email_id.'</a></p>';
									
								$html_invoice .= '</div>
								<div>
									<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
										<tr>
											<td>
												<table cellpadding="0" cellspacing="0" border="0" style="width:700px;">
													<tr>
														<th style="width:150px;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Date</th>
														<th style="width:150px;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Enrol No</th>
														<th style="width:150px;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Name</th>
														<th style="width:150px;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Class/Group</th>
														<th style="width:150px;padding:10px;border-bottom:1px solid #000">Section(Batch)</th>
													</tr>
													<tr>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>'.date("d-M-Y").'</strong></td>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getPayemtDetails[0]['enrollment_no'].'</strong></td>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$student_name.'</strong></td>';
													if($getPayemtDetails[0]['fees_type'] == 'Class'){		
														$html_invoice .= '<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$course_name.'</strong></td>
														<td style="padding:10px;font-size:14px"><strong>'.$batch_name.'</strong></td>';
													}else{
														$html_invoice .= '<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$group_name.'</strong></td>
														<td style="padding:10px;font-size:14px"><strong>-</strong></td>';
													}
														
													$html_invoice .= '</tr>
													<tr>
														<td colspan="2" style="padding:10px;text-align:left;font-size:14px">
															<p style="color:#000;font-size:14px;margin:3px 0;">Father Name: <strong>'.$getPayemtDetails[0]['father_name'].'</strong></p>
															<p style="color:#000;font-size:14px;margin:3px 0;">Address: '.$getPayemtDetails[0]['present_address'].'</p>
														</td>						
														<td colspan="2"  style="padding:10px;text-align:left;font-size:14px"> 
															<p style="color:#000;font-size:14px;margin:3px 0;">Payment Mode: <strong>'.$getPayemtDetails[0]['payment_mode'].'</strong></p>
														</td>						
														<td style="padding:10px;text-align:left;font-size:14px"> 
															<p style="color:#000;font-size:14px;margin:3px 0;">Receipt No:<strong>'.$reciept_no.'</strong></p>
														</td>
													</tr>					
												</table>
											</td>
										</tr>
									</table>
									
									<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;" >
										<tr>
											<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Fee Component	</th>
											<th style="padding:10px;text-align:left;border-top:1px solid #000;border-bottom:1px solid #000">Amount</th>	
										</tr>';
										//get fess components
										$component_condition = "f.status='Active' && i.fees_master_id='".$getPayemtDetails[0]['fees_id']."' ";
										$getFeesComponents = $this->admissionmodel->getFeesComponents($component_condition);
										if(!empty($getFeesComponents)){
											for($i=0; $i < sizeof($getFeesComponents); $i++){
												$html_invoice .= '<tr>						
													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getFeesComponents[$i]['fees_component_master_name'].'</strong></td>
													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getFeesComponents[$i]['component_fees'].'</strong></td>					
												</tr>';
											}
										}
											
										
										$html_invoice .= '<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Total Fees Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$total_fees.' /-</strong></td>						
										</tr>
										
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Discount Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$discount_amount.' /-</strong></td>						
										</tr>
										
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Total Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$total_amount.' /-</strong></td>						
										</tr>
										
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Collected Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$collected_fees.' /-</strong></td>						
										</tr>
										
										<tr>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>Remaining Amount</strong></td>
											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$remaining_fees.' /-</strong></td>						
										</tr>
										
									</table>
									
									<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
													<tr>						
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Bank Name: '.$getPayemtDetails[0]['bank_name'].'</strong></td>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Payment Mode: '.$getPayemtDetails[0]['payment_mode'].'</strong></td>
													</tr>';
												if($getPayemtDetails[0]['payment_mode'] == 'Cheque'){	
													$html_invoice .= '<tr>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque No: '.$getPayemtDetails[0]['cheque_no'].'</strong></td>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque Date: '.$getPayemtDetails[0]['transaction_date'].'</strong></td>
													</tr>';
												}else if($getPayemtDetails[0]['payment_mode'] == 'Netbanking'){	
													$html_invoice .= '<tr>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Transaction No: '.$getPayemtDetails[0]['transaction_id'].'</strong></td>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque Date: '.$getPayemtDetails[0]['transaction_date'].'</strong></td>
													</tr>';
												}else{
													$html_invoice .= '<tr>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>&nbsp;</strong></td>
														<td style="padding:10px;text-align:left;font-size:14px"><strong>&nbsp;</strong></td>
													</tr>';
												}
													
												$html_invoice .= '</table>
									
									<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:750px;margin:20px auto;">
											<tr>
												<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Installment Number</th>
												<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Planned Installment Date</th>
												<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Installment Amount</th>
												<th style="padding:5px;text-align:center;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">Paid Amount</th>		
												<th style="padding:5px;text-align:center;font-size:14px;border-bottom:1px solid #000">Remaining Amount</th>
											</tr>
											<tr>
												<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>
												<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">&nbsp;</td>
												<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">(In INR)</td>		
												<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">(In INR)</td>		
												<td style="padding:5px;text-align:right;font-size:14px;border-bottom:1px solid #000">(In INR)</td>
											</tr>';
										//get installment details
										$getPayemtInstallmentDetails = $this->admissionmodel->getdata("tbl_admission_fees_instalments", "admission_fees_id='".$_REQUEST['admission_fees_id']."' " );	
										if(!empty($getPayemtInstallmentDetails)){
											$count = 1;
											for($i=0; $i < sizeof($getPayemtInstallmentDetails); $i++){
												
												$html_invoice .= '<tr>
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.$count.'</td>
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.date("M Y", strtotime($getPayemtInstallmentDetails[$i]['instalment_due_date'])).'</td>
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.$getPayemtInstallmentDetails[$i]['instalment_amount'].'</td>	
													<td style="padding:5px;text-align:right;font-size:14px;border-right:1px solid #000;border-bottom:1px solid #000">'.$getPayemtInstallmentDetails[$i]['instalment_collected_amount'].'</td>		
													<td style="padding:5px;text-align:right;font-size:14px;border-bottom:1px solid #000">'.$getPayemtInstallmentDetails[$i]['instalment_remaining_amount'].'</td>
												</tr>';
												$count++;
											}
										}
										
										$html_invoice .= '</table>
									
									<p style="font-size:15px;text-align:center;"><strong>*CHEQUES SUBJECT TO REALISATION. *NO CHANGE OR CASH CAN BE TAKEN IN EXCHANGE OF GIFT VOUCHER. THIS RECEIPT MUST BE PRODUCED WHEN DEMANDED. FEES ONCE PAID ARE NONREFUNDABLE AND NON-TRANSFERABLE.</strong></p>
								</div>';
			//echo $html_invoice;exit;
			
			
			$this->load->library('M_pdf');
			//echo "here...";exit;
			$pdf = $this->m_pdf->load();
			//$pdf = new mPDF('utf-8',array($fwidth_mm,$fheight_mm),0,0,0,0,0,0);
			$pdf = new mPDF('utf-8');
			
			$pdf->WriteHTML($html_invoice); // write the HTML into the PDF
			$pdf->Output($invoice_pdfFilePath, 'F'); // save to file because we can	
			
			
			
			//insert invoice file
			$invoice_file = array();
			$invoice_file['admission_fees_id'] = $_REQUEST['admission_fees_id'];
			$invoice_file['receipt_no'] = $reciept_no;
			$invoice_file['receipt_file'] = $pdf_filename;
			
			$invoice_file['created_on'] = date("Y-m-d H:m:i");
			$invoice_file['created_by'] = $_SESSION["webadmin"][0]->user_id;
			$invoice_file['updated_on'] = date("Y-m-d H:m:i");
			$invoice_file['updated_by'] = $_SESSION["webadmin"][0]->user_id;
			
			$this->admissionmodel->insertData('tbl_fees_payment_receipt', $invoice_file, 1);
			
				
			echo json_encode(array("status_code" => "200", "msg" => "Information saved successfully.", "body" => $file_path));
			exit;
		
		}
	}

	function groupfees($id = NULL)
	{
		if (!empty($_SESSION["webadmin"])) {
			$record_id = "";

			if (!empty($_GET['text']) && isset($_GET['text'])) {
				$varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
				parse_str($varr, $url_prams);
				$record_id = $varr;
			}
			$result = [];
			$result['student_id'] = $record_id;
			$result['student_detail'] = $this->admissionmodel->getdata1("tbl_student_master", "student_first_name, student_last_name", "status='Active' &&  student_id='".$record_id."'");
			$user_id = $_SESSION["webadmin"][0]->user_id;
			$condition = "i.user_id='".$user_id."'  ";
			$result['groups'] = $this->admissionmodel->getCenterGroups($condition);
			$result['zones'] = $this->admissionmodel->getDropdown1("tbl_zones","zone_id,zone_name");
			$result['months'] = $this->admissionmodel->getMonths("tbl_childactivity_fees_month", "month_id, month_name", "status='Active' ");
			$result['login_name'] = $_SESSION["webadmin"][0]->first_name .' '. $_SESSION["webadmin"][0]->last_name;
			// print_r($result);
			// print_r($_SESSION["webadmin"]);
			// exit();
			$this->load->view('template/header.php');
			$this->load->view('admission/groupFees', $result);
			$this->load->view('template/footer.php');
		}
		else {

			// If no session, redirect to login page
			redirect('login', 'refresh');
		}
	}

	function getCenterUserGroupBatches(){
		$result = $this->admissionmodel->getdata1("tbl_batch_master", "batch_id, batch_name, start_date, end_date", "group_id='".$_REQUEST['group_id']."' ");
		$option = '';
		$batch_id = '';
		// echo "<pre>";
		// print_r($result);exit();
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$option .= '<option value="'.$result[$i]['batch_id'].'" >'.$result[$i]['batch_name'].'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function getCenterFees(){
		$condition = "f.fees_selection_type='Group'  && i.fees_level_id='".$_REQUEST['fees_level_id']."' && i.zone_id='".$_REQUEST['zone_id']."' && i.center_id='".$_REQUEST['center_id']."' && i.status='Active' && i.group_id='".$_REQUEST['group_id']."'"; 
		$result = $this->admissionmodel->getCenterFees($condition);
		$option = '';
		$fees_id = '';
		// echo "<pre>";
		// print_r($result);exit();
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$option .= '<option value="'.$result[$i]['fees_id'].'" >'.$result[$i]['fees_name'].'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function getCenterFeesLevel(){
		$condition = "i.center_id='".$_REQUEST['center_id']."' && i.zone_id='".$_REQUEST['zone_id']."' && f.status ='Active'  ";
		$result = $this->admissionmodel->getCenterFeesLevel($condition);
		// print_r($result);exit();
		$option = '';
		$fees_level_id = '';
		// echo "<pre>";
		// print_r($result);exit();
		
		if(!empty($result)){
			for($i=0;$i<sizeof($result);$i++){
				$option .= '<option value="'.$result[$i]->fees_level_id.'" >'.$result[$i]->fees_level_name.'</option>';
			}
		}
		
		
		echo json_encode(array("status"=>"success","option"=>$option));
		exit;
	}

	function getMonthDiscount(){
		$result = $this->admissionmodel->getdata1("tbl_fees_discount_master", "discount", "month_id='".$_REQUEST['month_id']."'");
		// print_r($result);exit();
		echo json_encode(array("status"=>"success","option"=>$result));
		exit;
	}

	function submitfeesGroupInfoForm(){
		$data = array();
		$data['academic_year_id'] = 0;
		$data['student_id'] = (!empty($_POST['student_id'])) ? $_POST['student_id'] : '';
		$data['fees_id'] = (!empty($_POST['fees_id'])) ? $_POST['fees_id'] : '';
		$data['fees_type'] = 'Group';
		$data['zone_id'] = (!empty($_POST['zone_id'])) ? $_POST['zone_id'] : '';
		$data['center_id'] = (!empty($_POST['center_id'])) ? $_POST['center_id'] : '';
		$data['category_id'] = 0;
		$data['course_id'] = 0;
		$data['batch_id'] = (!empty($_POST['batchid'])) ? $_POST['batchid'] : '';
		$data['group_id'] = (!empty($_POST['groupid'])) ? $_POST['groupid'] : '';
		$data['is_instalment'] = 'No';
		$data['no_of_installments'] = 0;
		
		$data['fees_total_amount'] = (!empty($_POST['fees_total_amount'])) ? $_POST['fees_total_amount'] : '';


		// print_r($_POST);exit();
		$component_condition = "f.status='Active' && i.fees_master_id='".$_POST['fees_id']."' ";
		$getFeesComponents = $this->admissionmodel->getFeesComponents($component_condition);

			// echo "<pre>";
			// print_r($_POST);exit();
		$total_discount_amount = 0;
		if(!empty($getFeesComponents)){
			for($i=0; $i < sizeof($getFeesComponents); $i++){
				$fees_component = array();
				if($getFeesComponents[$i]['is_discountable'] == 'Yes'){
					$discount_amount = ($getFeesComponents[$i]['component_fees'] * $_POST['discount_amount'])/100;
					if($getFeesComponents[$i]['component_fees'] < $discount_amount){
						$fees_component['discount_amount'] = 0;
						$fees_component['discount_percentage'] = 0;
					}
					else{
						$fees_component['discount_amount'] = ($getFeesComponents[$i]['component_fees'] * $_POST['discount_amount'])/100;
						$fees_component['discount_percentage'] = $_POST['discount_amount'];
					}
				}
				else{
					$fees_component['discount_amount'] = 0;
					$fees_component['discount_percentage'] = 0;
				}
				$total_discount_amount = $total_discount_amount + $fees_component['discount_amount'];
			}
		}

		$data['discount_amount'] = $total_discount_amount;
		$data['gst_amount'] = (!empty($_POST['total_gst_amount'])) ? $_POST['total_gst_amount'] : '';
		$data['total_amount'] = (!empty($_POST['total_amount'])) ? $_POST['total_amount'] : '';
		$data['fees_amount_collected'] = (!empty($_POST['total_amount'])) ? $_POST['total_amount'] : '';
		$data['fees_remaining_amount'] = 0;
		$data['fees_remark'] = (!empty($_POST['fees_remark'])) ? $_POST['fees_remark'] : '';
		$data['fees_approved_accepted_by_name'] = (!empty($_POST['fees_approved_accepted_by_name'])) ? $_POST['fees_approved_accepted_by_name'] : '';
		
		
		$data['created_on'] = date("Y-m-d H:m:i");
		$data['created_by'] = $_SESSION["webadmin"][0]->user_id;
		$data['updated_on'] = date("Y-m-d H:m:i");
		$data['updated_by'] = $_SESSION["webadmin"][0]->user_id;
		
		$result = $this->admissionmodel->insertData('tbl_admission_fees', $data, 1);
		
		if(!empty($result)){
			//get fess components
			$component_condition = "f.status='Active' && i.fees_master_id='".$_POST['fees_id']."' ";
			$getFeesComponents = $this->admissionmodel->getFeesComponents($component_condition);
			// echo "<pre>";
			// print_r($getFeesComponents);exit();
			if(!empty($getFeesComponents)){
				for($i=0; $i < sizeof($getFeesComponents); $i++){
					$getGstDetail = $this->admissionmodel->getdata1("tbl_gst_master", "gst_master_id, tax", "status='Active' &&  gst_master_id='".$getFeesComponents[$i]['gst_tax_id']."'");
					$fees_component = array();
					$fees_component['admission_fees_id'] = $result;
					$fees_component['fees_id'] = $_POST['fees_id'];
					$fees_component['fees_component_id'] = $getFeesComponents[$i]['fees_component_master_id'];
					$fees_component['fees_component_id_value'] = $getFeesComponents[$i]['component_fees'];
					if($getFeesComponents[$i]['is_discountable'] == 'Yes'){
						$discount_amount = ($getFeesComponents[$i]['component_fees'] * $_POST['discount_amount'])/100;
						if($getFeesComponents[$i]['component_fees'] < $discount_amount){
							$fees_component['discount_amount'] = 0;
							$fees_component['discount_percentage'] = 0;
						}
						else{
							$fees_component['discount_amount'] = ($getFeesComponents[$i]['component_fees'] * $_POST['discount_amount'])/100;
							$fees_component['discount_percentage'] = $_POST['discount_amount'];
						}
					}
					else{
						$fees_component['discount_amount'] = 0;
						$fees_component['discount_percentage'] = 0;
					}
					$discountamount = $getFeesComponents[$i]['component_fees'] - $fees_component['discount_amount'];
					if($getFeesComponents[$i]['is_gst'] == 'Yes'){
						$fees_component['gst_amount'] = ($discountamount * $getGstDetail[0]['tax'])/100;
						$tax = explode(' ', $getGstDetail[0]['tax']);
						$fees_component['gst_percentage'] = $tax[0];
					}
					else{
						$fees_component['gst_amount'] = 0;
						$fees_component['gst_percentage'] = 0;
					}
					$fees_component['sub_total'] = ($discountamount) - $fees_component['gst_amount'];
					$this->admissionmodel->insertData('tbl_admission_fees_components', $fees_component, 1);
				}
			}
		}
		// print_r($fees_component);exit();
		//save payment details
		// if(!isset($_POST['group'])){
		// 	$payment_data = array();
		// 	$payment_data['admission_fees_id'] = $result;
		// 	$payment_data['payment_mode'] = (!empty($_POST['payment_mode'])) ? $_POST['payment_mode'] : '' ;
		// 	$payment_data['bank_name'] = (!empty($_POST['bank_name'])) ? $_POST['bank_name'] : '' ;
		// 	$payment_data['transaction_date'] = (!empty($_POST['transaction_date'])) ? date("Y-m-d", strtotime($_POST['transaction_date'])) : '' ;
		// 	$payment_data['cheque_no'] = (!empty($_POST['cheque_no'])) ? $_POST['cheque_no'] : '' ;
		// 	$payment_data['transaction_id'] = (!empty($_POST['transaction_id'])) ? $_POST['transaction_id'] : '' ;
		// 	$payment_data['fees_remark'] = (!empty($_POST['fees_remark'])) ? $_POST['fees_remark'] : '';
		// 	$payment_data['fees_approved_accepted_by_name'] = (!empty($_POST['fees_approved_accepted_by_name'])) ? $_POST['fees_approved_accepted_by_name'] : '';
			
		// 	$payment_data['created_on'] = date("Y-m-d H:m:i");
		// 	$payment_data['created_by'] = (!empty($_POST['userid'])) ? $_POST['userid'] : '';
		// 	$payment_data['updated_on'] = date("Y-m-d H:m:i");
		// 	$payment_data['updated_by'] = (!empty($_POST['userid'])) ? $_POST['userid'] : '';
			
		// 	$this->teachersapimodel->insertData('tbl_fees_payment_details', $payment_data, 1);
		// 	// print_r($_POST);exit();
		// 	if(!empty($result)){
		// 		if($_POST['fees_amount_collected'] < $_POST['total_mandatory_amount']){
		// 			$sstatus_array['status'] = 'In-active';
		// 		}
		// 		else{
		// 			$status_data = $this->teachersapimodel->getdata("tbl_student_master", "status", "student_id='".$_POST['studentid']."' ");
		// 			// print_r($status_data);exit();
		// 			if($status_data[0]['status'] != 'In-active'){
		// 				$sstatus_array['status'] = 'Active';
		// 			}
		// 			else{
		// 				$sstatus_array['status'] = 'In-active';
		// 			}
		// 		}
		// 		$this->teachersapimodel->updateRecord('tbl_student_master', $sstatus_array, "student_id='".$_POST['studentid']."' ");
				
		// 		$sdstatus_array = array();
		// 		if($_POST['fees_amount_collected'] < $_POST['total_mandatory_amount']){
		// 			$sdstatus_array['status'] = 'In-active';
		// 		}
		// 		else{
		// 			$status_data = $this->teachersapimodel->getdata("tbl_student_details", "status", "student_id='".$_POST['studentid']."' ");
		// 			if($status_data[0]['status'] != 'In-active'){
		// 				$sdstatus_array['status'] = 'Active';
		// 			}else{
		// 				$sdstatus_array['status'] = 'In-active';
		// 			}
		// 		}
		// 		$this->teachersapimodel->updateRecord('tbl_student_group', $sdstatus_array, "student_id='".$_POST['studentid']."' && academic_year_id='".$_POST['acadmicyear']."' ");
		// 	}
			
		// 	//get payment details
		// 	$getPayemtDetails = $this->teachersapimodel->getPayemtDetails("admission_fees_id='".$result."' ");
		// 	//echo "<pre>";print_r($getPayemtDetails);exit;
		// 	$student_name = (!empty($getPayemtDetails[0]['student_first_name'])) ? $getPayemtDetails[0]['student_first_name']." ".$getPayemtDetails[0]['student_last_name'] : '';
		// 	$academic_year = (!empty($getPayemtDetails[0]['academic_year_master_name'])) ? $getPayemtDetails[0]['academic_year_master_name'] : '';
		// 	$category_name = (!empty($getPayemtDetails[0]['categoy_name'])) ? $getPayemtDetails[0]['categoy_name'] : '';
		// 	$course_name = (!empty($getPayemtDetails[0]['course_name'])) ? $getPayemtDetails[0]['course_name'] : '';
		// 	$batch_name = (!empty($getPayemtDetails[0]['batch_name'])) ? $getPayemtDetails[0]['batch_name'] : '';
		// 	$group_name = (!empty($getPayemtDetails[0]['group_master_name'])) ? $getPayemtDetails[0]['group_master_name'] : '';
		// 	$center_name = (!empty($getPayemtDetails[0]['center_name'])) ? $getPayemtDetails[0]['center_name'] : '';
		// 	$center_address = (!empty($getPayemtDetails[0]['center_address'])) ? $getPayemtDetails[0]['center_address'] : '';
		// 	$center_contact_no = (!empty($getPayemtDetails[0]['center_contact_no'])) ? $getPayemtDetails[0]['center_contact_no'] : '';
		// 	$center_email_id = (!empty($getPayemtDetails[0]['center_email_id'])) ? $getPayemtDetails[0]['center_email_id'] : '';
		// 	$center_code = (!empty($getPayemtDetails[0]['center_code'])) ? $getPayemtDetails[0]['center_code'] : '';
			
		// 	$intallment_yes_no = (!empty($getPayemtDetails[0]['is_instalment'])) ? $getPayemtDetails[0]['is_instalment'] : '';
		// 	$installment_nos = (!empty($getPayemtDetails[0]['no_of_installments'])) ? $getPayemtDetails[0]['no_of_installments'] : '';
		// 	$total_fees = (!empty($getPayemtDetails[0]['fees_total_amount'])) ? $getPayemtDetails[0]['fees_total_amount'] : '';
		// 	$discount_amount = (!empty($getPayemtDetails[0]['discount_amount'])) ? $getPayemtDetails[0]['discount_amount'] : '';
		// 	$gst_amount = (!empty($getPayemtDetails[0]['gst_amount'])) ? $getPayemtDetails[0]['gst_amount'] : '';
		// 	$total_amount = (!empty($getPayemtDetails[0]['total_amount'])) ? $getPayemtDetails[0]['total_amount'] : '';
		// 	$collected_fees = (!empty($getPayemtDetails[0]['fees_amount_collected'])) ? $getPayemtDetails[0]['fees_amount_collected'] : '';
		// 	$remaining_fees = (!empty($getPayemtDetails[0]['fees_remaining_amount'])) ? $getPayemtDetails[0]['fees_remaining_amount'] : '';
			
		// 	$getRecieptNo = $this->teachersapimodel->getdata_orderby_limit1("tbl_fees_payment_receipt", "fees_payment_receipt_id", "1=1", "order by fees_payment_receipt_id desc");
				
		// 	$reciept_id = (!empty($getRecieptNo)) ? ($getRecieptNo[0]['fees_payment_receipt_id'] + 1) : 1;
			
		// 	$reciept_no = "";
		// 	if(!empty($center_code)){
		// 		$reciept_no .= $center_code."/";
		// 	}
		// 	if(!empty($academic_year)){
		// 		$reciept_no .= $academic_year."/";
		// 	}
			
		// 	$reciept_no .= $reciept_id;
			
		// 	if(!empty($getPayemtDetails[0]['father_email_id']) && $_POST['isadmission'] == 'Yes'){
				
				
				
				
				
		// 		$message = '';
		// 		$message  = '<html><body>';	
		// 		$message .= '<div style="width:750px;padding:0 30px;">
		// 							<div style="margin:10px;">
		// 							<img src="https://www.aptechmontanapreschool.com/wp-content/uploads/2018/06/aptech-logo-1.png" alt="The Montana International Pre school Pvt. Ltd." title="The Montana International Pre school Pvt. Ltd." style="width:180px;"/>
		// 							</div>';
		// 							$message .= '<div>
		// 								<h1 style="text-align:center;color:#000;font-size:25px;margin:10px 0;padding:0;">'.$center_name.'</h1>
		// 								<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;">'.$center_address.'</p>
		// 								<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><a href="mailto:'.$center_email_id.' " style="color:#000;text-decoration:none;">'.$center_email_id.'</a></p>
		// 							</div>';
									
		// 							$message .= '<div style="height:1px;background:#000;margin:10px 0">&nbsp;</div>
		// 							<h4 style="text-align: center;font-size:20px;margin:10px 0;padding:0">Booking Confirmation</h4>
		// 							<table cellpadding="0" cellspacing="0" border="0" style="border:0px none;width:700px;">
		// 								<tr>
		// 									<td style="width:350px;padding:10px;text-align:left;">
		// 										<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Name:</strong>'.$getPayemtDetails[0]['student_first_name'].' '.$getPayemtDetails[0]['student_last_name'].'</p> 
		// 										<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Address:</strong>'.$getPayemtDetails[0]['present_address'].'</p> 
		// 										<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Student ID:</strong>'.$getPayemtDetails[0]['enrollment_no'].'</p> 
		// 										<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Program Family:</strong>'.$getPayemtDetails[0]['categoy_name'].'</p>
		// 										<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Program Name:</strong>'.$getPayemtDetails[0]['course_name'].'</p>
		// 									</td>
		// 									<td style="width:350px;padding:10px;text-align:left;">
		// 										<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Number:</strong>'.$getPayemtDetails[0]['father_mobile_contact_no'].'</p> 
		// 										<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Date:</strong>'.date("d-M-Y").'</p> 
		// 										<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Email ID:</strong>'.$getPayemtDetails[0]['father_email_id'].'</p> 
		// 										<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Currency:</strong>INR</p>
		// 										<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Receipt Number: '.$reciept_no.'</strong></p>
		// 										<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Receipt Date: '.date("d-M-Y").'</strong></p>
		// 									</td>
		// 								</tr>
		// 							</table>
		// 							<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
		// 								<tr>
		// 									<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Fee Component	</th>
		// 									<th style="padding:10px;text-align:left;border-top:1px solid #000;border-bottom:1px solid #000">Amount</th>	
		// 								</tr>';
		// 								//get fess components
		// 								$component_condition = "f.status='Active' && i.fees_master_id='".$_POST['feesid']."' ";
		// 								$getFeesComponents = $this->teachersapimodel->getFeesComponents($component_condition);
		// 								if(!empty($getFeesComponents)){
		// 									for($i=0; $i < sizeof($getFeesComponents); $i++){
		// 										$message .= '<tr>						
		// 											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getFeesComponents[$i]['fees_component_master_name'].'</strong></td>
		// 											<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getFeesComponents[$i]['component_fees'].'</strong></td>					
		// 										</tr>';
		// 									}
		// 								}
											
										
		// 								$message .= '<tr>
		// 									<td style="padding:10px;text-align:left;font-size:14px"><strong>Total Fees Amount</strong></td>
		// 									<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$total_fees.' /-</strong></td>						
		// 								</tr>
										
		// 								<tr>
		// 									<td style="padding:10px;text-align:left;font-size:14px"><strong>Discount Amount</strong></td>
		// 									<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$total_discount_amount.' /-</strong></td>						
		// 								</tr>

		// 								<tr>
		// 									<td style="padding:10px;text-align:left;font-size:14px"><strong>GST Amount</strong></td>
		// 									<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$gst_amount.' /-</strong></td>						
		// 								</tr>
										
		// 								<tr>
		// 									<td style="padding:10px;text-align:left;font-size:14px"><strong>Total Amount</strong></td>
		// 									<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$total_amount.' /-</strong></td>						
		// 								</tr>
										
		// 								<tr>
		// 									<td style="padding:10px;text-align:left;font-size:14px"><strong>Collected Amount</strong></td>
		// 									<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$collected_fees.' /-</strong></td>						
		// 								</tr>
										
		// 								<tr>
		// 									<td style="padding:10px;text-align:left;font-size:14px"><strong>Remaining Amount</strong></td>
		// 									<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$remaining_fees.' /-</strong></td>						
		// 								</tr>
										
		// 							</table>
									
		// 							<p style="font-size:15px;margin:4px 0;">*CHEQUES SUBJECT TO REALISATION</p>
		// 							<p style="font-size:15px;margin:4px 0;">*NO CHANGE OR CASH CAN BE TAKEN IN EXCHANGE OF GIFT VOUCHER</p>
		// 							<p style="font-size:15px;margin:4px 0;">THIS RECEIPT MUST BE PRODUCED WHEN DEMANDED</p>
		// 							<p style="font-size:15px;margin:4px 0;">FEES ONCE PAID ARE NOT REFUNDABLE</p>
		// 							<p style="font-size:15px;margin:4px 0;">SUBJECT TO TERMS AND CONDITION PRINTED OVERLEAF THE BOOKING CONFIRMATION</p>
		// 							<p style="font-size:15px;margin:4px 0;">1) This Booking Confirmation provides provisional admission to the child.</p>
		// 							<p style="font-size:15px;margin:4px 0;">2) Ensure timely payments as mentioned in the Booking confirmation.</p>
		// 							<p style="font-size:15px;margin:4px 0;">3) Taxes, as applicable, at the time of payment will be charged extra.</p>
		// 							<p style="font-size:15px;margin:4px 0;">4) Parents are required to inform the school if there is any change in their address and telephone numbers.</p>
		// 							<p style="font-size:15px;margin:4px 0;">5) The parents are responsible for timely drop and pick up of child.</p>
		// 							<p style="font-size:15px;margin:4px 0;">6) Please refer detailed code of conduct and policy Guidelines sent on your mail.</p>
		// 							<p style="font-size:15px;margin:4px 0 50px 0;">I have read and understood the code of conduct and payment terms / installment plan mentioned above and agree to abide by them and also the terms and conditions printed overleaf.</p>
		// 							<div style="width:200px;float:right;text-align:center">
		// 								<p style="font-size:15px;margin:4px 0;">The Montana <br/>International Preschool<br/> Pvt Ltd </p>
		// 							</div>
		// 							<div style="clear:both;margin-bottom:80px">&nbsp;</div>
		// 							<div style="width:200px;float:left;text-align:center">
		// 								<p style="font-size:15px;margin:4px 0;"><strong>Signature of Parent / Guardian </strong></p>
		// 							</div>
		// 							<div style="width:200px;float:right;text-align:center">
		// 								<p style="font-size:15px;margin:4px 0 ;"><strong>AUTHORISED <br/>SIGNATORY</strong></p>
		// 							</div>
		// 							<div style="clear:both;">&nbsp;</div>
		// 								<p style="font-size:15px;margin:15px 0;">For any feedback or suggestions please write to us at <a href="mailto:feedback@montanapreschool.com" style="color:#000;text-decoration:none;">feedback@montanapreschool.com</a></p>
		// 								<p style="font-size:15px;margin:15px 0;">Registered Office : 402, Sagar Tech Plaza, B wing, Andheri-Kurla Road, Sakinaka, Andheri East, Mumbai - 400 072 Maharashtra, India, </p>
		// 								<p style="font-size:15px;margin:15px 0;"><a href="https://www.facebook.com/montanapreschool/" target="_blank" style="color:#000;text-decoration:none;">https://www.facebook.com/montanapreschool/</a>
		// 								<p style="font-size:15px;margin:4px 0;">Disclaimer : Taxes will be charged extra, as applicable, on the date of payment.</p>
		// 								</p>
		// 							</div>';
		// 		$message .=  '</body></html>';
				
		// 		$this->email->clear();
		// 		$this->email->from("info@attoinfotech.in"); // change it to yours
		// 		$subject = "";
		// 		$subject = "Aptech Montana - Admission Confirmation";
		// 		$this->email->to($getPayemtDetails[0]['father_email_id']);
		// 		//$this->email->to("danish.akhtar@attoinfotech.com");
		// 		$this->email->subject($subject);
		// 		$this->email->message($message);
		// 		$bfilePath = DOC_ROOT."/images/M_attachment.pdf";
		// 		$this->email->attach($bfilePath);
		// 		$checkemail = $this->email->send();
		// 		$this->email->clear(TRUE);
			
		// 	}
			
			
		// 	//genrate invoice pdf
		// 	ini_set('memory_limit','100M');
		// 	//echo "here";exit;
		// 	//$this->load->library('m_pdf');
		// 	//echo "here1";exit;
		// 	$date_v = date("d_m_Y_H_i_s");
		// 	$pdf_filename = "paymentinvoice_".$date_v.".pdf";
		// 	$invoice_pdfFilePath = DOC_ROOT."/images/payment_invoices/$pdf_filename";
		// 	$file_path = base_url()."/images/payment_invoices/$pdf_filename";
			
		// 	$html_invoice = '';
		// 	$html_invoice = '<div style="width:750px;padding:0 30px;">
		// 							<div style="margin:10px;">
		// 								<img src="https://www.aptechmontanapreschool.com/wp-content/uploads/2018/06/aptech-logo-1.png" alt="The Montana International Pre school Pvt. Ltd." title="The Montana International Pre school Pvt. Ltd." style="width:180px;"/>
		// 							</div>
		// 							<div>';
		// 							//get center information
									
		// 								$html_invoice .= '<h1 style="text-align:center;color:#000;font-size:25px;margin:10px 0;padding:0;">'.$center_name.'</h1>
		// 								<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;">'.$center_address.' Helpline: '.$center_contact_no.'</p>
		// 								<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><a href="mailto:'.$center_email_id.' " style="color:#000;text-decoration:none;">'.$center_email_id.'</a></p>';
										
		// 							$html_invoice .= '</div>
		// 							<div>
		// 								<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
		// 									<tr>
		// 										<td>
		// 											<table cellpadding="0" cellspacing="0" border="0" style="width:700px;">
		// 												<tr>
		// 													<th style="width:150px;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Date</th>
		// 													<th style="width:150px;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Enrol No</th>
		// 													<th style="width:150px;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Name</th>
		// 													<th style="width:150px;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Class/Group</th>
		// 													<th style="width:150px;padding:10px;border-bottom:1px solid #000">Section(Batch)</th>
		// 												</tr>
		// 												<tr>
		// 													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.date("d-M-Y").'</strong></td>
		// 													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getPayemtDetails[0]['enrollment_no'].'</strong></td>
		// 													<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$student_name.'</strong></td>';
		// 												if($_POST['feestype'] == 'Class'){		
		// 													$html_invoice .= '<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$course_name.'</strong></td>
		// 													<td style="padding:10px;font-size:14px"><strong>'.$batch_name.'</strong></td>';
		// 												}else{
		// 													$html_invoice .= '<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$group_name.'</strong></td>
		// 													<td style="padding:10px;font-size:14px"><strong>-</strong></td>';
		// 												}
															
		// 												$html_invoice .= '</tr>
		// 												<tr>
		// 													<td colspan="2" style="padding:10px;text-align:left;font-size:14px">
		// 														<p style="color:#000;font-size:14px;margin:3px 0;">Father Name: <strong>'.$getPayemtDetails[0]['father_name'].'</strong></p>
		// 														<p style="color:#000;font-size:14px;margin:3px 0;">Address: '.$getPayemtDetails[0]['present_address'].'</p>
		// 													</td>						
		// 													<td colspan="2"  style="padding:10px;text-align:left;font-size:14px"> 
		// 														<p style="color:#000;font-size:14px;margin:3px 0;">Payment Mode: <strong>'.$_POST['payment_mode'].'</strong></p>
		// 													</td>						
		// 													<td style="padding:10px;text-align:left;font-size:14px"> 
		// 														<p style="color:#000;font-size:14px;margin:3px 0;">Receipt No:<strong>'.$reciept_no.'</strong></p>
		// 													</td>
		// 												</tr>					
		// 											</table>
		// 										</td>
		// 									</tr>
											
		// 								</table>
										
		// 								<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
		// 									<tr>
		// 										<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Fee Component	</th>
		// 										<th style="padding:10px;text-align:left;border-top:1px solid #000;border-bottom:1px solid #000">Amount</th>	
		// 									</tr>';
		// 									//get fess components
		// 									$component_condition = "f.status='Active' && i.fees_master_id='".$_POST['feesid']."' ";
		// 									$getFeesComponents = $this->teachersapimodel->getFeesComponents($component_condition);
		// 									if(!empty($getFeesComponents)){
		// 										for($i=0; $i < sizeof($getFeesComponents); $i++){
		// 											$html_invoice .= '<tr>						
		// 												<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getFeesComponents[$i]['fees_component_master_name'].'</strong></td>
		// 												<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getFeesComponents[$i]['component_fees'].'</strong></td>					
		// 											</tr>';
		// 										}
		// 									}
												
											
		// 									$html_invoice .= '<tr>
		// 										<td style="padding:10px;text-align:left;font-size:14px"><strong>Total Fees Amount</strong></td>
		// 										<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$total_fees.' /-</strong></td>						
		// 									</tr>
											
		// 									<tr>
		// 										<td style="padding:10px;text-align:left;font-size:14px"><strong>Discount Amount</strong></td>
		// 										<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$total_discount_amount.' /-</strong></td>						
		// 									</tr>

		// 									<tr>
		// 										<td style="padding:10px;text-align:left;font-size:14px"><strong>GST Amount</strong></td>
		// 										<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$gst_amount.' /-</strong></td>						
		// 									</tr>
											
		// 									<tr>
		// 										<td style="padding:10px;text-align:left;font-size:14px"><strong>Total Amount</strong></td>
		// 										<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$total_amount.' /-</strong></td>						
		// 									</tr>
											
		// 									<tr>
		// 										<td style="padding:10px;text-align:left;font-size:14px"><strong>Collected Amount</strong></td>
		// 										<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$collected_fees.' /-</strong></td>						
		// 									</tr>
											
		// 									<tr>
		// 										<td style="padding:10px;text-align:left;font-size:14px"><strong>Remaining Amount</strong></td>
		// 										<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$remaining_fees.' /-</strong></td>						
		// 									</tr>
											
		// 								</table>
										
		// 								<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
		// 												<tr>						
		// 													<td style="padding:10px;text-align:left;font-size:14px"><strong>Bank Name: '.$_POST['bank_name'].'</strong></td>
		// 													<td style="padding:10px;text-align:left;font-size:14px"><strong>Payment Mode: '.$_POST['payment_mode'].'</strong></td>
		// 												</tr>';
		// 											if($_POST['payment_mode'] == 'Cheque'){	
		// 												$html_invoice .= '<tr>
		// 													<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque No: '.$_POST['cheque_no'].'</strong></td>
		// 													<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque Date: '.$_POST['transaction_date'].'</strong></td>
		// 												</tr>';
		// 											}else if($_POST['payment_mode'] == 'Netbanking'){	
		// 												$html_invoice .= '<tr>
		// 													<td style="padding:10px;text-align:left;font-size:14px"><strong>Transaction No: '.$_POST['transaction_id'].'</strong></td>
		// 													<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque Date: '.$_POST['transaction_date'].'</strong></td>
		// 												</tr>';
		// 											}else{
		// 												$html_invoice .= '<tr>
		// 													<td style="padding:10px;text-align:left;font-size:14px"><strong>&nbsp;</strong></td>
		// 													<td style="padding:10px;text-align:left;font-size:14px"><strong>&nbsp;</strong></td>
		// 												</tr>';
		// 											}
														
		// 											$html_invoice .= '</table>
										
		// 								<p style="font-size:15px;text-align:center;"><strong>*CHEQUES SUBJECT TO REALISATION. *NO CHANGE OR CASH CAN BE TAKEN IN EXCHANGE OF GIFT VOUCHER. THIS RECEIPT MUST BE PRODUCED WHEN DEMANDED. FEES ONCE PAID ARE NONREFUNDABLE AND NON-TRANSFERABLE.</strong></p>
		// 							</div>';
				
		
		// 	//echo $html_invoice;exit;
		// 	$this->load->library('m_pdf');
		// 	//echo "here...";exit;
		// 	$pdf = $this->m_pdf->load();
		// 	//$pdf = new mPDF('utf-8',array($fwidth_mm,$fheight_mm),0,0,0,0,0,0);
		// 	$pdf = new mPDF('utf-8');
			
		// 	$pdf->WriteHTML($html_invoice); // write the HTML into the PDF
		// 	$pdf->Output($invoice_pdfFilePath, 'F'); // save to file because we can
			
		// 	//insert invoice file
		// 	$invoice_file = array();
		// 	$invoice_file['admission_fees_id'] = $result;
		// 	$invoice_file['receipt_no'] = $reciept_no;
		// 	$invoice_file['receipt_file'] = $pdf_filename;
			
		// 	$invoice_file['created_on'] = date("Y-m-d H:m:i");
		// 	$invoice_file['created_by'] = (!empty($_POST['userid'])) ? $_POST['userid'] : '';
		// 	$invoice_file['updated_on'] = date("Y-m-d H:m:i");
		// 	$invoice_file['updated_by'] = (!empty($_POST['userid'])) ? $_POST['userid'] : '';
			
		// 	$this->teachersapimodel->insertData('tbl_fees_payment_receipt', $invoice_file, 1);
		// }

		if (!empty($result)) {
			echo json_encode(array(
				'success' => '1',
				'msg' => 'Record Added/Updated Successfully.',
				'flag' => $flag,
				'student_id' => $student_id
			));
			exit;
		}
		else{
			echo json_encode(array(
				'success' => '0',
				'msg' => 'Problem in data update.'
			));
			exit;
		}
	}

	function export(){
		$get_result = $this->admissionmodel->getExportRecords($_POST);
		
		if(!empty($get_result['query_result']) && count($get_result['query_result']) > 0)
		{
			$this->load->library('Excel');
			
			//Create new PHPExcel object
			$objPHPExcel = new PHPExcel();
			//Set properties
			$objPHPExcel->getProperties()->setCreator("Attoinfotech")
			->setLastModifiedBy("Attoinfotech")
			->setTitle("Office 2007 XLSX Test Document")
			->setSubject("Office 2007 XLSX Test Document")
			->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
			->setKeywords("office 2007 openxml php")
			->setCategory("Export Excel");
			
			$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
			$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
			
			// Rename sheet
			$objPHPExcel->getActiveSheet()->setTitle('OutPut-File');
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("A1", 'First Name', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("B1", 'Last Name', PHPExcel_Cell_DataType::TYPE_STRING);
			
			
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("C1", 'Enrollment No', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("D1", 'Zone', PHPExcel_Cell_DataType::TYPE_STRING);
			$objPHPExcel->getActiveSheet()->setCellValueExplicit("E1", 'Center', PHPExcel_Cell_DataType::TYPE_STRING);
			
			$objPHPExcel->getActiveSheet()->getStyle("A1:M1")->getFont()->setBold(true);
			
			$j = 2;
			
			for($i=0;$i<sizeof($get_result['query_result']);$i++)
			{
				
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("A$j", $get_result['query_result'][$i]->student_first_name);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("B$j", $get_result['query_result'][$i]->student_last_name);
				
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("C$j", $get_result['query_result'][$i]->enrollment_no);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("D$j", $get_result['query_result'][$i]->zone_name);
				$objPHPExcel->getActiveSheet()->setCellValueExplicit("E$j", $get_result['query_result'][$i]->center_name);
				
				$j++;
			}
			
			// Redirect output to a client's web browser (Excel5)
			header('Content-Type: application/vnd.ms-excel');
			header('Content-Disposition: attachment;filename="Admission('.date('d-m-Y').').xls"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
			$objWriter->save('php://output');
			
			$_SESSION['admission_export_success'] = "success";
			$_SESSION['admission_export_msg'] = "Records Exported Successfully.";

			redirect('admission');
			exit;
		}
		else
		{
			$_SESSION['contactus_export_success'] = "error";
			$_SESSION['contactus_export_msg'] = "No records found for export";	
			redirect('admission');
			exit;
		}
	}
	
	function delrecord12()
	{
		//echo $_POST['status'];exit;
		$id=$_POST['id'];
		$status=$_POST['status'];
		
		$appdResult = $this->admissionmodel->delrecord12("tbl_student_master","student_id",$id,$status);
		if($appdResult)
		{
			echo "1";
		}
		else
		{
			echo "2";	
		}	
	}
	
	
}

?>
