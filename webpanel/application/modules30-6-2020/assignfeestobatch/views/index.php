<div id="content" class="content-wrapper">
	 <div class="page-title">
      <div>
        <h1>Assign Fees To Batch</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>assignfeestobatch">Assign Fees To Batch</a></li>
        </ul>
      </div>
    </div> 
    <div class="card">
    	<div class="page-title-border">
        	<div class="col-sm-12 col-md-12 text-right">
				<a href="<?php  echo base_url();?>assignfeestobatch/addEdit" class="btn btn-primary icon-btn pull-right"><i class="fa fa-plus"></i>Add Fees To Batch</a>		
            <div class="clearfix"></div>
            </div>
        </div>     
        <div class="col-sm-12" style="clear: both">
         	<div class="box-content form-horizontal product-filter">    
				
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="zonename" class="control-label">Zone Name</label>
						<input id="sSearch_0" name="sSearch_0" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="centername" class="control-label">Center Name</label>
						<input id="sSearch_1" name="sSearch_1" type="text" class="searchInput form-control"/>
					</div>
				</div>        	
            	
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="groupname" class="control-label">Group Name</label>
						<input id="sSearch_2" name="sSearch_2" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="batchname" class="control-label">Batch Name</label>
						<input id="sSearch_3" name="sSearch_3" type="text" class="searchInput form-control"/>
					</div>
				</div>

				<div class="col-sm-2 col-xs-12">
					<div class="dataTables_filter searchFilterClass form-group">
						<label for="batchname" class="control-label">Fees Name</label>
						<input id="sSearch_4" name="sSearch_4" type="text" class="searchInput form-control"/>
					</div>
				</div>
				
				<div class="control-group clearFilter">
					<div class="controls">
						<a href="#" onclick="clearSearchFilters();"><button class="btn" style="margin:32px 10px 10px 10px;">Clear Search</button></a>
					</div>
				</div>
            </div>
         </div>
		 
		 
         <div class="clearfix"></div>
         <div class="card-body">
          	<div class="box-content">
            	 <div class="table-responsive scroll-table">
                    <table class="dynamicTable display table table-bordered non-bootstrap" >
                        <thead>
                          <tr>
							<th>Zone Name</th>
							<th>Center Name</th>
							<th>Group Name</th>
							<th>Batch Name</th>
							<th>Fees Name</th>
							<th data-bSortable="false">Action</th>
						</tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>           
                </div>
            </div>
         </div>
         <div class="clearfix"></div>
    </div>
</div><!-- end: Content -->
			
<script>
	$( document ).ready(function() {

		clearSearchFilters();
	});	
	
	
	function deleteMapping(id){
    	if (confirm("Are you sure to remove this mapping..?" )){
			$.ajax({
				url: "<?php echo base_url().$this->router->fetch_module();?>/delrecord/",
				data:{"id":id},
				async: false,
				type: "POST",
				success: function(data2){
					data2 = $.trim(data2);
					if(data2 == "1"){
						displayMsg("success","Record has Removed!");
						clearSearchFilters();
					}else{
						displayMsg("error","Oops something went wrong!");
					}
				}
			});
    	}
    }
	
	document.title = "Assign Fees To Batch";
</script>