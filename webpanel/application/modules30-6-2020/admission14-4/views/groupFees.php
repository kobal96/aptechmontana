<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
	<div class="page-title">
          <div>
            <h1>Group Fees</h1>            
          </div>
          <!-- <div>
            <ul class="breadcrumb">
              <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
              <li><a href="<?php echo base_url();?>camera">Camera</a></li>
            </ul>
          </div> -->
    </div>
    <div class="card">       
         <div class="card-body">             
            <div class="box-content">
                <div class="col-sm-8 col-md-12">
					<form class="form-horizontal" id="feesInfoform-validate" method="post" enctype="multipart/form-data">
						<input type="hidden" id="student_id" name="student_id" value="<?php if(!empty($student_id)){echo $student_id;}?>" />
						
						<!-- <div class="row form-group">
							<div class="control-group col-md-6">
								<label class="control-label"><span>Zone*</span></label> 
								<div class="controls">
									<select id="zone_id" name="zone_id" class="form-control"  onchange="getCenters(this.value);getFeesLevel(this.value)">
										<option value="">Select Zone</option>
										<?php 
											if(isset($zones) && !empty($zones)){
												foreach($zones as $cdrow){
													$sel = ($cdrow->zone_id == $details[0]->zone_id) ? 'selected="selected"' : '';
										?>
											<option value="<?php echo $cdrow->zone_id;?>" <?php echo $sel; ?>><?php echo $cdrow->zone_name;?></option>
										<?php }}?>
									</select>
								</div>
							</div>
						
							<div class="control-group col-md-6">
								<label class="control-label"><span>Center*</span></label> 
								<div class="controls">
									<select id="center_id" name="center_id" class="form-control" onchange="getFeesLevel(this.value)">
										<option value="">Select Center</option>
									</select>
								</div>
							</div>
						</div> -->

						<input type="hidden" id="zone_id" name="zone_id" value="<?php echo $student_detail[0]['zone_id']?>" />

						<input type="hidden" id="center_id" name="center_id" value="<?php echo $student_detail[0]['center_id']?>" />
						
						<div class="row form-group">
							<div class="control-group col-md-6">
								<label class="text_green"> Student </label><br>
								<input type="text" class="form-control" name="studentname" readonly="readonly" value="<?php echo $student_detail[0]['student_first_name'].' '.$student_detail[0]['student_last_name']?>">
								<div class="clearfix"></div>
							</div>
							<div class="control-group col-md-6">
								<label class="text_green"> Group<span class="text-danger">*</span> </label>
								<select class="form-control" name="groupid" onchange="getCenterUserGroupBatches(this.value);getCenterFees(this.value);checkAllowFrequency();getFeesLevel(this.value)" id="groupid">
					                <option value="" selected="selected" disabled="disabled">Select Group</option>
					                <?php 
										if(isset($groups) && !empty($groups)){
											foreach($groups as $cdrow){
									?>
										<option value="<?php echo $cdrow->group_id;?>"><?php echo $cdrow->group_master_name;?></option>
									<?php }}?>
				              	</select>
							</div>
						</div>

						<div class="row form-group">
							<div class="control-group col-md-6">
								<label class="control-label"><span>Batch*</span></label> 
								<div class="controls">
									<select id="batchid" name="batchid" class="form-control">
										<option value="">Select Batch</option>
									</select>
								</div>
							</div>
							<div class="control-group col-md-6">
								<label class="control-label"><span>Fees Level*</span></label> 
								<div class="controls">
									<select id="fees_level" name="fees_level" class="form-control" onchange="getCenterFees(this.value)">
										<option value="">Select Fees Level</option>
									</select>
								</div>
							</div>
						</div>

						<div class="row form-group">
							<div class="control-group col-md-6">
								<label class="control-label"><span>Fees*</span></label> 
								<div class="controls">
									<select id="fees_id" name="fees_id" class="form-control" onchange="getFeesDetails(this.value)">
										<option value="">Select Fees</option>
									</select>
								</div>
							</div>
							<div class="control-group col-md-6 frequency_wrapper">
								<label class="control-label">Payment Frequency*</label>
								<select class="form-control" name="month_id" onchange="getMonthDiscount(this.value)" id="month_id">
					                <option value="">Select Payment Frequency</option>
					                <?php 
										if(isset($months) && !empty($months)){
											foreach($months as $cdrow){
									?>
										<option value="<?php echo $cdrow['month_id'];?>" <?php echo $sel; ?>><?php echo $cdrow['name'];?></option>
									<?php }}?>
				              	</select>
							</div>
						</div>

						<table class="table table-striped component_show" style="border: 1px">
									<thead>
										<tr>
											<th>Sequence No</th>
						        			<th>Fees Component</th>
						        			<th>Component Fees</th>
						        			<th>Discount Amount</th>
						        			<th>GST Amount</th>
						        			<th>Sub Amount</th>
										</tr>
									</thead>
									<tbody class="c_table">
									</tbody>
						</table>
						<input type="hidden" name="total_gst_amount" id="total_gst_amount">
						<input type="hidden" name="mandatory_component_amount" id="mandatory_component_amount">
						<input type="hidden" name="component_size" id="component_size">
						<input type="hidden" name="fees_total_amount" id="fees_total_amount">
						<input type="hidden" name="discount_amount" id="discountamountpercentage">

						<div class="row form-group">
							<!-- <div class="col-md-6 control-group">
								<label class="control-label"><span>Amount Collected*</span></label>
								<div class="controls">
									<input type="text" class="form-control" name="fees_amount_collected" value="" id="fees_amount_collected1">
								</div>
								<span class="collectedamountError text-danger"></span>
							</div> -->
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Discount Amount</span></label>
								<div class="controls">
									<input type="number" class="form-control" name="discount_amount_rs" id="total_discount_amount" readonly="readonly">
								</div>
							</div>
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Net Fees</span></label>
								<div class="controls">
									<input type="text" class="form-control" name="total_amount" value="" readonly="readonly" id="total_amount">
								</div>
							</div>
						</div>


						<div class="row form-group">
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Fees Remark*</span></label>
								<div class="controls">
									<textarea class="form-control" name="fees_remark" value="" placeholder="Fees Remark" id="fees_remark"><?php if(!empty($details[0]->fees_remark)){echo $details[0]->fees_remark;}?></textarea>
								</div>
							</div>
							<div class="col-md-6 control-group">
								<label class="control-label"><span>Fees Accepted By</span></label>
								<div class="controls">
									<input type="text" class="form-control" name="fees_approved_accepted_by_name" value="<?php echo $login_name;?>" readonly="readonly">
								</div>
							</div>
						</div>

						<div class="row form-group">
						</div>
						
						<div style="clear:both; margin-bottom: 2%;"></div>
						
						<div class="form-actions">
							<button type="submit" class="btn btn-primary" name="skip">Skip To Payment</button>
							<button type="submit" class="btn btn-primary save" name="paymentprocess">Save</button>
							<a href="<?php echo base_url();?>admission" class="btn btn-primary">Cancel</a>
							<button type="submit" class="btn btn-primary" name="group">Add Group Fees</button>
						</div>
					</form>
                </div>
            <div class="clearfix"></div>
            </div>
         </div>
    </div>        
	</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	$('.component_show').hide()
	$('.save').hide()
	$('.paymentprocess').hide()
	let zone_id = $('#zone_id').val()
	let center_id = $('#center_id').val()
	getFeesLevel(zone_id,center_id)
});

$('#fees_amount_collected1').keyup(function(){
	let collected_amount = $(this).val()
	let total_amount = $('#total_amount').val()
	if(collected_amount != '' || collected_amount != undefined){
		if(parseFloat(collected_amount) > total_amount){
			$('.collectedamountError').text('Course Amount not greater than Total Amount')
			return
		}
		else{
			$('.collectedamountError').text('')
		}
	}
})

function getCenters(zone_id,center_id = null)
{;
	if(center_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>admission/getCenters",
			data:{zone_id:zone_id, center_id:center_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#center_id").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#center_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#center_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getCenterUserGroupBatches(group_id){
	if(group_id != "" )
	{
		$.ajax({
			url:"<?php echo base_url();?>admission/getCenterUserGroupBatches",
			data:{group_id:group_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#batchid").html("<option value=''>Select</option>"+res['option']);
					}
					else
					{
						$("#batchid").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#batchid").html("<option value=''>Select</option>");
				}
			}
		});
	}
}


function getFeesLevel(zone_id,center_id){
	center_id = $('#center_id').val()
	zone_id = $('#zone_id').val()
	//  = $('#zone_id').val()
	if(center_id != undefined && zone_id != undefined && center_id != '' && zone_id != '' ){
		$.ajax({
			url:"<?php echo base_url();?>admission/getCenterFeesLevel",
			data:{center_id:center_id,zone_id:zone_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#fees_level").html(res['option']);
						let fees_level_id = $('#fees_level').val()
						let group_id = $('#groupid').val()
						getCenterFees(fees_level_id,group_id)
					}
					else
					{
						$("#fees_level").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#fees_level").html("<option value=''>Select</option>");
				}
			}
		});
	}
}

function getCenterFees(fees_level_id,group_id){
	fees_level_id = $('#fees_level').val()
	group_id = $('#groupid').val()
	center_id = $('#center_id').val()
	zone_id = $('#zone_id').val()
	if(fees_level_id != undefined && group_id != undefined && center_id != undefined && zone_id != undefined && fees_level_id != '' && group_id != '' && center_id != '' && zone_id != '' ){
		$.ajax({
			url:"<?php echo base_url();?>admission/getCenterFees",
			data:{group_id:group_id,fees_level_id:fees_level_id,center_id:center_id,zone_id:zone_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['status']=="success")
				{
					if(res['option'] != "")
					{
						$("#fees_id").html(res['option']);
						let fees_id = $('#fees_id').val()
						getFeesDetails(fees_id)
					}
					else
					{
						$("#fees_id").html("<option value=''>Select</option>");
					}
				}
				else
				{	
					$("#fees_id").html("<option value=''>Select</option>");
				}
			}
		});
	}
}


function getFeesDetails(fees_id){
	if(fees_id != "" )
	{
		$(".c_table").children().remove();
		$.ajax({
			url:"<?php echo base_url();?>admission/getFessDetails",
			data:{ fees_id:fees_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				console.log(res)
				if(res['status']=="success" )
				{
					// $('#fees_total_amount').val(res['result'][0]['amount'])
					// $('#fees_remaining_amount').val(res['result'][0]['amount'])
					// $('#total_amount').val(res['result'][0]['amount'])
					$('#max_installment').val(res['result'][0]['installment_no'])
					let total_amount = 0
			    	let t_amount = 0;
			    	let total_sub_amount = 0
			    	let total_gst_amount = 0
			    	let total_mandatory_amount = 0
			    	let total_discount_amount = 0





					$('.component_show').show()
					var size = res['result'].length
					$('#component_size').val(size)
					for(let i=0;i<size;i++){
						// let installment_amount = ($('#fees_total_amount').val()/installmentNo).toFixed(2)
						$(".c_table").append("<tr>");
						$(".c_table").append('<td>'+Number(i+1)+'</td>');
						$(".c_table").append('<td>'+res['result'][i]['fees_component_master_name']+'</td>');
						$(".c_table").append('<td>'+res['result'][i]['component_fees']+'</td>');
						$(".c_table").append('<td><div class="form-group"><input type="text" class="form-control discount_amount"  id="discountamount_'+i+'"name="discount_amount[]" readonly="readonly"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="text" class="form-control gst_amount"  id="gstamount_'+i+'"name="gst_amount[]" readonly="readonly"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="text" class="form-control sub_amount"  id="subamount_'+i+'"name="sub_amount[]" readonly="readonly"><input type="hidden" class="form-control component_type"  id="component_type_'+i+'" name="component_type[]" value="'+res['result'][i]['component_type']+'"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control component_gst_fees"  id="componentgstfees_'+i+'"name="component_gst_fees[]"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control is_discountable"  id="isdiscountable_'+i+'"name="is_discountable[]"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control is_gst"  id="isgst_'+i+'"name="is_gst[]"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control fees"  id="fees_'+i+'"name="fees[]"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control tax"  id="tax_'+i+'"name="tax[]"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control component_new_fees"  id="componentnewfees_'+i+'"name="component_new_fees[]"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control component_fees"  id="componentfees_'+i+'"name="component_fees[]"></div></td>');
						$(".c_table").append('<td><div class="form-group"><input type="hidden" class="form-control original_gst_fees"  id="originalgstfees_'+i+'"name="original_gst_fees[]"></div></td>');
						$(".c_table").append("</tr>");
						$(".c_table").append("</br>");
						$('#isdiscountable_'+i).val(res['result'][i]['is_discountable'])
						$('#isgst_'+i).val(res['result'][i]['is_gst'])
						$("#discountamount_"+i).val(0);
						$('#fees_'+i).val(parseFloat(res['result'][i]['component_fees']))
						$('#tax_'+i).val(res['result'][i]['tax'])
						var frequancy = 1;
						if(res['result'][i]['component_type'] == "Annual" && $("#month_id").val() != "" ){
							var frequancy_array = $("#month_id option:selected").text().split(" ");
							frequancy = frequancy_array[0];
						}
						if(res['result'][i]['is_gst'] == 'Yes'){
							let gst_amount = ((parseFloat(res['result'][i]['component_fees']) * parseFloat(res['result'][i]['tax']))/100).toFixed(2);
							$('#gstamount_'+i).val(gst_amount)
							let componentgstfees = (parseFloat(res['result'][i]['component_fees']) - parseFloat(gst_amount)).toFixed(2); //gst added amount
							
							$('#componentgstfees_'+i).val(parseFloat(componentgstfees)*parseFloat(frequancy))
							$('#originalgstfees_'+i).val(gst_amount)
							$('#subamount_'+i).val(parseFloat(componentgstfees).toFixed(2))
						}
						else{
							$('#componentgstfees_'+i).val(res['result'][i]['component_fees'])
							var componentgstfees = $('#componentgstfees_'+i).val()
							// alert(componentgstfees);
							$('#subamount_'+i).val(parseFloat(componentgstfees)*parseFloat(frequancy))
							$('#gstamount_'+i).val(0)
							$('#originalgstfees_'+i).val(0)
						}
						if(res['result'][i]['is_mandatory'] == 'Yes'){
			    			total_mandatory_amount = total_mandatory_amount + parseFloat(res['result'][i]['component_fees'])
			    		}
			    		$('#componentfees_'+i).val(res['result'][i]['component_fees'])
			    		var componentgstfees = $('#componentgstfees_'+i).val()
			    		total_amount = total_amount + parseFloat(componentgstfees);
			    		total_gst_amount = total_gst_amount + parseFloat(res['result'][i]['gst_amount'])
			    		$('#total_gst_amount').val(total_gst_amount)
			    		t_amount = t_amount + parseFloat(res['result'][i]['component_fees'])//total amount column display
			    		let subamountval = $('#subamount_'+i).val()
			    		total_sub_amount = parseFloat(total_sub_amount) + parseFloat(subamountval)
			    		$('#total_amount').val(parseFloat(total_sub_amount).toFixed(2))
						// $('.instalment_amount').val(installment_amount)
						// $('.instalment_collected_amount').val(0)
						// $('.instalment_remaining_amount').val(installment_amount)
					}

					$(".c_table").append("<tr>");
					$(".c_table").append('<td><b>Total</b></td>');
					$(".c_table").append('<td></td>');
					$(".c_table").append('<td class="t_amount"></td>');
					$(".c_table").append('<td></td>');
					$(".c_table").append('<td></td>');
					$(".c_table").append('<td class="total_sub_amount"></td>');
					$(".c_table").append("</tr>");


			    	$('.total_sub_amount').text(parseFloat(total_sub_amount).toFixed(2))

			    	$('.t_amount').text((t_amount).toFixed(2))
					$('#total_discount_amount').val(parseFloat(total_discount_amount))
			    	$('#mandatory_component_amount').val(parseFloat(total_mandatory_amount).toFixed(2))
			    	$('#fees_total_amount').val(parseFloat(t_amount).toFixed(2));
			    	$('#fees_amount_collected').val(parseFloat(total_sub_amount))
			    	$('#fees_remaining_amount').val(0)
				}
				else
				{	
					$('#fees_total_amount').val(0)
					$('#fees_remaining_amount').val(0)
					$('#total_amount').val(0)
					$('#max_installment').val(0)
					$(".c_table").children().remove();
				}
			}
		});
	}
	else{
		$(".c_table").children().remove();
	}
}

function getMonthDiscount(month_id){
	$('#fees_amount_collected1').val('')
	if(month_id != '' && month_id != undefined){
		$.ajax({
			url:"<?php echo base_url();?>admission/getMonthDiscount",
			data:{month_id:month_id},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res['option'] == false){
					var discount_amount = 0
				}
				else{
					var discount_amount = res['option'][0]['discount'];
				}
				$('#discountamountpercentage').val(discount_amount)
				var fees_total_amount = $('#fees_total_amount').val();
				if(fees_total_amount == undefined){
				} 
				else{
					let total_discount_amount = 0; //sum of total discount amount
					let total_sub_amount = 0
					let total_gst_amount = 0;
					let component_size = $('#component_size').val()
					for(let i=0;i<component_size;i++){
						var frequancy = 1;
						if($('#component_type_'+i).val() == "Annual" && $("#month_id").val() != "" ){
							var frequancy_array = $("#month_id option:selected").text().split(" ");
							frequancy = frequancy_array[0];
						}
						if(discount_amount != 0){
							var is_discount = $('#isdiscountable_'+i).val()
							if(is_discount == 'Yes'){
								let component_fees = $('#fees_'+i).val()
								let discount_amount_inpercentage = (parseFloat(component_fees) * parseFloat(discount_amount))/100;
								$('#discountamount_'+i).val(discount_amount_inpercentage)
								if(discount_amount_inpercentage <= component_fees){

									total_discount_amount = parseFloat(total_discount_amount) + discount_amount_inpercentage
									$('#total_discount_amount').val(total_discount_amount)
									let is_gst = $('#isgst_'+i).val()
									if(is_gst == 'Yes'){
										let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage))
										$('#componentnewfees_'+i).val(component_new_fees)
										let tax = $('#tax_'+i).val()
										let component_gst_fees = ((parseFloat(component_new_fees) * parseFloat(tax))/100).toFixed(2);
										$('#componentgstfees_'+i).val(parseFloat(component_gst_fees).toFixed(2))
										$('#gstamount_'+i).val(component_gst_fees)
									}
									else{
										let component_new_fees = (parseFloat(component_fees)) - (parseFloat(discount_amount_inpercentage))
										$('#componentnewfees_'+i).val(parseFloat(component_new_fees).toFixed(2))
										let component_gst_fees = 0
										$('#componentgstfees_'+i).val(parseFloat(component_gst_fees).toFixed(2))
										$('#gstamount_'+i).val(0)
										$('#originalgstfees_'+i).val(0)
									}
									let component_new_fees = $('#componentnewfees_'+i).val()
									let component_gst_fees = $('#componentgstfees_'+i).val()
									let subamount = ((parseFloat(component_new_fees) - parseFloat(component_gst_fees))).toFixed(2)
									$('#subamount_'+i).val(parseFloat(subamount)*parseFloat(frequancy))
								}
								else{
									$('#discountamount_'+i).val(0)
									let component_fees = $('#componentgstfees_'+i).val()
									let gst_amount = $('#originalgstfees_'+i).val()
									$("#gstamount_"+i).val(gst_amount)
									let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
									$('#subamount_'+i).val(parseFloat(subamount)*parseFloat(frequancy))
								}
							}
							else{
								$('#discountamount_'+i).val(0)
								let component_fees = $('#componentgstfees_'+i).val()
								let gst_amount = $('#originalgstfees_'+i).val()
								$("#gstamount_"+i).val(gst_amount)
								let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
								$('#subamount_'+i).val(parseFloat(subamount)*parseFloat(frequancy))
							}
						}
						else{
							$('#total_discount_amount').val('')
							$('#discountamount_'+i).val(0)
							let component_fees = $('#componentfees_'+i).val()
							let gst_amount = $('#originalgstfees_'+i).val()
							$("#gstamount_"+i).val(gst_amount)
							let subamount = ((parseFloat(component_fees) - parseFloat(gst_amount))).toFixed(2)
							$('#subamount_'+i).val(parseFloat(subamount)*parseFloat(frequancy))
						}
						let gst_amount = $('#gstamount_'+i).val()
						total_gst_amount = total_gst_amount + parseFloat(gst_amount)
						$('#total_gst_amount').val(total_gst_amount)
						let sub_amount = $('#subamount_'+i).val()
						total_sub_amount = total_sub_amount + parseFloat(sub_amount)
						$('.total_sub_amount').text((total_sub_amount).toFixed(2))
					}
				}
				var amount_after_discount = Number(fees_total_amount) - Number(discount_amount);
				let totalsubamount = $('.total_sub_amount').text()
				$('#total_amount').val(totalsubamount)
				let installmentNo = $('.no_of_installments').val()
				let installment_amount = (amount_after_discount/installmentNo).toFixed(2)
				$('#fees_amount_collected').val(totalsubamount)
				let total_discount_amount = $('#total_discount_amount').val()
				$('#total_discount_amount').val(total_discount_amount)
				let discount_amount_rs = $('#total_discount_amount').val()
				$('.instalment_amount').val(installment_amount)
				let mandatory_component_amount = $('#mandatory_component_amount').val()
				let mandatory_discount_amount = parseFloat(mandatory_component_amount) - parseFloat(discount_amount_rs)
				$('#mandatory_component_amount').val(mandatory_discount_amount)
				$('.instalment_collected_amount').val(0)
				$('.instalment_remaining_amount').val(installment_amount)
				let installmentno = $('.no_of_installments').val()
				let installamount = (totalsubamount/installmentno).toFixed(2)
				$('.instalment_amount').val(installamount)
			}
		});
	}
}


var vRules = {
	zone_id : {required:true},
	center_id :  {required:true},
	groupid :  {required:true},
	batchid:{required:true},
	fees_level:{required:true},
	fees:{required:true},
	fees_amount_collected : {required:true},
	month_id :  {required:true},
	fees_remark : {required:true},
	
};
var vMessages = {
	zone_id:{required:"Please select zone."},
	center_id:{required:"Please select center."},
	groupid:{required:"Please select group."},
	batchid:{required:"Please select batch."},
	fees_level:{required:"Please select fees level."},
	fees:{required:"Please select fees."},
	fees_amount_collected :  {required:"Please enter course amount."},
	month_id:{required:"Please select payment frequency."},
	fees_remark : {required:"Please enter fees remark."},
};

$("#feesInfoform-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>admission/submitfeesGroupInfoForm";
		$("#feesInfoform-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					if(res['flag'] == 1){
						var student_id = btoa(res['student_id'])
						window.location = "<?php echo base_url();?>admission/groupfees?text="+student_id;
					}
					else{
						var student_id = btoa(res['student_id'])
						window.location = "<?php echo base_url();?>admission/paymentprocess?text="+student_id;
					}
				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});

function checkAllowFrequency(){

	if($("#groupid").val() != ""){
		$.ajax({
			url:"<?php echo base_url();?>admission/checkAllowFrequency",
			data:{group_id:$("#groupid").val()},
			dataType: 'json',
			method:'post',
			success: function(res)
			{
				if(res.status){
					$(".frequency_wrapper").show();
				}else{
					$(".frequency_wrapper").hide();
				}
			}
		});
	}else{

	}
}


document.title = "Add - GroupFees";

 
</script>					
