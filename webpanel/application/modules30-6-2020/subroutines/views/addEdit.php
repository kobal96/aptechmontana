<?php 
//error_reporting(0);
?>
<style>
	.appended .appenddia{
		border-bottom: 1px solid black;
	}
</style>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>
<div id="content" class="content-wrapper">
				<div class="page-title">
                  <div>
                    <h1>Subroutines</h1>            
                  </div>
                  <div>
                    <ul class="breadcrumb">
                      <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
                      <li><a href="<?php echo base_url();?>subroutines">Subroutine</a></li>
                    </ul>
                  </div>
                </div>
                <div class="card">       
                 <div class="card-body">             
                    <div class="box-content">
                        <div class="col-sm-8 col-md-12">
							<form class="form-horizontal" id="form-validate" method="post" enctype="multipart/form-data">
								<input type="hidden" id="subroutine_id" name="subroutine_id" value="<?php if(!empty($details[0]->subroutine_id)){echo $details[0]->subroutine_id;}?>" />
								
								<div class="control-group form-group">
									<label class="control-label"><span>Routine Type*</span></label> 
									<div class="controls">
										<select id="routine_type" name="routine_type" class="form-control">
											<option value="">Select Routine Type</option>
											<option value="Activity" <?php if(!empty($details[0]->routine_type) && $details[0]->routine_type == 'Activity'){?>selected<?php }?>>Activity</option>
											<option value="Food" <?php if(!empty($details[0]->routine_type) && $details[0]->routine_type == 'Food'){?>selected<?php }?>>Food</option>
											<option value="Incident" <?php if(!empty($details[0]->routine_type) && $details[0]->routine_type == 'Incident'){?>selected<?php }?>>Incident</option>
											<option value="Mood" <?php if(!empty($details[0]->routine_type) && $details[0]->routine_type == 'Mood'){?>selected<?php }?>>Mood</option>
											<option value="Notes" <?php if(!empty($details[0]->routine_type) && $details[0]->routine_type == 'Notes'){?>selected<?php }?>>Notes</option>
											<option value="Sleep" <?php if(!empty($details[0]->routine_type) && $details[0]->routine_type == 'Sleep'){?>selected<?php }?>>Sleep</option>
											
											
										</select>
									</div>
								</div>
							
								<div class="control-group form-group">
									<label class="control-label"><span>Subroutine*</span></label>
									<div class="controls">
										<input type="text" class="form-control required" placeholder="Enter Subroutine" id="subroutine_name" name="subroutine_name" value="<?php if(!empty($details[0]->subroutine_name)){echo $details[0]->subroutine_name;}?>" >
									</div>
								</div>
								
								<div class="form-actions form-group">
									<button type="submit" class="btn btn-primary">Submit</button>
									<a href="<?php echo base_url();?>subroutines" class="btn btn-primary">Cancel</a>
								</div>
							</form>
                        </div>
                    <div class="clearfix"></div>
                    </div>
                 </div>
                </div>        
			</div><!-- end: Content -->								
<script>

 
$( document ).ready(function() {
	<?php 
		if(!empty($details[0]->newsletter_id)){
	?>
		//getRegion('<?php echo $details[0]->zone_text; ?>', '<?php echo $details[0]->region_text; ?>');
		//getArea('<?php echo $details[0]->region_text; ?>', '<?php echo $details[0]->area_text; ?>');
		//getCenter('<?php echo $details[0]->area_text; ?>', '<?php echo $details[0]->center_id; ?>');
	<?php }?>
	
});

var vRules = {
	routine_type:{required:true},
	subroutine_name:{required:true, alphanumericwithspace:true}
	
};
var vMessages = {
	routine_type:{required:"Please select routine type."},
	subroutine_name:{required:"Please enter subroutine."}
	
};

$("#form-validate").validate({
	rules: vRules,
	messages: vMessages,
	submitHandler: function(form) 
	{
		var act = "<?php echo base_url();?>subroutines/submitForm";
		$("#form-validate").ajaxSubmit({
			url: act, 
			type: 'post',
			cache: false,
			clearForm: false,
			success: function (response) {
				var res = eval('('+response+')');
				if(res['success'] == "1")
				{
					displayMsg("success",res['msg']);
					setTimeout(function(){
						window.location = "<?php echo base_url();?>subroutines";
					},2000);

				}
				else
				{	
					//$("#error_msg").show();
					displayMsg("error",res['msg']);
					return false;
				}
			}
		});
	}
});


document.title = "AddEdit - Subroutine";

 
</script>					
