<?php 
//error_reporting(0);
?>

<div id="content" class="content-wrapper">
	<div class="page-title">
      <div>
        <h1>Payment Details</h1>            
      </div>
      <div>
        <ul class="breadcrumb">
          <li><a href="<?php echo base_url();?>home"><i class="fa fa-home fa-lg"></i></a></li>
          <li><a href="<?php echo base_url();?>admission">Admission</a></li>
        </ul>
      </div>
    </div>
    <div class="card">       
     <div class="card-body">             
        <div class="box-content">
            <div class="col-sm-8 col-md-12">
				<div class="table-responsive scroll-table" style="overflow-x:auto;width:100%;">
					<table class="display table table-bordered non-bootstrap">
						<thead>
							<tr>
								<th>Receipt No</th>
								<th>Created On</th>
								<th>Invoice</th>
							</tr>
						</thead>
						
						<tbody id="tableBody1">
							<?php
								if(!empty($receipts)){
									$count1 = 1;
									for($i=0;$i<sizeof($receipts);$i++){
								?>	
									<tr>
										<td>
											<?php if(!empty($receipts[$i]['receipt_no'])){ echo $receipts[$i]['receipt_no']; }?>
										</td>
										<td>
											<?php if(!empty($receipts[$i]['created_on'])){ echo $receipts[$i]['created_on']; }?>
										</td>
										<td style="text-align: center;">
											<?php if(!empty($receipts[$i]['receipt_file'])){?>
												<a href="<?php echo base_url();?>paymentdetails/viewPdf?text=<?php echo $receipts[$i]['fees_payment_receipt_id']; ?>" "  title=""><i class="fa fa-file-pdf-o" style="font-size: 30px"></i><br/>View Pdf</a>
											<?php }?>
										</td>
									</tr>
								<?php $count1++; }}?>
						</tbody>
					</table>					
				</div>
				<div style="clear:both; margin-bottom: 2%;"></div>
				<div class="form-actions form-group">
					<a href="<?php echo base_url();?>admission" class="btn btn-primary">Back</a>
				</div>
            </div>
        <div class="clearfix"></div>
        </div>
     </div>
    </div>        
</div><!-- end: Content -->								
<script>


 
$( document ).ready(function() {
	
});




document.title = "Payment Details";

 
</script>					
