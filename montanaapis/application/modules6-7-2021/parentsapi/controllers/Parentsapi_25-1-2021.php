<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI

class Parentsapi extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('parentsapimodel', '', TRUE);
		$this->load->helper('core_helper');
		ini_set( 'memory_limit', '25M' );
		ini_set('upload_max_filesize', '25M');  
		ini_set('post_max_size', '25M');  
		ini_set('max_input_time', 3600);  
		ini_set('max_execution_time', 3600);
		
	}
	
	private function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	function pphpinfo(){
	    echo phpinfo();exit;
	}

	//For getting activity list
	function getMyActivities(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['academicyear'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide academic year.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['studentid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide student id .", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['zoneid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide Zone id .", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['courseid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide course year.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['batchid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide batch id year.", "body" => NULL));
						exit;
					}

					if(empty($response_headers['categoryid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide category id .", "body" => NULL));
						exit;
					}
					
					//get all activity according to user wise 
					$checkstatus = $this->parentsapimodel->getdata("tbl_student_master", "*","student_id='".$response_headers['userid']."' ");
					//echo "<pre>";print_r($getCoursesCategories);exit;
					
					if($checkstatus[0]['status'] == 'In-active'){
						$data = array();
						$data['fcm_token'] = '';
						$data['device_id'] = '';
						$data['apptype'] = '';
						$this->parentsapimodel->updateRecord('tbl_student_master', $data, "student_id = '".$response_headers['userid']."' ");
						//echo "<pre>";print_r($getCoursesCategories);exit;."' ");
						
						echo json_encode(array("status_code" => "200", "msg" => "User Logout Successfully.", "body" => NULL));
						exit;
					}
					if(!empty($checkstatus)){
						$getactivecenter = $this->parentsapimodel->getdata("tbl_centers", "*", "center_id='".$checkstatus[0]['center_id']."' && status = 'Active' ");
						if(empty($getactivecenter)){
							
							$data = array();
							$data['fcm_token'] = '';
							$data['device_id'] = '';
							$data['apptype'] = '';
							$this->parentsapimodel->updateRecord('tbl_student_master', $data, "student_id = '".$response_headers['userid']."' ");
							//echo "<pre>";print_r($getCoursesCategories);exit;."' ");
							
							echo json_encode(array("status_code" => "200", "msg" => "User Logout Successfully.", "body" => NULL));
							exit;
						}
						
					}
					
					$page = (!empty($response_headers['page'])) ? $response_headers['page'] : 0;

					$condition = "asmp.student_id='".$response_headers['userid']."' &&  i.academic_year_id='".$response_headers['academicyear']."' && i.zone_id ='".$response_headers['zoneid']."'  && i.center_id='".$response_headers['centerid']."'  &&  asmp.category_id='".$response_headers['categoryid']."' &&  asmp.batch_id ='".$response_headers['batchid']."' &&  asmp.course_id ='".$response_headers['courseid']."' AND activity_date <='".date('Y-m-d 23:59:59')."'  ";
					if(!empty($_POST['search_key']) && isset($_POST['search_key'])){
						$condition .=" AND i.activity_title like '%".$_POST['search_key']."%' ";
					}
					// $submittedcondition = "student_id ='".$response_headers['userid']."' &&  batch_id ='".$response_headers['batchid']."'  && course_id ='".$response_headers['courseid']."' ";
					
					// $getActivity = $this->parentsapimodel->getActivityRecords($condition,$submittedcondition,$page);
					$getActivity = $this->parentsapimodel->getActivityRecords($condition,$page);
					// echo "<pre>";
					// print_r($getActivity);
					// exit;
					if($getActivity){
						foreach ($getActivity as $key => $value) {
							$getActivitydata[$key]['activity_id'] = $value['activity_id'] ;
							$getActivitydata[$key]['activity_title'] = $value['activity_title'] ;
							$getActivitydata[$key]['activity_description'] = $value['activity_description'];
							$getActivitydata[$key]['created_on'] = $value['created_on'];
							$getActivitydata[$key]['resource_type'] = '';
							$getActivitydata[$key]['resource_data'] = '';
							$getActivitydata[$key]['file_type'] = '';
							$getActivitydata[$key]['submission_date'] = $value['submission_date'];
							$getActivitydata[$key]['activity_date'] =  date('d-M-Y h:i a',strtotime((!empty($value['activity_date'])?$value['activity_date'] :$value['created_on'] )));
							
							if($value['resource_type'] == 'images'){
								$getActivitydata[$key]['resource_type'] = "images";
								$all_images = explode( ",",$value['resource_data']);
								$getActivitydata[$key]['resource_data'] = FRONT_URL.'/images/activity_resources/'.$all_images[0];
							}else if($value['resource_type'] == 'capture'){
								$getActivitydata[$key]['resource_type'] = "capture";
								$getActivitydata[$key]['resource_data'] = FRONT_URL.'/images/activity_resources/'.$value['resource_data'];
							}else if ($value['resource_type'] == 'timetable'){
								
								$getActivitydata[$key]['resource_type'] = "timetable";
								$getActivitydata[$key]['file_type'] = $value['file_type'];
								$getTimetableImage= $this->parentsapimodel->getdata("tbl_timetable_master", "*", "timetable_id='".$value['resource_data']."' && status = 'Active' ");
								if($getTimetableImage){
									$getActivitydata[$key]['resource_data'] = FRONT_ROOT_URL.'/images/timetable_images/'.$getTimetableImage[0]['cover_image'];
								}
							}else if($value['resource_type'] == 'resource'){
								$getActivitydata[$key]['resource_type'] = "resource";
								$getActivitydata[$key]['file_type'] = $value['file_type'];
								$getdocumentImage= $this->parentsapimodel->getdata("tbl_document_folders", "*", "document_folder_id='".$value['resource_data']."' && status = 'Active' ");
								if($getdocumentImage){
									$getActivitydata[$key]['resource_data'] = FRONT_ROOT_URL.'/images/document_folder_images/'.$getdocumentImage[0]['cover_image'];	
								}
							}else if($value['resource_type'] == 'pdf'){
								$getActivitydata[$key]['resource_type'] = "pdf";
								$getActivitydata[$key]['resource_data'] = FRONT_URL.'/images/activity_resources/'.$value['resource_data'];
							}else if($value['resource_type'] == 'audio'){
								$getActivitydata[$key]['resource_type'] = "audio";
								$getActivitydata[$key]['resource_data'] = FRONT_URL.'/images/activity_resources/'.$value['resource_data'];
							}else if($value['resource_type'] == 'video'){
								$getActivitydata[$key]['resource_type'] = "video";
								$getActivitydata[$key]['resource_data'] = FRONT_URL.'/images/activity_resources/'.$value['resource_data'];
							}else if($value['resource_type'] == 'link'){
								$getActivitydata[$key]['resource_type'] = "link";
								$getActivitydata[$key]['resource_data'] = $value['resource_data'];
							}else if($value['resource_type'] == 'notes'){
								$getActivitydata[$key]['resource_type'] = "notes";
								$getActivitydata[$key]['resource_data'] = $value['resource_data'];
							}
						}
						// print_r($getActivitydata);
						
						if(!empty($getActivitydata) && isset($getActivitydata)){
							echo json_encode(array("status_code" => "200", "msg" => "Activity found.", "body" => $getActivitydata));
							exit;
						}

					}else if($page > 0 && empty($getActivity)){
							echo json_encode(array("status_code" => "401", "msg" => "No more activity found", "body" => NULL));
							exit;
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "Activity not found.", "body" => NULL));
						exit;
					}
				
					
					//echo "<pre>";print_r($getStudentProfile);exit;
					
					echo json_encode(array("status_code" => "200", "msg" => "Information found successfully.", "body" => $getStudentProfile));
					exit;
					
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}

	//For getting activity detail
	function getMyActivityDetail(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['academicyear'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide academic year.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['studentid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide student id .", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['zoneid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide Zone id .", "body" => NULL));
						exit;
					}
					
					// if(empty($response_headers['courseid'])){
					// 	echo json_encode(array("status_code" => "400", "msg" => "Please provide course id.", "body" => NULL));
					// 	exit;
					// }
					
					// if(empty($response_headers['batchid'])){
					// 	echo json_encode(array("status_code" => "400", "msg" => "Please provide batch id year.", "body" => NULL));
					// 	exit;
					// }

					// if(empty($response_headers['categoryid'])){
					// 	echo json_encode(array("status_code" => "400", "msg" => "Please provide category id .", "body" => NULL));
					// 	exit;
					// }
					
					//get all activity according to user wise 
					$checkstatus = $this->parentsapimodel->getdata("tbl_student_master", "*","student_id='".$response_headers['userid']."' ");
					//echo "<pre>";print_r($getCoursesCategories);exit;
					
					if($checkstatus[0]['status'] == 'In-active'){
						$data = array();
						$data['fcm_token'] = '';
						$data['device_id'] = '';
						$data['apptype'] = '';
						$this->parentsapimodel->updateRecord('tbl_student_master', $data, "student_id = '".$response_headers['userid']."' ");
						//echo "<pre>";print_r($getCoursesCategories);exit;."' ");
						
						echo json_encode(array("status_code" => "200", "msg" => "User Logout Successfully.", "body" => NULL));
						exit;
					}
					if(!empty($checkstatus)){
						$getactivecenter = $this->parentsapimodel->getdata("tbl_centers", "*", "center_id='".$checkstatus[0]['center_id']."' && status = 'Active' ");
						if(empty($getactivecenter)){
							
							$data = array();
							$data['fcm_token'] = '';
							$data['device_id'] = '';
							$data['apptype'] = '';
							$this->parentsapimodel->updateRecord('tbl_student_master', $data, "student_id = '".$response_headers['userid']."' ");
							//echo "<pre>";print_r($getCoursesCategories);exit;."' ");
							
							echo json_encode(array("status_code" => "200", "msg" => "User Logout Successfully.", "body" => NULL));
							exit;
						}
						
					}
					
					// $page = (!empty($response_headers['page'])) ? $response_headers['page'] : 0;

					// $condition = " as.student_id = '".$response_headers['studentid']."' ||  (i.activity_id ='".$response_headers['activityid']."' &&  i.academic_year_id='".$response_headers['academicyear']."' && i.zone_id ='".$response_headers['zoneid']."'  && i.center_id='".$response_headers['centerid']."') ";
					$condition = " asm.student_id = '".$response_headers['studentid']."' && i.activity_id ='".$response_headers['activityid']."' &&  i.academic_year_id='".$response_headers['academicyear']."' && i.zone_id ='".$response_headers['zoneid']."'  && i.center_id='".$response_headers['centerid']."' ";

					$getActivity = $this->parentsapimodel->getActivityRecordDetail($condition);
					// echo "<pre>";
					// print_r($getActivity);
					// exit;
					if($getActivity){
						foreach ($getActivity as $key => $value) {
							$getActivitydata['activity_id'] = $value['activity_id'] ;
							$getActivitydata['activity_title'] = $value['activity_title'] ;
							$getActivitydata['activity_description'] = $value['activity_description'];
							$getActivitydata['created_on'] = $value['created_on'];
							$getActivitydata['resource_type'] = '';
							$getActivitydata['resource_data'] = '';
							$getActivitydata['file_type'] = '';
							$getActivitydata['activity_date'] =  date('d-M-Y h:i a',strtotime((!empty($value['activity_date'])?$value['activity_date'] :$value['created_on'] )));

							if(empty($value['activity_accepted_by_teacher'])){
								$getActivitydata['activity_submitted'] = 0;
							}else{
								$getActivitydata['activity_submitted'] = 1;

							}
								
							if($value['resource_type'] == 'images'){
								$getActivitydata['resource_type'] = "images";
								$all_images = explode( ",",$value['resource_data']);
								$multiimage = array();
								foreach ($all_images as $keyimage => $valueimage) {
									array_push($multiimage,FRONT_URL.'/images/activity_resources/'.$valueimage);
								}
								$getActivitydata['resource_data'] = implode(",",$multiimage);
							}else if($value['resource_type'] == 'capture'){
								$getActivitydata['resource_type'] = "capture";
								$getActivitydata['resource_data'] = FRONT_URL.'/images/activity_resources/'.$value['resource_data'];
							}else if ($value['resource_type'] == 'timetable'){
								$getActivitydata['file_type'] = $value['file_type'] ;
								if($value['file_type'] == 'video'){
									$table_name = "tbl_timetable_videos";
									$column_name ="timetable_video_id";
									$file_name = "video_url";
								}else{
									$table_name = "tbl_timetable_documents";
									$column_name ="timetable_document_id";
									$file_name = "doc_file";
								}
								$getTimetableData = $this->parentsapimodel->getdata($table_name, "*", "status = 'Active' && $column_name = '".$value['resource_data']."'  ");
								if($getTimetableData){
									$getActivitydata['resource_type'] = "timetable";
									if($value['file_type'] == 'video'){
										$getActivitydata['resource_data'] = $getTimetableData[0][$file_name];
										
									}else{
										$getActivitydata['resource_data'] = FRONT_ROOT_URL.'/images/tbl_folder_document/'.$getTimetableData[0][$file_name];
									}
								}
							}else if ($value['resource_type'] == 'resource'){
								$getdocumentData= $this->parentsapimodel->getdata("tbl_folder_document", "*", "folder_document_id='".$value['resource_data']."' && status = 'Active' ");
								if($getdocumentData){
									$getActivitydata['resource_type'] = "resource";
									$getActivitydata['file_type'] = $value['file_type'] ;
									if($getdocumentData[0]['doc_type'] == 'video'){
										$getActivitydata['resource_data'] = $getdocumentData[0]['doc_type'] ;
									}else{
										$getActivitydata['resource_data'] = FRONT_ROOT_URL.'/images/folder_documents/'.$getdocumentData[0]['document_field_value'];
									}
								}
							}else if($value['resource_type'] == 'pdf'){
								$getActivitydata['resource_type'] = "pdf";
								$getActivitydata['resource_data'] = FRONT_URL.'/images/activity_resources/'.$value['resource_data'];
							}else if($value['resource_type'] == 'audio'){
								$getActivitydata['resource_type'] = "audio";
								$getActivitydata['resource_data'] = FRONT_URL.'/images/activity_resources/'.$value['resource_data'];
							}else if($value['resource_type'] == 'video'){
								$getActivitydata['resource_type'] = "video";
								$getActivitydata['resource_data'] = FRONT_URL.'/images/activity_resources/'.$value['resource_data'];
							}else if($value['resource_type'] == 'link'){
								$getActivitydata['resource_type'] = "link";
								$getActivitydata['resource_data'] = $value['resource_data'];
							}else if($value['resource_type'] == 'notes'){
								$getActivitydata['resource_type'] = "notes";
								$getActivitydata['resource_data'] = $value['resource_data'];
							}


							// for student submitted data
							$getActivityResponseStudentdata['response_date'] = (!empty($value['student_submission_date'])?date('d-M-Y h:i a',strtotime($value['student_submission_date'])) :null);
							$getActivityResponseStudentdata['response_msg'] = $value['student_activity_comment'];

							if($value['student_resource_type'] == 'images'){
							$getActivityResponseStudentdata['response_type'] = "images";
							$all_images = explode( ",",$value['student_resource_data']);
							$multiimage = array();
							foreach ($all_images as $keyimage => $valueimage) {
								array_push($multiimage,FRONT_URL.'/images/activity_resources_response/'.$valueimage);
							}
							$getActivityResponseStudentdata['response_data'] = implode(",",$multiimage);
							}else if($value['student_resource_type'] == 'capture'){
								$getActivityResponseStudentdata['response_type'] = "capture";
								$getActivityResponseStudentdata['response_data'] = FRONT_URL.'/images/activity_resources_response/'.$value['student_resource_data'];
							}else if($value['student_resource_type'] == 'pdf'){
								$getActivityResponseStudentdata['response_type'] = "pdf";
								$getActivityResponseStudentdata['response_data'] = FRONT_URL.'/images/activity_resources_response/'.$value['student_resource_data'];
							}else if($value['student_resource_type'] == 'audio'){
								$getActivityResponseStudentdata['response_type'] = "audio";
								$getActivityResponseStudentdata['response_data'] = FRONT_URL.'/images/activity_resources_response/'.$value['student_resource_data'];
							}else if($value['student_resource_type'] == 'video'){
								$getActivityResponseStudentdata['response_type'] = "video";
								$getActivityResponseStudentdata['response_data'] = FRONT_URL.'/images/activity_resources_response/'.$value['student_resource_data'];
							}else if($value['student_resource_type'] == 'link'){
								$getActivityResponseStudentdata['response_type'] = "link";
								$getActivityResponseStudentdata['response_data'] = $value['student_resource_data'];
							}else if($value['student_resource_type'] == 'notes'){
								$getActivityResponseStudentdata['response_type'] = "notes";
								$getActivityResponseStudentdata['response_data'] = $value['student_resource_data'];
							}


							// make a student submitted array 
							$getActivityStudentDetail['student_name']=$value['student_first_name'].' '.$value['student_last_name']; 
							$getActivityStudentDetail['student_id']= $value['student_id']; 
							$getActivityStudentDetail['student_profile_pic']= FRONT_URL."/images/student_profile_pics/".$value['profile_pic'];
							// $getActivityStudentDetail['submision_date']= $value['created_on'];

							// for teacher detail
							$getTeacherDetail['teacher_name'] = $value['first_name'].' '.$value['last_name'];
							$getTeacherDetail['teacher_msg'] = $value['teacher_activity_comment'];
							$getTeacherDetail['date'] = (!empty($value['teacher_submission_date'])?date('d-M-Y h:i a',strtotime($value['teacher_submission_date'])) :null);
							$getTeacherDetail['teacher_profile_pic'] = FRONT_URL."/images/teacher_profile_pics/".$value['teacher_profile_pic'];

						}
						
						// print_r($getActivitydata);

						if(!empty($getActivitydata) && isset($getActivitydata)){
							echo json_encode(array("status_code" => "200", "msg" => "Activity found.", "body" =>array("activity_detail"=> $getActivitydata, "student_detail" =>$getActivityStudentDetail,"student_response_detail"=>$getActivityResponseStudentdata,"teacher_response_detail"=>$getTeacherDetail)));
							exit;
						}

					}else if($page > 0 && empty($getActivity)){
							echo json_encode(array("status_code" => "401", "msg" => "No more activity found", "body" => NULL));
							exit;
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "Activity not found.", "body" => NULL));
						exit;
					}
				
					
					//echo "<pre>";print_r($getStudentProfile);exit;
					
					echo json_encode(array("status_code" => "200", "msg" => "Information found successfully.", "body" => $getStudentProfile));
					exit;
					
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}


	// submit activity response from parent end for teacher 
	function submitActivityResponse(){
		// print_r($_POST);
		// print_r($_FILES);
		// exit;
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['academicyear'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide academic year.", "body" => NULL));
						exit;
					}

					// if(empty($response_headers['teacherid'])){
					// 	echo json_encode(array("status_code" => "400", "msg" => "Please provide teacher id .", "body" => NULL));
					// 	exit;
					// }
					
					if(empty($response_headers['studentid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide student id .", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['zoneid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide Zone id .", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['courseid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide course id.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['batchid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide batch id year.", "body" => NULL));
						exit;
					}

					if(empty($response_headers['categoryid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide category id .", "body" => NULL));
						exit;
					}
					
					//get all activity according to user wise 
					$checkstatus = $this->parentsapimodel->getdata("tbl_student_master", "*","student_id='".$response_headers['userid']."' ");
					//echo "<pre>";print_r($getCoursesCategories);exit;
					
					if($checkstatus[0]['status'] == 'In-active'){
						$data = array();
						$data['fcm_token'] = '';
						$data['device_id'] = '';
						$data['apptype'] = '';
						$this->parentsapimodel->updateRecord('tbl_student_master', $data, "student_id = '".$response_headers['userid']."' ");
						//echo "<pre>";print_r($getCoursesCategories);exit;."' ");
						
						echo json_encode(array("status_code" => "200", "msg" => "User Logout Successfully.", "body" => NULL));
						exit;
					}
					if(!empty($checkstatus)){
						$getactivecenter = $this->parentsapimodel->getdata("tbl_centers", "*", "center_id='".$checkstatus[0]['center_id']."' && status = 'Active' ");
						if(empty($getactivecenter)){
							
							$data = array();
							$data['fcm_token'] = '';
							$data['device_id'] = '';
							$data['apptype'] = '';
							$this->parentsapimodel->updateRecord('tbl_student_master', $data, "student_id = '".$response_headers['userid']."' ");
							//echo "<pre>";print_r($getCoursesCategories);exit;."' ");
							
							echo json_encode(array("status_code" => "200", "msg" => "User Logout Successfully.", "body" => NULL));
							exit;
						}
						
					}
					
					// $page = (!empty($response_headers['page'])) ? $response_headers['page'] : 0;
					$data= array();
					$files_upload = array();
					if($_POST['file_type'] == 'images' || $_POST['file_type'] == 'capture' || $_POST['file_type'] == 'video' || $_POST['file_type'] == 'audio' || $_POST['file_type'] == 'pdf'){
						// echo "inside";
						if(!empty($_FILES['file']['name']) && count(array_filter($_FILES['file']['name'])) > 0){ 
							$filesCount = count($_FILES['file']['name']); 
							for($i = 0; $i < $filesCount; $i++){ 

								$_FILES['files']['name']     = $_FILES['file']['name'][$i]; 
								$_FILES['files']['type']     = $_FILES['file']['type'][$i]; 
								$_FILES['files']['tmp_name'] = $_FILES['file']['tmp_name'][$i]; 
								$_FILES['files']['error']    = $_FILES['file']['error'][$i]; 
								$_FILES['files']['size']     = $_FILES['file']['size'][$i]; 
								
								// File upload configuration 
								$config['upload_path'] = DOC_ROOT."/images/activity_resources_response/"; 
								// $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf';
								$config['allowed_types'] = '*';
								// Load and initialize upload library 
								$this->load->library('upload', $config); 
								$this->upload->initialize($config); 
								
								// Upload file to server 
								if($this->upload->do_upload('files')){ 
									// Uploaded file data 
									$fileData = $this->upload->data(); 
									// echo "file uploaded";
									array_push($files_upload,$fileData['file_name']);
									// $resource[$i]['file_name'] = $fileData['file_name']; 
								}else{  
									$errorUploadType .= $_FILES['file']['name'].' | ';  
								} 
							} 
						}
						
					$data['resource_data'] = implode(",",$files_upload);
					}else if($_POST['file_type'] == 'link' || $_POST['file_type'] == 'notes'){
					    if(!empty($_POST['content']) && isset($_POST['content'])){
						    $data['resource_data'] = $_POST['content'];
					    }
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "No file attached in this activity .", "body" => NULL));
						exit;
					}
					$data['resource_type'] = $_POST['file_type'];
					
                    if(empty($response_headers['studentid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide student id .", "body" => NULL));
						exit;
					}
					// print_r($files_upload);
					// print_r($files_upload);
					// if(count($files_upload)>1){
					// }else{
					// 	$data['resource_data'] = $files_upload;
					// }
						

					// print_r($data);	
					$data['activity_id'] = $response_headers['activityid'];
					// $data['teacher_id'] = '';
					$data['course_id'] = $response_headers['courseid'];
					$data['batch_id'] = $response_headers['batchid'];
					$data['student_activity_comment'] =$_POST['student_activity_comment'];
					$data['student_id'] = $response_headers['studentid'];
					$data['activity_accepted_by_teacher'] = 'No';
					$data['status'] = 'Active';
					$data['created_on'] = date("Y-m-d H:i:s");
					$data['created_by'] = $response_headers['userid'];

					$result =  $this->parentsapimodel->insertData('tbl_activity_submission',$data,"1");	
					if($result){
							echo json_encode(array("status_code" => "200", "msg" => "Activity submitted successfully", "body" => $result));
							exit;
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "Activity not found.", "body" => NULL));
						exit;
					}
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}
	/*function getallheaders_new() {
		$response_headers = getallheaders();
		foreach ($response_headers as $name => $val) {
			$name1 = str_replace('-', '_', $name);
			$response_header1[$name1] = $val;
		}

		$headers = array_change_key_case($response_header1, CASE_LOWER);
		return $headers;
	} */
	// 31-08-2020 Mayank created new function post server migration
	function getallheaders_new() {
		$headers = [];
		foreach ($_SERVER as $name => $value) {
			if (substr($name, 0, 5) == 'HTTP_') {
				$headers[strtolower(str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5))))))] = $value;
			}
		}
		return $headers;
		}

	function getUtoken() {
		$response_headers = $this->getallheaders_new();
	
		$device_id = '';
		$user_id = '';
		if (isset($response_headers['device_id']) && !empty($response_headers['device_id'])) {
			$device_id = $response_headers['device_id'];
		}

		if (isset($response_headers['user_id']) && !empty($response_headers['user_id'])) {
			$user_id = $response_headers['user_id'];
		}

		$data['device_id'] = $device_id;
		//$data['user_id'] = $user_id;
		//$data['status'] = 'Active';

		$result_token = $this->parentsapimodel->check_guest_utoken($device_id);

		//if ($result_token == false) {
			$this->parentsapimodel->updateRecord("tbl_admin_users", $data, "user_id='".$user_id."' ");
		//}

		$utoken = base64_encode($user_id . "base64");
		$res_data['utoken'] = $utoken;
	
		echo json_encode(array("status_code" => "200", "msg" => "Success", "body" => $res_data));
		exit;
	}

	function checkUtoken($user_token) {
		if (isset($user_token) && !empty($user_token)) {
			$encoded_utoken = $user_token;
			$utoken = base64_decode($encoded_utoken);
			$exp = explode("base64", $utoken);
			//echo "utoken: ".$exp[0];exit;

			$result = $this->parentsapimodel->check_utoken($exp[0]);

			if (is_array($result) && count($result)) {
				return true;
			}
			
		} else {
			return false;
		}
	}

	
	private function sentence_case($str) {
		$cap = true;
		$ret='';
		for($x = 0; $x < strlen($str); $x++){
			$letter = substr($str, $x, 1);
			if($letter == "." || $letter == "!" || $letter == "?"){
				$cap = true;
			}elseif($letter != " " && $cap == true){
				$letter = strtoupper($letter);
				$cap = false;
			} 
			$ret .= $letter;
		}
		return $ret;
	}

	private function set_upload_options() {
		//upload an image options //products
		$config = array();
		$config['upload_path'] = DOC_ROOT . "/degree/";
		$config['allowed_types'] = 'gif|jpg|png|pdf|doc|docx|jpeg';
		$config['max_size'] = '1000000';
		$config['overwrite'] = FALSE;

		return $config;
	}

	function save_fcm_token() {
	//userid, username, fcmtoken,deviceid, apptype
		$response_headers = $this->getallheaders_new();
		//echo "<pre>";print_r($response_headers);exit;
		$this->logout_user();
		//exit;
		if (!empty($response_headers['userid'])) {
			if (!empty($response_headers['username'])) {
				if (!empty($response_headers['apptype'])) {
					if (!empty($response_headers['fcmtoken'])) {
						if (!empty($response_headers['deviceid'])) {
							
							$userid = $response_headers['userid'];
							$username = $response_headers['username'];
							$usertype = $response_headers['usertype'];
							$fcmtoken = $response_headers['fcmtoken'];
							$deviceid = $response_headers['deviceid'];
							$apptype = $response_headers['apptype'];
							
							$result_data = $this->parentsapimodel->update_insert_fcm_token($userid, $username, $usertype, $fcmtoken, $deviceid, $apptype);
							if ($result_data) {
								echo json_encode(array("status_code" => "200", "msg" => "FCM Token Saved successfully.", "body" => $result_data));
								exit;
							} else {
								echo json_encode(array("status_code" => "400", "msg" => "Something went wrong while updating.", "body" => NULL));
								exit;
							}
							
						}else {
							echo json_encode(array("status_code" => "400", "msg" => "Authentication failed Device Id not provided.", "body" => NULL));
							exit;
						}
					
					}else {
						echo json_encode(array("status_code" => "400", "msg" => "Authentication failed FCM Token not provided.", "body" => NULL));
						exit;
					}
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Authentication failed App type not provided.", "body" => NULL));
					exit;
				}
			}else {
				echo json_encode(array("status_code" => "400", "msg" => "Authentication failed User name not provided.", "body" => NULL));
				exit;
			}
			
		}else {
			echo json_encode(array("status_code" => "400", "msg" => "Authentication failed User ID not provided.", "body" => NULL));
			exit;
		}
	}
		
	function getRNDno(){
		$response_headers = $this->getallheaders_new();
		$condition = "student_id='".$response_headers['studentid']."' && teacher_id='".$response_headers['teacherid']."' ";
		$getDetails = $this->parentsapimodel->getdata_orderby1("tbl_save_rndno", "rnd_number", $condition);
		//echo "<pre>";print_r($getDetails);exit;
		
		if (!empty($getDetails)) {
			echo json_encode(array("status_code" => "200", "msg" => "Newsletters.", "body" => $getDetails));
			exit;
		} else {
			echo json_encode(array("status_code" => "400", "msg" => "No newsletters found.", "body" => NULL));
			exit;
		}
	}
	
	//For getting list of countries
	function getCountries() {
		$response_headers = $this->getallheaders_new();
		$result = array();
		$result = $this->parentsapimodel->getdata("tbl_countries", "country_id, country_code,country_name,country_phonecode", "1=1");
		if (empty($result)) {
			$result = array();
		}
		
		echo json_encode(array("status_code" => "200", "msg" => "Country fetch successfully.", "body" => $result));
		exit;
	}
	
	//For getting list of states
	function getState() {
		$response_headers = $this->getallheaders_new();
		$result = array();
		$result = $this->parentsapimodel->getdata("tbl_states", "state_id, state_name", "country_id='".$response_headers['countryid']."'");
		if (empty($result)) {
			$result = array();
		}
		
		echo json_encode(array("status_code" => "200", "msg" => "State fetch successfully.", "body" => $result));
		exit;
	}
	
	//For getting list of cities
	function getCities() {
		$response_headers = $this->getallheaders_new();
		$result = array();
		$result = $this->parentsapimodel->getdata("tbl_cities", "city_id, city_name", "state_id='".$response_headers['stateid']."'");
		if (empty($result)) {
			$result = array();
		}
		
		echo json_encode(array("status_code" => "200", "msg" => " fetch successfully.", "body" => $result));
		exit;
	}
	
	//For login all users
	// function userLogin() {
	// 	//echo "here...";exit;
	// 	$response_headers = $this->getallheaders_new();
	// 	if (is_array($response_headers) && count($response_headers) > 0) {
		
	// 		if (empty($response_headers['username']) ) {
	// 			echo json_encode(array("status_code" => "400", "Metadata" => array("Message" => "Please enter username." ), "Data" => NULL ));
	// 			exit;
	// 		}
			 
	// 		if (!empty($response_headers['password'])) {
	// 			$password = md5($response_headers['password']);
	// 		} else {
	// 			echo json_encode(array("status_code" => "400", "Metadata" => array("Message" => "Please enter password." ), "Data" => NULL ));
	// 			exit;
	// 		}
			 
	// 		$condition = "enrollment_no='".$response_headers['username']."' &&  password='".$password."' ";
	// 		$result_login = $this->parentsapimodel->login_check($condition);
			
	// 		$result_data = $result_login[0];
	// 		//echo "<pre>";print_r($result_data);exit;
      
	// 		if (is_array($result_login) && count($result_login) > 0) {
	// 			$result_data['utoken'] = base64_encode($result_data['student_id']. "base64");
	// 			//get academic year
	// 			$current_date = date("Y-m-d");
	// 			$acadCondition = " start_date <= '".$current_date."' AND end_date >= '".$current_date."' AND status='Active' ";
	// 			$getAcadYear = $this->parentsapimodel->getdata_orderby_limit1("tbl_academic_year_master", "academic_year_master_id, academic_year_master_name", $acadCondition, " order by academic_year_master_id desc");
	// 			//echo "<pre>";print_r($getAcadYear);exit;
	// 			if(!empty($getAcadYear)){
	// 				$result_data['academic_year'] = $getAcadYear[0]['academic_year_master_id'];
	// 			}else{
	// 				$getDefaultAcadYear = $this->parentsapimodel->getdata_orderby_limit1("tbl_academic_year_master", "academic_year_master_id, academic_year_master_name", "1=1", " order by academic_year_master_id desc");
	// 				$result_data['academic_year'] = $getDefaultAcadYear[0]['academic_year_master_id'];
	// 			}
				
	// 			$result_data['dob'] = date("d-m-Y", strtotime($result_data['dob']));
	// 			$result_data['profile_pic_path'] = (!empty($result_data['profile_pic'])) ? FRONT_URL. "/images/student_profile_pics/".$result_data['profile_pic'] : '';
				
	// 			//get course details
	// 			$getCourseDetails = $this->parentsapimodel->getdata("tbl_student_details", "admission_date, category_id, course_id, batch_id, programme_start_date", "student_id='".$result_data['student_id']."' ");
	// 			//echo "<pre>";print_r($getCourseDetails);exit;
				
	// 			$result_data['admission_date'] = (!empty($getCourseDetails[0]['admission_date'])) ? date("d-m-Y", strtotime($getCourseDetails[0]['admission_date'])) : '';
	// 			$result_data['category_id'] = (!empty($getCourseDetails[0]['category_id'])) ? $getCourseDetails[0]['category_id'] : '';
	// 			$result_data['course_id'] = (!empty($getCourseDetails[0]['course_id'])) ? $getCourseDetails[0]['course_id'] : '';
	// 			$result_data['batch_id'] = (!empty($getCourseDetails[0]['batch_id'])) ? $getCourseDetails[0]['batch_id'] : '';
	// 			$result_data['programme_start_date'] = (!empty($getCourseDetails[0]['programme_start_date'])) ? date("d-m-Y", strtotime($getCourseDetails[0]['programme_start_date'])) : '';
				
	// 			$data = array();
	// 			$data['fcm_token'] = (!empty($response_headers['fcmtoken'])) ? $response_headers['fcmtoken'] : '';
	// 			$data['device_id'] = (!empty($response_headers['deviceid'])) ? $response_headers['deviceid'] : '';
	// 			$data['apptype'] = (!empty($response_headers['apptype'])) ? $response_headers['apptype'] : '';
	// 			$this->parentsapimodel->updateRecord('tbl_student_master', $data, "student_id = '".$result_login[0]['student_id']."' ");
						
	// 			echo json_encode(array("status_code" => "200", "msg" => "User login successfully.", "body" => $result_data));
	// 			exit;
	// 		} else {
	// 			echo json_encode(array("status_code" => "400", "msg" => "Incorrect Username or Password.", "body" => NULL));
	// 			exit;
	// 		}
	// 	} else {
	// 		echo json_encode(array("status_code" => "400", "msg" => "Hearder section empty.", "body" => NULL));
	// 		exit;
	// 	}
	// }
	
		
//For login all users
function userLogin() {
	//echo "here...";exit;
	$response_headers = $this->getallheaders_new();
	if (is_array($response_headers) && count($response_headers) > 0) {
	
		if (empty($response_headers['username']) ) {
			echo json_encode(array("status_code" => "400", "Metadata" => array("Message" => "Please enter username." ), "Data" => NULL ));
			exit;
		}
		 
		if (!empty($response_headers['password'])) {
			$password = md5($response_headers['password']);
		} else {
			echo json_encode(array("status_code" => "400", "Metadata" => array("Message" => "Please enter password." ), "Data" => NULL ));
			exit;
		}
		 
		$condition = "enrollment_no='".$response_headers['username']."' &&  password='".$password."' ";
		$result_login = $this->parentsapimodel->login_check($condition);
		
		$result_data = $result_login[0];
		//echo "<pre>";print_r($result_data);exit;
		
		if($result_login[0]['status'] == 'In-active'){
			echo json_encode(array("status_code" => "400", "msg" => "Invalid User, Please Contact Administrator", "body" => NULL));
					exit;
		}
			
		if(!empty($result_login)){
				$getactivecenter = $this->parentsapimodel->getdata("tbl_centers", "*", "center_id='".$result_login[0]['center_id']."' && status = 'Active' ");
				// echo "<pre>";print_r($getactivecenter);exit;
				if(empty($getactivecenter)){
					echo json_encode(array("status_code" => "400", "msg" => "Invalid User, Please Contact Administrator", "body" => NULL));
					exit;
				}

			}
  
		if (is_array($result_login) && count($result_login) > 0) {
			$result_data['utoken'] = base64_encode($result_data['student_id']. "base64");
			//get academic year
			$current_date = date("Y-m-d");
			$acadCondition = " start_date <= '".$current_date."' AND end_date >= '".$current_date."' AND status='Active' ";
			$getAcadYear = $this->parentsapimodel->getdata_orderby_limit1("tbl_academic_year_master", "academic_year_master_id, academic_year_master_name", $acadCondition, " order by academic_year_master_id desc");
		
			if(!empty($getAcadYear)){
				$result_data['academic_year'] = $getAcadYear[0]['academic_year_master_id'];
				$result_data['academic_year_name'] = $getAcadYear[0]['academic_year_master_name'];
			}else{
			// 	$getDefaultAcadYear = $this->parentsapimodel->getdata_orderby_limit1("tbl_academic_year_master", "academic_year_master_id, academic_year_master_name", "1=1", " order by academic_year_master_id desc");
			// 	$result_data['academic_year'] = $getDefaultAcadYear[0]['academic_year_master_id'];
				echo json_encode(array("status_code" => "400", "msg" => "current date in not belonging to any academic year ", "body" => NULL));
				   exit;
			}
			
			$result_data['dob'] = date("d-m-Y", strtotime($result_data['dob']));
			$result_data['profile_pic_path'] = (!empty($result_data['profile_pic'])) ? FRONT_URL. "/images/student_profile_pics/".$result_data['profile_pic'] : '';
			
			//get student for first time details
			$getadmissionDetails = $this->parentsapimodel->getdata("tbl_student_details", "admission_date, category_id, course_id, batch_id, programme_start_date", "student_id='".$result_data['student_id']."' ");
			//echo "<pre>";print_r($getadmissionDetails);exit;
			
			//get student details according to academic year details
			$getCourseDetails = $this->parentsapimodel->getdata("tbl_promoted_student", "*", "student_id='".$result_data['student_id']."' AND promoted_academic_year_id ='".$result_data['academic_year']."' ");
			
			// print_r($getCourseDetails);
			// echo $this->db->last_query();	
			if(empty($getCourseDetails)){
				echo json_encode(array("status_code" => "400", "msg" => "you are not assigned to any current academic year", "body" => NULL));
				   exit;
			}
			
			$result_data['admission_date'] = (!empty($getadmissionDetails[0]['admission_date'])) ? date("d-m-Y", strtotime($getadmissionDetails[0]['admission_date'])) : '';
			$result_data['category_id'] = (!empty($getCourseDetails[0]['category_id'])) ? $getCourseDetails[0]['category_id'] : '';
			$result_data['course_id'] = (!empty($getCourseDetails[0]['promoted_course_id'])) ? $getCourseDetails[0]['promoted_course_id'] : '';
			$result_data['batch_id'] = (!empty($getCourseDetails[0]['promoted_batch_id'])) ? $getCourseDetails[0]['promoted_batch_id'] : '';
			$result_data['programme_start_date'] = (!empty($getCourseDetails[0]['programme_start_date'])) ? date("d-m-Y", strtotime($getCourseDetails[0]['programme_start_date'])) : '';
			
			
			//getall academic year 
			$result_data['academic_year_list'] = $this->parentsapimodel->getdata("tbl_academic_year_master", "academic_year_master_id, academic_year_master_name", "status='Active' ");
			//echo "<pre>";print_r($getadmissionDetails);exit;
			
			$data = array();
			$data['fcm_token'] = (!empty($response_headers['fcmtoken'])) ? $response_headers['fcmtoken'] : '';
			$data['device_id'] = (!empty($response_headers['deviceid'])) ? $response_headers['deviceid'] : '';
			$data['apptype'] = (!empty($response_headers['apptype'])) ? $response_headers['apptype'] : '';
			$this->parentsapimodel->updateRecord('tbl_student_master', $data, "student_id = '".$result_login[0]['student_id']."' ");
					
			echo json_encode(array("status_code" => "200", "msg" => "User login successfully.", "body" => $result_data));
			exit;
		} else {
			echo json_encode(array("status_code" => "400", "msg" => "Incorrect Username or Password.", "body" => NULL));
			exit;
		}
	} else {
		echo json_encode(array("status_code" => "400", "msg" => "Hearder section empty.", "body" => NULL));
		exit;
	}
}


//For switching account academic year wise 
function switchAcademicYear(){
	$response_headers = $this->getallheaders_new();
	if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
		$utoken = $response_headers['utoken'];
		$response = $this->checkUtoken($utoken);
		if ($response == true) {	
			if (!empty($response_headers['centerid']) ) {
			
				if(empty($response_headers['userid'])){
					echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
					exit;
				}
				
				if(empty($response_headers['academicyear'])){
					echo json_encode(array("status_code" => "400", "msg" => "Please provide academic year.", "body" => NULL));
					exit;
				}
				
				   // get all academic year data with if exist
				   $result_data = $this->parentsapimodel->getacdemicyearwisedata($response_headers['studentid']);
				echo json_encode(array("status_code" => "200", "msg" => "Information found successfully.", "body" => $result_data));
				exit;
				
			}else {
				echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}else{
		echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
		exit;
	}
}
	

	//For change password
	function changePassword(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if($response){
				if(empty($response_headers['currentpassword'])){
					echo json_encode(array("status_code" => "400", "Metadata" => array("Message" => "Please Enter Current Password." ), "Data" => NULL ));
					exit;
				}
				if(empty($response_headers['newpassword'])){
					echo json_encode(array("status_code" => "400", "Metadata" => array("Message" => "Please Enter New Password." ), "Data" => NULL ));
					exit;
				}
				if(empty($response_headers['confirmpassword'])){
					echo json_encode(array("status_code" => "400", "Metadata" => array("Message" => "Please Enter Confirm Password." ), "Data" => NULL ));
					exit;
				}
				if(empty($response_headers['userid'])){
					echo json_encode(array("status_code" => "400", "Metadata" => array("Message" => "Please Provide User id." ), "Data" => NULL ));
					exit;
				}
				
				$current_password = trim(md5($response_headers['currentpassword']));
				$new_password = trim(md5($response_headers['newpassword']));
				$confirm_password = trim(md5($response_headers['confirmpassword']));

				//check existing password
				$existing_condition = "student_id = '".$response_headers['userid']."' AND password='".$current_password."' ";
				$existing_password = $this->parentsapimodel->getdata('tbl_student_master',"student_id", $existing_condition );
				if(!empty($existing_password)){
					if($new_password === $confirm_password){
						$data = array();
						$data['password'] = $new_password;
						$condition = "student_id = '".$response_headers['userid']."'  ";
						$result = $this->parentsapimodel->updateRecord('tbl_student_master', $data,$condition);
						if($result){
							echo json_encode(array("status_code" => "200", "msg" => "New password updated successfully.", "body" => ""));
							exit;
						}else{
							echo json_encode(array("status_code" => "400", "msg" => "Problem in updating password.", "body" => ""));
							exit;
						}
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "Confirm password is mismatched.", "body" => ""));
						exit;
					}
				}else{
					echo json_encode(array("status_code" => "400", "msg" => "Incorrect Current password.", "body" => ""));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => ""));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => ""));
			exit;
		}
	}
	
	//for logout
	function userLogOut() {
		$response_headers = $this->getallheaders_new();
		if (!empty($response_headers['userid'])) {
			$userid = (!empty($response_headers['userid'])) ? $response_headers['userid'] : '';
			
			$data = array();
			$data['fcm_token'] = '';
			$data['device_id'] = '';
			$data['apptype'] = '';
			$this->parentsapimodel->updateRecord('tbl_student_master', $data, "student_id = '".$userid."' ");
			
			echo json_encode(array("status_code" => "200", "msg" => "User Logout Successfully.", "body" => NULL));
			exit;
			
		}else{
			echo json_encode(array("status_code" => "400", "msg" => "Please Provide user id.", "body" => NULL));
			exit;
		}		
	}
	
	function getAppVersions(){
		$response_headers = $this->getallheaders_new();
		if (!empty($response_headers['apptype']) ) {
			$details = $this->parentsapimodel->getdata("tbl_app_versions", "*", "app_type='".$response_headers['apptype']."' ");
			echo json_encode(array("status_code" => "200", "msg" => "Version Information.", "body" => $details));
			exit;
		}else{
			echo json_encode(array("status_code" => "400", "msg" => "App Type Not Provided.", "body" => NULL));
			exit;
		}
		
		
	}
	
	// function getAlbums(){
	// 	$response_headers = $this->getallheaders_new();
	// 	if (!empty($response_headers['userid']) ) {
			
	// 		if (!empty($response_headers['centerid']) ) {
	// 			$condition = "1=1 && center_id='".$response_headers['centerid']."' && status='Active' ";
	// 			$page = $response_headers['page'];
	// 			$default_sort_column = "album_id";
	// 			$default_sort_order = "desc";
				
	// 			$getDetails = $this->parentsapimodel->getRecords("tbl_albums", $condition, $page, $default_sort_column, $default_sort_order);
	// 			//echo "<pre>";print_r($getDetails);exit;
	// 			if(!empty($getDetails)){
	// 				for($i=0; $i < sizeof($getDetails); $i++){
	// 					//zone name
	// 					$zone_name = $this->parentsapimodel->getdata("tbl_zones", "*", "zone_id='".$getDetails[$i]->zone_id."' ");
	// 					$getDetails[$i]->zone_name = (!empty($zone_name)) ? $zone_name[0]['zone_name'] : '';
						
	// 					//center name
	// 					$center_name = $this->parentsapimodel->getdata("tbl_centers", "*", "center_id='".$getDetails[$i]->center_id."' ");
	// 					$getDetails[$i]->center_name = (!empty($center_name)) ? $center_name[0]['center_name'] : '';
						
	// 					//get last uploaded image
	// 					$image_condition = "1=1 && center_id='".$response_headers['centerid']."' && album_id='".$getDetails[$i]->album_id."' && status='Active' ";
	// 					$getImage = $this->parentsapimodel->getdata_orderby_limit1("tbl_album_images", "album_image_id, album_pic, status", $image_condition,' order by created_on desc');
	// 					if(!empty($getImage)){
	// 						$getDetails[$i]->album_pic_path = base_url().'images/teacher_album_images/'.$getImage[0]['album_pic'];
	// 						$getDetails[$i]->album_pic_thumb = base_url().'images/teacher_album_images/thumbs/'.$getImage[0]['album_pic'];
	// 					}else{
	// 						$getDetails[$i]->album_pic_path = base_url().'images/teacher_album_images/album_default.PNG';
	// 						$getDetails[$i]->album_pic_thumb = base_url().'images/teacher_album_images/album_default.PNG';
	// 					}
						
	// 				}
	// 				echo json_encode(array("status_code" => "200", "msg" => "Albums.", "body" => $getDetails));
	// 				exit;
	// 			}else if($page > 0 && empty($getDetails)){
	// 				echo json_encode(array("status_code" => "401", "msg" => "No more albums found", "body" => NULL));
	// 				exit;
	// 			}else{
	// 				echo json_encode(array("status_code" => "400", "msg" => "Album not found", "body" => NULL));
	// 				exit;
	// 			}
				
	// 		}else {
	// 			echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
	// 			exit;
	// 		}
			
	// 	}else {
	// 		echo json_encode(array("status_code" => "400", "msg" => "Teacher ID not provided", "body" => NULL));
	// 		exit;
	// 	}
	// }
	// function getAlbums(){
	// 	$response_headers = $this->getallheaders_new();
	// 	if (!empty($response_headers['userid']) ) {
			
	// 		if (!empty($response_headers['centerid']) ) {
	// 			$getBatchesCoursesCategories = $this->parentsapimodel->getdata("tbl_student_details", "category_id, course_id, batch_id","student_id='".$response_headers['userid']."' && academic_year_id='".$response_headers['academicyear']."' ");
	// 			$getGroups = $this->parentsapimodel->getdata("tbl_student_group", "group_id, batch_id"," student_id='".$response_headers['userid']."' && academic_year_id='".$response_headers['academicyear']."' ");
	// 			$group_array = array();
	// 			$group_batch_array = array();
	// 			$batch_array = array();
	// 			$course_array = array();
	// 			$category_array = array();
	// 			for($i=0; $i < sizeof($getBatchesCoursesCategories); $i++){
	// 				$batch_array[] = $getBatchesCoursesCategories[$i]['batch_id'];
	// 				$course_array[] = $getBatchesCoursesCategories[$i]['course_id'];
	// 				$category_array[] = $getBatchesCoursesCategories[$i]['category_id'];
	// 			}
	// 			$batch_implode = implode(",", $batch_array);
	// 			$course_implode = implode(",", $course_array);
	// 			$category_implode = implode(",", $category_array);
					
				
	// 			// print_r($batch_implode);
	// 			// exit();
	// 			for($i=0; $i < sizeof($getGroups); $i++){
	// 				$group_array[] = $getGroups[$i]['group_id'];
	// 				$group_batch_array[] = $getGroups[$i]['batch_id'];
	// 			}
				
	// 			$group_implode = implode(",", $group_array);
	// 			$group_batch_array_implode = implode(",", $group_batch_array);

	// 			if(empty($batch_implode)){
	// 				echo json_encode(array("status_code" => "400", "msg" => "Batch not allocated to login user.", "body" => NULL));
	// 				exit;
	// 			}

	// 			if(empty($course_implode)){
	// 				echo json_encode(array("status_code" => "400", "msg" => "Course not allocated to login user.", "body" => NULL));
	// 				exit;
	// 			}
				
	// 			if(empty($category_implode)){
	// 				echo json_encode(array("status_code" => "400", "msg" => "Category not allocated to login user.", "body" => NULL));
	// 				exit;
	// 			}

	// 			// $condition = "1=1 && teacher_id='".$response_headers['userid']."' && center_id='".$response_headers['centerid']."' && status='Active' ";
	// 			$condition = "i.status='Active' && category_id  IN (".$category_implode.") && course_id IN (".$course_implode.") && batch_id IN (".$batch_implode.") && i.center_id ='".$response_headers['centerid']."' && i.zone_id ='".$response_headers['zoneid']."'  ";
	// 			if(!empty($group_implode)){
	// 				$condition .=" || group_id  IN (".$group_implode.")";
	// 			}
	// 			$page = $response_headers['page'];
	// 			// $default_sort_column = "album_id";
	// 			// $default_sort_order = "desc";
				
	// 			$getDetails = $this->parentsapimodel->getRecords1("tbl_assign_album", $condition, $page, 'album_id');
	// 			// echo "<pre>";print_r($getDetails);exit;
	// 			if(!empty($getDetails)){
	// 				for($i=0; $i < sizeof($getDetails); $i++){
	// 					//zone name
	// 					$zone_name = $this->parentsapimodel->getdata("tbl_zones", "*", "zone_id='".$getDetails[$i]->zone_id."' ");
	// 					$getDetails[$i]->zone_name = (!empty($zone_name)) ? $zone_name[0]['zone_name'] : '';
						
	// 					//center name
	// 					$center_name = $this->parentsapimodel->getdata("tbl_centers", "*", "center_id='".$getDetails[$i]->center_id."' ");
	// 					$getDetails[$i]->center_name = (!empty($center_name)) ? $center_name[0]['center_name'] : '';

	// 					//album name
	// 					$album_name = $this->parentsapimodel->getdata("tbl_album", "*", "album_id='".$getDetails[$i]->album_id."' ");
	// 					$getDetails[$i]->album_name = (!empty($album_name)) ? $album_name[0]['album_name'] : '';
						
	// 					//get last uploaded image
	//  					//$image_condition = "1=1 && center_id='".$response_headers['centerid']."' && album_id='".$getDetails[$i]->album_id."' && status='Active' ";
	//  					//$getImage = $this->parentsapimodel->getdata_orderby_limit1("tbl_album_images", "album_image_id, album_pic, status", $image_condition,' order by created_on desc');
						
						
	// 					//$image_condition = "(i.album_id='".$getDetails[$i]->album_id."' &&  i.center_id ='".$response_headers['centerid']."' && i.zone_id ='".$response_headers['zoneid']."' ) && ( m.category_id  IN (".$category_implode.") && m.course_id IN (".$course_implode.") && m.batch_id IN (".$batch_implode.") )  ";
						
	// 					$image_condition = "(i.album_id='".$getDetails[$i]->album_id."' &&  i.center_id ='".$response_headers['centerid']."' && i.zone_id ='".$response_headers['zoneid']."' )  ";
	// 					//echo $image_condition;
						
	// 					if(!empty($category_implode) &&  !empty($group_implode)){
	// 						$image_condition .=" && (m.category_id  IN (".$category_implode.") && m.course_id IN (".$course_implode.") && m.batch_id IN (".$batch_implode.")  ||  ( m.group_id  IN (".$group_implode.") && m.batch_id IN (".$group_batch_array_implode.") ) )";
	// 					}else if(!empty($category_implode) &&  empty($group_implode)){
	// 						$image_condition .=" && (m.category_id  IN (".$category_implode.") && m.course_id IN (".$course_implode.") && m.batch_id IN (".$batch_implode.")   )";
	// 					}else if(empty($category_implode) &&  !empty($group_implode)){
	// 						$image_condition .=" && ( m.group_id  IN (".$group_implode.") && m.batch_id IN (".$group_batch_array_implode.") ) ";
	// 					}
						
						
	// 					$getImage = $this->parentsapimodel->getAlbumLastImage($image_condition);
						
	// 					//echo "<pre>";print_r($getImage);
						
	// 					if(!empty($getImage)){
	// 						$getDetails[$i]->album_pic_path = base_url().'images/teacher_album_images/'.$getImage[0]['album_pic'];
	// 						$getDetails[$i]->album_pic_thumb = base_url().'images/teacher_album_images/thumbs/'.$getImage[0]['album_pic'];
	// 						//$getDetails[$i]->album_pic_thumb = base_url().'images/teacher_album_images/'.$getImage[0]['album_pic'];
	//  					}/*else{
	// 						$getDetails[$i]->album_pic_path = base_url().'images/teacher_album_images/album_default.PNG';
	//  						$getDetails[$i]->album_pic_thumb = base_url().'images/teacher_album_images/album_default.PNG';
	//  					}*/
						
	// 				}
	// 				echo json_encode(array("status_code" => "200", "msg" => "Albums.", "body" => $getDetails));
	// 				exit;
	// 			}else if($page > 0 && empty($getDetails)){
	// 				echo json_encode(array("status_code" => "401", "msg" => "No more albums found", "body" => NULL));
	// 				exit;
	// 			}else{
	// 				echo json_encode(array("status_code" => "400", "msg" => "Album not found", "body" => NULL));
	// 				exit;
	// 			}
				
	// 		}else {
	// 			echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
	// 			exit;
	// 		}
			
	// 	}else {
	// 		echo json_encode(array("status_code" => "400", "msg" => "Teacher ID not provided", "body" => NULL));
	// 		exit;
	// 	}
	// }


	// function getAlbums(){
	// 	$response_headers = $this->getallheaders_new();
	// 	if (!empty($response_headers['userid']) ) {
			
	// 		if (!empty($response_headers['centerid']) ) {
	// 			$getBatchesCoursesCategories = $this->parentsapimodel->getdata("tbl_student_details", "category_id, course_id, batch_id","student_id='".$response_headers['userid']."' && academic_year_id='".$response_headers['academicyear']."' ");
	// 			$getGroups = $this->parentsapimodel->getdata("tbl_student_group", "group_id, batch_id"," student_id='".$response_headers['userid']."' && academic_year_id='".$response_headers['academicyear']."' ");

	// 			$group_array = array();
	// 			$group_batch_array = array();
	// 			$batch_array = array();
	// 			$course_array = array();
	// 			$category_array = array();
	// 			for($i=0; $i < sizeof($getBatchesCoursesCategories); $i++){
	// 				$batch_array[] = $getBatchesCoursesCategories[$i]['batch_id'];
	// 				$course_array[] = $getBatchesCoursesCategories[$i]['course_id'];
	// 				$category_array[] = $getBatchesCoursesCategories[$i]['category_id'];
	// 			}
	// 			$batch_implode = implode(",", $batch_array);
	// 			$course_implode = implode(",", $course_array);
	// 			$category_implode = implode(",", $category_array);

	// 			for($i=0; $i < sizeof($getGroups); $i++){
	// 				$group_array[] = $getGroups[$i]['group_id'];
	// 				$group_batch_array[] = $getGroups[$i]['batch_id'];
	// 			}
				
	// 			$group_implode = implode(",", $group_array);
	// 			$group_batch_array_implode = implode(",", $group_batch_array);

	// 			if(empty($batch_implode)){
	// 				echo json_encode(array("status_code" => "400", "msg" => "Batch not allocated to login user.", "body" => NULL));
	// 				exit;
	// 			}

	// 			if(empty($course_implode)){
	// 				echo json_encode(array("status_code" => "400", "msg" => "Course not allocated to login user.", "body" => NULL));
	// 				exit;
	// 			}
				
	// 			if(empty($category_implode)){
	// 				echo json_encode(array("status_code" => "400", "msg" => "Category not allocated to login user.", "body" => NULL));
	// 				exit;
	// 			}

	// 			$condition = "i.status='Active' && category_id  IN (".$category_implode.") && course_id IN (".$course_implode.") && batch_id IN (".$batch_implode.") && i.center_id ='".$response_headers['centerid']."' && i.zone_id ='".$response_headers['zoneid']."'  ";
	// 			if(!empty($group_implode)){
	// 				$condition .=" || group_id  IN (".$group_implode.")";
	// 			}
	// 			$page = $response_headers['page'];
	// 			// $default_sort_column = "album_id";
	// 			// $default_sort_order = "desc";
				
	// 			$getDetails = $this->parentsapimodel->getRecords1("tbl_assign_album", $condition, $page, 'album_id');

	// 			if(!empty($getDetails)){
	// 				for($i=0; $i < sizeof($getDetails); $i++){
						
	// 					$image_condition = "(i.album_id='".$getDetails[$i]->album_id."' &&  i.center_id ='".$response_headers['centerid']."' && i.zone_id ='".$response_headers['zoneid']."' )  ";
						
	// 					if(!empty($category_implode) &&  !empty($group_implode)){
	// 						$image_condition .=" && (m.category_id  IN (".$category_implode.") && m.course_id IN (".$course_implode.") && m.batch_id IN (".$batch_implode.")  ||  ( m.group_id  IN (".$group_implode.") && m.batch_id IN (".$group_batch_array_implode.") ) )";
	// 					}else if(!empty($category_implode) &&  empty($group_implode)){
	// 						$image_condition .=" && (m.category_id  IN (".$category_implode.") && m.course_id IN (".$course_implode.") && m.batch_id IN (".$batch_implode.")   )";
	// 					}else if(empty($category_implode) &&  !empty($group_implode)){
	// 						$image_condition .=" && ( m.group_id  IN (".$group_implode.") && m.batch_id IN (".$group_batch_array_implode.") ) ";
	// 					}
						
						
	// 					$getImage = $this->parentsapimodel->getAlbumLastImage($image_condition);

	// 					if(!empty($getImage)){
	// 						$imagearray[] = $getDetails[$i];
	//  					}
	//  					else{
	//  						$withoutimagearray[] = $getDetails[$i];
	//  					}
						
	// 				}
	// 				//if image dn save in different array and dn merge so latest uploaded folder display first
	// 				$latestfolderdisplay = array_merge($imagearray,$withoutimagearray);
	// 				// exit();
	// 				for($i=0; $i < sizeof($latestfolderdisplay); $i++){
	// 					//zone name
	// 					$zone_name = $this->parentsapimodel->getdata("tbl_zones", "*", "zone_id='".$latestfolderdisplay[$i]->zone_id."' ");
	// 					$latestfolderdisplay[$i]->zone_name = (!empty($zone_name)) ? $zone_name[0]['zone_name'] : '';
						
	// 					//center name
	// 					$center_name = $this->parentsapimodel->getdata("tbl_centers", "*", "center_id='".$latestfolderdisplay[$i]->center_id."' ");
	// 					$latestfolderdisplay[$i]->center_name = (!empty($center_name)) ? $center_name[0]['center_name'] : '';

	// 					//album name
	// 					$album_name = $this->parentsapimodel->getdata("tbl_album", "*", "album_id='".$latestfolderdisplay[$i]->album_id."' ");
	// 					$latestfolderdisplay[$i]->album_name = (!empty($album_name)) ? $album_name[0]['album_name'] : '';
						
	// 					$image_condition = "(i.album_id='".$latestfolderdisplay[$i]->album_id."' &&  i.center_id ='".$response_headers['centerid']."' && i.zone_id ='".$response_headers['zoneid']."' )  ";
						
	// 					if(!empty($category_implode) &&  !empty($group_implode)){
	// 						$image_condition .=" && (m.category_id  IN (".$category_implode.") && m.course_id IN (".$course_implode.") && m.batch_id IN (".$batch_implode.")  ||  ( m.group_id  IN (".$group_implode.") && m.batch_id IN (".$group_batch_array_implode.") ) )";
	// 					}else if(!empty($category_implode) &&  empty($group_implode)){
	// 						$image_condition .=" && (m.category_id  IN (".$category_implode.") && m.course_id IN (".$course_implode.") && m.batch_id IN (".$batch_implode.")   )";
	// 					}else if(empty($category_implode) &&  !empty($group_implode)){
	// 						$image_condition .=" && ( m.group_id  IN (".$group_implode.") && m.batch_id IN (".$group_batch_array_implode.") ) ";
	// 					}
						
						
	// 					$getImage = $this->parentsapimodel->getAlbumLastImage($image_condition);
	// 					if(!empty($getImage)){
	// 						$latestfolderdisplay[$i]->album_pic_path = base_url().'images/teacher_album_images/'.$getImage[0]['album_pic'];
	// 						$latestfolderdisplay[$i]->album_pic_thumb = base_url().'images/teacher_album_images/thumbs/'.$getImage[0]['album_pic'];
	//  					}
						
	// 				}
	// 				echo json_encode(array("status_code" => "200", "msg" => "Albums.", "body" => $latestfolderdisplay));
	// 				exit;
	// 			}else if($page > 0 && empty($latestfolderdisplay)){
	// 				echo json_encode(array("status_code" => "401", "msg" => "No more albums found", "body" => NULL));
	// 				exit;
	// 			}else{
	// 				echo json_encode(array("status_code" => "400", "msg" => "Album not found", "body" => NULL));
	// 				exit;
	// 			}
				
	// 		}else {
	// 			echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
	// 			exit;
	// 		}
			
	// 	}else {
	// 		echo json_encode(array("status_code" => "400", "msg" => "Teacher ID not provided", "body" => NULL));
	// 		exit;
	// 	}
	// }
	
	//8-8-19 change
	function getAlbums(){
		$response_headers = $this->getallheaders_new();
		if (!empty($response_headers['userid']) ) {
			
			if (!empty($response_headers['centerid']) ) {
				// $getBatchesCoursesCategories = $this->parentsapimodel->getdata("tbl_student_details", "category_id, course_id, batch_id","student_id='".$response_headers['userid']."' && academic_year_id='".$response_headers['academicyear']."' ");
				$getBatchesCoursesCategories = $this->parentsapimodel->getdata("tbl_promoted_student", "category_id,promoted_course_id as course_id, promoted_batch_id as batch_id","student_id='".$response_headers['userid']."' && promoted_academic_year_id='".$response_headers['academicyear']."' ");
				
				$getGroups = $this->parentsapimodel->getdata("tbl_student_group", "group_id, batch_id"," student_id='".$response_headers['userid']."' && academic_year_id='".$response_headers['academicyear']."' ");

				$group_array = array();
				$group_batch_array = array(); 
				$batch_array = array();
				$course_array = array();
				$category_array = array();
				for($i=0; $i < sizeof($getBatchesCoursesCategories); $i++){
					$batch_array[] = $getBatchesCoursesCategories[$i]['batch_id'];
					$course_array[] = $getBatchesCoursesCategories[$i]['course_id'];
					$category_array[] = $getBatchesCoursesCategories[$i]['category_id'];
				}
				$batch_implode = implode(",", $batch_array);
				$course_implode = implode(",", $course_array);
				$category_implode = implode(",", $category_array);

				for($i=0; $i < sizeof($getGroups); $i++){
					$group_array[] = $getGroups[$i]['group_id'];
					$group_batch_array[] = $getGroups[$i]['batch_id'];
				}
				
				$group_implode = implode(",", $group_array);
				$group_batch_array_implode = implode(",", $group_batch_array);

				if(empty($batch_implode)){
					echo json_encode(array("status_code" => "400", "msg" => "Batch not allocated to login user.", "body" => NULL));
					exit;
				}

				if(empty($course_implode)){
					echo json_encode(array("status_code" => "400", "msg" => "Course not allocated to login user.", "body" => NULL));
					exit;
				}
				
				if(empty($category_implode)){
					echo json_encode(array("status_code" => "400", "msg" => "Category not allocated to login user.", "body" => NULL));
					exit;
				}

				$condition = "i.status='Active' && category_id  IN (".$category_implode.") && course_id IN (".$course_implode.") && batch_id IN (".$batch_implode.") && i.center_id ='".$response_headers['centerid']."' && i.zone_id ='".$response_headers['zoneid']."' && i.academic_year_id = '".$response_headers['academicyear']."'  ";
				if(!empty($group_implode)){
					$condition .=" || group_id  IN (".$group_implode.")";
				}
				$page = $response_headers['page'];
				// $default_sort_column = "album_id";
				// $default_sort_order = "desc";
				
				// echo $condition;
				$getDetails = $this->parentsapimodel->getRecords1("tbl_assign_album", $condition, $page, 'album_id');
				// echo $this->db->last_query();
				$albumimageid = [];
				if(!empty($getDetails)){
					for($i=0; $i < sizeof($getDetails); $i++){
						
						$image_condition = "(i.album_id='".$getDetails[$i]->album_id."' &&  i.center_id ='".$response_headers['centerid']."' && i.zone_id ='".$response_headers['zoneid']."' )  ";
						
						if(!empty($category_implode) &&  !empty($group_implode)){
							$image_condition .=" && (m.category_id  IN (".$category_implode.") && m.course_id IN (".$course_implode.") && m.batch_id IN (".$batch_implode.")  ||  ( m.group_id  IN (".$group_implode.") && m.batch_id IN (".$group_batch_array_implode.") ) )";
						}else if(!empty($category_implode) &&  empty($group_implode)){
							$image_condition .=" && (m.category_id  IN (".$category_implode.") && m.course_id IN (".$course_implode.") && m.batch_id IN (".$batch_implode.")   )";
						}else if(empty($category_implode) &&  !empty($group_implode)){
							$image_condition .=" && ( m.group_id  IN (".$group_implode.") && m.batch_id IN (".$group_batch_array_implode.") ) ";
						}
						
						
						$getImage = $this->parentsapimodel->getAlbumLastImage($image_condition);

						$getAlbumImageId = $this->parentsapimodel->getAlbumImageId($image_condition);
						// print_r($getAlbumImageId);
						if(!empty($getAlbumImageId)){
							for($im =0;$im<sizeof($getAlbumImageId);$im++) {
								$albumimageid[$getAlbumImageId[$im]['album_image_id']] = (object)$getAlbumImageId[$im];
							}
						}
						if(empty($getImage)){
							$withoutimagearray[] = $getDetails[$i];
	 					}
						
					}
					arsort($albumimageid);
					//if image dn save in different array and dn merge so latest uploaded folder display first
					$latestfolderdisplay = array_merge($albumimageid,$withoutimagearray);
					// print_r($latestfolderdisplay);
					// exit();
					for($i=0; $i < sizeof($latestfolderdisplay); $i++){
						//zone name
						$zone_name = $this->parentsapimodel->getdata("tbl_zones", "*", "zone_id='".$response_headers['zoneid']."' ");
						$latestfolderdisplay[$i]->zone_name = (!empty($zone_name)) ? $zone_name[0]['zone_name'] : '';
						
						//center name
						$center_name = $this->parentsapimodel->getdata("tbl_centers", "*", "center_id='".$response_headers['centerid']."' ");
						$latestfolderdisplay[$i]->center_name = (!empty($center_name)) ? $center_name[0]['center_name'] : '';

						//album name
						$album_name = $this->parentsapimodel->getdata("tbl_album", "*", "album_id='".$latestfolderdisplay[$i]->album_id."' ");
						$latestfolderdisplay[$i]->album_name = (!empty($album_name)) ? $album_name[0]['album_name'] : '';
						
						$image_condition = "(i.album_id='".$latestfolderdisplay[$i]->album_id."' &&  i.center_id ='".$response_headers['centerid']."' && i.zone_id ='".$response_headers['zoneid']."' )  ";
						
						if(!empty($category_implode) &&  !empty($group_implode)){
							$image_condition .=" && (m.category_id  IN (".$category_implode.") && m.course_id IN (".$course_implode.") && m.batch_id IN (".$batch_implode.")  ||  ( m.group_id  IN (".$group_implode.") && m.batch_id IN (".$group_batch_array_implode.") ) )";
						}else if(!empty($category_implode) &&  empty($group_implode)){
							$image_condition .=" && (m.category_id  IN (".$category_implode.") && m.course_id IN (".$course_implode.") && m.batch_id IN (".$batch_implode.")   )";
						}else if(empty($category_implode) &&  !empty($group_implode)){
							$image_condition .=" && ( m.group_id  IN (".$group_implode.") && m.batch_id IN (".$group_batch_array_implode.") ) ";
						}
						
						
						$getImage = $this->parentsapimodel->getAlbumLastImage($image_condition);
						if(!empty($getImage)){
							$latestfolderdisplay[$i]->album_pic_path = base_url().'images/teacher_album_images/'.$getImage[0]['album_pic'];
							$latestfolderdisplay[$i]->album_pic_thumb = base_url().'images/teacher_album_images/thumbs/'.$getImage[0]['album_pic'];
	 					}
						
					}
					echo json_encode(array("status_code" => "200", "msg" => "Albums.", "body" => $latestfolderdisplay));
					exit;
				}else if($page > 0 && empty($latestfolderdisplay)){
					echo json_encode(array("status_code" => "401", "msg" => "No more albums found", "body" => NULL));
					exit;
				}else{
					echo json_encode(array("status_code" => "400", "msg" => "Album not found", "body" => NULL));
					exit;
				}
				
			}else {
				echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
				exit;
			}
			
		}else {
			echo json_encode(array("status_code" => "400", "msg" => "Teacher ID not provided", "body" => NULL));
			exit;
		}
	}

	function getAlbumsImages(){
		$response_headers = $this->getallheaders_new();
		if (!empty($response_headers['userid']) ) {
			if (!empty($response_headers['centerid']) ) {
				if (!empty($response_headers['albumid']) ) {
					// $getStudentBatch = $this->parentsapimodel->getdata("tbl_student_details", "batch_id","academic_year_id='".$response_headers['academicyear']."' && student_id='".$response_headers['userid']."' ");

					$getStudentBatch = $this->parentsapimodel->getdata("tbl_promoted_student", "promoted_batch_id as batch_id","promoted_academic_year_id='".$response_headers['academicyear']."' && student_id='".$response_headers['userid']."' ");

					// print_r($getStudentBatch);

					$getStudentGroupBatch = $this->parentsapimodel->getdata("tbl_student_group", "batch_id","academic_year_id='".$response_headers['academicyear']."' && student_id='".$response_headers['userid']."' ");

					if(!empty($getStudentBatch) && isset($getStudentBatch)){
						for($i=0;$i<sizeof($getStudentBatch);$i++){
							$batch_id[] = $getStudentBatch[$i]['batch_id'];
						}
					}
					// print_r(sizeof($getStudentBatch));
					if(!empty($getStudentGroupBatch) && isset($getStudentGroupBatch)){
						for($i=0;$i<sizeof($getStudentGroupBatch);$i++){
							$batch_id[] = $getStudentGroupBatch[$i]['batch_id'];
						}
					}
					// print_r($batch_id);

					if(!empty($batch_id)){
						$batch_implode_id = implode(',', $batch_id);
					}
					// print_r($batch_implode_id);
					// $condition = "1=1 && center_id='".$response_headers['centerid']."' && album_id='".$response_headers['albumid']."' && batch_id IN (".$batch_implode_id." ) && status='Active' ";
					$getDetails = $this->parentsapimodel->getdata_group_order_by($response_headers['centerid'],$response_headers['albumid'],$batch_id,"m.uploaded_on");
					if(!empty($getDetails)){
						$result = array();
						for($i=0; $i < sizeof($getDetails); $i++){
							$result[$i]['uploaded_on'] = date("d-m-Y", strtotime($getDetails[$i]['uploaded_on']));
							$image_condition = "1=1 && i.center_id='".$response_headers['centerid']."' && i.album_id='".$response_headers['albumid']."' && i.status='Active' && i.uploaded_on ='".$getDetails[$i]['uploaded_on']."'&& m.batch_id IN (".$batch_implode_id." ) ";
							$getImages = $this->parentsapimodel->getdata_orderby_album_image($image_condition);
							if(!empty($getImages)){
								for($m=0 ; $m < sizeof($getImages); $m++){
									$getImages[$m]['album_pic_path'] = !empty($getImages[$m]['album_pic']) ? base_url().'images/teacher_album_images/'.$getImages[$m]['album_pic'] : NULL;
									$getImages[$m]['album_pic_thumb'] = !empty($getImages[$m]['album_pic']) ? base_url().'images/teacher_album_images/thumbs/'.$getImages[$m]['album_pic'] : '';
									$getImages[$m]['album_videos_path'] = !empty($getImages[$m]['album_video']) ? base_url().'images/teacher_album_videos/'.$getImages[$m]['album_video'] : NULL;
								}
								$result[$i]['images'] = $getImages;
							}else{
								$result[$i]['images'] = NULL;
							}
						}
						echo json_encode(array("status_code" => "200", "msg" => "Album Images.", "body" => $result));
						exit;
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "Album Images not found", "body" => NULL));
						exit;
					}
					if(!empty($getDetails)){
						echo json_encode(array("status_code" => "200", "msg" => "Album Images.", "body" => $result));
						exit;
					}else if($page > 0 && empty($getDetails)){
						echo json_encode(array("status_code" => "401", "msg" => "No more images found", "body" => NULL));
						exit;
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "Album Images not found", "body" => NULL));
						exit;
					}
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Album ID not provided", "body" => NULL));
					exit;
				}	
			}else {
				echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
				exit;
			}
		}else {
			echo json_encode(array("status_code" => "400", "msg" => "Teacher ID not provided", "body" => NULL));
			exit;
		}
	}
	
	//For showing listing of newsletters
	function getNewsletters(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					if(empty($response_headers['academicyear'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide academic year.", "body" => NULL));
						exit;
					}
					$getCoursesCategories = $this->parentsapimodel->getdata("tbl_student_details", "category_id, course_id, batch_id","academic_year_id='".$response_headers['academicyear']."' && student_id='".$response_headers['userid']."' ");
					if(empty($getCoursesCategories[0]['course_id'])){
						echo json_encode(array("status_code" => "400", "msg" => "Course not allocated to login user.", "body" => NULL));
						exit;
					}
					if(empty($getCoursesCategories[0]['category_id'])){
						echo json_encode(array("status_code" => "400", "msg" => "Category not allocated to login user.", "body" => NULL));
						exit;
					}
					$category_id = $getCoursesCategories[0]['category_id'];
					$course_id = $getCoursesCategories[0]['course_id'];
					$getGroups = $this->parentsapimodel->getdata("tbl_student_group", "group_id","academic_year_id='".$response_headers['academicyear']."' && student_id='".$response_headers['userid']."' ");
					
					$group_array = array();
					for($i=0; $i < sizeof($getGroups); $i++){
						$group_array[] = $getGroups[$i]['group_id'];
					}
					$group_implode = implode(",", $group_array);
					$condition = "";
					$condition .= "   i.center_id='".$response_headers['centerid']."' &&  i.academic_year_id='".$response_headers['academicyear']."'  ";
					if(!empty($category_id) && !empty($course_id) && !empty($group_implode) ){
						$condition .= " && ( ( i.course_id='".$course_id."'  && i.category_id ='".$category_id."' ) ||  (i.group_id IN (".$group_implode." ) ))";
					}else{
						$condition .= " && ( ( i.course_id='".$course_id."'  && i.category_id ='".$category_id."' ) )";
					}
					$page = (!empty($response_headers['page'])) ? $response_headers['page'] : 0;
					$getCenterUserNewsletters = $this->parentsapimodel->getCenterUserNewsletters($condition, $page);
					if(!empty($getCenterUserNewsletters['query_result'])){
						for ($i = 0; $i < sizeof($getCenterUserNewsletters['query_result']); $i++) {
							$getCenterUserNewsletters['query_result'][$i]->newsletter_description = html_entity_decode($getCenterUserNewsletters['query_result'][$i]->newsletter_description);
							if(!empty($getCenterUserNewsletters['query_result'][$i]->cover_image)){
								$getCenterUserNewsletters['query_result'][$i]->cover_img_path = FRONT_ROOT_URL.'/images/newsletters_images/'.$getCenterUserNewsletters['query_result'][$i]->cover_image;
							}else{
								$getCenterUserNewsletters['query_result'][$i]->cover_img_path = base_url().'images/newsletter_default.png';
							}
							if(!empty($getCenterUserNewsletters['query_result'][$i]->newsletter_pdf)){
								$getCenterUserNewsletters['query_result'][$i]->pdf_path = FRONT_ROOT_URL.'/images/newsletters_images/'.$getCenterUserNewsletters['query_result'][$i]->newsletter_pdf;
							}else{
								$getCenterUserNewsletters['query_result'][$i]->pdf_path = "";
							}
							if(!empty($getCenterUserNewsletters['query_result'][$i]->cover_image2)){
								$getCenterUserNewsletters['query_result'][$i]->cover_image2_path = FRONT_ROOT_URL.'/images/newsletters_images/'.$getCenterUserNewsletters['query_result'][$i]->cover_image2;
							}else{
								$getCenterUserNewsletters['query_result'][$i]->cover_image2_path = base_url().'images/newsletter_default.png';
							}
							if(!empty($getCenterUserNewsletters['query_result'][$i]->newsletter_pdf2)){
								$getCenterUserNewsletters['query_result'][$i]->newsletter_pdf2_path = FRONT_ROOT_URL.'/images/newsletters_images/'.$getCenterUserNewsletters['query_result'][$i]->newsletter_pdf2;
							}else{
								$getCenterUserNewsletters['query_result'][$i]->newsletter_pdf2_path = "";
							}
							
							$getCenterUserNewsletters['query_result'][$i]->created_on = date("d-m-Y", strtotime($getCenterUserNewsletters['query_result'][$i]->created_on));
							$getCenterUserNewsletters['query_result'][$i]->updated_on = date("d-m-Y", strtotime($getCenterUserNewsletters['query_result'][$i]->updated_on));
							$getCenterUserNewsletters['query_result'][$i]->newsletter_date = date("d M Y", strtotime($getCenterUserNewsletters['query_result'][$i]->newsletter_datetime));
							$getCenterUserNewsletters['query_result'][$i]->newsletter_time = date("g:i A", strtotime($getCenterUserNewsletters['query_result'][$i]->newsletter_datetime));
							$getCenterUserNewsletters['query_result'][$i]->webview_path = base_url().'home/index/'.$getCenterUserNewsletters['query_result'][$i]->newsletter_id;
							$getCenterUserNewsletters['query_result'][$i]->newsletter_datetime = date("d-m-Y H:i", strtotime($getCenterUserNewsletters['query_result'][$i]->newsletter_datetime));
							
						}
					}
					if (!empty($getCenterUserNewsletters['query_result'])) {
						echo json_encode(array("status_code" => "200", "msg" => "Newsletter found successfully.", "body" => $getCenterUserNewsletters));
						exit;
					} else if($page > 0 && empty($getCenterUserNewsletters['query_result'])){
							echo json_encode(array("status_code" => "401", "msg" => "No more newsletter found", "body" => NULL));
							exit;
					}else {
						echo json_encode(array("status_code" => "400", "msg" => "No newsletter found.", "body" => NULL));
						exit;
					}
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}
	
	
	//For getting student profile
	function getStudentProfile(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['academicyear'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide academic year.", "body" => NULL));
						exit;
					}
					
					//get student information
					$condition = "ps.student_id='".$response_headers['userid']."' &&  ps.promoted_academic_year_id ='".$response_headers['academicyear']."' ";
					$getStudentProfile = $this->parentsapimodel->getStudentProfile($condition);
					
					if(!empty($getStudentProfile)){
						$getStudentProfile[0]['dob'] = (!empty($getStudentProfile[0]['dob'])) ? date("d M Y", strtotime($getStudentProfile[0]['dob'])) : '';
						$getStudentProfile[0]['profile_pic_path'] = (!empty($getStudentProfile[0]['profile_pic'])) ? base_url().'images/student_profile_pics/'.$getStudentProfile[0]['profile_pic'] : '';
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "Student profile not found", "body" => NULL));
						exit;
					}
					
					//echo "<pre>";print_r($getStudentProfile);exit;
					
					echo json_encode(array("status_code" => "200", "msg" => "Information found successfully.", "body" => $getStudentProfile));
					exit;
					
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}
	
	//for uploading profile pic
	function profilePicUpload(){
		$response_headers = $this->getallheaders_new();
		if (!empty($response_headers['userid']) ) {
			$profile_pic ="";
			if(isset($_FILES) && isset($_FILES["profile_pic"]["name"]) && !empty($_FILES["profile_pic"]["name"])){
				$config['upload_path'] = DOC_ROOT."/images/student_profile_pics/";
				$config['max_size']       = '10000';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['file_name']     = md5(uniqid("100_ID", true));

				$this->load->library('upload', $config);
				//print_r($_FILES);

				if (!$this->upload->do_upload("profile_pic"))
				{
					$image_error = array('error' => $this->upload->display_errors());
					//print_r($image_error);
					echo json_encode(array("status_code" => "400", "msg" => strip_tags($image_error['error']), "body" => NULL));
					exit;
				}
				else
				{
					$image_data = array('upload_data' => $this->upload->data());
					$profile_pic = $image_data['upload_data']['file_name'];
				}
				
				$getImage = $this->parentsapimodel->getdata("tbl_student_master", "profile_pic", "student_id='" . $response_headers['userid'] . "' ");
				
				@unlink(DOC_ROOT . "/images/student_profile_pics/".$getImage[0]['profile_pic']);
				
				$image_clear_data = array();
				$image_clear_data['profile_pic'] = "";
				$this->parentsapimodel->updateRecord("tbl_student_master", $image_clear_data, "student_id='".$response_headers['userid']."' ");
				
				$data = array();
				$data['profile_pic'] = $profile_pic;
				
				$result = $this->parentsapimodel->updateRecord("tbl_student_master", $data, "student_id='".$response_headers['userid']."' ");
				
				if(!empty($result)){
					echo json_encode(array("status_code" => "200", "msg" => "Image changed successfully", "body" => base_url().'images/student_profile_pics/'.$profile_pic));
					exit;
				}
				
			}else{
				echo json_encode(array("status_code" => "400", "msg" => "Please upload Profile pic", "body" => NULL));
				exit;
			}
			
		}else {
			echo json_encode(array("status_code" => "400", "msg" => "User ID not provided", "body" => NULL));
			exit;
		}
	}
	
	//For giving feedback
	function parentFeedback(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					if(empty($_POST['feedbackmsg'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide feedback message.", "body" => NULL));
						exit;
					}
					
					
					//get student information
					$condition = "student_id='".$response_headers['userid']."' ";
					$getStudentProfile = $this->parentsapimodel->getdata("tbl_student_master", "father_name, father_email_id, father_mobile_contact_no" ,$condition);
					//echo "<pre>";print_r($getStudentProfile);exit;
					
					$father_name = (!empty($getStudentProfile[0]['father_name'])) ? $getStudentProfile[0]['father_name'] : '';
					$father_email_id = (!empty($getStudentProfile[0]['father_email_id'])) ? $getStudentProfile[0]['father_email_id'] : '';
					$father_mobile_contact_no = (!empty($getStudentProfile[0]['father_mobile_contact_no'])) ? $getStudentProfile[0]['father_mobile_contact_no'] : '';
					
					$data = array();
							
					$data['zone_id'] = (!empty($response_headers['zoneid'])) ? $response_headers['zoneid'] : '';
					$data['center_id'] = (!empty($response_headers['centerid'])) ? $response_headers['centerid'] : '';
					$data['student_id'] = (!empty($response_headers['userid'])) ? $response_headers['userid'] : '';
					
					$data['feedback_name'] = (!empty($father_name)) ? $father_name : '';
					$data['feedback_email'] = (!empty($father_email_id)) ? $father_email_id : '';
					$data['feedback_contact'] = (!empty($father_mobile_contact_no)) ? $father_mobile_contact_no : '';
					
					$data['feedback_msg'] = (!empty($_POST['feedbackmsg'])) ? $_POST['feedbackmsg'] : '';
					
					
					$data['created_on'] = date("Y-m-d H:i:s");
					$data['created_by'] = $response_headers['userid'];
					$data['updated_on'] = date("Y-m-d H:i:s");
					$data['updated_by'] = $response_headers['userid'];
					
					$result = $this->parentsapimodel->insertData('tbl_feedbacks', $data, 1);
					
					if(!empty($result)){
						echo json_encode(array("status_code" => "200", "msg" => "Feedback saved successfully.", "body" => $result));
						exit;
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "Feedback not saved successfully.", "body" => NULL));
						exit;
					}
					
					
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
		
	}
	
	
	//For taking appointment
	function requestAppointment(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['appointmentdate'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide appointment date.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['appointmentmsg'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide appointment message.", "body" => NULL));
						exit;
					}
					
					//get student information
					$condition = "student_id='".$response_headers['userid']."' && appointment_date='".date("Y-m-d", strtotime($response_headers['appointmentdate']))."'  ";
					$checkAppointment = $this->parentsapimodel->getdata("tbl_appointments", "appointment_id" ,$condition);
					
					if(!empty($checkAppointment)){
						echo json_encode(array("status_code" => "400", "msg" => "You have already taken appointment on selected date.", "body" => NULL));
						exit;
					}
					$condition = "student_id='".$response_headers['centerid']."' ";
					$get_studentName = $this->parentsapimodel->getdata("tbl_student_master
					", "student_first_name,student_last_name" ,$condition);
					//print_r($get_studentName[0]['student_first_name']);exit;

					$condition = "zone_id='".$response_headers['zoneid']."' && center_id='".$response_headers['centerid']."'  ";
					$getTeacher = $this->parentsapimodel->getdata("tbl_admin_users", "user_id,fcm_token" ,$condition);
					//print_r($getTeacher);exit;
					if(!empty($getTeacher)){
						for($s=0; $s < sizeof($getTeacher); $s++){
							$title = '';
							$notification = '';
							$email_content=  '';
							$fcm_token = '';
							$subject='';
							$from_email='';
							$to_email='';
							$cc_email = '';
							$notification_type = array();
							
							$title = "New Appointment";
										
							$notification = " ".$get_studentName[0]['student_first_name']." ".$get_studentName[0]['student_last_name']."'s"." Parent booked an appointment: ".$response_headers['appointmentdate'].".";
							//print_r($notification);exit;
							$notification_type = array('type'=>'service_request');
							
							$notification_data = array();
							$notification_data['teacher_id'] = $getTeacher[$s]['user_id'];
							$notification_data['notification_type'] = 'Appointment';
							$notification_data['notification_title'] = $title;
							$notification_data['notification_contents'] = $notification;
							
							$notification_data['created_on'] = date("Y-m-d H:i:s");
							$notification_data['created_by'] = (!empty($response_headers['userid'])) ? $response_headers['userid'] : '' ;
							$notification_data['updated_on'] = date("Y-m-d H:i:s");
							$notification_data['updated_by'] = (!empty($response_headers['userid'])) ? $response_headers['userid'] : '' ;
							 $this->parentsapimodel->insertData('tbl_teacher_notifications', $notification_data, 1);
							
							 if(!empty($getTeacher[$s]['fcm_token'])){
								 //echo "h1";
								 $fcm_token = $getTeacher[$s]['fcm_token'];
								 if(!empty($fcm_token)){
									 sendNotification($title,$notification,$email_content,$fcm_token,$subject,$from_email,$to_email,$cc_email,$notification_type);
									//echo "<pre>";print_r($res);exit;
								 }	
							 }
						}
					}
					
					$data = array();
							
					$data['zone_id'] = (!empty($response_headers['zoneid'])) ? $response_headers['zoneid'] : '';
					$data['center_id'] = (!empty($response_headers['centerid'])) ? $response_headers['centerid'] : '';
					$data['student_id'] = (!empty($response_headers['userid'])) ? $response_headers['userid'] : '';
					
					$data['appointment_date'] = (!empty($response_headers['appointmentdate'])) ? date("Y-m-d", strtotime($response_headers['appointmentdate'])) : '';
					
					$data['appointment_message'] = (!empty($_POST['appointmentmsg'])) ? $_POST['appointmentmsg'] : '';
					
					
					$data['created_on'] = date("Y-m-d H:i:s");
					$data['created_by'] = $response_headers['userid'];
					$data['updated_on'] = date("Y-m-d H:i:s");
					$data['updated_by'] = $response_headers['userid'];
					
					$result = $this->parentsapimodel->insertData('tbl_appointments', $data, 1);
					
					if(!empty($result)){
						echo json_encode(array("status_code" => "200", "msg" => "Thank you for your request. Please wait for approval.", "body" => $result));
						exit;
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "Appointment not booked successfully.", "body" => NULL));
						exit;
					}
					
					
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
		
	}
	
	//For getting user categories
	function getUserCategories(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['academicyear'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide academic year.", "body" => NULL));
						exit;
					}
					
					$getDetails = $this->parentsapimodel->getUserCategories("student_id='".$response_headers['userid']."' ");
					//echo "<pre>";print_r($getDetails);exit;
					
					if (!empty($getDetails)) {
						echo json_encode(array("status_code" => "200", "msg" => "Record found successfully.", "body" => $getDetails));
						exit;
					} else {
						echo json_encode(array("status_code" => "400", "msg" => "No Record found.", "body" => NULL));
						exit;
					}
					
				
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}
	
	//For getting user courses
    // 	function getUserCourses(){
// 		$response_headers = $this->getallheaders_new();
// 		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
// 			$utoken = $response_headers['utoken'];
// 			$response = $this->checkUtoken($utoken);
// 			if ($response == true) {	
				
// 					if(empty($response_headers['userid'])){
// 						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
// 						exit;
// 					}
					
// 					if(empty($response_headers['categoryid'])){
// 						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
// 						exit;
// 					}
					
// 					if(empty($response_headers['academicyear'])){
// 						echo json_encode(array("status_code" => "400", "msg" => "Please provide academic year.", "body" => NULL));
// 						exit;
// 					}
					
// 					$getDetails = $this->parentsapimodel->getUserCourses("i.academic_year_id='".$response_headers['academicyear']."' && i.student_id='".$response_headers['userid']."' && i.category_id='".$response_headers['categoryid']."' ");
// 					//echo "<pre>";print_r($getDetails);exit;
					
// 					if (!empty($getDetails)) {
// 						echo json_encode(array("status_code" => "200", "msg" => "Record found successfully.", "body" => $getDetails));
// 						exit;
// 					} else {
// 						echo json_encode(array("status_code" => "400", "msg" => "No Record found.", "body" => NULL));
// 						exit;
// 					}
					
				
// 			}else{
// 				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
// 				exit;
// 			}
// 		}else{
// 			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
// 			exit;
// 		}
// 	}
    
    //   start code by shiv on 27-04-2020  changes for academic yearwise code
    	function getUserCourses(){
    		$response_headers = $this->getallheaders_new();
    		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
    			$utoken = $response_headers['utoken'];
    			$response = $this->checkUtoken($utoken);
    			if ($response == true) {	
    				
    					if(empty($response_headers['userid'])){
    						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
    						exit;
    					}
    					
    					if(empty($response_headers['categoryid'])){
    						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
    						exit;
    					}
    					
    					if(empty($response_headers['academicyear'])){
    						echo json_encode(array("status_code" => "400", "msg" => "Please provide academic year.", "body" => NULL));
    						exit;
    					}
    					
    					$getDetails = $this->parentsapimodel->getUserCourses("ps.promoted_academic_year_id='".$response_headers['academicyear']."' && ps.student_id='".$response_headers['userid']."' && i.category_id='".$response_headers['categoryid']."' ");
    					//echo "<pre>";print_r($getDetails);exit;
    					
    					if (!empty($getDetails)) {
    						echo json_encode(array("status_code" => "200", "msg" => "Record found successfully.", "body" => $getDetails));
    						exit;
    					} else {
    						echo json_encode(array("status_code" => "400", "msg" => "No Record found.", "body" => NULL));
    						exit;
    					}
    					
    				
    			}else{
    				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
    				exit;
    			}
    		}else{
    			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
    			exit;
    		}
    	}
    	//   end code by shiv on 27-04-2020  changes for academic yearwise code
		
		
		//For getting user batches
        // 	function getUserBatches(){
    // 		$response_headers = $this->getallheaders_new();
    // 		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
    // 			$utoken = $response_headers['utoken'];
    // 			$response = $this->checkUtoken($utoken);
    // 			if ($response == true) {	
    				
    // 					if(empty($response_headers['userid'])){
    // 						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
    // 						exit;
    // 					}
    					
    // 					if(empty($response_headers['academicyear'])){
    // 						echo json_encode(array("status_code" => "400", "msg" => "Please provide academic year.", "body" => NULL));
    // 						exit;
    // 					}
    					
    // 					if(empty($response_headers['categoryid'])){
    // 						echo json_encode(array("status_code" => "400", "msg" => "Please provide category id.", "body" => NULL));
    // 						exit;
    // 					}
    					
    // 					if(empty($response_headers['courseid'])){
    // 						echo json_encode(array("status_code" => "400", "msg" => "Please provide course id.", "body" => NULL));
    // 						exit;
    // 					}
    					
    // 					$getDetails = $this->parentsapimodel->getUserBatches("i.academic_year_id='".$response_headers['academicyear']."' && i.student_id='".$response_headers['userid']."' && i.category_id='".$response_headers['categoryid']."' && i.course_id='".$response_headers['courseid']."' ");
    // 					//echo "<pre>";print_r($getDetails);exit;
    					
    // 					if (!empty($getDetails)) {
    // 						echo json_encode(array("status_code" => "200", "msg" => "Record found successfully.", "body" => $getDetails));
    // 						exit;
    // 					} else {
    // 						echo json_encode(array("status_code" => "400", "msg" => "No Record found.", "body" => NULL));
    // 						exit;
    // 					}
    					
    				
    // 			}else{
    // 				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
    // 				exit;
    // 			}
    // 		}else{
    // 			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
    // 			exit;
    // 		}
    // 	}
        //   start code by shiv on 27-04-2020  changes for academic yearwise code
    	function getUserBatches(){
    		$response_headers = $this->getallheaders_new();
    		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
    			$utoken = $response_headers['utoken'];
    			$response = $this->checkUtoken($utoken);
    			if ($response == true) {	
    				
    					if(empty($response_headers['userid'])){
    						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
    						exit;
    					}
    					
    					if(empty($response_headers['academicyear'])){
    						echo json_encode(array("status_code" => "400", "msg" => "Please provide academic year.", "body" => NULL));
    						exit;
    					}
    					
    					if(empty($response_headers['categoryid'])){
    						echo json_encode(array("status_code" => "400", "msg" => "Please provide category id.", "body" => NULL));
    						exit;
    					}
    					
    					if(empty($response_headers['courseid'])){
    						echo json_encode(array("status_code" => "400", "msg" => "Please provide course id.", "body" => NULL));
    						exit;
    					}
    					
    					$getDetails = $this->parentsapimodel->getUserBatches("ps.promoted_academic_year_id='".$response_headers['academicyear']."' && ps.student_id='".$response_headers['userid']."' && ps.category_id='".$response_headers['categoryid']."' && ps.promoted_course_id='".$response_headers['courseid']."' ");
    					//echo "<pre>";print_r($getDetails);exit;
    					
    					if (!empty($getDetails)) {
    						echo json_encode(array("status_code" => "200", "msg" => "Record found successfully.", "body" => $getDetails));
    						exit;
    					} else {
    						echo json_encode(array("status_code" => "400", "msg" => "No Record found.", "body" => NULL));
    						exit;
    					}
    					
    				
    			}else{
    				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
    				exit;
    			}
    		}else{
    			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
    			exit;
    		}
    	}
    	//   end code by shiv on 27-04-2020  changes for academic yearwise code
    	
	

	//For getting user categories
	function getUserGroups(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['academicyear'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide academic year.", "body" => NULL));
						exit;
					}
					
					//$getDetails = $this->parentsapimodel->getUserGroups("student_id='".$response_headers['userid']."' ");
					$getDetails = $this->parentsapimodel->getUserGroups($response_headers['userid']);
					//echo "<pre>";print_r($getDetails);exit;
					
					if (!empty($getDetails)) {
						echo json_encode(array("status_code" => "200", "msg" => "Record found successfully.", "body" => $getDetails));
						exit;
					} else {
						echo json_encode(array("status_code" => "400", "msg" => "No Record found.", "body" => NULL));
						exit;
					}
					
				
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}
	
	//For view attendance
	function viewAttendance(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['feesselectiontype'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide selection type.", "body" => NULL));
						exit;
					}
					
					if(!empty($response_headers['feesselectiontype']) && $response_headers['feesselectiontype'] == 'Class'){
						if(empty($response_headers['categoryid'])){
							echo json_encode(array("status_code" => "400", "msg" => "Please provide category id.", "body" => NULL));
							exit;
						}
						
						if(empty($response_headers['courseid'])){
							echo json_encode(array("status_code" => "400", "msg" => "Please provide course id.", "body" => NULL));
							exit;
						}
						
						if(empty($response_headers['batchid'])){
							echo json_encode(array("status_code" => "400", "msg" => "Please provide batch id.", "body" => NULL));
							exit;
						}
						
					}else{
						if(empty($response_headers['groupid'])){
							echo json_encode(array("status_code" => "400", "msg" => "Please provide group id.", "body" => NULL));
							exit;
						}
					}
					
					if(empty($response_headers['attendancemonth'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide attendance month.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['attendanceyear'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide attendance year.", "body" => NULL));
						exit;
					}
					
					
					$datelist = array();
					$month = (!empty($response_headers['attendancemonth'])) ? $response_headers['attendancemonth'] : 1;
					$year = (!empty($response_headers['attendanceyear'])) ? $response_headers['attendanceyear'] : date('Y');
					
					for($d=1; $d <= 31; $d++){
						$time = mktime(12, 0, 0, $month, $d, $year);          
						if (date('m', $time) == $month ){
							$datelist[]=date('d-m-Y', $time);
						}
					}
					
					
					
					//echo "hre <pre>";print_r($datelist);exit;
					$result = array();
					if(!empty($datelist)){
						for($i=0; $i < sizeof($datelist); $i++){
							$dval = date("d_m_Y", strtotime($datelist[$i]));
							$result[$i]['date'] = $dval;
							//$result[$i]['date_data'] = "data";
							//get attendance data
							$data_condition = "";
							if(!empty($response_headers['feesselectiontype']) && $response_headers['feesselectiontype'] == 'Class'){
								$data_condition = "center_id='".$response_headers['centerid']."' && academic_year_id='".$response_headers['academicyear']."' && selection_type='".$response_headers['feesselectiontype']."' && category_id='".$response_headers['categoryid']."' && course_id='".$response_headers['courseid']."' && batch_id='".$response_headers['batchid']."' && student_id='".$response_headers['userid']."' && attendance_date='".date("Y-m-d", strtotime($datelist[$i]))."' ";
							}else{
								$data_condition = "center_id='".$response_headers['centerid']."' && academic_year_id='".$response_headers['academicyear']."' && selection_type='".$response_headers['feesselectiontype']."' && group_id='".$response_headers['groupid']."' && student_id='".$response_headers['userid']."' && attendance_date='".date("Y-m-d", strtotime($datelist[$i]))."' ";
							}
							$getDateData = $this->parentsapimodel->getdata('tbl_mark_attendance', "mark_attendance_id", $data_condition);
							$result[$i]['date_data'] = (!empty($getDateData)) ? true : false;
						}
					}
					
					//echo "hre <pre>";print_r($result);exit;
					echo json_encode(array("status_code" => "200", "msg" => "View attendance data.", "body" => $result));
					exit;
					
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
		
	}
	
	//For getting attendance Information
	function checkAttendance(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['academicyear'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide academic year.", "body" => NULL));
						exit;
					}
					
					$getCoursesCategories = $this->parentsapimodel->getdata("tbl_student_details", "category_id, course_id, batch_id","academic_year_id='".$response_headers['academicyear']."' && student_id='".$response_headers['userid']."' ");
					//echo "<pre>";print_r($getCoursesCategories);exit;
					
					if(empty($getCoursesCategories[0]['course_id'])){
						echo json_encode(array("status_code" => "400", "msg" => "Course not allocated to login user.", "body" => NULL));
						exit;
					}
					
					if(empty($getCoursesCategories[0]['category_id'])){
						echo json_encode(array("status_code" => "400", "msg" => "Category not allocated to login user.", "body" => NULL));
						exit;
					}
					
					$category_id = $getCoursesCategories[0]['category_id'];
					$course_id = $getCoursesCategories[0]['course_id'];
					$batch_id = $getCoursesCategories[0]['batch_id'];
					
					
					$getGroups = $this->parentsapimodel->getdata("tbl_student_group", "group_id","academic_year_id='".$response_headers['academicyear']."' && student_id='".$response_headers['userid']."' ");
					//echo "<pre>";print_r($getGroups);exit;
					
					$group_array = array();
					for($i=0; $i < sizeof($getGroups); $i++){
						$group_array[] = $getGroups[$i]['group_id'];
					}
					
					$group_implode = implode(",", $group_array);
					
					//exit;
					
					$condition = "";
					$current_date = date("Y-m-d");
					//echo $current_time;exit;
					$condition .= " student_id='".$response_headers['userid']."' && center_id='".$response_headers['centerid']."' && status='Active' && attendance_date='".$current_date."'  ";
					if(!empty($category_id) && !empty($course_id) && !empty($group_implode) ){
						$condition .= " && ( ( course_id='".$course_id."'  && category_id ='".$category_id."' && batch_id='".$batch_id."' ) ||  (group_id IN (".$group_implode." ) ))";
					}else{
						$condition .= " && ( ( course_id='".$course_id."'  && category_id ='".$category_id."' && batch_id='".$batch_id."' ) )";
					}
					
					//echo $condition;exit;
					
					
					$checkAttendance = $this->parentsapimodel->getdata("tbl_mark_attendance","*", $condition);
					$query = $this->db->last_query();
					//echo "<pre>";print_r($checkAttendance);exit;
					if(!empty($checkAttendance)){
						echo json_encode(array("status_code" => "200", "msg" => "Attendance marked for today.", "body" => Null,"query"=>$query));
						exit;
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "Today's attendance not marked.", "body" => NULL));
						exit;
					}
					
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
		
	}
	
	//For getting camera list
	function getCamaraList(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['academicyear'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide academic year.", "body" => NULL));
						exit;
					}
					
					// $getCoursesCategories = $this->parentsapimodel->getdata("tbl_student_details", "category_id, course_id, batch_id","academic_year_id='".$response_headers['academicyear']."' && student_id='".$response_headers['userid']."' ");
					$getCoursesCategories = $this->parentsapimodel->getdata("tbl_promoted_student", "category_id, promoted_course_id as course_id, promoted_batch_id as batch_id ","promoted_academic_year_id='".$response_headers['academicyear']."' && student_id='".$response_headers['userid']."' ");
					
					//echo "<pre>";print_r($getCoursesCategories);exit;
					
					if(empty($getCoursesCategories[0]['course_id'])){
						echo json_encode(array("status_code" => "400", "msg" => "Course not allocated to login user.", "body" => NULL));
						exit;
					}
					
					if(empty($getCoursesCategories[0]['category_id'])){
						echo json_encode(array("status_code" => "400", "msg" => "Category not allocated to login user.", "body" => NULL));
						exit;
					}
					
					$category_id = $getCoursesCategories[0]['category_id'];
					$course_id = $getCoursesCategories[0]['course_id'];
					$batch_id = $getCoursesCategories[0]['batch_id'];
					
					
					$getGroups = $this->parentsapimodel->getdata("tbl_student_group", "group_id, batch_id","academic_year_id='".$response_headers['academicyear']."' && student_id='".$response_headers['userid']."' ");
					//echo "<pre>";print_r($getGroups);exit;
					
					$group_array = array();
					$group_batch_array = array();
					for($i=0; $i < sizeof($getGroups); $i++){
						$group_array[] = $getGroups[$i]['group_id'];
						$group_batch_array[] = $getGroups[$i]['batch_id'];
					}
					
					$group_implode = implode(",", $group_array);
					$group_batch_implode = implode(",", $group_batch_array);
					
					//exit;
					
					$condition = "";
					$current_time = date("H:i");
					//echo $current_time;exit;
					//$condition .= " i.center_id='".$response_headers['centerid']."' && c.status='Active' && ( c.start_time < '".$current_time."' and '".$current_time."' < c.end_time )  ";
					$condition .= " i.center_id='".$response_headers['centerid']."' && (c.status='Active' && i.status='Active')  ";
					if(!empty($category_id) && !empty($course_id) && !empty($group_implode) && !empty($group_batch_implode) ){
						$condition .= " && ( ( i.course_id='".$course_id."'  && i.category_id ='".$category_id."' && i.batch_id='".$batch_id."' ) ||  (i.group_id IN (".$group_implode." )  && i.batch_id IN (".$group_batch_implode." )))";
					}else{
						$condition .= " && ( ( i.course_id='".$course_id."'  && i.category_id ='".$category_id."' && i.batch_id='".$batch_id."' ) )";
					}
					
					//echo $condition;exit;
					
					
					$getCamaraList = $this->parentsapimodel->getCamaraList($condition);
					//echo "<pre>";print_r($getCamaraList);exit;
					if(!empty($getCamaraList)){
						echo json_encode(array("status_code" => "200", "msg" => "Camera list", "body" => $getCamaraList));
						exit;
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "Camera list not found.", "body" => NULL));
						exit;
					}
					
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
		
	}
	

	//for getting camera settings center wise
	function centerCameraSettings(){
		$response_headers = $this->getallheaders_new();
		if (!empty($response_headers['centerid']) ) {
			
			$condition = "status='Active' && center_id='".$response_headers['centerid']."' ";
			$page = (!empty($response_headers['page'])) ? $response_headers['page'] : 0;
			$default_sort_column = "camera_setting_id";
			$default_sort_order = "desc";
			
			$getDetails = $this->parentsapimodel->getRecords("tbl_camera_settings", $condition, $page, $default_sort_column, $default_sort_order);
			
			//echo "<pre>";print_r($getDetails);exit;
			
			if (!empty($getDetails)) {
				echo json_encode(array("status_code" => "200", "msg" => "Camera Settings.", "body" => $getDetails));
				exit;
			} else if($page > 0 && empty($getDetails)){
					echo json_encode(array("status_code" => "401", "msg" => "No more settings found", "body" => NULL));
					exit;
			}else {
				echo json_encode(array("status_code" => "400", "msg" => "No settings found.", "body" => NULL));
				exit;
			}
			
		}else {
			echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
			exit;
		}
	}
	
	//cumulative setting
	function cumulativeSettings(){
		$response_headers = $this->getallheaders_new();
		if (!empty($response_headers['centerid']) ) {
			if (!empty($response_headers['categoryid']) && $response_headers['categoryid'] !='0' ) {
				if (!empty($response_headers['courseid']) && $response_headers['courseid'] !='0' ) {
					$current_date = date("Y-m-d");
					$today_date = date("Y-m-d");
					//delete settings
					$this->db->query("DELETE FROM tbl_check_cumulative_setting WHERE center_id='".$response_headers['centerid']."' && category_id='".$response_headers['categoryid']."' && course_id='".$response_headers['courseid']."' && student_id='".$response_headers['studentid']."' && setting_taken_time !='".$current_date."' ");
					
					$condition = "status='Active' && center_id='".$response_headers['centerid']."' && category_id='".$response_headers['categoryid']."' && course_id='".$response_headers['courseid']."' ";
					$getDetails = $this->parentsapimodel->getdata("tbl_admin_cumulative_settings", '*',$condition);
					
					if(!empty($response_headers['deviceid'])){
						$check_setting_taken_condition = "center_id='".$response_headers['centerid']."' && category_id='".$response_headers['categoryid']."' && course_id='".$response_headers['courseid']."' && student_id='".$response_headers['studentid']."' && setting_taken_time ='".$current_date."' && device_id='".$response_headers['deviceid']."' ";
					}else{
						$check_setting_taken_condition = "center_id='".$response_headers['centerid']."' && category_id='".$response_headers['categoryid']."' && course_id='".$response_headers['courseid']."' && student_id='".$response_headers['studentid']."' && setting_taken_time ='".$current_date."' ";
					}
					
					$check_setting_taken = $this->parentsapimodel->getdata("tbl_check_cumulative_setting", '*',$check_setting_taken_condition);
					//echo "<pre>";print_r($check_setting_taken);exit;
					if(!empty($check_setting_taken)){
						$result =  array();
						$result['max_cumulative_time'] = (!empty($getDetails[0]['center_cumulative_time'])) ? $getDetails[0]['center_cumulative_time'] : $getDetails[0]['admin_cumulative_time'];
						
						//check today entry
						$today_remaining_time_condition = "center_id='".$response_headers['centerid']."' && student_id='".$response_headers['studentid']."' && DATE(rec_date_time)='".$today_date."' ";
						$today_remaining_time = $this->parentsapimodel->getdata_orderby_limit1("tbl_student_cctv_remaining_time", '*',$today_remaining_time_condition, 'order by remaining_time_id desc');
						if(empty($today_remaining_time)){
							$result['student_remaining_time'] = (!empty($getDetails[0]['center_cumulative_time'])) ? ($getDetails[0]['center_cumulative_time'] * 60) : ($getDetails[0]['admin_cumulative_time'] * 60);
							
							$result['student_remaining_time'] = (int)$result['student_remaining_time'];
						}else{
						
							$remaining_time_condition = "center_id='".$response_headers['centerid']."' && student_id='".$response_headers['studentid']."' ";
							$remaining_time = $this->parentsapimodel->getdata_orderby_limit1("tbl_student_cctv_remaining_time", '*',$remaining_time_condition, 'order by remaining_time_id desc');
						
							$result['student_remaining_time'] = (!empty($remaining_time[0]['remaining_time'])) ? $remaining_time[0]['remaining_time'] : 0;
							$result['student_remaining_time'] = (int)$result['student_remaining_time'];
						}
						
						$result['is_setting_taken'] = true;
						
						
						echo json_encode(array("status_code" => "200", "msg" => "Requested setting already taken.", "body" => $result));
						exit;
					}else{
						$data = array();
						$data['device_id'] = (!empty($response_headers['deviceid'])) ? $response_headers['deviceid'] : '';
						$data['center_id'] = (!empty($response_headers['centerid'])) ? $response_headers['centerid'] : '';
						$data['category_id'] = (!empty($response_headers['categoryid'])) ? $response_headers['categoryid'] : '';
						$data['course_id'] = (!empty($response_headers['courseid'])) ? $response_headers['courseid'] : '';
						$data['student_id'] = (!empty($response_headers['studentid'])) ? $response_headers['studentid'] : '';
						$data['setting_taken_time'] = $current_date;
						
						$insertSettings = $this->parentsapimodel->insertData('tbl_check_cumulative_setting', $data, 1);
					}
					
					
					//echo "<pre>";print_r($getDetails);exit;
					if(!empty($getDetails)){
						$result =  array();
						$result['max_cumulative_time'] = (!empty($getDetails[0]['center_cumulative_time'])) ? $getDetails[0]['center_cumulative_time'] : $getDetails[0]['admin_cumulative_time'];
						
						//check today entry
						$today_remaining_time_condition = "center_id='".$response_headers['centerid']."' && student_id='".$response_headers['studentid']."' && DATE(rec_date_time)='".$today_date."' ";
						$today_remaining_time = $this->parentsapimodel->getdata_orderby_limit1("tbl_student_cctv_remaining_time", '*',$today_remaining_time_condition, 'order by remaining_time_id desc');
						if(empty($today_remaining_time)){
							$result['student_remaining_time'] = (!empty($getDetails[0]['center_cumulative_time'])) ? ($getDetails[0]['center_cumulative_time'] * 60) : ($getDetails[0]['admin_cumulative_time'] * 60);
							$result['student_remaining_time'] = (int)$result['student_remaining_time'];
						}else{
							$remaining_time_condition = "center_id='".$response_headers['centerid']."' && student_id='".$response_headers['studentid']."' ";
							$remaining_time = $this->parentsapimodel->getdata_orderby_limit1("tbl_student_cctv_remaining_time", '*',$remaining_time_condition, 'order by remaining_time_id desc');
						
							$result['student_remaining_time'] = (!empty($remaining_time[0]['remaining_time'])) ? $remaining_time[0]['remaining_time'] : 0;
							$result['student_remaining_time'] = (int)$result['student_remaining_time'];
						}
						
						$result['is_setting_taken'] = false;
						
						
						echo json_encode(array("status_code" => "200", "msg" => "Setting found successfully", "body" => $result));
						exit;
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "Requested setting not found.", "body" => NULL));
						exit;
					}
				}else{
					echo json_encode(array("status_code" => "400", "msg" => "Course id not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "400", "msg" => "Course family id not provided", "body" => NULL));
				exit;
			}
		}else {
			echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
			exit;
		}
	}
	
	//for getting camera connection
	function getCameraConnection(){
		$response_headers = $this->getallheaders_new();
		//print_r($response_headers);exit;
		if (!empty($response_headers['centerid']) ) {
			if (!empty($response_headers['studentid']) ) {
				if (!empty($response_headers['cameraid']) ) {
					
					$getCameraDetails = $this->parentsapimodel->getdata("tbl_camera", "start_time, end_time", "camera_id='".$response_headers['cameraid']."' ");
					
					/*$now = new Datetime("now");
					$begintime = new DateTime($getCameraDetails[0]['start_time']);
					$endtime = new DateTime('17:00');

					if($now >= $begintime && $now <= $endtime){
					    // between times
					    echo "yay";
					} else {
					    // not between times
					    echo "nay";
					}*/
					
					$current_time = date("H:i");
					
					
					if(!empty($getCameraDetails) && ($current_time > $getCameraDetails[0]['start_time'] && $current_time < $getCameraDetails[0]['end_time']) ){
						
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "You are assigned to access this camera from ".date("h:i a", strtotime($getCameraDetails[0]['start_time']))." to ".date("h:i a", strtotime($getCameraDetails[0]['end_time'])).". Thank you!", "body" => NULL));
						exit;
					}
					
					$current_datetime = date("Y-m-d H:i:s");
					$current_date = date("Y-m-d");
					
					//insert into request logs 
					$req_data = array();
					$req_data['center_id'] = $response_headers['centerid'];
					$req_data['student_id'] = $response_headers['studentid'];
					$req_data['camera_id'] = $response_headers['cameraid'];
					$req_data['request_datetime'] = $current_datetime;
					$this->parentsapimodel->insertData('tbl_cctv_users_request_logs', $req_data, 1);
					
					//Get Camera Settings
					$cam_setting_condition = "center_id='".$response_headers['centerid']."' && status='Active' ";
					$getCameraSettings = $this->parentsapimodel->getdata_orderby("tbl_camera_settings", "*", $cam_setting_condition, 'order by camera_setting_id desc ');
					//echo "<pre>";print_r($getCameraSettings);exit;
					if(empty($getCameraSettings)){
						echo json_encode(array("status_code" => "400", "msg" => "Camera Setting Not Available.", "body" => NULL));
						exit;
					}
					
					$max_connections = $getCameraSettings[0]['max_connections'];
					$max_viewing_timing = $getCameraSettings[0]['max_viewing_timing'];
					$cooling_period = $getCameraSettings[0]['cooling_period'];
					
					//checking cooling period pass or not?
					$usercooling_condition = "center_id='".$response_headers['centerid']."' && student_id='".$response_headers['studentid']."'  ";
					$getUserCoolingDetails = $this->parentsapimodel->getdata("tbl_cctv_users_cooling_priods", "*", $usercooling_condition);
					//echo "<pre>";print_r($getUserCoolingDetails);exit;
					if(!empty($getUserCoolingDetails)){
						//Cooling details found
						$previous_connected_datetime = 0;
						$time_difference = 0;
						$previous_connected_datetime = strtotime($getUserCoolingDetails[0]['prev_datetime']);
						$current_datetime = date("Y-m-d H:i:s");
						$current_datetime = strtotime($current_datetime);
						//echo "previous_connected_datetime: ".$current_datetime;exit;
						$time_difference = round(abs($current_datetime - $previous_connected_datetime) / 60,2);
						//echo $time_difference;exit;
						if($cooling_period >= $time_difference ){
							//error msg
							$connection_remaining_time = 0;
							$connection_remaining_time = ($cooling_period - $time_difference);
							$connection_remaining_time = round($connection_remaining_time)." Minutes."; 
							if($connection_remaining_time == 0){
								$connection_remaining_time = "less than 1 Minutes."; 
							}
							
							echo json_encode(array("status_code" => "400", "msg" => "Thank you. Your next view to Camera feed will be after ".$connection_remaining_time." ", "body" => NULL));
							exit;
							
						}else{
							//echo "here...";
							//cooling period  is over
							$present_connection_condition = "center_id='".$response_headers['centerid']."' && camera_id='".$response_headers['cameraid']."' ";
							$presentConnections = $this->parentsapimodel->getdata("tbl_cctv_active_users", "*", $present_connection_condition);
							if(count($presentConnections) >= $max_connections){
								//echo "if";exit;
								//checking every user time over???
								for($ct=0; $ct < sizeof($presentConnections); $ct++){
									$previous_connected_datetime = 0;
									$time_difference = 0;
									$previous_connected_datetime = strtotime($presentConnections[$ct]['connected_datetime']);
									$current_datetime = date("Y-m-d H:i:s");
									$current_datetime = strtotime($current_datetime);
									$time_difference = round(abs($current_datetime - $previous_connected_datetime) / 60,2);
									//echo "max_viewing_timing: ".$max_viewing_timing;
									//echo "time_difference: ".$time_difference;
									if($max_viewing_timing <= $time_difference ){
										$del_condition = "student_id='".$presentConnections[$ct]['student_id']."' ";
										$this->parentsapimodel->delrecord_condition('tbl_cctv_active_users', $del_condition);
									}
								}
								
								//now check connection available?
								$recheckpresent_connection_condition = "center_id='".$response_headers['centerid']."' && camera_id='".$response_headers['cameraid']."' ";
								$recheckpresentConnections = $this->parentsapimodel->getdata("tbl_cctv_active_users", "*", $recheckpresent_connection_condition);
								if(count($recheckpresentConnections) >= $max_connections){
									//connection not available
									echo json_encode(array("status_code" => "400", "msg" => "Connection not available.", "body" => NULL));
									exit;
								}else{
									//connection available add user
									$data = array();
									$data['center_id'] = $response_headers['centerid'];
									$data['student_id'] = $response_headers['studentid'];
									$data['camera_id'] = $response_headers['cameraid'];
									$data['connected_datetime'] = date("Y-m-d H:i:s");
									
									$result = $this->parentsapimodel->insertData('tbl_cctv_active_users', $data, 1);
									$response = array();
									$response['connection_available'] = true;
									$response['record_id'] = $result;
									
									$response['remainingtime'] = 0;
									$rm_time_condition = "student_id='".$response_headers['studentid']."' && rec_date='".$current_date."' ";
									$rm_time = $this->parentsapimodel->getdata("tbl_student_remaining_time", "remaining_time", $rm_time_condition);
									if(!empty($rm_time)){
										$response['remainingtime'] = (!empty($rm_time[0]['remaining_time'])) ? $rm_time[0]['remaining_time'] : 0;
									}
									
									//entery into cooling tbl
									/*$coolingdel_condition = "student_id='".$response_headers['studentid']."' ";
									$this->parentsapimodel->delrecord_condition('tbl_cctv_users_cooling_priods', $coolingdel_condition);
									$cooling_data = array();
									$cooling_data['center_id'] = $response_headers['centerid'];
									$cooling_data['student_id'] = $response_headers['studentid'];
									$cooling_data['prev_datetime'] = date("Y-m-d H:i:s");
									
									$this->parentsapimodel->insertData('tbl_cctv_users_cooling_priods', $cooling_data, 1);
									*/
									if(!empty($result)){
										echo json_encode(array("status_code" => "200", "msg" => "Connection Added.", "body" => $response));
										exit;
									}else{
										echo json_encode(array("status_code" => "400", "msg" => "Problem in insert.", "body" => NULL));
										exit;
									}
								}
								
							}else{
								//connection available add user
								
								$data = array();
								$data['center_id'] = $response_headers['centerid'];
								$data['student_id'] = $response_headers['studentid'];
								$data['camera_id'] = $response_headers['cameraid'];
								$data['connected_datetime'] = date("Y-m-d H:i:s");
								
								$result = $this->parentsapimodel->insertData('tbl_cctv_active_users', $data, 1);
								$response = array();
								$response['connection_available'] = true;
								$response['record_id'] = $result;
								
								$response['remainingtime'] = 0;
								$rm_time_condition = "student_id='".$response_headers['studentid']."'  && rec_date='".$current_date."' ";
								$rm_time = $this->parentsapimodel->getdata("tbl_student_remaining_time", "remaining_time", $rm_time_condition);
								if(!empty($rm_time)){
									$response['remainingtime'] = (!empty($rm_time[0]['remaining_time'])) ? $rm_time[0]['remaining_time'] : 0;
								}
								
								//entery into cooling tbl
								/*$coolingdel_condition = "student_id='".$response_headers['studentid']."' ";
								$this->parentsapimodel->delrecord_condition('tbl_cctv_users_cooling_priods', $coolingdel_condition);
								$cooling_data = array();
								$cooling_data['center_id'] = $response_headers['centerid'];
								$cooling_data['student_id'] = $response_headers['studentid'];
								$cooling_data['prev_datetime'] = date("Y-m-d H:i:s");
								
								$this->parentsapimodel->insertData('tbl_cctv_users_cooling_priods', $cooling_data, 1);
								*/
								if(!empty($result)){
									echo json_encode(array("status_code" => "200", "msg" => "Connection Added.", "body" => $response));
									exit;
								}else{
									echo json_encode(array("status_code" => "400", "msg" => "Problem in insert.", "body" => NULL));
									exit;
								}
							}
						}
						
					}else{
						//Cooling details not found
						//check number of connection
						$present_connection_condition = "center_id='".$response_headers['centerid']."' && camera_id='".$response_headers['cameraid']."' ";
						$presentConnections = $this->parentsapimodel->getdata("tbl_cctv_active_users", "*", $present_connection_condition);
						//echo count($presentConnections);exit;
						//check every user time completed or not??
						if(!empty($presentConnections)){
							//check no. of connection with max connection allowed
							if(count($presentConnections) >= $max_connections){
								//check every user time over or not.
								//echo "<pre>";print_r($presentConnections);exit;
								for($ct=0; $ct < sizeof($presentConnections); $ct++){
									//echo $presentConnections[$ct]['connected_datetime'];
									$previous_connected_datetime = 0;
									$time_difference = 0;
									$previous_connected_datetime = strtotime($presentConnections[$ct]['connected_datetime']);
									$current_datetime = date("Y-m-d H:i:s");
									$current_datetime = strtotime($current_datetime);
									$time_difference = round(abs($current_datetime - $previous_connected_datetime) / 60,2);
									//echo "max_viewing_timing: ".$max_viewing_timing;
									//echo "time_difference: ".$time_difference;
									//exit;
									if($max_viewing_timing <= $time_difference ){
										$del_condition = "student_id='".$presentConnections[$ct]['student_id']."' ";
										$this->parentsapimodel->delrecord_condition('tbl_cctv_active_users', $del_condition);
									}
								}
								//exit;
								
								//now check connection available?
								$recheckpresent_connection_condition = "center_id='".$response_headers['centerid']."' && camera_id='".$response_headers['cameraid']."' ";
								$recheckpresentConnections = $this->parentsapimodel->getdata("tbl_cctv_active_users", "*", $recheckpresent_connection_condition);
								if(count($recheckpresentConnections) >= $max_connections){
									//connection not available
									echo json_encode(array("status_code" => "400", "msg" => "Connection not available1.", "body" => NULL));
									exit;
								}else{
									//connection available add user
									$data = array();
									$data['center_id'] = $response_headers['centerid'];
									$data['student_id'] = $response_headers['studentid'];
									$data['camera_id'] = $response_headers['cameraid'];
									$data['connected_datetime'] = date("Y-m-d H:i:s");
									
									$result = $this->parentsapimodel->insertData('tbl_cctv_active_users', $data, 1);
									$response = array();
									$response['connection_available'] = true;
									$response['record_id'] = $result;
									
									$response['remainingtime'] = 0;
									$rm_time_condition = "student_id='".$response_headers['studentid']."'  && rec_date='".$current_date."'  ";
									$rm_time = $this->parentsapimodel->getdata("tbl_student_remaining_time", "remaining_time", $rm_time_condition);
									if(!empty($rm_time)){
										$response['remainingtime'] = (!empty($rm_time[0]['remaining_time'])) ? $rm_time[0]['remaining_time'] : 0;
									}
									
									//entery into cooling tbl
									/*$coolingdel_condition = "student_id='".$response_headers['studentid']."' ";
									$this->parentsapimodel->delrecord_condition('tbl_cctv_users_cooling_priods', $coolingdel_condition);
									$cooling_data = array();
									$cooling_data['center_id'] = $response_headers['centerid'];
									$cooling_data['student_id'] = $response_headers['studentid'];
									$cooling_data['prev_datetime'] = date("Y-m-d H:i:s");
									
									$this->parentsapimodel->insertData('tbl_cctv_users_cooling_priods', $cooling_data, 1);
									*/
									if(!empty($result)){
										echo json_encode(array("status_code" => "200", "msg" => "Connection Added.", "body" => $response));
										exit;
									}else{
										echo json_encode(array("status_code" => "400", "msg" => "Problem in insert.", "body" => NULL));
										exit;
									}
								}
								
							}else{
								//connection available add user
								
								$data = array();
								$data['center_id'] = $response_headers['centerid'];
								$data['student_id'] = $response_headers['studentid'];
								$data['camera_id'] = $response_headers['cameraid'];
								$data['connected_datetime'] = date("Y-m-d H:i:s");
								
								$result = $this->parentsapimodel->insertData('tbl_cctv_active_users', $data, 1);
								$response = array();
								$response['connection_available'] = true;
								$response['record_id'] = $result;
								
								$response['remainingtime'] = 0;
								$rm_time_condition = "student_id='".$response_headers['studentid']."'  && rec_date='".$current_date."'  ";
								$rm_time = $this->parentsapimodel->getdata("tbl_student_remaining_time", "remaining_time", $rm_time_condition);
								if(!empty($rm_time)){
									$response['remainingtime'] = (!empty($rm_time[0]['remaining_time'])) ? $rm_time[0]['remaining_time'] : 0;
								}
								
								//entery into cooling tbl
								/*$coolingdel_condition = "student_id='".$response_headers['studentid']."' ";
								$this->parentsapimodel->delrecord_condition('tbl_cctv_users_cooling_priods', $coolingdel_condition);
								$cooling_data = array();
								$cooling_data['center_id'] = $response_headers['centerid'];
								$cooling_data['student_id'] = $response_headers['studentid'];
								$cooling_data['prev_datetime'] = date("Y-m-d H:i:s");
								
								$this->parentsapimodel->insertData('tbl_cctv_users_cooling_priods', $cooling_data, 1);
								*/
								if(!empty($result)){
									echo json_encode(array("status_code" => "200", "msg" => "Connection Added.", "body" => $response));
									exit;
								}else{
									echo json_encode(array("status_code" => "400", "msg" => "Problem in insert.", "body" => NULL));
									exit;
								}
								
							}
						}else{
							//add user directly
							$data = array();
							$data['center_id'] = $response_headers['centerid'];
							$data['student_id'] = $response_headers['studentid'];
							$data['camera_id'] = $response_headers['cameraid'];
							$data['connected_datetime'] = date("Y-m-d H:i:s");
							
							$result = $this->parentsapimodel->insertData('tbl_cctv_active_users', $data, 1);
							$response = array();
							$response['connection_available'] = true;
							$response['record_id'] = $result;
							
							$response['remainingtime'] = 0;
							$rm_time_condition = "student_id='".$response_headers['studentid']."'  && rec_date='".$current_date."'  ";
							$rm_time = $this->parentsapimodel->getdata("tbl_student_remaining_time", "remaining_time", $rm_time_condition);
							if(!empty($rm_time)){
								$response['remainingtime'] = (!empty($rm_time[0]['remaining_time'])) ? $rm_time[0]['remaining_time'] : 0;
							}
							
							//entery into cooling tbl
							/*$coolingdel_condition = "student_id='".$response_headers['studentid']."' ";
							$this->parentsapimodel->delrecord_condition('tbl_cctv_users_cooling_priods', $coolingdel_condition);
							$cooling_data = array();
							$cooling_data['center_id'] = $response_headers['centerid'];
							$cooling_data['student_id'] = $response_headers['studentid'];
							$cooling_data['prev_datetime'] = date("Y-m-d H:i:s");
							
							$this->parentsapimodel->insertData('tbl_cctv_users_cooling_priods', $cooling_data, 1);
							*/
							if(!empty($result)){
								echo json_encode(array("status_code" => "200", "msg" => "Connection Added.", "body" => $response));
								exit;
							}else{
								echo json_encode(array("status_code" => "400", "msg" => "Problem in insert.", "body" => NULL));
								exit;
							}									
							
						}
						
					}
					//exit;
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Camera URL not provided", "body" => NULL));
					exit;
				}
			}else {
				echo json_encode(array("status_code" => "400", "msg" => "Student ID not provided", "body" => NULL));
				exit;
			}
		}else {
			echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
			exit;
		}
	}
	
	function deleteCameraRequest(){
		$response_headers = $this->getallheaders_new();
		//print_r($response_headers);exit;
		if (!empty($response_headers['centerid']) ) {
			if (!empty($response_headers['studentid']) ) {				
				$result = $this->parentsapimodel->delrecord('tbl_cctv_active_users', 'student_id',$response_headers['studentid']);
				if($result){
					$this->parentsapimodel->delrecord('tbl_student_remaining_time', 'student_id',$response_headers['studentid']);
					$data = array();
					$data['center_id'] = $response_headers['centerid'];
					$data['student_id'] = $response_headers['studentid'];
					$data['remaining_time'] = $response_headers['remainingtime'];
					$data['rec_date'] = date("Y-m-d");
					$result = $this->parentsapimodel->insertData('tbl_student_remaining_time', $data, 1);
					
					$this->parentsapimodel->delrecord('tbl_cctv_users_cooling_priods', 'student_id',$response_headers['studentid']);
					
					if($response_headers['remainingtime'] == 0){
						$cooling_data = array();
						$cooling_data['center_id'] = $response_headers['centerid'];
						$cooling_data['student_id'] = $response_headers['studentid'];
						$cooling_data['prev_datetime'] = date("Y-m-d H:i:s");
						
						$this->parentsapimodel->insertData('tbl_cctv_users_cooling_priods', $cooling_data, 1);
					}
					
					echo json_encode(array("status_code" => "200", "msg" => "User removed.", "body" => NULL));
					exit;
				}else{
					echo json_encode(array("status_code" => "400", "msg" => "Delete Failed", "body" => NULL));
					exit;
				}
			}else {
				echo json_encode(array("status_code" => "400", "msg" => "Student ID not provided", "body" => NULL));
				exit;
			}
		}else {
			echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
			exit;
		}
	}
	
	function insertCoolingPeriod(){
		$response_headers = $this->getallheaders_new();
		//print_r($response_headers);exit;
		if (!empty($response_headers['centerid']) ) {
			if (!empty($response_headers['studentid']) ) {
				$coolingdel_condition = "student_id='".$response_headers['studentid']."' ";
				$this->parentsapimodel->delrecord_condition('tbl_cctv_users_cooling_priods', $coolingdel_condition);
				$cooling_data = array();
				$cooling_data['center_id'] = $response_headers['centerid'];
				$cooling_data['student_id'] = $response_headers['studentid'];
				$cooling_data['prev_datetime'] = date("Y-m-d H:i:s");
				
				$result = $this->parentsapimodel->insertData('tbl_cctv_users_cooling_priods', $cooling_data, 1);
				
				if($result){
					echo json_encode(array("status_code" => "200", "msg" => "Cooling period information saved.", "body" => NULL));
					exit;
				}else{
					echo json_encode(array("status_code" => "400", "msg" => "Information not saved", "body" => NULL));
					exit;
				}
			}else {
				echo json_encode(array("status_code" => "400", "msg" => "Student ID not provided", "body" => NULL));
				exit;
			}
		}else {
			echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
			exit;
		}
	}
	
	function insertRemainingTime(){
		$response_headers = $this->getallheaders_new();
		//print_r($response_headers);exit;
		if (!empty($response_headers['centerid']) ) {
			if (!empty($response_headers['studentid']) ) {
				$del_condition = "student_id='".$response_headers['studentid']."' ";
				$this->parentsapimodel->delrecord_condition('tbl_student_cctv_remaining_time', $del_condition);
				$cooling_data = array();
				$cooling_data['center_id'] = $response_headers['centerid'];
				$cooling_data['student_id'] = $response_headers['studentid'];
				$cooling_data['remaining_time'] = $response_headers['remainingtime'];
				$cooling_data['rec_date_time'] = date("Y-m-d H:i:s");
				
				$result = $this->parentsapimodel->insertData('tbl_student_cctv_remaining_time', $cooling_data, 1);
				
				if($result){
					echo json_encode(array("status_code" => "200", "msg" => "Information saved.", "body" => NULL));
					exit;
				}else{
					echo json_encode(array("status_code" => "400", "msg" => "Information not saved", "body" => NULL));
					exit;
				}
			}else {
				echo json_encode(array("status_code" => "400", "msg" => "Student ID not provided", "body" => NULL));
				exit;
			}
		}else {
			echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
			exit;
		}
	}
	
	
	
	//For getting list of academic years
	function getAcademicYears() {
		$response_headers = $this->getallheaders_new();
		$result = array();
		$result = $this->parentsapimodel->getdata("tbl_academic_year_master", "academic_year_master_id, academic_year_master_name", "status='Active' ");
		if (empty($result)) {
			$result = array();
		}
		
		echo json_encode(array("status_code" => "200", "msg" => " fetch successfully.", "body" => $result));
		exit;
	}
	
	//get Notifications
	function getNotifications(){
		$response_headers = $this->getallheaders_new();
		if (!empty($response_headers['userid']) ) {
			
			if (!empty($response_headers['centerid']) ) {
				$condition = "student_id='".$response_headers['userid']."'  ";
				$page = $response_headers['page'];
				$default_sort_column = "student_notification_id";
				$default_sort_order = "desc";
				
				if(!empty($_POST['search_key']) && isset($_POST['search_key'])){
					$condition .=" AND notification_title like '%".$_POST['search_key']."%' ";
				}
				$getDetails = $this->parentsapimodel->getRecords("tbl_student_notifications", $condition, $page, $default_sort_column, $default_sort_order,"student_notification_id");
				//echo "<pre>";print_r($getDetails);exit;
				if(!empty($getDetails)){
					for($i=0; $i < sizeof($getDetails); $i++){
						$getDetails[$i]->created_on = date("d-m-Y H:i", strtotime($getDetails[$i]->created_on));
						$getDetails[$i]->updated_on = date("d-m-Y H:i", strtotime($getDetails[$i]->updated_on));
					}
					echo json_encode(array("status_code" => "200", "msg" => "Notifications.", "body" => $getDetails));
					exit;
				}else if($page > 0 && empty($getDetails)){
					echo json_encode(array("status_code" => "401", "msg" => "No more Notifications found", "body" => NULL));
					exit;
				}else{
					echo json_encode(array("status_code" => "400", "msg" => "Notifications not found", "body" => NULL));
					exit;
				}
				
			}else {
				echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
				exit;
			}
			
		}else {
			echo json_encode(array("status_code" => "400", "msg" => "Teacher ID not provided", "body" => NULL));
			exit;
		}
	}
	
	//get center contact details
	function contactUs(){
		$response_headers = $this->getallheaders_new();
		if (!empty($response_headers['userid']) ) {
			
			if (!empty($response_headers['centerid']) ) {
				$condition = "student_id='".$response_headers['userid']."'  ";
								
				$getDetails = $this->parentsapimodel->getdata("tbl_centers", "center_id, center_name, center_code, center_address, center_spoc, center_contact_no, center_email_id", "center_id='".$response_headers['centerid']."' ");
				//echo "<pre>";print_r($getDetails);exit;
				if(!empty($getDetails)){
					echo json_encode(array("status_code" => "200", "msg" => "Center information.", "body" => $getDetails));
					exit;
				}else{
					echo json_encode(array("status_code" => "400", "msg" => "Center information not found", "body" => NULL));
					exit;
				}
				
			}else {
				echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
				exit;
			}
			
		}else {
			echo json_encode(array("status_code" => "400", "msg" => "Teacher ID not provided", "body" => NULL));
			exit;
		}
	}
	
	//For getting Communication Types
	function getCommunicationTypes() {
		$response_headers = $this->getallheaders_new();
				
		$result = array();
		$result = $this->parentsapimodel->getdata("tbl_communication_categories", "communication_category_id, communication_categoy_name", "status='Active' ");
		if (empty($result)) {
			$result = array();
		}
		
		echo json_encode(array("status_code" => "200", "msg" => " fetch successfully.", "body" => $result));
		exit;
	}
	
	function studentNotices(){
		$response_headers = $this->getallheaders_new();
		if (!empty($response_headers['centerid']) ) {
			if (!empty($response_headers['userid']) ) {
			
				$std_condtion = "student_id='".$response_headers['userid']."' ";
				$getNotices = $this->parentsapimodel->getdata('tbl_notice_students','student_notice_id' , $std_condtion);
				//echo "<pre>";
				//print_r($getNotices);
				//exit;
				if(!empty($getNotices)){
					$notice_ids_array = array();
					for($i=0; $i < sizeof($getNotices); $i++){
						$notice_ids_array[] = $getNotices[$i]['student_notice_id'];
					}
				}else{
					echo json_encode(array("status_code" => "400", "msg" => "No notices found.", "body" => NULL));
					exit;
				}
				
				//echo "<pre>";print_r($notice_ids_array);exit;
				$implode_notice_ids = "";
				$implode_notice_ids = implode(",",$notice_ids_array);
				$implode_notice_ids = "(".$implode_notice_ids.")";
				$condition = "status='Active' && center_id='".$response_headers['centerid']."' && student_notice_id in ".$implode_notice_ids." && communication_category_id='".$response_headers['categoryid']."' ";
				
				$page = (!empty($response_headers['page'])) ? $response_headers['page'] : 0;
				$default_sort_column = "student_notice_id";
				$default_sort_order = "desc";
				
				if(!empty($_POST['search_key']) && isset($_POST['search_key'])){
					$condition .=" AND noticetitle like '%".$_POST['search_key']."%' ";
				}
				$getDetails = $this->parentsapimodel->getRecords("tbl_student_notices", $condition, $page, $default_sort_column, $default_sort_order);
				//echo "<pre>";print_r($getDetails);exit;
				
				
				
				if (!empty($getDetails)) {
					for($i=0; $i < sizeof($getDetails); $i++){
						$getDetails[$i]->notice_file_path = base_url().'images/communication_files/'.$getDetails[$i]->notice_file;
						$getDetails[$i]->created_on = date("d-m-Y H:i", strtotime($getDetails[$i]->created_on));
						$getDetails[$i]->updated_on = date("d-m-Y H:i", strtotime($getDetails[$i]->updated_on));
					}
					echo json_encode(array("status_code" => "200", "msg" => "Student Notices.", "body" => $getDetails));
					exit;
				} else if($page > 0 && empty($getDetails)){
						echo json_encode(array("status_code" => "401", "msg" => "No more notices found", "body" => NULL));
						exit;
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "No notices found.", "body" => NULL));
					exit;
				}
				
			}else {
				echo json_encode(array("status_code" => "400", "msg" => "Student ID not provided", "body" => NULL));
				exit;
			}
		}else {
			echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
			exit;
		}
	}
	
	function getStudentRoutine(){
		$response_headers = $this->getallheaders_new();
		// print_r($response_headers);
		// exit();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['routinedate'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide routine date.", "body" => NULL));
						exit;
					}

					if(!empty($response_headers['routinedate'])){
						$condition  = " r.routine_date = '".date("Y-m-d", strtotime($response_headers['routinedate']))."' ";
					}
					
					//get student routine information
					$condition .= "&& r.student_id='".$response_headers['userid']."' ";
					$getStudentRoutine = $this->parentsapimodel->getStudentRoutine($condition);
					
					if(empty($getStudentRoutine)){
						echo json_encode(array("status_code" => "400", "msg" => "Student routine not found", "body" => NULL));
						exit;
					}else{
						for($i=0; $i < sizeof($getStudentRoutine); $i++){
							//'Activity','Food','Incident','Mood','Notes','Sleep'
							if($getStudentRoutine[$i]['routine_type'] == 'Activity'){
								$getStudentRoutine[$i]['routine_icon'] = FRONT_URL. "/images/ic_activity.png";
							}else if($getStudentRoutine[$i]['routine_type'] == 'Food'){
								$getStudentRoutine[$i]['routine_icon'] = FRONT_URL. "/images/ic_food.png";
							}else if($getStudentRoutine[$i]['routine_type'] == 'Incident'){
								$getStudentRoutine[$i]['routine_icon'] = FRONT_URL. "/images/ic_incident.png";
							}else if($getStudentRoutine[$i]['routine_type'] == 'Mood'){
								$getStudentRoutine[$i]['routine_icon'] = FRONT_URL. "/images/ic_mood.png";
							}else if($getStudentRoutine[$i]['routine_type'] == 'Notes'){
								$getStudentRoutine[$i]['routine_icon'] = FRONT_URL. "/images/ic_notes.png";
							}else if($getStudentRoutine[$i]['routine_type'] == 'Sleep'){
								$getStudentRoutine[$i]['routine_icon'] = FRONT_URL. "/images/ic_sleep.png";
							}
							
							$getStudentRoutine[$i]['routine_time'] = date("h:i a", strtotime($getStudentRoutine[$i]['created_on']));
						}
						echo json_encode(array("status_code" => "200", "msg" => "Student routine found successfully.", "body" => $getStudentRoutine));
					}
					
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
		
	}
	
	function getInstallmentDetails(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if(empty($response_headers['studentid'])){
					echo json_encode(array("status_code" => "400", "msg" => "Student id not provided.", "body" => NULL));
					exit;
				}
				if(empty($response_headers['zoneid'])){
					echo json_encode(array("status_code" => "400", "msg" => "Zone id not provided.", "body" => NULL));
					exit;
				}
				if(empty($response_headers['centerid'])){
					echo json_encode(array("status_code" => "400", "msg" => "Center id not provided.", "body" => NULL));
					exit;
				}
				if(empty($response_headers['academicyear'])){
					echo json_encode(array("status_code" => "400", "msg" => "Academic year not provided.", "body" => NULL));
					exit;
				}

				//get installment details
				$installment_details = $this->parentsapimodel->getInstallmentDetails($response_headers['academicyear'],$response_headers['studentid'],$response_headers['zoneid'],$response_headers['centerid']);
				if(!empty($installment_details)){
					echo json_encode(array("status_code" => "200", "msg" => "Installment data found.", "body" => $installment_details));
					exit;
				}else{
					echo json_encode(array("status_code" => "400", "msg" => "No Installments found.", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}

	function initiateTransaction(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {
			    
		
				if(empty($response_headers['studentid'])){
					echo json_encode(array("status_code" => "400", "msg" => "Student id not provided.", "body" => NULL));
					exit;
				}
				if(empty($response_headers['zoneid'])){
					echo json_encode(array("status_code" => "400", "msg" => "Zone id not provided.", "body" => NULL));
					exit;
				}
				if(empty($response_headers['centerid'])){
					echo json_encode(array("status_code" => "400", "msg" => "Center id not provided.", "body" => NULL));
					exit;
				}
				if(empty($response_headers['academicyear'])){
					echo json_encode(array("status_code" => "400", "msg" => "Academic year not provided.", "body" => NULL));
					exit;
				}
				if(empty($response_headers['courseid'])){
					echo json_encode(array("status_code" => "400", "msg" => "Course not provided.", "body" => NULL));
					exit;
				}
				if(empty($response_headers['batchid'])){
					echo json_encode(array("status_code" => "400", "msg" => "Batch not provided.", "body" => NULL));
					exit;
				}

				$CenterCredential = $this->parentsapimodel->getCenterCredential($response_headers['centerid']);
				if($CenterCredential){
					$mid = $CenterCredential[0]['production_mid'];
					$merchant_key = $CenterCredential[0]['production_merchant_key'];
					$wap_name = $CenterCredential[0]['production_wap_name'];
					$industry_type_id = $CenterCredential[0]['industry_type_id'];
					//print_r($CenterCredential);exit;

				}else{
					echo json_encode(array("status_code" => "400", "msg" => "Center credential not found.", "body" => NULL));
					exit;
				}
			
				$installment_paytmpayment = $this->parentsapimodel->getInstallmentpaytmpayment($_POST['admission_fees_instalment_id']);
				$installment_id =implode(',', $_POST['admission_fees_instalment_id']);
				
				$total_sum=0;
				foreach ($installment_paytmpayment as $row){
				$total_sum+=$row['instalment_remaining_amount'];
				}
				$student_details = $this->parentsapimodel->getStudentDetail($response_headers['studentid']);
				$contact_no =(!empty($student_details[0]['father_mobile_contact_no']))?$student_details[0]['father_mobile_contact_no']:$student_details[0]['mother_mobile_contact_no'];

				$email_id =(!empty($student_details[0]['father_email_id']))?$student_details[0]['father_email_id']:$student_details[0]['mother_email_id'];

							
				$paytm_payment = array();
				$paytm_payment['center_id'] = $response_headers['centerid'];
				$paytm_payment['student_id'] = $response_headers['studentid'];
				$paytm_payment['zone_id'] = $response_headers['zoneid'];
				$paytm_payment['academic_year_id'] = $response_headers['academicyear'];
				$paytm_payment['course_id'] = $response_headers['courseid'];
				$paytm_payment['batch_id'] = $response_headers['batchid'];
				$paytm_payment['amount'] = $total_sum;
				$paytm_payment['installment_id'] = $installment_id;
				$paytm_payment['payment_date'] = date("Y-m-d H:i:s");
				$paytm_payment['checksum_value'] = "";
				$paytm_payment['status'] = 'pending';
				$paytm_payment['mode'] = 'paytm';
				
				$result = $this->parentsapimodel->insertData('tbl_paytmpayment', $paytm_payment, 1);
				//print_r($result);exit;
				if($result){
				header("Pragma: no-cache");
				header("Cache-Control: no-cache");
				header("Expires: 0");
				// following files need to be included
				// $this->load->view('paytm_app/lib/config_paytm.php');
				$this->load->view('paytm_app/lib/encdec_paytm.php');
				$checkSum = "";

				$paytmParams = array();
				$customer_id = $response_headers['studentid'];
				/* add checksum parameters in Array */
				$paytmParams["MID"] = "$mid";
				$paytmParams["ORDERID"] = "$result";
				$paytmParams["CUST_ID"] = "$customer_id";
				$paytmParams['MOBILE_NO'] = '5555566666';
				$paytmParams['EMAIL'] = 'test555666@paytm.com';
				$paytmParams['CHANNEL_ID'] = 'WAP';
				// $response['TXN_AMOUNT'] = number_format($total_sum, 2, '.', '');
				// $paytmParams['TXN_AMOUNT'] = 1;
				$paytmParams['TXN_AMOUNT'] = "$total_sum";
				$paytmParams['WEBSITE'] = "$wap_name";
				$paytmParams['INDUSTRY_TYPE_ID'] = "$industry_type_id";
				$paytmParams['CALLBACK_URL'] = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=$result";
				
				//$checksum = getChecksumFromArray($paytmParams, "YOUR_KEY_HERE");
				$checkSum = getChecksumFromArray($paytmParams,$merchant_key);

					$response =array();
					$response['MID'] = "$mid";
					$response['ORDER_ID'] = "$result";
					$response['CUST_ID'] = "$customer_id";
					$response['MOBILE_NO'] = '5555566666';
					$response['EMAIL'] = 'test555666@paytm.com';
					$response['CHANNEL_ID'] = 'WAP';
					// $response['TXN_AMOUNT'] = number_format($total_sum, 2, '.', '');
					// $response['TXN_AMOUNT'] = 1;
					
					$response['TXN_AMOUNT'] = "$total_sum";
					$response['WEBSITE'] = "$wap_name";
					$response['INDUSTRY_TYPE_ID'] = "$industry_type_id";
					//$response['CALLBACK_URL'] = "https://securegw-stage.paytm.in/theia/processTransaction"; 
					$response['CALLBACK_URL'] = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=$result";
					$response['CHECKSUMHASH'] = $checkSum;
					echo json_encode(array("status_code" => "200", "msg" => "Information saved.", "body" => $response));
					exit;
				}else{
					echo json_encode(array("status_code" => "400", "msg" => "Information not saved", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}
	
	function processPaytmTransaction(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if(empty($_POST['CHECKSUMHASH'])){
					echo json_encode(array("status_code" => "400", "msg" => "Checksumhas not provided.", "body" => NULL));
					exit;
				}
				if(empty($_POST['ORDERID'])){
					echo json_encode(array("status_code" => "400", "msg" => "Order id not provided.", "body" => NULL));
					exit;
				}
				if(empty($_POST['MID'])){
					echo json_encode(array("status_code" => "400", "msg" => "MID not provided.", "body" => NULL));
					exit;
				}
				
				$MerchantKey = $this->parentsapimodel->getMerchantKey($_POST['MID']);
				if($MerchantKey){
					$merchant_key = $MerchantKey[0]['production_merchant_key'];
				}else{
					echo json_encode(array("status_code" => "402", "msg" => "Merchant key not found.", "body" => NULL));
					exit;
				}
				header("Pragma: no-cache");
				header("Cache-Control: no-cache");
				header("Expires: 0");
				// following files need to be included
				$this->load->view('paytm_app/lib/encdec_paytm.php');
				$paytmChecksum = "";
				/* Create a Dictionary from the parameters received in POST */
				$paytmParams = array();
				foreach($_POST as $key => $value){
					if($key == "CHECKSUMHASH"){
						$paytmChecksum = $value;
					} else {
						$paytmParams[$key] = $value;
					}
				}
				// print_r($paytmParams);exit;
				/**
				* Verify checksum
				* Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
				*/
				$isValidChecksum = verifychecksum_e($paytmParams, $merchant_key, $paytmChecksum);
				//echo $isValidChecksum;exit;
				if($isValidChecksum == "TRUE") {
					//echo "Checksum Matched";
					$data = array();
					$data['checksum_value'] = $_POST['CHECKSUMHASH'];
					$data['status'] = $_POST['RESPMSG'];
					$data['payment_success_date'] = date("Y-m-d H:i:s");
					$this->parentsapimodel->updateRecord("tbl_paytmpayment", $data, "id='".$_POST['ORDERID']."' ");
					
					if($_POST['RESPMSG'] == 'Txn Success'){
						$installment_id = $this->parentsapimodel->getInstallmentIds($_POST['ORDERID']);
						//print_r($installment_id);exit;
						$admission_id= $this->parentsapimodel->getAdmintionFeeIds(explode(",",$installment_id[0]['installment_id']));
						$student_id = $installment_id[0]['student_id'];
						// print_r($admission_id);exit;
						if(!empty($admission_id)){
							$i=0;
							$group_components_result = array();
							$class_components_result = array();
							$instalment_collected_amount = array();
							foreach($admission_id as $item){
								if($item['fees_type'] == 'Group'){
									$instalment_collected_amount[] = $item['instalment_remaining_amount'];
									$total_collected_group_amount= $item['instalment_remaining_amount'];
									$group_components_result = $this->parentsapimodel->getGroupFeesComponents($item['admission_fees_id'],$item['group_id']);

									foreach($group_components_result as $ckey=>$cval){
										// existing history of payment
										$group_component_payment_history = $this->parentsapimodel->getdata("tbl_admission_fees_components_collected_history", "*", $condition = "is_last_entry = 'Yes' AND admission_fees_component_id = '".$cval['admission_fees_component_id']."'");
										// print_r($group_component_payment_history);exit;
										if(!empty($group_component_payment_history)){
											//update exiting history entry 
											$existing_data = array();
											$existing_data['is_last_entry'] = "No";
											$this->parentsapimodel->updateRecordpaytm('tbl_admission_fees_components_collected_history', $existing_data, "component_collected_history_id",$group_component_payment_history[0]['component_collected_history_id']);
			
											//make new entry for payment history
											$collected_component_history_data = array();
											$collected_component_history_data['admission_fees_component_id'] = $cval['admission_fees_component_id'];
					
											//collected amount calculation
											$collected_amount = $total_collected_group_amount;
											$remaining_total_amount = $total_collected_group_amount - $group_component_payment_history[0]['remaining_amount'];
											if(($remaining_total_amount) > 0){
												$collected_amount = $group_component_payment_history[0]['remaining_amount'];
												$total_collected_group_amount = $remaining_total_amount;
											}else{
												$total_collected_group_amount = 0;
											}
											
											$collected_component_history_data['collected_amount'] = $collected_amount;
											$collected_component_history_data['remaining_amount'] = ($group_component_payment_history[0]['remaining_amount'] - $collected_amount);
											$collected_component_history_data['is_last_entry'] = "Yes";
											$collected_component_history_data['collected_date'] = date("Y-m-d");
											
											// insert new history
											$this->parentsapimodel->insertData('tbl_admission_fees_components_collected_history', $collected_component_history_data, 1);
										}else{
											$collected_component_history_data = array();
											$collected_component_history_data['admission_fees_component_id'] = $cval['admission_fees_component_id'];
					
											//collected amount calculation
											$collected_amount = $total_collected_group_amount;
											$remaining_total_amount = $total_collected_group_amount - $cval['sub_total'];
											if(($remaining_total_amount) > 0){
												$collected_amount = $cval['sub_total'];
												$total_collected_group_amount = $remaining_total_amount;
											}else{
												$total_collected_group_amount = 0;
											}
											$collected_component_history_data['collected_amount'] = $collected_amount;
											$collected_component_history_data['remaining_amount'] = ($cval['sub_total'] - $collected_amount);
											$collected_component_history_data['is_last_entry'] = "Yes";
											$collected_component_history_data['collected_date'] = date("Y-m-d");
											$this->parentsapimodel->insertData('tbl_admission_fees_components_collected_history', $collected_component_history_data, 1);
										}
									}
									
								}
								if($item['fees_type'] == 'Class'){
									//print_r($item);exit;
									$instalment_collected_amount[] = $item['instalment_remaining_amount'];
									$total_collected_class_amount= $item['instalment_remaining_amount'];
									$class_components_result = $this->parentsapimodel->getClassFeesComponents($item['admission_fees_id'],$item['course_id']);
									//print_r($class_components_result);exit;
									if(!empty($class_components_result)){
										$remaining_total_amount = 0;
										foreach($class_components_result as $ckey=>$cval){
											// existing history of payment
											$class_component_payment_history = $this->parentsapimodel->getdata("tbl_admission_fees_components_collected_history", "*", $condition = "is_last_entry = 'Yes' AND admission_fees_component_id = '".$cval['admission_fees_component_id']."'");
											if(!empty($class_component_payment_history)){
												//update exiting history entry 
												$existing_data = array();
												$existing_data['is_last_entry'] = "No";
												$this->parentsapimodel->updateRecordpaytm('tbl_admission_fees_components_collected_history', $existing_data, "component_collected_history_id",$class_component_payment_history[0]['component_collected_history_id']);
			
												//make new entry for payment history
												$collected_component_history_data = array();
												$collected_component_history_data['admission_fees_component_id'] = $cval['admission_fees_component_id'];
						
												//collected amount calculation
												$collected_amount = $total_collected_class_amount;
												$remaining_total_amount = $total_collected_class_amount - $class_component_payment_history[0]['remaining_amount'];
												if(($remaining_total_amount) > 0){
													$collected_amount = $class_component_payment_history[0]['remaining_amount'];
													$total_collected_class_amount = $remaining_total_amount;
												}else{
													$total_collected_class_amount = 0;
												}
												
												$collected_component_history_data['collected_amount'] = $collected_amount;
												$collected_component_history_data['remaining_amount'] = ($class_component_payment_history[0]['remaining_amount'] - $collected_amount);
												$collected_component_history_data['is_last_entry'] = "Yes";
												$collected_component_history_data['collected_date'] = date("Y-m-d");
												
												//insert new history
												//print_r($collected_component_history_data);exit;
												$this->parentsapimodel->insertData('tbl_admission_fees_components_collected_history', $collected_component_history_data, 1);
											}else{
												$collected_component_history_data = array();
												$collected_component_history_data['admission_fees_component_id'] = $cval['admission_fees_component_id'];
												//collected amount calculation
											//	print_r($total_collected_class_amount);exit;
												$collected_amount = $total_collected_class_amount;
												$remaining_total_amount = $total_collected_class_amount - $cval['sub_total'];
												//print_r($remaining_total_amount);exit;
												if(($remaining_total_amount) > 0){
													$collected_amount = $cval['sub_total'];
													$total_collected_class_amount = $remaining_total_amount;
												}else{
													$total_collected_class_amount = 0;
												}
												$collected_component_history_data['collected_amount'] = $collected_amount;
												$collected_component_history_data['remaining_amount'] = ($cval['sub_total'] - $collected_amount);
												$collected_component_history_data['is_last_entry'] = "Yes";
												$collected_component_history_data['collected_date'] = date("Y-m-d");
											//	print_r($collected_component_history_data);exit;
												$this->parentsapimodel->insertData('tbl_admission_fees_components_collected_history', $collected_component_history_data, 1);
											}
											
										}
										
									}
								}
							}
						//	exit;
							/* ------------------------------ */
							$total_mandatory_amount = 0;
							$total_amount = 0;
							$already_collected_amount = 0;
							$total_remaining_amount = 0;

							$total_mandatory_amount_group = 0;
							$total_amount_group = 0;
							$already_collected_amount_group = 0;
							$total_remaining_amount_group = 0;
							foreach($admission_id as $key=>$val){
								if(!empty($val['fees_type'] == 'Class')){
									$total_amount = $total_amount + $val['instalment_remaining_amount'];
									$already_collected_amount = $already_collected_amount + $val['instalment_collected_amount'];
									$total_remaining_amount = $val['total_amount'] - $total_amount;
									$total_mandatory_amount = $total_mandatory_amount + $val['instalment_remaining_amount'];
								
									// $total_collected_amount = $total_amount + $already_collected_amount;
									$total_collected_amount = $total_amount;
									$feesdata['fees_amount_collected'] = (!empty($total_collected_amount)) ? $total_collected_amount : 0;
									$feesdata['fees_remaining_amount'] = (!empty($total_remaining_amount)) ? $total_remaining_amount : 0;
									$feesdata['updated_on'] = date("Y-m-d H:m:i");
									$feesdata['updated_by'] = $student_id;
									$this->parentsapimodel->updateRecordpaytm('tbl_admission_fees', $feesdata, "admission_fees_id",$val['admission_fees_id']);
						
									$collected_amount = $val['instalment_remaining_amount'];
									$installmentdata['instalment_collected_amount'] = (!empty($collected_amount) ? $collected_amount : 0);
									$installmentdata['instalment_remaining_amount'] = '0';
									if($collected_amount == '0'){
										$installmentdata['instalment_status'] = 'Pending';
									}else if($val['instalment_remaining_amount'] == $collected_amount){
										$installmentdata['instalment_status'] = 'Paid';
									}else if($val['instalment_collected_amount'] < $collected_amount){
										$installmentdata['instalment_status'] = 'Partial';
									}
									$installmentdata['updated_on'] = date("Y-m-d H:m:i");
									$installmentdata['updated_by'] = $student_id;
									$result = $this->parentsapimodel->updateRecordpaytm('tbl_admission_fees_instalments', $installmentdata, "admission_fees_instalment_id",$val['admission_fees_instalment_id']);
								}
								//updation for Group 
								if(!empty($val['fees_type'] == 'Group')){
									$total_amount = $total_amount + $val['instalment_remaining_amount'];
									$already_collected_amount_group = $already_collected_amount_group + $val['instalment_collected_amount'];
									$total_remaining_amount_group = $val['total_amount'] - $total_amount;
									$total_mandatory_amount_group = $total_mandatory_amount_group + $val['instalment_remaining_amount'];
									
									// $total_collected_amount = $total_amount_group + $already_collected_amount_group;
									$total_collected_amount = $total_amount;
									$feesdata_group['fees_amount_collected'] = (!empty($total_collected_amount)) ? $total_collected_amount : 0;
									$feesdata_group['fees_remaining_amount'] = (!empty($total_remaining_amount_group)) ? $total_remaining_amount_group : 0;
									$feesdata_group['updated_on'] = date("Y-m-d H:m:i");
									$feesdata_group['updated_by'] = $student_id;
									$this->parentsapimodel->updateRecordpaytm('tbl_admission_fees', $feesdata_group, "admission_fees_id",$val['admission_fees_id']);

									$collected_amount = $val['instalment_remaining_amount'];
									$installmentdata['instalment_collected_amount'] = (!empty($collected_amount) ? $collected_amount : 0);
									$installmentdata['instalment_remaining_amount'] = '0';
									if($collected_amount == '0'){
										$installmentdata['instalment_status'] = 'Pending';
									}else if($val['instalment_remaining_amount'] == $collected_amount){
										$installmentdata['instalment_status'] = 'Paid';
									}else if($val['instalment_remaining_amount'] < $collected_amount){
										$installmentdata['instalment_status'] = 'Partial';
									}
									$installmentdata['updated_on'] = date("Y-m-d H:m:i");
									$installmentdata['updated_by'] = $student_id;
									$result = $this->parentsapimodel->updateRecordpaytm('tbl_admission_fees_instalments', $installmentdata, "admission_fees_instalment_id",$val['admission_fees_instalment_id']);
								}
							}
							
							/* ------------------------------ */
							$result1 = $this->parentsapimodel->getdata("tbl_admission_fees","*"," student_id=' ".$student_id."' ");
							for($r=0;$r<sizeof($result1);$r++){
								$result = $result1[$r]['admission_fees_id'];
								//changeexisting payment details
								$isPaymentDone = $this->parentsapimodel->getdata("tbl_fees_payment_details", "*", "admission_fees_id='".$result."' ");
								if(!empty($isPaymentDone)){
									foreach($isPaymentDone as $pkey=>$pval){
										$existing_payment_data = array();
										$existing_payment_data['is_last_entry'] = 'No';
										$this->parentsapimodel->updateRecordpaytm('tbl_fees_payment_details', $existing_payment_data,"fees_payment_detail_id",$pval['fees_payment_detail_id']);
									}
								}

								$installmentid = [];
								$installmentid = $this->parentsapimodel->getdata("tbl_admission_fees_instalments", "admission_fees_instalment_id,instalment_collected_amount", "admission_fees_id='".$result."' ");
								if(!empty($installmentid)){
									$size = sizeof($installmentid);
									for($s=0;$s<$size;$s++){
										$payment_data = array();
										$payment_data['admission_fees_id'] = $result;
										$payment_data['admission_fees_instalment_id'] = $installmentid[$s]['admission_fees_instalment_id'];
										$payment_data['instalment_collected_amount'] = $installmentid[$s]['instalment_collected_amount'];
										$payment_data['payment_mode'] = 'Paytm' ;
										$payment_data['receipt_date'] = date("Y-m-d H:m:i");
										$payment_data['created_on'] = date("Y-m-d H:m:i");
										$payment_data['created_by'] = $student_id;
										$payment_data['updated_on'] = date("Y-m-d H:m:i");
										$payment_data['updated_by'] = $student_id;
										
										$save_data = $this->parentsapimodel->insertData('tbl_fees_payment_details', $payment_data, 1);
									}
								}
								if(isset($installment_id[0]['installment_id'])){
									if($total_collected_amount < $total_mandatory_amount){
										$sstatus_array['status'] = 'In-active';
									}
									else{
										$status_data = $this->parentsapimodel->getdata("tbl_student_master", "status", "student_id ='".$student_id."'");
										$sstatus_array['status'] = 'Active';
									}
									$this->parentsapimodel->updateRecordpaytm('tbl_student_master', $sstatus_array, "student_id",$student_id);
									
									$sdstatus_array = array();
									if($total_collected_amount < $total_mandatory_amount){
										$sdstatus_array['status'] = 'In-active';
									}
									else{
										$status_data = $this->parentsapimodel->getdata("tbl_student_details","status", "student_id ='".$student_id."'");
										$sdstatus_array['status'] = 'Active';
									}
									$this->parentsapimodel->updateRecordpaytm('tbl_student_details', $sdstatus_array, "student_id",$student_id);
									//echo "done";exit;
								}
							}
							if (!empty($save_data)) {
								// echo "<pre>";print_r($_POST);exit;
								$reciept_date = date("Y-m-d");
								$studentinfo = $this->parentsapimodel->getdata1("tbl_admission_fees", "admission_fees_id,fees_type,category_id", "student_id='".$student_id."'");
							//print_r($studentinfo);exit;
								/* ---------------- single receiep strat ------------- */
								// single receipt
								$reciept_no = "";
								$receipt_amount = 0;
								$receipt_payment_mode = "";
								$return_receipt = "";
								if(!empty($studentinfo)){
									$admition_id = array_column($studentinfo,"admission_fees_id");
									$admintion_fee_id = implode(',',$admition_id);
									$getPayemtDetails = $this->parentsapimodel->getPayemtDetails("i.admission_fees_id IN($admintion_fee_id)");
									$count = count($getPayemtDetails);
									//print_r($getPayemtDetails);exit;
									$student_name = (!empty($getPayemtDetails[0]['student_first_name'])) ? $getPayemtDetails[0]['student_first_name']." ".$getPayemtDetails[0]['student_last_name'] : '';
									$academic_year = (!empty($getPayemtDetails[0]['academic_year_master_name'])) ? $getPayemtDetails[0]['academic_year_master_name'] : '';
									$category_name = (!empty($getPayemtDetails[0]['categoy_name'])) ? $getPayemtDetails[0]['categoy_name'] : '';
									$course_name = (!empty($getPayemtDetails[0]['course_name'])) ? $getPayemtDetails[0]['course_name'] : '';
									$batch_name = (!empty($getPayemtDetails[0]['batch_name'])) ? $getPayemtDetails[0]['batch_name'] : '';
									$group_name = (!empty($getPayemtDetails[0]['group_master_name'])) ? $getPayemtDetails[0]['group_master_name'] : '';
									$center_name = (!empty($getPayemtDetails[0]['center_name'])) ? $getPayemtDetails[0]['center_name'] : '';
									$receipt_print_name = (!empty($getPayemtDetails[0]['receipt_print_name'])) ? $getPayemtDetails[0]['receipt_print_name'] : '';
									$center_address = (!empty($getPayemtDetails[0]['center_address'])) ? $getPayemtDetails[0]['center_address'] : '';
									$center_contact_no = (!empty($getPayemtDetails[0]['center_contact_no'])) ? $getPayemtDetails[0]['center_contact_no'] : '';
									$center_email_id = (!empty($getPayemtDetails[0]['center_email_id'])) ? $getPayemtDetails[0]['center_email_id'] : '';
									$center_code = (!empty($getPayemtDetails[0]['center_code'])) ? $getPayemtDetails[0]['center_code'] : '';
									for($i=0;$i< $count;$i++){
										$fees_id[] = (!empty($getPayemtDetails[$i]['fees_id'])) ? $getPayemtDetails[$i]['fees_id'] : '';
									}
									$getRecieptNo = $this->parentsapimodel->getdata_orderby_limit1("tbl_fees_payment_receipt", "fees_payment_receipt_id,receipt_no", "1=1", "order by fees_payment_receipt_id desc");	
									$reciept_id = 0;
									if(!empty($getRecieptNo)){
										$reciept_id =  substr($getRecieptNo[0]['receipt_no'], strrpos($getRecieptNo[0]['receipt_no'], '/') + 1) ;
									}
									$reciept_id = $reciept_id+1;
									if(!empty($center_code)){
										$reciept_no .= $center_code."/";
									}
									if(!empty($academic_year)){
										$reciept_no .= $academic_year."/";
									}
									$reciept_no .= $reciept_id;
									$uniquefee_id=array_unique($fees_id);
									$fee_ids =implode(',',$uniquefee_id);
									$date_v = date("d_m_Y_H_i_s",strtotime($reciept_date));
									$combine_receipt = '';
									$combine_receipt .= '<div style="width:750px;padding:0 30px;">
														<div style="margin:10px;text-align:center;margin:0 auto">
															<img src="https://app.montanapreschool.com/assets/images/aptech-montana-logo.jpg" alt="The Montana International Pre school Pvt. Ltd." title="The Montana International Pre school Pvt. Ltd." style="width:180px;"/>
														</div>
														<div>';
														//get center information
														$combine_receipt .= '<h1 style="text-align:center;color:#000;font-size:25px;margin:10px 0;padding:0;">'.$receipt_print_name.'</h1>
														<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><b>Centre Address:</b> '.$center_address.' Contact: '.$center_contact_no.', '.'<a href="mailto:'.$center_email_id.' " style="color:#000;text-decoration:none;">'.$center_email_id.'</a></p><p style="text-align:center;width:100%;margin-top:-10px;font-size:12px;">CIN No.: U80101MH2013PTC250962 | GST No.: 27AAECT8559Q1ZA</p>';
														
														$combine_receipt .= '</div>
														<div>
														<table cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
															<tr>
																<td>
																	<table cellpadding="0" cellspacing="0" border="0" style="width:700px;">
																		<tr>
																			<th style="width:25%;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Date</th>
																			<th style="width:25%;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">ENR Number</th>
																			<th style="width:25%;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000">Name</th>
																			<th style="width:25%;padding:10px;text-align:left;border-right:1px solid #000;border-bottom:1px solid #000" colspan="2">Class</th>
																		</tr>
																		<tr>
																			<td style="padding:10px;text-align:left;font-size:14px"><strong>'.date("d-M-Y",strtotime($reciept_date)).'</strong></td>
																			<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$getPayemtDetails[0]['enrollment_no'].'</strong></td>
																			<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$student_name.'</strong></td>';
																		if($getPayemtDetails[0]['fees_type'] == 'Class'){		
																			$combine_receipt .= '<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$course_name.'</strong></td>
																			';
																		}else{
																			$combine_receipt .= '<td style="padding:10px;text-align:left;font-size:14px"><strong>'.$group_name.'</strong></td>
																			<td style="padding:10px;font-size:14px"><strong>-</strong></td>';
																		}
																			
																		$combine_receipt .= '</tr>
																		<tr>
																			<td colspan="3" style="padding:10px;text-align:left;font-size:14px">';
																		if($getPayemtDetails[0]['guardian']=='father'){
																			$combine_receipt.= '<p style="color:#000;font-size:14px;margin:3px 0;"><b>Father Name:</b> '.$getPayemtDetails[0]['father_name'].'</p>';
																		}else if($getPayemtDetails[0]['guardian']=='mother'){
																			$combine_receipt.= '<p style="color:#000;font-size:14px;margin:3px 0;"><b>Mother Name:</b> '.$getPayemtDetails[0]['mother_name'].'</p>';
																		}else if($getPayemtDetails[0]['guardian']=='guadian'){
																			$combine_receipt.= '<p style="color:#000;font-size:14px;margin:3px 0;"><b>Guadian Name: </b> '.$getPayemtDetails[0]['parent_name'].'</p>';
																		
																		}
																		$combine_receipt.= '<p style="color:#000;font-size:14px;margin:3px 0;"><b>Address: </b>'.$getPayemtDetails[0]['present_address'].' ,'.$getPayemtDetails[0]['address1'].', '.$getPayemtDetails[0]['address2'].', '.$getPayemtDetails[0]['address3'].', '.$getPayemtDetails[0]['city_name'].' ,'.$getPayemtDetails[0]['state_name'].' ,'.$getPayemtDetails[0]['country_name'].', '.$getPayemtDetails[0]['pincode'].'</p>
																			</td>						
																					
						
																			<td style="padding:10px;text-align:left;font-size:14px"> 
																				<p style="color:#000;font-size:14px;margin:3px 0;"><b>Receipt No: </b>'.$reciept_no.'</p>
																			</td>
																		</tr>					
																	</table>
																</td>
															</tr>
														</table>
														<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;" >
															<tr>
																<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Component(s)</th>
																<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Fee Type</th>
																<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Class/Group</th>
																<th style="padding:10px;text-align:right;border-top:1px solid #000;border-bottom:1px solid #000">Amount</th>	
															</tr>';

															//update installment details collected amount and receipt no
															$getallFeesInstallmentUpdate_condition = " is_last_entry='Yes' && admission_fees_id IN(".$admintion_fee_id.") ";
															$getallFeesInstallmenForUpdate = $this->parentsapimodel->getdataPaytm("tbl_fees_payment_details",$getallFeesInstallmentUpdate_condition);
															
															if(!empty($getallFeesInstallmenForUpdate)){
																foreach($getallFeesInstallmenForUpdate as $key=>$val){
																	$installment_data = array();
																	$installment_data['receipt_no'] = $reciept_no;
																	$this->parentsapimodel->updateRecordpaytm("tbl_fees_payment_details", $installment_data,"fees_payment_detail_id",$val['fees_payment_detail_id']);
																}
															}

															//get fess components
															$component_condition = " afc.fees_id IN(".$fee_ids.") && afcch.is_last_entry='Yes' && afcch.collected_amount > 0 && afc.admission_fees_id IN(".$admintion_fee_id.") ";
															$getallFeesComponents = $this->parentsapimodel->getFeesComponentsforReceipt($component_condition);

															//update collected history by receipt number
															$getallFeesComponentsForUpdate_condition = " afc.fees_id IN(".$fee_ids.") && afcch.is_last_entry='Yes' && afc.admission_fees_id IN(".$admintion_fee_id.") ";
															$getallFeesComponentsForUpdate = $this->parentsapimodel->getFeesComponentsforReceipt($getallFeesComponentsForUpdate_condition);
															// print_r($getallFeesComponentsForUpdate);exit;
															if(!empty($getallFeesComponentsForUpdate)){
																foreach($getallFeesComponentsForUpdate as $key=>$val){
																	$receipt_collected_data = array();
																	$receipt_collected_data['receipt_no'] = $reciept_no;
																	$this->parentsapimodel->updateRecordpaytm("tbl_admission_fees_components_collected_history", $receipt_collected_data,"component_collected_history_id",$val['component_collected_history_id']);
																}
															}

															// print_r($getallFeesComponents);exit;
															if(!empty($getallFeesComponents)){
																/* -------------- combine fees for receipt start */
																$course_data = array();
																$group_data = array();
																//separate group and class fees
																foreach($getallFeesComponents as $key=>$val){
																	if(!empty($val['course_name'])){
																		$course_data[] = $val;
																	}else{
																		$group_data[]  = $val;
																	}
																}
																//combine course fee component by receipt name
																$course_data_final_array = array();
																if(!empty($course_data)){
																	$course_data = $this->array_formation($course_data,'receipt_name');
																	$count = 0;
																	foreach($course_data as $key=>$val){
																		$totalVal = array_sum(array_column($val,"collected_amount"));
																		$course_data_final_array[$count]['receipt_name'] = $val[0]['receipt_name'];
																		$course_data_final_array[$count]['fees_type'] = $val[0]['fees_type'];
																		$course_data_final_array[$count]['collected_amount'] = $totalVal;
																		$course_data_final_array[$count]['course_name'] = $val[0]['course_name'];
																		$course_data_final_array[$count]['group_master_name'] = $val[0]['group_master_name'];
																		$count++;
																	}
																}
															//print_r($course_data);exit;
																//combine group fee component by receipt name
																$group_data_final_array = array();
																if(!empty($group_data)){
																	// multiple group formation
																	$multiple_group_data =  $this->array_formation($group_data,'group_master_name');
																	if(!empty($multiple_group_data)){
																		$count = 0;
																		foreach($multiple_group_data as $mgkey=>$mgval){
																			$mgval = $this->array_formation($mgval,'receipt_name');
																			foreach($mgval as $key=>$val){
																				$totalVal = array_sum(array_column($val,"collected_amount"));
																				$group_data_final_array[$count]['receipt_name'] = $val[0]['receipt_name'];
																				$group_data_final_array[$count]['fees_type'] = $val[0]['fees_type'];
																				$group_data_final_array[$count]['collected_amount'] = $totalVal;
																				$group_data_final_array[$count]['course_name'] = $val[0]['course_name'];
																				$group_data_final_array[$count]['group_master_name'] = $val[0]['group_master_name'];
																				$count++;
																			}
																		}
																	}
																}
															//	 echo "<pre>";print_r($group_data_final_array);exit;
																$getallFeesComponents = array_merge($course_data_final_array,$group_data_final_array);
															//	print_r($getallFeesComponents);exit;
																/* -------------- combine fees for receipt end */
																$sumofcomponent = 0;
																for($i=0; $i < sizeof($getallFeesComponents); $i++){
																	$sumofcomponent += $getallFeesComponents[$i]['collected_amount'];
																	$combine_receipt .= '<tr>						
																		<td style="padding:10px;text-align:left;font-size:14px">'.$getallFeesComponents[$i]['receipt_name'].'</td>
																		<td style="padding:10px;text-align:left;font-size:14px">'.$getallFeesComponents[$i]['fees_type'].' Fee</td>
																		<td style="padding:10px;text-align:left;font-size:14px">'.(!empty($getallFeesComponents[$i]['group_master_name'])?$getallFeesComponents[$i]['group_master_name']:$getallFeesComponents[$i]['course_name']).'</td>
																		<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getallFeesComponents[$i]['collected_amount'],2).'</td>
																							
																	</tr>';
																}
															}
														
															$combine_receipt .= '<tr>
																<td style="padding:10px;text-align:left;font-size:14px" colspan="3"><strong>Total Amount</strong></td>
																
																<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofcomponent,2).'</strong></td>						
															</tr>';
						
															$componentrupees = (float)$sumofcomponent;
															$receipt_amount = number_format((float)$sumofcomponent, 2, '.', '');
															//$receipt_payment_mode = $getPayemtDetails[0]['payment_mode'];
														
															$combine_receipt .='<tr>
															<td style="padding:10px;text-align:left;font-size:14px" colspan="1"><b>Total Amount (In Words): </b></td>
															<td style="padding:10px;text-align:left;font-size:14px" colspan="3">'.strtoupper($this->getIndianCurrency($componentrupees)).'</td>
															</tr>';
															
						
															$combine_receipt .= '</table>
														
														<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border:1px solid #000;width:700px;margin:0 auto;">
																			<tr>
																				<td style="padding:10px;text-align:left;font-size:14px"><strong>Payment Mode:</strong> '.$getPayemtDetails[0]['payment_mode'].'</td>
																				<td style="padding:10px;text-align:left;font-size:14px">';
																				if($getPayemtDetails[0]['payment_mode'] != 'Cash' ){
																					$combine_receipt .= '<strong>Bank Name: </strong>'.$getPayemtDetails[0]['bank_name'];
																				}
																		$combine_receipt .= '</td>
																			</tr>';
																		if($getPayemtDetails[0]['payment_mode'] == 'Cheque'){	
																			$combine_receipt .= '<tr>
																				<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque No: </strong>'.$getPayemtDetails[0]['cheque_no'].'</td>
																				<td style="padding:10px;text-align:left;font-size:14px"><strong>Cheque Date: </strong>'.date("d-M-Y", strtotime($getPayemtDetails[0]['transaction_date'])).'</td>
																			</tr>';
																		}else if($getPayemtDetails[0]['payment_mode'] == 'Netbanking'){	
																			$combine_receipt .= '<tr>
																				<td style="padding:10px;text-align:left;font-size:14px"><strong>Transaction No: </strong>'.$getPayemtDetails[0]['transaction_id'].'</td>
																				<td style="padding:10px;text-align:left;font-size:14px"><strong>Transaction Date: </strong>'.date("d-M-Y", strtotime($getPayemtDetails[0]['transaction_date'])).'</td>
																			</tr>';
																		}else{
																			$combine_receipt .= '<tr>
																				<td style="padding:10px;text-align:left;font-size:14px"><strong>&nbsp;</strong></td>
																				<td style="padding:10px;text-align:left;font-size:14px"><strong>&nbsp;</strong></td>
																			</tr>';
																		}
																		$combine_receipt .= '</table>';
															
														$combine_receipt .='<div><p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"> <b>Notes:</b> 1. Cheques Subject to REALISATION. 2. Fees once paid are Non-Refundable & Non-Transferable. 3. This Receipt must be produced when demanded. 4. This is electronically generated receipt, hence does not require signature. </p>
														</div><br><br>';
														if($getPayemtDetails[0]['payment_mode']=='Cheque' && (strtotime($getPayemtDetails[0]['transaction_date']) > strtotime(date("Y-m-d",strtotime($reciept_date))))){
															$combine_receipt .='<div><p style="text-align:center;color:#000;font-size:14px;"><b>“This receipt is issued against a Post Dated Cheque (PDC)” </b></p></div><br><br>';
														}	
									ini_set('memory_limit','100M');
									$date_v = date("d.m.y",strtotime($reciept_date));
									$receipt_file_name = "PaymentReceipt_".$reciept_id."_".$date_v.".pdf";
									$receipt_pdfFilePath = DOC_ROOT_FRONT."/images/payment_invoices/".$receipt_file_name;
									$file_path = FRONT_API_URL."/images/payment_invoices/".$receipt_file_name;	
									$return_receipt = $file_path;
									$this->load->library('M_pdf');
									$pdf = $this->m_pdf->load();
									$pdf = new mPDF('utf-8');
									$pdf->WriteHTML($combine_receipt); // write the HTML into the PDF
									$pdf->Output($receipt_pdfFilePath, 'F'); // save to file because we can	
								}
								/* ----------------- single recept end ---------------- */
						
								for ($s=0; $s <sizeof($studentinfo) ; $s++) { 
									$getbcNo = $this->parentsapimodel->getdata_orderby_limit1("tbl_fees_payment_receipt", "fees_payment_receipt_id", "1=1", "order by fees_payment_receipt_id desc");
										
									$bc_id = (!empty($getbcNo)) ? ($getbcNo[0]['fees_payment_receipt_id'] + 1) : 1;
									
									$bc_no = "";
									if(!empty($center_code) || !empty($academic_year)){
										$bc_no .= "BC".$bc_id."/".$academic_year."/".$center_code;
									}
						
									$getreceiptinfo = $this->parentsapimodel->getdata1("tbl_fees_payment_receipt", "fees_payment_receipt_id", "admission_fees_id='".$studentinfo[$s]['admission_fees_id']."'");
						
									
									$getPayemtDetails = $this->parentsapimodel->getPayemtDetails("i.admission_fees_id='".$studentinfo[$s]['admission_fees_id']."' ");
									//print_r($getPayemtDetails);exit;
									$student_name = (!empty($getPayemtDetails[0]['student_first_name'])) ? $getPayemtDetails[0]['student_first_name']." ".$getPayemtDetails[0]['student_last_name'] : '';
									$academic_year = (!empty($getPayemtDetails[0]['academic_year_master_name'])) ? $getPayemtDetails[0]['academic_year_master_name'] : '';
									$category_name = (!empty($getPayemtDetails[0]['categoy_name'])) ? $getPayemtDetails[0]['categoy_name'] : '';
									$course_name = (!empty($getPayemtDetails[0]['course_name'])) ? $getPayemtDetails[0]['course_name'] : '';
									$batch_name = (!empty($getPayemtDetails[0]['batch_name'])) ? $getPayemtDetails[0]['batch_name'] : '';
									$group_name = (!empty($getPayemtDetails[0]['group_master_name'])) ? $getPayemtDetails[0]['group_master_name'] : '';
									$center_name = (!empty($getPayemtDetails[0]['center_name'])) ? $getPayemtDetails[0]['center_name'] : '';
									$receipt_print_name = (!empty($getPayemtDetails[0]['receipt_print_name'])) ? $getPayemtDetails[0]['receipt_print_name'] : '';
									$center_address = (!empty($getPayemtDetails[0]['center_address'])) ? $getPayemtDetails[0]['center_address'] : '';
									$center_contact_no = (!empty($getPayemtDetails[0]['center_contact_no'])) ? $getPayemtDetails[0]['center_contact_no'] : '';
									$center_email_id = (!empty($getPayemtDetails[0]['center_email_id'])) ? $getPayemtDetails[0]['center_email_id'] : '';
									$center_code = (!empty($getPayemtDetails[0]['center_code'])) ? $getPayemtDetails[0]['center_code'] : '';
									$intallment_yes_no = (!empty($getPayemtDetails[0]['is_instalment'])) ? $getPayemtDetails[0]['is_instalment'] : '';
									$installment_nos = (!empty($getPayemtDetails[0]['no_of_installments'])) ? $getPayemtDetails[0]['no_of_installments'] : '';
									$discount_amount = (!empty($getPayemtDetails[0]['discount_amount'])) ? $getPayemtDetails[0]['discount_amount'] : '';
									$gst_amount = (!empty($getPayemtDetails[0]['gst_amount'])) ? $getPayemtDetails[0]['gst_amount'] : '';
									$total_fees = (!empty($getPayemtDetails[0]['fees_total_amount'])) ? $getPayemtDetails[0]['fees_total_amount'] : '';
									$total_amount = (!empty($getPayemtDetails[0]['total_amount'])) ? $getPayemtDetails[0]['total_amount'] : '';
									$collected_fees = (!empty($getPayemtDetails[0]['fees_amount_collected'])) ? $getPayemtDetails[0]['fees_amount_collected'] : '';
									$remaining_fees = (!empty($getPayemtDetails[0]['fees_remaining_amount'])) ? $getPayemtDetails[0]['fees_remaining_amount'] : '';
									
									$_POST['isadmission'] = 'No';
									
									//create invoice
									ini_set('memory_limit','100M');
									$date_v = date("d.m.y",strtotime($reciept_date));
									$invoice_file_name = "BookingConfirmation_BC".$bc_id."_".$date_v.".pdf";
									$invoice_pdfFilePath = DOC_ROOT_FRONT."/images/payment_invoices/".$invoice_file_name;
									$file_path = FRONT_API_URL."/images/payment_invoices/".$invoice_file_name;
									// invoice format
									$invoice = '';
									$invoice  = '<html><body>';	
									$invoice .= '<div style="width:750px;padding:0 30px;">
														<div style="margin:10px;text-align:center;margin:0 auto">
														<img src="https://app.montanapreschool.com/assets/images/aptech-montana-logo.jpg" alt="The Montana International Pre school Pvt. Ltd." title="The Montana International Pre school Pvt. Ltd." style="width:180px;"/>
														</div>';
														$invoice .= '<div>
															<h1 style="text-align:center;color:#000;font-size:25px;margin:10px 0;padding:0;">'.$receipt_print_name.'</h1>
															<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><b>Centre Address:</b> '.$center_address.'</p>
															<p style="text-align:center;color:#000;font-size:14px;margin:3px 0;"><a href="mailto:'.$center_email_id.' " style="color:#000;text-decoration:none;">'.$center_email_id.'</a></p><p style="text-align:center;width:100%;margin-top:-10px;font-size:12px;">CIN No.: U80101MH2013PTC250962 | GST No.: 27AAECT8559Q1ZA</p>
														</div>';
														
														$invoice .= '<hr style="1px dashed black">&nbsp;
														<h4 style="text-align: center;font-size:20px;margin:10px 0;padding:0">Booking Confirmation</h4>
														<table cellpadding="0" cellspacing="0" border="0" style="border:0px none;width:700px;">
															<tr>
																<td style="width:350px;padding:10px;text-align:left;">
																<p style="color:#000;font-size:14px;margin:7px 0;"><strong>BC Number: </strong>'.$bc_no.'</p>
																	<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Child’s Name:</strong>'.$getPayemtDetails[0]['student_first_name'].' '.$getPayemtDetails[0]['student_last_name'].'</p>'; 
						
																	if($getPayemtDetails[0]['guardian']=='father'){
																		$invoice .=	'<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Father Name:</strong> '.$getPayemtDetails[0]['father_name'].'</p>';
																	}else if($getPayemtDetails[0]['guardian']=='mother'){
																		$invoice .=	'<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Mother Name: </strong>'.$getPayemtDetails[0]['mother_name'].'</p>';
																	}else if($getPayemtDetails[0]['guardian']=='guadian'){
																		$invoice .=	'<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Guadian Name:</strong> '.$getPayemtDetails[0]['parent_name'].'</p>';
																	}
																	$invoice .=	'<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Address: </strong>'.$getPayemtDetails[0]['present_address'].' ,'.$getPayemtDetails[0]['address1'].', '.$getPayemtDetails[0]['address2'].', '.$getPayemtDetails[0]['address3'].', '.$getPayemtDetails[0]['city_name'].' ,'.$getPayemtDetails[0]['state_name'].' ,'.$getPayemtDetails[0]['country_name'].', '.$getPayemtDetails[0]['pincode'].'</p>';
																	if($getPayemtDetails[0]['course_name'] != ""){
																		$invoice .= '<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Class/Group:</strong>'.$getPayemtDetails[0]['course_name'].'</p>';
																	}else{
																		$invoice .= '<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Class/Group:</strong>'.$getPayemtDetails[0]['group_master_name'].'</p>';
																	}
															$invoice .= '</td>
																<td style="width:350px;padding:10px;text-align:left;" valign="top">
																	<p style="color:#000;font-size:14px;margin:7px 0;"><strong>ENR Number:</strong>'.$getPayemtDetails[0]['enrollment_no'].'</p>
																	<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Phone Number:</strong>'.$getPayemtDetails[0]['father_mobile_contact_no'].'</p> 
																	<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Email ID:</strong>'.$getPayemtDetails[0]['father_email_id'].'</p> 
																	<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Receipt Number: </strong>'.$reciept_no.'</p>
																	
																	<p style="color:#000;font-size:14px;margin:7px 0;"><strong>Receipt Date: </strong>'.date("d-M-Y",strtotime($reciept_date)).'</p>
																	
																</td>
																
															</tr>
														</table><br>';
														//fee component
														$invoice .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Details of Booking Confirmation.</h4>';
														$invoice .='<table width="100%" cellpadding="0" cellspacing="0" border="1" style="border:1px solid #000;width:700px;margin:0 auto;border-collapse: collapse;">
															<tr>
																<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Fee Component </th>
																<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Gross </th>
																<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Discount</th>
																<th style="padding:10px;text-align:right;border-top:1px solid #000;border-bottom:1px solid #000">Total</th>	
															</tr>';
															$getFeesComponentsinvoice = $this->parentsapimodel->getFeesComponentsforinvoice($getPayemtDetails[0]['admission_fees_id']);
															if(!empty($getFeesComponentsinvoice)){
																$sumofgross =0;
																$sumofdiscount =0;
																$sumofcomponent =0;
																for($i=0; $i < sizeof($getFeesComponentsinvoice); $i++){
																	$sumofgross += $getFeesComponentsinvoice[$i]['fees_component_id_value'];
																	$sumofdiscount += $getFeesComponentsinvoice[$i]['discount_amount'];
																	$sumofcomponent += $getFeesComponentsinvoice[$i]['sub_total'];
																	$invoice .= '<tr>						
																		<td style="padding:10px;text-align:left;font-size:14px">'.$getFeesComponentsinvoice[$i]['fees_component_master_name'].'</td>
																		<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getFeesComponentsinvoice[$i]['fees_component_id_value'],2).'</td>	
																		<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getFeesComponentsinvoice[$i]['discount_amount'],2).'</td>
																		<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getFeesComponentsinvoice[$i]['sub_total'],2).'</td>					
																	</tr>';
																}
																$invoice .='<tr>
																<td style="padding:10px;text-align:left;font-size:14px"><strong>Total</strong></td>
																<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofgross,2).'</strong></td>
																<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofdiscount,2).'</strong></td>
																<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofcomponent,2).'</strong></td>
																</tr>';
															}
															
															$invoice .='</table><br>';
														//installment
														$invoice .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Details of Planned Installment Payment.</h4>';
														$invoice .='<table width="100%" cellpadding="0" cellspacing="0" border="1" style="border:1px solid #000;width:700px;margin:0 auto;border-collapse: collapse;">
															<tr>
																<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Installment Number</th>
																<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Planned Installment Date</th>
																
																<th style="padding:10px;text-align:right;border-top:1px solid #000;border-bottom:1px solid #000">Planned Total Installment Amount</th>	
															</tr>';
															$getinstallmentinvoice = $this->parentsapimodel->getinstallmentforinvoice($getPayemtDetails[0]['admission_fees_id']);
															//print_r();exit;
															$sumofinstallment=0;
															$sumofinstallmentdue=0;
															if(!empty($getinstallmentinvoice)){
																$cnt = 0;
																for($i=0; $i < sizeof($getinstallmentinvoice); $i++){
																	$sumofinstallment += $getinstallmentinvoice[$i]['instalment_amount'];
																	$sumofinstallmentdue += $getinstallmentinvoice[$i]['instalment_remaining_amount'];
																	$invoice .= '<tr>						
																		<td style="padding:10px;text-align:right;font-size:14px">'.++$cnt.'</td>
																		<td style="padding:10px;text-align:right;font-size:14px">'.date("d-M-Y", strtotime($getinstallmentinvoice[$i]['instalment_due_date'])).'</td>	
																	
																		<td style="padding:10px;text-align:right;font-size:14px">'.number_format($getinstallmentinvoice[$i]['instalment_amount'],2).'</td>					
																	</tr>';
																}
																$invoice .='<tr>
																<td colspan="2" style="padding:10px;text-align:left;font-size:14px"><strong>Total</strong></td>
																<td style="padding:10px;text-align:right;font-size:14px"><strong>'.number_format($sumofinstallment,2).'</strong></td>
																
																</tr>';
															}
															$invoice .='</table><br>';
															$installmentrupees = (float)$sumofinstallment;
														
														$invoice .='<p style="font-size:13px;margin:4px 0;"><b>Total Amount (In Words): </b>'.strtoupper($this->getIndianCurrency($installmentrupees)).'
														</p><br>';
														//for Receipt Number
														$invoice .='<h4 style="text-align: left;font-size:16px;margin:10px 0;padding:0">Your Payment Details as on Date.</h4>';
														$invoice .='<table width="100%" cellpadding="0" cellspacing="0" border="1" style="border:1px solid #000;width:700px;margin:0 auto;border-collapse: collapse;">
															<tr>
																<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Receipt Number</th>
																<th style="padding:10px;text-align:left;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Receipt Date</th>
																<th style="padding:10px;text-align:right;border-right:1px solid #000;border-top:1px solid #000;border-bottom:1px solid #000">Receipt Amount</th>
																	
															</tr>
															<tr>
																<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">'.$reciept_no.'</td>
																<td style="padding:10px;text-align:left;font-size:14px;border:1px solid #000;">'.date("d-M-Y",strtotime($reciept_date)).'</td>
																<td style="padding:10px;text-align:right;font-size:14px;border:1px solid #000;">'.number_format($collected_fees,2).'</td>
																						
															</tr>
															<tr>
															<td colspan="2" style="padding:10px;text-align:left;font-size:14px"><strong>Total</strong></td>
																<td style="padding:10px;text-align:right;font-size:14px;border:1px solid #000;"><strong>'.number_format($collected_fees,2).'</strong></td>
																					
															</tr>
														</table><br>';
														$rupees = (float)$collected_fees;
														
														$invoice .='<p style="font-size:13px;margin:4px 0;"><b>Total Amount (In Words): </b>'.strtoupper($this->getIndianCurrency($rupees)).'
														</p><br>';
						
														$invoice .= '<p style="font-size:13px;margin:4px 0;">Notes:</p>
														<p style="font-size:13px;margin:4px 0;">1)  Cheques Subject to REALISATION.</p>
														<p style="font-size:13px;margin:4px 0;">2) Fees once paid are Non-Refundable & Non-Transferable. </p>
														<p style="font-size:13px;margin:4px 0;">3) This Receipt must be produced when demanded.</p>
														<p style="font-size:13px;margin:4px 0;">4) This Booking Confirmation provides provisional admission to the child.</p>
														<p style="font-size:13px;margin:4px 0;">5) Ensure timely payments as mentioned in the Booking confirmation.</p>
														<p style="font-size:13px;margin:4px 0;">6) Parents are required to inform the school if there is any change in their address and telephone numbers.</p>
														<p style="font-size:13px;margin:4px 0;">7) The parents are responsible for timely drop and pick up of child.</p>
														<p style="font-size:13px;margin:4px 0;">8) Please refer detailed code of conduct and policy Guidelines sent on your mail.</p>
						
														
															<p style="font-size:13px;margin:13px 0;">For any feedback or suggestions please write to us at <a href="mailto:feedback@montanapreschool.com" style="color:#000;text-decoration:none;"><u>feedback@montanapreschool.com</u></a></p><br>
															<p style="font-size:13px;margin:13px 0;">Registered Office : 402, Sagar Tech Plaza, B wing, Andheri-Kurla Road, Sakinaka,
															Andheri East, Mumbai - 400 072 Maharashtra, India, </p>
															<p style="font-size:13px;margin:13px 0;"><a href="info@montanapreschool.com/" target="_blank" style="color:#000;text-decoration:none;">info@montanapreschool.com/</a></p>
															<p style="font-size:13px;margin:13px 0;"><a href="https://www.facebook.com/montanapreschool/" target="_blank" style="color:#000;text-decoration:none;">https://www.facebook.com/montanapreschool/</a><br>
						
															
						
															<p style="font-size:13px;margin:4px 0;">Disclaimer: Taxes will be charged extra, as applicable, on the date of payment.</p>
															</p>
														</div><br>';
						
									
									$invoice .=  '</body></html>';
						
									$this->load->library('M_pdf');
									$pdf = $this->m_pdf->load();
									$pdf = new mPDF('utf-8');
									$pdf->WriteHTML($invoice); // write the HTML into the PDF
									$pdf->Output($invoice_pdfFilePath, 'F'); // save to file because we can	
									
									//genrate invoice pdf
								
									
									//insert invoice file
									$invoice_file = array();
									$invoice_file['admission_fees_id'] = $studentinfo[$s]['admission_fees_id'];
									$invoice_file['receipt_no'] = $reciept_no;
									$invoice_file['bc_no'] = $bc_no;
									$invoice_file['receipt_file'] = $receipt_file_name;
									$invoice_file['receipt_file_withoutgst'] = $invoice_file_name;
									$invoice_file['receipt_amount'] = $receipt_amount;
									$invoice_file['payment_mode'] = 'Paytm';
									
									$invoice_file['created_on'] = date("Y-m-d H:m:i",strtotime($reciept_date));
									$invoice_file['created_by'] = $student_id;
									$invoice_file['updated_on'] = date("Y-m-d H:m:i",strtotime($reciept_date));
									$invoice_file['updated_by'] = $student_id;
						
									$result_insert = $this->parentsapimodel->insertData('tbl_fees_payment_receipt', $invoice_file, 1);
									if($result_insert){
										
											$get_receipt = $file_path = FRONT_API_URL."/images/payment_invoices/".$receipt_file_name;
											$get_invoice = $file_path = FRONT_API_URL."/images/payment_invoices/".$invoice_file_name;
											echo json_encode(array("status_code" => "200",'success' => '1','msg' => 'Record Added/Updated Successfully.',"body"=>array('receipt'=>$get_receipt,'invoice'=>$get_invoice,'student_id' => $student_id)));
											exit;	
									}
								}
					
							}else{
								echo json_encode(array('success' => '0','msg' => 'Problem in data update.'));
								exit;
							}
						}
					}else{
						//echo "failed";exit;
						echo json_encode(array("status_code" => "402", "msg" => "Paytm Transaction failed.", "body" => NULL));
						exit;
					}
					//echo json_encode(array("status_code" => "200", "msg" => "Checksum Matched.", "body" => $response));
					// exit;
				}else{
						//echo "Checksum Mismatched";
						
					echo json_encode(array("status_code" => "402", "msg" => "Checksum Mismatched.", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}

	function getIndianCurrency($number){
		$decimal = round($number - ($no = floor($number)), 2) * 100;
		$hundred = null;
		$digits_length = strlen($no);
		$i = 0;
		$str = array();
		$words = array(0 => '', 1 => 'one', 2 => 'two',
			3 => 'three', 4 => 'four', 5 => 'five', 6 => 'six',
			7 => 'seven', 8 => 'eight', 9 => 'nine',
			10 => 'ten', 11 => 'eleven', 12 => 'twelve',
			13 => 'thirteen', 14 => 'fourteen', 15 => 'fifteen',
			16 => 'sixteen', 17 => 'seventeen', 18 => 'eighteen',
			19 => 'nineteen', 20 => 'twenty', 30 => 'thirty',
			40 => 'forty', 50 => 'fifty', 60 => 'sixty',
			70 => 'seventy', 80 => 'eighty', 90 => 'ninety');
		$digits = array('', 'hundred','thousand','lakh', 'crore');
		while( $i < $digits_length ) {
			$divider = ($i == 2) ? 10 : 100;
			$number = floor($no % $divider);
			$no = floor($no / $divider);
			$i += $divider == 10 ? 1 : 2;
			if ($number) {
				$plural = (($counter = count($str)) && $number > 9) ? 's' : null;
				$hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
				$str [] = ($number < 21) ? $words[$number].' '. $digits[$counter]. $plural.' '.$hundred:$words[floor($number / 10) * 10].' '.$words[$number % 10]. ' '.$digits[$counter].$plural.' '.$hundred;
			} else $str[] = null;
		}
		$Rupees = implode('', array_reverse($str));
		$paise = ($decimal > 0) ? "." . ($words[$decimal / 10] . " " . $words[$decimal % 10]) . ' Paise' : '';
		return ($Rupees ? $Rupees . 'Rupees ' : '') . $paise; 
	}
	function array_formation($array,$label){
		$return_array = array();
		$tempkey = "";
		foreach($array as $key=>$val){
			$return_array[$val[$label]][] = $val;
		}
		return $return_array;
	}
	function pushLeadData(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if(empty($response_headers['studentid'])){
					echo json_encode(array("status_code" => "400", "msg" => "Student id not provided.", "body" => NULL));
					exit;
				}
				if(empty($response_headers['zoneid'])){
					echo json_encode(array("status_code" => "400", "msg" => "Zone id not provided.", "body" => NULL));
					exit;
				}
				if(empty($response_headers['centerid'])){
					echo json_encode(array("status_code" => "400", "msg" => "Center id not provided.", "body" => NULL));
					exit;
				}
				if(empty($response_headers['academicyear'])){
					echo json_encode(array("status_code" => "400", "msg" => "Academic year not provided.", "body" => NULL));
					exit;
				}
				if(empty($response_headers['courseid'])){
					echo json_encode(array("status_code" => "400", "msg" => "Course not provided.", "body" => NULL));
					exit;
				}
				if(empty($response_headers['batchid'])){
					echo json_encode(array("status_code" => "400", "msg" => "Batch not provided.", "body" => NULL));
					exit;
				}
				if(empty($response_headers['tenure'])){
					echo json_encode(array("status_code" => "400", "msg" => "Requested tenure not provided.", "body" => NULL));
					exit;
				}
				$installment_paytmpayment = $this->parentsapimodel->getInstallmentpaytmpayment($_POST['admission_fees_instalment_id']);
				//print_r($installment_paytmpayment);exit;
				$installment_id =implode(',', $_POST['admission_fees_instalment_id']);
				
				$total_sum=0;
				foreach ($installment_paytmpayment as $row){
				$total_sum+=$row['instalment_remaining_amount'];
				}
				$student_details = $this->parentsapimodel->getStudentDetail($response_headers['studentid']);
				//print_r($student_details);exit;
				if(empty($student_details)){
					echo json_encode(array("status_code" => "400", "msg" => "Country,state,city not fill please contact to center.", "body" => NULL));
					exit;
				}
				//print_r($student_details);exit;
				if(empty($student_details[0]['father_name'])){
					echo json_encode(array("status_code" => "400", "msg" => "Father Name is empty please contact to center.", "body" => NULL));
					exit;
				}
				if(empty($student_details[0]['mother_name'])){
					echo json_encode(array("status_code" => "400", "msg" => "Mother Name is empty please contact to center.", "body" => NULL));
					exit;
				}
				if(empty($student_details[0]['aadhar_no'])){
					echo json_encode(array("status_code" => "400", "msg" => "Aadhar number is empty please contact to center.", "body" => NULL));
					exit;
				}
				if(empty($student_details[0]['pan_no'])){
					echo json_encode(array("status_code" => "400", "msg" => "Pan number is empty please contact to center.", "body" => NULL));
					exit;
				}
				if(empty($student_details[0]['annual_income'])){
					echo json_encode(array("status_code" => "400", "msg" => "Annual income is empty please contact to center.", "body" => NULL));
					exit;
				}
				if(empty($student_details[0]['present_address'])){
					echo json_encode(array("status_code" => "400", "msg" => "Present address is empty please contact to center.", "body" => NULL));
					exit;
				}
				
				if(!empty($student_details[0]['guardian']=='father')){
					$applicant_name = explode(' ',$student_details[0]['father_name']);
					$applicant_first_name =$applicant_name[0];
					$applicant_last_name =$applicant_name[1];
					$applicant_gender = 'M';
					if(empty($student_details[0]['father_dob'])){
						echo json_encode(array("status_code" => "400", "msg" => "Father dob is empty please contact to center.", "body" => NULL));
					exit;
					}
					$applicant_dob = $student_details[0]['father_dob'];
				}
				if(!empty($student_details[0]['guardian'] == 'mother')){
					$applicant_name = explode(' ',$student_details[0]['mother_name']);
					$applicant_first_name =$applicant_name[0];
					$applicant_last_name =$applicant_name[1];
					$applicant_gender = 'F';
					if(empty($student_details[0]['mother_dob'])){
						echo json_encode(array("status_code" => "400", "msg" => "Mother dob is empty please contact to center.", "body" => NULL));
					exit;
					}
					$applicant_dob = $student_details[0]['mother_dob'];
				}
				if(!empty($student_details[0]['guardian'] == 'guardian')){
					$applicant_name = explode(' ',$student_details[0]['parent_name']);
					$applicant_first_name =$applicant_name[0];
					$applicant_last_name =$applicant_name[1];
					$applicant_gender = 'M';
					if(empty($student_details[0]['guardian_dob'])){
						echo json_encode(array("status_code" => "400", "msg" => "Guardian dob is empty please contact to center.", "body" => NULL));
					exit;
					}
					$applicant_dob = $student_details[0]['guardian_dob'];
				}
				$contact_no =(!empty($student_details[0]['father_mobile_contact_no']))?$student_details[0]['father_mobile_contact_no']:$student_details[0]['mother_mobile_contact_no'];
				if(empty($contact_no)){
					echo json_encode(array("status_code" => "400", "msg" => "MObile number is empty please contact to center.", "body" => NULL));
				exit;
				}
				$email_id =(!empty($student_details[0]['father_email_id']))?$student_details[0]['father_email_id']:$student_details[0]['mother_email_id'];
				if(empty($email_id)){
					echo json_encode(array("status_code" => "400", "msg" => "Email id is empty please contact to center.", "body" => NULL));
				exit;
				}
				$student_dob = date("d-m-Y",strtotime($student_details[0]['dob']));
				if(empty($student_dob)){
					echo json_encode(array("status_code" => "400", "msg" => "Student dob is empty please contact to center.", "body" => NULL));
				exit;
				}
				
				$profession =(!empty($student_details[0]['father_prof']))?$student_details[0]['father_prof']:$student_details[0]['mother_prof'];
				if(empty($profession)){
					echo json_encode(array("status_code" => "400", "msg" => "Profession is empty please contact to center.", "body" => NULL));
				exit;
				}
				$paytm_payment = array();
				$paytm_payment['center_id'] = $response_headers['centerid'];
				$paytm_payment['student_id'] = $response_headers['studentid'];
				$paytm_payment['zone_id'] = $response_headers['zoneid'];
				$paytm_payment['academic_year_id'] = $response_headers['academicyear'];
				$paytm_payment['course_id'] = $response_headers['courseid'];
				$paytm_payment['batch_id'] = $response_headers['batchid'];
				$paytm_payment['amount'] = $total_sum;
				$paytm_payment['installment_id'] = $installment_id;
				$paytm_payment['payment_date'] = date("Y-m-d H:i:s");
				$paytm_payment['checksum_value'] = "";
				$paytm_payment['status'] = 'pending';
				$paytm_payment['mode'] = 'financepeer';
				$result = $this->parentsapimodel->insertData('tbl_paytmpayment', $paytm_payment, 1);
				//print_r($result);exit;
				if(!empty($result)){
					$data = array();
					$data['isTest'] = "true";
					$data['student_first_name'] = $student_details[0]['student_first_name'];
					$data['student_last_name'] = $student_details[0]['student_last_name'];
					$data['student_gender'] = "M";
					$data['student_dob'] =  $student_dob;
					$data['institutes_id'] = $student_details[0]['instituteId'];
					$data['course_id'] = $student_details[0]['courseId'];
					$data['institutes_branch_id'] = $student_details[0]['branchId'];
					$data['loan_amount'] = "$total_sum";
					$data['requested_tenure'] = $response_headers['tenure'];
					$data['roll_no'] = $response_headers['studentid'];
					$data['applicant_first_name'] = $applicant_first_name;
					$data['applicant_last_name'] = $applicant_last_name;
					$data['applicant_gender'] = $applicant_gender;
					$data['applicant_dob'] = $applicant_dob;
					$data['mobile'] = $contact_no;
					$data['email'] = $email_id;
					$data['aadhar_no'] = $student_details[0]['aadhar_no'];
					$data['marital_status'] = $student_details[0]['marital_status'];
					$data['pan_no'] = $student_details[0]['pan_no'];
					$data['profession'] = $profession;
					$data['employer_name'] = 'sunil' ;
					$data['annual_income'] = $student_details[0]['annual_income'] ;
					$data['is_current_address_same_as_kyc_address'] = 'Y';
					$data['kyc_address'] = $student_details[0]['present_address'];
					$data['kyc_address_pin'] = $student_details[0]['pincode'];
					$data['kyc_address_country'] = $student_details[0]['country_name'];
					$data['kyc_address_state'] = $student_details[0]['state_name'];
					$data['kyc_address_city'] = $student_details[0]['city_name'];
					$data['current_residence_type'] = 'O';
					$data['current_address'] = $student_details[0]['present_address'];
					$data['current_address_state'] = $student_details[0]['state_name'];
					$data['current_address_city'] = $student_details[0]['city_name'];
					$data['current_address_pin'] = $student_details[0]['pincode'];
					$data['is_current_address_same_as_permanent_address'] = 'Y';
					$data['permanent_residence_type'] = 'O';
					$data['permanent_address'] = $student_details[0]['present_address'];
					$data['permanent_address_city'] = $student_details[0]['city_name'];
					$data['permanent_address_state'] = $student_details[0]['state_name'];
					$data['permanent_address_pin'] = $student_details[0]['pincode'];
					$data['office_address'] = "Andheri Mumbai Maharashtra 400069";
					$data['office_address_city'] = "Mumbai";
					$data['office_address_state'] = "Maharashtra";
					$data['office_address_pin'] = "400069";

					$data['lead_id'] = "$result";
					$data['action'] = 'LEAD-CREATE';
					 //print_r($data);exit;
					$url = 'https://financepeer.co/uat/api/partner/lead-transfer/';
					$data_string = json_encode($data);
					$ch = curl_init($url);                                                                      
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json',
						'Content-Length: ' . strlen($data_string),
						'client-id:MONTANA_UAT_2019_01',
						'client-secret:4PKXgK7k6VfiEKBEY2YwUEUsKQbHdR1l',
						'Content-Type:application/json',
						)                                                                       
					);
					$push_data_result = curl_exec ($ch);
					$encodedarray = json_decode($push_data_result);
					//print_r($encodedarray);exit;
					if($encodedarray->status == 'success'){
						$status_data['status'] = $encodedarray->status;
						$status_data['message'] = $encodedarray->message;
						$status_data['meta_data'] = $encodedarray->meta_data;
						$status_data['updated_on'] = date("Y-m-d H:m:i");
						$updateFinancepeer = $this->parentsapimodel->updateFinancepeerRecord("tbl_paytmpayment", $status_data, "id='".$result."' ");
						if($updateFinancepeer){
							$body =array();
							$body['lead_id'] =$encodedarray->lead_id;
							$body['meta_data'] =$encodedarray->meta_data;
							echo json_encode(array("status_code" => "200", "msg" => $encodedarray->message, "body" => $body));
							exit;
						}else{
							echo json_encode(array("status_code" => "400", "msg" => "Problem in update.", "body" => NULL));
							exit;
						}
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "Lead push failed.", "body" => NULL));exit;
					}
				}else{
				echo json_encode(array("status_code" => "400", "msg" => "Problem in data insertion.", "body" => NULL));
				exit;
				}	
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}	
	function pushLeadDocuments(){
		$response_headers = $this->getallheaders_new();
		if(empty($_POST['lead_id'])){
			echo json_encode(array("status_code" => "400", "msg" => "Lead id not provided.", "body" => NULL));
			exit;
		}
		// if(empty($_POST['meta_data'])){
		// 	echo json_encode(array("status_code" => "400", "msg" => "Meta data not provided.", "body" => NULL));
		// 	exit;
		// }
		$result = $this->parentsapimodel->getdata('tbl_paytmpayment', 'id,pan,aadhaar,bank_statement',"id='".$_POST['lead_id']."'");
		//print_r($result);exit;
		if(!empty($result)){
			$upload_path=DOC_ROOT."/images/FinancePeerDocuments/";
			if(!file_exists($upload_path)) 
			{
				mkdir($upload_path, 0777, true);
			}
			if(isset($_FILES) && isset($_FILES["pan"]["name"]) && !empty($_FILES["pan"]["name"])){
				$config['upload_path'] = DOC_ROOT."/images/FinancePeerDocuments/";
				$config['max_size']       = '10000';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
				$config['file_name']     = md5(uniqid("100_ID", true));
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("pan")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("status_code" => "400", "msg" => strip_tags($image_error['error']), "body" => NULL));
					exit;
				}
				else{
					$image_data = array('upload_data' => $this->upload->data());
					$pan = $image_data['upload_data']['file_name'];
				}
				if (!empty($result[0]['pan'])) {
					@unlink(DOC_ROOT . "/images/FinancePeerDocuments/".$result[0]['pan']);
				}
			}
			if(isset($_FILES) && isset($_FILES["aadhaar"]["name"]) && !empty($_FILES["aadhaar"]["name"])){
				$config['upload_path'] = DOC_ROOT."/images/FinancePeerDocuments/";
				$config['max_size']       = '10000';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
				$config['file_name']     = md5(uniqid("100_ID", true));
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("aadhaar")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("status_code" => "400", "msg" => strip_tags($image_error['error']), "body" => NULL));
					exit;
				}
				else{
					$image_data = array('upload_data' => $this->upload->data());
					$aadhaar = $image_data['upload_data']['file_name'];
				}
				if (!empty($result[0]['aadhaar'])) {
					@unlink(DOC_ROOT . "/images/FinancePeerDocuments/".$result[0]['aadhaar']);
				}
			}
			if(isset($_FILES) && isset($_FILES["bank_statement"]["name"]) && !empty($_FILES["bank_statement"]["name"])){
				$config['upload_path'] = DOC_ROOT."/images/FinancePeerDocuments/";
				$config['max_size']       = '10000';
				$config['allowed_types'] = 'gif|jpg|png|jpeg|pdf';
				$config['file_name']     = md5(uniqid("100_ID", true));
				$this->load->library('upload', $config);
				if (!$this->upload->do_upload("bank_statement")){
					$image_error = array('error' => $this->upload->display_errors());
					echo json_encode(array("status_code" => "400", "msg" => strip_tags($image_error['error']), "body" => NULL));
					exit;
				}
				else{
					$image_data = array('upload_data' => $this->upload->data());
					$bank_statement = $image_data['upload_data']['file_name'];
				}
				if (!empty($result[0]['bank_statement'])) {
					@unlink(DOC_ROOT . "/images/FinancePeerDocuments/".$result[0]['bank_statement']);
				}
			}
		}
		$dataupdate =array();
		$dataupdate['pan'] = (!empty($pan)) ? $pan : '';
		$dataupdate['aadhaar'] = (!empty($aadhaar)) ? $aadhaar : '';
		$dataupdate['bank_statement'] =  (!empty($bank_statement)) ? $bank_statement : '';
		$dataupdate['meta_data'] = (!empty($_POST['meta_data'])) ? $_POST['meta_data'] : '';
		$dataupdate['updated_on'] = date("Y-m-d H:m:i");
		$updateFinancepeer = $this->parentsapimodel->updateFinancepeerRecord("tbl_paytmpayment", $dataupdate, "id='".$_POST['lead_id']."' ");
		if($updateFinancepeer){
			$get_content = $this->parentsapimodel->getdata('tbl_paytmpayment', 'pan,aadhaar,bank_statement',"id='".$_POST['lead_id']."'");
			$data_pan = "";
			$data_aadhaar = "";
			$data_bank_statement = "";
		
			if(!empty($get_content[0]['pan'])){
				$pan_path = DOC_ROOT.'/images/FinancePeerDocuments/'.$get_content[0]['pan'];
				$pan_encode = fread(fopen($pan_path,'r'),filesize($pan_path));
				$data_pan = base64_encode($pan_encode);
			}
			if(!empty($get_content[0]['aadhaar'])){
				$aadhaar_path = DOC_ROOT.'/images/FinancePeerDocuments/'.$get_content[0]['aadhaar'];
				$aadhaar_encode = fread(fopen($aadhaar_path,'r'),filesize($aadhaar_path));
				$data_aadhaar = base64_encode($aadhaar_encode);
			}
			if(!empty($get_content[0]['bank_statement'])){
				$bank_statement_path = DOC_ROOT.'/images/FinancePeerDocuments/'.$get_content[0]['bank_statement'];
				$bank_statement_encode = fread(fopen($bank_statement_path,'r'),filesize($bank_statement_path));
				$data_bank_statement = base64_encode($bank_statement_encode);  
			}

			$data = array();
			$data['pan'] = (!empty($data_pan)) ? $data_pan : '';
			$data['aadhaar'] = (!empty($data_aadhaar)) ? $data_aadhaar : '';
			$data['bank_statement'] = (!empty($data_bank_statement)) ? $data_bank_statement : '';
			$data['lead_id'] = $_POST['lead_id'];
			$data['meta_data'] = (!empty($_POST['meta_data'])) ? $_POST['meta_data'] : "1920";
			
			$url = 'https://financepeer.co/uat/api/partner/upload-documents/';
			$data_string = json_encode($data);
			// print_r($data['pan']);exit;
			$ch = curl_init($url);                                                                      
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'client-id:MONTANA_UAT_2019_01',
					'client-secret:4PKXgK7k6VfiEKBEY2YwUEUsKQbHdR1l',
					'Content-Type:application/x-www-form-urlencoded',
				)                                                                       
			);
			$push_data_result = curl_exec ($ch);
			//echo $push_data_result;exit;
			$encodedarray = json_decode($push_data_result);
			if($encodedarray->status == 'success'){
				$status_data['message'] = $encodedarray->message;
				$status_data['meta_data'] = (!empty($encodedarray->meta_data)) ? $encodedarray->meta_data : '1920';
				$status_data['updated_on'] = date("Y-m-d H:m:i");
				$updateFinancepeer = $this->parentsapimodel->updateFinancepeerRecord("tbl_paytmpayment", $status_data, "id='".$_POST['lead_id']."' ");
				if($updateFinancepeer){
					$body =array();
					$body['lead_id'] =$encodedarray->lead_id;
					echo json_encode(array("status_code" => "200", "msg" => $encodedarray->message, "body" => $body));
					exit;
				}else{
					echo json_encode(array("status_code" => "400", "msg" => "Problem in message update.", "body" => NULL));
					exit;
				}
			}
		}else{
			echo json_encode(array("status" => "failed", "message" => "Problem in update.", "body" => NULL));
			exit;
		}
	}
	function sendNotifications(){
		$response_headers = $this->getallheaders_new();
			
		if(empty($response_headers['status'])){
			echo json_encode(array("status_code" => "400", "msg" => "Status not provided.", "body" => NULL));
			exit;
		}
		if(empty($response_headers['description'])){
			echo json_encode(array("status_code" => "400", "msg" => "Description not provided.", "body" => NULL));
			exit;
		}
		if(empty($response_headers['lead_id'])){
			echo json_encode(array("status_code" => "400", "msg" => "Lead id not provided.", "body" => NULL));
			exit;
		}
		if(empty($response_headers['meta_data'])){
			echo json_encode(array("status_code" => "400", "msg" => "Meta data not provided.", "body" => NULL));
			exit;
		}

		//get installment details
		$status_details = $this->parentsapimodel->getdata("tbl_paytmpayment", "id", "id='".$response_headers['lead_id']."' ");
		if(!empty($status_details)){
			//print_r($status_details[0]['id']);exit;
			$status_data = array();
			$status_data['status'] = $response_headers['status'];
			$status_data['description'] = $response_headers['description'];
			$status_data['meta_data'] = $response_headers['meta_data'];
			$status_data['updated_on'] = date("Y-m-d H:m:i");
			// $status_data['updated_by'] = $response_headers['meta_data'];
			
			$updateFinancepeer = $this->parentsapimodel->updateFinancepeerRecord("tbl_paytmpayment", $status_data, "id='".$response_headers['lead_id']."' ");
			//print_r($updateFinancepeer);exit;
			if($updateFinancepeer){
				$status = explode('-',$response_headers['status']);
			//print_r($status[0]);exit;
				if(trim($status[0]) == 'ELS301'){
					echo json_encode(array("status" => "success", "message" => "invoice generated.", "body" => NULL));
					exit;
				}
			echo json_encode(array("status" => "success", "message" => "Updated successfully.", "error_code" => "", "error" => "NA", "response_time_stamp" => date('Y-M-D:H:m:s')));
			exit;
			}else{
			echo json_encode(array("status" => "failed", "message" => "Problem in update.", "body" => NULL));
			exit;
			}

		}else{
			echo json_encode(array("status" => "failed", "message" => "Invalid Lead id found.", "body" => NULL));
			exit;
		}
	}
	
	//For getting selected timetable details
	function viewTimetableDetails(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['timetableid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide timetable id.", "body" => NULL));
						exit;
					}
					
					$getTimeTableDetails = $this->parentsapimodel->getdata("tbl_timetable_master", "*","timetable_id='".$response_headers['timetableid']."' ");
					if(!empty($getTimeTableDetails)){
						$result = array();
						$getTimeTableDetails[0]['cover_image_path'] = (!empty($getTimeTableDetails[0]['cover_image'])) ? FRONT_ROOT_URL. "/images/timetable_images/".$getTimeTableDetails[0]['cover_image'] : '';
						$result['timetable_details'] = $getTimeTableDetails;
						
						$getTimeTableVideos = $this->parentsapimodel->getdata("tbl_timetable_videos", "*","timetable_id='".$response_headers['timetableid']."' ");
						$result['timetable_video'] = (!empty($getTimeTableVideos) && $getTimeTableVideos != false)?$getTimeTableVideos:NULL;
						
						$getTimeTableDocuments = $this->parentsapimodel->getdata("tbl_timetable_documents", "*","timetable_id='".$response_headers['timetableid']."' ");
						//echo "<pre>";print_r($getTimeTableDocuments);exit;
						if(!empty($getTimeTableDocuments)){
							for($i=0; $i < sizeof($getTimeTableDocuments); $i++){
								if($getTimeTableDocuments[$i]['doc_type'] == 'image'){
									$getTimeTableDocuments[$i]['image_path'] = FRONT_ROOT_URL. "/images/timetable_images/".$getTimeTableDocuments[$i]['doc_file'];
									$getTimeTableDocuments[$i]['doc_path'] = "";
								}else{
									$getTimeTableDocuments[$i]['image_path'] = "";
									$getTimeTableDocuments[$i]['doc_path'] = FRONT_ROOT_URL. "/images/timetable_images/".$getTimeTableDocuments[$i]['doc_file'];
								}
							}
						}
						
						$result['timetable_documents'] =(!empty($getTimeTableDocuments) && $getTimeTableDocuments != false)?$getTimeTableDocuments:NULL;
						
						echo json_encode(array("status_code" => "200", "msg" => "Time table found", "body" => $result));
						exit;
					}else{
						echo json_encode(array("status_code" => "400", "msg" => "Time table not found", "body" => NULL));
						exit;
					}
					
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}
	//For showing list of document folders
	function viewDocumentFolders(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					

					// shiv commented this code on 8-6-2020
					/*
					$getCoursesCategories = $this->parentsapimodel->getdata("tbl_center_user_courses", "*","user_id='".$response_headers['userid']."' ");
					//echo "<pre>";print_r($getCoursesCategories);exit;
					$course_array = array();
					$category_array = array();
					for($i=0; $i < sizeof($getCoursesCategories); $i++){
						$course_array[] = $getCoursesCategories[$i]['course_id'];
						$category_array[] = $getCoursesCategories[$i]['category_id'];
					}
					//echo "<pre>";print_r($category_array);exit;
					$course_implode = implode(",", $course_array);
					$category_implode = implode(",", $category_array);
					
					if(empty($course_implode)){
						echo json_encode(array("status_code" => "400", "msg" => "Course not allocated to login user.", "body" => NULL));
						exit;
					}
					
					if(empty($category_implode)){
						echo json_encode(array("status_code" => "400", "msg" => "Category not allocated to login user.", "body" => NULL));
						exit;
					}*/
					
					//exit;
					// $current_date = date("Y-m-d");
					// $condition = "";
					// // $condition = "fd.parent_show='Yes' && i.status='Active' && c.category_id  IN (".$category_implode.") && ce.center_id ='".$response_headers['centerid']."' && c.course_id IN (".$course_implode.")  ";
					// $condition = "fd.parent_show='Yes' && i.status='Active' && c.category_id  IN (".$response_headers['categoryid'].") && ce.center_id ='".$response_headers['centerid']."' && c.course_id IN (".$response_headers['courseid'].")  ";

					// $condition .=" && (ce.start_date <= '".$current_date."'  && ce.end_date >= '".$current_date."' )";


					// $page = (!empty($response_headers['page'])) ? $response_headers['page'] : 0;

					$condition = "";
					// start  shiv code for adding 10 extra to foldercenter date on 1-7-2020  
					
					$current_date = date("Y-m-d",strtotime('+10 days'));
					$end_date = date("Y-m-d",strtotime('-10 days'));
					$condition = "fd.parent_show='Yes' && i.status='Active' AND c.status='Active' AND c.category_id  IN (".$response_headers['categoryid'].") AND ce.center_id ='".$response_headers['centerid']."' AND c.course_id IN (".$response_headers['courseid'].")  ";
					$condition .=" AND (ce.start_date <= '".$current_date."'  AND ce.end_date >= '".$end_date."' )";
					// end  shiv code for adding 10 extra to foldercenter date on 1-7-2020  
					// shiv commented
					// $condition = "i.status='Active' && c.category_id  IN (".$category_implode.") && ce.center_id ='".$response_headers['centerid']."' && c.course_id IN (".$course_implode.")  ";
					// $condition .=" && (ce.start_date <= '".$current_date."'  && ce.end_date >= '".$current_date."' )";
					$page = (!empty($response_headers['page'])) ? $response_headers['page'] : 0;

					if(!empty($_POST['search_key']) && isset($_POST['search_key'])){
						$condition .=" AND  i.folder_name like '%".$_POST['search_key']."%' ";
					}

					
					$getUserDocumentFolder = $this->parentsapimodel->getUserDocumentFolder($condition, $page);
					// echo "<pre>";print_r($getUserDocumentFolder);exit;
					
					/*if(!empty($getUserDocumentFolder['query_result'])){
						for ($i = 0; $i < sizeof($getUserDocumentFolder['query_result']); $i++) {
							
						}
					}*/
					
					//echo "<pre>";print_r($getDetails);exit;
					//exit;
					if (!empty($getUserDocumentFolder['query_result'])) {
						echo json_encode(array("status_code" => "200", "msg" => "Folder found successfully.", "body" => $getUserDocumentFolder));
						exit;
					} else if($page > 0 && empty($getUserDocumentFolder['query_result'])){
							echo json_encode(array("status_code" => "401", "msg" => "No more folder found", "body" => NULL));
							exit;
					}else {
						echo json_encode(array("status_code" => "400", "msg" => "No folder found.", "body" => NULL));
						exit;
					}
					
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}
	
	function viewTimetables(){
	    $query = "";
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					// shiv commented this code on 8-6-2020 
					/*$getCoursesCategories = $this->parentsapimodel->getdata("tbl_center_user_courses", "*","user_id='".$response_headers['userid']."' ");
			
					$course_array = array();
					$category_array = array();
					for($i=0; $i < sizeof($getCoursesCategories); $i++){
						$course_array[] = $getCoursesCategories[$i]['course_id'];
						$category_array[] = $getCoursesCategories[$i]['category_id'];
					}
					$course_implode = implode(",", $course_array);
					$category_implode = implode(",", $category_array);
					
					if(empty($course_array)){
						echo json_encode(array("status_code" => "400", "msg" => "Course not allocated to login user.", "body" => NULL));
						exit;
					}
					
					if(empty($category_array)){
						echo json_encode(array("status_code" => "400", "msg" => "Category not allocated to login user.", "body" => NULL));
						exit;
					}*/
					
					//exit;
					
					$getSeeBeforeAfter = $this->parentsapimodel->getdata("tbl_centers", "*","center_id='".$response_headers['centerid']."' ");
					$see_before = $getSeeBeforeAfter[0]['can_see_timetable_before'];
					$see_after = $getSeeBeforeAfter[0]['can_see_timetable_after'];
					
					$condition = "";
					// 	$condition .= "  i.course_id IN (".$course_implode.")  AND i.category_id  IN (".$category_implode.") AND i.center_id='".$response_headers['centerid']."'  AND  i.start_date BETWEEN DATE_ADD(CURDATE(), INTERVAL -".$see_before." DAY) AND DATE_ADD(CURDATE(), INTERVAL ".$see_after." DAY)";
					$condition .= "  i.course_id IN (".$response_headers['courseid'].")  && i.category_id  IN (".$response_headers['categoryid'].") && i.center_id='".$response_headers['centerid']."'  &&  CURDATE() >= DATE_ADD(i.start_date, INTERVAL -".$see_before." DAY) AND CURDATE() <= DATE_ADD(i.end_date, INTERVAL ".$see_after." DAY) && parent_show ='Yes' && i.status ='Active'  &&  i.academic_year_id ='".$response_headers['academicyear']."' ";
					//$getDetails = $this->parentsapimodel->getdata_orderby("tbl_newsletters", "*", $condition, 'order by newsletter_id desc');
					$page = (!empty($response_headers['page'])) ? $response_headers['page'] : 0;

					
					if(!empty($_POST['search_key']) && isset($_POST['search_key'])){
						$condition .=" &&  t.timetable_title like '%".$_POST['search_key']."%' ";
					}
					
					$getUserTimeTables = $this->parentsapimodel->getUserTimeTables($condition, $page);
				// 	echo = $this->db->last_query();
					//echo "<pre>";print_r($getUserTimeTables);exit;
					if(!empty($getUserTimeTables['query_result'])){
						for ($i = 0; $i < sizeof($getUserTimeTables['query_result']); $i++) {
							$getUserTimeTables['query_result'][$i]->cover_image_path = (!empty($getUserTimeTables['query_result'][$i]->cover_image)) ? FRONT_ROOT_URL. "/images/timetable_images/".$getUserTimeTables['query_result'][$i]->cover_image : '';
							$getSeeBeforeAfter = $this->parentsapimodel->getdata("tbl_centers", "*","center_id='".$response_headers['centerid']."' ");
							//echo "<pre>";print_r($getSeeBeforeAfter);exit;
							
							$see_before = $getSeeBeforeAfter[0]['can_see_timetable_before'];
							$see_after = $getSeeBeforeAfter[0]['can_see_timetable_after'];
							
							$see_before_date = date('Y-m-d', strtotime($getUserTimeTables['query_result'][$i]->start_date. ' - '.$see_before.' days'));
							$see_after_date = date('Y-m-d', strtotime($getUserTimeTables['query_result'][$i]->end_date. ' + '.$see_after.' days'));
							//echo "see_before_date: ".$see_before_date." see_after_date: ".$see_after_date;exit;
							$current_date = date("Y-m-d");
							if (($current_date >= $see_before_date) && ($current_date <= $see_after_date)){
								$getUserTimeTables['query_result'][$i]->is_timetable_visible = true;
							}else{
								$getUserTimeTables['query_result'][$i]->is_timetable_visible = false;
							}
							
						}
					}
					
					//echo "<pre>";print_r($getDetails);exit;
					//exit;
					if (!empty($getUserTimeTables['query_result'])) {
						echo json_encode(array("status_code" => "200", "msg" => "Timetables found successfully.", "body" => $getUserTimeTables));
						exit;
					} else if($page > 0 && empty($getUserTimeTables['query_result'])){
							echo json_encode(array("status_code" => "401", "msg" => "No more timetable found", "body" => NULL));
							exit;
					}else {
						echo json_encode(array("status_code" => "400", "msg" => "No timetable found.", "body" => NULL));
						exit;
					}
					
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}
	
	//For showing documents in selected folder
	function viewFolderDocuments(){
		$response_headers = $this->getallheaders_new();
		if (isset($response_headers['utoken']) && !empty($response_headers['utoken'])) {
			$utoken = $response_headers['utoken'];
			$response = $this->checkUtoken($utoken);
			if ($response == true) {	
				if (!empty($response_headers['centerid']) ) {
				
					if(empty($response_headers['userid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide userid.", "body" => NULL));
						exit;
					}
					
					if(empty($response_headers['folderid'])){
						echo json_encode(array("status_code" => "400", "msg" => "Please provide folderid.", "body" => NULL));
						exit;
					}
					
					
					$condition = "";
					$condition = " i.document_folder_id='".$response_headers['folderid']."' && i.status ='Active' && i.parent_show = 'Yes' ";
					$page = (!empty($response_headers['page'])) ? $response_headers['page'] : 0;
					
					$getUserFolderDocuments = $this->parentsapimodel->getUserFolderDocuments($condition);
					//echo "<pre>";print_r($getUserFolderDocuments);exit;
					
					if(!empty($getUserFolderDocuments['query_result'])){
						for ($i = 0; $i < sizeof($getUserFolderDocuments['query_result']); $i++) {
							
							if(!empty($getUserFolderDocuments['query_result'][$i]->doc_type) && $getUserFolderDocuments['query_result'][$i]->doc_type == 'video'){
								$getUserFolderDocuments['query_result'][$i]->video_path = $getUserFolderDocuments['query_result'][$i]->document_field_value;
								$getUserFolderDocuments['query_result'][$i]->pdf_path = "";
								$getUserFolderDocuments['query_result'][$i]->doc_path = "";
								$getUserFolderDocuments['query_result'][$i]->img_path = "";
							}else if(!empty($getUserFolderDocuments['query_result'][$i]->doc_type) && $getUserFolderDocuments['query_result'][$i]->doc_type == 'doc'){
								$getUserFolderDocuments['query_result'][$i]->video_path = "";
								$getUserFolderDocuments['query_result'][$i]->pdf_path = FRONT_ROOT_URL.'/images/folder_documents/'.$getUserFolderDocuments['query_result'][$i]->document_field_value;
								$getUserFolderDocuments['query_result'][$i]->doc_path = "";
								$getUserFolderDocuments['query_result'][$i]->img_path = "";
							}else if(!empty($getUserFolderDocuments['query_result'][$i]->doc_type) && $getUserFolderDocuments['query_result'][$i]->doc_type == 'doc_excel'){
								$getUserFolderDocuments['query_result'][$i]->video_path = "";
								$getUserFolderDocuments['query_result'][$i]->pdf_path = "";
								$getUserFolderDocuments['query_result'][$i]->doc_path = FRONT_ROOT_URL.'/images/folder_documents/'.$getUserFolderDocuments['query_result'][$i]->document_field_value;
								$getUserFolderDocuments['query_result'][$i]->img_path = "";
							}else if(!empty($getUserFolderDocuments['query_result'][$i]->doc_type) && $getUserFolderDocuments['query_result'][$i]->doc_type == 'image'){
								$getUserFolderDocuments['query_result'][$i]->video_path = "";
								$getUserFolderDocuments['query_result'][$i]->pdf_path = "";
								$getUserFolderDocuments['query_result'][$i]->img_path = FRONT_ROOT_URL.'/images/folder_documents/'.$getUserFolderDocuments['query_result'][$i]->document_field_value;
								$getUserFolderDocuments['query_result'][$i]->doc_path = "";
							}
							
						}
					}
					
					//echo "<pre>";print_r($getDetails);exit;
					//exit;
					if (!empty($getUserFolderDocuments['query_result'])) {
						echo json_encode(array("status_code" => "200", "msg" => "Documents found successfully.", "body" => $getUserFolderDocuments));
						exit;
    					}else if( $page > 0  && empty($getUserFolderDocuments['query_result'])){
    							echo json_encode(array("status_code" => "401", "msg" => "No more documents found", "body" => NULL));
    							exit;
    					}else {
						echo json_encode(array("status_code" => "400", "msg" => "No documents found.", "body" => NULL));
						exit;
					}
					
				}else {
					echo json_encode(array("status_code" => "400", "msg" => "Center ID not provided", "body" => NULL));
					exit;
				}
			}else{
				echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
				exit;
			}
		}else{
			echo json_encode(array("status_code" => "402", "msg" => "Authentication failed.", "body" => NULL));
			exit;
		}
	}
}

?>
