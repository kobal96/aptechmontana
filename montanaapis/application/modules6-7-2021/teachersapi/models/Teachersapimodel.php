<?PHP
class Teachersapimodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
	 	$this->db->insert($tbl_name,$data_array);
	 	$result_id = $this->db->insert_id();
	 	
	 	/*echo $result_id;
	 	exit;*/
	 	
	 	if($sendid == 1)
	 	{
	 		//return id
	 		return $result_id;
	 	}
	}
	
	function insertBatchData($tbl_name,$data_array,$sendid = NULL)
	{
	 	$this->db->insert_batch($tbl_name,$data_array);
	 	$result_id = $this->db->insert_id();
	 	
	 	/*echo $result_id;
	 	exit;*/
	 	
	 	if($sendid == 1)
	 	{
	 		//return id
	 		return $result_id;
	 	}
	}
	
	function check_utoken($user_id) {
		$this -> db -> select('u.*');
		$this -> db -> from('tbl_admin_users as u');
		$this -> db -> where('u.user_id',$user_id);
		
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
	}
	function getActivityRecords($condition,$page){
	    $rows = 10;
		$page = $rows * $page;
		$this -> db -> select('*');
		$this -> db -> from('tbl_activity as i');
		$this -> db -> join('tbl_activity_resources as ar', 'i.activity_id  = ar.activity_id', '');
		
		$this->db->where("($condition)");
		$this->db->order_by('i.activity_id','desc');
			$this->db->limit($rows,$page);
		// $this->db->group_by("i.fees_level_id");
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();;
		}
	}

	function getActivityResponseRecords($condition){
		// echo "ejllo";
		$this -> db -> select('i.*,ar.*,as.student_activity_comment,as.teacher_activity_comment,sm.student_first_name,sm.student_last_name,sm.profile_pic,as.resource_type as student_resource_type,as.resource_data as student_resource_data,as.created_on as student_submission_date,as.updated_on as teacher_submission_date ,au.first_name,au.last_name,au.user_name,au.profile_pic as teacher_profile_pic,sm.student_id');

		$this -> db -> from('tbl_activity as i');
		$this -> db -> join('tbl_activity_resources as ar', 'i.activity_id  = ar.activity_id', '');
		$this -> db -> join('tbl_activity_submission as as', 'as.activity_id  = i.activity_id','');	
		$this -> db -> join('tbl_student_master as sm', 'sm.student_id  = as.student_id','');
		$this -> db -> join('tbl_admin_users as au', 'au.user_id  = i.teacher_id','');

		$this->db->where("($condition)");
		$this->db->order_by('i.activity_id','desc');
		$query = $this -> db -> get();
		// echo $this->db->last_query();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();;
		}
	}


	function getActivityStudentSubmitted($condition){
		// $rows = 10;
		// $page = $rows * $page;
		$this -> db -> select('i.*,ar.*,asm.*,sm.student_first_name,sm.student_last_name,sm.profile_pic,as1.created_on as submission_date');
		$this -> db -> from('tbl_activity as i');
		$this -> db -> join('tbl_activity_resources as ar', 'i.activity_id  = ar.activity_id', '');
		$this -> db -> join('tbl_activity_student_mapping as asm', 'i.activity_id  = asm.activity_id','');
		$this -> db -> join('tbl_student_master as sm', 'sm.student_id  = asm.student_id','');
		$this -> db -> join('tbl_activity_submission as as1','i.activity_id  = as1.activity_id AND asm.student_id = as1.student_id','left');

		$this->db->where("($condition)");
		$this->db->order_by('i.activity_id','desc');
			// $this->db->limit($rows,$page);
		$this->db->group_by("asm.student_id");
		$query = $this -> db -> get();
		// echo $this->db->last_query();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();;
		}
	}

	
	function login_check($condition) {
		// $this -> db -> select('user_id, zone_id, center_id, user_type,contact_no ,first_name, last_name, user_name, role_id, is_franchise,profile_pic,status');
		$this -> db -> select('user_id, zone_id, center_id, user_type,contact_no,first_name, last_name, user_name, role_id, is_franchise,profile_pic,status');
		
		$this -> db -> from('tbl_admin_users');
		$this -> db -> where("($condition)");
		
		$query = $this -> db -> get();
		//print_r($this->db->last_query());
		//exit;
	 
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
	}
  
	function fetch_otp($mobile,$country_code){
		$sql = $this->db->query("select * from tbl_otp_check where mobile = ".$this->db->escape($mobile).' And country_code ='.$this->db->escape($country_code));
		if($sql->num_rows()>0){
			return $sql->result();
		}else{
			return null;
		}
	}
  
	function generate_otp($mobile,$country_code,$otp){
		$sql = $this->db->query("select * from tbl_otp_check where mobile = ".$this->db->escape($mobile).' And country_code ='.$this->db->escape($country_code) );
		
		$data = array(
               "mobile"=>$mobile,
               "country_code"=>$country_code,
               "otp" => $otp,
               "time_stamp"=>date('Y-m-d H:i:s')
		);
    
		if($sql->num_rows()>0){
			$this->updatedataotp($data, "tbl_otp_check", "mobile", $mobile,"country_code", $country_code);
		}else{
			$this->insertData("tbl_otp_check",$data,1);
		}
		
		return true;
	}
  
	function updatedataotp($data,$table,$column1,$value1,$column2,$value2){
		$this->db->where($column1,$value1);
		$this->db->where($column2,$value2);
		$this->db->update($table,$data);
	}
	
	function getdata($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return null;
		}
	}

	function getMonths($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition order by cast(month_name as unsigned)");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return null;
		}
	}

	function getdata1($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition group by month_id");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return null;
		}
	}

	function getdataalbum($col,$fld,$col1,$fld1,$col2,$fld2,$col3,$fld3){
		//echo "Select $fields from $table where $condition";exit;
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_assign_album as i');
		$this -> db -> where($col, $fld);
		$this -> db -> where($col1, $fld1);
		$this -> db -> where($col2, $fld2);
		$this -> db -> where_in($col3, $fld3);
		$sql = $this -> db -> get();


		// $sql = $this->db->query("Select $fields from $table where $condition");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return null;
		}
	}
	
	function getdata_orderby($table, $fields, $condition = '1=1', $order_by){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition $order_by");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return null;
		}
	}
	
	function getdata_group_order_by($table, $fields, $condition = '1=1', $group_by="", $order_by=""){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition $group_by $order_by");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return null;
		}
	}

	function getdata_group_by($table, $fields, $condition = '1=1', $group_by=""){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition $group_by");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return null;
		}
	}
	
	function getdata_orderby_limit1($table, $fields, $condition = '1=1', $order_by){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition $order_by limit 1");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return null;
		}
	}
	
	function getdata_orderby_limit($table, $fields, $condition = '1=1', $order_by, $page){
	
		$rows = 10;
		$page = $rows * $page;
		$limit = $page.",".$rows;
		
		$sql = $this->db->query("Select $fields from $table where $condition $order_by limit $limit");
		//print_r($this->db->last_query());
		//exit;
		
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return null;
		}
	}
    
    
    
	function updateRecord($tbl_name,$datar,$condition)
	{
		//$this -> db -> where($comp_col, $eid);
		$this->db->where("($condition)");
		$this -> db -> update($tbl_name,$datar);
		 
		if ($this->db->affected_rows() > 0){
			return true;
		}else{
			return true;
		} 
	}
	 
	function getFormdata($ID)
	{
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_categories as i');
		$this -> db -> where('i.category_id', $ID);
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1){
			return $query->result();
		}else{
			return null;
		}
	}
	
	
	function delrecord($tbl_name,$tbl_id,$record_id)
	{
		$this->db->where($tbl_id, $record_id);
	    $this->db->delete($tbl_name);
		if($this->db->affected_rows() >= 1)
		{
			return true;
	    }
	    else
	    {
			return null;
	    }
	}
	
	function getRecords($tbl_name, $condition, $page, $default_sort_column=null, $default_sort_order=null, $group_by=null){
		//echo "here...<br/>";
		//print_r($_REQUEST);
		//exit;
		
		$table = $tbl_name;
		$default_sort_column = $default_sort_column;
		$default_sort_order = $default_sort_order;
		
		
		//$rows = 100;
		$rows = 10;
		$page = $rows * $page;
		
		// sort order by column
		$sort = $default_sort_column;  
		$order = $default_sort_order;

		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		
		$this->db->where("($condition)");
	
		$this->db->order_by($sort, "desc");
		if(!empty($group_by)){
			$this->db->group_by($group_by);
		}
		$this->db->limit($rows,$page);
		
		$query = $this -> db -> get();
		
		// print_r($this->db->last_query());
		//exit;
		
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return null;
		}
		
		
		//exit;
	}

	function getRecords1($tbl_name, $condition, $page, $group_by=null){
		//echo "here...<br/>";
		//print_r($_REQUEST);
		//exit;
		
		$table = $tbl_name;
		
		
		//$rows = 100;
		$rows = 10;
		$page = $rows * $page;
		

		$this -> db -> select('i.*,a.album_image_id,a.teacher_id');
		$this -> db -> from('tbl_assign_album as i');
		$this -> db -> join('tbl_album_images as a', 'i.album_id  = a.album_id', 'left');
		
		$this->db->where("($condition)");
		if(!empty($group_by)){
			$this->db->group_by($group_by);
		}
		$this->db->order_by('a.album_image_id','desc');
		$this->db->limit($rows,$page);
		
		$query = $this -> db -> get();
		
		//print_r($this->db->last_query());
		//exit;
		
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return null;
		}
		
		
		//exit;
	}
	
	function get_fcm_data($user_id){
		$query = $this->db->query('Select * from tbl_student_master where student_id = '.$this->db->escape($user_id).' ');
	    
		if($query -> num_rows() >= 1){
				return $query->result_array();
			}else{
				return null;
			}
	}
	
	function get_email_data($eid){
		$query = $this->db->query('Select * from tbl_emailcontents where eid = '.$this->db->escape($eid));
	    
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return null;
		}
	}
	
	function getUserTimeTables($condition, $page){
		
		$rows = 10;
		$page = $rows * $page;

		$this -> db -> select('i.*,t.timetable_title,t.cover_image');
		$this -> db -> from('tbl_assign_timetable as i');
		$this -> db -> join('tbl_timetable_master as t', 'i.timetable_id  = t.timetable_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->order_by("i.timetable_id", "desc");
		$this->db->limit($rows,$page);
		$query = $this -> db -> get();
		
		$this -> db -> select('i.*,t.timetable_title');
		$this -> db -> from('tbl_assign_timetable as i');
		$this -> db -> join('tbl_timetable_master as t', 'i.timetable_id  = t.timetable_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->order_by("i.timetable_id", "desc");	
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return null;
			return array("totalRecords"=>0);
		}
		
	}

	function getCenterGroups($condition){
		$this -> db -> select('i.*,g.group_master_name');
		$this -> db -> from('tbl_center_user_groups as i');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		
		$this->db->where("($condition)");
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return array("query_result" => $query->result());
		}
		
	}

	function getPaymentData($condition){
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_admission_fees as i');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		
		$this->db->where("($condition)");
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return array("query_result" => $query->result());
		}
		
	}

	function isPaymentDone($condition){
		$this -> db -> select('i.is_instalment,i.admission_fees_id');
		$this -> db -> from('tbl_fees_payment_details as f');
		$this -> db -> join('tbl_admission_fees as i', 'i.admission_fees_id  = f.admission_fees_id', 'left');
		
		$this->db->where("($condition)");
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return array("query_result" => $query->result());
		}
		
	}

	function getFeesData($condition){
		
		$this -> db -> select('i.*,c.course_name,g.group_master_name');
		$this -> db -> from('tbl_admission_fees as i');
		$this -> db -> join('tbl_courses as c', 'i.course_id  = c.course_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
		
	}

	function getCenterFeesLevel($condition){
		$this -> db -> select('f.fees_level_name,f.fees_level_id');
		$this -> db -> from('tbl_assign_fees as i');
		$this -> db -> join('tbl_fees_level_master as f', 'i.fees_level_id  = f.fees_level_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->group_by("i.fees_level_id");
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return array("query_result" => $query->result());
		}
		
	}
	
	function getUserDocumentFolder($condition, $page){
		
		$rows = 10;
		$page = $rows * $page;

		$this -> db -> select('i.document_folder_id, i.folder_name');
		$this -> db -> from('tbl_document_folders as i');
		$this -> db -> join('tbl_folder_courses as c', 'i.document_folder_id  = c.document_folder_id', 'left');
		$this -> db -> join('tbl_folder_centers as ce', 'i.document_folder_id  = ce.document_folder_id', 'left');

		$this->db->where("($condition)");
		$this->db->group_by('i.document_folder_id');
		$this->db->order_by('i.document_folder_id', 'desc');
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		$this -> db -> select('i.document_folder_id, i.folder_name');
		$this -> db -> from('tbl_document_folders as i');
		$this -> db -> join('tbl_folder_courses as c', 'i.document_folder_id  = c.document_folder_id', 'left');
		$this -> db -> join('tbl_folder_centers as ce', 'i.document_folder_id  = ce.document_folder_id', 'left');

		$this->db->where("($condition)");
		$this->db->group_by('i.document_folder_id');
		$this->db->order_by('i.document_folder_id', 'desc');
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return null;
			return array("totalRecords"=>0);
		}
		
	}
	
	function getUserFolderDocuments($condition, $page){
		
		$rows = 10;
		$page = $rows * $page;

		$this -> db -> select('i.*,a.folder_name');
		$this -> db -> from('tbl_folder_document as i');
		$this -> db -> join('tbl_document_folders as a', 'i.document_folder_id  = a.document_folder_id', 'left');
		//$this-> db -> where('i.document_folder_id',$get['document_folder_id']);
		$this->db->where("($condition)");
		
		$this->db->order_by('i.folder_document_id', 'desc');
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		//print_r($this->db->last_query());
		//exit;
		
		$this -> db -> select('i.*,a.folder_name');
		$this -> db -> from('tbl_folder_document as i');
		$this -> db -> join('tbl_document_folders as a', 'i.document_folder_id  = a.document_folder_id', 'left');
		//$this-> db -> where('i.document_folder_id',$get['document_folder_id']);
		$this->db->where("($condition)");
		$this->db->order_by('i.folder_document_id', 'desc');	
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return null;
			return array("totalRecords"=>0);
		}
		
	}
	
	function getCenterInquires($condition, $page){
		
		$rows = 10;
		$page = $rows * $page;

		$this -> db -> select('*');
		$this -> db -> from('tbl_inquiry_master ');
		//$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_master_id  = a.academic_year_master_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('inquiry_master_id', 'desc');
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		//print_r($this->db->last_query());
		//exit;
		
		$this -> db -> select('*');
		$this -> db -> from('tbl_inquiry_master');
		//$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_master_id  = a.academic_year_master_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('inquiry_master_id','desc');
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return null;
			return array("totalRecords"=>0);
		}
		
	}

	function getCommunication($condition, $page){
		
		$rows = 10;
		$page = $rows * $page;

		$this -> db -> select('*');
		$this -> db -> from('tbl_student_notices ');
		//$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_master_id  = a.academic_year_master_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('student_notice_id', 'desc');
		//$this->db->group_by('random_no');
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		//print_r($this->db->last_query());
		//exit;
		
		$this -> db -> select('*');
		$this -> db -> from('tbl_student_notices');
		//$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_master_id  = a.academic_year_master_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('student_notice_id','desc');
	//	$this->db->group_by('random_no');
		$query1 = $this -> db -> get();
		// echo "<pre>";
		// print_r($query1->result());exit();
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return null;
			return array("totalRecords"=>0);
		}
		
	}
	
	function getAdmissionList($condition, $page, $orderBy, $orderType){
		
		$rows = 10;
		$page = $rows * $page;

		$this -> db -> select('s.student_id, i.category_id, i.course_id, i.batch_id, i.programme_start_date, i.admission_date, s.student_id, s.inquiry_master_id, s.enrollment_no,s.dob as student_dob, s.zone_id, s.center_id, s.student_first_name, s.student_last_name, c.category_id,c.categoy_name, cr.course_id,cr.course_name,b.batch_name,sg.group_id');
		$this -> db -> from('tbl_student_details as i');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_categories as c', 'i.category_id  = c.category_id', 'left');
		$this -> db -> join('tbl_courses as cr', 'i.course_id  = cr.course_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this -> db -> join('tbl_student_group as sg', '(i.student_id  = sg.student_id) && (i.academic_year_id = sg.academic_year_id)', 'left');
		//$this -> db -> join('tbl_admission_fees as f', 'i.student_id  = f.student_id and i.academic_year_id=f.academic_year_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by($orderBy, $orderType);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		//print_r($this->db->last_query());
		//exit;
		
		$this -> db -> select('s.student_id, i.category_id, i.course_id, i.batch_id, i.programme_start_date, i.admission_date, s.student_id, s.inquiry_master_id, s.enrollment_no, s.zone_id, s.center_id, s.student_first_name, s.student_last_name, c.category_id,c.categoy_name, cr.course_id,cr.course_name,b.batch_name ');
		$this -> db -> from('tbl_student_details as i');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_categories as c', 'i.category_id  = c.category_id', 'left');
		$this -> db -> join('tbl_courses as cr', 'i.course_id  = cr.course_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		//$this -> db -> join('tbl_admission_fees as f', 'i.student_id  = f.student_id and i.academic_year_id=f.academic_year_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by($orderBy, $orderType);
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return null;
			return array("totalRecords"=>0);
		}
		
	}
	
	function getCenterUserCategories($condition){
		
		$this -> db -> select('c.category_id,c.categoy_name');
		$this -> db -> from('tbl_center_user_courses as i');
		$this -> db -> join('tbl_categories as c', 'i.category_id  = c.category_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('c.categoy_name', 'asc');	
		$this->db->group_by('i.category_id');	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
		
	}
	
	function getCenterUserCourses($condition){
		
		$this -> db -> select('c.course_id,c.course_name');
		$this -> db -> from('tbl_center_user_courses as i');
		$this -> db -> join('tbl_courses as c', 'i.course_id  = c.course_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('c.course_name', 'asc');	
		$this->db->group_by('i.course_id');	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
		
	}
	
	function getCenterFees($condition,$fees_type =  null){
		$this -> db -> select('f.fees_id,f.fees_name, f.installment_no as max_installment_allowed');
		$this -> db -> from('tbl_assign_fees as i');
		$this -> db -> join('tbl_fees_master as f', 'i.fees_id  = f.fees_id', 'left');
		$this -> db -> join('tbl_assign_fees_center_mapping as map', 'i.assign_fees_id  = map.assign_fees_id', 'left');
		if($fees_type != null){
			if($fees_type == "Class"){
				$this -> db -> join('tbl_assign_fees_course_mapping as gc', 'i.assign_fees_id  = gc.assign_fees_id', 'left');
			}else{
				$this -> db -> join('tbl_assign_fees_group_mapping as gc', 'i.assign_fees_id  = gc.assign_fees_id', 'left');
			}
		}
		$this->db->where("($condition)");
		$this->db->order_by('f.fees_name', 'asc');
		$this->db->group_by('f.fees_id');
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
		
	}
	
	function getStudentClassWiseList($condition){
		
		$this -> db -> select('s.student_id,s.inquiry_master_id,s.enrollment_no,s.zone_id,s.center_id,s.student_first_name,s.student_last_name,s.dob,s.profile_pic, ps.created_by as admission_date, ps.promoted_academic_year_id as academic_year_id, ps.category_id, ps.promoted_course_id as course_id, ps.promoted_batch_id as  batch_id ');
		// $this -> db -> from('tbl_student_details as d');
		$this -> db -> from('tbl_promoted_student as ps');
		$this -> db -> join('tbl_student_master as s', 'ps.student_id  = s.student_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('s.student_first_name', 'asc');
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
		
	}
	
	function getStudentGroupWiseList($condition){
		
		$this -> db -> select('s.student_id,s.inquiry_master_id,s.enrollment_no,s.zone_id,s.center_id,s.student_first_name,s.student_last_name,s.dob,s.profile_pic, d.academic_year_id');
		$this -> db -> from('tbl_student_group as d');
		$this -> db -> join('tbl_student_master as s', 'd.student_id  = s.student_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('s.student_first_name', 'asc');
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
		
	}

	function getCommunicationStudentGroupWiseList($condition){
		
		$this -> db -> select('s.student_id,s.inquiry_master_id,s.enrollment_no,s.zone_id,s.center_id,s.student_first_name,s.student_last_name,s.dob, d.academic_year_id');
		$this -> db -> from('tbl_student_group as d');
		$this -> db -> join('tbl_student_master as s', 'd.student_id  = s.student_id', 'left');
		$this->db->where("($condition)");
		$this->db->group_by('s.student_id');
		$this->db->order_by('s.student_first_name', 'asc');
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
		
	}
	
	
	function getCenterUserNewsletters($condition, $page){
		
		$rows = 10;
		$page = $rows * $page;

		$this -> db -> select('n.*');
		$this -> db -> from('tbl_assign_newsletters as i');
		$this -> db -> join('tbl_newsletters as n', 'i.newsletter_id  = n.newsletter_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->order_by("n.newsletter_id", "desc");
		$this->db->limit($rows,$page);
		$query = $this -> db -> get();
		
		$this -> db -> select('n.*');
		$this -> db -> from('tbl_assign_newsletters as i');
		$this -> db -> join('tbl_newsletters as n', 'i.newsletter_id  = n.newsletter_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->order_by("n.newsletter_id", "desc");	
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return null;
			return array("totalRecords"=>0);
		}
		
	}
	
	
	function getPayemtDetails($condition){
		
		$this -> db -> select('i.*, s.student_id, s.student_first_name, s.student_last_name,s.enrollment_no, s.father_name, s.present_address, s.father_mobile_contact_no, s.father_email_id, ct.categoy_name, cr.course_name, b.batch_name, g.group_master_name, a.academic_year_master_name, cn.center_name, cn.center_address, cn.center_contact_no, cn.center_email_id, cn.center_code');
		$this -> db -> from('tbl_admission_fees as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cr', 'i.course_id  = cr.course_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id = g.group_master_id', 'left');
		$this -> db -> join('tbl_centers as cn', 'i.center_id = cn.center_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
		
	}

	function getSaveAdmissionData($condition){
		
		$this -> db -> select('i.*,f.*');
		$this -> db -> from('tbl_admission_fees as i');
		$this -> db -> join('tbl_admission_fees_instalments as f', 'i.admission_fees_id  = f.admission_fees_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
		
	}

	function getStudentFeesInvoice($condition){
		
		$this -> db -> select('i.receipt_file,i.receipt_file_withoutgst,fm.fees_name');
		$this -> db -> from('tbl_fees_payment_receipt as i');
		$this -> db -> join('tbl_admission_fees as f', 'i.admission_fees_id  = f.admission_fees_id', 'left');
		$this -> db -> join('tbl_fees_master as fm', 'f.fees_id  = fm.fees_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
		
	}
	
	function getFeesComponents($condition){
		
		$this -> db -> select('i.*,f.fees_component_master_id, f.fees_component_master_name, f.amount');
		$this -> db -> from('tbl_fees_component_data as i');
		$this -> db -> join('tbl_fees_component_master as f', 'i.fees_component_id  = f.fees_component_master_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
		
	}
	
	function getAppointments($condition, $page){
		
		$rows = 10;
		$page = $rows * $page;

		$this -> db -> select('i.appointment_id, i.appointment_date, i.appointment_message, s.student_first_name, s.student_last_name');
		$this -> db -> from('tbl_appointments as i');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->order_by("i.appointment_date", "desc");
		$this->db->limit($rows,$page);
		$query = $this -> db -> get();
		
		$this -> db -> select('i.appointment_id, i.appointment_date, i.appointment_message, s.student_first_name, s.student_last_name');
		$this -> db -> from('tbl_appointments as i');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->order_by("i.appointment_date", "desc");
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return null;
			return array("totalRecords"=>0);
		}
		
	}

	function getAssignToGroup($condition, $page){
		
		$rows = 10;
		$page = $rows * $page;

		$this -> db -> select('s.student_first_name, s.student_last_name,g.group_master_name,b.batch_name,a.academic_year_master_name');
		$this -> db -> from('tbl_student_group as i');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_id  = a.academic_year_master_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->limit($rows,$page);
		$query = $this -> db -> get();
		
		$this -> db -> select('s.student_first_name, s.student_last_name,g.group_master_name,b.batch_name,a.academic_year_master_name');
		$this -> db -> from('tbl_student_group as i');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_id  = a.academic_year_master_id', 'left');
		$this->db->where("($condition)");
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return null;
			return array("totalRecords"=>0);
		}
		
	}
    function getAssignedGroups($studentId, $academicYearId){
        $this -> db -> select('i.student_group_id,i.zone_id,i.center_id,i.group_id,i.batch_id,i.group_start_date,i.group_end_date, g.group_master_name, b.batch_name');
        $this -> db -> from('tbl_student_group as i');
        $this -> db -> join('tbl_group_master as g', 'i.group_id  = g.group_master_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		
		$this->db->where("i.student_id = '".$studentId."'  AND i.academic_year_id = '".$academicYearId."' ");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return null;
		}
    }	
	function getClassStudents($condition){
		
		$this -> db -> select('i.student_id, i.fcm_token');
		$this -> db -> from('tbl_student_master as i');
		// $this -> db -> join('tbl_student_details as d', 'i.student_id  = d.student_id', 'left');
		$this -> db -> join('tbl_promoted_student as d', 'i.student_id  = d.student_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	function getBatchwiseStudent($condition){
		// echo "ejllo";
		$this -> db -> select('am.*,count(am.student_id) as student_count,bm.batch_name,c.course_name');
		$this -> db -> from('tbl_activity_student_mapping as am');
		$this -> db -> join('tbl_batch_master as bm', 'bm.batch_id = am.batch_id', 'left');
		$this -> db -> join('tbl_courses as c', 'c.course_id = bm.course_id','left');	

		$this->db->where("($condition)");
		// $this->db->order_by('am.activity_id','desc');
		$this->db->group_by("am.batch_id");
		$query = $this -> db -> get();
		// echo $this->db->last_query();
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();;
		}
	}

	
	function getGroupStudents($condition){
		$this -> db -> select('d.student_id, d.fcm_token');
		$this -> db -> from('tbl_student_group as i');
		$this -> db -> join('tbl_student_master as d', 'i.student_id  = d.student_id', 'left');
		$this->db->where("($condition)");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return null;
		}
	}
	
	function getAlbumCenterUserBatches($tbl_id,$comp_id,$tbl_id1,$comp_id1){
		$this -> db -> select('b.batch_id,b.batch_name');
		$this -> db -> from('tbl_center_user_batches as cb');
		$this -> db -> join('tbl_batch_master as b', 'b.batch_id  = cb.batch_id', 'left');

		$this -> db -> where($comp_id,$tbl_id);
		$this -> db -> where($comp_id1,$tbl_id1);
		$this -> db -> where('b.course_id != 9');//not to display other course batches
		$this -> db -> where('b.status','Active');	
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return null;
		}
	}

	function getAlbumCenterUserBatches1($tbl_id,$comp_id){
		$this -> db -> select('b.batch_id');
		$this -> db -> from('tbl_center_user_batches as cb');
		$this -> db -> join('tbl_batch_master as b', 'b.batch_id  = cb.batch_id', 'left');
		$this -> db -> where($comp_id,$tbl_id);
		$this -> db -> where('b.status','Active');	
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return null;
		}
	}
	
	function getOptions($tbl_id,$comp_id,$tbl_id1,$comp_id1){
		$this -> db -> select('a.album_id,a.album_name');
		$this -> db -> from('tbl_assign_album as as');
		$this -> db -> join('tbl_album as a', 'as.album_id  = a.album_id', 'left');
		$this -> db -> where_in($comp_id,$tbl_id);
		$this -> db -> where($comp_id1,$tbl_id1);
		$this -> db -> group_by('a.album_id');		
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return null;
		}
	}

	function enum_select($table, $field){
		$query = " SHOW COLUMNS FROM `$table` LIKE '$field' ";
		$row = $this->db->query(" SHOW COLUMNS FROM `$table` LIKE '$field' ")->row()->Type;
		$regex = "/'(.*?)'/";
		preg_match_all( $regex , $row, $enum_array );
		$enum_fields = $enum_array[1];
		return( $enum_fields );
	}

	function getFeesDetails($condition){
		$this -> db -> select('fc.*,f.fees_component_master_name,f.is_mandatory,fm.installment_no,fm.allow_frequency');
		$this -> db -> from('tbl_fees_component_data as fc');
		$this -> db -> join('tbl_fees_component_master as f', 'fc.fees_component_id  = f.fees_component_master_id', 'left');
		$this->db->where("($condition)");
		$this -> db -> join('tbl_fees_master as fm', 'fc.fees_master_id  = fm.fees_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('fc.fees_component_data_id','asc');
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return null;
		}
	}

	function teacher_details($User_id){
		$this -> db -> select('center_id,user_type,first_name,last_name,contact_no,user_name as email_id,profile_pic');
		$this -> db -> from('tbl_admin_users');
		$this-> db -> where('user_id',$User_id);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}
		else{
			return null;
		}
	}

	function teacher_center_details($center_id){
		$this -> db -> select('i.*,c.center_name, ct.categoy_name, co.course_name');
		$this -> db -> from('tbl_center_courses as i');
		$this -> db -> join('tbl_centers as c', 'i.center_id  = c.center_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as co', 'i.course_id  = co.course_id', 'left');
		$this-> db -> where('i.center_id',$center_id);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return null;
		}
	}
	
	function teacher_group_details($user_id){
		$this -> db -> select('g.group_master_name,cug.group_id');
		$this -> db -> from('tbl_center_user_groups as cug');
		$this -> db -> join('tbl_admin_users as u', 'u.user_id  = cug.user_id', 'inner');
		$this -> db -> join('tbl_group_master as g', 'cug.group_id  = g.group_master_id', 'inner');
		$this-> db -> where('cug.user_id',$user_id);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return null;
		}
	}

	function batch_group_details($condition){
		$this -> db -> select('cub.batch_id,bm.batch_name');
		$this -> db -> from('tbl_center_user_batches as cub');
		$this -> db -> join('tbl_batch_master as bm', 'bm.batch_id  = cub.batch_id', 'left');
		$this-> db -> where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return null;
		}
	}

	function batch_details($condition){
		$this -> db -> select('b.batch_id,b.batch_name');
		$this -> db -> from('tbl_center_user_batches as i');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this-> db -> where($condition);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}
		else{
			return null;
		}
	}

	function getLeads($condition, $page, $orderBy, $orderType){
	    $rows = 10;
		$page = $rows * $page;

		$this -> db -> select('i.*');
		$this -> db -> from('tbl_inquiry_master as i');
		$this->db->where('type','1');
		$this->db->where("($condition)");
		$this->db->where("i.inquiry_master_id NOT IN (select lead_id from tbl_inquiry_master where rec_type = 'Inquiry')");
		$this->db->order_by($orderBy, $orderType);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_inquiry_master as i');
		$this->db->where('type',1);
		$this->db->where("($condition)");
		$this->db->where("i.inquiry_master_id NOT IN (select lead_id from tbl_inquiry_master where rec_type = 'Inquiry')");
		$this->db->order_by($orderBy, $orderType);
		$query1 = $this -> db -> get();
// 		echo $this->db->last_query();exit;
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return null;
			return array("totalRecords"=>0);
		}
		
	}
		function getLeadDetails($leadId){
	    $this -> db -> select('a.lead_id,a.inquiry_master_id,a.academic_year_master_id, a.inquiry_no,a.inquiry_date, a.type, a.student_first_name, a.student_last_name,  a.father_name,  a.father_contact_no, a.father_contact_no2, a.father_emailid, a.country_id, a.state_id, a.city_id, a.pincode,a.area_locality_sector, a.how_know_about_school, a.know_about_us, a.student_dob, a.gender, a.course_interested_in, a.lead_category, a.inquiry_remark , a.lead_status_id, c.status_description as lead_status_description, a.lead_sub_status_id, d.status_sub_description as lead_sub_status_description, a.lead_planned_status_id, e.status_description as lead_planned_status_description, a.lead_enquiry_status_id, f.lead_enquiry_status_description as lead_enquiry_status_description, g.city_name as city_name ');
		$this -> db -> from('tbl_inquiry_master as a');
// 		$this -> db -> join('tbl_brands as b', 'a.career_in  = b.brand_id', 'left');
		$this -> db -> join('tbl_lead_status as c', 'a.lead_status_id  = c.lead_status_id', 'left');
		$this -> db -> join('tbl_lead_sub_status as d', 'a.lead_sub_status_id  = d.lead_sub_status', 'left');
		$this -> db -> join('tbl_default_status as e', 'a.lead_planned_status_id  = e.default_status_id', 'left');
		$this -> db -> join('tbl_lead_enquiry_status as f', 'a.lead_enquiry_status_id  = f.lead_enquiry_status_id', 'left');
		$this -> db -> join('tbl_cities as g', 'a.city_id  = g.city_id', 'left');
		$this -> db -> where("a.type = '1'");
		$this -> db -> where("a.inquiry_master_id = '".$leadId."'");
		$query = $this -> db -> get();
// 		echo $this->db->last_query();exit;
		if($query -> num_rows() >= 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
    function getEnquiries($condition, $page, $orderBy, $orderType){
	    $rows = 10;
		$page = $rows * $page;

		$this -> db -> select('*');
		$this -> db -> from('tbl_inquiry_master');
		$this->db->where('type','2');
		$this->db->where("($condition)");
		$this->db->order_by($orderBy, $orderType);
		$this->db->limit($rows,$page);		
		$query = $this -> db -> get();
		
		
		$this -> db -> select('*');
		$this -> db -> from('tbl_inquiry_master');
		$this->db->where('type',2);
		$this->db->where("($condition)");
		$this->db->order_by($orderBy, $orderType);
		$query1 = $this -> db -> get();
// 		echo $this->db->last_query();exit;
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return null;
			return array("totalRecords"=>0);
		}
		
	}
	
	function getEnquiryDetails($enquiryId){
		   
	    $this -> db -> select('a.*, c.status_description as lead_status_description, d.status_sub_description as lead_sub_status_description, e.status_description as lead_planned_status_description, f.lead_enquiry_status_description as lead_enquiry_status_description ');
		$this -> db -> from('tbl_inquiry_master as a');
// 		$this -> db -> join('tbl_brands as b', 'a.career_in  = b.brand_id', 'left');
		$this -> db -> join('tbl_lead_status as c', 'a.lead_status_id  = c.lead_status_id', 'left');
		$this -> db -> join('tbl_lead_sub_status as d', 'a.lead_sub_status_id  = d.lead_sub_status', 'left');
		$this -> db -> join('tbl_default_status as e', 'a.lead_planned_status_id  = e.default_status_id', 'left');
		$this -> db -> join('tbl_lead_enquiry_status as f', 'a.lead_enquiry_status_id  = f.lead_enquiry_status_id', 'left');
// 		$this -> db -> where("a.type IN ('1','2')");
		$this -> db -> where("a.inquiry_master_id = '".$enquiryId."'");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
		function getFollowUpList($condition, $page, $orderBy, $orderType, $fromFollowUpDate, $toFollowUpDate){
    	    $rows = 10;
    		$page = $rows * $page;
    
            $this -> db -> select('a.lead_id');
    	    $this -> db -> from('tbl_lead_enquiry_log as a');
    		$this -> db -> join('tbl_inquiry_master as le', 'le.inquiry_master_id  = a.lead_id', 'inner');
    		$this->db->where("($condition)");
    		$this->db->where('a.type IN (1,2)');
    		$this->db->order_by($orderBy, $orderType);
    		$this->db->group_by('a.lead_id');
    		$this->db->limit($rows,$page);		
    		$query = $this -> db -> get();
    		
    		if($query -> num_rows() >= 1){
    			$retArray = array();
    			$itr = 0;
    			
    			foreach ($query->result() as $key => $value) {
    			    $leadId = $value->lead_id;
    			    $checkInquiryExist = $this->getdata_orderby_limit1('tbl_inquiry_master','inquiry_master_id',"lead_id = '".$leadId."'","");
    			    if(empty($checkInquiryExist)){
        			    $this -> db -> select('a.*, g.city_name as city_name ');
                		$this -> db -> from('tbl_inquiry_master as a');
                		$this -> db -> join('tbl_cities as g', 'a.city_id  = g.city_id', 'left');
                		$this -> db -> where("a.inquiry_master_id = '".$leadId."'");
                		$query = $this -> db -> get();
                		if($query -> num_rows() == 1){
                		    $fData = $query->result_array()[0];
                		    
                		    $lastFollowUp = $this ->getLastFollowUp($leadId, "");
                		    if(!empty($lastFollowUp)){
                		        $fData["last_followup"] = $lastFollowUp[0];
                		    }
                		    $nextFollowUp = $this ->getNextFollowUpDateTime($leadId, "");
                		    if(!empty($nextFollowUp)){
                		        $fData["next_follow_up_on_date"] = $nextFollowUp[0]["follow_up_date"];
                		        $fData["next_follow_up_on_time"] = $nextFollowUp[0]["follow_up_time"];
                		    }
                		    if(empty($fromFollowUpDate) || empty($toFollowUpDate)){
                    		    $retArray[$itr]= $fData;
                    		    $itr++;
                		    }else{
                		        if ($fromFollowUpDate <= $fData["next_follow_up_on_date"] && $fData["next_follow_up_on_date"] <= $toFollowUpDate){
                                    $retArray[$itr]= $fData;
                                    $itr++;
                		        }
                		    }
                		}
    			    }
    			}
    			
    			return array("query_result" => $retArray,"totalRecords"=>0);
    		}else{
    			return array("totalRecords"=>10);
    		}
		
		
	}
	function getFollowUps($leadId, $type){
	    $this -> db -> select('a.ID as follow_up_id, a.lead_id, a.follow_up_date, a.follow_up_time, a.lead_status_id, c.status_description as lead_status_description, a.lead_sub_status_id, d.status_sub_description as lead_sub_status_description, a.lead_planned_status_id, e.status_description as lead_planned_status_description, a.lead_enquiry_status_id, f.lead_enquiry_status_description as lead_enquiry_status_description, a.notes, a.updated_on, a.communication_mode, im.starred');
	    $this -> db -> from('tbl_lead_enquiry_log as a');
		$this -> db -> join('tbl_lead_status as c', 'a.lead_status_id  = c.lead_status_id', 'left');
		$this -> db -> join('tbl_lead_sub_status as d', 'a.lead_sub_status_id  = d.lead_sub_status', 'left');
		$this -> db -> join('tbl_default_status as e', 'a.lead_planned_status_id  = e.default_status_id', 'left');
		$this -> db -> join('tbl_lead_enquiry_status as f', 'a.lead_enquiry_status_id  = f.lead_enquiry_status_id', 'left');
    	$this -> db -> join('tbl_inquiry_master as im', 'a.lead_id  = im.inquiry_master_id', 'left');
	    $this -> db -> where("a.lead_id = '".$leadId."'");
		if(!empty($type)){
    	   $this -> db -> where("a.type = '".$type."'");
    	}
		$this->db->order_by('a.ID', "DESC");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1) {
			return $query->result_array();
		} else {
			return null;
		}
	}
	function getLastFollowUp($leadId, $type,$leadstatusid){
		    $this -> db -> select('a.ID as follow_up_id, a.lead_id, a.follow_up_date, a.follow_up_time, a.lead_status_id, c.status_description as lead_status_description, a.lead_sub_status_id, d.status_sub_description as lead_sub_status_description, a.lead_planned_status_id, e.status_description as lead_planned_status_description, a.lead_enquiry_status_id, f.lead_enquiry_status_description as lead_enquiry_status_description, a.notes, a.updated_on');
    	    $this -> db -> from('tbl_lead_enquiry_log as a');
    		$this -> db -> join('tbl_lead_status as c', 'a.lead_status_id  = c.lead_status_id', 'left');
    		$this -> db -> join('tbl_lead_sub_status as d', 'a.lead_sub_status_id  = d.lead_sub_status', 'left');
    		$this -> db -> join('tbl_default_status as e', 'a.lead_planned_status_id  = e.default_status_id', 'left');
    		$this -> db -> join('tbl_lead_enquiry_status as f', 'a.lead_enquiry_status_id  = f.lead_enquiry_status_id', 'left');
    	    $this -> db -> where("a.lead_id = '".$leadId."'");
			if($leadstatusid){
				$this -> db -> where("a.lead_status_id = '".$leadstatusid."'");
			}
    	    if(!empty($type)){
    		    $this -> db -> where("a.type = '".$type."'");
    	    }
    		$this -> db -> where("a.lead_status_id IS NOT NULL");
    		$this->db->order_by('a.ID', "DESC");
		    $this->db->limit("1","0");
    		$query = $this -> db -> get();
    		if($query -> num_rows() >= 1) {
    			return $query->result_array();
    		} else {
    			return null;
    		}
	    
	}
	function getNextFollowUpDateTime($leadId, $type){
	    $this -> db -> select('a.follow_up_date, a.follow_up_time');
	    $this -> db -> from('tbl_lead_enquiry_log as a');
	    $this -> db -> where("a.lead_id = '".$leadId."'");
	    if(!empty($type)){
		    $this -> db -> where("a.type = '".$type."'");
	    }
		$this->db->order_by('a.ID', "DESC");
	    $this->db->limit("1","0");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1) {
			return $query->result_array();
		} else {
			return null;
		}
	}
	function getFollowUpDetails($leadId){
	    $this -> db -> select('a.*, c.status_description as lead_status_description, d.status_sub_description as lead_sub_status_description, e.status_description as lead_planned_status_description, f.lead_enquiry_status_description as lead_enquiry_status_description, g.city_name as city_name, lm.inquiry_no as lead_inquiry_no ');
		$this -> db -> from('tbl_inquiry_master as a');
		$this -> db -> join('tbl_lead_status as c', 'a.lead_status_id  = c.lead_status_id', 'left');
		$this -> db -> join('tbl_lead_sub_status as d', 'a.lead_sub_status_id  = d.lead_sub_status', 'left');
		$this -> db -> join('tbl_default_status as e', 'a.lead_planned_status_id  = e.default_status_id', 'left');
		$this -> db -> join('tbl_lead_enquiry_status as f', 'a.lead_enquiry_status_id  = f.lead_enquiry_status_id', 'left');
		$this -> db -> join('tbl_cities as g', 'a.city_id  = g.city_id', 'left');
		$this -> db -> join('tbl_inquiry_master as lm', 'a.lead_id  = lm.inquiry_master_id', 'left');
		//$this -> db -> where("a.type = '1'");
		$this -> db -> where("a.inquiry_master_id = '".$leadId."'");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1) {
			return $query->result_array();
		} else {
			return false;
		}
	}
	
	function getLastPromotion($studentId, $academicYearId){
	    $this -> db -> select('i.promoted_id, a.academic_year_master_name as previous_academic_year_name, b.academic_year_master_name as promoted_academic_year_name');
		$this -> db -> from('tbl_promoted_student as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.current_academic_year_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_academic_year_master as b', 'i.promoted_academic_year_id  = b.academic_year_master_id', 'left');
	    $this->db->where("i.student_id = '".$studentId."'  AND i.promoted_academic_year_id = '".$academicYearId."' ");
	    $this->db->order_by('i.promoted_id', "DESC");
		$this->db->limit("1","0");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1) {
			return $query->result_array();
		} else {
			return null;
		}
	}
}
?>