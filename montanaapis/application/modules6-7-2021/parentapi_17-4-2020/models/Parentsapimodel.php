<?PHP
class Parentsapimodel extends CI_Model
{
	function insertData($tbl_name,$data_array,$sendid = NULL)
	{
	 	$this->db->insert($tbl_name,$data_array);
	 	$result_id = $this->db->insert_id();
	 	
	 	/*echo $result_id;
	 	exit;*/
	 	
	 	if($sendid == 1)
	 	{
	 		//return id
	 		return $result_id;
	 	}
	}
	
	function insertBatchData($tbl_name,$data_array,$sendid = NULL)
	{
	 	$this->db->insert_batch($tbl_name,$data_array);
	 	$result_id = $this->db->insert_id();
	 	
	 	/*echo $result_id;
	 	exit;*/
	 	
	 	if($sendid == 1)
	 	{
	 		//return id
	 		return $result_id;
	 	}
	}
  
  
  
  
	function update_insert_fcm_token($user_id=null, $user_name=null,$user_type=null,$fcm_token=null,$device_id=null,$app_type){
		if($user_id != null && $user_name  != null && $user_type  != null && $fcm_token != null && $device_id != null){
			$sql = $this->db->query("select * 
						from tbl_fcm_user_token 
						where user_id = ".$this->db->escape($user_id).'
						And user_name = '.$this->db->escape($user_name).'
						And user_type = '.$this->db->escape($user_type).'
						And app_type = '.$this->db->escape($app_type).'
						'  
					);
			
			$data = array(
						'user_id'=>$user_id,
						'user_name'=>$user_name,
						'user_type'=>$user_type,
						'fcm_token'=>$fcm_token,
						'device_id'=>$device_id,
						'app_type'=>$app_type,
					);
					
			$res = false;
			if($sql->num_rows() > 0){
				$result = $sql->result_array();
				$fcm_user_token_id = $result[0]['fcm_user_token_id'];
				$res = $this->updateRecord('tbl_fcm_user_token', $data, 'fcm_user_token_id = '.$this->db->escape($fcm_user_token_id));
			}else{
				$res = $this->insertData('tbl_fcm_user_token', $data,1);
			}
			
			return $res;
		}
	}
		 
	
	function deleteuser_fcm_details($user_id,$user_type)
	{
		$this->db->where('user_id', $user_id);
		$this->db->where('user_type', $user_type);
		$this->db->delete('tbl_fcm_user_token');
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
  
  
	function fetch_otp($mobile,$country_code){
		$sql = $this->db->query("select * from tbl_otp_check where mobile = ".$this->db->escape($mobile).' And country_code ='.$this->db->escape($country_code));
		if($sql->num_rows()>0){
			return $sql->result();
		}else{
			return false;
		}
	}
  
	function generate_otp($mobile,$country_code,$otp){
		$sql = $this->db->query("select * from tbl_otp_check where mobile = ".$this->db->escape($mobile).' And country_code ='.$this->db->escape($country_code) );
		
		$data = array(
               "mobile"=>$mobile,
               "country_code"=>$country_code,
               "otp" => $otp,
               "time_stamp"=>date('Y-m-d H:i:s')
		);
    
		if($sql->num_rows()>0){
			$this->updatedataotp($data, "tbl_otp_check", "mobile", $mobile,"country_code", $country_code);
		}else{
			$this->insertData("tbl_otp_check",$data,1);
		}
		
		return true;
	}
  
	function updatedataotp($data,$table,$column1,$value1,$column2,$value2){
		$this->db->where($column1,$value1);
		$this->db->where($column2,$value2);
		$this->db->update($table,$data);
	}
	
	function getdata($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
	
	function getdata_orderby($table, $fields, $condition = '1=1', $order_by){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition $order_by");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getdata_orderby_album_image($condition ){
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_album_images as i');
		$this -> db -> join('tbl_album_map_data as m', 'i.album_image_id  = m.album_image_id', 'left');
		
		$this->db->where($condition);
		$this->db->order_by("i.created_on",'desc');
		$sql = $this -> db -> get();

		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
	
	function getdata_orderby_limit($table, $fields, $condition = '1=1', $order_by, $page){
	
		$rows = 10;
		$page = $rows * $page;
		$limit = $page.",".$rows;
		
		$sql = $this->db->query("Select $fields from $table where $condition $order_by limit $limit");
		//print_r($this->db->last_query());
		//exit;
		
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
	
	function getdata_orderby1($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition order by id desc limit 1");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
	
	function getdata_group_order_by($col1,$col2,$col3, $fields){
		//echo "Select $fields from $table where $condition";exit;
		// $sql = $this->db->query("Select $fields from $table where $condition $group_by $order_by");

		$this -> db -> select('i.*');
		$this -> db -> from('tbl_album_images as i');
		$this -> db -> join('tbl_album_map_data as m', 'i.album_image_id  = m.album_image_id', 'left');
		
		$this->db->where("i.center_id",$col1);
		$this->db->where("i.album_id",$col2);
		$this->db->where_in("m.batch_id",$col3);
		$this->db->group_by("i.uploaded_on");
		$this->db->order_by("i.uploaded_on",'desc');
		$sql = $this -> db -> get();

		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
	
	function getAlbumLastImage($condition){
		//echo "Select $fields from $table where $condition";exit;
		// $sql = $this->db->query("Select $fields from $table where $condition $group_by $order_by");

		$this -> db -> select('i.album_pic');
		$this -> db -> from('tbl_album_images as i');
		$this -> db -> join('tbl_album_map_data as m', 'i.album_image_id  = m.album_image_id', 'left');
		
		$this->db->where("($condition)");
		
		$this->db->group_by("i.album_image_id");
		$this->db->order_by("i.album_image_id",'desc');
		$this->db->limit(1);
		
		$sql = $this -> db -> get();

		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getAlbumImageId($condition){
		//echo "Select $fields from $table where $condition";exit;
		// $sql = $this->db->query("Select $fields from $table where $condition $group_by $order_by");

		$this -> db -> select('a.*,i.album_pic,i.album_image_id,i.album_id');
		$this -> db -> from('tbl_album_images as i');
		$this -> db -> join('tbl_album_map_data as m', 'i.album_image_id  = m.album_image_id', 'left');
		$this -> db -> join('tbl_assign_album as a', 'i.album_id  = a.album_id', 'left');
		
		$this->db->where("($condition)");
		
		$this->db->group_by("i.album_image_id");
		$this->db->order_by("i.album_image_id",'desc');
		$this->db->limit(1);
		
		$sql = $this -> db -> get();

		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}

	function getImageData($condition){
		//echo "Select $fields from $table where $condition";exit;
		// $sql = $this->db->query("Select $fields from $table where $condition $group_by $order_by");

		$this -> db -> select('a.*,i.album_pic,i.album_image_id,i.album_id');
		$this -> db -> from('tbl_album_images as i');
		$this -> db -> join('tbl_album_map_data as m', 'i.album_image_id  = m.album_image_id', 'left');
		
		$this->db->where("($condition)");
		
		$this->db->group_by("i.album_image_id");
		$this->db->order_by("i.album_image_id",'desc');
		$this->db->limit(1);
		
		$sql = $this -> db -> get();

		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
	
	function getdata_orderby_limit1($table, $fields, $condition = '1=1', $order_by){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition $order_by limit 1");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
    
    
    
	function updateRecord($tbl_name,$datar,$condition)
	{
		//$this -> db -> where($comp_col, $eid);
		$this->db->where("($condition)");
		$this -> db -> update($tbl_name,$datar);
		 
		if ($this->db->affected_rows() > 0){
			return true;
		}else{
			return true;
		} 
	}
	 
	function getFormdata($ID)
	{
		$this -> db -> select('i.*');
		$this -> db -> from('tbl_categories as i');
		$this -> db -> where('i.category_id', $ID);
	
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1){
			return $query->result();
		}else{
			return false;
		}
	}
	
	
	function delrecord($tbl_name,$tbl_id,$record_id)
	{
		$this->db->where($tbl_id, $record_id);
	    $this->db->delete($tbl_name);
		if($this->db->affected_rows() >= 1)
		{
			return true;
	    }
	    else
	    {
			return false;
	    }
	}
	
	function delrecord_condition($tbl_name,$condition)
	{
		//$this->db->where($tbl_id, $record_id);
		$this->db->where("($condition)");
		$this->db->delete($tbl_name);
		if($this->db->affected_rows() >= 1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	function checkEmail($email_id){
		$condition = "1=1 && i.email='".$email_id."' ";
		
		$this -> db -> select('i.professional_id');
		$this -> db -> from('tbl_professional as i');
		$this -> db -> where("($condition)");
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1){
			return $query -> result_array();
		}else{
			return false;
		}
	}
	
	function get_fcm_data($user_id,$user_type){
		$query = $this->db->query('Select * from tbl_fcm_user_token where user_id = '.$this->db->escape($user_id).' And user_type='.$this->db->escape($user_type).' ');
		//echo 'Select * from tbl_fcm_user_token where user_id = '.$this->db->escape($user_id).' And user_type='.$this->db->escape($user_type).' ';
		//exit;
	    //print_r($this->db->last_query());
		//exit;
		if($query -> num_rows() >= 1){
				return $query->result_array();
			}else{
				return false;
			}
	}
	
	function get_email_data($eid){
		$query = $this->db->query('Select * from tbl_emailcontents where eid = '.$this->db->escape($eid));
	    
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function check_guest_utoken($device_id) {
		$this -> db -> select('u.*');
		$this -> db -> from('tbl_admin_users as u');	
		$this -> db -> where('u.device_id',$device_id);
		
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
			
	}
	
	function check_utoken($user_id) {
		$this -> db -> select('u.*');
		$this -> db -> from('tbl_student_master as u');
		$this -> db -> where('u.student_id',$user_id);
		
		$query = $this -> db -> get();
	   
		//print_r($this->db->last_query());
		//exit;
	   
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	function check_registered_utoken($utoken) {
		$this -> db -> select('u.*');
		$this -> db -> from('tbl_users as u');	
		$this -> db -> where('u.email',$utoken);
		$query = $this -> db -> get();
	
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	
	
	function getRecords($tbl_name, $condition, $page, $default_sort_column=null, $default_sort_order=null, $group_by=null){
		//echo "here...<br/>";
		//print_r($_REQUEST);
		//exit;
		
		$table = $tbl_name;
		$default_sort_column = $default_sort_column;
		$default_sort_order = $default_sort_order;
		
		
		//$rows = 100;
		$rows = 10;
		$page = $rows * $page;
		
		// sort order by column
		$sort = $default_sort_column;  
		$order = $default_sort_order;

		$this -> db -> select('*');
		$this -> db -> from($tbl_name);
		
		$this->db->where("($condition)");
		if(!empty($group_by)){
			$this->db->group_by($group_by);
		}
		$this->db->order_by($sort, $order);
		$this->db->limit($rows,$page);
		
		$query = $this -> db -> get();
		
		//print_r($this->db->last_query());
		//exit;
		
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
		
		//exit;
	}

	function getRecords1($tbl_name, $condition, $page, $group_by=null){
		//echo "here...<br/>";
		//print_r($_REQUEST);
		//exit;
		
		$table = $tbl_name;
		
		
		//$rows = 100;
		$rows = 10;
		$page = $rows * $page;
		

		$this -> db -> select('i.*,a.album_image_id,a.teacher_id');
		$this -> db -> from('tbl_assign_album as i');
		$this -> db -> join('tbl_album_images as a', 'i.album_id  = a.album_id', 'left');
		
		$this->db->where("($condition)");
		if(!empty($group_by)){
			$this->db->group_by($group_by);
		}
		$this->db->order_by('a.album_image_id','desc');
		$this->db->limit($rows,$page);
		
		$query = $this -> db -> get();
		
		//print_r($this->db->last_query());
		//exit;
		
		
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return false;
		}
		
		
		//exit;
	}
	
	function login_check($condition) {
		$this -> db -> select('student_id, enrollment_no, zone_id, center_id, student_first_name, student_last_name, dob, profile_pic');
		$this -> db -> from('tbl_student_master');
		$this -> db -> where("($condition)");
		
		$query = $this -> db -> get();
		//print_r($this->db->last_query());
		//exit;
	 
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	
	function getCenterUserNewsletters($condition, $page){
		
		$rows = 10;
		$page = $rows * $page;

		$this -> db -> select('n.*');
		$this -> db -> from('tbl_assign_newsletters as i');
		$this -> db -> join('tbl_newsletters as n', 'i.newsletter_id  = n.newsletter_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->order_by("n.newsletter_id", "desc");
		$this->db->limit($rows,$page);
		$query = $this -> db -> get();
		
		$this -> db -> select('n.*');
		$this -> db -> from('tbl_assign_newsletters as i');
		$this -> db -> join('tbl_newsletters as n', 'i.newsletter_id  = n.newsletter_id', 'left');
		
		$this->db->where("($condition)");
		$this->db->order_by("n.newsletter_id", "desc");	
		$query1 = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			$totcount = $query1 -> num_rows();
			return array("query_result" => $query->result(),"totalRecords"=>$totcount);
		}
		else
		{
			//return false;
			return array("totalRecords"=>0);
		}
		
	}
	
	function getStudentProfile($condition){
		
		$this -> db -> select('s.student_id, s.enrollment_no, s.student_first_name, s.student_last_name, s.dob, s.nationality, s.religion, s.father_name, s.mother_name, s.present_address, s.mother_home_contact_no, s.mother_mobile_contact_no, s.father_home_contact_no, s.father_mobile_contact_no, s.profile_pic, a.academic_year_master_name, ct.categoy_name, cr.course_name, b.batch_name, z.zone_name, cn.center_name');
		
		$this -> db -> from('tbl_student_details as d');
		$this -> db -> join('tbl_student_master as s', 'd.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_academic_year_master as a', 'd.academic_year_id  = a.academic_year_master_id', 'left');
		
		$this -> db -> join('tbl_categories as ct', 'd.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cr', 'd.course_id  = cr.course_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'd.batch_id  = b.batch_id', 'left');
		
		$this -> db -> join('tbl_zones as z', 's.zone_id  = z.zone_id', 'left');
		$this -> db -> join('tbl_centers as cn', 's.center_id  = cn.center_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	
	function getUserCategories($condition){
		
		$this -> db -> select('c.category_id,c.categoy_name');
		$this -> db -> from('tbl_student_details as i');
		$this -> db -> join('tbl_categories as c', 'i.category_id  = c.category_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('c.categoy_name', 'asc');	
		$this->db->group_by('i.category_id');	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	
	function getUserCourses($condition){
		
		$this -> db -> select('c.course_id,c.course_name');
		$this -> db -> from('tbl_student_details as i');
		$this -> db -> join('tbl_courses as c', 'i.course_id  = c.course_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('c.course_name', 'asc');	
		$this->db->group_by('i.course_id');	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	
	function getUserBatches($condition){
		
		$this -> db -> select('b.batch_id,b.batch_name');
		$this -> db -> from('tbl_student_details as i');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('b.batch_name', 'asc');	
		$this->db->group_by('i.batch_id');	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	
	function getCamaraList($condition){
		
		$this -> db -> select('c.camera_id,c.camera_name, c.url, c.start_time, c.end_time');
		$this -> db -> from('tbl_assign_camera as i');
		$this -> db -> join('tbl_camera as c', 'i.camera_id  = c.camera_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('c.camera_name', 'asc');	
		$this->db->group_by('i.camera_id');	
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	
	 //ravi code start 14-02-2018
	 
	 
	public function getvideotime()
     {

		$this -> db -> select('videotime');
		$this -> db -> from('videotime as i');
		$query = $this -> db -> get();
	         
		if($query -> num_rows() >= 1)
		{
			return $query->result();
		}
		else
		{
			return 0;
		}	
	}
	  
	
	//ravi code end 14-02-2018
	
	/* arul start */
	function getUserGroups($student_id){
		$condition = " sg.student_id = ".$student_id ;
		$this -> db -> select('gm.group_master_id,gm.group_master_name');
		$this -> db -> from('tbl_student_group as sg');
		$this -> db -> join('tbl_group_master as gm', 'gm.group_master_id  = sg.group_id', 'left');
		$this->db->where("($condition)");
		$this->db->order_by('gm.group_master_name', 'ASC');	
		$this->db->group_by('gm.group_master_id');
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{	
			return false;
		}
	}
	/* arul end */

	//aakashi code 29-06-2019
	function getStudentRoutine($condition){
		
		$this -> db -> select('ct.categoy_name, cr.course_name, cn.center_name,r.routine_date,s.student_first_name, s.student_last_name ,r.routine_type,subr.subroutine_name,ra.routine_action,r.sleep_start_time,r.sleep_end_time,r.routine_comments,r.created_on');
		
		$this -> db -> from('tbl_student_daily_routine_data as r');
		
		$this -> db -> join('tbl_categories as ct', 'r.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cr', 'r.course_id  = cr.course_id', 'left');
		$this -> db -> join('tbl_centers as cn', 'r.center_id  = cn.center_id', 'left');
		$this -> db -> join('tbl_student_master as s', 'r.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_subroutine_master as subr', 'r.subroutine_id  = subr.subroutine_id', 'left');
		$this -> db -> join('tbl_routine_actions_master as ra', 'r.routine_action_id  = ra.routine_action_id', 'left');
		
		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	//aakashi code end

	function getInstallmentDetails($academic_year,$student_id,$zone_id,$center_id){
		$condition = array("af.academic_year_id"=>$academic_year,"af.student_id"=>$student_id,"af.zone_id"=>$zone_id,"af.center_id"=>$center_id);
		$this -> db ->select("tc.course_name,gm.group_master_name as group_name,afi.admission_fees_instalment_id,afi.admission_fees_id,af.fees_id,af.fees_type,DATE_FORMAT(afi.instalment_due_date, '%a, %d %b %Y') as formated_instalment_due_date,afi.instalment_due_date,afi.instalment_amount,afi.instalment_collected_amount,afi.instalment_remaining_amount,afi.instalment_status,af.category_id,af.course_id,af.batch_id,af.group_id,af.fees_remark,case when (af.is_instalment='Yes') then 'true' else 'false' end as is_installment");
		$this -> db ->from("tbl_admission_fees_instalments as afi");
		$this -> db ->join("tbl_admission_fees as af","af.admission_fees_id = afi.admission_fees_id","LEFT");
		$this -> db ->join("tbl_courses as tc","tc.course_id = af.course_id","LEFT");
		$this -> db ->join("tbl_group_master as gm","gm.group_master_id = af.group_id","LEFT");
		$this -> db ->where($condition);
		$this -> db ->order_by("afi.instalment_due_date","ASC");
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}

	function getInstallmentpaytmpayment($installment_id)
	{
		//print_r($installment_id);
		$this -> db ->select('instalment_remaining_amount');
		$this -> db ->from('tbl_admission_fees_instalments');
		$this -> db ->where_in('admission_fees_instalment_id',$installment_id);
		$query = $this -> db -> get();
		if($query -> num_rows() >= 1){
			//echo $this -> db->last_query();
			return $query->result_array();
		}else{
			return false;
		}
	}
	function getStudentDetail($student_id)
	{
		$this-> db ->select('sm.student_first_name,sm.student_last_name,sm.dob,sm.center_id,sd.course_id,sd.batch_id,sm.father_name,sm.mother_name,sm.mother_mobile_contact_no,sm.mother_email_id,sm.father_mobile_contact_no,sm.father_email_id,sm.father_prof,sm.mother_prof,sm.present_address,sm.country_id,sm.state_id,sm.city_id,sm.pincode,sm.parent_name,sm.guardian,sm.student_gender,sm.father_dob,sm.mother_dob,sm.guardian_dob,sm.aadhar_no,sm.pan_no,sm.annual_income,sm.marital_status,ctry.country_name,s.state_name,c.city_name,cn.instituteId,cn.branchId,cs.courseId');
		$this-> db ->from('tbl_student_master as sm');
		$this -> db ->join("tbl_student_details as sd","sd.student_id = sm.student_id","inner");
		$this -> db ->join("tbl_countries as ctry","sm.country_id = ctry.country_id","inner");
		$this -> db ->join("tbl_states as s","sm.state_id = s.state_id","inner");
		$this -> db ->join("tbl_cities as c","sm.city_id = c.city_id","inner");
		$this -> db ->join("tbl_centers as cn","sm.center_id = cn.center_id","left");
		$this -> db ->join("tbl_courses as cs","sd.course_id = cs.course_id","left");
		$this-> db ->where('sm.student_id',$student_id);
		$query = $this-> db ->get();
		//echo $this -> db->last_query();exit;
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	function getCenterCredential($center_id)
	{
		$this-> db ->select('production_mid,production_merchant_key,production_wap_name,industry_type_id');
		$this-> db ->from('center_transaction_credential');
		$this-> db ->where('center_id',$center_id);
		$query = $this-> db ->get();
		//echo $this -> db->last_query();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	function getMerchantKey($mid)
	{
		$this-> db ->select('production_merchant_key');
		$this-> db ->from('center_transaction_credential');
		$this-> db ->where('production_mid',$mid);
		$query = $this-> db ->get();
		//echo $this -> db->last_query();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	function getInstallmentIds($order_id)
	{
		$this-> db ->select('installment_id,student_id');
		$this-> db ->from('tbl_paytmpayment');
		$this-> db ->where('id',$order_id);
		$query = $this-> db ->get();
		//echo $this -> db->last_query();
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	function getAdmintionFeeIds($ids)
	{
		$this-> db ->select('afi.admission_fees_id,afi.fees_type,afi.instalment_remaining_amount,af.group_id,af.course_id,afi.instalment_collected_amount,afi.admission_fees_instalment_id,afi.instalment_amount,af.total_amount');
		$this-> db ->from('tbl_admission_fees_instalments afi');
		$this -> db -> join('tbl_admission_fees as af', 'af.admission_fees_id  = afi.admission_fees_id', 'left');
		$this-> db ->where_in('afi.admission_fees_instalment_id' ,$ids);
		$query = $this-> db ->get();
		//echo $this -> db->last_query();exit;
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	function getGroupFeesComponents($admission_fees_id,$group_id){
		if($admission_fees_id != "" && $group_id != ""){
			$condition = array("af.admission_fees_id"=>$admission_fees_id,"af.group_id"=>$group_id);
			$this -> db -> select('afc.*');
			$this -> db -> from('tbl_admission_fees_components as afc');
			$this -> db -> join('tbl_admission_fees as af', 'af.admission_fees_id  = afc.admission_fees_id', 'left');
			$this->db->where($condition);
			$this->db->order_by("afc.fees_component_id","ASC");
			$query = $this -> db -> get();
			//echo $this -> db->last_query();exit;
			if($query -> num_rows() >= 1){
				return $query->result_array();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	function getClassFeesComponents($admission_fees_id,$course_id){
		if($admission_fees_id != "" && $course_id != ""){
			$condition = array("af.admission_fees_id"=>$admission_fees_id,"af.course_id"=>$course_id);
			$this -> db -> select('afc.*');
			$this -> db -> from('tbl_admission_fees_components as afc');
			$this -> db -> join('tbl_admission_fees as af', 'af.admission_fees_id  = afc.admission_fees_id', 'left');
			$this->db->where($condition);
			$this->db->order_by("afc.fees_component_id","ASC");
			$query = $this -> db -> get();
			//echo $this -> db->last_query();exit;
			if($query -> num_rows() >= 1){
				return $query->result_array();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	
	function updateRecordpaytm($tableName, $data, $column, $value)
	{
		$this->db->where("$column", $value);
		$this->db->update($tableName, $data);
		if ($this->db->affected_rows() > 0) {
			return true;
		}
		else {
			return true;
		}
	} 
	function getdata1($table, $fields, $condition = '1=1'){
		//echo "Select $fields from $table where $condition";exit;
		$sql = $this->db->query("Select $fields from $table where $condition");
		if($sql->num_rows() > 0){
			return $sql->result_array();
		}else{
			return false;
		}
	}
	function getPayemtDetails($condition){
		$this -> db -> select('i.*, s.student_id, s.student_first_name, s.student_last_name,s.enrollment_no, s.father_name, s.present_address,s.address1, s.address2, s.address3,s.pincode, s.father_mobile_contact_no, s.father_email_id,s.parent_name,s.guardian,s.mother_name,s.password, ct.categoy_name, cr.course_name, b.batch_name, g.group_master_name, a.academic_year_master_name, cn.center_name, cn.center_address, cn.center_contact_no, cn.center_email_id, cn.center_code,cn.receipt_print_name,fp.*,country.country_name,state.state_name,city.city_name');
		$this -> db -> from('tbl_admission_fees as i');
		$this -> db -> join('tbl_academic_year_master as a', 'i.academic_year_id  = a.academic_year_master_id', 'left');
		$this -> db -> join('tbl_student_master as s', 'i.student_id  = s.student_id', 'left');
		$this -> db -> join('tbl_student_details as sd', 'i.student_id  = sd.student_id', 'left');
		$this -> db -> join('tbl_categories as ct', 'i.category_id  = ct.category_id', 'left');
		$this -> db -> join('tbl_courses as cr', 'i.course_id  = cr.course_id', 'left');
		$this -> db -> join('tbl_batch_master as b', 'i.batch_id  = b.batch_id', 'left');
		$this -> db -> join('tbl_group_master as g', 'i.group_id = g.group_master_id', 'left');
		$this -> db -> join('tbl_centers as cn', 'i.center_id = cn.center_id', 'left');
		$this -> db -> join('tbl_fees_payment_details as fp', 'i.admission_fees_id = fp.admission_fees_id', 'left');
		$this -> db -> join('tbl_countries as country', 's.country_id = country.country_id', 'left');
		$this -> db -> join('tbl_states as state', 's.state_id = state.state_id', 'left');
		$this -> db -> join('tbl_cities as city', 's.city_id = city.city_id', 'left');

		$this->db->where("($condition)");
		
		$query = $this -> db -> get();
		// echo $this->db->last_query();exit;
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
		
	}
	function getFeesComponentsforReceipt($condition){
		$this -> db -> select('afcch.component_collected_history_id,fcm.fees_component_master_name,fcm.receipt_name,af.fees_type, afcch.collected_amount,c.course_name,g.group_master_name');
		$this -> db -> from('tbl_admission_fees_components as afc');
		$this -> db -> join('tbl_admission_fees_components_collected_history as afcch', 'afcch.admission_fees_component_id  = afc.admission_fees_component_id', 'left');
		$this -> db -> join('tbl_fees_component_master as fcm', 'fcm.fees_component_master_id  = afc.fees_component_id ', 'left');
		$this -> db -> join('tbl_admission_fees as af', 'af.admission_fees_id  = afc.admission_fees_id ', 'right');
		$this -> db -> join('tbl_courses as c', 'c.course_id  = af.course_id ', 'left');
		$this -> db -> join('tbl_group_master as g', 'g.group_master_id  = af.group_id ', 'left');
		$this->db->where($condition);
		$query = $this -> db -> get();
		//echo $this->db->last_query();exit;
		if($query -> num_rows() >= 1){
			return $query->result_array();
		}else{
			return false;
		}
	}
	function getFeesComponentsforinvoice($admission_fees_id)
	{
		$this -> db -> select('ac.*,f.fees_component_master_id, f.fees_type, f.fees_component_master_name');
		$this -> db -> from('tbl_admission_fees_components as ac');
		$this -> db -> join('tbl_fees_component_master as f', 'ac.fees_component_id  = f.fees_component_master_id', 'inner');
		
		$this->db->where('ac.admission_fees_id',$admission_fees_id);
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}
	function getinstallmentforinvoice($admission_fees_id)
	{
		$this -> db -> select('*');
		$this -> db -> from('tbl_admission_fees_instalments');
		$this->db->where('admission_fees_id',$admission_fees_id);
		
		$query = $this -> db -> get();
		
		if($query -> num_rows() >= 1)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	}

	function updateFinancepeerRecord($tbl_name,$datar,$condition)
	{
		//$this -> db -> where($comp_col, $eid);
		$this->db->where("($condition)");
		$this -> db -> update($tbl_name,$datar);
		// echo $this->db->last_query();exit;
		if ($this->db->affected_rows() > 0){
			return true;
		}else{
			return false;
		} 
	}
}
?>