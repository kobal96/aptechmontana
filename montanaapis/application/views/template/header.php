<!DOCTYPE html>
<html lang="en">
<head>
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Webpanel</title>
	<meta name="description" content="Web Panel Dashboard">	
	<!-- end: Meta -->	
	<!-- start: Mobile Specific -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->	
	<!-- start: CSS -->	
	<link id="base-style-responsive" href="<?PHP echo base_url();?>css/main.css" rel="stylesheet">			
	<link href="https://fonts.googleapis.com/css?family=Niconne" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=PT+Sans" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700" rel="stylesheet">
	<link href="<?PHP echo base_url();?>css/jquery.dataTables.css" rel="stylesheet">
	<link href="<?PHP echo base_url();?>css/jquery.noty.css" rel="stylesheet">
	<link href="<?PHP echo base_url();?>css/noty_theme_default.css" rel="stylesheet">
	<link href="<?PHP echo base_url();?>css/adminLTE.min.css" rel="stylesheet">
    <link href="<?PHP echo base_url();?>css/ionicons.min.css" rel="stylesheet">
   
    
    <link rel="manifest" href="<?PHP echo base_url();?>css/manifest.json">
    <link href="<?PHP echo base_url();?>css/select2.css" type="text/css" rel="stylesheet" />
	<!-- end: CSS -->	
	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="<?PHP echo base_url();?>css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="<?PHP echo base_url();?>css/ie9.css" rel="stylesheet">
	<![endif]-->
	<!-- start: JavaScript -->	
	<script src="<?PHP echo base_url();?>js/jquery-2.1.4.min.js"></script>
	<script src="<?PHP echo base_url();?>js/plugins/jquery-ui.custom.min.js"></script>
	<script src="<?PHP echo base_url();?>js/essential-plugins.js"></script>
	<script src="<?PHP echo base_url();?>js/bootstrap.min.v3.3.6.js"></script>
	<script src="<?PHP echo base_url();?>js/plugins/pace.min.js"></script>	
	<script src="<?PHP echo base_url();?>js/jquery.form.js"></script>
	<script src="<?PHP echo base_url();?>js/jquery.validate.js"></script>
	<script src="<?PHP echo base_url();?>js/additional-methods.js"></script>
	<script src="<?PHP echo base_url();?>js/plugins/bootstrap-datepicker.min.js"></script>
	<!--<script src="<?PHP echo base_url();?>js/bootstrap-clockpicker.min.js"></script>-->
	<!-- Datatable plugin-->
	<script src='<?PHP echo base_url();?>js/jquery.dataTables.min.js'></script>
	<script src='<?PHP echo base_url();?>js/datatable.js'></script>
	<script src="<?PHP echo base_url();?>js/jquery.noty.js"></script>
	<!-- Datatable plugin-->
	<!-- CK Editor plugins -->
	<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="<?PHP echo base_url();?>js/ckeditor/adapters/jquery.js"></script>	  
	<!-- CK Editor plugins -->
    
	<!-- Start: Select2-->
		
	<script type="text/javascript" src="<?PHP echo base_url();?>js/select2.min.js"></script>
    
	<!-- end: Select2-->
	<!-- end: JavaScript -->
    
	<link href="<?PHP echo base_url();?>css/bootstrap-glyphicons.css" rel="stylesheet">
    
    
  
	
	<script>
		function setTabIndex(){
			var tabindex = 1;
			$('input,select,textarea,.icon-plus,.icon-minus,button,a').each(function() {
				if (this.type != "hidden") {
					var $input = $(this);
					$input.attr("tabindex", tabindex);
					tabindex++;
				}
			});
		}
		
		$(function(){
			setTabIndex();
			$(".select2").each(function(){
				$(this).select2({
					placeholder: "Select",
					allowClear: true
				});
				$("#s2id_"+$(this).attr("id")).removeClass("searchInput");
			});
			$(".dataTables_filter input.hasDatepicker").change( function () {				
				oTable.fnFilter( this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $(".searchInput").index(this) ) );
			});			
			window.scrollTo(0,0);
		});
		
		function displayMsg(type,msg)
		{
			
			$.noty({
				text:msg,
				layout:"topRight",
				type:type
			});
		}
	</script>	
    <!-- Old CSS & JS Files-->
</head>

<body class="sidebar-mini fixed">	     
<?php //echo "<pre>";print_r($_SESSION);exit;?>
<div class="wrapper"><!-- Wrapper Start -->	
<!-- Navbar-->
     <header class="main-header hidden-print">
		<a class="logo" href="<?php echo base_url();?>home">
      		<img src="<?PHP echo base_url();?>images/TraworldLogo.png" alt="HashTag" width="50" height="50" />
		</a>
		<nav class="navbar navbar-static-top">
          <!-- Sidebar toggle button--><a class="sidebar-toggle" href="#" data-toggle="offcanvas"></a>
          <!-- Navbar Right Menu-->
			<div class="navbar-custom-menu">
				<ul class="top-nav">
				<!-- User Menu-->
					<li class="dropdown">
						<a class="dropdown-toggle" href="#" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<i class="fa fa-user fa-lg"></i> <?php echo $_SESSION["webadmin"][0]->first_name." ".$_SESSION["webadmin"][0]->last_name; ?>
						</a>
						<ul class="dropdown-menu settings-menu">
							<!-- <li><a href="page-user.html"><i class="fa fa-cog fa-lg"></i> Settings</a></li>-->
							<li><a href="<?PHP echo base_url();?>changepassword/addEdit"><i class="fa fa-user fa-lg"></i> Change Password</a></li>
							<li><a href="<?PHP echo base_url();?>home/logout"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
						</ul>
					</li>
				</ul>
			</div>
        </nav>
     </header>
     <!-- Side-Nav-->
     <aside class="main-sidebar hidden-print">
		<section class="sidebar">          
		<!-- Sidebar Menu-->
          <ul class="sidebar-menu">
			<li class="active"><a href="<?php echo base_url();?>home"><i class="fa fa-dashboard"></i><span style="font-size: 100%;">Dashboard</span></a></li>
			
				<!--<li class="treeview"><a href="#"><i class="fa fa-users"></i><span style="font-size: 100%;"> UAC</span><i class="fa fa-angle-right"></i></a>
					<ul class="treeview-menu">
					
						<li><a href="<?php echo base_url();?>permission"><i class="fa fa-tasks"></i> Permissions</a></li>					
					
						<li><a href="<?php echo base_url();?>roles"><i class="fa fa-exchange"></i> Roles</a></li>					
					
						<li><a href="<?php echo base_url();?>users"><i class="fa fa-user-plus"></i> Admin Users</a></li>
						              
					</ul>
				</li>
				-->
			
         
			
				<li class="treeview"><a href="#"><i class="fa fa-laptop"></i><span style="font-size: 100%;">Master's</span><i class="fa fa-angle-right"></i></a>
					<ul class="treeview-menu">
						
						<li><a href="<?php echo base_url();?>banners"><i class="fa fa-tags"></i>Banner's</a></li>
						<li><a href="<?php echo base_url();?>categories"><i class="fa fa-tag"></i> Categories</a></li>
						<li><a href="<?php echo base_url();?>subcategory"><i class="fa fa-tags"></i> Sub Category</a></li>
						<li><a href="<?php echo base_url();?>color"><i class="fa fa-tags"></i> Color's</a></li>
						<li><a href="<?php echo base_url();?>collectionsmaster"><i class="fa fa-tags"></i> Collections</a></li>
						<li><a href="<?php echo base_url();?>products"><i class="fa fa-tags"></i> Products</a></li>
							
					</ul>
				</li>
           
           
           		          
          </ul>
		</section>
     </aside>	
 		
<noscript>
	<div class="alert alert-block span10">
		<h4 class="alert-heading">Warning!</h4>
		<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
	</div>
</noscript>
